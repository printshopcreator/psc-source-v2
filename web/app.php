<?php
header('P3P:CP="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"');

use App\Kernel;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\ErrorHandler\Debug;
use Symfony\Component\HttpFoundation\Request;
require_once (__DIR__ .'/../src/PSC/Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
require_once (__DIR__ .'/../src/PSC/Shop/EntityBundle/Lagacy/TP_Basket_Collection.php');
require dirname(__DIR__).'/vendor/autoload.php';

(new Dotenv())->bootEnv(dirname(__DIR__).'/.env');

if ($_SERVER['APP_DEBUG']) {
    umask(0000);

    Debug::enable();
}

$kernel = new Kernel($_SERVER['APP_ENV'], (bool) $_SERVER['APP_DEBUG']);
$request = Request::createFromGlobals();
Request::setTrustedProxies(
// the IP address (or range) of your proxy
    ['REMOTE_ADDR', '192.0.0.1', '10.0.0.0/8'],

    // trust *all* "X-Forwarded-*" headers
    Request::HEADER_X_FORWARDED_FOR | Request::HEADER_X_FORWARDED_HOST | Request::HEADER_X_FORWARDED_PORT | Request::HEADER_X_FORWARDED_PROTO

// or, if your proxy instead uses the "Forwarded" header
// Request::HEADER_FORWARDED

// or, if you're using a well-known proxy
// Request::HEADER_X_FORWARDED_AWS_ELB
// Request::HEADER_X_FORWARDED_TRAEFIK
);
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
