(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["plugins/system/psc/invoice"],{

/***/ "./var/plugins/System/PSC/Invoice/Webpack/index.ts":
/*!*********************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/index.ts ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

__webpack_require__(/*! reflect-metadata */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/reflect-metadata/Reflect.js");

__webpack_require__(/*! bootstrap/scss/bootstrap.scss */ "./node_modules/bootstrap/scss/bootstrap.scss");

var app_1 = __webpack_require__(/*! ./src/app/app */ "./var/plugins/System/PSC/Invoice/Webpack/src/app/app.tsx");

var app = new app_1.App(jwt_token);
app.init();
app.run();

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/app/app.tsx":
/*!****************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/app/app.tsx ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.App = void 0;

var token_1 = __webpack_require__(/*! ../services/token */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var React = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var ReactDOM = __webpack_require__(/*! react-dom/client */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-dom/client.js");

var BaseComponent_1 = __webpack_require__(/*! ../modules/base/BaseComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/base/BaseComponent.tsx");

var App =
/** @class */
function () {
  function App(jwt) {
    var token = tsyringe_1.container.resolve(token_1.Token);
    token.currentToken = jwt;
    token.init();
  }

  App.prototype.init = function () {
    this.buildUi();
  };

  App.prototype.buildUi = function () {
    var root = ReactDOM.createRoot(document.getElementById('root'));
    root.render(React.createElement(BaseComponent_1["default"]));
  };

  App.prototype.run = function () {};

  return App;
}();

exports.App = App;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/model/account.ts":
/*!*********************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/model/account.ts ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.Account = void 0;

var uuid_1 = __webpack_require__(/*! uuid */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/uuid/dist/esm-browser/index.js");

var Account =
/** @class */
function () {
  function Account() {
    this.id = 0;
    this.uuid = (0, uuid_1.v4)();
    this.appendix = "";
    this.title = "";
  }

  return Account;
}();

exports.Account = Account;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/model/address.ts":
/*!*********************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/model/address.ts ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.Address = void 0;

var uuid_1 = __webpack_require__(/*! uuid */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/uuid/dist/esm-browser/index.js");

var Address =
/** @class */
function () {
  function Address() {
    this.id = 0;
    this.uuid = (0, uuid_1.v4)();
    this.firstname = "";
    this.lastname = "";
  }

  return Address;
}();

exports.Address = Address;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/model/contact.ts":
/*!*********************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/model/contact.ts ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.Contact = void 0;

var uuid_1 = __webpack_require__(/*! uuid */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/uuid/dist/esm-browser/index.js");

var Contact =
/** @class */
function () {
  function Contact() {
    this.id = 0;
    this.uuid = (0, uuid_1.v4)();
    this.username = "";
    this.password = "";
  }

  return Contact;
}();

exports.Contact = Contact;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/model/product.ts":
/*!*********************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/model/product.ts ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.Product = void 0;

var Product =
/** @class */
function () {
  function Product() {
    this.id = 0;
    this.title = "";
    this.uuid = "";
  }

  return Product;
}();

exports.Product = Product;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/model/productGroup.ts":
/*!**************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/model/productGroup.ts ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
exports.ProductGroup = void 0;

var ProductGroup =
/** @class */
function () {
  function ProductGroup() {
    this.id = 0;
    this.label = "";
    this.uuid = "";
    this.options = [];
  }

  return ProductGroup;
}();

exports.ProductGroup = ProductGroup;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts":
/*!******************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");

exports.__esModule = true;
exports.Shop = void 0;

var Shop =
/** @class */
function () {
  function Shop() {
    this.id = 0;
    this.name = "";
    this.uuid = "";
  }

  return Shop;
}();

exports.Shop = Shop;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/account/AccountSelectComponent.tsx":
/*!***********************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/account/AccountSelectComponent.tsx ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var react_select_async_paginate_1 = __webpack_require__(/*! react-select-async-paginate */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-select-async-paginate/es/index.js");

var account_1 = __webpack_require__(/*! ../../services/account */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/account.ts");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var AccountSelectComponent = function AccountSelectComponent(props) {
  var _a = (0, react_1.useState)(null),
      shopUuid = _a[0],
      setShopUuid = _a[1];

  (0, react_1.useEffect)(function () {
    setShopUuid(props.shop.uuid);
  }, [props.shop.uuid]);
  var order = tsyringe_1.container.resolve(order_1.Order);

  var loadOptions = function loadOptions(searchQuery, loadedOptions) {
    return __awaiter(void 0, void 0, void 0, function () {
      var account_service, data;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            account_service = new account_1.AccountService();
            return [4
            /*yield*/
            , account_service.getAccounts(props.shop)];

          case 1:
            data = _a.sent();
            return [2
            /*return*/
            , {
              options: data,
              hasMore: false
            }];
        }
      });
    });
  };

  var onChange = function onChange(option) {
    order.account = option;
  };

  return (0, jsx_runtime_1.jsx)("div", {
    children: (0, jsx_runtime_1.jsx)(react_select_async_paginate_1.AsyncPaginate, {
      defaultOptions: true,
      loadOptions: loadOptions,
      getOptionValue: function getOptionValue(option) {
        return option.uuid;
      },
      getOptionLabel: function getOptionLabel(option) {
        return option.title;
      },
      onChange: onChange,
      isSearchable: true
    }, JSON.stringify(shopUuid))
  });
};

AccountSelectComponent.propTypes = {
  shop: PropTypes.instanceOf(shop_1.Shop)
};
exports["default"] = AccountSelectComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/base/BaseComponent.tsx":
/*!***********************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/base/BaseComponent.tsx ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var ContactComponent_1 = __webpack_require__(/*! ../contact/ContactComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactComponent.tsx");

var TopBarComponent_1 = __webpack_require__(/*! ../topbar/TopBarComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/topbar/TopBarComponent.tsx");

var PositionsComponent_1 = __webpack_require__(/*! ../positions/PositionsComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/positions/PositionsComponent.tsx");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var BaseComponent = function BaseComponent(props) {
  var _a = (0, react_1.useState)(new shop_1.Shop()),
      shop = _a[0],
      setShop = _a[1];

  return (0, jsx_runtime_1.jsxs)("div", {
    children: [(0, jsx_runtime_1.jsx)("div", {
      children: (0, jsx_runtime_1.jsx)(TopBarComponent_1["default"], {
        shop: shop,
        change: setShop
      })
    }), (0, jsx_runtime_1.jsx)("div", {
      children: shop.id != 0 && (0, jsx_runtime_1.jsx)(ContactComponent_1["default"], {
        shop: shop
      })
    }), (0, jsx_runtime_1.jsx)("div", {
      children: shop.id != 0 && (0, jsx_runtime_1.jsx)(PositionsComponent_1["default"], {
        shop: shop
      })
    })]
  });
};

exports["default"] = BaseComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactAddressSelect.tsx":
/*!*********************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactAddressSelect.tsx ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var react_select_async_paginate_1 = __webpack_require__(/*! react-select-async-paginate */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-select-async-paginate/es/index.js");

var contact_1 = __webpack_require__(/*! ../../model/contact */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/contact.ts");

var address_1 = __webpack_require__(/*! ../../model/address */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/address.ts");

var contact_2 = __webpack_require__(/*! ../../services/contact */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/contact.ts");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var ContactAddressSelect = function ContactAddressSelect(props) {
  var _a = (0, react_1.useState)(null),
      contactUuid = _a[0],
      setContactUuid = _a[1];

  (0, react_1.useEffect)(function () {
    setContactUuid(props.contact.uuid);
  }, [props.contact.uuid]);

  var loadOptions = function loadOptions(searchQuery, loadedOptions) {
    return __awaiter(void 0, void 0, void 0, function () {
      var contact_service, data;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            contact_service = new contact_2.ContactService();
            return [4
            /*yield*/
            , contact_service.getContactAddress(contactUuid, props.type)];

          case 1:
            data = _a.sent();
            return [2
            /*return*/
            , {
              options: data,
              hasMore: false
            }];
        }
      });
    });
  };

  var onChange = function onChange(option) {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }
  };

  return (0, jsx_runtime_1.jsx)(react_select_async_paginate_1.AsyncPaginate, {
    defaultOptions: true,
    value: props.value,
    loadOptions: loadOptions,
    getOptionValue: function getOptionValue(option) {
      return option.uuid;
    },
    getOptionLabel: function getOptionLabel(option) {
      return option.firstname + ' ' + option.lastname;
    },
    onChange: onChange,
    isSearchable: false
  }, JSON.stringify(contactUuid));
};

ContactAddressSelect.propTypes = {
  value: PropTypes.instanceOf(address_1.Address),
  contact: PropTypes.instanceOf(contact_1.Contact),
  type: PropTypes.number,
  onChange: PropTypes.func
};
exports["default"] = ContactAddressSelect;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactComponent.tsx":
/*!*****************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactComponent.tsx ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.object.assign */ "./node_modules/core-js/modules/es.object.assign.js");

var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var ContactSelect_1 = __webpack_require__(/*! ./ContactSelect */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactSelect.tsx");

var ContactAddressSelect_1 = __webpack_require__(/*! ./ContactAddressSelect */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactAddressSelect.tsx");

var contact_1 = __webpack_require__(/*! ../../model/contact */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/contact.ts");

var address_1 = __webpack_require__(/*! ../../model/address */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/address.ts");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var AccountSelectComponent_1 = __webpack_require__(/*! ../account/AccountSelectComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/account/AccountSelectComponent.tsx");

var ContactComponent = function ContactComponent(props) {
  var orderState = tsyringe_1.container.resolve(order_1.Order);

  var _a = (0, react_1.useState)(new contact_1.Contact()),
      contact = _a[0],
      setContact = _a[1];

  var _b = (0, react_1.useState)(new address_1.Address()),
      contactAddressInvoice = _b[0],
      setContactAddressInvoice = _b[1];

  var _c = (0, react_1.useState)(new address_1.Address()),
      contactAddressDelivery = _c[0],
      setContactAddressDelivery = _c[1];

  var _d = (0, react_1.useState)(new address_1.Address()),
      contactAddressSender = _d[0],
      setContactAddressSender = _d[1];

  orderState.deliveryAddress = contactAddressDelivery;
  orderState.invoiceAddress = contactAddressInvoice;
  orderState.senderAddress = contactAddressSender;
  return (0, jsx_runtime_1.jsxs)("div", {
    children: [(0, jsx_runtime_1.jsxs)("div", __assign({
      className: 'row'
    }, {
      children: [(0, jsx_runtime_1.jsxs)("div", __assign({
        className: 'col-6'
      }, {
        children: [(0, jsx_runtime_1.jsx)("h4", {
          children: "Contact"
        }), (0, jsx_runtime_1.jsx)(ContactSelect_1["default"], {
          value: contact,
          shop: props.shop,
          onChange: setContact
        })]
      })), (0, jsx_runtime_1.jsxs)("div", __assign({
        className: 'col-6'
      }, {
        children: [(0, jsx_runtime_1.jsx)("h4", {
          children: "Account"
        }), (0, jsx_runtime_1.jsx)(AccountSelectComponent_1["default"], {
          shop: props.shop
        })]
      }))]
    })), (0, jsx_runtime_1.jsxs)("div", __assign({
      className: 'row'
    }, {
      children: [(0, jsx_runtime_1.jsxs)("div", __assign({
        className: 'col-4'
      }, {
        children: [(0, jsx_runtime_1.jsx)("h4", {
          children: "Invoice"
        }), (0, jsx_runtime_1.jsx)(ContactAddressSelect_1["default"], {
          value: contactAddressInvoice,
          contact: contact,
          type: 1,
          onChange: setContactAddressInvoice
        })]
      })), (0, jsx_runtime_1.jsxs)("div", __assign({
        className: 'col-4'
      }, {
        children: [(0, jsx_runtime_1.jsx)("h4", {
          children: "Delivery"
        }), (0, jsx_runtime_1.jsx)(ContactAddressSelect_1["default"], {
          value: contactAddressDelivery,
          contact: contact,
          type: 2,
          onChange: setContactAddressDelivery
        })]
      })), (0, jsx_runtime_1.jsxs)("div", __assign({
        className: 'col-4'
      }, {
        children: [(0, jsx_runtime_1.jsx)("h4", {
          children: "Sender"
        }), (0, jsx_runtime_1.jsx)(ContactAddressSelect_1["default"], {
          value: contactAddressSender,
          contact: contact,
          type: 3,
          onChange: setContactAddressSender
        })]
      }))]
    }))]
  });
};

exports["default"] = ContactComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactSelect.tsx":
/*!**************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/contact/ContactSelect.tsx ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var react_select_async_paginate_1 = __webpack_require__(/*! react-select-async-paginate */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-select-async-paginate/es/index.js");

var contact_1 = __webpack_require__(/*! ../../model/contact */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/contact.ts");

var contact_2 = __webpack_require__(/*! ../../services/contact */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/contact.ts");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var ContactSelect = function ContactSelect(props) {
  var _a = (0, react_1.useState)(null),
      shopUuid = _a[0],
      setShopUuid = _a[1];

  (0, react_1.useEffect)(function () {
    setShopUuid(props.shop.uuid);
  }, [props.shop.uuid]);
  var order = tsyringe_1.container.resolve(order_1.Order);

  var loadOptions = function loadOptions(searchQuery, loadedOptions, _a) {
    var page = _a.page;
    return __awaiter(void 0, void 0, void 0, function () {
      var contact_api, contacts;
      return __generator(this, function (_b) {
        switch (_b.label) {
          case 0:
            contact_api = new contact_2.ContactService();
            return [4
            /*yield*/
            , contact_api.getContacts(searchQuery, page, props.shop)];

          case 1:
            contacts = _b.sent();
            return [2
            /*return*/
            , {
              options: contacts,
              hasMore: false,
              additional: {
                page: page + 1
              }
            }];
        }
      });
    });
  };

  var onChange = function onChange(option) {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }

    order.contact = option;
  };

  return (0, jsx_runtime_1.jsx)(react_select_async_paginate_1.AsyncPaginate, {
    defaultOptions: true,
    value: props.value,
    loadOptions: loadOptions,
    getOptionValue: function getOptionValue(option) {
      return option.uuid;
    },
    getOptionLabel: function getOptionLabel(option) {
      return option.username;
    },
    onChange: onChange,
    isSearchable: true,
    additional: {
      page: 1
    }
  }, JSON.stringify(shopUuid));
};

ContactSelect.propTypes = {
  value: PropTypes.instanceOf(contact_1.Contact),
  onChange: PropTypes.func,
  shop: PropTypes.instanceOf(shop_1.Shop)
};
exports["default"] = ContactSelect;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/positions/AddPositionComponent.tsx":
/*!***********************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/positions/AddPositionComponent.tsx ***!
  \***********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.object.assign */ "./node_modules/core-js/modules/es.object.assign.js");

var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var Button_1 = __webpack_require__(/*! react-bootstrap/Button */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-bootstrap/esm/Button.js");

var Modal_1 = __webpack_require__(/*! react-bootstrap/Modal */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-bootstrap/esm/Modal.js");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var product_1 = __webpack_require__(/*! ../../model/product */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/product.ts");

var productGroup_1 = __webpack_require__(/*! ../../model/productGroup */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/productGroup.ts");

var ProductGroupSelect_1 = __webpack_require__(/*! ../product/ProductGroupSelect */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductGroupSelect.tsx");

var ProductSelect_1 = __webpack_require__(/*! ../product/ProductSelect */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductSelect.tsx");

var ProductForm_1 = __webpack_require__(/*! ../product/ProductForm */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductForm.tsx");

var AddPositionComponent = function AddPositionComponent(props) {
  var _a = (0, react_1.useState)(null),
      shop = _a[0],
      setShop = _a[1];

  (0, react_1.useEffect)(function () {
    setShop(props.shop);
  }, [props.shop]);

  var _b = (0, react_1.useState)(false),
      show = _b[0],
      setShow = _b[1];

  var handleClose = function handleClose() {
    return setShow(false);
  };

  var handleShow = function handleShow() {
    return setShow(true);
  };

  var _c = (0, react_1.useState)(new product_1.Product()),
      product = _c[0],
      setProduct = _c[1];

  var _d = (0, react_1.useState)(new productGroup_1.ProductGroup()),
      productGroup = _d[0],
      setProductGroup = _d[1];

  return (0, jsx_runtime_1.jsxs)("div", {
    children: [(0, jsx_runtime_1.jsx)(Button_1["default"], __assign({
      variant: "primary",
      onClick: handleShow
    }, {
      children: "add position"
    })), (0, jsx_runtime_1.jsxs)(Modal_1["default"], __assign({
      show: show,
      fullscreen: true,
      onHide: handleClose
    }, {
      children: [(0, jsx_runtime_1.jsx)(Modal_1["default"].Header, __assign({
        closeButton: true
      }, {
        children: (0, jsx_runtime_1.jsx)(Modal_1["default"].Title, {
          children: "add position"
        })
      })), (0, jsx_runtime_1.jsxs)(Modal_1["default"].Body, {
        children: [(0, jsx_runtime_1.jsxs)("div", __assign({
          className: 'row'
        }, {
          children: [(0, jsx_runtime_1.jsxs)("div", __assign({
            className: 'col-6'
          }, {
            children: [(0, jsx_runtime_1.jsx)("h4", {
              children: "ProductGroup"
            }), (0, jsx_runtime_1.jsx)(ProductGroupSelect_1["default"], {
              value: productGroup,
              shop: shop,
              onChange: setProductGroup
            })]
          })), (0, jsx_runtime_1.jsxs)("div", __assign({
            className: 'col-6'
          }, {
            children: [(0, jsx_runtime_1.jsx)("h4", {
              children: "Product"
            }), (0, jsx_runtime_1.jsx)(ProductSelect_1["default"], {
              value: product,
              shop: shop,
              productGroup: productGroup,
              onChange: setProduct
            })]
          }))]
        })), (0, jsx_runtime_1.jsx)(ProductForm_1["default"], {
          product: product
        })]
      })]
    }))]
  });
};

AddPositionComponent.propTypes = {
  shop: PropTypes.instanceOf(shop_1.Shop)
};
exports["default"] = AddPositionComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/positions/PositionsComponent.tsx":
/*!*********************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/positions/PositionsComponent.tsx ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var AddPositionComponent_1 = __webpack_require__(/*! ./AddPositionComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/positions/AddPositionComponent.tsx");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var PositionsComponent = function PositionsComponent(props) {
  return (0, jsx_runtime_1.jsx)(jsx_runtime_1.Fragment, {
    children: (0, jsx_runtime_1.jsx)(AddPositionComponent_1["default"], {
      shop: props.shop
    })
  });
};

PositionsComponent.propTypes = {
  shop: PropTypes.instanceOf(shop_1.Shop)
};
exports["default"] = PositionsComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductForm.tsx":
/*!************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductForm.tsx ***!
  \************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.function.bind */ "./node_modules/core-js/modules/es.function.bind.js");

__webpack_require__(/*! core-js/modules/es.object.assign */ "./node_modules/core-js/modules/es.object.assign.js");

var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var product_1 = __webpack_require__(/*! ../../model/product */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/product.ts");

var validator_ajv6_1 = __webpack_require__(/*! @rjsf/validator-ajv6 */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/@rjsf/validator-ajv6/dist/validator-ajv6.esm.js");

var core_1 = __webpack_require__(/*! @rjsf/core */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/@rjsf/core/dist/core.esm.js");

var product_2 = __webpack_require__(/*! ../../services/product */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/product.ts");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var ProductForm = function ProductForm(props) {
  var _a = (0, react_1.useState)({}),
      schema = _a[0],
      setSchema = _a[1];

  var loadSchema = function loadSchema() {
    if (props.product.uuid == "") {
      return [];
    }

    var product_api = new product_2.ProductService();
    product_api.getAddProductSchema(props.product).then(function (value) {
      setSchema(value['jsonForm']);
    });
  };

  (0, react_1.useEffect)(function () {
    loadSchema();
  }, [props.product]);

  var log = function log(type) {
    return console.log.bind(console, type);
  };

  return (0, jsx_runtime_1.jsx)(jsx_runtime_1.Fragment, {
    children: (0, jsx_runtime_1.jsxs)("div", __assign({
      className: 'row'
    }, {
      children: [(0, jsx_runtime_1.jsx)("div", __assign({
        className: 'col-6'
      }, {
        children: (0, jsx_runtime_1.jsx)(core_1["default"], {
          schema: schema,
          validator: validator_ajv6_1["default"],
          onChange: log("changed"),
          onSubmit: log("submitted"),
          onError: log("errors")
        })
      })), (0, jsx_runtime_1.jsxs)("div", __assign({
        className: 'col-6'
      }, {
        children: [(0, jsx_runtime_1.jsx)("h5", {
          children: "Netto: "
        }), (0, jsx_runtime_1.jsx)("h5", {
          children: "Mwert: "
        }), (0, jsx_runtime_1.jsx)("h4", {
          children: "Brutto"
        })]
      }))]
    }))
  });
};

ProductForm.propTypes = {
  shop: PropTypes.instanceOf(shop_1.Shop),
  product: PropTypes.instanceOf(product_1.Product)
};
exports["default"] = ProductForm;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductGroupSelect.tsx":
/*!*******************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductGroupSelect.tsx ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var productGroup_1 = __webpack_require__(/*! ../../model/productGroup */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/productGroup.ts");

var productgroup_1 = __webpack_require__(/*! ../../services/productgroup */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/productgroup.ts");

var async_1 = __webpack_require__(/*! react-select/async */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-select/async/dist/react-select.esm.js");

var ProductGroupSelect = function ProductGroupSelect(props) {
  var order = tsyringe_1.container.resolve(order_1.Order);

  var loadOptions = function loadOptions(searchQuery) {
    return __awaiter(void 0, void 0, void 0, function () {
      var productgroup_api;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            productgroup_api = new productgroup_1.ProductGroupService();
            return [4
            /*yield*/
            , productgroup_api.getProductGroups(searchQuery, props.shop)];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  var onChange = function onChange(option) {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }
  };

  return (0, jsx_runtime_1.jsx)(async_1["default"], {
    defaultOptions: true,
    value: props.value,
    loadOptions: loadOptions,
    getOptionValue: function getOptionValue(option) {
      return option.uuid;
    },
    onChange: onChange,
    isSearchable: true
  }, JSON.stringify(props.shop.uuid));
};

ProductGroupSelect.propTypes = {
  shop: PropTypes.instanceOf(shop_1.Shop),
  value: PropTypes.instanceOf(productGroup_1.ProductGroup),
  onChange: PropTypes.func
};
exports["default"] = ProductGroupSelect;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductSelect.tsx":
/*!**************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/product/ProductSelect.tsx ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var PropTypes = __webpack_require__(/*! prop-types */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/prop-types/index.js");

var shop_1 = __webpack_require__(/*! ../../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var product_1 = __webpack_require__(/*! ../../model/product */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/product.ts");

var productGroup_1 = __webpack_require__(/*! ../../model/productGroup */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/productGroup.ts");

var async_1 = __webpack_require__(/*! react-select/async */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-select/async/dist/react-select.esm.js");

var product_2 = __webpack_require__(/*! ../../services/product */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/product.ts");

function debounce(inner, ms) {
  if (ms === void 0) {
    ms = 300;
  }

  var timer = null;
  var resolves = [];
  return function () {
    var args = [];

    for (var _i = 0; _i < arguments.length; _i++) {
      args[_i] = arguments[_i];
    } // Run the function after a certain amount of time


    clearTimeout(timer);
    timer = setTimeout(function () {
      // Get the result of the inner function, then apply it to the resolve function of
      // each promise that has been created since the last time the inner function was run
      var result = inner.apply(void 0, args);
      resolves.forEach(function (r) {
        return r(result);
      });
      resolves = [];
    }, ms);
    return new Promise(function (r) {
      return resolves.push(r);
    });
  };
}

var ProductSelect = function ProductSelect(props) {
  var order = tsyringe_1.container.resolve(order_1.Order);
  var loadOptions = debounce(function (searchQuery) {
    return __awaiter(void 0, void 0, void 0, function () {
      var product_api;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            if (searchQuery == "" && props.productGroup.uuid == "") {
              return [2
              /*return*/
              , []];
            }

            product_api = new product_2.ProductService();
            if (!(searchQuery != "")) return [3
            /*break*/
            , 2];
            return [4
            /*yield*/
            , product_api.getProductsByTerm(searchQuery, props.shop)];

          case 1:
            return [2
            /*return*/
            , _a.sent()];

          case 2:
            return [4
            /*yield*/
            , product_api.getProducts(props.productGroup)];

          case 3:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  });

  var onChange = function onChange(option) {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }
  };

  return (0, jsx_runtime_1.jsx)(async_1["default"], {
    defaultOptions: true,
    value: props.value,
    loadOptions: loadOptions,
    getOptionValue: function getOptionValue(option) {
      return option.uuid;
    },
    getOptionLabel: function getOptionLabel(option) {
      return option.title;
    },
    onChange: onChange,
    isSearchable: true
  }, JSON.stringify(props.productGroup.uuid));
};

ProductSelect.propTypes = {
  shop: PropTypes.instanceOf(shop_1.Shop),
  value: PropTypes.instanceOf(product_1.Product),
  productGroup: PropTypes.instanceOf(productGroup_1.ProductGroup),
  onChange: PropTypes.func
};
exports["default"] = ProductSelect;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/shop/ShopSelectComponent.tsx":
/*!*****************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/shop/ShopSelectComponent.tsx ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var react_select_async_paginate_1 = __webpack_require__(/*! react-select-async-paginate */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-select-async-paginate/es/index.js");

var shop_1 = __webpack_require__(/*! ../../services/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/shop.ts");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var ShopSelectComponent = function ShopSelectComponent(props) {
  var order = tsyringe_1.container.resolve(order_1.Order);

  var loadOptions = function loadOptions(searchQuery, loadedOptions) {
    return __awaiter(void 0, void 0, void 0, function () {
      var shop_service, data;
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            shop_service = new shop_1.ShopService();
            return [4
            /*yield*/
            , shop_service.getShops()];

          case 1:
            data = _a.sent();
            return [2
            /*return*/
            , {
              options: data,
              hasMore: false
            }];
        }
      });
    });
  };

  var onChange = function onChange(option) {
    order.shop = option.id;
    props.change(option);
  };

  return (0, jsx_runtime_1.jsx)("div", {
    children: (0, jsx_runtime_1.jsx)(react_select_async_paginate_1.AsyncPaginate, {
      defaultOptions: true,
      loadOptions: loadOptions,
      getOptionValue: function getOptionValue(option) {
        return option.id;
      },
      getOptionLabel: function getOptionLabel(option) {
        return option.name;
      },
      onChange: onChange,
      isSearchable: false
    })
  });
};

exports["default"] = ShopSelectComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/topbar/TopBarComponent.tsx":
/*!***************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/topbar/TopBarComponent.tsx ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.object.assign */ "./node_modules/core-js/modules/es.object.assign.js");

var __assign = this && this.__assign || function () {
  __assign = Object.assign || function (t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
      s = arguments[i];

      for (var p in s) {
        if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
      }
    }

    return t;
  };

  return __assign.apply(this, arguments);
};

exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var order_2 = __webpack_require__(/*! ../../services/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/order.ts");

var TypeSelectComponent_1 = __webpack_require__(/*! ../type/TypeSelectComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/type/TypeSelectComponent.tsx");

var ShopSelectComponent_1 = __webpack_require__(/*! ../shop/ShopSelectComponent */ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/shop/ShopSelectComponent.tsx");

var TopBarComponent = function TopBarComponent(props) {
  var orderService = tsyringe_1.container.resolve(order_2.OrderService);
  var order = tsyringe_1.container.resolve(order_1.Order);

  var handleSave = function handleSave(e) {
    e.preventDefault();
    orderService.saveOrder(order);
  };

  return (0, jsx_runtime_1.jsxs)("div", __assign({
    className: 'row'
  }, {
    children: [(0, jsx_runtime_1.jsxs)("div", __assign({
      className: 'col-4'
    }, {
      children: [(0, jsx_runtime_1.jsx)("h4", {
        children: "Typ"
      }), " ", (0, jsx_runtime_1.jsx)(TypeSelectComponent_1["default"], {})]
    })), (0, jsx_runtime_1.jsxs)("div", __assign({
      className: 'col-4'
    }, {
      children: [(0, jsx_runtime_1.jsx)("h4", {
        children: "Shop"
      }), " ", (0, jsx_runtime_1.jsx)(ShopSelectComponent_1["default"], {
        shop: props.shop,
        change: props.change
      })]
    })), (0, jsx_runtime_1.jsx)("div", __assign({
      className: 'col-4'
    }, {
      children: (0, jsx_runtime_1.jsx)("button", __assign({
        className: 'btn btn-success',
        onClick: handleSave
      }, {
        children: "Save Order"
      }))
    }))]
  }));
};

exports["default"] = TopBarComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/modules/type/TypeSelectComponent.tsx":
/*!*****************************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/modules/type/TypeSelectComponent.tsx ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;

var jsx_runtime_1 = __webpack_require__(/*! react/jsx-runtime */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/jsx-runtime.js");

var react_select_1 = __webpack_require__(/*! react-select */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react-select/dist/react-select.esm.js");

var react_1 = __webpack_require__(/*! react */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/react/index.js");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var order_1 = __webpack_require__(/*! ../../state/order */ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts");

var TypeSelectComponent = function TypeSelectComponent(props) {
  var _a = (0, react_1.useState)(),
      type = _a[0],
      setType = _a[1];

  var order = tsyringe_1.container.resolve(order_1.Order);
  var options = [{
    value: 1,
    label: 'Auftrag'
  }, {
    value: 2,
    label: 'Angebot'
  }, {
    value: 3,
    label: 'Storno'
  }, {
    value: 4,
    label: 'Gutschrift'
  }, {
    value: 5,
    label: 'Rechnung'
  }, {
    value: 6,
    label: 'Teilrechnung'
  }, {
    value: 7,
    label: 'Teilstorno'
  }];

  var onChange = function onChange(option) {
    order.type = option.value;
  };

  return (0, jsx_runtime_1.jsx)("div", {
    children: (0, jsx_runtime_1.jsx)(react_select_1["default"], {
      options: options,
      onChange: onChange
    })
  });
};

exports["default"] = TypeSelectComponent;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/services/account.ts":
/*!************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/services/account.ts ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var __metadata = this && this.__metadata || function (k, v) {
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.AccountService = void 0;

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var token_1 = __webpack_require__(/*! ./token */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts");

var axios_1 = __webpack_require__(/*! axios */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/axios/index.js");

var account_1 = __webpack_require__(/*! ../model/account */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/account.ts");

var AccountService =
/** @class */
function () {
  function AccountService(token) {
    this.token = token;
  }

  AccountService.prototype.getAccounts = function (shop) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/apps/api/account/allbyshop/' + shop.uuid, {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data.data.map(function (item) {
                var account = new account_1.Account();
                account.id = item.id;
                account.title = item.title;
                account.appendix = item.appendix;
                account.uuid = item.uuid;
                return account;
              });
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  AccountService = __decorate([(0, tsyringe_1.singleton)(), (0, tsyringe_1.autoInjectable)(), __metadata("design:paramtypes", [token_1.Token])], AccountService);
  return AccountService;
}();

exports.AccountService = AccountService;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/services/contact.ts":
/*!************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/services/contact.ts ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var __metadata = this && this.__metadata || function (k, v) {
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.ContactService = void 0;

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var token_1 = __webpack_require__(/*! ./token */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts");

var axios_1 = __webpack_require__(/*! axios */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/axios/index.js");

var contact_1 = __webpack_require__(/*! ../model/contact */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/contact.ts");

var address_1 = __webpack_require__(/*! ../model/address */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/address.ts");

var ContactService =
/** @class */
function () {
  function ContactService(token) {
    this.token = token;
  }

  ContactService.prototype.getContacts = function (searchQuery, page, shop) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/apps/api/contact/allbyshop/' + shop.uuid + '?term=' + searchQuery + '&page=' + page, {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data.data.map(function (item) {
                var contact = new contact_1.Contact();
                contact.id = item.id;
                contact.username = item.username;
                contact.uuid = item.uuid;
                return contact;
              });
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  ContactService.prototype.getContactAddress = function (contact, type) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/apps/api/contact/address/getallfortype/' + contact + '/' + type, {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data.data.map(function (item) {
                var address = new address_1.Address();
                address.id = item.id;
                address.firstname = item.firstname;
                address.lastname = item.lastname;
                address.uuid = item.uuid;
                return address;
              });
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  ContactService = __decorate([(0, tsyringe_1.singleton)(), (0, tsyringe_1.autoInjectable)(), __metadata("design:paramtypes", [token_1.Token])], ContactService);
  return ContactService;
}();

exports.ContactService = ContactService;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/services/order.ts":
/*!**********************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/services/order.ts ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var __metadata = this && this.__metadata || function (k, v) {
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.OrderService = void 0;

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var token_1 = __webpack_require__(/*! ./token */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts");

var axios_1 = __webpack_require__(/*! axios */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/axios/index.js");

var OrderService =
/** @class */
function () {
  function OrderService(token) {
    this.token = token;
  }

  OrderService.prototype.saveOrder = function (order) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].post('/apps/api/order/create', order, {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              console.log(response);
              return response.data;
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  OrderService = __decorate([(0, tsyringe_1.singleton)(), (0, tsyringe_1.autoInjectable)(), __metadata("design:paramtypes", [token_1.Token])], OrderService);
  return OrderService;
}();

exports.OrderService = OrderService;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/services/product.ts":
/*!************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/services/product.ts ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var __metadata = this && this.__metadata || function (k, v) {
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.ProductService = void 0;

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var token_1 = __webpack_require__(/*! ./token */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts");

var axios_1 = __webpack_require__(/*! axios */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/axios/index.js");

var product_1 = __webpack_require__(/*! ../model/product */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/product.ts");

var ProductService =
/** @class */
function () {
  function ProductService(token) {
    this.token = token;
  }

  ProductService.prototype.getProductsByTerm = function (term, shop) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/apps/api/product/getallbyterm/' + shop.uuid + '/' + term, {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data.data.map(function (item) {
                var p = new product_1.Product();
                p.id = item.id;
                p.title = item.title;
                p.uuid = item.uuid;
                return p;
              });
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  ProductService.prototype.getProducts = function (productGroup) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/apps/api/product/getallbyproductgroup/' + productGroup.uuid, {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data.data.map(function (item) {
                var p = new product_1.Product();
                p.id = item.id;
                p.title = item.title;
                p.uuid = item.uuid;
                return p;
              });
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  ProductService.prototype.getAddProductSchema = function (product) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].post('/apps/api/product/get/form/json/' + product.uuid, [], {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data;
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  ProductService = __decorate([(0, tsyringe_1.singleton)(), (0, tsyringe_1.autoInjectable)(), __metadata("design:paramtypes", [token_1.Token])], ProductService);
  return ProductService;
}();

exports.ProductService = ProductService;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/services/productgroup.ts":
/*!*****************************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/services/productgroup.ts ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var __metadata = this && this.__metadata || function (k, v) {
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.ProductGroupService = void 0;

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var token_1 = __webpack_require__(/*! ./token */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts");

var axios_1 = __webpack_require__(/*! axios */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/axios/index.js");

var productGroup_1 = __webpack_require__(/*! ../model/productGroup */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/productGroup.ts");

var ProductGroupService =
/** @class */
function () {
  function ProductGroupService(token) {
    this.token = token;
  }

  ProductGroupService.prototype.getProductGroups = function (searchQuery, shop) {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/apps/api/productgroup/gettree/' + shop.uuid + '?term=' + searchQuery, {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data.data.map(function (item) {
                var pg = new productGroup_1.ProductGroup();
                pg.id = item.id;
                pg.label = item.title;
                pg.uuid = item.uuid;
                item.children.map(function (sub) {
                  var spg = new productGroup_1.ProductGroup();
                  spg.id = sub.id;
                  spg.label = sub.title;
                  spg.uuid = sub.uuid;
                  pg.options.push(spg);
                });
                return pg;
              });
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  ProductGroupService = __decorate([(0, tsyringe_1.singleton)(), (0, tsyringe_1.autoInjectable)(), __metadata("design:paramtypes", [token_1.Token])], ProductGroupService);
  return ProductGroupService;
}();

exports.ProductGroupService = ProductGroupService;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/services/shop.ts":
/*!*********************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/services/shop.ts ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.promise */ "./node_modules/core-js/modules/es.promise.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var __metadata = this && this.__metadata || function (k, v) {
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var __awaiter = this && this.__awaiter || function (thisArg, _arguments, P, generator) {
  function adopt(value) {
    return value instanceof P ? value : new P(function (resolve) {
      resolve(value);
    });
  }

  return new (P || (P = Promise))(function (resolve, reject) {
    function fulfilled(value) {
      try {
        step(generator.next(value));
      } catch (e) {
        reject(e);
      }
    }

    function rejected(value) {
      try {
        step(generator["throw"](value));
      } catch (e) {
        reject(e);
      }
    }

    function step(result) {
      result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected);
    }

    step((generator = generator.apply(thisArg, _arguments || [])).next());
  });
};

var __generator = this && this.__generator || function (thisArg, body) {
  var _ = {
    label: 0,
    sent: function sent() {
      if (t[0] & 1) throw t[1];
      return t[1];
    },
    trys: [],
    ops: []
  },
      f,
      y,
      t,
      g;
  return g = {
    next: verb(0),
    "throw": verb(1),
    "return": verb(2)
  }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
    return this;
  }), g;

  function verb(n) {
    return function (v) {
      return step([n, v]);
    };
  }

  function step(op) {
    if (f) throw new TypeError("Generator is already executing.");

    while (_) {
      try {
        if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
        if (y = 0, t) op = [op[0] & 2, t.value];

        switch (op[0]) {
          case 0:
          case 1:
            t = op;
            break;

          case 4:
            _.label++;
            return {
              value: op[1],
              done: false
            };

          case 5:
            _.label++;
            y = op[1];
            op = [0];
            continue;

          case 7:
            op = _.ops.pop();

            _.trys.pop();

            continue;

          default:
            if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
              _ = 0;
              continue;
            }

            if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
              _.label = op[1];
              break;
            }

            if (op[0] === 6 && _.label < t[1]) {
              _.label = t[1];
              t = op;
              break;
            }

            if (t && _.label < t[2]) {
              _.label = t[2];

              _.ops.push(op);

              break;
            }

            if (t[2]) _.ops.pop();

            _.trys.pop();

            continue;
        }

        op = body.call(thisArg, _);
      } catch (e) {
        op = [6, e];
        y = 0;
      } finally {
        f = t = 0;
      }
    }

    if (op[0] & 5) throw op[1];
    return {
      value: op[0] ? op[1] : void 0,
      done: true
    };
  }
};

exports.__esModule = true;
exports.ShopService = void 0;

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var token_1 = __webpack_require__(/*! ./token */ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts");

var axios_1 = __webpack_require__(/*! axios */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/axios/index.js");

var shop_1 = __webpack_require__(/*! ../model/shop */ "./var/plugins/System/PSC/Invoice/Webpack/src/model/shop.ts");

var ShopService =
/** @class */
function () {
  function ShopService(token) {
    this.token = token;
  }

  ShopService.prototype.getShops = function () {
    return __awaiter(this, void 0, void 0, function () {
      return __generator(this, function (_a) {
        switch (_a.label) {
          case 0:
            return [4
            /*yield*/
            , axios_1["default"].get('/apps/api/shops', {
              headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
              }
            }).then(function (response) {
              return response.data.data.map(function (item) {
                var shop = new shop_1.Shop();
                shop.id = item.id;
                shop.name = item.name;
                shop.uuid = item.uuid;
                return shop;
              });
            })];

          case 1:
            return [2
            /*return*/
            , _a.sent()];
        }
      });
    });
  };

  ShopService = __decorate([(0, tsyringe_1.singleton)(), (0, tsyringe_1.autoInjectable)(), __metadata("design:paramtypes", [token_1.Token])], ShopService);
  return ShopService;
}();

exports.ShopService = ShopService;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts":
/*!**********************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/services/token.ts ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

exports.__esModule = true;
exports.Token = void 0;

var axios_1 = __webpack_require__(/*! axios */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/axios/index.js");

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var Token =
/** @class */
function () {
  function Token() {}

  Token.prototype.init = function () {
    var _this = this;

    setInterval(function () {
      _this.refreshToken();
    }, 2 * 60 * 1000);
  };

  Token.prototype.refreshToken = function () {
    var _this = this;

    axios_1["default"].get('/apps/api/contact/refresh', {
      headers: {
        'Authorization': 'Bearer ' + this.currentToken
      }
    }).then(function (response) {
      _this.currentToken = response.data.token;
    });
  };

  Token = __decorate([(0, tsyringe_1.singleton)()], Token);
  return Token;
}();

exports.Token = Token;

/***/ }),

/***/ "./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts":
/*!*******************************************************************!*\
  !*** ./var/plugins/System/PSC/Invoice/Webpack/src/state/order.ts ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var __decorate = this && this.__decorate || function (decorators, target, key, desc) {
  var c = arguments.length,
      r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
      d;
  if ((typeof Reflect === "undefined" ? "undefined" : _typeof(Reflect)) === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) {
    if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
  }
  return c > 3 && r && Object.defineProperty(target, key, r), r;
};

exports.__esModule = true;
exports.Order = void 0;

var tsyringe_1 = __webpack_require__(/*! tsyringe */ "./var/plugins/System/PSC/Invoice/Webpack/node_modules/tsyringe/dist/esm5/index.js");

var Order =
/** @class */
function () {
  function Order() {}

  Order = __decorate([(0, tsyringe_1.singleton)()], Order);
  return Order;
}();

exports.Order = Order;

/***/ })

},[["./var/plugins/System/PSC/Invoice/Webpack/index.ts","runtime","vendors~backend/dashboard~plugins/system/psc/invoice","vendors~plugins/system/psc/invoice"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9pbmRleC50cyIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvYXBwL2FwcC50c3giLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZGVsL2FjY291bnQudHMiLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZGVsL2FkZHJlc3MudHMiLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZGVsL2NvbnRhY3QudHMiLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZGVsL3Byb2R1Y3QudHMiLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZGVsL3Byb2R1Y3RHcm91cC50cyIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvbW9kZWwvc2hvcC50cyIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvbW9kdWxlcy9hY2NvdW50L0FjY291bnRTZWxlY3RDb21wb25lbnQudHN4Iiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9tb2R1bGVzL2Jhc2UvQmFzZUNvbXBvbmVudC50c3giLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZHVsZXMvY29udGFjdC9Db250YWN0QWRkcmVzc1NlbGVjdC50c3giLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZHVsZXMvY29udGFjdC9Db250YWN0Q29tcG9uZW50LnRzeCIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvbW9kdWxlcy9jb250YWN0L0NvbnRhY3RTZWxlY3QudHN4Iiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9tb2R1bGVzL3Bvc2l0aW9ucy9BZGRQb3NpdGlvbkNvbXBvbmVudC50c3giLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZHVsZXMvcG9zaXRpb25zL1Bvc2l0aW9uc0NvbXBvbmVudC50c3giLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZHVsZXMvcHJvZHVjdC9Qcm9kdWN0Rm9ybS50c3giLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZHVsZXMvcHJvZHVjdC9Qcm9kdWN0R3JvdXBTZWxlY3QudHN4Iiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9tb2R1bGVzL3Byb2R1Y3QvUHJvZHVjdFNlbGVjdC50c3giLCJ3ZWJwYWNrOi8vLy4vdmFyL3BsdWdpbnMvU3lzdGVtL1BTQy9JbnZvaWNlL1dlYnBhY2svc3JjL21vZHVsZXMvc2hvcC9TaG9wU2VsZWN0Q29tcG9uZW50LnRzeCIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvbW9kdWxlcy90b3BiYXIvVG9wQmFyQ29tcG9uZW50LnRzeCIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvbW9kdWxlcy90eXBlL1R5cGVTZWxlY3RDb21wb25lbnQudHN4Iiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9zZXJ2aWNlcy9hY2NvdW50LnRzIiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9zZXJ2aWNlcy9jb250YWN0LnRzIiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9zZXJ2aWNlcy9vcmRlci50cyIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvc2VydmljZXMvcHJvZHVjdC50cyIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvc2VydmljZXMvcHJvZHVjdGdyb3VwLnRzIiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9zZXJ2aWNlcy9zaG9wLnRzIiwid2VicGFjazovLy8uL3Zhci9wbHVnaW5zL1N5c3RlbS9QU0MvSW52b2ljZS9XZWJwYWNrL3NyYy9zZXJ2aWNlcy90b2tlbi50cyIsIndlYnBhY2s6Ly8vLi92YXIvcGx1Z2lucy9TeXN0ZW0vUFNDL0ludm9pY2UvV2VicGFjay9zcmMvc3RhdGUvb3JkZXIudHMiXSwibmFtZXMiOlsiZXhwb3J0cyIsIl9fZXNNb2R1bGUiLCJyZXF1aXJlIiwiYXBwXzEiLCJhcHAiLCJBcHAiLCJqd3RfdG9rZW4iLCJpbml0IiwicnVuIiwidG9rZW5fMSIsInRzeXJpbmdlXzEiLCJSZWFjdCIsIlJlYWN0RE9NIiwiQmFzZUNvbXBvbmVudF8xIiwiand0IiwidG9rZW4iLCJjb250YWluZXIiLCJyZXNvbHZlIiwiVG9rZW4iLCJjdXJyZW50VG9rZW4iLCJwcm90b3R5cGUiLCJidWlsZFVpIiwicm9vdCIsImNyZWF0ZVJvb3QiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwicmVuZGVyIiwiY3JlYXRlRWxlbWVudCIsIkFjY291bnQiLCJ1dWlkXzEiLCJpZCIsInV1aWQiLCJ2NCIsImFwcGVuZGl4IiwidGl0bGUiLCJBZGRyZXNzIiwiZmlyc3RuYW1lIiwibGFzdG5hbWUiLCJDb250YWN0IiwidXNlcm5hbWUiLCJwYXNzd29yZCIsIlByb2R1Y3QiLCJQcm9kdWN0R3JvdXAiLCJsYWJlbCIsIm9wdGlvbnMiLCJTaG9wIiwibmFtZSIsIl9fYXdhaXRlciIsInRoaXNBcmciLCJfYXJndW1lbnRzIiwiUCIsImdlbmVyYXRvciIsImFkb3B0IiwidmFsdWUiLCJQcm9taXNlIiwicmVqZWN0IiwiZnVsZmlsbGVkIiwic3RlcCIsIm5leHQiLCJlIiwicmVqZWN0ZWQiLCJyZXN1bHQiLCJkb25lIiwidGhlbiIsImFwcGx5IiwiX19nZW5lcmF0b3IiLCJib2R5IiwiXyIsInNlbnQiLCJ0IiwidHJ5cyIsIm9wcyIsImYiLCJ5IiwiZyIsInZlcmIiLCJTeW1ib2wiLCJpdGVyYXRvciIsIm4iLCJ2Iiwib3AiLCJUeXBlRXJyb3IiLCJjYWxsIiwicG9wIiwibGVuZ3RoIiwicHVzaCIsImpzeF9ydW50aW1lXzEiLCJyZWFjdF9zZWxlY3RfYXN5bmNfcGFnaW5hdGVfMSIsImFjY291bnRfMSIsIm9yZGVyXzEiLCJyZWFjdF8xIiwiUHJvcFR5cGVzIiwic2hvcF8xIiwiQWNjb3VudFNlbGVjdENvbXBvbmVudCIsInByb3BzIiwiX2EiLCJ1c2VTdGF0ZSIsInNob3BVdWlkIiwic2V0U2hvcFV1aWQiLCJ1c2VFZmZlY3QiLCJzaG9wIiwib3JkZXIiLCJPcmRlciIsImxvYWRPcHRpb25zIiwic2VhcmNoUXVlcnkiLCJsb2FkZWRPcHRpb25zIiwiYWNjb3VudF9zZXJ2aWNlIiwiZGF0YSIsIkFjY291bnRTZXJ2aWNlIiwiZ2V0QWNjb3VudHMiLCJoYXNNb3JlIiwib25DaGFuZ2UiLCJvcHRpb24iLCJhY2NvdW50IiwianN4IiwiY2hpbGRyZW4iLCJBc3luY1BhZ2luYXRlIiwiZGVmYXVsdE9wdGlvbnMiLCJnZXRPcHRpb25WYWx1ZSIsImdldE9wdGlvbkxhYmVsIiwiaXNTZWFyY2hhYmxlIiwiSlNPTiIsInN0cmluZ2lmeSIsInByb3BUeXBlcyIsImluc3RhbmNlT2YiLCJDb250YWN0Q29tcG9uZW50XzEiLCJUb3BCYXJDb21wb25lbnRfMSIsIlBvc2l0aW9uc0NvbXBvbmVudF8xIiwiQmFzZUNvbXBvbmVudCIsInNldFNob3AiLCJqc3hzIiwiY2hhbmdlIiwiY29udGFjdF8xIiwiYWRkcmVzc18xIiwiY29udGFjdF8yIiwiQ29udGFjdEFkZHJlc3NTZWxlY3QiLCJjb250YWN0VXVpZCIsInNldENvbnRhY3RVdWlkIiwiY29udGFjdCIsImNvbnRhY3Rfc2VydmljZSIsIkNvbnRhY3RTZXJ2aWNlIiwiZ2V0Q29udGFjdEFkZHJlc3MiLCJ0eXBlIiwibnVtYmVyIiwiZnVuYyIsIl9fYXNzaWduIiwiT2JqZWN0IiwiYXNzaWduIiwicyIsImkiLCJhcmd1bWVudHMiLCJwIiwiaGFzT3duUHJvcGVydHkiLCJDb250YWN0U2VsZWN0XzEiLCJDb250YWN0QWRkcmVzc1NlbGVjdF8xIiwiQWNjb3VudFNlbGVjdENvbXBvbmVudF8xIiwiQ29udGFjdENvbXBvbmVudCIsIm9yZGVyU3RhdGUiLCJzZXRDb250YWN0IiwiX2IiLCJjb250YWN0QWRkcmVzc0ludm9pY2UiLCJzZXRDb250YWN0QWRkcmVzc0ludm9pY2UiLCJfYyIsImNvbnRhY3RBZGRyZXNzRGVsaXZlcnkiLCJzZXRDb250YWN0QWRkcmVzc0RlbGl2ZXJ5IiwiX2QiLCJjb250YWN0QWRkcmVzc1NlbmRlciIsInNldENvbnRhY3RBZGRyZXNzU2VuZGVyIiwiZGVsaXZlcnlBZGRyZXNzIiwiaW52b2ljZUFkZHJlc3MiLCJzZW5kZXJBZGRyZXNzIiwiY2xhc3NOYW1lIiwiQ29udGFjdFNlbGVjdCIsInBhZ2UiLCJjb250YWN0X2FwaSIsImNvbnRhY3RzIiwiZ2V0Q29udGFjdHMiLCJhZGRpdGlvbmFsIiwiQnV0dG9uXzEiLCJNb2RhbF8xIiwicHJvZHVjdF8xIiwicHJvZHVjdEdyb3VwXzEiLCJQcm9kdWN0R3JvdXBTZWxlY3RfMSIsIlByb2R1Y3RTZWxlY3RfMSIsIlByb2R1Y3RGb3JtXzEiLCJBZGRQb3NpdGlvbkNvbXBvbmVudCIsInNob3ciLCJzZXRTaG93IiwiaGFuZGxlQ2xvc2UiLCJoYW5kbGVTaG93IiwicHJvZHVjdCIsInNldFByb2R1Y3QiLCJwcm9kdWN0R3JvdXAiLCJzZXRQcm9kdWN0R3JvdXAiLCJ2YXJpYW50Iiwib25DbGljayIsImZ1bGxzY3JlZW4iLCJvbkhpZGUiLCJIZWFkZXIiLCJjbG9zZUJ1dHRvbiIsIlRpdGxlIiwiQm9keSIsIkFkZFBvc2l0aW9uQ29tcG9uZW50XzEiLCJQb3NpdGlvbnNDb21wb25lbnQiLCJGcmFnbWVudCIsInZhbGlkYXRvcl9hanY2XzEiLCJjb3JlXzEiLCJwcm9kdWN0XzIiLCJQcm9kdWN0Rm9ybSIsInNjaGVtYSIsInNldFNjaGVtYSIsImxvYWRTY2hlbWEiLCJwcm9kdWN0X2FwaSIsIlByb2R1Y3RTZXJ2aWNlIiwiZ2V0QWRkUHJvZHVjdFNjaGVtYSIsImxvZyIsImNvbnNvbGUiLCJiaW5kIiwidmFsaWRhdG9yIiwib25TdWJtaXQiLCJvbkVycm9yIiwicHJvZHVjdGdyb3VwXzEiLCJhc3luY18xIiwiUHJvZHVjdEdyb3VwU2VsZWN0IiwicHJvZHVjdGdyb3VwX2FwaSIsIlByb2R1Y3RHcm91cFNlcnZpY2UiLCJnZXRQcm9kdWN0R3JvdXBzIiwiZGVib3VuY2UiLCJpbm5lciIsIm1zIiwidGltZXIiLCJyZXNvbHZlcyIsImFyZ3MiLCJfaSIsImNsZWFyVGltZW91dCIsInNldFRpbWVvdXQiLCJmb3JFYWNoIiwiciIsIlByb2R1Y3RTZWxlY3QiLCJnZXRQcm9kdWN0c0J5VGVybSIsImdldFByb2R1Y3RzIiwiU2hvcFNlbGVjdENvbXBvbmVudCIsInNob3Bfc2VydmljZSIsIlNob3BTZXJ2aWNlIiwiZ2V0U2hvcHMiLCJvcmRlcl8yIiwiVHlwZVNlbGVjdENvbXBvbmVudF8xIiwiU2hvcFNlbGVjdENvbXBvbmVudF8xIiwiVG9wQmFyQ29tcG9uZW50Iiwib3JkZXJTZXJ2aWNlIiwiT3JkZXJTZXJ2aWNlIiwiaGFuZGxlU2F2ZSIsInByZXZlbnREZWZhdWx0Iiwic2F2ZU9yZGVyIiwicmVhY3Rfc2VsZWN0XzEiLCJUeXBlU2VsZWN0Q29tcG9uZW50Iiwic2V0VHlwZSIsIl9fZGVjb3JhdGUiLCJkZWNvcmF0b3JzIiwidGFyZ2V0Iiwia2V5IiwiZGVzYyIsImMiLCJnZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IiLCJkIiwiUmVmbGVjdCIsImRlY29yYXRlIiwiZGVmaW5lUHJvcGVydHkiLCJfX21ldGFkYXRhIiwiayIsIm1ldGFkYXRhIiwiYXhpb3NfMSIsImdldCIsImhlYWRlcnMiLCJyZXNwb25zZSIsIm1hcCIsIml0ZW0iLCJzaW5nbGV0b24iLCJhdXRvSW5qZWN0YWJsZSIsImFkZHJlc3MiLCJwb3N0IiwidGVybSIsInBnIiwic3ViIiwic3BnIiwiX3RoaXMiLCJzZXRJbnRlcnZhbCIsInJlZnJlc2hUb2tlbiJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7OztBQUFhOztBQUNiQSxPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7O0FBQ0FDLG1CQUFPLENBQUMsMkdBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxtRkFBRCxDQUFQOztBQUNBLElBQUlDLEtBQUssR0FBR0QsbUJBQU8sQ0FBQywrRUFBRCxDQUFuQjs7QUFDQSxJQUFJRSxHQUFHLEdBQUcsSUFBSUQsS0FBSyxDQUFDRSxHQUFWLENBQWNDLFNBQWQsQ0FBVjtBQUNBRixHQUFHLENBQUNHLElBQUo7QUFDQUgsR0FBRyxDQUFDSSxHQUFKLEc7Ozs7Ozs7Ozs7OztBQ1BhOztBQUNiUixPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7QUFDQUQsT0FBTyxDQUFDSyxHQUFSLEdBQWMsS0FBSyxDQUFuQjs7QUFDQSxJQUFJSSxPQUFPLEdBQUdQLG1CQUFPLENBQUMseUZBQUQsQ0FBckI7O0FBQ0EsSUFBSVEsVUFBVSxHQUFHUixtQkFBTyxDQUFDLG1HQUFELENBQXhCOztBQUNBLElBQUlTLEtBQUssR0FBR1QsbUJBQU8sQ0FBQyxtRkFBRCxDQUFuQjs7QUFDQSxJQUFJVSxRQUFRLEdBQUdWLG1CQUFPLENBQUMsbUdBQUQsQ0FBdEI7O0FBQ0EsSUFBSVcsZUFBZSxHQUFHWCxtQkFBTyxDQUFDLGtIQUFELENBQTdCOztBQUNBLElBQUlHLEdBQUc7QUFBRztBQUFlLFlBQVk7QUFDakMsV0FBU0EsR0FBVCxDQUFhUyxHQUFiLEVBQWtCO0FBQ2QsUUFBSUMsS0FBSyxHQUFHTCxVQUFVLENBQUNNLFNBQVgsQ0FBcUJDLE9BQXJCLENBQTZCUixPQUFPLENBQUNTLEtBQXJDLENBQVo7QUFDQUgsU0FBSyxDQUFDSSxZQUFOLEdBQXFCTCxHQUFyQjtBQUNBQyxTQUFLLENBQUNSLElBQU47QUFDSDs7QUFDREYsS0FBRyxDQUFDZSxTQUFKLENBQWNiLElBQWQsR0FBcUIsWUFBWTtBQUM3QixTQUFLYyxPQUFMO0FBQ0gsR0FGRDs7QUFHQWhCLEtBQUcsQ0FBQ2UsU0FBSixDQUFjQyxPQUFkLEdBQXdCLFlBQVk7QUFDaEMsUUFBSUMsSUFBSSxHQUFHVixRQUFRLENBQUNXLFVBQVQsQ0FBb0JDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixNQUF4QixDQUFwQixDQUFYO0FBQ0FILFFBQUksQ0FBQ0ksTUFBTCxDQUFZZixLQUFLLENBQUNnQixhQUFOLENBQW9CZCxlQUFlLENBQUMsU0FBRCxDQUFuQyxDQUFaO0FBQ0gsR0FIRDs7QUFJQVIsS0FBRyxDQUFDZSxTQUFKLENBQWNaLEdBQWQsR0FBb0IsWUFBWSxDQUMvQixDQUREOztBQUVBLFNBQU9ILEdBQVA7QUFDSCxDQWhCd0IsRUFBekI7O0FBaUJBTCxPQUFPLENBQUNLLEdBQVIsR0FBY0EsR0FBZCxDOzs7Ozs7Ozs7Ozs7QUN6QmE7O0FBQ2JMLE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjtBQUNBRCxPQUFPLENBQUM0QixPQUFSLEdBQWtCLEtBQUssQ0FBdkI7O0FBQ0EsSUFBSUMsTUFBTSxHQUFHM0IsbUJBQU8sQ0FBQyxrR0FBRCxDQUFwQjs7QUFDQSxJQUFJMEIsT0FBTztBQUFHO0FBQWUsWUFBWTtBQUNyQyxXQUFTQSxPQUFULEdBQW1CO0FBQ2YsU0FBS0UsRUFBTCxHQUFVLENBQVY7QUFDQSxTQUFLQyxJQUFMLEdBQVksQ0FBQyxHQUFHRixNQUFNLENBQUNHLEVBQVgsR0FBWjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxTQUFLQyxLQUFMLEdBQWEsRUFBYjtBQUNIOztBQUNELFNBQU9OLE9BQVA7QUFDSCxDQVI0QixFQUE3Qjs7QUFTQTVCLE9BQU8sQ0FBQzRCLE9BQVIsR0FBa0JBLE9BQWxCLEM7Ozs7Ozs7Ozs7OztBQ2JhOztBQUNiNUIsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELE9BQU8sQ0FBQ21DLE9BQVIsR0FBa0IsS0FBSyxDQUF2Qjs7QUFDQSxJQUFJTixNQUFNLEdBQUczQixtQkFBTyxDQUFDLGtHQUFELENBQXBCOztBQUNBLElBQUlpQyxPQUFPO0FBQUc7QUFBZSxZQUFZO0FBQ3JDLFdBQVNBLE9BQVQsR0FBbUI7QUFDZixTQUFLTCxFQUFMLEdBQVUsQ0FBVjtBQUNBLFNBQUtDLElBQUwsR0FBWSxDQUFDLEdBQUdGLE1BQU0sQ0FBQ0csRUFBWCxHQUFaO0FBQ0EsU0FBS0ksU0FBTCxHQUFpQixFQUFqQjtBQUNBLFNBQUtDLFFBQUwsR0FBZ0IsRUFBaEI7QUFDSDs7QUFDRCxTQUFPRixPQUFQO0FBQ0gsQ0FSNEIsRUFBN0I7O0FBU0FuQyxPQUFPLENBQUNtQyxPQUFSLEdBQWtCQSxPQUFsQixDOzs7Ozs7Ozs7Ozs7QUNiYTs7QUFDYm5DLE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjtBQUNBRCxPQUFPLENBQUNzQyxPQUFSLEdBQWtCLEtBQUssQ0FBdkI7O0FBQ0EsSUFBSVQsTUFBTSxHQUFHM0IsbUJBQU8sQ0FBQyxrR0FBRCxDQUFwQjs7QUFDQSxJQUFJb0MsT0FBTztBQUFHO0FBQWUsWUFBWTtBQUNyQyxXQUFTQSxPQUFULEdBQW1CO0FBQ2YsU0FBS1IsRUFBTCxHQUFVLENBQVY7QUFDQSxTQUFLQyxJQUFMLEdBQVksQ0FBQyxHQUFHRixNQUFNLENBQUNHLEVBQVgsR0FBWjtBQUNBLFNBQUtPLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxTQUFLQyxRQUFMLEdBQWdCLEVBQWhCO0FBQ0g7O0FBQ0QsU0FBT0YsT0FBUDtBQUNILENBUjRCLEVBQTdCOztBQVNBdEMsT0FBTyxDQUFDc0MsT0FBUixHQUFrQkEsT0FBbEIsQzs7Ozs7Ozs7Ozs7O0FDYmE7O0FBQ2J0QyxPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7QUFDQUQsT0FBTyxDQUFDeUMsT0FBUixHQUFrQixLQUFLLENBQXZCOztBQUNBLElBQUlBLE9BQU87QUFBRztBQUFlLFlBQVk7QUFDckMsV0FBU0EsT0FBVCxHQUFtQjtBQUNmLFNBQUtYLEVBQUwsR0FBVSxDQUFWO0FBQ0EsU0FBS0ksS0FBTCxHQUFhLEVBQWI7QUFDQSxTQUFLSCxJQUFMLEdBQVksRUFBWjtBQUNIOztBQUNELFNBQU9VLE9BQVA7QUFDSCxDQVA0QixFQUE3Qjs7QUFRQXpDLE9BQU8sQ0FBQ3lDLE9BQVIsR0FBa0JBLE9BQWxCLEM7Ozs7Ozs7Ozs7OztBQ1hhOztBQUNiekMsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELE9BQU8sQ0FBQzBDLFlBQVIsR0FBdUIsS0FBSyxDQUE1Qjs7QUFDQSxJQUFJQSxZQUFZO0FBQUc7QUFBZSxZQUFZO0FBQzFDLFdBQVNBLFlBQVQsR0FBd0I7QUFDcEIsU0FBS1osRUFBTCxHQUFVLENBQVY7QUFDQSxTQUFLYSxLQUFMLEdBQWEsRUFBYjtBQUNBLFNBQUtaLElBQUwsR0FBWSxFQUFaO0FBQ0EsU0FBS2EsT0FBTCxHQUFlLEVBQWY7QUFDSDs7QUFDRCxTQUFPRixZQUFQO0FBQ0gsQ0FSaUMsRUFBbEM7O0FBU0ExQyxPQUFPLENBQUMwQyxZQUFSLEdBQXVCQSxZQUF2QixDOzs7Ozs7Ozs7Ozs7QUNaYTs7OztBQUNiMUMsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELE9BQU8sQ0FBQzZDLElBQVIsR0FBZSxLQUFLLENBQXBCOztBQUNBLElBQUlBLElBQUk7QUFBRztBQUFlLFlBQVk7QUFDbEMsV0FBU0EsSUFBVCxHQUFnQjtBQUNaLFNBQUtmLEVBQUwsR0FBVSxDQUFWO0FBQ0EsU0FBS2dCLElBQUwsR0FBWSxFQUFaO0FBQ0EsU0FBS2YsSUFBTCxHQUFZLEVBQVo7QUFDSDs7QUFDRCxTQUFPYyxJQUFQO0FBQ0gsQ0FQeUIsRUFBMUI7O0FBUUE3QyxPQUFPLENBQUM2QyxJQUFSLEdBQWVBLElBQWYsQzs7Ozs7Ozs7Ozs7O0FDWGE7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNiLElBQUlFLFNBQVMsR0FBSSxRQUFRLEtBQUtBLFNBQWQsSUFBNEIsVUFBVUMsT0FBVixFQUFtQkMsVUFBbkIsRUFBK0JDLENBQS9CLEVBQWtDQyxTQUFsQyxFQUE2QztBQUNyRixXQUFTQyxLQUFULENBQWVDLEtBQWYsRUFBc0I7QUFBRSxXQUFPQSxLQUFLLFlBQVlILENBQWpCLEdBQXFCRyxLQUFyQixHQUE2QixJQUFJSCxDQUFKLENBQU0sVUFBVWpDLE9BQVYsRUFBbUI7QUFBRUEsYUFBTyxDQUFDb0MsS0FBRCxDQUFQO0FBQWlCLEtBQTVDLENBQXBDO0FBQW9GOztBQUM1RyxTQUFPLEtBQUtILENBQUMsS0FBS0EsQ0FBQyxHQUFHSSxPQUFULENBQU4sRUFBeUIsVUFBVXJDLE9BQVYsRUFBbUJzQyxNQUFuQixFQUEyQjtBQUN2RCxhQUFTQyxTQUFULENBQW1CSCxLQUFuQixFQUEwQjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUNPLElBQVYsQ0FBZUwsS0FBZixDQUFELENBQUo7QUFBOEIsT0FBcEMsQ0FBcUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDM0YsYUFBU0MsUUFBVCxDQUFrQlAsS0FBbEIsRUFBeUI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDLE9BQUQsQ0FBVCxDQUFtQkUsS0FBbkIsQ0FBRCxDQUFKO0FBQWtDLE9BQXhDLENBQXlDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzlGLGFBQVNGLElBQVQsQ0FBY0ksTUFBZCxFQUFzQjtBQUFFQSxZQUFNLENBQUNDLElBQVAsR0FBYzdDLE9BQU8sQ0FBQzRDLE1BQU0sQ0FBQ1IsS0FBUixDQUFyQixHQUFzQ0QsS0FBSyxDQUFDUyxNQUFNLENBQUNSLEtBQVIsQ0FBTCxDQUFvQlUsSUFBcEIsQ0FBeUJQLFNBQXpCLEVBQW9DSSxRQUFwQyxDQUF0QztBQUFzRjs7QUFDOUdILFFBQUksQ0FBQyxDQUFDTixTQUFTLEdBQUdBLFNBQVMsQ0FBQ2EsS0FBVixDQUFnQmhCLE9BQWhCLEVBQXlCQyxVQUFVLElBQUksRUFBdkMsQ0FBYixFQUF5RFMsSUFBekQsRUFBRCxDQUFKO0FBQ0gsR0FMTSxDQUFQO0FBTUgsQ0FSRDs7QUFTQSxJQUFJTyxXQUFXLEdBQUksUUFBUSxLQUFLQSxXQUFkLElBQThCLFVBQVVqQixPQUFWLEVBQW1Ca0IsSUFBbkIsRUFBeUI7QUFDckUsTUFBSUMsQ0FBQyxHQUFHO0FBQUV4QixTQUFLLEVBQUUsQ0FBVDtBQUFZeUIsUUFBSSxFQUFFLGdCQUFXO0FBQUUsVUFBSUMsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFPLENBQVgsRUFBYyxNQUFNQSxDQUFDLENBQUMsQ0FBRCxDQUFQO0FBQVksYUFBT0EsQ0FBQyxDQUFDLENBQUQsQ0FBUjtBQUFjLEtBQXZFO0FBQXlFQyxRQUFJLEVBQUUsRUFBL0U7QUFBbUZDLE9BQUcsRUFBRTtBQUF4RixHQUFSO0FBQUEsTUFBc0dDLENBQXRHO0FBQUEsTUFBeUdDLENBQXpHO0FBQUEsTUFBNEdKLENBQTVHO0FBQUEsTUFBK0dLLENBQS9HO0FBQ0EsU0FBT0EsQ0FBQyxHQUFHO0FBQUVoQixRQUFJLEVBQUVpQixJQUFJLENBQUMsQ0FBRCxDQUFaO0FBQWlCLGFBQVNBLElBQUksQ0FBQyxDQUFELENBQTlCO0FBQW1DLGNBQVVBLElBQUksQ0FBQyxDQUFEO0FBQWpELEdBQUosRUFBNEQsT0FBT0MsTUFBUCxLQUFrQixVQUFsQixLQUFpQ0YsQ0FBQyxDQUFDRSxNQUFNLENBQUNDLFFBQVIsQ0FBRCxHQUFxQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsR0FBakYsQ0FBNUQsRUFBZ0pILENBQXZKOztBQUNBLFdBQVNDLElBQVQsQ0FBY0csQ0FBZCxFQUFpQjtBQUFFLFdBQU8sVUFBVUMsQ0FBVixFQUFhO0FBQUUsYUFBT3RCLElBQUksQ0FBQyxDQUFDcUIsQ0FBRCxFQUFJQyxDQUFKLENBQUQsQ0FBWDtBQUFzQixLQUE1QztBQUErQzs7QUFDbEUsV0FBU3RCLElBQVQsQ0FBY3VCLEVBQWQsRUFBa0I7QUFDZCxRQUFJUixDQUFKLEVBQU8sTUFBTSxJQUFJUyxTQUFKLENBQWMsaUNBQWQsQ0FBTjs7QUFDUCxXQUFPZCxDQUFQO0FBQVUsVUFBSTtBQUNWLFlBQUlLLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsS0FBS0osQ0FBQyxHQUFHVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBUixHQUFZUCxDQUFDLENBQUMsUUFBRCxDQUFiLEdBQTBCTyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFQLENBQUMsQ0FBQyxPQUFELENBQUQsS0FBZSxDQUFDSixDQUFDLEdBQUdJLENBQUMsQ0FBQyxRQUFELENBQU4sS0FBcUJKLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLENBQXJCLEVBQWdDLENBQS9DLENBQVIsR0FBNERBLENBQUMsQ0FBQ2YsSUFBakcsQ0FBRCxJQUEyRyxDQUFDLENBQUNXLENBQUMsR0FBR0EsQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsRUFBVU8sRUFBRSxDQUFDLENBQUQsQ0FBWixDQUFMLEVBQXVCbEIsSUFBOUksRUFBb0osT0FBT08sQ0FBUDtBQUNwSixZQUFJSSxDQUFDLEdBQUcsQ0FBSixFQUFPSixDQUFYLEVBQWNXLEVBQUUsR0FBRyxDQUFDQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBVCxFQUFZWCxDQUFDLENBQUNoQixLQUFkLENBQUw7O0FBQ2QsZ0JBQVEyQixFQUFFLENBQUMsQ0FBRCxDQUFWO0FBQ0ksZUFBSyxDQUFMO0FBQVEsZUFBSyxDQUFMO0FBQVFYLGFBQUMsR0FBR1csRUFBSjtBQUFROztBQUN4QixlQUFLLENBQUw7QUFBUWIsYUFBQyxDQUFDeEIsS0FBRjtBQUFXLG1CQUFPO0FBQUVVLG1CQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFYO0FBQWdCbEIsa0JBQUksRUFBRTtBQUF0QixhQUFQOztBQUNuQixlQUFLLENBQUw7QUFBUUssYUFBQyxDQUFDeEIsS0FBRjtBQUFXOEIsYUFBQyxHQUFHTyxFQUFFLENBQUMsQ0FBRCxDQUFOO0FBQVdBLGNBQUUsR0FBRyxDQUFDLENBQUQsQ0FBTDtBQUFVOztBQUN4QyxlQUFLLENBQUw7QUFBUUEsY0FBRSxHQUFHYixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTixFQUFMOztBQUFrQmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjOztBQUN4QztBQUNJLGdCQUFJLEVBQUVkLENBQUMsR0FBR0YsQ0FBQyxDQUFDRyxJQUFOLEVBQVlELENBQUMsR0FBR0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWCxJQUFnQmYsQ0FBQyxDQUFDQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFaLENBQW5DLE1BQXVESixFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBaEYsQ0FBSixFQUF3RjtBQUFFYixlQUFDLEdBQUcsQ0FBSjtBQUFPO0FBQVc7O0FBQzVHLGdCQUFJYSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixLQUFnQixDQUFDWCxDQUFELElBQU9XLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBVCxJQUFnQlcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFoRCxDQUFKLEVBQTJEO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVXFDLEVBQUUsQ0FBQyxDQUFELENBQVo7QUFBaUI7QUFBUTs7QUFDdEYsZ0JBQUlBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWViLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQTlCLEVBQW1DO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7QUFBZ0JBLGVBQUMsR0FBR1csRUFBSjtBQUFRO0FBQVE7O0FBQ3JFLGdCQUFJWCxDQUFDLElBQUlGLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQXBCLEVBQXlCO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7O0FBQWdCRixlQUFDLENBQUNJLEdBQUYsQ0FBTWMsSUFBTixDQUFXTCxFQUFYOztBQUFnQjtBQUFROztBQUNuRSxnQkFBSVgsQ0FBQyxDQUFDLENBQUQsQ0FBTCxFQUFVRixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTjs7QUFDVmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjO0FBWHRCOztBQWFBSCxVQUFFLEdBQUdkLElBQUksQ0FBQ2dCLElBQUwsQ0FBVWxDLE9BQVYsRUFBbUJtQixDQUFuQixDQUFMO0FBQ0gsT0FqQlMsQ0FpQlIsT0FBT1IsQ0FBUCxFQUFVO0FBQUVxQixVQUFFLEdBQUcsQ0FBQyxDQUFELEVBQUlyQixDQUFKLENBQUw7QUFBYWMsU0FBQyxHQUFHLENBQUo7QUFBUSxPQWpCekIsU0FpQmtDO0FBQUVELFNBQUMsR0FBR0gsQ0FBQyxHQUFHLENBQVI7QUFBWTtBQWpCMUQ7O0FBa0JBLFFBQUlXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFaLEVBQWUsTUFBTUEsRUFBRSxDQUFDLENBQUQsQ0FBUjtBQUFhLFdBQU87QUFBRTNCLFdBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUUEsRUFBRSxDQUFDLENBQUQsQ0FBVixHQUFnQixLQUFLLENBQTlCO0FBQWlDbEIsVUFBSSxFQUFFO0FBQXZDLEtBQVA7QUFDL0I7QUFDSixDQTFCRDs7QUEyQkE5RCxPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7O0FBQ0EsSUFBSXFGLGFBQWEsR0FBR3BGLG1CQUFPLENBQUMscUdBQUQsQ0FBM0I7O0FBQ0EsSUFBSXFGLDZCQUE2QixHQUFHckYsbUJBQU8sQ0FBQyxrSUFBRCxDQUEzQzs7QUFDQSxJQUFJc0YsU0FBUyxHQUFHdEYsbUJBQU8sQ0FBQyxnR0FBRCxDQUF2Qjs7QUFDQSxJQUFJUSxVQUFVLEdBQUdSLG1CQUFPLENBQUMsbUdBQUQsQ0FBeEI7O0FBQ0EsSUFBSXVGLE9BQU8sR0FBR3ZGLG1CQUFPLENBQUMsc0ZBQUQsQ0FBckI7O0FBQ0EsSUFBSXdGLE9BQU8sR0FBR3hGLG1CQUFPLENBQUMsbUZBQUQsQ0FBckI7O0FBQ0EsSUFBSXlGLFNBQVMsR0FBR3pGLG1CQUFPLENBQUMsNkZBQUQsQ0FBdkI7O0FBQ0EsSUFBSTBGLE1BQU0sR0FBRzFGLG1CQUFPLENBQUMsb0ZBQUQsQ0FBcEI7O0FBQ0EsSUFBSTJGLHNCQUFzQixHQUFHLFNBQXpCQSxzQkFBeUIsQ0FBVUMsS0FBVixFQUFpQjtBQUMxQyxNQUFJQyxFQUFFLEdBQUcsQ0FBQyxHQUFHTCxPQUFPLENBQUNNLFFBQVosRUFBc0IsSUFBdEIsQ0FBVDtBQUFBLE1BQXNDQyxRQUFRLEdBQUdGLEVBQUUsQ0FBQyxDQUFELENBQW5EO0FBQUEsTUFBd0RHLFdBQVcsR0FBR0gsRUFBRSxDQUFDLENBQUQsQ0FBeEU7O0FBQ0EsR0FBQyxHQUFHTCxPQUFPLENBQUNTLFNBQVosRUFBdUIsWUFBWTtBQUMvQkQsZUFBVyxDQUFDSixLQUFLLENBQUNNLElBQU4sQ0FBV3JFLElBQVosQ0FBWDtBQUNILEdBRkQsRUFFRyxDQUFDK0QsS0FBSyxDQUFDTSxJQUFOLENBQVdyRSxJQUFaLENBRkg7QUFHQSxNQUFJc0UsS0FBSyxHQUFHM0YsVUFBVSxDQUFDTSxTQUFYLENBQXFCQyxPQUFyQixDQUE2QndFLE9BQU8sQ0FBQ2EsS0FBckMsQ0FBWjs7QUFDQSxNQUFJQyxXQUFXLEdBQUcsU0FBZEEsV0FBYyxDQUFVQyxXQUFWLEVBQXVCQyxhQUF2QixFQUFzQztBQUFFLFdBQU8xRCxTQUFTLENBQUMsS0FBSyxDQUFOLEVBQVMsS0FBSyxDQUFkLEVBQWlCLEtBQUssQ0FBdEIsRUFBeUIsWUFBWTtBQUMzRyxVQUFJMkQsZUFBSixFQUFxQkMsSUFBckI7QUFDQSxhQUFPMUMsV0FBVyxDQUFDLElBQUQsRUFBTyxVQUFVOEIsRUFBVixFQUFjO0FBQ25DLGdCQUFRQSxFQUFFLENBQUNwRCxLQUFYO0FBQ0ksZUFBSyxDQUFMO0FBQ0krRCwyQkFBZSxHQUFHLElBQUlsQixTQUFTLENBQUNvQixjQUFkLEVBQWxCO0FBQ0EsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBY0YsZUFBZSxDQUFDRyxXQUFoQixDQUE0QmYsS0FBSyxDQUFDTSxJQUFsQyxDQUFkLENBQVA7O0FBQ0osZUFBSyxDQUFMO0FBQ0lPLGdCQUFJLEdBQUdaLEVBQUUsQ0FBQzNCLElBQUgsRUFBUDtBQUNBLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWU7QUFDZHhCLHFCQUFPLEVBQUUrRCxJQURLO0FBRWRHLHFCQUFPLEVBQUU7QUFGSyxhQUFmLENBQVA7QUFOUjtBQVdILE9BWmlCLENBQWxCO0FBYUgsS0FmeUUsQ0FBaEI7QUFlckQsR0FmTDs7QUFnQkEsTUFBSUMsUUFBUSxHQUFHLFNBQVhBLFFBQVcsQ0FBVUMsTUFBVixFQUFrQjtBQUM3QlgsU0FBSyxDQUFDWSxPQUFOLEdBQWdCRCxNQUFoQjtBQUNILEdBRkQ7O0FBR0EsU0FBUSxDQUFDLEdBQUcxQixhQUFhLENBQUM0QixHQUFsQixFQUF1QixLQUF2QixFQUE4QjtBQUFFQyxZQUFRLEVBQUUsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIzQiw2QkFBNkIsQ0FBQzZCLGFBQXJELEVBQW9FO0FBQUVDLG9CQUFjLEVBQUUsSUFBbEI7QUFBd0JkLGlCQUFXLEVBQUVBLFdBQXJDO0FBQWtEZSxvQkFBYyxFQUFFLHdCQUFVTixNQUFWLEVBQWtCO0FBQUUsZUFBT0EsTUFBTSxDQUFDakYsSUFBZDtBQUFxQixPQUEzRztBQUE2R3dGLG9CQUFjLEVBQUUsd0JBQVVQLE1BQVYsRUFBa0I7QUFBRSxlQUFPQSxNQUFNLENBQUM5RSxLQUFkO0FBQXNCLE9BQXZLO0FBQXlLNkUsY0FBUSxFQUFFQSxRQUFuTDtBQUE2TFMsa0JBQVksRUFBRTtBQUEzTSxLQUFwRSxFQUF1UkMsSUFBSSxDQUFDQyxTQUFMLENBQWV6QixRQUFmLENBQXZSO0FBQVosR0FBOUIsQ0FBUjtBQUNILENBMUJEOztBQTJCQUosc0JBQXNCLENBQUM4QixTQUF2QixHQUFtQztBQUMvQnZCLE1BQUksRUFBRVQsU0FBUyxDQUFDaUMsVUFBVixDQUFxQmhDLE1BQU0sQ0FBQy9DLElBQTVCO0FBRHlCLENBQW5DO0FBR0E3QyxPQUFPLENBQUMsU0FBRCxDQUFQLEdBQXFCNkYsc0JBQXJCLEM7Ozs7Ozs7Ozs7OztBQzVFYTs7QUFDYjdGLE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjs7QUFDQSxJQUFJcUYsYUFBYSxHQUFHcEYsbUJBQU8sQ0FBQyxxR0FBRCxDQUEzQjs7QUFDQSxJQUFJMEYsTUFBTSxHQUFHMUYsbUJBQU8sQ0FBQyxvRkFBRCxDQUFwQjs7QUFDQSxJQUFJMkgsa0JBQWtCLEdBQUczSCxtQkFBTyxDQUFDLHNIQUFELENBQWhDOztBQUNBLElBQUk0SCxpQkFBaUIsR0FBRzVILG1CQUFPLENBQUMsa0hBQUQsQ0FBL0I7O0FBQ0EsSUFBSTZILG9CQUFvQixHQUFHN0gsbUJBQU8sQ0FBQyw4SEFBRCxDQUFsQzs7QUFDQSxJQUFJd0YsT0FBTyxHQUFHeEYsbUJBQU8sQ0FBQyxtRkFBRCxDQUFyQjs7QUFDQSxJQUFJOEgsYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixDQUFVbEMsS0FBVixFQUFpQjtBQUNqQyxNQUFJQyxFQUFFLEdBQUcsQ0FBQyxHQUFHTCxPQUFPLENBQUNNLFFBQVosRUFBc0IsSUFBSUosTUFBTSxDQUFDL0MsSUFBWCxFQUF0QixDQUFUO0FBQUEsTUFBbUR1RCxJQUFJLEdBQUdMLEVBQUUsQ0FBQyxDQUFELENBQTVEO0FBQUEsTUFBaUVrQyxPQUFPLEdBQUdsQyxFQUFFLENBQUMsQ0FBRCxDQUE3RTs7QUFDQSxTQUFRLENBQUMsR0FBR1QsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0I7QUFBRWYsWUFBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIsS0FBdkIsRUFBOEI7QUFBRUMsY0FBUSxFQUFFLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCWSxpQkFBaUIsQ0FBQyxTQUFELENBQXhDLEVBQXFEO0FBQUUxQixZQUFJLEVBQUVBLElBQVI7QUFBYytCLGNBQU0sRUFBRUY7QUFBdEIsT0FBckQ7QUFBWixLQUE5QixDQUFELEVBQXFJLENBQUMsR0FBRzNDLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCLEtBQXZCLEVBQThCO0FBQUVDLGNBQVEsRUFBRWYsSUFBSSxDQUFDdEUsRUFBTCxJQUFXLENBQVgsSUFBZ0IsQ0FBQyxHQUFHd0QsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJXLGtCQUFrQixDQUFDLFNBQUQsQ0FBekMsRUFBc0Q7QUFBRXpCLFlBQUksRUFBRUE7QUFBUixPQUF0RDtBQUE1QixLQUE5QixDQUFySSxFQUF5USxDQUFDLEdBQUdkLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCLEtBQXZCLEVBQThCO0FBQUVDLGNBQVEsRUFBRWYsSUFBSSxDQUFDdEUsRUFBTCxJQUFXLENBQVgsSUFBZ0IsQ0FBQyxHQUFHd0QsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJhLG9CQUFvQixDQUFDLFNBQUQsQ0FBM0MsRUFBd0Q7QUFBRTNCLFlBQUksRUFBRUE7QUFBUixPQUF4RDtBQUE1QixLQUE5QixDQUF6UTtBQUFaLEdBQS9CLENBQVI7QUFDSCxDQUhEOztBQUlBcEcsT0FBTyxDQUFDLFNBQUQsQ0FBUCxHQUFxQmdJLGFBQXJCLEM7Ozs7Ozs7Ozs7OztBQ1phOzs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDYixJQUFJakYsU0FBUyxHQUFJLFFBQVEsS0FBS0EsU0FBZCxJQUE0QixVQUFVQyxPQUFWLEVBQW1CQyxVQUFuQixFQUErQkMsQ0FBL0IsRUFBa0NDLFNBQWxDLEVBQTZDO0FBQ3JGLFdBQVNDLEtBQVQsQ0FBZUMsS0FBZixFQUFzQjtBQUFFLFdBQU9BLEtBQUssWUFBWUgsQ0FBakIsR0FBcUJHLEtBQXJCLEdBQTZCLElBQUlILENBQUosQ0FBTSxVQUFVakMsT0FBVixFQUFtQjtBQUFFQSxhQUFPLENBQUNvQyxLQUFELENBQVA7QUFBaUIsS0FBNUMsQ0FBcEM7QUFBb0Y7O0FBQzVHLFNBQU8sS0FBS0gsQ0FBQyxLQUFLQSxDQUFDLEdBQUdJLE9BQVQsQ0FBTixFQUF5QixVQUFVckMsT0FBVixFQUFtQnNDLE1BQW5CLEVBQTJCO0FBQ3ZELGFBQVNDLFNBQVQsQ0FBbUJILEtBQW5CLEVBQTBCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQ08sSUFBVixDQUFlTCxLQUFmLENBQUQsQ0FBSjtBQUE4QixPQUFwQyxDQUFxQyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUMzRixhQUFTQyxRQUFULENBQWtCUCxLQUFsQixFQUF5QjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUMsT0FBRCxDQUFULENBQW1CRSxLQUFuQixDQUFELENBQUo7QUFBa0MsT0FBeEMsQ0FBeUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDOUYsYUFBU0YsSUFBVCxDQUFjSSxNQUFkLEVBQXNCO0FBQUVBLFlBQU0sQ0FBQ0MsSUFBUCxHQUFjN0MsT0FBTyxDQUFDNEMsTUFBTSxDQUFDUixLQUFSLENBQXJCLEdBQXNDRCxLQUFLLENBQUNTLE1BQU0sQ0FBQ1IsS0FBUixDQUFMLENBQW9CVSxJQUFwQixDQUF5QlAsU0FBekIsRUFBb0NJLFFBQXBDLENBQXRDO0FBQXNGOztBQUM5R0gsUUFBSSxDQUFDLENBQUNOLFNBQVMsR0FBR0EsU0FBUyxDQUFDYSxLQUFWLENBQWdCaEIsT0FBaEIsRUFBeUJDLFVBQVUsSUFBSSxFQUF2QyxDQUFiLEVBQXlEUyxJQUF6RCxFQUFELENBQUo7QUFDSCxHQUxNLENBQVA7QUFNSCxDQVJEOztBQVNBLElBQUlPLFdBQVcsR0FBSSxRQUFRLEtBQUtBLFdBQWQsSUFBOEIsVUFBVWpCLE9BQVYsRUFBbUJrQixJQUFuQixFQUF5QjtBQUNyRSxNQUFJQyxDQUFDLEdBQUc7QUFBRXhCLFNBQUssRUFBRSxDQUFUO0FBQVl5QixRQUFJLEVBQUUsZ0JBQVc7QUFBRSxVQUFJQyxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQU8sQ0FBWCxFQUFjLE1BQU1BLENBQUMsQ0FBQyxDQUFELENBQVA7QUFBWSxhQUFPQSxDQUFDLENBQUMsQ0FBRCxDQUFSO0FBQWMsS0FBdkU7QUFBeUVDLFFBQUksRUFBRSxFQUEvRTtBQUFtRkMsT0FBRyxFQUFFO0FBQXhGLEdBQVI7QUFBQSxNQUFzR0MsQ0FBdEc7QUFBQSxNQUF5R0MsQ0FBekc7QUFBQSxNQUE0R0osQ0FBNUc7QUFBQSxNQUErR0ssQ0FBL0c7QUFDQSxTQUFPQSxDQUFDLEdBQUc7QUFBRWhCLFFBQUksRUFBRWlCLElBQUksQ0FBQyxDQUFELENBQVo7QUFBaUIsYUFBU0EsSUFBSSxDQUFDLENBQUQsQ0FBOUI7QUFBbUMsY0FBVUEsSUFBSSxDQUFDLENBQUQ7QUFBakQsR0FBSixFQUE0RCxPQUFPQyxNQUFQLEtBQWtCLFVBQWxCLEtBQWlDRixDQUFDLENBQUNFLE1BQU0sQ0FBQ0MsUUFBUixDQUFELEdBQXFCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxHQUFqRixDQUE1RCxFQUFnSkgsQ0FBdko7O0FBQ0EsV0FBU0MsSUFBVCxDQUFjRyxDQUFkLEVBQWlCO0FBQUUsV0FBTyxVQUFVQyxDQUFWLEVBQWE7QUFBRSxhQUFPdEIsSUFBSSxDQUFDLENBQUNxQixDQUFELEVBQUlDLENBQUosQ0FBRCxDQUFYO0FBQXNCLEtBQTVDO0FBQStDOztBQUNsRSxXQUFTdEIsSUFBVCxDQUFjdUIsRUFBZCxFQUFrQjtBQUNkLFFBQUlSLENBQUosRUFBTyxNQUFNLElBQUlTLFNBQUosQ0FBYyxpQ0FBZCxDQUFOOztBQUNQLFdBQU9kLENBQVA7QUFBVSxVQUFJO0FBQ1YsWUFBSUssQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxLQUFLSixDQUFDLEdBQUdXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFSLEdBQVlQLENBQUMsQ0FBQyxRQUFELENBQWIsR0FBMEJPLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVAsQ0FBQyxDQUFDLE9BQUQsQ0FBRCxLQUFlLENBQUNKLENBQUMsR0FBR0ksQ0FBQyxDQUFDLFFBQUQsQ0FBTixLQUFxQkosQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsQ0FBckIsRUFBZ0MsQ0FBL0MsQ0FBUixHQUE0REEsQ0FBQyxDQUFDZixJQUFqRyxDQUFELElBQTJHLENBQUMsQ0FBQ1csQ0FBQyxHQUFHQSxDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxFQUFVTyxFQUFFLENBQUMsQ0FBRCxDQUFaLENBQUwsRUFBdUJsQixJQUE5SSxFQUFvSixPQUFPTyxDQUFQO0FBQ3BKLFlBQUlJLENBQUMsR0FBRyxDQUFKLEVBQU9KLENBQVgsRUFBY1csRUFBRSxHQUFHLENBQUNBLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFULEVBQVlYLENBQUMsQ0FBQ2hCLEtBQWQsQ0FBTDs7QUFDZCxnQkFBUTJCLEVBQUUsQ0FBQyxDQUFELENBQVY7QUFDSSxlQUFLLENBQUw7QUFBUSxlQUFLLENBQUw7QUFBUVgsYUFBQyxHQUFHVyxFQUFKO0FBQVE7O0FBQ3hCLGVBQUssQ0FBTDtBQUFRYixhQUFDLENBQUN4QixLQUFGO0FBQVcsbUJBQU87QUFBRVUsbUJBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQVg7QUFBZ0JsQixrQkFBSSxFQUFFO0FBQXRCLGFBQVA7O0FBQ25CLGVBQUssQ0FBTDtBQUFRSyxhQUFDLENBQUN4QixLQUFGO0FBQVc4QixhQUFDLEdBQUdPLEVBQUUsQ0FBQyxDQUFELENBQU47QUFBV0EsY0FBRSxHQUFHLENBQUMsQ0FBRCxDQUFMO0FBQVU7O0FBQ3hDLGVBQUssQ0FBTDtBQUFRQSxjQUFFLEdBQUdiLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOLEVBQUw7O0FBQWtCaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7O0FBQ3hDO0FBQ0ksZ0JBQUksRUFBRWQsQ0FBQyxHQUFHRixDQUFDLENBQUNHLElBQU4sRUFBWUQsQ0FBQyxHQUFHQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFYLElBQWdCZixDQUFDLENBQUNBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVosQ0FBbkMsTUFBdURKLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWVBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFoRixDQUFKLEVBQXdGO0FBQUViLGVBQUMsR0FBRyxDQUFKO0FBQU87QUFBVzs7QUFDNUcsZ0JBQUlhLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLEtBQWdCLENBQUNYLENBQUQsSUFBT1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFULElBQWdCVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQWhELENBQUosRUFBMkQ7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVcUMsRUFBRSxDQUFDLENBQUQsQ0FBWjtBQUFpQjtBQUFROztBQUN0RixnQkFBSUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZWIsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBOUIsRUFBbUM7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDtBQUFnQkEsZUFBQyxHQUFHVyxFQUFKO0FBQVE7QUFBUTs7QUFDckUsZ0JBQUlYLENBQUMsSUFBSUYsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBcEIsRUFBeUI7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDs7QUFBZ0JGLGVBQUMsQ0FBQ0ksR0FBRixDQUFNYyxJQUFOLENBQVdMLEVBQVg7O0FBQWdCO0FBQVE7O0FBQ25FLGdCQUFJWCxDQUFDLENBQUMsQ0FBRCxDQUFMLEVBQVVGLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOOztBQUNWaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7QUFYdEI7O0FBYUFILFVBQUUsR0FBR2QsSUFBSSxDQUFDZ0IsSUFBTCxDQUFVbEMsT0FBVixFQUFtQm1CLENBQW5CLENBQUw7QUFDSCxPQWpCUyxDQWlCUixPQUFPUixDQUFQLEVBQVU7QUFBRXFCLFVBQUUsR0FBRyxDQUFDLENBQUQsRUFBSXJCLENBQUosQ0FBTDtBQUFhYyxTQUFDLEdBQUcsQ0FBSjtBQUFRLE9BakJ6QixTQWlCa0M7QUFBRUQsU0FBQyxHQUFHSCxDQUFDLEdBQUcsQ0FBUjtBQUFZO0FBakIxRDs7QUFrQkEsUUFBSVcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVosRUFBZSxNQUFNQSxFQUFFLENBQUMsQ0FBRCxDQUFSO0FBQWEsV0FBTztBQUFFM0IsV0FBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRQSxFQUFFLENBQUMsQ0FBRCxDQUFWLEdBQWdCLEtBQUssQ0FBOUI7QUFBaUNsQixVQUFJLEVBQUU7QUFBdkMsS0FBUDtBQUMvQjtBQUNKLENBMUJEOztBQTJCQTlELE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjs7QUFDQSxJQUFJcUYsYUFBYSxHQUFHcEYsbUJBQU8sQ0FBQyxxR0FBRCxDQUEzQjs7QUFDQSxJQUFJeUYsU0FBUyxHQUFHekYsbUJBQU8sQ0FBQyw2RkFBRCxDQUF2Qjs7QUFDQSxJQUFJcUYsNkJBQTZCLEdBQUdyRixtQkFBTyxDQUFDLGtJQUFELENBQTNDOztBQUNBLElBQUlrSSxTQUFTLEdBQUdsSSxtQkFBTyxDQUFDLDBGQUFELENBQXZCOztBQUNBLElBQUltSSxTQUFTLEdBQUduSSxtQkFBTyxDQUFDLDBGQUFELENBQXZCOztBQUNBLElBQUlvSSxTQUFTLEdBQUdwSSxtQkFBTyxDQUFDLGdHQUFELENBQXZCOztBQUNBLElBQUl3RixPQUFPLEdBQUd4RixtQkFBTyxDQUFDLG1GQUFELENBQXJCOztBQUNBLElBQUlxSSxvQkFBb0IsR0FBRyxTQUF2QkEsb0JBQXVCLENBQVV6QyxLQUFWLEVBQWlCO0FBQ3hDLE1BQUlDLEVBQUUsR0FBRyxDQUFDLEdBQUdMLE9BQU8sQ0FBQ00sUUFBWixFQUFzQixJQUF0QixDQUFUO0FBQUEsTUFBc0N3QyxXQUFXLEdBQUd6QyxFQUFFLENBQUMsQ0FBRCxDQUF0RDtBQUFBLE1BQTJEMEMsY0FBYyxHQUFHMUMsRUFBRSxDQUFDLENBQUQsQ0FBOUU7O0FBQ0EsR0FBQyxHQUFHTCxPQUFPLENBQUNTLFNBQVosRUFBdUIsWUFBWTtBQUMvQnNDLGtCQUFjLENBQUMzQyxLQUFLLENBQUM0QyxPQUFOLENBQWMzRyxJQUFmLENBQWQ7QUFDSCxHQUZELEVBRUcsQ0FBQytELEtBQUssQ0FBQzRDLE9BQU4sQ0FBYzNHLElBQWYsQ0FGSDs7QUFHQSxNQUFJd0UsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBVUMsV0FBVixFQUF1QkMsYUFBdkIsRUFBc0M7QUFBRSxXQUFPMUQsU0FBUyxDQUFDLEtBQUssQ0FBTixFQUFTLEtBQUssQ0FBZCxFQUFpQixLQUFLLENBQXRCLEVBQXlCLFlBQVk7QUFDM0csVUFBSTRGLGVBQUosRUFBcUJoQyxJQUFyQjtBQUNBLGFBQU8xQyxXQUFXLENBQUMsSUFBRCxFQUFPLFVBQVU4QixFQUFWLEVBQWM7QUFDbkMsZ0JBQVFBLEVBQUUsQ0FBQ3BELEtBQVg7QUFDSSxlQUFLLENBQUw7QUFDSWdHLDJCQUFlLEdBQUcsSUFBSUwsU0FBUyxDQUFDTSxjQUFkLEVBQWxCO0FBQ0EsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBY0QsZUFBZSxDQUFDRSxpQkFBaEIsQ0FBa0NMLFdBQWxDLEVBQStDMUMsS0FBSyxDQUFDZ0QsSUFBckQsQ0FBZCxDQUFQOztBQUNKLGVBQUssQ0FBTDtBQUNJbkMsZ0JBQUksR0FBR1osRUFBRSxDQUFDM0IsSUFBSCxFQUFQO0FBQ0EsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBZTtBQUNkeEIscUJBQU8sRUFBRStELElBREs7QUFFZEcscUJBQU8sRUFBRTtBQUZLLGFBQWYsQ0FBUDtBQU5SO0FBV0gsT0FaaUIsQ0FBbEI7QUFhSCxLQWZ5RSxDQUFoQjtBQWVyRCxHQWZMOztBQWdCQSxNQUFJQyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFVQyxNQUFWLEVBQWtCO0FBQzdCLFFBQUksT0FBT2xCLEtBQUssQ0FBQ2lCLFFBQWIsS0FBMEIsVUFBOUIsRUFBMEM7QUFDdENqQixXQUFLLENBQUNpQixRQUFOLENBQWVDLE1BQWY7QUFDSDtBQUNKLEdBSkQ7O0FBS0EsU0FBUSxDQUFDLEdBQUcxQixhQUFhLENBQUM0QixHQUFsQixFQUF1QjNCLDZCQUE2QixDQUFDNkIsYUFBckQsRUFBb0U7QUFBRUMsa0JBQWMsRUFBRSxJQUFsQjtBQUF3QmhFLFNBQUssRUFBRXlDLEtBQUssQ0FBQ3pDLEtBQXJDO0FBQTRDa0QsZUFBVyxFQUFFQSxXQUF6RDtBQUFzRWUsa0JBQWMsRUFBRSx3QkFBVU4sTUFBVixFQUFrQjtBQUFFLGFBQU9BLE1BQU0sQ0FBQ2pGLElBQWQ7QUFBcUIsS0FBL0g7QUFBaUl3RixrQkFBYyxFQUFFLHdCQUFVUCxNQUFWLEVBQWtCO0FBQUUsYUFBT0EsTUFBTSxDQUFDNUUsU0FBUCxHQUFtQixHQUFuQixHQUF5QjRFLE1BQU0sQ0FBQzNFLFFBQXZDO0FBQWtELEtBQXZOO0FBQXlOMEUsWUFBUSxFQUFFQSxRQUFuTztBQUE2T1MsZ0JBQVksRUFBRTtBQUEzUCxHQUFwRSxFQUF3VUMsSUFBSSxDQUFDQyxTQUFMLENBQWVjLFdBQWYsQ0FBeFUsQ0FBUjtBQUNILENBM0JEOztBQTRCQUQsb0JBQW9CLENBQUNaLFNBQXJCLEdBQWlDO0FBQzdCdEUsT0FBSyxFQUFFc0MsU0FBUyxDQUFDaUMsVUFBVixDQUFxQlMsU0FBUyxDQUFDbEcsT0FBL0IsQ0FEc0I7QUFFN0J1RyxTQUFPLEVBQUUvQyxTQUFTLENBQUNpQyxVQUFWLENBQXFCUSxTQUFTLENBQUM5RixPQUEvQixDQUZvQjtBQUc3QndHLE1BQUksRUFBRW5ELFNBQVMsQ0FBQ29ELE1BSGE7QUFJN0JoQyxVQUFRLEVBQUVwQixTQUFTLENBQUNxRDtBQUpTLENBQWpDO0FBTUFoSixPQUFPLENBQUMsU0FBRCxDQUFQLEdBQXFCdUksb0JBQXJCLEM7Ozs7Ozs7Ozs7OztBQy9FYTs7OztBQUNiLElBQUlVLFFBQVEsR0FBSSxRQUFRLEtBQUtBLFFBQWQsSUFBMkIsWUFBWTtBQUNsREEsVUFBUSxHQUFHQyxNQUFNLENBQUNDLE1BQVAsSUFBaUIsVUFBUzlFLENBQVQsRUFBWTtBQUNwQyxTQUFLLElBQUkrRSxDQUFKLEVBQU9DLENBQUMsR0FBRyxDQUFYLEVBQWN2RSxDQUFDLEdBQUd3RSxTQUFTLENBQUNsRSxNQUFqQyxFQUF5Q2lFLENBQUMsR0FBR3ZFLENBQTdDLEVBQWdEdUUsQ0FBQyxFQUFqRCxFQUFxRDtBQUNqREQsT0FBQyxHQUFHRSxTQUFTLENBQUNELENBQUQsQ0FBYjs7QUFDQSxXQUFLLElBQUlFLENBQVQsSUFBY0gsQ0FBZDtBQUFpQixZQUFJRixNQUFNLENBQUM5SCxTQUFQLENBQWlCb0ksY0FBakIsQ0FBZ0N0RSxJQUFoQyxDQUFxQ2tFLENBQXJDLEVBQXdDRyxDQUF4QyxDQUFKLEVBQ2JsRixDQUFDLENBQUNrRixDQUFELENBQUQsR0FBT0gsQ0FBQyxDQUFDRyxDQUFELENBQVI7QUFESjtBQUVIOztBQUNELFdBQU9sRixDQUFQO0FBQ0gsR0FQRDs7QUFRQSxTQUFPNEUsUUFBUSxDQUFDakYsS0FBVCxDQUFlLElBQWYsRUFBcUJzRixTQUFyQixDQUFQO0FBQ0gsQ0FWRDs7QUFXQXRKLE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjs7QUFDQSxJQUFJcUYsYUFBYSxHQUFHcEYsbUJBQU8sQ0FBQyxxR0FBRCxDQUEzQjs7QUFDQSxJQUFJdUosZUFBZSxHQUFHdkosbUJBQU8sQ0FBQyx1R0FBRCxDQUE3Qjs7QUFDQSxJQUFJd0osc0JBQXNCLEdBQUd4SixtQkFBTyxDQUFDLHFIQUFELENBQXBDOztBQUNBLElBQUlrSSxTQUFTLEdBQUdsSSxtQkFBTyxDQUFDLDBGQUFELENBQXZCOztBQUNBLElBQUltSSxTQUFTLEdBQUduSSxtQkFBTyxDQUFDLDBGQUFELENBQXZCOztBQUNBLElBQUl3RixPQUFPLEdBQUd4RixtQkFBTyxDQUFDLG1GQUFELENBQXJCOztBQUNBLElBQUl1RixPQUFPLEdBQUd2RixtQkFBTyxDQUFDLHNGQUFELENBQXJCOztBQUNBLElBQUlRLFVBQVUsR0FBR1IsbUJBQU8sQ0FBQyxtR0FBRCxDQUF4Qjs7QUFDQSxJQUFJeUosd0JBQXdCLEdBQUd6SixtQkFBTyxDQUFDLGtJQUFELENBQXRDOztBQUNBLElBQUkwSixnQkFBZ0IsR0FBRyxTQUFuQkEsZ0JBQW1CLENBQVU5RCxLQUFWLEVBQWlCO0FBQ3BDLE1BQUkrRCxVQUFVLEdBQUduSixVQUFVLENBQUNNLFNBQVgsQ0FBcUJDLE9BQXJCLENBQTZCd0UsT0FBTyxDQUFDYSxLQUFyQyxDQUFqQjs7QUFDQSxNQUFJUCxFQUFFLEdBQUcsQ0FBQyxHQUFHTCxPQUFPLENBQUNNLFFBQVosRUFBc0IsSUFBSW9DLFNBQVMsQ0FBQzlGLE9BQWQsRUFBdEIsQ0FBVDtBQUFBLE1BQXlEb0csT0FBTyxHQUFHM0MsRUFBRSxDQUFDLENBQUQsQ0FBckU7QUFBQSxNQUEwRStELFVBQVUsR0FBRy9ELEVBQUUsQ0FBQyxDQUFELENBQXpGOztBQUNBLE1BQUlnRSxFQUFFLEdBQUcsQ0FBQyxHQUFHckUsT0FBTyxDQUFDTSxRQUFaLEVBQXNCLElBQUlxQyxTQUFTLENBQUNsRyxPQUFkLEVBQXRCLENBQVQ7QUFBQSxNQUF5RDZILHFCQUFxQixHQUFHRCxFQUFFLENBQUMsQ0FBRCxDQUFuRjtBQUFBLE1BQXdGRSx3QkFBd0IsR0FBR0YsRUFBRSxDQUFDLENBQUQsQ0FBckg7O0FBQ0EsTUFBSUcsRUFBRSxHQUFHLENBQUMsR0FBR3hFLE9BQU8sQ0FBQ00sUUFBWixFQUFzQixJQUFJcUMsU0FBUyxDQUFDbEcsT0FBZCxFQUF0QixDQUFUO0FBQUEsTUFBeURnSSxzQkFBc0IsR0FBR0QsRUFBRSxDQUFDLENBQUQsQ0FBcEY7QUFBQSxNQUF5RkUseUJBQXlCLEdBQUdGLEVBQUUsQ0FBQyxDQUFELENBQXZIOztBQUNBLE1BQUlHLEVBQUUsR0FBRyxDQUFDLEdBQUczRSxPQUFPLENBQUNNLFFBQVosRUFBc0IsSUFBSXFDLFNBQVMsQ0FBQ2xHLE9BQWQsRUFBdEIsQ0FBVDtBQUFBLE1BQXlEbUksb0JBQW9CLEdBQUdELEVBQUUsQ0FBQyxDQUFELENBQWxGO0FBQUEsTUFBdUZFLHVCQUF1QixHQUFHRixFQUFFLENBQUMsQ0FBRCxDQUFuSDs7QUFDQVIsWUFBVSxDQUFDVyxlQUFYLEdBQTZCTCxzQkFBN0I7QUFDQU4sWUFBVSxDQUFDWSxjQUFYLEdBQTRCVCxxQkFBNUI7QUFDQUgsWUFBVSxDQUFDYSxhQUFYLEdBQTJCSixvQkFBM0I7QUFDQSxTQUFRLENBQUMsR0FBR2hGLGFBQWEsQ0FBQzRDLElBQWxCLEVBQXdCLEtBQXhCLEVBQStCO0FBQUVmLFlBQVEsRUFBRSxDQUFDLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRDLElBQWxCLEVBQXdCLEtBQXhCLEVBQStCZSxRQUFRLENBQUM7QUFBRTBCLGVBQVMsRUFBRTtBQUFiLEtBQUQsRUFBdUI7QUFBRXhELGNBQVEsRUFBRSxDQUFDLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRDLElBQWxCLEVBQXdCLEtBQXhCLEVBQStCZSxRQUFRLENBQUM7QUFBRTBCLGlCQUFTLEVBQUU7QUFBYixPQUFELEVBQXlCO0FBQUV4RCxnQkFBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIsSUFBdkIsRUFBNkI7QUFBRUMsa0JBQVEsRUFBRTtBQUFaLFNBQTdCLENBQUQsRUFBd0QsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJ1QyxlQUFlLENBQUMsU0FBRCxDQUF0QyxFQUFtRDtBQUFFcEcsZUFBSyxFQUFFcUYsT0FBVDtBQUFrQnRDLGNBQUksRUFBRU4sS0FBSyxDQUFDTSxJQUE5QjtBQUFvQ1csa0JBQVEsRUFBRStDO0FBQTlDLFNBQW5ELENBQXhEO0FBQVosT0FBekIsQ0FBdkMsQ0FBRCxFQUEwUCxDQUFDLEdBQUd4RSxhQUFhLENBQUM0QyxJQUFsQixFQUF3QixLQUF4QixFQUErQmUsUUFBUSxDQUFDO0FBQUUwQixpQkFBUyxFQUFFO0FBQWIsT0FBRCxFQUF5QjtBQUFFeEQsZ0JBQVEsRUFBRSxDQUFDLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCLElBQXZCLEVBQTZCO0FBQUVDLGtCQUFRLEVBQUU7QUFBWixTQUE3QixDQUFELEVBQXdELENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCeUMsd0JBQXdCLENBQUMsU0FBRCxDQUEvQyxFQUE0RDtBQUFFdkQsY0FBSSxFQUFFTixLQUFLLENBQUNNO0FBQWQsU0FBNUQsQ0FBeEQ7QUFBWixPQUF6QixDQUF2QyxDQUExUDtBQUFaLEtBQXZCLENBQXZDLENBQUQsRUFBc2lCLENBQUMsR0FBR2QsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0JlLFFBQVEsQ0FBQztBQUFFMEIsZUFBUyxFQUFFO0FBQWIsS0FBRCxFQUF1QjtBQUFFeEQsY0FBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0JlLFFBQVEsQ0FBQztBQUFFMEIsaUJBQVMsRUFBRTtBQUFiLE9BQUQsRUFBeUI7QUFBRXhELGdCQUFRLEVBQUUsQ0FBQyxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QixJQUF2QixFQUE2QjtBQUFFQyxrQkFBUSxFQUFFO0FBQVosU0FBN0IsQ0FBRCxFQUF3RCxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QndDLHNCQUFzQixDQUFDLFNBQUQsQ0FBN0MsRUFBMEQ7QUFBRXJHLGVBQUssRUFBRTJHLHFCQUFUO0FBQWdDdEIsaUJBQU8sRUFBRUEsT0FBekM7QUFBa0RJLGNBQUksRUFBRSxDQUF4RDtBQUEyRC9CLGtCQUFRLEVBQUVrRDtBQUFyRSxTQUExRCxDQUF4RDtBQUFaLE9BQXpCLENBQXZDLENBQUQsRUFBc1MsQ0FBQyxHQUFHM0UsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0JlLFFBQVEsQ0FBQztBQUFFMEIsaUJBQVMsRUFBRTtBQUFiLE9BQUQsRUFBeUI7QUFBRXhELGdCQUFRLEVBQUUsQ0FBQyxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QixJQUF2QixFQUE2QjtBQUFFQyxrQkFBUSxFQUFFO0FBQVosU0FBN0IsQ0FBRCxFQUF5RCxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QndDLHNCQUFzQixDQUFDLFNBQUQsQ0FBN0MsRUFBMEQ7QUFBRXJHLGVBQUssRUFBRThHLHNCQUFUO0FBQWlDekIsaUJBQU8sRUFBRUEsT0FBMUM7QUFBbURJLGNBQUksRUFBRSxDQUF6RDtBQUE0RC9CLGtCQUFRLEVBQUVxRDtBQUF0RSxTQUExRCxDQUF6RDtBQUFaLE9BQXpCLENBQXZDLENBQXRTLEVBQThrQixDQUFDLEdBQUc5RSxhQUFhLENBQUM0QyxJQUFsQixFQUF3QixLQUF4QixFQUErQmUsUUFBUSxDQUFDO0FBQUUwQixpQkFBUyxFQUFFO0FBQWIsT0FBRCxFQUF5QjtBQUFFeEQsZ0JBQVEsRUFBRSxDQUFDLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCLElBQXZCLEVBQTZCO0FBQUVDLGtCQUFRLEVBQUU7QUFBWixTQUE3QixDQUFELEVBQXVELENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCd0Msc0JBQXNCLENBQUMsU0FBRCxDQUE3QyxFQUEwRDtBQUFFckcsZUFBSyxFQUFFaUgsb0JBQVQ7QUFBK0I1QixpQkFBTyxFQUFFQSxPQUF4QztBQUFpREksY0FBSSxFQUFFLENBQXZEO0FBQTBEL0Isa0JBQVEsRUFBRXdEO0FBQXBFLFNBQTFELENBQXZEO0FBQVosT0FBekIsQ0FBdkMsQ0FBOWtCO0FBQVosS0FBdkIsQ0FBdkMsQ0FBdGlCO0FBQVosR0FBL0IsQ0FBUjtBQUNILENBVkQ7O0FBV0F2SyxPQUFPLENBQUMsU0FBRCxDQUFQLEdBQXFCNEosZ0JBQXJCLEM7Ozs7Ozs7Ozs7OztBQ2pDYTs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ2IsSUFBSTdHLFNBQVMsR0FBSSxRQUFRLEtBQUtBLFNBQWQsSUFBNEIsVUFBVUMsT0FBVixFQUFtQkMsVUFBbkIsRUFBK0JDLENBQS9CLEVBQWtDQyxTQUFsQyxFQUE2QztBQUNyRixXQUFTQyxLQUFULENBQWVDLEtBQWYsRUFBc0I7QUFBRSxXQUFPQSxLQUFLLFlBQVlILENBQWpCLEdBQXFCRyxLQUFyQixHQUE2QixJQUFJSCxDQUFKLENBQU0sVUFBVWpDLE9BQVYsRUFBbUI7QUFBRUEsYUFBTyxDQUFDb0MsS0FBRCxDQUFQO0FBQWlCLEtBQTVDLENBQXBDO0FBQW9GOztBQUM1RyxTQUFPLEtBQUtILENBQUMsS0FBS0EsQ0FBQyxHQUFHSSxPQUFULENBQU4sRUFBeUIsVUFBVXJDLE9BQVYsRUFBbUJzQyxNQUFuQixFQUEyQjtBQUN2RCxhQUFTQyxTQUFULENBQW1CSCxLQUFuQixFQUEwQjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUNPLElBQVYsQ0FBZUwsS0FBZixDQUFELENBQUo7QUFBOEIsT0FBcEMsQ0FBcUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDM0YsYUFBU0MsUUFBVCxDQUFrQlAsS0FBbEIsRUFBeUI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDLE9BQUQsQ0FBVCxDQUFtQkUsS0FBbkIsQ0FBRCxDQUFKO0FBQWtDLE9BQXhDLENBQXlDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzlGLGFBQVNGLElBQVQsQ0FBY0ksTUFBZCxFQUFzQjtBQUFFQSxZQUFNLENBQUNDLElBQVAsR0FBYzdDLE9BQU8sQ0FBQzRDLE1BQU0sQ0FBQ1IsS0FBUixDQUFyQixHQUFzQ0QsS0FBSyxDQUFDUyxNQUFNLENBQUNSLEtBQVIsQ0FBTCxDQUFvQlUsSUFBcEIsQ0FBeUJQLFNBQXpCLEVBQW9DSSxRQUFwQyxDQUF0QztBQUFzRjs7QUFDOUdILFFBQUksQ0FBQyxDQUFDTixTQUFTLEdBQUdBLFNBQVMsQ0FBQ2EsS0FBVixDQUFnQmhCLE9BQWhCLEVBQXlCQyxVQUFVLElBQUksRUFBdkMsQ0FBYixFQUF5RFMsSUFBekQsRUFBRCxDQUFKO0FBQ0gsR0FMTSxDQUFQO0FBTUgsQ0FSRDs7QUFTQSxJQUFJTyxXQUFXLEdBQUksUUFBUSxLQUFLQSxXQUFkLElBQThCLFVBQVVqQixPQUFWLEVBQW1Ca0IsSUFBbkIsRUFBeUI7QUFDckUsTUFBSUMsQ0FBQyxHQUFHO0FBQUV4QixTQUFLLEVBQUUsQ0FBVDtBQUFZeUIsUUFBSSxFQUFFLGdCQUFXO0FBQUUsVUFBSUMsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFPLENBQVgsRUFBYyxNQUFNQSxDQUFDLENBQUMsQ0FBRCxDQUFQO0FBQVksYUFBT0EsQ0FBQyxDQUFDLENBQUQsQ0FBUjtBQUFjLEtBQXZFO0FBQXlFQyxRQUFJLEVBQUUsRUFBL0U7QUFBbUZDLE9BQUcsRUFBRTtBQUF4RixHQUFSO0FBQUEsTUFBc0dDLENBQXRHO0FBQUEsTUFBeUdDLENBQXpHO0FBQUEsTUFBNEdKLENBQTVHO0FBQUEsTUFBK0dLLENBQS9HO0FBQ0EsU0FBT0EsQ0FBQyxHQUFHO0FBQUVoQixRQUFJLEVBQUVpQixJQUFJLENBQUMsQ0FBRCxDQUFaO0FBQWlCLGFBQVNBLElBQUksQ0FBQyxDQUFELENBQTlCO0FBQW1DLGNBQVVBLElBQUksQ0FBQyxDQUFEO0FBQWpELEdBQUosRUFBNEQsT0FBT0MsTUFBUCxLQUFrQixVQUFsQixLQUFpQ0YsQ0FBQyxDQUFDRSxNQUFNLENBQUNDLFFBQVIsQ0FBRCxHQUFxQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsR0FBakYsQ0FBNUQsRUFBZ0pILENBQXZKOztBQUNBLFdBQVNDLElBQVQsQ0FBY0csQ0FBZCxFQUFpQjtBQUFFLFdBQU8sVUFBVUMsQ0FBVixFQUFhO0FBQUUsYUFBT3RCLElBQUksQ0FBQyxDQUFDcUIsQ0FBRCxFQUFJQyxDQUFKLENBQUQsQ0FBWDtBQUFzQixLQUE1QztBQUErQzs7QUFDbEUsV0FBU3RCLElBQVQsQ0FBY3VCLEVBQWQsRUFBa0I7QUFDZCxRQUFJUixDQUFKLEVBQU8sTUFBTSxJQUFJUyxTQUFKLENBQWMsaUNBQWQsQ0FBTjs7QUFDUCxXQUFPZCxDQUFQO0FBQVUsVUFBSTtBQUNWLFlBQUlLLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsS0FBS0osQ0FBQyxHQUFHVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBUixHQUFZUCxDQUFDLENBQUMsUUFBRCxDQUFiLEdBQTBCTyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFQLENBQUMsQ0FBQyxPQUFELENBQUQsS0FBZSxDQUFDSixDQUFDLEdBQUdJLENBQUMsQ0FBQyxRQUFELENBQU4sS0FBcUJKLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLENBQXJCLEVBQWdDLENBQS9DLENBQVIsR0FBNERBLENBQUMsQ0FBQ2YsSUFBakcsQ0FBRCxJQUEyRyxDQUFDLENBQUNXLENBQUMsR0FBR0EsQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsRUFBVU8sRUFBRSxDQUFDLENBQUQsQ0FBWixDQUFMLEVBQXVCbEIsSUFBOUksRUFBb0osT0FBT08sQ0FBUDtBQUNwSixZQUFJSSxDQUFDLEdBQUcsQ0FBSixFQUFPSixDQUFYLEVBQWNXLEVBQUUsR0FBRyxDQUFDQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBVCxFQUFZWCxDQUFDLENBQUNoQixLQUFkLENBQUw7O0FBQ2QsZ0JBQVEyQixFQUFFLENBQUMsQ0FBRCxDQUFWO0FBQ0ksZUFBSyxDQUFMO0FBQVEsZUFBSyxDQUFMO0FBQVFYLGFBQUMsR0FBR1csRUFBSjtBQUFROztBQUN4QixlQUFLLENBQUw7QUFBUWIsYUFBQyxDQUFDeEIsS0FBRjtBQUFXLG1CQUFPO0FBQUVVLG1CQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFYO0FBQWdCbEIsa0JBQUksRUFBRTtBQUF0QixhQUFQOztBQUNuQixlQUFLLENBQUw7QUFBUUssYUFBQyxDQUFDeEIsS0FBRjtBQUFXOEIsYUFBQyxHQUFHTyxFQUFFLENBQUMsQ0FBRCxDQUFOO0FBQVdBLGNBQUUsR0FBRyxDQUFDLENBQUQsQ0FBTDtBQUFVOztBQUN4QyxlQUFLLENBQUw7QUFBUUEsY0FBRSxHQUFHYixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTixFQUFMOztBQUFrQmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjOztBQUN4QztBQUNJLGdCQUFJLEVBQUVkLENBQUMsR0FBR0YsQ0FBQyxDQUFDRyxJQUFOLEVBQVlELENBQUMsR0FBR0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWCxJQUFnQmYsQ0FBQyxDQUFDQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFaLENBQW5DLE1BQXVESixFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBaEYsQ0FBSixFQUF3RjtBQUFFYixlQUFDLEdBQUcsQ0FBSjtBQUFPO0FBQVc7O0FBQzVHLGdCQUFJYSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixLQUFnQixDQUFDWCxDQUFELElBQU9XLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBVCxJQUFnQlcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFoRCxDQUFKLEVBQTJEO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVXFDLEVBQUUsQ0FBQyxDQUFELENBQVo7QUFBaUI7QUFBUTs7QUFDdEYsZ0JBQUlBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWViLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQTlCLEVBQW1DO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7QUFBZ0JBLGVBQUMsR0FBR1csRUFBSjtBQUFRO0FBQVE7O0FBQ3JFLGdCQUFJWCxDQUFDLElBQUlGLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQXBCLEVBQXlCO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7O0FBQWdCRixlQUFDLENBQUNJLEdBQUYsQ0FBTWMsSUFBTixDQUFXTCxFQUFYOztBQUFnQjtBQUFROztBQUNuRSxnQkFBSVgsQ0FBQyxDQUFDLENBQUQsQ0FBTCxFQUFVRixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTjs7QUFDVmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjO0FBWHRCOztBQWFBSCxVQUFFLEdBQUdkLElBQUksQ0FBQ2dCLElBQUwsQ0FBVWxDLE9BQVYsRUFBbUJtQixDQUFuQixDQUFMO0FBQ0gsT0FqQlMsQ0FpQlIsT0FBT1IsQ0FBUCxFQUFVO0FBQUVxQixVQUFFLEdBQUcsQ0FBQyxDQUFELEVBQUlyQixDQUFKLENBQUw7QUFBYWMsU0FBQyxHQUFHLENBQUo7QUFBUSxPQWpCekIsU0FpQmtDO0FBQUVELFNBQUMsR0FBR0gsQ0FBQyxHQUFHLENBQVI7QUFBWTtBQWpCMUQ7O0FBa0JBLFFBQUlXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFaLEVBQWUsTUFBTUEsRUFBRSxDQUFDLENBQUQsQ0FBUjtBQUFhLFdBQU87QUFBRTNCLFdBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUUEsRUFBRSxDQUFDLENBQUQsQ0FBVixHQUFnQixLQUFLLENBQTlCO0FBQWlDbEIsVUFBSSxFQUFFO0FBQXZDLEtBQVA7QUFDL0I7QUFDSixDQTFCRDs7QUEyQkE5RCxPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7O0FBQ0EsSUFBSXFGLGFBQWEsR0FBR3BGLG1CQUFPLENBQUMscUdBQUQsQ0FBM0I7O0FBQ0EsSUFBSXlGLFNBQVMsR0FBR3pGLG1CQUFPLENBQUMsNkZBQUQsQ0FBdkI7O0FBQ0EsSUFBSXFGLDZCQUE2QixHQUFHckYsbUJBQU8sQ0FBQyxrSUFBRCxDQUEzQzs7QUFDQSxJQUFJa0ksU0FBUyxHQUFHbEksbUJBQU8sQ0FBQywwRkFBRCxDQUF2Qjs7QUFDQSxJQUFJb0ksU0FBUyxHQUFHcEksbUJBQU8sQ0FBQyxnR0FBRCxDQUF2Qjs7QUFDQSxJQUFJMEYsTUFBTSxHQUFHMUYsbUJBQU8sQ0FBQyxvRkFBRCxDQUFwQjs7QUFDQSxJQUFJd0YsT0FBTyxHQUFHeEYsbUJBQU8sQ0FBQyxtRkFBRCxDQUFyQjs7QUFDQSxJQUFJUSxVQUFVLEdBQUdSLG1CQUFPLENBQUMsbUdBQUQsQ0FBeEI7O0FBQ0EsSUFBSXVGLE9BQU8sR0FBR3ZGLG1CQUFPLENBQUMsc0ZBQUQsQ0FBckI7O0FBQ0EsSUFBSTBLLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVTlFLEtBQVYsRUFBaUI7QUFDakMsTUFBSUMsRUFBRSxHQUFHLENBQUMsR0FBR0wsT0FBTyxDQUFDTSxRQUFaLEVBQXNCLElBQXRCLENBQVQ7QUFBQSxNQUFzQ0MsUUFBUSxHQUFHRixFQUFFLENBQUMsQ0FBRCxDQUFuRDtBQUFBLE1BQXdERyxXQUFXLEdBQUdILEVBQUUsQ0FBQyxDQUFELENBQXhFOztBQUNBLEdBQUMsR0FBR0wsT0FBTyxDQUFDUyxTQUFaLEVBQXVCLFlBQVk7QUFDL0JELGVBQVcsQ0FBQ0osS0FBSyxDQUFDTSxJQUFOLENBQVdyRSxJQUFaLENBQVg7QUFDSCxHQUZELEVBRUcsQ0FBQytELEtBQUssQ0FBQ00sSUFBTixDQUFXckUsSUFBWixDQUZIO0FBR0EsTUFBSXNFLEtBQUssR0FBRzNGLFVBQVUsQ0FBQ00sU0FBWCxDQUFxQkMsT0FBckIsQ0FBNkJ3RSxPQUFPLENBQUNhLEtBQXJDLENBQVo7O0FBQ0EsTUFBSUMsV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBVUMsV0FBVixFQUF1QkMsYUFBdkIsRUFBc0NWLEVBQXRDLEVBQTBDO0FBQ3hELFFBQUk4RSxJQUFJLEdBQUc5RSxFQUFFLENBQUM4RSxJQUFkO0FBQ0EsV0FBTzlILFNBQVMsQ0FBQyxLQUFLLENBQU4sRUFBUyxLQUFLLENBQWQsRUFBaUIsS0FBSyxDQUF0QixFQUF5QixZQUFZO0FBQ2pELFVBQUkrSCxXQUFKLEVBQWlCQyxRQUFqQjtBQUNBLGFBQU85RyxXQUFXLENBQUMsSUFBRCxFQUFPLFVBQVU4RixFQUFWLEVBQWM7QUFDbkMsZ0JBQVFBLEVBQUUsQ0FBQ3BILEtBQVg7QUFDSSxlQUFLLENBQUw7QUFDSW1JLHVCQUFXLEdBQUcsSUFBSXhDLFNBQVMsQ0FBQ00sY0FBZCxFQUFkO0FBQ0EsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBY2tDLFdBQVcsQ0FBQ0UsV0FBWixDQUF3QnhFLFdBQXhCLEVBQXFDcUUsSUFBckMsRUFBMkMvRSxLQUFLLENBQUNNLElBQWpELENBQWQsQ0FBUDs7QUFDSixlQUFLLENBQUw7QUFDSTJFLG9CQUFRLEdBQUdoQixFQUFFLENBQUMzRixJQUFILEVBQVg7QUFDQSxtQkFBTyxDQUFDO0FBQUU7QUFBSCxjQUFlO0FBQ2R4QixxQkFBTyxFQUFFbUksUUFESztBQUVkakUscUJBQU8sRUFBRSxLQUZLO0FBR2RtRSx3QkFBVSxFQUFFO0FBQ1JKLG9CQUFJLEVBQUVBLElBQUksR0FBRztBQURMO0FBSEUsYUFBZixDQUFQO0FBTlI7QUFjSCxPQWZpQixDQUFsQjtBQWdCSCxLQWxCZSxDQUFoQjtBQW1CSCxHQXJCRDs7QUFzQkEsTUFBSTlELFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQVVDLE1BQVYsRUFBa0I7QUFDN0IsUUFBSSxPQUFPbEIsS0FBSyxDQUFDaUIsUUFBYixLQUEwQixVQUE5QixFQUEwQztBQUN0Q2pCLFdBQUssQ0FBQ2lCLFFBQU4sQ0FBZUMsTUFBZjtBQUNIOztBQUNEWCxTQUFLLENBQUNxQyxPQUFOLEdBQWdCMUIsTUFBaEI7QUFDSCxHQUxEOztBQU1BLFNBQVEsQ0FBQyxHQUFHMUIsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIzQiw2QkFBNkIsQ0FBQzZCLGFBQXJELEVBQW9FO0FBQUVDLGtCQUFjLEVBQUUsSUFBbEI7QUFBd0JoRSxTQUFLLEVBQUV5QyxLQUFLLENBQUN6QyxLQUFyQztBQUE0Q2tELGVBQVcsRUFBRUEsV0FBekQ7QUFBc0VlLGtCQUFjLEVBQUUsd0JBQVVOLE1BQVYsRUFBa0I7QUFBRSxhQUFPQSxNQUFNLENBQUNqRixJQUFkO0FBQXFCLEtBQS9IO0FBQWlJd0Ysa0JBQWMsRUFBRSx3QkFBVVAsTUFBVixFQUFrQjtBQUFFLGFBQU9BLE1BQU0sQ0FBQ3pFLFFBQWQ7QUFBeUIsS0FBOUw7QUFBZ013RSxZQUFRLEVBQUVBLFFBQTFNO0FBQW9OUyxnQkFBWSxFQUFFLElBQWxPO0FBQXdPeUQsY0FBVSxFQUFFO0FBQ3hUSixVQUFJLEVBQUU7QUFEa1Q7QUFBcFAsR0FBcEUsRUFFQ3BELElBQUksQ0FBQ0MsU0FBTCxDQUFlekIsUUFBZixDQUZELENBQVI7QUFHSCxDQXJDRDs7QUFzQ0EyRSxhQUFhLENBQUNqRCxTQUFkLEdBQTBCO0FBQ3RCdEUsT0FBSyxFQUFFc0MsU0FBUyxDQUFDaUMsVUFBVixDQUFxQlEsU0FBUyxDQUFDOUYsT0FBL0IsQ0FEZTtBQUV0QnlFLFVBQVEsRUFBRXBCLFNBQVMsQ0FBQ3FELElBRkU7QUFHdEI1QyxNQUFJLEVBQUVULFNBQVMsQ0FBQ2lDLFVBQVYsQ0FBcUJoQyxNQUFNLENBQUMvQyxJQUE1QjtBQUhnQixDQUExQjtBQUtBN0MsT0FBTyxDQUFDLFNBQUQsQ0FBUCxHQUFxQjRLLGFBQXJCLEM7Ozs7Ozs7Ozs7OztBQzFGYTs7OztBQUNiLElBQUkzQixRQUFRLEdBQUksUUFBUSxLQUFLQSxRQUFkLElBQTJCLFlBQVk7QUFDbERBLFVBQVEsR0FBR0MsTUFBTSxDQUFDQyxNQUFQLElBQWlCLFVBQVM5RSxDQUFULEVBQVk7QUFDcEMsU0FBSyxJQUFJK0UsQ0FBSixFQUFPQyxDQUFDLEdBQUcsQ0FBWCxFQUFjdkUsQ0FBQyxHQUFHd0UsU0FBUyxDQUFDbEUsTUFBakMsRUFBeUNpRSxDQUFDLEdBQUd2RSxDQUE3QyxFQUFnRHVFLENBQUMsRUFBakQsRUFBcUQ7QUFDakRELE9BQUMsR0FBR0UsU0FBUyxDQUFDRCxDQUFELENBQWI7O0FBQ0EsV0FBSyxJQUFJRSxDQUFULElBQWNILENBQWQ7QUFBaUIsWUFBSUYsTUFBTSxDQUFDOUgsU0FBUCxDQUFpQm9JLGNBQWpCLENBQWdDdEUsSUFBaEMsQ0FBcUNrRSxDQUFyQyxFQUF3Q0csQ0FBeEMsQ0FBSixFQUNibEYsQ0FBQyxDQUFDa0YsQ0FBRCxDQUFELEdBQU9ILENBQUMsQ0FBQ0csQ0FBRCxDQUFSO0FBREo7QUFFSDs7QUFDRCxXQUFPbEYsQ0FBUDtBQUNILEdBUEQ7O0FBUUEsU0FBTzRFLFFBQVEsQ0FBQ2pGLEtBQVQsQ0FBZSxJQUFmLEVBQXFCc0YsU0FBckIsQ0FBUDtBQUNILENBVkQ7O0FBV0F0SixPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7O0FBQ0EsSUFBSXFGLGFBQWEsR0FBR3BGLG1CQUFPLENBQUMscUdBQUQsQ0FBM0I7O0FBQ0EsSUFBSXdGLE9BQU8sR0FBR3hGLG1CQUFPLENBQUMsbUZBQUQsQ0FBckI7O0FBQ0EsSUFBSWdMLFFBQVEsR0FBR2hMLG1CQUFPLENBQUMsbUhBQUQsQ0FBdEI7O0FBQ0EsSUFBSWlMLE9BQU8sR0FBR2pMLG1CQUFPLENBQUMsaUhBQUQsQ0FBckI7O0FBQ0EsSUFBSXlGLFNBQVMsR0FBR3pGLG1CQUFPLENBQUMsNkZBQUQsQ0FBdkI7O0FBQ0EsSUFBSTBGLE1BQU0sR0FBRzFGLG1CQUFPLENBQUMsb0ZBQUQsQ0FBcEI7O0FBQ0EsSUFBSWtMLFNBQVMsR0FBR2xMLG1CQUFPLENBQUMsMEZBQUQsQ0FBdkI7O0FBQ0EsSUFBSW1MLGNBQWMsR0FBR25MLG1CQUFPLENBQUMsb0dBQUQsQ0FBNUI7O0FBQ0EsSUFBSW9MLG9CQUFvQixHQUFHcEwsbUJBQU8sQ0FBQywwSEFBRCxDQUFsQzs7QUFDQSxJQUFJcUwsZUFBZSxHQUFHckwsbUJBQU8sQ0FBQyxnSEFBRCxDQUE3Qjs7QUFDQSxJQUFJc0wsYUFBYSxHQUFHdEwsbUJBQU8sQ0FBQyw0R0FBRCxDQUEzQjs7QUFDQSxJQUFJdUwsb0JBQW9CLEdBQUcsU0FBdkJBLG9CQUF1QixDQUFVM0YsS0FBVixFQUFpQjtBQUN4QyxNQUFJQyxFQUFFLEdBQUcsQ0FBQyxHQUFHTCxPQUFPLENBQUNNLFFBQVosRUFBc0IsSUFBdEIsQ0FBVDtBQUFBLE1BQXNDSSxJQUFJLEdBQUdMLEVBQUUsQ0FBQyxDQUFELENBQS9DO0FBQUEsTUFBb0RrQyxPQUFPLEdBQUdsQyxFQUFFLENBQUMsQ0FBRCxDQUFoRTs7QUFDQSxHQUFDLEdBQUdMLE9BQU8sQ0FBQ1MsU0FBWixFQUF1QixZQUFZO0FBQy9COEIsV0FBTyxDQUFDbkMsS0FBSyxDQUFDTSxJQUFQLENBQVA7QUFDSCxHQUZELEVBRUcsQ0FBQ04sS0FBSyxDQUFDTSxJQUFQLENBRkg7O0FBR0EsTUFBSTJELEVBQUUsR0FBRyxDQUFDLEdBQUdyRSxPQUFPLENBQUNNLFFBQVosRUFBc0IsS0FBdEIsQ0FBVDtBQUFBLE1BQXVDMEYsSUFBSSxHQUFHM0IsRUFBRSxDQUFDLENBQUQsQ0FBaEQ7QUFBQSxNQUFxRDRCLE9BQU8sR0FBRzVCLEVBQUUsQ0FBQyxDQUFELENBQWpFOztBQUNBLE1BQUk2QixXQUFXLEdBQUcsU0FBZEEsV0FBYyxHQUFZO0FBQUUsV0FBT0QsT0FBTyxDQUFDLEtBQUQsQ0FBZDtBQUF3QixHQUF4RDs7QUFDQSxNQUFJRSxVQUFVLEdBQUcsU0FBYkEsVUFBYSxHQUFZO0FBQUUsV0FBT0YsT0FBTyxDQUFDLElBQUQsQ0FBZDtBQUF1QixHQUF0RDs7QUFDQSxNQUFJekIsRUFBRSxHQUFHLENBQUMsR0FBR3hFLE9BQU8sQ0FBQ00sUUFBWixFQUFzQixJQUFJb0YsU0FBUyxDQUFDM0ksT0FBZCxFQUF0QixDQUFUO0FBQUEsTUFBeURxSixPQUFPLEdBQUc1QixFQUFFLENBQUMsQ0FBRCxDQUFyRTtBQUFBLE1BQTBFNkIsVUFBVSxHQUFHN0IsRUFBRSxDQUFDLENBQUQsQ0FBekY7O0FBQ0EsTUFBSUcsRUFBRSxHQUFHLENBQUMsR0FBRzNFLE9BQU8sQ0FBQ00sUUFBWixFQUFzQixJQUFJcUYsY0FBYyxDQUFDM0ksWUFBbkIsRUFBdEIsQ0FBVDtBQUFBLE1BQW1Fc0osWUFBWSxHQUFHM0IsRUFBRSxDQUFDLENBQUQsQ0FBcEY7QUFBQSxNQUF5RjRCLGVBQWUsR0FBRzVCLEVBQUUsQ0FBQyxDQUFELENBQTdHOztBQUNBLFNBQVEsQ0FBQyxHQUFHL0UsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0I7QUFBRWYsWUFBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJnRSxRQUFRLENBQUMsU0FBRCxDQUEvQixFQUE0Q2pDLFFBQVEsQ0FBQztBQUFFaUQsYUFBTyxFQUFFLFNBQVg7QUFBc0JDLGFBQU8sRUFBRU47QUFBL0IsS0FBRCxFQUE4QztBQUFFMUUsY0FBUSxFQUFFO0FBQVosS0FBOUMsQ0FBcEQsQ0FBRCxFQUFtSSxDQUFDLEdBQUc3QixhQUFhLENBQUM0QyxJQUFsQixFQUF3QmlELE9BQU8sQ0FBQyxTQUFELENBQS9CLEVBQTRDbEMsUUFBUSxDQUFDO0FBQUV5QyxVQUFJLEVBQUVBLElBQVI7QUFBY1UsZ0JBQVUsRUFBRSxJQUExQjtBQUFnQ0MsWUFBTSxFQUFFVDtBQUF4QyxLQUFELEVBQXdEO0FBQUV6RSxjQUFRLEVBQUUsQ0FBQyxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QmlFLE9BQU8sQ0FBQyxTQUFELENBQVAsQ0FBbUJtQixNQUExQyxFQUFrRHJELFFBQVEsQ0FBQztBQUFFc0QsbUJBQVcsRUFBRTtBQUFmLE9BQUQsRUFBd0I7QUFBRXBGLGdCQUFRLEVBQUUsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJpRSxPQUFPLENBQUMsU0FBRCxDQUFQLENBQW1CcUIsS0FBMUMsRUFBaUQ7QUFBRXJGLGtCQUFRLEVBQUU7QUFBWixTQUFqRDtBQUFaLE9BQXhCLENBQTFELENBQUQsRUFBbUwsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0JpRCxPQUFPLENBQUMsU0FBRCxDQUFQLENBQW1Cc0IsSUFBM0MsRUFBaUQ7QUFBRXRGLGdCQUFRLEVBQUUsQ0FBQyxDQUFDLEdBQUc3QixhQUFhLENBQUM0QyxJQUFsQixFQUF3QixLQUF4QixFQUErQmUsUUFBUSxDQUFDO0FBQUUwQixtQkFBUyxFQUFFO0FBQWIsU0FBRCxFQUF1QjtBQUFFeEQsa0JBQVEsRUFBRSxDQUFDLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRDLElBQWxCLEVBQXdCLEtBQXhCLEVBQStCZSxRQUFRLENBQUM7QUFBRTBCLHFCQUFTLEVBQUU7QUFBYixXQUFELEVBQXlCO0FBQUV4RCxvQkFBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIsSUFBdkIsRUFBNkI7QUFBRUMsc0JBQVEsRUFBRTtBQUFaLGFBQTdCLENBQUQsRUFBNkQsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJvRSxvQkFBb0IsQ0FBQyxTQUFELENBQTNDLEVBQXdEO0FBQUVqSSxtQkFBSyxFQUFFMkksWUFBVDtBQUF1QjVGLGtCQUFJLEVBQUVBLElBQTdCO0FBQW1DVyxzQkFBUSxFQUFFa0Y7QUFBN0MsYUFBeEQsQ0FBN0Q7QUFBWixXQUF6QixDQUF2QyxDQUFELEVBQXdRLENBQUMsR0FBRzNHLGFBQWEsQ0FBQzRDLElBQWxCLEVBQXdCLEtBQXhCLEVBQStCZSxRQUFRLENBQUM7QUFBRTBCLHFCQUFTLEVBQUU7QUFBYixXQUFELEVBQXlCO0FBQUV4RCxvQkFBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIsSUFBdkIsRUFBNkI7QUFBRUMsc0JBQVEsRUFBRTtBQUFaLGFBQTdCLENBQUQsRUFBd0QsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJxRSxlQUFlLENBQUMsU0FBRCxDQUF0QyxFQUFtRDtBQUFFbEksbUJBQUssRUFBRXlJLE9BQVQ7QUFBa0IxRixrQkFBSSxFQUFFQSxJQUF4QjtBQUE4QjRGLDBCQUFZLEVBQUVBLFlBQTVDO0FBQTBEakYsc0JBQVEsRUFBRWdGO0FBQXBFLGFBQW5ELENBQXhEO0FBQVosV0FBekIsQ0FBdkMsQ0FBeFE7QUFBWixTQUF2QixDQUF2QyxDQUFELEVBQXVtQixDQUFDLEdBQUd6RyxhQUFhLENBQUM0QixHQUFsQixFQUF1QnNFLGFBQWEsQ0FBQyxTQUFELENBQXBDLEVBQWlEO0FBQUVNLGlCQUFPLEVBQUVBO0FBQVgsU0FBakQsQ0FBdm1CO0FBQVosT0FBakQsQ0FBbkw7QUFBWixLQUF4RCxDQUFwRCxDQUFuSTtBQUFaLEdBQS9CLENBQVI7QUFDSCxDQVhEOztBQVlBTCxvQkFBb0IsQ0FBQzlELFNBQXJCLEdBQWlDO0FBQzdCdkIsTUFBSSxFQUFFVCxTQUFTLENBQUNpQyxVQUFWLENBQXFCaEMsTUFBTSxDQUFDL0MsSUFBNUI7QUFEdUIsQ0FBakM7QUFHQTdDLE9BQU8sQ0FBQyxTQUFELENBQVAsR0FBcUJ5TCxvQkFBckIsQzs7Ozs7Ozs7Ozs7O0FDdkNhOztBQUNiekwsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCOztBQUNBLElBQUlxRixhQUFhLEdBQUdwRixtQkFBTyxDQUFDLHFHQUFELENBQTNCOztBQUNBLElBQUl3TSxzQkFBc0IsR0FBR3hNLG1CQUFPLENBQUMsdUhBQUQsQ0FBcEM7O0FBQ0EsSUFBSXlGLFNBQVMsR0FBR3pGLG1CQUFPLENBQUMsNkZBQUQsQ0FBdkI7O0FBQ0EsSUFBSTBGLE1BQU0sR0FBRzFGLG1CQUFPLENBQUMsb0ZBQUQsQ0FBcEI7O0FBQ0EsSUFBSXlNLGtCQUFrQixHQUFHLFNBQXJCQSxrQkFBcUIsQ0FBVTdHLEtBQVYsRUFBaUI7QUFDdEMsU0FBUSxDQUFDLEdBQUdSLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCNUIsYUFBYSxDQUFDc0gsUUFBckMsRUFBK0M7QUFBRXpGLFlBQVEsRUFBRSxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QndGLHNCQUFzQixDQUFDLFNBQUQsQ0FBN0MsRUFBMEQ7QUFBRXRHLFVBQUksRUFBRU4sS0FBSyxDQUFDTTtBQUFkLEtBQTFEO0FBQVosR0FBL0MsQ0FBUjtBQUNILENBRkQ7O0FBR0F1RyxrQkFBa0IsQ0FBQ2hGLFNBQW5CLEdBQStCO0FBQzNCdkIsTUFBSSxFQUFFVCxTQUFTLENBQUNpQyxVQUFWLENBQXFCaEMsTUFBTSxDQUFDL0MsSUFBNUI7QUFEcUIsQ0FBL0I7QUFHQTdDLE9BQU8sQ0FBQyxTQUFELENBQVAsR0FBcUIyTSxrQkFBckIsQzs7Ozs7Ozs7Ozs7O0FDWmE7Ozs7OztBQUNiLElBQUkxRCxRQUFRLEdBQUksUUFBUSxLQUFLQSxRQUFkLElBQTJCLFlBQVk7QUFDbERBLFVBQVEsR0FBR0MsTUFBTSxDQUFDQyxNQUFQLElBQWlCLFVBQVM5RSxDQUFULEVBQVk7QUFDcEMsU0FBSyxJQUFJK0UsQ0FBSixFQUFPQyxDQUFDLEdBQUcsQ0FBWCxFQUFjdkUsQ0FBQyxHQUFHd0UsU0FBUyxDQUFDbEUsTUFBakMsRUFBeUNpRSxDQUFDLEdBQUd2RSxDQUE3QyxFQUFnRHVFLENBQUMsRUFBakQsRUFBcUQ7QUFDakRELE9BQUMsR0FBR0UsU0FBUyxDQUFDRCxDQUFELENBQWI7O0FBQ0EsV0FBSyxJQUFJRSxDQUFULElBQWNILENBQWQ7QUFBaUIsWUFBSUYsTUFBTSxDQUFDOUgsU0FBUCxDQUFpQm9JLGNBQWpCLENBQWdDdEUsSUFBaEMsQ0FBcUNrRSxDQUFyQyxFQUF3Q0csQ0FBeEMsQ0FBSixFQUNibEYsQ0FBQyxDQUFDa0YsQ0FBRCxDQUFELEdBQU9ILENBQUMsQ0FBQ0csQ0FBRCxDQUFSO0FBREo7QUFFSDs7QUFDRCxXQUFPbEYsQ0FBUDtBQUNILEdBUEQ7O0FBUUEsU0FBTzRFLFFBQVEsQ0FBQ2pGLEtBQVQsQ0FBZSxJQUFmLEVBQXFCc0YsU0FBckIsQ0FBUDtBQUNILENBVkQ7O0FBV0F0SixPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7O0FBQ0EsSUFBSXFGLGFBQWEsR0FBR3BGLG1CQUFPLENBQUMscUdBQUQsQ0FBM0I7O0FBQ0EsSUFBSXlGLFNBQVMsR0FBR3pGLG1CQUFPLENBQUMsNkZBQUQsQ0FBdkI7O0FBQ0EsSUFBSTBGLE1BQU0sR0FBRzFGLG1CQUFPLENBQUMsb0ZBQUQsQ0FBcEI7O0FBQ0EsSUFBSWtMLFNBQVMsR0FBR2xMLG1CQUFPLENBQUMsMEZBQUQsQ0FBdkI7O0FBQ0EsSUFBSTJNLGdCQUFnQixHQUFHM00sbUJBQU8sQ0FBQyxtSUFBRCxDQUE5Qjs7QUFDQSxJQUFJNE0sTUFBTSxHQUFHNU0sbUJBQU8sQ0FBQyxxR0FBRCxDQUFwQjs7QUFDQSxJQUFJNk0sU0FBUyxHQUFHN00sbUJBQU8sQ0FBQyxnR0FBRCxDQUF2Qjs7QUFDQSxJQUFJd0YsT0FBTyxHQUFHeEYsbUJBQU8sQ0FBQyxtRkFBRCxDQUFyQjs7QUFDQSxJQUFJOE0sV0FBVyxHQUFHLFNBQWRBLFdBQWMsQ0FBVWxILEtBQVYsRUFBaUI7QUFDL0IsTUFBSUMsRUFBRSxHQUFHLENBQUMsR0FBR0wsT0FBTyxDQUFDTSxRQUFaLEVBQXNCLEVBQXRCLENBQVQ7QUFBQSxNQUFvQ2lILE1BQU0sR0FBR2xILEVBQUUsQ0FBQyxDQUFELENBQS9DO0FBQUEsTUFBb0RtSCxTQUFTLEdBQUduSCxFQUFFLENBQUMsQ0FBRCxDQUFsRTs7QUFDQSxNQUFJb0gsVUFBVSxHQUFHLFNBQWJBLFVBQWEsR0FBWTtBQUN6QixRQUFJckgsS0FBSyxDQUFDZ0csT0FBTixDQUFjL0osSUFBZCxJQUFzQixFQUExQixFQUE4QjtBQUMxQixhQUFPLEVBQVA7QUFDSDs7QUFDRCxRQUFJcUwsV0FBVyxHQUFHLElBQUlMLFNBQVMsQ0FBQ00sY0FBZCxFQUFsQjtBQUNBRCxlQUFXLENBQUNFLG1CQUFaLENBQWdDeEgsS0FBSyxDQUFDZ0csT0FBdEMsRUFBK0MvSCxJQUEvQyxDQUFvRCxVQUFVVixLQUFWLEVBQWlCO0FBQ2pFNkosZUFBUyxDQUFDN0osS0FBSyxDQUFDLFVBQUQsQ0FBTixDQUFUO0FBQ0gsS0FGRDtBQUdILEdBUkQ7O0FBU0EsR0FBQyxHQUFHcUMsT0FBTyxDQUFDUyxTQUFaLEVBQXVCLFlBQVk7QUFDL0JnSCxjQUFVO0FBQ2IsR0FGRCxFQUVHLENBQUNySCxLQUFLLENBQUNnRyxPQUFQLENBRkg7O0FBR0EsTUFBSXlCLEdBQUcsR0FBRyxTQUFOQSxHQUFNLENBQVV6RSxJQUFWLEVBQWdCO0FBQUUsV0FBTzBFLE9BQU8sQ0FBQ0QsR0FBUixDQUFZRSxJQUFaLENBQWlCRCxPQUFqQixFQUEwQjFFLElBQTFCLENBQVA7QUFBeUMsR0FBckU7O0FBQ0EsU0FBUSxDQUFDLEdBQUd4RCxhQUFhLENBQUM0QixHQUFsQixFQUF1QjVCLGFBQWEsQ0FBQ3NILFFBQXJDLEVBQStDO0FBQUV6RixZQUFRLEVBQUUsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0JlLFFBQVEsQ0FBQztBQUFFMEIsZUFBUyxFQUFFO0FBQWIsS0FBRCxFQUF1QjtBQUFFeEQsY0FBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIsS0FBdkIsRUFBOEIrQixRQUFRLENBQUM7QUFBRTBCLGlCQUFTLEVBQUU7QUFBYixPQUFELEVBQXlCO0FBQUV4RCxnQkFBUSxFQUFFLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCNEYsTUFBTSxDQUFDLFNBQUQsQ0FBN0IsRUFBMEM7QUFBRUcsZ0JBQU0sRUFBRUEsTUFBVjtBQUFrQlMsbUJBQVMsRUFBRWIsZ0JBQWdCLENBQUMsU0FBRCxDQUE3QztBQUEwRDlGLGtCQUFRLEVBQUV3RyxHQUFHLENBQUMsU0FBRCxDQUF2RTtBQUFvRkksa0JBQVEsRUFBRUosR0FBRyxDQUFDLFdBQUQsQ0FBakc7QUFBZ0hLLGlCQUFPLEVBQUVMLEdBQUcsQ0FBQyxRQUFEO0FBQTVILFNBQTFDO0FBQVosT0FBekIsQ0FBdEMsQ0FBRCxFQUFxUSxDQUFDLEdBQUdqSSxhQUFhLENBQUM0QyxJQUFsQixFQUF3QixLQUF4QixFQUErQmUsUUFBUSxDQUFDO0FBQUUwQixpQkFBUyxFQUFFO0FBQWIsT0FBRCxFQUF5QjtBQUFFeEQsZ0JBQVEsRUFBRSxDQUFDLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCLElBQXZCLEVBQTZCO0FBQUVDLGtCQUFRLEVBQUU7QUFBWixTQUE3QixDQUFELEVBQXdELENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCLElBQXZCLEVBQTZCO0FBQUVDLGtCQUFRLEVBQUU7QUFBWixTQUE3QixDQUF4RCxFQUErRyxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QixJQUF2QixFQUE2QjtBQUFFQyxrQkFBUSxFQUFFO0FBQVosU0FBN0IsQ0FBL0c7QUFBWixPQUF6QixDQUF2QyxDQUFyUTtBQUFaLEtBQXZCLENBQXZDO0FBQVosR0FBL0MsQ0FBUjtBQUNILENBaEJEOztBQWlCQTZGLFdBQVcsQ0FBQ3JGLFNBQVosR0FBd0I7QUFDcEJ2QixNQUFJLEVBQUVULFNBQVMsQ0FBQ2lDLFVBQVYsQ0FBcUJoQyxNQUFNLENBQUMvQyxJQUE1QixDQURjO0FBRXBCaUosU0FBTyxFQUFFbkcsU0FBUyxDQUFDaUMsVUFBVixDQUFxQndELFNBQVMsQ0FBQzNJLE9BQS9CO0FBRlcsQ0FBeEI7QUFJQXpDLE9BQU8sQ0FBQyxTQUFELENBQVAsR0FBcUJnTixXQUFyQixDOzs7Ozs7Ozs7Ozs7QUMxQ2E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNiLElBQUlqSyxTQUFTLEdBQUksUUFBUSxLQUFLQSxTQUFkLElBQTRCLFVBQVVDLE9BQVYsRUFBbUJDLFVBQW5CLEVBQStCQyxDQUEvQixFQUFrQ0MsU0FBbEMsRUFBNkM7QUFDckYsV0FBU0MsS0FBVCxDQUFlQyxLQUFmLEVBQXNCO0FBQUUsV0FBT0EsS0FBSyxZQUFZSCxDQUFqQixHQUFxQkcsS0FBckIsR0FBNkIsSUFBSUgsQ0FBSixDQUFNLFVBQVVqQyxPQUFWLEVBQW1CO0FBQUVBLGFBQU8sQ0FBQ29DLEtBQUQsQ0FBUDtBQUFpQixLQUE1QyxDQUFwQztBQUFvRjs7QUFDNUcsU0FBTyxLQUFLSCxDQUFDLEtBQUtBLENBQUMsR0FBR0ksT0FBVCxDQUFOLEVBQXlCLFVBQVVyQyxPQUFWLEVBQW1Cc0MsTUFBbkIsRUFBMkI7QUFDdkQsYUFBU0MsU0FBVCxDQUFtQkgsS0FBbkIsRUFBMEI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDTyxJQUFWLENBQWVMLEtBQWYsQ0FBRCxDQUFKO0FBQThCLE9BQXBDLENBQXFDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzNGLGFBQVNDLFFBQVQsQ0FBa0JQLEtBQWxCLEVBQXlCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQyxPQUFELENBQVQsQ0FBbUJFLEtBQW5CLENBQUQsQ0FBSjtBQUFrQyxPQUF4QyxDQUF5QyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUM5RixhQUFTRixJQUFULENBQWNJLE1BQWQsRUFBc0I7QUFBRUEsWUFBTSxDQUFDQyxJQUFQLEdBQWM3QyxPQUFPLENBQUM0QyxNQUFNLENBQUNSLEtBQVIsQ0FBckIsR0FBc0NELEtBQUssQ0FBQ1MsTUFBTSxDQUFDUixLQUFSLENBQUwsQ0FBb0JVLElBQXBCLENBQXlCUCxTQUF6QixFQUFvQ0ksUUFBcEMsQ0FBdEM7QUFBc0Y7O0FBQzlHSCxRQUFJLENBQUMsQ0FBQ04sU0FBUyxHQUFHQSxTQUFTLENBQUNhLEtBQVYsQ0FBZ0JoQixPQUFoQixFQUF5QkMsVUFBVSxJQUFJLEVBQXZDLENBQWIsRUFBeURTLElBQXpELEVBQUQsQ0FBSjtBQUNILEdBTE0sQ0FBUDtBQU1ILENBUkQ7O0FBU0EsSUFBSU8sV0FBVyxHQUFJLFFBQVEsS0FBS0EsV0FBZCxJQUE4QixVQUFVakIsT0FBVixFQUFtQmtCLElBQW5CLEVBQXlCO0FBQ3JFLE1BQUlDLENBQUMsR0FBRztBQUFFeEIsU0FBSyxFQUFFLENBQVQ7QUFBWXlCLFFBQUksRUFBRSxnQkFBVztBQUFFLFVBQUlDLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBTyxDQUFYLEVBQWMsTUFBTUEsQ0FBQyxDQUFDLENBQUQsQ0FBUDtBQUFZLGFBQU9BLENBQUMsQ0FBQyxDQUFELENBQVI7QUFBYyxLQUF2RTtBQUF5RUMsUUFBSSxFQUFFLEVBQS9FO0FBQW1GQyxPQUFHLEVBQUU7QUFBeEYsR0FBUjtBQUFBLE1BQXNHQyxDQUF0RztBQUFBLE1BQXlHQyxDQUF6RztBQUFBLE1BQTRHSixDQUE1RztBQUFBLE1BQStHSyxDQUEvRztBQUNBLFNBQU9BLENBQUMsR0FBRztBQUFFaEIsUUFBSSxFQUFFaUIsSUFBSSxDQUFDLENBQUQsQ0FBWjtBQUFpQixhQUFTQSxJQUFJLENBQUMsQ0FBRCxDQUE5QjtBQUFtQyxjQUFVQSxJQUFJLENBQUMsQ0FBRDtBQUFqRCxHQUFKLEVBQTRELE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsS0FBaUNGLENBQUMsQ0FBQ0UsTUFBTSxDQUFDQyxRQUFSLENBQUQsR0FBcUIsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLEdBQWpGLENBQTVELEVBQWdKSCxDQUF2Sjs7QUFDQSxXQUFTQyxJQUFULENBQWNHLENBQWQsRUFBaUI7QUFBRSxXQUFPLFVBQVVDLENBQVYsRUFBYTtBQUFFLGFBQU90QixJQUFJLENBQUMsQ0FBQ3FCLENBQUQsRUFBSUMsQ0FBSixDQUFELENBQVg7QUFBc0IsS0FBNUM7QUFBK0M7O0FBQ2xFLFdBQVN0QixJQUFULENBQWN1QixFQUFkLEVBQWtCO0FBQ2QsUUFBSVIsQ0FBSixFQUFPLE1BQU0sSUFBSVMsU0FBSixDQUFjLGlDQUFkLENBQU47O0FBQ1AsV0FBT2QsQ0FBUDtBQUFVLFVBQUk7QUFDVixZQUFJSyxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEtBQUtKLENBQUMsR0FBR1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVIsR0FBWVAsQ0FBQyxDQUFDLFFBQUQsQ0FBYixHQUEwQk8sRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRUCxDQUFDLENBQUMsT0FBRCxDQUFELEtBQWUsQ0FBQ0osQ0FBQyxHQUFHSSxDQUFDLENBQUMsUUFBRCxDQUFOLEtBQXFCSixDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxDQUFyQixFQUFnQyxDQUEvQyxDQUFSLEdBQTREQSxDQUFDLENBQUNmLElBQWpHLENBQUQsSUFBMkcsQ0FBQyxDQUFDVyxDQUFDLEdBQUdBLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLEVBQVVPLEVBQUUsQ0FBQyxDQUFELENBQVosQ0FBTCxFQUF1QmxCLElBQTlJLEVBQW9KLE9BQU9PLENBQVA7QUFDcEosWUFBSUksQ0FBQyxHQUFHLENBQUosRUFBT0osQ0FBWCxFQUFjVyxFQUFFLEdBQUcsQ0FBQ0EsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVQsRUFBWVgsQ0FBQyxDQUFDaEIsS0FBZCxDQUFMOztBQUNkLGdCQUFRMkIsRUFBRSxDQUFDLENBQUQsQ0FBVjtBQUNJLGVBQUssQ0FBTDtBQUFRLGVBQUssQ0FBTDtBQUFRWCxhQUFDLEdBQUdXLEVBQUo7QUFBUTs7QUFDeEIsZUFBSyxDQUFMO0FBQVFiLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVyxtQkFBTztBQUFFVSxtQkFBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBWDtBQUFnQmxCLGtCQUFJLEVBQUU7QUFBdEIsYUFBUDs7QUFDbkIsZUFBSyxDQUFMO0FBQVFLLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVzhCLGFBQUMsR0FBR08sRUFBRSxDQUFDLENBQUQsQ0FBTjtBQUFXQSxjQUFFLEdBQUcsQ0FBQyxDQUFELENBQUw7QUFBVTs7QUFDeEMsZUFBSyxDQUFMO0FBQVFBLGNBQUUsR0FBR2IsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU4sRUFBTDs7QUFBa0JoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYzs7QUFDeEM7QUFDSSxnQkFBSSxFQUFFZCxDQUFDLEdBQUdGLENBQUMsQ0FBQ0csSUFBTixFQUFZRCxDQUFDLEdBQUdBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVgsSUFBZ0JmLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWixDQUFuQyxNQUF1REosRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQWhGLENBQUosRUFBd0Y7QUFBRWIsZUFBQyxHQUFHLENBQUo7QUFBTztBQUFXOztBQUM1RyxnQkFBSWEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsS0FBZ0IsQ0FBQ1gsQ0FBRCxJQUFPVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQVQsSUFBZ0JXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBaEQsQ0FBSixFQUEyRDtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVVxQyxFQUFFLENBQUMsQ0FBRCxDQUFaO0FBQWlCO0FBQVE7O0FBQ3RGLGdCQUFJQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlYixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUE5QixFQUFtQztBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYO0FBQWdCQSxlQUFDLEdBQUdXLEVBQUo7QUFBUTtBQUFROztBQUNyRSxnQkFBSVgsQ0FBQyxJQUFJRixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFwQixFQUF5QjtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYOztBQUFnQkYsZUFBQyxDQUFDSSxHQUFGLENBQU1jLElBQU4sQ0FBV0wsRUFBWDs7QUFBZ0I7QUFBUTs7QUFDbkUsZ0JBQUlYLENBQUMsQ0FBQyxDQUFELENBQUwsRUFBVUYsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU47O0FBQ1ZoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYztBQVh0Qjs7QUFhQUgsVUFBRSxHQUFHZCxJQUFJLENBQUNnQixJQUFMLENBQVVsQyxPQUFWLEVBQW1CbUIsQ0FBbkIsQ0FBTDtBQUNILE9BakJTLENBaUJSLE9BQU9SLENBQVAsRUFBVTtBQUFFcUIsVUFBRSxHQUFHLENBQUMsQ0FBRCxFQUFJckIsQ0FBSixDQUFMO0FBQWFjLFNBQUMsR0FBRyxDQUFKO0FBQVEsT0FqQnpCLFNBaUJrQztBQUFFRCxTQUFDLEdBQUdILENBQUMsR0FBRyxDQUFSO0FBQVk7QUFqQjFEOztBQWtCQSxRQUFJVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBWixFQUFlLE1BQU1BLEVBQUUsQ0FBQyxDQUFELENBQVI7QUFBYSxXQUFPO0FBQUUzQixXQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFBLEVBQUUsQ0FBQyxDQUFELENBQVYsR0FBZ0IsS0FBSyxDQUE5QjtBQUFpQ2xCLFVBQUksRUFBRTtBQUF2QyxLQUFQO0FBQy9CO0FBQ0osQ0ExQkQ7O0FBMkJBOUQsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCOztBQUNBLElBQUlxRixhQUFhLEdBQUdwRixtQkFBTyxDQUFDLHFHQUFELENBQTNCOztBQUNBLElBQUl5RixTQUFTLEdBQUd6RixtQkFBTyxDQUFDLDZGQUFELENBQXZCOztBQUNBLElBQUkwRixNQUFNLEdBQUcxRixtQkFBTyxDQUFDLG9GQUFELENBQXBCOztBQUNBLElBQUlRLFVBQVUsR0FBR1IsbUJBQU8sQ0FBQyxtR0FBRCxDQUF4Qjs7QUFDQSxJQUFJdUYsT0FBTyxHQUFHdkYsbUJBQU8sQ0FBQyxzRkFBRCxDQUFyQjs7QUFDQSxJQUFJbUwsY0FBYyxHQUFHbkwsbUJBQU8sQ0FBQyxvR0FBRCxDQUE1Qjs7QUFDQSxJQUFJMk4sY0FBYyxHQUFHM04sbUJBQU8sQ0FBQywwR0FBRCxDQUE1Qjs7QUFDQSxJQUFJNE4sT0FBTyxHQUFHNU4sbUJBQU8sQ0FBQyw2SEFBRCxDQUFyQjs7QUFDQSxJQUFJNk4sa0JBQWtCLEdBQUcsU0FBckJBLGtCQUFxQixDQUFVakksS0FBVixFQUFpQjtBQUN0QyxNQUFJTyxLQUFLLEdBQUczRixVQUFVLENBQUNNLFNBQVgsQ0FBcUJDLE9BQXJCLENBQTZCd0UsT0FBTyxDQUFDYSxLQUFyQyxDQUFaOztBQUNBLE1BQUlDLFdBQVcsR0FBRyxTQUFkQSxXQUFjLENBQVVDLFdBQVYsRUFBdUI7QUFBRSxXQUFPekQsU0FBUyxDQUFDLEtBQUssQ0FBTixFQUFTLEtBQUssQ0FBZCxFQUFpQixLQUFLLENBQXRCLEVBQXlCLFlBQVk7QUFDNUYsVUFBSWlMLGdCQUFKO0FBQ0EsYUFBTy9KLFdBQVcsQ0FBQyxJQUFELEVBQU8sVUFBVThCLEVBQVYsRUFBYztBQUNuQyxnQkFBUUEsRUFBRSxDQUFDcEQsS0FBWDtBQUNJLGVBQUssQ0FBTDtBQUNJcUwsNEJBQWdCLEdBQUcsSUFBSUgsY0FBYyxDQUFDSSxtQkFBbkIsRUFBbkI7QUFDQSxtQkFBTyxDQUFDO0FBQUU7QUFBSCxjQUFjRCxnQkFBZ0IsQ0FBQ0UsZ0JBQWpCLENBQWtDMUgsV0FBbEMsRUFBK0NWLEtBQUssQ0FBQ00sSUFBckQsQ0FBZCxDQUFQOztBQUNKLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWVMLEVBQUUsQ0FBQzNCLElBQUgsRUFBZixDQUFQO0FBSlo7QUFNSCxPQVBpQixDQUFsQjtBQVFILEtBVjBELENBQWhCO0FBVXRDLEdBVkw7O0FBV0EsTUFBSTJDLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQVVDLE1BQVYsRUFBa0I7QUFDN0IsUUFBSSxPQUFPbEIsS0FBSyxDQUFDaUIsUUFBYixLQUEwQixVQUE5QixFQUEwQztBQUN0Q2pCLFdBQUssQ0FBQ2lCLFFBQU4sQ0FBZUMsTUFBZjtBQUNIO0FBQ0osR0FKRDs7QUFLQSxTQUFRLENBQUMsR0FBRzFCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCNEcsT0FBTyxDQUFDLFNBQUQsQ0FBOUIsRUFBMkM7QUFBRXpHLGtCQUFjLEVBQUUsSUFBbEI7QUFBd0JoRSxTQUFLLEVBQUV5QyxLQUFLLENBQUN6QyxLQUFyQztBQUE0Q2tELGVBQVcsRUFBRUEsV0FBekQ7QUFBc0VlLGtCQUFjLEVBQUUsd0JBQVVOLE1BQVYsRUFBa0I7QUFBRSxhQUFPQSxNQUFNLENBQUNqRixJQUFkO0FBQXFCLEtBQS9IO0FBQWlJZ0YsWUFBUSxFQUFFQSxRQUEzSTtBQUFxSlMsZ0JBQVksRUFBRTtBQUFuSyxHQUEzQyxFQUFzTkMsSUFBSSxDQUFDQyxTQUFMLENBQWU1QixLQUFLLENBQUNNLElBQU4sQ0FBV3JFLElBQTFCLENBQXROLENBQVI7QUFDSCxDQW5CRDs7QUFvQkFnTSxrQkFBa0IsQ0FBQ3BHLFNBQW5CLEdBQStCO0FBQzNCdkIsTUFBSSxFQUFFVCxTQUFTLENBQUNpQyxVQUFWLENBQXFCaEMsTUFBTSxDQUFDL0MsSUFBNUIsQ0FEcUI7QUFFM0JRLE9BQUssRUFBRXNDLFNBQVMsQ0FBQ2lDLFVBQVYsQ0FBcUJ5RCxjQUFjLENBQUMzSSxZQUFwQyxDQUZvQjtBQUczQnFFLFVBQVEsRUFBRXBCLFNBQVMsQ0FBQ3FEO0FBSE8sQ0FBL0I7QUFLQWhKLE9BQU8sQ0FBQyxTQUFELENBQVAsR0FBcUIrTixrQkFBckIsQzs7Ozs7Ozs7Ozs7O0FDdkVhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDYixJQUFJaEwsU0FBUyxHQUFJLFFBQVEsS0FBS0EsU0FBZCxJQUE0QixVQUFVQyxPQUFWLEVBQW1CQyxVQUFuQixFQUErQkMsQ0FBL0IsRUFBa0NDLFNBQWxDLEVBQTZDO0FBQ3JGLFdBQVNDLEtBQVQsQ0FBZUMsS0FBZixFQUFzQjtBQUFFLFdBQU9BLEtBQUssWUFBWUgsQ0FBakIsR0FBcUJHLEtBQXJCLEdBQTZCLElBQUlILENBQUosQ0FBTSxVQUFVakMsT0FBVixFQUFtQjtBQUFFQSxhQUFPLENBQUNvQyxLQUFELENBQVA7QUFBaUIsS0FBNUMsQ0FBcEM7QUFBb0Y7O0FBQzVHLFNBQU8sS0FBS0gsQ0FBQyxLQUFLQSxDQUFDLEdBQUdJLE9BQVQsQ0FBTixFQUF5QixVQUFVckMsT0FBVixFQUFtQnNDLE1BQW5CLEVBQTJCO0FBQ3ZELGFBQVNDLFNBQVQsQ0FBbUJILEtBQW5CLEVBQTBCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQ08sSUFBVixDQUFlTCxLQUFmLENBQUQsQ0FBSjtBQUE4QixPQUFwQyxDQUFxQyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUMzRixhQUFTQyxRQUFULENBQWtCUCxLQUFsQixFQUF5QjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUMsT0FBRCxDQUFULENBQW1CRSxLQUFuQixDQUFELENBQUo7QUFBa0MsT0FBeEMsQ0FBeUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDOUYsYUFBU0YsSUFBVCxDQUFjSSxNQUFkLEVBQXNCO0FBQUVBLFlBQU0sQ0FBQ0MsSUFBUCxHQUFjN0MsT0FBTyxDQUFDNEMsTUFBTSxDQUFDUixLQUFSLENBQXJCLEdBQXNDRCxLQUFLLENBQUNTLE1BQU0sQ0FBQ1IsS0FBUixDQUFMLENBQW9CVSxJQUFwQixDQUF5QlAsU0FBekIsRUFBb0NJLFFBQXBDLENBQXRDO0FBQXNGOztBQUM5R0gsUUFBSSxDQUFDLENBQUNOLFNBQVMsR0FBR0EsU0FBUyxDQUFDYSxLQUFWLENBQWdCaEIsT0FBaEIsRUFBeUJDLFVBQVUsSUFBSSxFQUF2QyxDQUFiLEVBQXlEUyxJQUF6RCxFQUFELENBQUo7QUFDSCxHQUxNLENBQVA7QUFNSCxDQVJEOztBQVNBLElBQUlPLFdBQVcsR0FBSSxRQUFRLEtBQUtBLFdBQWQsSUFBOEIsVUFBVWpCLE9BQVYsRUFBbUJrQixJQUFuQixFQUF5QjtBQUNyRSxNQUFJQyxDQUFDLEdBQUc7QUFBRXhCLFNBQUssRUFBRSxDQUFUO0FBQVl5QixRQUFJLEVBQUUsZ0JBQVc7QUFBRSxVQUFJQyxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQU8sQ0FBWCxFQUFjLE1BQU1BLENBQUMsQ0FBQyxDQUFELENBQVA7QUFBWSxhQUFPQSxDQUFDLENBQUMsQ0FBRCxDQUFSO0FBQWMsS0FBdkU7QUFBeUVDLFFBQUksRUFBRSxFQUEvRTtBQUFtRkMsT0FBRyxFQUFFO0FBQXhGLEdBQVI7QUFBQSxNQUFzR0MsQ0FBdEc7QUFBQSxNQUF5R0MsQ0FBekc7QUFBQSxNQUE0R0osQ0FBNUc7QUFBQSxNQUErR0ssQ0FBL0c7QUFDQSxTQUFPQSxDQUFDLEdBQUc7QUFBRWhCLFFBQUksRUFBRWlCLElBQUksQ0FBQyxDQUFELENBQVo7QUFBaUIsYUFBU0EsSUFBSSxDQUFDLENBQUQsQ0FBOUI7QUFBbUMsY0FBVUEsSUFBSSxDQUFDLENBQUQ7QUFBakQsR0FBSixFQUE0RCxPQUFPQyxNQUFQLEtBQWtCLFVBQWxCLEtBQWlDRixDQUFDLENBQUNFLE1BQU0sQ0FBQ0MsUUFBUixDQUFELEdBQXFCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxHQUFqRixDQUE1RCxFQUFnSkgsQ0FBdko7O0FBQ0EsV0FBU0MsSUFBVCxDQUFjRyxDQUFkLEVBQWlCO0FBQUUsV0FBTyxVQUFVQyxDQUFWLEVBQWE7QUFBRSxhQUFPdEIsSUFBSSxDQUFDLENBQUNxQixDQUFELEVBQUlDLENBQUosQ0FBRCxDQUFYO0FBQXNCLEtBQTVDO0FBQStDOztBQUNsRSxXQUFTdEIsSUFBVCxDQUFjdUIsRUFBZCxFQUFrQjtBQUNkLFFBQUlSLENBQUosRUFBTyxNQUFNLElBQUlTLFNBQUosQ0FBYyxpQ0FBZCxDQUFOOztBQUNQLFdBQU9kLENBQVA7QUFBVSxVQUFJO0FBQ1YsWUFBSUssQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxLQUFLSixDQUFDLEdBQUdXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFSLEdBQVlQLENBQUMsQ0FBQyxRQUFELENBQWIsR0FBMEJPLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVAsQ0FBQyxDQUFDLE9BQUQsQ0FBRCxLQUFlLENBQUNKLENBQUMsR0FBR0ksQ0FBQyxDQUFDLFFBQUQsQ0FBTixLQUFxQkosQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsQ0FBckIsRUFBZ0MsQ0FBL0MsQ0FBUixHQUE0REEsQ0FBQyxDQUFDZixJQUFqRyxDQUFELElBQTJHLENBQUMsQ0FBQ1csQ0FBQyxHQUFHQSxDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxFQUFVTyxFQUFFLENBQUMsQ0FBRCxDQUFaLENBQUwsRUFBdUJsQixJQUE5SSxFQUFvSixPQUFPTyxDQUFQO0FBQ3BKLFlBQUlJLENBQUMsR0FBRyxDQUFKLEVBQU9KLENBQVgsRUFBY1csRUFBRSxHQUFHLENBQUNBLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFULEVBQVlYLENBQUMsQ0FBQ2hCLEtBQWQsQ0FBTDs7QUFDZCxnQkFBUTJCLEVBQUUsQ0FBQyxDQUFELENBQVY7QUFDSSxlQUFLLENBQUw7QUFBUSxlQUFLLENBQUw7QUFBUVgsYUFBQyxHQUFHVyxFQUFKO0FBQVE7O0FBQ3hCLGVBQUssQ0FBTDtBQUFRYixhQUFDLENBQUN4QixLQUFGO0FBQVcsbUJBQU87QUFBRVUsbUJBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQVg7QUFBZ0JsQixrQkFBSSxFQUFFO0FBQXRCLGFBQVA7O0FBQ25CLGVBQUssQ0FBTDtBQUFRSyxhQUFDLENBQUN4QixLQUFGO0FBQVc4QixhQUFDLEdBQUdPLEVBQUUsQ0FBQyxDQUFELENBQU47QUFBV0EsY0FBRSxHQUFHLENBQUMsQ0FBRCxDQUFMO0FBQVU7O0FBQ3hDLGVBQUssQ0FBTDtBQUFRQSxjQUFFLEdBQUdiLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOLEVBQUw7O0FBQWtCaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7O0FBQ3hDO0FBQ0ksZ0JBQUksRUFBRWQsQ0FBQyxHQUFHRixDQUFDLENBQUNHLElBQU4sRUFBWUQsQ0FBQyxHQUFHQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFYLElBQWdCZixDQUFDLENBQUNBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVosQ0FBbkMsTUFBdURKLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWVBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFoRixDQUFKLEVBQXdGO0FBQUViLGVBQUMsR0FBRyxDQUFKO0FBQU87QUFBVzs7QUFDNUcsZ0JBQUlhLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLEtBQWdCLENBQUNYLENBQUQsSUFBT1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFULElBQWdCVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQWhELENBQUosRUFBMkQ7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVcUMsRUFBRSxDQUFDLENBQUQsQ0FBWjtBQUFpQjtBQUFROztBQUN0RixnQkFBSUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZWIsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBOUIsRUFBbUM7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDtBQUFnQkEsZUFBQyxHQUFHVyxFQUFKO0FBQVE7QUFBUTs7QUFDckUsZ0JBQUlYLENBQUMsSUFBSUYsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBcEIsRUFBeUI7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDs7QUFBZ0JGLGVBQUMsQ0FBQ0ksR0FBRixDQUFNYyxJQUFOLENBQVdMLEVBQVg7O0FBQWdCO0FBQVE7O0FBQ25FLGdCQUFJWCxDQUFDLENBQUMsQ0FBRCxDQUFMLEVBQVVGLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOOztBQUNWaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7QUFYdEI7O0FBYUFILFVBQUUsR0FBR2QsSUFBSSxDQUFDZ0IsSUFBTCxDQUFVbEMsT0FBVixFQUFtQm1CLENBQW5CLENBQUw7QUFDSCxPQWpCUyxDQWlCUixPQUFPUixDQUFQLEVBQVU7QUFBRXFCLFVBQUUsR0FBRyxDQUFDLENBQUQsRUFBSXJCLENBQUosQ0FBTDtBQUFhYyxTQUFDLEdBQUcsQ0FBSjtBQUFRLE9BakJ6QixTQWlCa0M7QUFBRUQsU0FBQyxHQUFHSCxDQUFDLEdBQUcsQ0FBUjtBQUFZO0FBakIxRDs7QUFrQkEsUUFBSVcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVosRUFBZSxNQUFNQSxFQUFFLENBQUMsQ0FBRCxDQUFSO0FBQWEsV0FBTztBQUFFM0IsV0FBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRQSxFQUFFLENBQUMsQ0FBRCxDQUFWLEdBQWdCLEtBQUssQ0FBOUI7QUFBaUNsQixVQUFJLEVBQUU7QUFBdkMsS0FBUDtBQUMvQjtBQUNKLENBMUJEOztBQTJCQTlELE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjs7QUFDQSxJQUFJcUYsYUFBYSxHQUFHcEYsbUJBQU8sQ0FBQyxxR0FBRCxDQUEzQjs7QUFDQSxJQUFJeUYsU0FBUyxHQUFHekYsbUJBQU8sQ0FBQyw2RkFBRCxDQUF2Qjs7QUFDQSxJQUFJMEYsTUFBTSxHQUFHMUYsbUJBQU8sQ0FBQyxvRkFBRCxDQUFwQjs7QUFDQSxJQUFJUSxVQUFVLEdBQUdSLG1CQUFPLENBQUMsbUdBQUQsQ0FBeEI7O0FBQ0EsSUFBSXVGLE9BQU8sR0FBR3ZGLG1CQUFPLENBQUMsc0ZBQUQsQ0FBckI7O0FBQ0EsSUFBSWtMLFNBQVMsR0FBR2xMLG1CQUFPLENBQUMsMEZBQUQsQ0FBdkI7O0FBQ0EsSUFBSW1MLGNBQWMsR0FBR25MLG1CQUFPLENBQUMsb0dBQUQsQ0FBNUI7O0FBQ0EsSUFBSTROLE9BQU8sR0FBRzVOLG1CQUFPLENBQUMsNkhBQUQsQ0FBckI7O0FBQ0EsSUFBSTZNLFNBQVMsR0FBRzdNLG1CQUFPLENBQUMsZ0dBQUQsQ0FBdkI7O0FBQ0EsU0FBU2lPLFFBQVQsQ0FBa0JDLEtBQWxCLEVBQXlCQyxFQUF6QixFQUE2QjtBQUN6QixNQUFJQSxFQUFFLEtBQUssS0FBSyxDQUFoQixFQUFtQjtBQUFFQSxNQUFFLEdBQUcsR0FBTDtBQUFXOztBQUNoQyxNQUFJQyxLQUFLLEdBQUcsSUFBWjtBQUNBLE1BQUlDLFFBQVEsR0FBRyxFQUFmO0FBQ0EsU0FBTyxZQUFZO0FBQ2YsUUFBSUMsSUFBSSxHQUFHLEVBQVg7O0FBQ0EsU0FBSyxJQUFJQyxFQUFFLEdBQUcsQ0FBZCxFQUFpQkEsRUFBRSxHQUFHbkYsU0FBUyxDQUFDbEUsTUFBaEMsRUFBd0NxSixFQUFFLEVBQTFDLEVBQThDO0FBQzFDRCxVQUFJLENBQUNDLEVBQUQsQ0FBSixHQUFXbkYsU0FBUyxDQUFDbUYsRUFBRCxDQUFwQjtBQUNILEtBSmMsQ0FLZjs7O0FBQ0FDLGdCQUFZLENBQUNKLEtBQUQsQ0FBWjtBQUNBQSxTQUFLLEdBQUdLLFVBQVUsQ0FBQyxZQUFZO0FBQzNCO0FBQ0E7QUFDQSxVQUFJOUssTUFBTSxHQUFHdUssS0FBSyxDQUFDcEssS0FBTixDQUFZLEtBQUssQ0FBakIsRUFBb0J3SyxJQUFwQixDQUFiO0FBQ0FELGNBQVEsQ0FBQ0ssT0FBVCxDQUFpQixVQUFVQyxDQUFWLEVBQWE7QUFBRSxlQUFPQSxDQUFDLENBQUNoTCxNQUFELENBQVI7QUFBbUIsT0FBbkQ7QUFDQTBLLGNBQVEsR0FBRyxFQUFYO0FBQ0gsS0FOaUIsRUFNZkYsRUFOZSxDQUFsQjtBQU9BLFdBQU8sSUFBSS9LLE9BQUosQ0FBWSxVQUFVdUwsQ0FBVixFQUFhO0FBQUUsYUFBT04sUUFBUSxDQUFDbEosSUFBVCxDQUFjd0osQ0FBZCxDQUFQO0FBQTBCLEtBQXJELENBQVA7QUFDSCxHQWZEO0FBZ0JIOztBQUNELElBQUlDLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsQ0FBVWhKLEtBQVYsRUFBaUI7QUFDakMsTUFBSU8sS0FBSyxHQUFHM0YsVUFBVSxDQUFDTSxTQUFYLENBQXFCQyxPQUFyQixDQUE2QndFLE9BQU8sQ0FBQ2EsS0FBckMsQ0FBWjtBQUNBLE1BQUlDLFdBQVcsR0FBRzRILFFBQVEsQ0FBQyxVQUFVM0gsV0FBVixFQUF1QjtBQUFFLFdBQU96RCxTQUFTLENBQUMsS0FBSyxDQUFOLEVBQVMsS0FBSyxDQUFkLEVBQWlCLEtBQUssQ0FBdEIsRUFBeUIsWUFBWTtBQUNyRyxVQUFJcUssV0FBSjtBQUNBLGFBQU9uSixXQUFXLENBQUMsSUFBRCxFQUFPLFVBQVU4QixFQUFWLEVBQWM7QUFDbkMsZ0JBQVFBLEVBQUUsQ0FBQ3BELEtBQVg7QUFDSSxlQUFLLENBQUw7QUFDSSxnQkFBSTZELFdBQVcsSUFBSSxFQUFmLElBQXFCVixLQUFLLENBQUNrRyxZQUFOLENBQW1CakssSUFBbkIsSUFBMkIsRUFBcEQsRUFBd0Q7QUFDcEQscUJBQU8sQ0FBQztBQUFFO0FBQUgsZ0JBQWUsRUFBZixDQUFQO0FBQ0g7O0FBQ0RxTCx1QkFBVyxHQUFHLElBQUlMLFNBQVMsQ0FBQ00sY0FBZCxFQUFkO0FBQ0EsZ0JBQUksRUFBRTdHLFdBQVcsSUFBSSxFQUFqQixDQUFKLEVBQTBCLE9BQU8sQ0FBQztBQUFFO0FBQUgsY0FBYyxDQUFkLENBQVA7QUFDMUIsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBYzRHLFdBQVcsQ0FBQzJCLGlCQUFaLENBQThCdkksV0FBOUIsRUFBMkNWLEtBQUssQ0FBQ00sSUFBakQsQ0FBZCxDQUFQOztBQUNKLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWVMLEVBQUUsQ0FBQzNCLElBQUgsRUFBZixDQUFQOztBQUNSLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWNnSixXQUFXLENBQUM0QixXQUFaLENBQXdCbEosS0FBSyxDQUFDa0csWUFBOUIsQ0FBZCxDQUFQOztBQUNSLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWVqRyxFQUFFLENBQUMzQixJQUFILEVBQWYsQ0FBUDtBQVZaO0FBWUgsT0FiaUIsQ0FBbEI7QUFjSCxLQWhCbUUsQ0FBaEI7QUFnQi9DLEdBaEJxQixDQUExQjs7QUFpQkEsTUFBSTJDLFFBQVEsR0FBRyxTQUFYQSxRQUFXLENBQVVDLE1BQVYsRUFBa0I7QUFDN0IsUUFBSSxPQUFPbEIsS0FBSyxDQUFDaUIsUUFBYixLQUEwQixVQUE5QixFQUEwQztBQUN0Q2pCLFdBQUssQ0FBQ2lCLFFBQU4sQ0FBZUMsTUFBZjtBQUNIO0FBQ0osR0FKRDs7QUFLQSxTQUFRLENBQUMsR0FBRzFCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCNEcsT0FBTyxDQUFDLFNBQUQsQ0FBOUIsRUFBMkM7QUFBRXpHLGtCQUFjLEVBQUUsSUFBbEI7QUFBd0JoRSxTQUFLLEVBQUV5QyxLQUFLLENBQUN6QyxLQUFyQztBQUE0Q2tELGVBQVcsRUFBRUEsV0FBekQ7QUFBc0VlLGtCQUFjLEVBQUUsd0JBQVVOLE1BQVYsRUFBa0I7QUFBRSxhQUFPQSxNQUFNLENBQUNqRixJQUFkO0FBQXFCLEtBQS9IO0FBQWlJd0Ysa0JBQWMsRUFBRSx3QkFBVVAsTUFBVixFQUFrQjtBQUFFLGFBQU9BLE1BQU0sQ0FBQzlFLEtBQWQ7QUFBc0IsS0FBM0w7QUFBNkw2RSxZQUFRLEVBQUVBLFFBQXZNO0FBQWlOUyxnQkFBWSxFQUFFO0FBQS9OLEdBQTNDLEVBQWtSQyxJQUFJLENBQUNDLFNBQUwsQ0FBZTVCLEtBQUssQ0FBQ2tHLFlBQU4sQ0FBbUJqSyxJQUFsQyxDQUFsUixDQUFSO0FBQ0gsQ0F6QkQ7O0FBMEJBK00sYUFBYSxDQUFDbkgsU0FBZCxHQUEwQjtBQUN0QnZCLE1BQUksRUFBRVQsU0FBUyxDQUFDaUMsVUFBVixDQUFxQmhDLE1BQU0sQ0FBQy9DLElBQTVCLENBRGdCO0FBRXRCUSxPQUFLLEVBQUVzQyxTQUFTLENBQUNpQyxVQUFWLENBQXFCd0QsU0FBUyxDQUFDM0ksT0FBL0IsQ0FGZTtBQUd0QnVKLGNBQVksRUFBRXJHLFNBQVMsQ0FBQ2lDLFVBQVYsQ0FBcUJ5RCxjQUFjLENBQUMzSSxZQUFwQyxDQUhRO0FBSXRCcUUsVUFBUSxFQUFFcEIsU0FBUyxDQUFDcUQ7QUFKRSxDQUExQjtBQU1BaEosT0FBTyxDQUFDLFNBQUQsQ0FBUCxHQUFxQjhPLGFBQXJCLEM7Ozs7Ozs7Ozs7OztBQ3BHYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDYixJQUFJL0wsU0FBUyxHQUFJLFFBQVEsS0FBS0EsU0FBZCxJQUE0QixVQUFVQyxPQUFWLEVBQW1CQyxVQUFuQixFQUErQkMsQ0FBL0IsRUFBa0NDLFNBQWxDLEVBQTZDO0FBQ3JGLFdBQVNDLEtBQVQsQ0FBZUMsS0FBZixFQUFzQjtBQUFFLFdBQU9BLEtBQUssWUFBWUgsQ0FBakIsR0FBcUJHLEtBQXJCLEdBQTZCLElBQUlILENBQUosQ0FBTSxVQUFVakMsT0FBVixFQUFtQjtBQUFFQSxhQUFPLENBQUNvQyxLQUFELENBQVA7QUFBaUIsS0FBNUMsQ0FBcEM7QUFBb0Y7O0FBQzVHLFNBQU8sS0FBS0gsQ0FBQyxLQUFLQSxDQUFDLEdBQUdJLE9BQVQsQ0FBTixFQUF5QixVQUFVckMsT0FBVixFQUFtQnNDLE1BQW5CLEVBQTJCO0FBQ3ZELGFBQVNDLFNBQVQsQ0FBbUJILEtBQW5CLEVBQTBCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQ08sSUFBVixDQUFlTCxLQUFmLENBQUQsQ0FBSjtBQUE4QixPQUFwQyxDQUFxQyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUMzRixhQUFTQyxRQUFULENBQWtCUCxLQUFsQixFQUF5QjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUMsT0FBRCxDQUFULENBQW1CRSxLQUFuQixDQUFELENBQUo7QUFBa0MsT0FBeEMsQ0FBeUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDOUYsYUFBU0YsSUFBVCxDQUFjSSxNQUFkLEVBQXNCO0FBQUVBLFlBQU0sQ0FBQ0MsSUFBUCxHQUFjN0MsT0FBTyxDQUFDNEMsTUFBTSxDQUFDUixLQUFSLENBQXJCLEdBQXNDRCxLQUFLLENBQUNTLE1BQU0sQ0FBQ1IsS0FBUixDQUFMLENBQW9CVSxJQUFwQixDQUF5QlAsU0FBekIsRUFBb0NJLFFBQXBDLENBQXRDO0FBQXNGOztBQUM5R0gsUUFBSSxDQUFDLENBQUNOLFNBQVMsR0FBR0EsU0FBUyxDQUFDYSxLQUFWLENBQWdCaEIsT0FBaEIsRUFBeUJDLFVBQVUsSUFBSSxFQUF2QyxDQUFiLEVBQXlEUyxJQUF6RCxFQUFELENBQUo7QUFDSCxHQUxNLENBQVA7QUFNSCxDQVJEOztBQVNBLElBQUlPLFdBQVcsR0FBSSxRQUFRLEtBQUtBLFdBQWQsSUFBOEIsVUFBVWpCLE9BQVYsRUFBbUJrQixJQUFuQixFQUF5QjtBQUNyRSxNQUFJQyxDQUFDLEdBQUc7QUFBRXhCLFNBQUssRUFBRSxDQUFUO0FBQVl5QixRQUFJLEVBQUUsZ0JBQVc7QUFBRSxVQUFJQyxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQU8sQ0FBWCxFQUFjLE1BQU1BLENBQUMsQ0FBQyxDQUFELENBQVA7QUFBWSxhQUFPQSxDQUFDLENBQUMsQ0FBRCxDQUFSO0FBQWMsS0FBdkU7QUFBeUVDLFFBQUksRUFBRSxFQUEvRTtBQUFtRkMsT0FBRyxFQUFFO0FBQXhGLEdBQVI7QUFBQSxNQUFzR0MsQ0FBdEc7QUFBQSxNQUF5R0MsQ0FBekc7QUFBQSxNQUE0R0osQ0FBNUc7QUFBQSxNQUErR0ssQ0FBL0c7QUFDQSxTQUFPQSxDQUFDLEdBQUc7QUFBRWhCLFFBQUksRUFBRWlCLElBQUksQ0FBQyxDQUFELENBQVo7QUFBaUIsYUFBU0EsSUFBSSxDQUFDLENBQUQsQ0FBOUI7QUFBbUMsY0FBVUEsSUFBSSxDQUFDLENBQUQ7QUFBakQsR0FBSixFQUE0RCxPQUFPQyxNQUFQLEtBQWtCLFVBQWxCLEtBQWlDRixDQUFDLENBQUNFLE1BQU0sQ0FBQ0MsUUFBUixDQUFELEdBQXFCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxHQUFqRixDQUE1RCxFQUFnSkgsQ0FBdko7O0FBQ0EsV0FBU0MsSUFBVCxDQUFjRyxDQUFkLEVBQWlCO0FBQUUsV0FBTyxVQUFVQyxDQUFWLEVBQWE7QUFBRSxhQUFPdEIsSUFBSSxDQUFDLENBQUNxQixDQUFELEVBQUlDLENBQUosQ0FBRCxDQUFYO0FBQXNCLEtBQTVDO0FBQStDOztBQUNsRSxXQUFTdEIsSUFBVCxDQUFjdUIsRUFBZCxFQUFrQjtBQUNkLFFBQUlSLENBQUosRUFBTyxNQUFNLElBQUlTLFNBQUosQ0FBYyxpQ0FBZCxDQUFOOztBQUNQLFdBQU9kLENBQVA7QUFBVSxVQUFJO0FBQ1YsWUFBSUssQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxLQUFLSixDQUFDLEdBQUdXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFSLEdBQVlQLENBQUMsQ0FBQyxRQUFELENBQWIsR0FBMEJPLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVAsQ0FBQyxDQUFDLE9BQUQsQ0FBRCxLQUFlLENBQUNKLENBQUMsR0FBR0ksQ0FBQyxDQUFDLFFBQUQsQ0FBTixLQUFxQkosQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsQ0FBckIsRUFBZ0MsQ0FBL0MsQ0FBUixHQUE0REEsQ0FBQyxDQUFDZixJQUFqRyxDQUFELElBQTJHLENBQUMsQ0FBQ1csQ0FBQyxHQUFHQSxDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxFQUFVTyxFQUFFLENBQUMsQ0FBRCxDQUFaLENBQUwsRUFBdUJsQixJQUE5SSxFQUFvSixPQUFPTyxDQUFQO0FBQ3BKLFlBQUlJLENBQUMsR0FBRyxDQUFKLEVBQU9KLENBQVgsRUFBY1csRUFBRSxHQUFHLENBQUNBLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFULEVBQVlYLENBQUMsQ0FBQ2hCLEtBQWQsQ0FBTDs7QUFDZCxnQkFBUTJCLEVBQUUsQ0FBQyxDQUFELENBQVY7QUFDSSxlQUFLLENBQUw7QUFBUSxlQUFLLENBQUw7QUFBUVgsYUFBQyxHQUFHVyxFQUFKO0FBQVE7O0FBQ3hCLGVBQUssQ0FBTDtBQUFRYixhQUFDLENBQUN4QixLQUFGO0FBQVcsbUJBQU87QUFBRVUsbUJBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQVg7QUFBZ0JsQixrQkFBSSxFQUFFO0FBQXRCLGFBQVA7O0FBQ25CLGVBQUssQ0FBTDtBQUFRSyxhQUFDLENBQUN4QixLQUFGO0FBQVc4QixhQUFDLEdBQUdPLEVBQUUsQ0FBQyxDQUFELENBQU47QUFBV0EsY0FBRSxHQUFHLENBQUMsQ0FBRCxDQUFMO0FBQVU7O0FBQ3hDLGVBQUssQ0FBTDtBQUFRQSxjQUFFLEdBQUdiLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOLEVBQUw7O0FBQWtCaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7O0FBQ3hDO0FBQ0ksZ0JBQUksRUFBRWQsQ0FBQyxHQUFHRixDQUFDLENBQUNHLElBQU4sRUFBWUQsQ0FBQyxHQUFHQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFYLElBQWdCZixDQUFDLENBQUNBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVosQ0FBbkMsTUFBdURKLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWVBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFoRixDQUFKLEVBQXdGO0FBQUViLGVBQUMsR0FBRyxDQUFKO0FBQU87QUFBVzs7QUFDNUcsZ0JBQUlhLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLEtBQWdCLENBQUNYLENBQUQsSUFBT1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFULElBQWdCVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQWhELENBQUosRUFBMkQ7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVcUMsRUFBRSxDQUFDLENBQUQsQ0FBWjtBQUFpQjtBQUFROztBQUN0RixnQkFBSUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZWIsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBOUIsRUFBbUM7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDtBQUFnQkEsZUFBQyxHQUFHVyxFQUFKO0FBQVE7QUFBUTs7QUFDckUsZ0JBQUlYLENBQUMsSUFBSUYsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBcEIsRUFBeUI7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDs7QUFBZ0JGLGVBQUMsQ0FBQ0ksR0FBRixDQUFNYyxJQUFOLENBQVdMLEVBQVg7O0FBQWdCO0FBQVE7O0FBQ25FLGdCQUFJWCxDQUFDLENBQUMsQ0FBRCxDQUFMLEVBQVVGLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOOztBQUNWaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7QUFYdEI7O0FBYUFILFVBQUUsR0FBR2QsSUFBSSxDQUFDZ0IsSUFBTCxDQUFVbEMsT0FBVixFQUFtQm1CLENBQW5CLENBQUw7QUFDSCxPQWpCUyxDQWlCUixPQUFPUixDQUFQLEVBQVU7QUFBRXFCLFVBQUUsR0FBRyxDQUFDLENBQUQsRUFBSXJCLENBQUosQ0FBTDtBQUFhYyxTQUFDLEdBQUcsQ0FBSjtBQUFRLE9BakJ6QixTQWlCa0M7QUFBRUQsU0FBQyxHQUFHSCxDQUFDLEdBQUcsQ0FBUjtBQUFZO0FBakIxRDs7QUFrQkEsUUFBSVcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVosRUFBZSxNQUFNQSxFQUFFLENBQUMsQ0FBRCxDQUFSO0FBQWEsV0FBTztBQUFFM0IsV0FBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRQSxFQUFFLENBQUMsQ0FBRCxDQUFWLEdBQWdCLEtBQUssQ0FBOUI7QUFBaUNsQixVQUFJLEVBQUU7QUFBdkMsS0FBUDtBQUMvQjtBQUNKLENBMUJEOztBQTJCQTlELE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjs7QUFDQSxJQUFJcUYsYUFBYSxHQUFHcEYsbUJBQU8sQ0FBQyxxR0FBRCxDQUEzQjs7QUFDQSxJQUFJcUYsNkJBQTZCLEdBQUdyRixtQkFBTyxDQUFDLGtJQUFELENBQTNDOztBQUNBLElBQUkwRixNQUFNLEdBQUcxRixtQkFBTyxDQUFDLDBGQUFELENBQXBCOztBQUNBLElBQUlRLFVBQVUsR0FBR1IsbUJBQU8sQ0FBQyxtR0FBRCxDQUF4Qjs7QUFDQSxJQUFJdUYsT0FBTyxHQUFHdkYsbUJBQU8sQ0FBQyxzRkFBRCxDQUFyQjs7QUFDQSxJQUFJK08sbUJBQW1CLEdBQUcsU0FBdEJBLG1CQUFzQixDQUFVbkosS0FBVixFQUFpQjtBQUN2QyxNQUFJTyxLQUFLLEdBQUczRixVQUFVLENBQUNNLFNBQVgsQ0FBcUJDLE9BQXJCLENBQTZCd0UsT0FBTyxDQUFDYSxLQUFyQyxDQUFaOztBQUNBLE1BQUlDLFdBQVcsR0FBRyxTQUFkQSxXQUFjLENBQVVDLFdBQVYsRUFBdUJDLGFBQXZCLEVBQXNDO0FBQUUsV0FBTzFELFNBQVMsQ0FBQyxLQUFLLENBQU4sRUFBUyxLQUFLLENBQWQsRUFBaUIsS0FBSyxDQUF0QixFQUF5QixZQUFZO0FBQzNHLFVBQUltTSxZQUFKLEVBQWtCdkksSUFBbEI7QUFDQSxhQUFPMUMsV0FBVyxDQUFDLElBQUQsRUFBTyxVQUFVOEIsRUFBVixFQUFjO0FBQ25DLGdCQUFRQSxFQUFFLENBQUNwRCxLQUFYO0FBQ0ksZUFBSyxDQUFMO0FBQ0l1TSx3QkFBWSxHQUFHLElBQUl0SixNQUFNLENBQUN1SixXQUFYLEVBQWY7QUFDQSxtQkFBTyxDQUFDO0FBQUU7QUFBSCxjQUFjRCxZQUFZLENBQUNFLFFBQWIsRUFBZCxDQUFQOztBQUNKLGVBQUssQ0FBTDtBQUNJekksZ0JBQUksR0FBR1osRUFBRSxDQUFDM0IsSUFBSCxFQUFQO0FBQ0EsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBZTtBQUNkeEIscUJBQU8sRUFBRStELElBREs7QUFFZEcscUJBQU8sRUFBRTtBQUZLLGFBQWYsQ0FBUDtBQU5SO0FBV0gsT0FaaUIsQ0FBbEI7QUFhSCxLQWZ5RSxDQUFoQjtBQWVyRCxHQWZMOztBQWdCQSxNQUFJQyxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFVQyxNQUFWLEVBQWtCO0FBQzdCWCxTQUFLLENBQUNELElBQU4sR0FBYVksTUFBTSxDQUFDbEYsRUFBcEI7QUFDQWdFLFNBQUssQ0FBQ3FDLE1BQU4sQ0FBYW5CLE1BQWI7QUFDSCxHQUhEOztBQUlBLFNBQVEsQ0FBQyxHQUFHMUIsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIsS0FBdkIsRUFBOEI7QUFBRUMsWUFBUSxFQUFFLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCM0IsNkJBQTZCLENBQUM2QixhQUFyRCxFQUFvRTtBQUFFQyxvQkFBYyxFQUFFLElBQWxCO0FBQXdCZCxpQkFBVyxFQUFFQSxXQUFyQztBQUFrRGUsb0JBQWMsRUFBRSx3QkFBVU4sTUFBVixFQUFrQjtBQUFFLGVBQU9BLE1BQU0sQ0FBQ2xGLEVBQWQ7QUFBbUIsT0FBekc7QUFBMkd5RixvQkFBYyxFQUFFLHdCQUFVUCxNQUFWLEVBQWtCO0FBQUUsZUFBT0EsTUFBTSxDQUFDbEUsSUFBZDtBQUFxQixPQUFwSztBQUFzS2lFLGNBQVEsRUFBRUEsUUFBaEw7QUFBMExTLGtCQUFZLEVBQUU7QUFBeE0sS0FBcEU7QUFBWixHQUE5QixDQUFSO0FBQ0gsQ0F2QkQ7O0FBd0JBeEgsT0FBTyxDQUFDLFNBQUQsQ0FBUCxHQUFxQmlQLG1CQUFyQixDOzs7Ozs7Ozs7Ozs7QUNuRWE7Ozs7QUFDYixJQUFJaEcsUUFBUSxHQUFJLFFBQVEsS0FBS0EsUUFBZCxJQUEyQixZQUFZO0FBQ2xEQSxVQUFRLEdBQUdDLE1BQU0sQ0FBQ0MsTUFBUCxJQUFpQixVQUFTOUUsQ0FBVCxFQUFZO0FBQ3BDLFNBQUssSUFBSStFLENBQUosRUFBT0MsQ0FBQyxHQUFHLENBQVgsRUFBY3ZFLENBQUMsR0FBR3dFLFNBQVMsQ0FBQ2xFLE1BQWpDLEVBQXlDaUUsQ0FBQyxHQUFHdkUsQ0FBN0MsRUFBZ0R1RSxDQUFDLEVBQWpELEVBQXFEO0FBQ2pERCxPQUFDLEdBQUdFLFNBQVMsQ0FBQ0QsQ0FBRCxDQUFiOztBQUNBLFdBQUssSUFBSUUsQ0FBVCxJQUFjSCxDQUFkO0FBQWlCLFlBQUlGLE1BQU0sQ0FBQzlILFNBQVAsQ0FBaUJvSSxjQUFqQixDQUFnQ3RFLElBQWhDLENBQXFDa0UsQ0FBckMsRUFBd0NHLENBQXhDLENBQUosRUFDYmxGLENBQUMsQ0FBQ2tGLENBQUQsQ0FBRCxHQUFPSCxDQUFDLENBQUNHLENBQUQsQ0FBUjtBQURKO0FBRUg7O0FBQ0QsV0FBT2xGLENBQVA7QUFDSCxHQVBEOztBQVFBLFNBQU80RSxRQUFRLENBQUNqRixLQUFULENBQWUsSUFBZixFQUFxQnNGLFNBQXJCLENBQVA7QUFDSCxDQVZEOztBQVdBdEosT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCOztBQUNBLElBQUlxRixhQUFhLEdBQUdwRixtQkFBTyxDQUFDLHFHQUFELENBQTNCOztBQUNBLElBQUl1RixPQUFPLEdBQUd2RixtQkFBTyxDQUFDLHNGQUFELENBQXJCOztBQUNBLElBQUlRLFVBQVUsR0FBR1IsbUJBQU8sQ0FBQyxtR0FBRCxDQUF4Qjs7QUFDQSxJQUFJbVAsT0FBTyxHQUFHblAsbUJBQU8sQ0FBQyw0RkFBRCxDQUFyQjs7QUFDQSxJQUFJb1AscUJBQXFCLEdBQUdwUCxtQkFBTyxDQUFDLHNIQUFELENBQW5DOztBQUNBLElBQUlxUCxxQkFBcUIsR0FBR3JQLG1CQUFPLENBQUMsc0hBQUQsQ0FBbkM7O0FBQ0EsSUFBSXNQLGVBQWUsR0FBRyxTQUFsQkEsZUFBa0IsQ0FBVTFKLEtBQVYsRUFBaUI7QUFDbkMsTUFBSTJKLFlBQVksR0FBRy9PLFVBQVUsQ0FBQ00sU0FBWCxDQUFxQkMsT0FBckIsQ0FBNkJvTyxPQUFPLENBQUNLLFlBQXJDLENBQW5CO0FBQ0EsTUFBSXJKLEtBQUssR0FBRzNGLFVBQVUsQ0FBQ00sU0FBWCxDQUFxQkMsT0FBckIsQ0FBNkJ3RSxPQUFPLENBQUNhLEtBQXJDLENBQVo7O0FBQ0EsTUFBSXFKLFVBQVUsR0FBRyxTQUFiQSxVQUFhLENBQVVoTSxDQUFWLEVBQWE7QUFDMUJBLEtBQUMsQ0FBQ2lNLGNBQUY7QUFDQUgsZ0JBQVksQ0FBQ0ksU0FBYixDQUF1QnhKLEtBQXZCO0FBQ0gsR0FIRDs7QUFJQSxTQUFRLENBQUMsR0FBR2YsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0JlLFFBQVEsQ0FBQztBQUFFMEIsYUFBUyxFQUFFO0FBQWIsR0FBRCxFQUF1QjtBQUFFeEQsWUFBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEMsSUFBbEIsRUFBd0IsS0FBeEIsRUFBK0JlLFFBQVEsQ0FBQztBQUFFMEIsZUFBUyxFQUFFO0FBQWIsS0FBRCxFQUF5QjtBQUFFeEQsY0FBUSxFQUFFLENBQUMsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUIsSUFBdkIsRUFBNkI7QUFBRUMsZ0JBQVEsRUFBRTtBQUFaLE9BQTdCLENBQUQsRUFBb0QsR0FBcEQsRUFBeUQsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUJvSSxxQkFBcUIsQ0FBQyxTQUFELENBQTVDLEVBQXlELEVBQXpELENBQXpEO0FBQVosS0FBekIsQ0FBdkMsQ0FBRCxFQUF5TSxDQUFDLEdBQUdoSyxhQUFhLENBQUM0QyxJQUFsQixFQUF3QixLQUF4QixFQUErQmUsUUFBUSxDQUFDO0FBQUUwQixlQUFTLEVBQUU7QUFBYixLQUFELEVBQXlCO0FBQUV4RCxjQUFRLEVBQUUsQ0FBQyxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QixJQUF2QixFQUE2QjtBQUFFQyxnQkFBUSxFQUFFO0FBQVosT0FBN0IsQ0FBRCxFQUFxRCxHQUFyRCxFQUEwRCxDQUFDLEdBQUc3QixhQUFhLENBQUM0QixHQUFsQixFQUF1QnFJLHFCQUFxQixDQUFDLFNBQUQsQ0FBNUMsRUFBeUQ7QUFBRW5KLFlBQUksRUFBRU4sS0FBSyxDQUFDTSxJQUFkO0FBQW9CK0IsY0FBTSxFQUFFckMsS0FBSyxDQUFDcUM7QUFBbEMsT0FBekQsQ0FBMUQ7QUFBWixLQUF6QixDQUF2QyxDQUF6TSxFQUEwYixDQUFDLEdBQUc3QyxhQUFhLENBQUM0QixHQUFsQixFQUF1QixLQUF2QixFQUE4QitCLFFBQVEsQ0FBQztBQUFFMEIsZUFBUyxFQUFFO0FBQWIsS0FBRCxFQUF5QjtBQUFFeEQsY0FBUSxFQUFFLENBQUMsR0FBRzdCLGFBQWEsQ0FBQzRCLEdBQWxCLEVBQXVCLFFBQXZCLEVBQWlDK0IsUUFBUSxDQUFDO0FBQUUwQixpQkFBUyxFQUFFLGlCQUFiO0FBQWdDd0IsZUFBTyxFQUFFd0Q7QUFBekMsT0FBRCxFQUF3RDtBQUFFeEksZ0JBQVEsRUFBRTtBQUFaLE9BQXhELENBQXpDO0FBQVosS0FBekIsQ0FBdEMsQ0FBMWI7QUFBWixHQUF2QixDQUF2QyxDQUFSO0FBQ0gsQ0FSRDs7QUFTQW5ILE9BQU8sQ0FBQyxTQUFELENBQVAsR0FBcUJ3UCxlQUFyQixDOzs7Ozs7Ozs7Ozs7QUM1QmE7O0FBQ2J4UCxPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7O0FBQ0EsSUFBSXFGLGFBQWEsR0FBR3BGLG1CQUFPLENBQUMscUdBQUQsQ0FBM0I7O0FBQ0EsSUFBSTRQLGNBQWMsR0FBRzVQLG1CQUFPLENBQUMsaUhBQUQsQ0FBNUI7O0FBQ0EsSUFBSXdGLE9BQU8sR0FBR3hGLG1CQUFPLENBQUMsbUZBQUQsQ0FBckI7O0FBQ0EsSUFBSVEsVUFBVSxHQUFHUixtQkFBTyxDQUFDLG1HQUFELENBQXhCOztBQUNBLElBQUl1RixPQUFPLEdBQUd2RixtQkFBTyxDQUFDLHNGQUFELENBQXJCOztBQUNBLElBQUk2UCxtQkFBbUIsR0FBRyxTQUF0QkEsbUJBQXNCLENBQVVqSyxLQUFWLEVBQWlCO0FBQ3ZDLE1BQUlDLEVBQUUsR0FBRyxDQUFDLEdBQUdMLE9BQU8sQ0FBQ00sUUFBWixHQUFUO0FBQUEsTUFBa0M4QyxJQUFJLEdBQUcvQyxFQUFFLENBQUMsQ0FBRCxDQUEzQztBQUFBLE1BQWdEaUssT0FBTyxHQUFHakssRUFBRSxDQUFDLENBQUQsQ0FBNUQ7O0FBQ0EsTUFBSU0sS0FBSyxHQUFHM0YsVUFBVSxDQUFDTSxTQUFYLENBQXFCQyxPQUFyQixDQUE2QndFLE9BQU8sQ0FBQ2EsS0FBckMsQ0FBWjtBQUNBLE1BQUkxRCxPQUFPLEdBQUcsQ0FDVjtBQUFFUyxTQUFLLEVBQUUsQ0FBVDtBQUFZVixTQUFLLEVBQUU7QUFBbkIsR0FEVSxFQUVWO0FBQUVVLFNBQUssRUFBRSxDQUFUO0FBQVlWLFNBQUssRUFBRTtBQUFuQixHQUZVLEVBR1Y7QUFBRVUsU0FBSyxFQUFFLENBQVQ7QUFBWVYsU0FBSyxFQUFFO0FBQW5CLEdBSFUsRUFJVjtBQUFFVSxTQUFLLEVBQUUsQ0FBVDtBQUFZVixTQUFLLEVBQUU7QUFBbkIsR0FKVSxFQUtWO0FBQUVVLFNBQUssRUFBRSxDQUFUO0FBQVlWLFNBQUssRUFBRTtBQUFuQixHQUxVLEVBTVY7QUFBRVUsU0FBSyxFQUFFLENBQVQ7QUFBWVYsU0FBSyxFQUFFO0FBQW5CLEdBTlUsRUFPVjtBQUFFVSxTQUFLLEVBQUUsQ0FBVDtBQUFZVixTQUFLLEVBQUU7QUFBbkIsR0FQVSxDQUFkOztBQVNBLE1BQUlvRSxRQUFRLEdBQUcsU0FBWEEsUUFBVyxDQUFVQyxNQUFWLEVBQWtCO0FBQzdCWCxTQUFLLENBQUN5QyxJQUFOLEdBQWE5QixNQUFNLENBQUMzRCxLQUFwQjtBQUNILEdBRkQ7O0FBR0EsU0FBUSxDQUFDLEdBQUdpQyxhQUFhLENBQUM0QixHQUFsQixFQUF1QixLQUF2QixFQUE4QjtBQUFFQyxZQUFRLEVBQUUsQ0FBQyxHQUFHN0IsYUFBYSxDQUFDNEIsR0FBbEIsRUFBdUI0SSxjQUFjLENBQUMsU0FBRCxDQUFyQyxFQUFrRDtBQUFFbE4sYUFBTyxFQUFFQSxPQUFYO0FBQW9CbUUsY0FBUSxFQUFFQTtBQUE5QixLQUFsRDtBQUFaLEdBQTlCLENBQVI7QUFDSCxDQWhCRDs7QUFpQkEvRyxPQUFPLENBQUMsU0FBRCxDQUFQLEdBQXFCK1AsbUJBQXJCLEM7Ozs7Ozs7Ozs7OztBQ3hCYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDYixJQUFJRSxVQUFVLEdBQUksUUFBUSxLQUFLQSxVQUFkLElBQTZCLFVBQVVDLFVBQVYsRUFBc0JDLE1BQXRCLEVBQThCQyxHQUE5QixFQUFtQ0MsSUFBbkMsRUFBeUM7QUFDbkYsTUFBSUMsQ0FBQyxHQUFHaEgsU0FBUyxDQUFDbEUsTUFBbEI7QUFBQSxNQUEwQnlKLENBQUMsR0FBR3lCLENBQUMsR0FBRyxDQUFKLEdBQVFILE1BQVIsR0FBaUJFLElBQUksS0FBSyxJQUFULEdBQWdCQSxJQUFJLEdBQUduSCxNQUFNLENBQUNxSCx3QkFBUCxDQUFnQ0osTUFBaEMsRUFBd0NDLEdBQXhDLENBQXZCLEdBQXNFQyxJQUFySDtBQUFBLE1BQTJIRyxDQUEzSDtBQUNBLE1BQUksUUFBT0MsT0FBUCx5Q0FBT0EsT0FBUCxPQUFtQixRQUFuQixJQUErQixPQUFPQSxPQUFPLENBQUNDLFFBQWYsS0FBNEIsVUFBL0QsRUFBMkU3QixDQUFDLEdBQUc0QixPQUFPLENBQUNDLFFBQVIsQ0FBaUJSLFVBQWpCLEVBQTZCQyxNQUE3QixFQUFxQ0MsR0FBckMsRUFBMENDLElBQTFDLENBQUosQ0FBM0UsS0FDSyxLQUFLLElBQUloSCxDQUFDLEdBQUc2RyxVQUFVLENBQUM5SyxNQUFYLEdBQW9CLENBQWpDLEVBQW9DaUUsQ0FBQyxJQUFJLENBQXpDLEVBQTRDQSxDQUFDLEVBQTdDO0FBQWlELFFBQUltSCxDQUFDLEdBQUdOLFVBQVUsQ0FBQzdHLENBQUQsQ0FBbEIsRUFBdUJ3RixDQUFDLEdBQUcsQ0FBQ3lCLENBQUMsR0FBRyxDQUFKLEdBQVFFLENBQUMsQ0FBQzNCLENBQUQsQ0FBVCxHQUFleUIsQ0FBQyxHQUFHLENBQUosR0FBUUUsQ0FBQyxDQUFDTCxNQUFELEVBQVNDLEdBQVQsRUFBY3ZCLENBQWQsQ0FBVCxHQUE0QjJCLENBQUMsQ0FBQ0wsTUFBRCxFQUFTQyxHQUFULENBQTdDLEtBQStEdkIsQ0FBbkU7QUFBeEU7QUFDTCxTQUFPeUIsQ0FBQyxHQUFHLENBQUosSUFBU3pCLENBQVQsSUFBYzNGLE1BQU0sQ0FBQ3lILGNBQVAsQ0FBc0JSLE1BQXRCLEVBQThCQyxHQUE5QixFQUFtQ3ZCLENBQW5DLENBQWQsRUFBcURBLENBQTVEO0FBQ0gsQ0FMRDs7QUFNQSxJQUFJK0IsVUFBVSxHQUFJLFFBQVEsS0FBS0EsVUFBZCxJQUE2QixVQUFVQyxDQUFWLEVBQWE5TCxDQUFiLEVBQWdCO0FBQzFELE1BQUksUUFBTzBMLE9BQVAseUNBQU9BLE9BQVAsT0FBbUIsUUFBbkIsSUFBK0IsT0FBT0EsT0FBTyxDQUFDSyxRQUFmLEtBQTRCLFVBQS9ELEVBQTJFLE9BQU9MLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQkQsQ0FBakIsRUFBb0I5TCxDQUFwQixDQUFQO0FBQzlFLENBRkQ7O0FBR0EsSUFBSWhDLFNBQVMsR0FBSSxRQUFRLEtBQUtBLFNBQWQsSUFBNEIsVUFBVUMsT0FBVixFQUFtQkMsVUFBbkIsRUFBK0JDLENBQS9CLEVBQWtDQyxTQUFsQyxFQUE2QztBQUNyRixXQUFTQyxLQUFULENBQWVDLEtBQWYsRUFBc0I7QUFBRSxXQUFPQSxLQUFLLFlBQVlILENBQWpCLEdBQXFCRyxLQUFyQixHQUE2QixJQUFJSCxDQUFKLENBQU0sVUFBVWpDLE9BQVYsRUFBbUI7QUFBRUEsYUFBTyxDQUFDb0MsS0FBRCxDQUFQO0FBQWlCLEtBQTVDLENBQXBDO0FBQW9GOztBQUM1RyxTQUFPLEtBQUtILENBQUMsS0FBS0EsQ0FBQyxHQUFHSSxPQUFULENBQU4sRUFBeUIsVUFBVXJDLE9BQVYsRUFBbUJzQyxNQUFuQixFQUEyQjtBQUN2RCxhQUFTQyxTQUFULENBQW1CSCxLQUFuQixFQUEwQjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUNPLElBQVYsQ0FBZUwsS0FBZixDQUFELENBQUo7QUFBOEIsT0FBcEMsQ0FBcUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDM0YsYUFBU0MsUUFBVCxDQUFrQlAsS0FBbEIsRUFBeUI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDLE9BQUQsQ0FBVCxDQUFtQkUsS0FBbkIsQ0FBRCxDQUFKO0FBQWtDLE9BQXhDLENBQXlDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzlGLGFBQVNGLElBQVQsQ0FBY0ksTUFBZCxFQUFzQjtBQUFFQSxZQUFNLENBQUNDLElBQVAsR0FBYzdDLE9BQU8sQ0FBQzRDLE1BQU0sQ0FBQ1IsS0FBUixDQUFyQixHQUFzQ0QsS0FBSyxDQUFDUyxNQUFNLENBQUNSLEtBQVIsQ0FBTCxDQUFvQlUsSUFBcEIsQ0FBeUJQLFNBQXpCLEVBQW9DSSxRQUFwQyxDQUF0QztBQUFzRjs7QUFDOUdILFFBQUksQ0FBQyxDQUFDTixTQUFTLEdBQUdBLFNBQVMsQ0FBQ2EsS0FBVixDQUFnQmhCLE9BQWhCLEVBQXlCQyxVQUFVLElBQUksRUFBdkMsQ0FBYixFQUF5RFMsSUFBekQsRUFBRCxDQUFKO0FBQ0gsR0FMTSxDQUFQO0FBTUgsQ0FSRDs7QUFTQSxJQUFJTyxXQUFXLEdBQUksUUFBUSxLQUFLQSxXQUFkLElBQThCLFVBQVVqQixPQUFWLEVBQW1Ca0IsSUFBbkIsRUFBeUI7QUFDckUsTUFBSUMsQ0FBQyxHQUFHO0FBQUV4QixTQUFLLEVBQUUsQ0FBVDtBQUFZeUIsUUFBSSxFQUFFLGdCQUFXO0FBQUUsVUFBSUMsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFPLENBQVgsRUFBYyxNQUFNQSxDQUFDLENBQUMsQ0FBRCxDQUFQO0FBQVksYUFBT0EsQ0FBQyxDQUFDLENBQUQsQ0FBUjtBQUFjLEtBQXZFO0FBQXlFQyxRQUFJLEVBQUUsRUFBL0U7QUFBbUZDLE9BQUcsRUFBRTtBQUF4RixHQUFSO0FBQUEsTUFBc0dDLENBQXRHO0FBQUEsTUFBeUdDLENBQXpHO0FBQUEsTUFBNEdKLENBQTVHO0FBQUEsTUFBK0dLLENBQS9HO0FBQ0EsU0FBT0EsQ0FBQyxHQUFHO0FBQUVoQixRQUFJLEVBQUVpQixJQUFJLENBQUMsQ0FBRCxDQUFaO0FBQWlCLGFBQVNBLElBQUksQ0FBQyxDQUFELENBQTlCO0FBQW1DLGNBQVVBLElBQUksQ0FBQyxDQUFEO0FBQWpELEdBQUosRUFBNEQsT0FBT0MsTUFBUCxLQUFrQixVQUFsQixLQUFpQ0YsQ0FBQyxDQUFDRSxNQUFNLENBQUNDLFFBQVIsQ0FBRCxHQUFxQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsR0FBakYsQ0FBNUQsRUFBZ0pILENBQXZKOztBQUNBLFdBQVNDLElBQVQsQ0FBY0csQ0FBZCxFQUFpQjtBQUFFLFdBQU8sVUFBVUMsQ0FBVixFQUFhO0FBQUUsYUFBT3RCLElBQUksQ0FBQyxDQUFDcUIsQ0FBRCxFQUFJQyxDQUFKLENBQUQsQ0FBWDtBQUFzQixLQUE1QztBQUErQzs7QUFDbEUsV0FBU3RCLElBQVQsQ0FBY3VCLEVBQWQsRUFBa0I7QUFDZCxRQUFJUixDQUFKLEVBQU8sTUFBTSxJQUFJUyxTQUFKLENBQWMsaUNBQWQsQ0FBTjs7QUFDUCxXQUFPZCxDQUFQO0FBQVUsVUFBSTtBQUNWLFlBQUlLLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsS0FBS0osQ0FBQyxHQUFHVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBUixHQUFZUCxDQUFDLENBQUMsUUFBRCxDQUFiLEdBQTBCTyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFQLENBQUMsQ0FBQyxPQUFELENBQUQsS0FBZSxDQUFDSixDQUFDLEdBQUdJLENBQUMsQ0FBQyxRQUFELENBQU4sS0FBcUJKLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLENBQXJCLEVBQWdDLENBQS9DLENBQVIsR0FBNERBLENBQUMsQ0FBQ2YsSUFBakcsQ0FBRCxJQUEyRyxDQUFDLENBQUNXLENBQUMsR0FBR0EsQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsRUFBVU8sRUFBRSxDQUFDLENBQUQsQ0FBWixDQUFMLEVBQXVCbEIsSUFBOUksRUFBb0osT0FBT08sQ0FBUDtBQUNwSixZQUFJSSxDQUFDLEdBQUcsQ0FBSixFQUFPSixDQUFYLEVBQWNXLEVBQUUsR0FBRyxDQUFDQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBVCxFQUFZWCxDQUFDLENBQUNoQixLQUFkLENBQUw7O0FBQ2QsZ0JBQVEyQixFQUFFLENBQUMsQ0FBRCxDQUFWO0FBQ0ksZUFBSyxDQUFMO0FBQVEsZUFBSyxDQUFMO0FBQVFYLGFBQUMsR0FBR1csRUFBSjtBQUFROztBQUN4QixlQUFLLENBQUw7QUFBUWIsYUFBQyxDQUFDeEIsS0FBRjtBQUFXLG1CQUFPO0FBQUVVLG1CQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFYO0FBQWdCbEIsa0JBQUksRUFBRTtBQUF0QixhQUFQOztBQUNuQixlQUFLLENBQUw7QUFBUUssYUFBQyxDQUFDeEIsS0FBRjtBQUFXOEIsYUFBQyxHQUFHTyxFQUFFLENBQUMsQ0FBRCxDQUFOO0FBQVdBLGNBQUUsR0FBRyxDQUFDLENBQUQsQ0FBTDtBQUFVOztBQUN4QyxlQUFLLENBQUw7QUFBUUEsY0FBRSxHQUFHYixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTixFQUFMOztBQUFrQmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjOztBQUN4QztBQUNJLGdCQUFJLEVBQUVkLENBQUMsR0FBR0YsQ0FBQyxDQUFDRyxJQUFOLEVBQVlELENBQUMsR0FBR0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWCxJQUFnQmYsQ0FBQyxDQUFDQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFaLENBQW5DLE1BQXVESixFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBaEYsQ0FBSixFQUF3RjtBQUFFYixlQUFDLEdBQUcsQ0FBSjtBQUFPO0FBQVc7O0FBQzVHLGdCQUFJYSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixLQUFnQixDQUFDWCxDQUFELElBQU9XLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBVCxJQUFnQlcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFoRCxDQUFKLEVBQTJEO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVXFDLEVBQUUsQ0FBQyxDQUFELENBQVo7QUFBaUI7QUFBUTs7QUFDdEYsZ0JBQUlBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWViLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQTlCLEVBQW1DO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7QUFBZ0JBLGVBQUMsR0FBR1csRUFBSjtBQUFRO0FBQVE7O0FBQ3JFLGdCQUFJWCxDQUFDLElBQUlGLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQXBCLEVBQXlCO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7O0FBQWdCRixlQUFDLENBQUNJLEdBQUYsQ0FBTWMsSUFBTixDQUFXTCxFQUFYOztBQUFnQjtBQUFROztBQUNuRSxnQkFBSVgsQ0FBQyxDQUFDLENBQUQsQ0FBTCxFQUFVRixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTjs7QUFDVmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjO0FBWHRCOztBQWFBSCxVQUFFLEdBQUdkLElBQUksQ0FBQ2dCLElBQUwsQ0FBVWxDLE9BQVYsRUFBbUJtQixDQUFuQixDQUFMO0FBQ0gsT0FqQlMsQ0FpQlIsT0FBT1IsQ0FBUCxFQUFVO0FBQUVxQixVQUFFLEdBQUcsQ0FBQyxDQUFELEVBQUlyQixDQUFKLENBQUw7QUFBYWMsU0FBQyxHQUFHLENBQUo7QUFBUSxPQWpCekIsU0FpQmtDO0FBQUVELFNBQUMsR0FBR0gsQ0FBQyxHQUFHLENBQVI7QUFBWTtBQWpCMUQ7O0FBa0JBLFFBQUlXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFaLEVBQWUsTUFBTUEsRUFBRSxDQUFDLENBQUQsQ0FBUjtBQUFhLFdBQU87QUFBRTNCLFdBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUUEsRUFBRSxDQUFDLENBQUQsQ0FBVixHQUFnQixLQUFLLENBQTlCO0FBQWlDbEIsVUFBSSxFQUFFO0FBQXZDLEtBQVA7QUFDL0I7QUFDSixDQTFCRDs7QUEyQkE5RCxPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7QUFDQUQsT0FBTyxDQUFDNEcsY0FBUixHQUF5QixLQUFLLENBQTlCOztBQUNBLElBQUlsRyxVQUFVLEdBQUdSLG1CQUFPLENBQUMsbUdBQUQsQ0FBeEI7O0FBQ0EsSUFBSU8sT0FBTyxHQUFHUCxtQkFBTyxDQUFDLCtFQUFELENBQXJCOztBQUNBLElBQUk2USxPQUFPLEdBQUc3USxtQkFBTyxDQUFDLG1GQUFELENBQXJCOztBQUNBLElBQUlzRixTQUFTLEdBQUd0RixtQkFBTyxDQUFDLHVGQUFELENBQXZCOztBQUNBLElBQUkwRyxjQUFjO0FBQUc7QUFBZSxZQUFZO0FBQzVDLFdBQVNBLGNBQVQsQ0FBd0I3RixLQUF4QixFQUErQjtBQUMzQixTQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDSDs7QUFDRDZGLGdCQUFjLENBQUN4RixTQUFmLENBQXlCeUYsV0FBekIsR0FBdUMsVUFBVVQsSUFBVixFQUFnQjtBQUNuRCxXQUFPckQsU0FBUyxDQUFDLElBQUQsRUFBTyxLQUFLLENBQVosRUFBZSxLQUFLLENBQXBCLEVBQXVCLFlBQVk7QUFDL0MsYUFBT2tCLFdBQVcsQ0FBQyxJQUFELEVBQU8sVUFBVThCLEVBQVYsRUFBYztBQUNuQyxnQkFBUUEsRUFBRSxDQUFDcEQsS0FBWDtBQUNJLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWNvTyxPQUFPLENBQUMsU0FBRCxDQUFQLENBQW1CQyxHQUFuQixDQUF1QixpQ0FBaUM1SyxJQUFJLENBQUNyRSxJQUE3RCxFQUFtRTtBQUN4RmtQLHFCQUFPLEVBQUU7QUFDTCxpQ0FBaUIsWUFBWSxLQUFLbFEsS0FBTCxDQUFXSTtBQURuQztBQUQrRSxhQUFuRSxFQUl0QjRDLElBSnNCLENBSWpCLFVBQVVtTixRQUFWLEVBQW9CO0FBQ3hCLHFCQUFPQSxRQUFRLENBQUN2SyxJQUFULENBQWNBLElBQWQsQ0FBbUJ3SyxHQUFuQixDQUF1QixVQUFVQyxJQUFWLEVBQWdCO0FBQzFDLG9CQUFJbkssT0FBTyxHQUFHLElBQUl6QixTQUFTLENBQUM1RCxPQUFkLEVBQWQ7QUFDQXFGLHVCQUFPLENBQUNuRixFQUFSLEdBQWFzUCxJQUFJLENBQUN0UCxFQUFsQjtBQUNBbUYsdUJBQU8sQ0FBQy9FLEtBQVIsR0FBZ0JrUCxJQUFJLENBQUNsUCxLQUFyQjtBQUNBK0UsdUJBQU8sQ0FBQ2hGLFFBQVIsR0FBbUJtUCxJQUFJLENBQUNuUCxRQUF4QjtBQUNBZ0YsdUJBQU8sQ0FBQ2xGLElBQVIsR0FBZXFQLElBQUksQ0FBQ3JQLElBQXBCO0FBQ0EsdUJBQU9rRixPQUFQO0FBQ0gsZUFQTSxDQUFQO0FBUUgsYUFid0IsQ0FBZCxDQUFQOztBQWNSLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWVsQixFQUFFLENBQUMzQixJQUFILEVBQWYsQ0FBUDtBQWZaO0FBaUJILE9BbEJpQixDQUFsQjtBQW1CSCxLQXBCZSxDQUFoQjtBQXFCSCxHQXRCRDs7QUF1QkF3QyxnQkFBYyxHQUFHcUosVUFBVSxDQUFDLENBQ3hCLENBQUMsR0FBR3ZQLFVBQVUsQ0FBQzJRLFNBQWYsR0FEd0IsRUFFeEIsQ0FBQyxHQUFHM1EsVUFBVSxDQUFDNFEsY0FBZixHQUZ3QixFQUd4QlYsVUFBVSxDQUFDLG1CQUFELEVBQXNCLENBQUNuUSxPQUFPLENBQUNTLEtBQVQsQ0FBdEIsQ0FIYyxDQUFELEVBSXhCMEYsY0FKd0IsQ0FBM0I7QUFLQSxTQUFPQSxjQUFQO0FBQ0gsQ0FqQ21DLEVBQXBDOztBQWtDQTVHLE9BQU8sQ0FBQzRHLGNBQVIsR0FBeUJBLGNBQXpCLEM7Ozs7Ozs7Ozs7OztBQ3RGYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDYixJQUFJcUosVUFBVSxHQUFJLFFBQVEsS0FBS0EsVUFBZCxJQUE2QixVQUFVQyxVQUFWLEVBQXNCQyxNQUF0QixFQUE4QkMsR0FBOUIsRUFBbUNDLElBQW5DLEVBQXlDO0FBQ25GLE1BQUlDLENBQUMsR0FBR2hILFNBQVMsQ0FBQ2xFLE1BQWxCO0FBQUEsTUFBMEJ5SixDQUFDLEdBQUd5QixDQUFDLEdBQUcsQ0FBSixHQUFRSCxNQUFSLEdBQWlCRSxJQUFJLEtBQUssSUFBVCxHQUFnQkEsSUFBSSxHQUFHbkgsTUFBTSxDQUFDcUgsd0JBQVAsQ0FBZ0NKLE1BQWhDLEVBQXdDQyxHQUF4QyxDQUF2QixHQUFzRUMsSUFBckg7QUFBQSxNQUEySEcsQ0FBM0g7QUFDQSxNQUFJLFFBQU9DLE9BQVAseUNBQU9BLE9BQVAsT0FBbUIsUUFBbkIsSUFBK0IsT0FBT0EsT0FBTyxDQUFDQyxRQUFmLEtBQTRCLFVBQS9ELEVBQTJFN0IsQ0FBQyxHQUFHNEIsT0FBTyxDQUFDQyxRQUFSLENBQWlCUixVQUFqQixFQUE2QkMsTUFBN0IsRUFBcUNDLEdBQXJDLEVBQTBDQyxJQUExQyxDQUFKLENBQTNFLEtBQ0ssS0FBSyxJQUFJaEgsQ0FBQyxHQUFHNkcsVUFBVSxDQUFDOUssTUFBWCxHQUFvQixDQUFqQyxFQUFvQ2lFLENBQUMsSUFBSSxDQUF6QyxFQUE0Q0EsQ0FBQyxFQUE3QztBQUFpRCxRQUFJbUgsQ0FBQyxHQUFHTixVQUFVLENBQUM3RyxDQUFELENBQWxCLEVBQXVCd0YsQ0FBQyxHQUFHLENBQUN5QixDQUFDLEdBQUcsQ0FBSixHQUFRRSxDQUFDLENBQUMzQixDQUFELENBQVQsR0FBZXlCLENBQUMsR0FBRyxDQUFKLEdBQVFFLENBQUMsQ0FBQ0wsTUFBRCxFQUFTQyxHQUFULEVBQWN2QixDQUFkLENBQVQsR0FBNEIyQixDQUFDLENBQUNMLE1BQUQsRUFBU0MsR0FBVCxDQUE3QyxLQUErRHZCLENBQW5FO0FBQXhFO0FBQ0wsU0FBT3lCLENBQUMsR0FBRyxDQUFKLElBQVN6QixDQUFULElBQWMzRixNQUFNLENBQUN5SCxjQUFQLENBQXNCUixNQUF0QixFQUE4QkMsR0FBOUIsRUFBbUN2QixDQUFuQyxDQUFkLEVBQXFEQSxDQUE1RDtBQUNILENBTEQ7O0FBTUEsSUFBSStCLFVBQVUsR0FBSSxRQUFRLEtBQUtBLFVBQWQsSUFBNkIsVUFBVUMsQ0FBVixFQUFhOUwsQ0FBYixFQUFnQjtBQUMxRCxNQUFJLFFBQU8wTCxPQUFQLHlDQUFPQSxPQUFQLE9BQW1CLFFBQW5CLElBQStCLE9BQU9BLE9BQU8sQ0FBQ0ssUUFBZixLQUE0QixVQUEvRCxFQUEyRSxPQUFPTCxPQUFPLENBQUNLLFFBQVIsQ0FBaUJELENBQWpCLEVBQW9COUwsQ0FBcEIsQ0FBUDtBQUM5RSxDQUZEOztBQUdBLElBQUloQyxTQUFTLEdBQUksUUFBUSxLQUFLQSxTQUFkLElBQTRCLFVBQVVDLE9BQVYsRUFBbUJDLFVBQW5CLEVBQStCQyxDQUEvQixFQUFrQ0MsU0FBbEMsRUFBNkM7QUFDckYsV0FBU0MsS0FBVCxDQUFlQyxLQUFmLEVBQXNCO0FBQUUsV0FBT0EsS0FBSyxZQUFZSCxDQUFqQixHQUFxQkcsS0FBckIsR0FBNkIsSUFBSUgsQ0FBSixDQUFNLFVBQVVqQyxPQUFWLEVBQW1CO0FBQUVBLGFBQU8sQ0FBQ29DLEtBQUQsQ0FBUDtBQUFpQixLQUE1QyxDQUFwQztBQUFvRjs7QUFDNUcsU0FBTyxLQUFLSCxDQUFDLEtBQUtBLENBQUMsR0FBR0ksT0FBVCxDQUFOLEVBQXlCLFVBQVVyQyxPQUFWLEVBQW1Cc0MsTUFBbkIsRUFBMkI7QUFDdkQsYUFBU0MsU0FBVCxDQUFtQkgsS0FBbkIsRUFBMEI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDTyxJQUFWLENBQWVMLEtBQWYsQ0FBRCxDQUFKO0FBQThCLE9BQXBDLENBQXFDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzNGLGFBQVNDLFFBQVQsQ0FBa0JQLEtBQWxCLEVBQXlCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQyxPQUFELENBQVQsQ0FBbUJFLEtBQW5CLENBQUQsQ0FBSjtBQUFrQyxPQUF4QyxDQUF5QyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUM5RixhQUFTRixJQUFULENBQWNJLE1BQWQsRUFBc0I7QUFBRUEsWUFBTSxDQUFDQyxJQUFQLEdBQWM3QyxPQUFPLENBQUM0QyxNQUFNLENBQUNSLEtBQVIsQ0FBckIsR0FBc0NELEtBQUssQ0FBQ1MsTUFBTSxDQUFDUixLQUFSLENBQUwsQ0FBb0JVLElBQXBCLENBQXlCUCxTQUF6QixFQUFvQ0ksUUFBcEMsQ0FBdEM7QUFBc0Y7O0FBQzlHSCxRQUFJLENBQUMsQ0FBQ04sU0FBUyxHQUFHQSxTQUFTLENBQUNhLEtBQVYsQ0FBZ0JoQixPQUFoQixFQUF5QkMsVUFBVSxJQUFJLEVBQXZDLENBQWIsRUFBeURTLElBQXpELEVBQUQsQ0FBSjtBQUNILEdBTE0sQ0FBUDtBQU1ILENBUkQ7O0FBU0EsSUFBSU8sV0FBVyxHQUFJLFFBQVEsS0FBS0EsV0FBZCxJQUE4QixVQUFVakIsT0FBVixFQUFtQmtCLElBQW5CLEVBQXlCO0FBQ3JFLE1BQUlDLENBQUMsR0FBRztBQUFFeEIsU0FBSyxFQUFFLENBQVQ7QUFBWXlCLFFBQUksRUFBRSxnQkFBVztBQUFFLFVBQUlDLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBTyxDQUFYLEVBQWMsTUFBTUEsQ0FBQyxDQUFDLENBQUQsQ0FBUDtBQUFZLGFBQU9BLENBQUMsQ0FBQyxDQUFELENBQVI7QUFBYyxLQUF2RTtBQUF5RUMsUUFBSSxFQUFFLEVBQS9FO0FBQW1GQyxPQUFHLEVBQUU7QUFBeEYsR0FBUjtBQUFBLE1BQXNHQyxDQUF0RztBQUFBLE1BQXlHQyxDQUF6RztBQUFBLE1BQTRHSixDQUE1RztBQUFBLE1BQStHSyxDQUEvRztBQUNBLFNBQU9BLENBQUMsR0FBRztBQUFFaEIsUUFBSSxFQUFFaUIsSUFBSSxDQUFDLENBQUQsQ0FBWjtBQUFpQixhQUFTQSxJQUFJLENBQUMsQ0FBRCxDQUE5QjtBQUFtQyxjQUFVQSxJQUFJLENBQUMsQ0FBRDtBQUFqRCxHQUFKLEVBQTRELE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsS0FBaUNGLENBQUMsQ0FBQ0UsTUFBTSxDQUFDQyxRQUFSLENBQUQsR0FBcUIsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLEdBQWpGLENBQTVELEVBQWdKSCxDQUF2Sjs7QUFDQSxXQUFTQyxJQUFULENBQWNHLENBQWQsRUFBaUI7QUFBRSxXQUFPLFVBQVVDLENBQVYsRUFBYTtBQUFFLGFBQU90QixJQUFJLENBQUMsQ0FBQ3FCLENBQUQsRUFBSUMsQ0FBSixDQUFELENBQVg7QUFBc0IsS0FBNUM7QUFBK0M7O0FBQ2xFLFdBQVN0QixJQUFULENBQWN1QixFQUFkLEVBQWtCO0FBQ2QsUUFBSVIsQ0FBSixFQUFPLE1BQU0sSUFBSVMsU0FBSixDQUFjLGlDQUFkLENBQU47O0FBQ1AsV0FBT2QsQ0FBUDtBQUFVLFVBQUk7QUFDVixZQUFJSyxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEtBQUtKLENBQUMsR0FBR1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVIsR0FBWVAsQ0FBQyxDQUFDLFFBQUQsQ0FBYixHQUEwQk8sRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRUCxDQUFDLENBQUMsT0FBRCxDQUFELEtBQWUsQ0FBQ0osQ0FBQyxHQUFHSSxDQUFDLENBQUMsUUFBRCxDQUFOLEtBQXFCSixDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxDQUFyQixFQUFnQyxDQUEvQyxDQUFSLEdBQTREQSxDQUFDLENBQUNmLElBQWpHLENBQUQsSUFBMkcsQ0FBQyxDQUFDVyxDQUFDLEdBQUdBLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLEVBQVVPLEVBQUUsQ0FBQyxDQUFELENBQVosQ0FBTCxFQUF1QmxCLElBQTlJLEVBQW9KLE9BQU9PLENBQVA7QUFDcEosWUFBSUksQ0FBQyxHQUFHLENBQUosRUFBT0osQ0FBWCxFQUFjVyxFQUFFLEdBQUcsQ0FBQ0EsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVQsRUFBWVgsQ0FBQyxDQUFDaEIsS0FBZCxDQUFMOztBQUNkLGdCQUFRMkIsRUFBRSxDQUFDLENBQUQsQ0FBVjtBQUNJLGVBQUssQ0FBTDtBQUFRLGVBQUssQ0FBTDtBQUFRWCxhQUFDLEdBQUdXLEVBQUo7QUFBUTs7QUFDeEIsZUFBSyxDQUFMO0FBQVFiLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVyxtQkFBTztBQUFFVSxtQkFBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBWDtBQUFnQmxCLGtCQUFJLEVBQUU7QUFBdEIsYUFBUDs7QUFDbkIsZUFBSyxDQUFMO0FBQVFLLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVzhCLGFBQUMsR0FBR08sRUFBRSxDQUFDLENBQUQsQ0FBTjtBQUFXQSxjQUFFLEdBQUcsQ0FBQyxDQUFELENBQUw7QUFBVTs7QUFDeEMsZUFBSyxDQUFMO0FBQVFBLGNBQUUsR0FBR2IsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU4sRUFBTDs7QUFBa0JoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYzs7QUFDeEM7QUFDSSxnQkFBSSxFQUFFZCxDQUFDLEdBQUdGLENBQUMsQ0FBQ0csSUFBTixFQUFZRCxDQUFDLEdBQUdBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVgsSUFBZ0JmLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWixDQUFuQyxNQUF1REosRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQWhGLENBQUosRUFBd0Y7QUFBRWIsZUFBQyxHQUFHLENBQUo7QUFBTztBQUFXOztBQUM1RyxnQkFBSWEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsS0FBZ0IsQ0FBQ1gsQ0FBRCxJQUFPVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQVQsSUFBZ0JXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBaEQsQ0FBSixFQUEyRDtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVVxQyxFQUFFLENBQUMsQ0FBRCxDQUFaO0FBQWlCO0FBQVE7O0FBQ3RGLGdCQUFJQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlYixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUE5QixFQUFtQztBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYO0FBQWdCQSxlQUFDLEdBQUdXLEVBQUo7QUFBUTtBQUFROztBQUNyRSxnQkFBSVgsQ0FBQyxJQUFJRixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFwQixFQUF5QjtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYOztBQUFnQkYsZUFBQyxDQUFDSSxHQUFGLENBQU1jLElBQU4sQ0FBV0wsRUFBWDs7QUFBZ0I7QUFBUTs7QUFDbkUsZ0JBQUlYLENBQUMsQ0FBQyxDQUFELENBQUwsRUFBVUYsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU47O0FBQ1ZoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYztBQVh0Qjs7QUFhQUgsVUFBRSxHQUFHZCxJQUFJLENBQUNnQixJQUFMLENBQVVsQyxPQUFWLEVBQW1CbUIsQ0FBbkIsQ0FBTDtBQUNILE9BakJTLENBaUJSLE9BQU9SLENBQVAsRUFBVTtBQUFFcUIsVUFBRSxHQUFHLENBQUMsQ0FBRCxFQUFJckIsQ0FBSixDQUFMO0FBQWFjLFNBQUMsR0FBRyxDQUFKO0FBQVEsT0FqQnpCLFNBaUJrQztBQUFFRCxTQUFDLEdBQUdILENBQUMsR0FBRyxDQUFSO0FBQVk7QUFqQjFEOztBQWtCQSxRQUFJVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBWixFQUFlLE1BQU1BLEVBQUUsQ0FBQyxDQUFELENBQVI7QUFBYSxXQUFPO0FBQUUzQixXQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFBLEVBQUUsQ0FBQyxDQUFELENBQVYsR0FBZ0IsS0FBSyxDQUE5QjtBQUFpQ2xCLFVBQUksRUFBRTtBQUF2QyxLQUFQO0FBQy9CO0FBQ0osQ0ExQkQ7O0FBMkJBOUQsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELE9BQU8sQ0FBQzRJLGNBQVIsR0FBeUIsS0FBSyxDQUE5Qjs7QUFDQSxJQUFJbEksVUFBVSxHQUFHUixtQkFBTyxDQUFDLG1HQUFELENBQXhCOztBQUNBLElBQUlPLE9BQU8sR0FBR1AsbUJBQU8sQ0FBQywrRUFBRCxDQUFyQjs7QUFDQSxJQUFJNlEsT0FBTyxHQUFHN1EsbUJBQU8sQ0FBQyxtRkFBRCxDQUFyQjs7QUFDQSxJQUFJa0ksU0FBUyxHQUFHbEksbUJBQU8sQ0FBQyx1RkFBRCxDQUF2Qjs7QUFDQSxJQUFJbUksU0FBUyxHQUFHbkksbUJBQU8sQ0FBQyx1RkFBRCxDQUF2Qjs7QUFDQSxJQUFJMEksY0FBYztBQUFHO0FBQWUsWUFBWTtBQUM1QyxXQUFTQSxjQUFULENBQXdCN0gsS0FBeEIsRUFBK0I7QUFDM0IsU0FBS0EsS0FBTCxHQUFhQSxLQUFiO0FBQ0g7O0FBQ0Q2SCxnQkFBYyxDQUFDeEgsU0FBZixDQUF5QjRKLFdBQXpCLEdBQXVDLFVBQVV4RSxXQUFWLEVBQXVCcUUsSUFBdkIsRUFBNkJ6RSxJQUE3QixFQUFtQztBQUN0RSxXQUFPckQsU0FBUyxDQUFDLElBQUQsRUFBTyxLQUFLLENBQVosRUFBZSxLQUFLLENBQXBCLEVBQXVCLFlBQVk7QUFDL0MsYUFBT2tCLFdBQVcsQ0FBQyxJQUFELEVBQU8sVUFBVThCLEVBQVYsRUFBYztBQUNuQyxnQkFBUUEsRUFBRSxDQUFDcEQsS0FBWDtBQUNJLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWNvTyxPQUFPLENBQUMsU0FBRCxDQUFQLENBQW1CQyxHQUFuQixDQUF1QixpQ0FBaUM1SyxJQUFJLENBQUNyRSxJQUF0QyxHQUE2QyxRQUE3QyxHQUF3RHlFLFdBQXhELEdBQXNFLFFBQXRFLEdBQWlGcUUsSUFBeEcsRUFBOEc7QUFDbklvRyxxQkFBTyxFQUFFO0FBQ0wsaUNBQWlCLFlBQVksS0FBS2xRLEtBQUwsQ0FBV0k7QUFEbkM7QUFEMEgsYUFBOUcsRUFJdEI0QyxJQUpzQixDQUlqQixVQUFVbU4sUUFBVixFQUFvQjtBQUN4QixxQkFBT0EsUUFBUSxDQUFDdkssSUFBVCxDQUFjQSxJQUFkLENBQW1Cd0ssR0FBbkIsQ0FBdUIsVUFBVUMsSUFBVixFQUFnQjtBQUMxQyxvQkFBSTFJLE9BQU8sR0FBRyxJQUFJTixTQUFTLENBQUM5RixPQUFkLEVBQWQ7QUFDQW9HLHVCQUFPLENBQUM1RyxFQUFSLEdBQWFzUCxJQUFJLENBQUN0UCxFQUFsQjtBQUNBNEcsdUJBQU8sQ0FBQ25HLFFBQVIsR0FBbUI2TyxJQUFJLENBQUM3TyxRQUF4QjtBQUNBbUcsdUJBQU8sQ0FBQzNHLElBQVIsR0FBZXFQLElBQUksQ0FBQ3JQLElBQXBCO0FBQ0EsdUJBQU8yRyxPQUFQO0FBQ0gsZUFOTSxDQUFQO0FBT0gsYUFad0IsQ0FBZCxDQUFQOztBQWFSLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWUzQyxFQUFFLENBQUMzQixJQUFILEVBQWYsQ0FBUDtBQWRaO0FBZ0JILE9BakJpQixDQUFsQjtBQWtCSCxLQW5CZSxDQUFoQjtBQW9CSCxHQXJCRDs7QUFzQkF3RSxnQkFBYyxDQUFDeEgsU0FBZixDQUF5QnlILGlCQUF6QixHQUE2QyxVQUFVSCxPQUFWLEVBQW1CSSxJQUFuQixFQUF5QjtBQUNsRSxXQUFPL0YsU0FBUyxDQUFDLElBQUQsRUFBTyxLQUFLLENBQVosRUFBZSxLQUFLLENBQXBCLEVBQXVCLFlBQVk7QUFDL0MsYUFBT2tCLFdBQVcsQ0FBQyxJQUFELEVBQU8sVUFBVThCLEVBQVYsRUFBYztBQUNuQyxnQkFBUUEsRUFBRSxDQUFDcEQsS0FBWDtBQUNJLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWNvTyxPQUFPLENBQUMsU0FBRCxDQUFQLENBQW1CQyxHQUFuQixDQUF1Qiw2Q0FBNkN0SSxPQUE3QyxHQUF1RCxHQUF2RCxHQUE2REksSUFBcEYsRUFBMEY7QUFDL0dtSSxxQkFBTyxFQUFFO0FBQ0wsaUNBQWlCLFlBQVksS0FBS2xRLEtBQUwsQ0FBV0k7QUFEbkM7QUFEc0csYUFBMUYsRUFJdEI0QyxJQUpzQixDQUlqQixVQUFVbU4sUUFBVixFQUFvQjtBQUN4QixxQkFBT0EsUUFBUSxDQUFDdkssSUFBVCxDQUFjQSxJQUFkLENBQW1Cd0ssR0FBbkIsQ0FBdUIsVUFBVUMsSUFBVixFQUFnQjtBQUMxQyxvQkFBSUcsT0FBTyxHQUFHLElBQUlsSixTQUFTLENBQUNsRyxPQUFkLEVBQWQ7QUFDQW9QLHVCQUFPLENBQUN6UCxFQUFSLEdBQWFzUCxJQUFJLENBQUN0UCxFQUFsQjtBQUNBeVAsdUJBQU8sQ0FBQ25QLFNBQVIsR0FBb0JnUCxJQUFJLENBQUNoUCxTQUF6QjtBQUNBbVAsdUJBQU8sQ0FBQ2xQLFFBQVIsR0FBbUIrTyxJQUFJLENBQUMvTyxRQUF4QjtBQUNBa1AsdUJBQU8sQ0FBQ3hQLElBQVIsR0FBZXFQLElBQUksQ0FBQ3JQLElBQXBCO0FBQ0EsdUJBQU93UCxPQUFQO0FBQ0gsZUFQTSxDQUFQO0FBUUgsYUFid0IsQ0FBZCxDQUFQOztBQWNSLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWV4TCxFQUFFLENBQUMzQixJQUFILEVBQWYsQ0FBUDtBQWZaO0FBaUJILE9BbEJpQixDQUFsQjtBQW1CSCxLQXBCZSxDQUFoQjtBQXFCSCxHQXRCRDs7QUF1QkF3RSxnQkFBYyxHQUFHcUgsVUFBVSxDQUFDLENBQ3hCLENBQUMsR0FBR3ZQLFVBQVUsQ0FBQzJRLFNBQWYsR0FEd0IsRUFFeEIsQ0FBQyxHQUFHM1EsVUFBVSxDQUFDNFEsY0FBZixHQUZ3QixFQUd4QlYsVUFBVSxDQUFDLG1CQUFELEVBQXNCLENBQUNuUSxPQUFPLENBQUNTLEtBQVQsQ0FBdEIsQ0FIYyxDQUFELEVBSXhCMEgsY0FKd0IsQ0FBM0I7QUFLQSxTQUFPQSxjQUFQO0FBQ0gsQ0F2RG1DLEVBQXBDOztBQXdEQTVJLE9BQU8sQ0FBQzRJLGNBQVIsR0FBeUJBLGNBQXpCLEM7Ozs7Ozs7Ozs7OztBQzdHYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ2IsSUFBSXFILFVBQVUsR0FBSSxRQUFRLEtBQUtBLFVBQWQsSUFBNkIsVUFBVUMsVUFBVixFQUFzQkMsTUFBdEIsRUFBOEJDLEdBQTlCLEVBQW1DQyxJQUFuQyxFQUF5QztBQUNuRixNQUFJQyxDQUFDLEdBQUdoSCxTQUFTLENBQUNsRSxNQUFsQjtBQUFBLE1BQTBCeUosQ0FBQyxHQUFHeUIsQ0FBQyxHQUFHLENBQUosR0FBUUgsTUFBUixHQUFpQkUsSUFBSSxLQUFLLElBQVQsR0FBZ0JBLElBQUksR0FBR25ILE1BQU0sQ0FBQ3FILHdCQUFQLENBQWdDSixNQUFoQyxFQUF3Q0MsR0FBeEMsQ0FBdkIsR0FBc0VDLElBQXJIO0FBQUEsTUFBMkhHLENBQTNIO0FBQ0EsTUFBSSxRQUFPQyxPQUFQLHlDQUFPQSxPQUFQLE9BQW1CLFFBQW5CLElBQStCLE9BQU9BLE9BQU8sQ0FBQ0MsUUFBZixLQUE0QixVQUEvRCxFQUEyRTdCLENBQUMsR0FBRzRCLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQlIsVUFBakIsRUFBNkJDLE1BQTdCLEVBQXFDQyxHQUFyQyxFQUEwQ0MsSUFBMUMsQ0FBSixDQUEzRSxLQUNLLEtBQUssSUFBSWhILENBQUMsR0FBRzZHLFVBQVUsQ0FBQzlLLE1BQVgsR0FBb0IsQ0FBakMsRUFBb0NpRSxDQUFDLElBQUksQ0FBekMsRUFBNENBLENBQUMsRUFBN0M7QUFBaUQsUUFBSW1ILENBQUMsR0FBR04sVUFBVSxDQUFDN0csQ0FBRCxDQUFsQixFQUF1QndGLENBQUMsR0FBRyxDQUFDeUIsQ0FBQyxHQUFHLENBQUosR0FBUUUsQ0FBQyxDQUFDM0IsQ0FBRCxDQUFULEdBQWV5QixDQUFDLEdBQUcsQ0FBSixHQUFRRSxDQUFDLENBQUNMLE1BQUQsRUFBU0MsR0FBVCxFQUFjdkIsQ0FBZCxDQUFULEdBQTRCMkIsQ0FBQyxDQUFDTCxNQUFELEVBQVNDLEdBQVQsQ0FBN0MsS0FBK0R2QixDQUFuRTtBQUF4RTtBQUNMLFNBQU95QixDQUFDLEdBQUcsQ0FBSixJQUFTekIsQ0FBVCxJQUFjM0YsTUFBTSxDQUFDeUgsY0FBUCxDQUFzQlIsTUFBdEIsRUFBOEJDLEdBQTlCLEVBQW1DdkIsQ0FBbkMsQ0FBZCxFQUFxREEsQ0FBNUQ7QUFDSCxDQUxEOztBQU1BLElBQUkrQixVQUFVLEdBQUksUUFBUSxLQUFLQSxVQUFkLElBQTZCLFVBQVVDLENBQVYsRUFBYTlMLENBQWIsRUFBZ0I7QUFDMUQsTUFBSSxRQUFPMEwsT0FBUCx5Q0FBT0EsT0FBUCxPQUFtQixRQUFuQixJQUErQixPQUFPQSxPQUFPLENBQUNLLFFBQWYsS0FBNEIsVUFBL0QsRUFBMkUsT0FBT0wsT0FBTyxDQUFDSyxRQUFSLENBQWlCRCxDQUFqQixFQUFvQjlMLENBQXBCLENBQVA7QUFDOUUsQ0FGRDs7QUFHQSxJQUFJaEMsU0FBUyxHQUFJLFFBQVEsS0FBS0EsU0FBZCxJQUE0QixVQUFVQyxPQUFWLEVBQW1CQyxVQUFuQixFQUErQkMsQ0FBL0IsRUFBa0NDLFNBQWxDLEVBQTZDO0FBQ3JGLFdBQVNDLEtBQVQsQ0FBZUMsS0FBZixFQUFzQjtBQUFFLFdBQU9BLEtBQUssWUFBWUgsQ0FBakIsR0FBcUJHLEtBQXJCLEdBQTZCLElBQUlILENBQUosQ0FBTSxVQUFVakMsT0FBVixFQUFtQjtBQUFFQSxhQUFPLENBQUNvQyxLQUFELENBQVA7QUFBaUIsS0FBNUMsQ0FBcEM7QUFBb0Y7O0FBQzVHLFNBQU8sS0FBS0gsQ0FBQyxLQUFLQSxDQUFDLEdBQUdJLE9BQVQsQ0FBTixFQUF5QixVQUFVckMsT0FBVixFQUFtQnNDLE1BQW5CLEVBQTJCO0FBQ3ZELGFBQVNDLFNBQVQsQ0FBbUJILEtBQW5CLEVBQTBCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQ08sSUFBVixDQUFlTCxLQUFmLENBQUQsQ0FBSjtBQUE4QixPQUFwQyxDQUFxQyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUMzRixhQUFTQyxRQUFULENBQWtCUCxLQUFsQixFQUF5QjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUMsT0FBRCxDQUFULENBQW1CRSxLQUFuQixDQUFELENBQUo7QUFBa0MsT0FBeEMsQ0FBeUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDOUYsYUFBU0YsSUFBVCxDQUFjSSxNQUFkLEVBQXNCO0FBQUVBLFlBQU0sQ0FBQ0MsSUFBUCxHQUFjN0MsT0FBTyxDQUFDNEMsTUFBTSxDQUFDUixLQUFSLENBQXJCLEdBQXNDRCxLQUFLLENBQUNTLE1BQU0sQ0FBQ1IsS0FBUixDQUFMLENBQW9CVSxJQUFwQixDQUF5QlAsU0FBekIsRUFBb0NJLFFBQXBDLENBQXRDO0FBQXNGOztBQUM5R0gsUUFBSSxDQUFDLENBQUNOLFNBQVMsR0FBR0EsU0FBUyxDQUFDYSxLQUFWLENBQWdCaEIsT0FBaEIsRUFBeUJDLFVBQVUsSUFBSSxFQUF2QyxDQUFiLEVBQXlEUyxJQUF6RCxFQUFELENBQUo7QUFDSCxHQUxNLENBQVA7QUFNSCxDQVJEOztBQVNBLElBQUlPLFdBQVcsR0FBSSxRQUFRLEtBQUtBLFdBQWQsSUFBOEIsVUFBVWpCLE9BQVYsRUFBbUJrQixJQUFuQixFQUF5QjtBQUNyRSxNQUFJQyxDQUFDLEdBQUc7QUFBRXhCLFNBQUssRUFBRSxDQUFUO0FBQVl5QixRQUFJLEVBQUUsZ0JBQVc7QUFBRSxVQUFJQyxDQUFDLENBQUMsQ0FBRCxDQUFELEdBQU8sQ0FBWCxFQUFjLE1BQU1BLENBQUMsQ0FBQyxDQUFELENBQVA7QUFBWSxhQUFPQSxDQUFDLENBQUMsQ0FBRCxDQUFSO0FBQWMsS0FBdkU7QUFBeUVDLFFBQUksRUFBRSxFQUEvRTtBQUFtRkMsT0FBRyxFQUFFO0FBQXhGLEdBQVI7QUFBQSxNQUFzR0MsQ0FBdEc7QUFBQSxNQUF5R0MsQ0FBekc7QUFBQSxNQUE0R0osQ0FBNUc7QUFBQSxNQUErR0ssQ0FBL0c7QUFDQSxTQUFPQSxDQUFDLEdBQUc7QUFBRWhCLFFBQUksRUFBRWlCLElBQUksQ0FBQyxDQUFELENBQVo7QUFBaUIsYUFBU0EsSUFBSSxDQUFDLENBQUQsQ0FBOUI7QUFBbUMsY0FBVUEsSUFBSSxDQUFDLENBQUQ7QUFBakQsR0FBSixFQUE0RCxPQUFPQyxNQUFQLEtBQWtCLFVBQWxCLEtBQWlDRixDQUFDLENBQUNFLE1BQU0sQ0FBQ0MsUUFBUixDQUFELEdBQXFCLFlBQVc7QUFBRSxXQUFPLElBQVA7QUFBYyxHQUFqRixDQUE1RCxFQUFnSkgsQ0FBdko7O0FBQ0EsV0FBU0MsSUFBVCxDQUFjRyxDQUFkLEVBQWlCO0FBQUUsV0FBTyxVQUFVQyxDQUFWLEVBQWE7QUFBRSxhQUFPdEIsSUFBSSxDQUFDLENBQUNxQixDQUFELEVBQUlDLENBQUosQ0FBRCxDQUFYO0FBQXNCLEtBQTVDO0FBQStDOztBQUNsRSxXQUFTdEIsSUFBVCxDQUFjdUIsRUFBZCxFQUFrQjtBQUNkLFFBQUlSLENBQUosRUFBTyxNQUFNLElBQUlTLFNBQUosQ0FBYyxpQ0FBZCxDQUFOOztBQUNQLFdBQU9kLENBQVA7QUFBVSxVQUFJO0FBQ1YsWUFBSUssQ0FBQyxHQUFHLENBQUosRUFBT0MsQ0FBQyxLQUFLSixDQUFDLEdBQUdXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFSLEdBQVlQLENBQUMsQ0FBQyxRQUFELENBQWIsR0FBMEJPLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVAsQ0FBQyxDQUFDLE9BQUQsQ0FBRCxLQUFlLENBQUNKLENBQUMsR0FBR0ksQ0FBQyxDQUFDLFFBQUQsQ0FBTixLQUFxQkosQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsQ0FBckIsRUFBZ0MsQ0FBL0MsQ0FBUixHQUE0REEsQ0FBQyxDQUFDZixJQUFqRyxDQUFELElBQTJHLENBQUMsQ0FBQ1csQ0FBQyxHQUFHQSxDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxFQUFVTyxFQUFFLENBQUMsQ0FBRCxDQUFaLENBQUwsRUFBdUJsQixJQUE5SSxFQUFvSixPQUFPTyxDQUFQO0FBQ3BKLFlBQUlJLENBQUMsR0FBRyxDQUFKLEVBQU9KLENBQVgsRUFBY1csRUFBRSxHQUFHLENBQUNBLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFULEVBQVlYLENBQUMsQ0FBQ2hCLEtBQWQsQ0FBTDs7QUFDZCxnQkFBUTJCLEVBQUUsQ0FBQyxDQUFELENBQVY7QUFDSSxlQUFLLENBQUw7QUFBUSxlQUFLLENBQUw7QUFBUVgsYUFBQyxHQUFHVyxFQUFKO0FBQVE7O0FBQ3hCLGVBQUssQ0FBTDtBQUFRYixhQUFDLENBQUN4QixLQUFGO0FBQVcsbUJBQU87QUFBRVUsbUJBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQVg7QUFBZ0JsQixrQkFBSSxFQUFFO0FBQXRCLGFBQVA7O0FBQ25CLGVBQUssQ0FBTDtBQUFRSyxhQUFDLENBQUN4QixLQUFGO0FBQVc4QixhQUFDLEdBQUdPLEVBQUUsQ0FBQyxDQUFELENBQU47QUFBV0EsY0FBRSxHQUFHLENBQUMsQ0FBRCxDQUFMO0FBQVU7O0FBQ3hDLGVBQUssQ0FBTDtBQUFRQSxjQUFFLEdBQUdiLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOLEVBQUw7O0FBQWtCaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7O0FBQ3hDO0FBQ0ksZ0JBQUksRUFBRWQsQ0FBQyxHQUFHRixDQUFDLENBQUNHLElBQU4sRUFBWUQsQ0FBQyxHQUFHQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFYLElBQWdCZixDQUFDLENBQUNBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVosQ0FBbkMsTUFBdURKLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWVBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFoRixDQUFKLEVBQXdGO0FBQUViLGVBQUMsR0FBRyxDQUFKO0FBQU87QUFBVzs7QUFDNUcsZ0JBQUlhLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLEtBQWdCLENBQUNYLENBQUQsSUFBT1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFULElBQWdCVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQWhELENBQUosRUFBMkQ7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVcUMsRUFBRSxDQUFDLENBQUQsQ0FBWjtBQUFpQjtBQUFROztBQUN0RixnQkFBSUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZWIsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBOUIsRUFBbUM7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDtBQUFnQkEsZUFBQyxHQUFHVyxFQUFKO0FBQVE7QUFBUTs7QUFDckUsZ0JBQUlYLENBQUMsSUFBSUYsQ0FBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBcEIsRUFBeUI7QUFBRUYsZUFBQyxDQUFDeEIsS0FBRixHQUFVMEIsQ0FBQyxDQUFDLENBQUQsQ0FBWDs7QUFBZ0JGLGVBQUMsQ0FBQ0ksR0FBRixDQUFNYyxJQUFOLENBQVdMLEVBQVg7O0FBQWdCO0FBQVE7O0FBQ25FLGdCQUFJWCxDQUFDLENBQUMsQ0FBRCxDQUFMLEVBQVVGLENBQUMsQ0FBQ0ksR0FBRixDQUFNWSxHQUFOOztBQUNWaEIsYUFBQyxDQUFDRyxJQUFGLENBQU9hLEdBQVA7O0FBQWM7QUFYdEI7O0FBYUFILFVBQUUsR0FBR2QsSUFBSSxDQUFDZ0IsSUFBTCxDQUFVbEMsT0FBVixFQUFtQm1CLENBQW5CLENBQUw7QUFDSCxPQWpCUyxDQWlCUixPQUFPUixDQUFQLEVBQVU7QUFBRXFCLFVBQUUsR0FBRyxDQUFDLENBQUQsRUFBSXJCLENBQUosQ0FBTDtBQUFhYyxTQUFDLEdBQUcsQ0FBSjtBQUFRLE9BakJ6QixTQWlCa0M7QUFBRUQsU0FBQyxHQUFHSCxDQUFDLEdBQUcsQ0FBUjtBQUFZO0FBakIxRDs7QUFrQkEsUUFBSVcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVosRUFBZSxNQUFNQSxFQUFFLENBQUMsQ0FBRCxDQUFSO0FBQWEsV0FBTztBQUFFM0IsV0FBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRQSxFQUFFLENBQUMsQ0FBRCxDQUFWLEdBQWdCLEtBQUssQ0FBOUI7QUFBaUNsQixVQUFJLEVBQUU7QUFBdkMsS0FBUDtBQUMvQjtBQUNKLENBMUJEOztBQTJCQTlELE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjtBQUNBRCxPQUFPLENBQUMwUCxZQUFSLEdBQXVCLEtBQUssQ0FBNUI7O0FBQ0EsSUFBSWhQLFVBQVUsR0FBR1IsbUJBQU8sQ0FBQyxtR0FBRCxDQUF4Qjs7QUFDQSxJQUFJTyxPQUFPLEdBQUdQLG1CQUFPLENBQUMsK0VBQUQsQ0FBckI7O0FBQ0EsSUFBSTZRLE9BQU8sR0FBRzdRLG1CQUFPLENBQUMsbUZBQUQsQ0FBckI7O0FBQ0EsSUFBSXdQLFlBQVk7QUFBRztBQUFlLFlBQVk7QUFDMUMsV0FBU0EsWUFBVCxDQUFzQjNPLEtBQXRCLEVBQTZCO0FBQ3pCLFNBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNIOztBQUNEMk8sY0FBWSxDQUFDdE8sU0FBYixDQUF1QnlPLFNBQXZCLEdBQW1DLFVBQVV4SixLQUFWLEVBQWlCO0FBQ2hELFdBQU90RCxTQUFTLENBQUMsSUFBRCxFQUFPLEtBQUssQ0FBWixFQUFlLEtBQUssQ0FBcEIsRUFBdUIsWUFBWTtBQUMvQyxhQUFPa0IsV0FBVyxDQUFDLElBQUQsRUFBTyxVQUFVOEIsRUFBVixFQUFjO0FBQ25DLGdCQUFRQSxFQUFFLENBQUNwRCxLQUFYO0FBQ0ksZUFBSyxDQUFMO0FBQVEsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBY29PLE9BQU8sQ0FBQyxTQUFELENBQVAsQ0FBbUJTLElBQW5CLENBQXdCLHdCQUF4QixFQUFrRG5MLEtBQWxELEVBQXlEO0FBQzlFNEsscUJBQU8sRUFBRTtBQUNMLGlDQUFpQixZQUFZLEtBQUtsUSxLQUFMLENBQVdJO0FBRG5DO0FBRHFFLGFBQXpELEVBSXRCNEMsSUFKc0IsQ0FJakIsVUFBVW1OLFFBQVYsRUFBb0I7QUFDeEIxRCxxQkFBTyxDQUFDRCxHQUFSLENBQVkyRCxRQUFaO0FBQ0EscUJBQU9BLFFBQVEsQ0FBQ3ZLLElBQWhCO0FBQ0gsYUFQd0IsQ0FBZCxDQUFQOztBQVFSLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWVaLEVBQUUsQ0FBQzNCLElBQUgsRUFBZixDQUFQO0FBVFo7QUFXSCxPQVppQixDQUFsQjtBQWFILEtBZGUsQ0FBaEI7QUFlSCxHQWhCRDs7QUFpQkFzTCxjQUFZLEdBQUdPLFVBQVUsQ0FBQyxDQUN0QixDQUFDLEdBQUd2UCxVQUFVLENBQUMyUSxTQUFmLEdBRHNCLEVBRXRCLENBQUMsR0FBRzNRLFVBQVUsQ0FBQzRRLGNBQWYsR0FGc0IsRUFHdEJWLFVBQVUsQ0FBQyxtQkFBRCxFQUFzQixDQUFDblEsT0FBTyxDQUFDUyxLQUFULENBQXRCLENBSFksQ0FBRCxFQUl0QndPLFlBSnNCLENBQXpCO0FBS0EsU0FBT0EsWUFBUDtBQUNILENBM0JpQyxFQUFsQzs7QUE0QkExUCxPQUFPLENBQUMwUCxZQUFSLEdBQXVCQSxZQUF2QixDOzs7Ozs7Ozs7Ozs7QUMvRWE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ2IsSUFBSU8sVUFBVSxHQUFJLFFBQVEsS0FBS0EsVUFBZCxJQUE2QixVQUFVQyxVQUFWLEVBQXNCQyxNQUF0QixFQUE4QkMsR0FBOUIsRUFBbUNDLElBQW5DLEVBQXlDO0FBQ25GLE1BQUlDLENBQUMsR0FBR2hILFNBQVMsQ0FBQ2xFLE1BQWxCO0FBQUEsTUFBMEJ5SixDQUFDLEdBQUd5QixDQUFDLEdBQUcsQ0FBSixHQUFRSCxNQUFSLEdBQWlCRSxJQUFJLEtBQUssSUFBVCxHQUFnQkEsSUFBSSxHQUFHbkgsTUFBTSxDQUFDcUgsd0JBQVAsQ0FBZ0NKLE1BQWhDLEVBQXdDQyxHQUF4QyxDQUF2QixHQUFzRUMsSUFBckg7QUFBQSxNQUEySEcsQ0FBM0g7QUFDQSxNQUFJLFFBQU9DLE9BQVAseUNBQU9BLE9BQVAsT0FBbUIsUUFBbkIsSUFBK0IsT0FBT0EsT0FBTyxDQUFDQyxRQUFmLEtBQTRCLFVBQS9ELEVBQTJFN0IsQ0FBQyxHQUFHNEIsT0FBTyxDQUFDQyxRQUFSLENBQWlCUixVQUFqQixFQUE2QkMsTUFBN0IsRUFBcUNDLEdBQXJDLEVBQTBDQyxJQUExQyxDQUFKLENBQTNFLEtBQ0ssS0FBSyxJQUFJaEgsQ0FBQyxHQUFHNkcsVUFBVSxDQUFDOUssTUFBWCxHQUFvQixDQUFqQyxFQUFvQ2lFLENBQUMsSUFBSSxDQUF6QyxFQUE0Q0EsQ0FBQyxFQUE3QztBQUFpRCxRQUFJbUgsQ0FBQyxHQUFHTixVQUFVLENBQUM3RyxDQUFELENBQWxCLEVBQXVCd0YsQ0FBQyxHQUFHLENBQUN5QixDQUFDLEdBQUcsQ0FBSixHQUFRRSxDQUFDLENBQUMzQixDQUFELENBQVQsR0FBZXlCLENBQUMsR0FBRyxDQUFKLEdBQVFFLENBQUMsQ0FBQ0wsTUFBRCxFQUFTQyxHQUFULEVBQWN2QixDQUFkLENBQVQsR0FBNEIyQixDQUFDLENBQUNMLE1BQUQsRUFBU0MsR0FBVCxDQUE3QyxLQUErRHZCLENBQW5FO0FBQXhFO0FBQ0wsU0FBT3lCLENBQUMsR0FBRyxDQUFKLElBQVN6QixDQUFULElBQWMzRixNQUFNLENBQUN5SCxjQUFQLENBQXNCUixNQUF0QixFQUE4QkMsR0FBOUIsRUFBbUN2QixDQUFuQyxDQUFkLEVBQXFEQSxDQUE1RDtBQUNILENBTEQ7O0FBTUEsSUFBSStCLFVBQVUsR0FBSSxRQUFRLEtBQUtBLFVBQWQsSUFBNkIsVUFBVUMsQ0FBVixFQUFhOUwsQ0FBYixFQUFnQjtBQUMxRCxNQUFJLFFBQU8wTCxPQUFQLHlDQUFPQSxPQUFQLE9BQW1CLFFBQW5CLElBQStCLE9BQU9BLE9BQU8sQ0FBQ0ssUUFBZixLQUE0QixVQUEvRCxFQUEyRSxPQUFPTCxPQUFPLENBQUNLLFFBQVIsQ0FBaUJELENBQWpCLEVBQW9COUwsQ0FBcEIsQ0FBUDtBQUM5RSxDQUZEOztBQUdBLElBQUloQyxTQUFTLEdBQUksUUFBUSxLQUFLQSxTQUFkLElBQTRCLFVBQVVDLE9BQVYsRUFBbUJDLFVBQW5CLEVBQStCQyxDQUEvQixFQUFrQ0MsU0FBbEMsRUFBNkM7QUFDckYsV0FBU0MsS0FBVCxDQUFlQyxLQUFmLEVBQXNCO0FBQUUsV0FBT0EsS0FBSyxZQUFZSCxDQUFqQixHQUFxQkcsS0FBckIsR0FBNkIsSUFBSUgsQ0FBSixDQUFNLFVBQVVqQyxPQUFWLEVBQW1CO0FBQUVBLGFBQU8sQ0FBQ29DLEtBQUQsQ0FBUDtBQUFpQixLQUE1QyxDQUFwQztBQUFvRjs7QUFDNUcsU0FBTyxLQUFLSCxDQUFDLEtBQUtBLENBQUMsR0FBR0ksT0FBVCxDQUFOLEVBQXlCLFVBQVVyQyxPQUFWLEVBQW1Cc0MsTUFBbkIsRUFBMkI7QUFDdkQsYUFBU0MsU0FBVCxDQUFtQkgsS0FBbkIsRUFBMEI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDTyxJQUFWLENBQWVMLEtBQWYsQ0FBRCxDQUFKO0FBQThCLE9BQXBDLENBQXFDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzNGLGFBQVNDLFFBQVQsQ0FBa0JQLEtBQWxCLEVBQXlCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQyxPQUFELENBQVQsQ0FBbUJFLEtBQW5CLENBQUQsQ0FBSjtBQUFrQyxPQUF4QyxDQUF5QyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUM5RixhQUFTRixJQUFULENBQWNJLE1BQWQsRUFBc0I7QUFBRUEsWUFBTSxDQUFDQyxJQUFQLEdBQWM3QyxPQUFPLENBQUM0QyxNQUFNLENBQUNSLEtBQVIsQ0FBckIsR0FBc0NELEtBQUssQ0FBQ1MsTUFBTSxDQUFDUixLQUFSLENBQUwsQ0FBb0JVLElBQXBCLENBQXlCUCxTQUF6QixFQUFvQ0ksUUFBcEMsQ0FBdEM7QUFBc0Y7O0FBQzlHSCxRQUFJLENBQUMsQ0FBQ04sU0FBUyxHQUFHQSxTQUFTLENBQUNhLEtBQVYsQ0FBZ0JoQixPQUFoQixFQUF5QkMsVUFBVSxJQUFJLEVBQXZDLENBQWIsRUFBeURTLElBQXpELEVBQUQsQ0FBSjtBQUNILEdBTE0sQ0FBUDtBQU1ILENBUkQ7O0FBU0EsSUFBSU8sV0FBVyxHQUFJLFFBQVEsS0FBS0EsV0FBZCxJQUE4QixVQUFVakIsT0FBVixFQUFtQmtCLElBQW5CLEVBQXlCO0FBQ3JFLE1BQUlDLENBQUMsR0FBRztBQUFFeEIsU0FBSyxFQUFFLENBQVQ7QUFBWXlCLFFBQUksRUFBRSxnQkFBVztBQUFFLFVBQUlDLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBTyxDQUFYLEVBQWMsTUFBTUEsQ0FBQyxDQUFDLENBQUQsQ0FBUDtBQUFZLGFBQU9BLENBQUMsQ0FBQyxDQUFELENBQVI7QUFBYyxLQUF2RTtBQUF5RUMsUUFBSSxFQUFFLEVBQS9FO0FBQW1GQyxPQUFHLEVBQUU7QUFBeEYsR0FBUjtBQUFBLE1BQXNHQyxDQUF0RztBQUFBLE1BQXlHQyxDQUF6RztBQUFBLE1BQTRHSixDQUE1RztBQUFBLE1BQStHSyxDQUEvRztBQUNBLFNBQU9BLENBQUMsR0FBRztBQUFFaEIsUUFBSSxFQUFFaUIsSUFBSSxDQUFDLENBQUQsQ0FBWjtBQUFpQixhQUFTQSxJQUFJLENBQUMsQ0FBRCxDQUE5QjtBQUFtQyxjQUFVQSxJQUFJLENBQUMsQ0FBRDtBQUFqRCxHQUFKLEVBQTRELE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsS0FBaUNGLENBQUMsQ0FBQ0UsTUFBTSxDQUFDQyxRQUFSLENBQUQsR0FBcUIsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLEdBQWpGLENBQTVELEVBQWdKSCxDQUF2Sjs7QUFDQSxXQUFTQyxJQUFULENBQWNHLENBQWQsRUFBaUI7QUFBRSxXQUFPLFVBQVVDLENBQVYsRUFBYTtBQUFFLGFBQU90QixJQUFJLENBQUMsQ0FBQ3FCLENBQUQsRUFBSUMsQ0FBSixDQUFELENBQVg7QUFBc0IsS0FBNUM7QUFBK0M7O0FBQ2xFLFdBQVN0QixJQUFULENBQWN1QixFQUFkLEVBQWtCO0FBQ2QsUUFBSVIsQ0FBSixFQUFPLE1BQU0sSUFBSVMsU0FBSixDQUFjLGlDQUFkLENBQU47O0FBQ1AsV0FBT2QsQ0FBUDtBQUFVLFVBQUk7QUFDVixZQUFJSyxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEtBQUtKLENBQUMsR0FBR1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVIsR0FBWVAsQ0FBQyxDQUFDLFFBQUQsQ0FBYixHQUEwQk8sRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRUCxDQUFDLENBQUMsT0FBRCxDQUFELEtBQWUsQ0FBQ0osQ0FBQyxHQUFHSSxDQUFDLENBQUMsUUFBRCxDQUFOLEtBQXFCSixDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxDQUFyQixFQUFnQyxDQUEvQyxDQUFSLEdBQTREQSxDQUFDLENBQUNmLElBQWpHLENBQUQsSUFBMkcsQ0FBQyxDQUFDVyxDQUFDLEdBQUdBLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLEVBQVVPLEVBQUUsQ0FBQyxDQUFELENBQVosQ0FBTCxFQUF1QmxCLElBQTlJLEVBQW9KLE9BQU9PLENBQVA7QUFDcEosWUFBSUksQ0FBQyxHQUFHLENBQUosRUFBT0osQ0FBWCxFQUFjVyxFQUFFLEdBQUcsQ0FBQ0EsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVQsRUFBWVgsQ0FBQyxDQUFDaEIsS0FBZCxDQUFMOztBQUNkLGdCQUFRMkIsRUFBRSxDQUFDLENBQUQsQ0FBVjtBQUNJLGVBQUssQ0FBTDtBQUFRLGVBQUssQ0FBTDtBQUFRWCxhQUFDLEdBQUdXLEVBQUo7QUFBUTs7QUFDeEIsZUFBSyxDQUFMO0FBQVFiLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVyxtQkFBTztBQUFFVSxtQkFBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBWDtBQUFnQmxCLGtCQUFJLEVBQUU7QUFBdEIsYUFBUDs7QUFDbkIsZUFBSyxDQUFMO0FBQVFLLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVzhCLGFBQUMsR0FBR08sRUFBRSxDQUFDLENBQUQsQ0FBTjtBQUFXQSxjQUFFLEdBQUcsQ0FBQyxDQUFELENBQUw7QUFBVTs7QUFDeEMsZUFBSyxDQUFMO0FBQVFBLGNBQUUsR0FBR2IsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU4sRUFBTDs7QUFBa0JoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYzs7QUFDeEM7QUFDSSxnQkFBSSxFQUFFZCxDQUFDLEdBQUdGLENBQUMsQ0FBQ0csSUFBTixFQUFZRCxDQUFDLEdBQUdBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVgsSUFBZ0JmLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWixDQUFuQyxNQUF1REosRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQWhGLENBQUosRUFBd0Y7QUFBRWIsZUFBQyxHQUFHLENBQUo7QUFBTztBQUFXOztBQUM1RyxnQkFBSWEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsS0FBZ0IsQ0FBQ1gsQ0FBRCxJQUFPVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQVQsSUFBZ0JXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBaEQsQ0FBSixFQUEyRDtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVVxQyxFQUFFLENBQUMsQ0FBRCxDQUFaO0FBQWlCO0FBQVE7O0FBQ3RGLGdCQUFJQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlYixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUE5QixFQUFtQztBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYO0FBQWdCQSxlQUFDLEdBQUdXLEVBQUo7QUFBUTtBQUFROztBQUNyRSxnQkFBSVgsQ0FBQyxJQUFJRixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFwQixFQUF5QjtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYOztBQUFnQkYsZUFBQyxDQUFDSSxHQUFGLENBQU1jLElBQU4sQ0FBV0wsRUFBWDs7QUFBZ0I7QUFBUTs7QUFDbkUsZ0JBQUlYLENBQUMsQ0FBQyxDQUFELENBQUwsRUFBVUYsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU47O0FBQ1ZoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYztBQVh0Qjs7QUFhQUgsVUFBRSxHQUFHZCxJQUFJLENBQUNnQixJQUFMLENBQVVsQyxPQUFWLEVBQW1CbUIsQ0FBbkIsQ0FBTDtBQUNILE9BakJTLENBaUJSLE9BQU9SLENBQVAsRUFBVTtBQUFFcUIsVUFBRSxHQUFHLENBQUMsQ0FBRCxFQUFJckIsQ0FBSixDQUFMO0FBQWFjLFNBQUMsR0FBRyxDQUFKO0FBQVEsT0FqQnpCLFNBaUJrQztBQUFFRCxTQUFDLEdBQUdILENBQUMsR0FBRyxDQUFSO0FBQVk7QUFqQjFEOztBQWtCQSxRQUFJVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBWixFQUFlLE1BQU1BLEVBQUUsQ0FBQyxDQUFELENBQVI7QUFBYSxXQUFPO0FBQUUzQixXQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFBLEVBQUUsQ0FBQyxDQUFELENBQVYsR0FBZ0IsS0FBSyxDQUE5QjtBQUFpQ2xCLFVBQUksRUFBRTtBQUF2QyxLQUFQO0FBQy9CO0FBQ0osQ0ExQkQ7O0FBMkJBOUQsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELE9BQU8sQ0FBQ3FOLGNBQVIsR0FBeUIsS0FBSyxDQUE5Qjs7QUFDQSxJQUFJM00sVUFBVSxHQUFHUixtQkFBTyxDQUFDLG1HQUFELENBQXhCOztBQUNBLElBQUlPLE9BQU8sR0FBR1AsbUJBQU8sQ0FBQywrRUFBRCxDQUFyQjs7QUFDQSxJQUFJNlEsT0FBTyxHQUFHN1EsbUJBQU8sQ0FBQyxtRkFBRCxDQUFyQjs7QUFDQSxJQUFJa0wsU0FBUyxHQUFHbEwsbUJBQU8sQ0FBQyx1RkFBRCxDQUF2Qjs7QUFDQSxJQUFJbU4sY0FBYztBQUFHO0FBQWUsWUFBWTtBQUM1QyxXQUFTQSxjQUFULENBQXdCdE0sS0FBeEIsRUFBK0I7QUFDM0IsU0FBS0EsS0FBTCxHQUFhQSxLQUFiO0FBQ0g7O0FBQ0RzTSxnQkFBYyxDQUFDak0sU0FBZixDQUF5QjJOLGlCQUF6QixHQUE2QyxVQUFVMEMsSUFBVixFQUFnQnJMLElBQWhCLEVBQXNCO0FBQy9ELFdBQU9yRCxTQUFTLENBQUMsSUFBRCxFQUFPLEtBQUssQ0FBWixFQUFlLEtBQUssQ0FBcEIsRUFBdUIsWUFBWTtBQUMvQyxhQUFPa0IsV0FBVyxDQUFDLElBQUQsRUFBTyxVQUFVOEIsRUFBVixFQUFjO0FBQ25DLGdCQUFRQSxFQUFFLENBQUNwRCxLQUFYO0FBQ0ksZUFBSyxDQUFMO0FBQVEsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBY29PLE9BQU8sQ0FBQyxTQUFELENBQVAsQ0FBbUJDLEdBQW5CLENBQXVCLG9DQUFvQzVLLElBQUksQ0FBQ3JFLElBQXpDLEdBQWdELEdBQWhELEdBQXNEMFAsSUFBN0UsRUFBbUY7QUFDeEdSLHFCQUFPLEVBQUU7QUFDTCxpQ0FBaUIsWUFBWSxLQUFLbFEsS0FBTCxDQUFXSTtBQURuQztBQUQrRixhQUFuRixFQUl0QjRDLElBSnNCLENBSWpCLFVBQVVtTixRQUFWLEVBQW9CO0FBQ3hCLHFCQUFPQSxRQUFRLENBQUN2SyxJQUFULENBQWNBLElBQWQsQ0FBbUJ3SyxHQUFuQixDQUF1QixVQUFVQyxJQUFWLEVBQWdCO0FBQzFDLG9CQUFJN0gsQ0FBQyxHQUFHLElBQUk2QixTQUFTLENBQUMzSSxPQUFkLEVBQVI7QUFDQThHLGlCQUFDLENBQUN6SCxFQUFGLEdBQU9zUCxJQUFJLENBQUN0UCxFQUFaO0FBQ0F5SCxpQkFBQyxDQUFDckgsS0FBRixHQUFVa1AsSUFBSSxDQUFDbFAsS0FBZjtBQUNBcUgsaUJBQUMsQ0FBQ3hILElBQUYsR0FBU3FQLElBQUksQ0FBQ3JQLElBQWQ7QUFDQSx1QkFBT3dILENBQVA7QUFDSCxlQU5NLENBQVA7QUFPSCxhQVp3QixDQUFkLENBQVA7O0FBYVIsZUFBSyxDQUFMO0FBQVEsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBZXhELEVBQUUsQ0FBQzNCLElBQUgsRUFBZixDQUFQO0FBZFo7QUFnQkgsT0FqQmlCLENBQWxCO0FBa0JILEtBbkJlLENBQWhCO0FBb0JILEdBckJEOztBQXNCQWlKLGdCQUFjLENBQUNqTSxTQUFmLENBQXlCNE4sV0FBekIsR0FBdUMsVUFBVWhELFlBQVYsRUFBd0I7QUFDM0QsV0FBT2pKLFNBQVMsQ0FBQyxJQUFELEVBQU8sS0FBSyxDQUFaLEVBQWUsS0FBSyxDQUFwQixFQUF1QixZQUFZO0FBQy9DLGFBQU9rQixXQUFXLENBQUMsSUFBRCxFQUFPLFVBQVU4QixFQUFWLEVBQWM7QUFDbkMsZ0JBQVFBLEVBQUUsQ0FBQ3BELEtBQVg7QUFDSSxlQUFLLENBQUw7QUFBUSxtQkFBTyxDQUFDO0FBQUU7QUFBSCxjQUFjb08sT0FBTyxDQUFDLFNBQUQsQ0FBUCxDQUFtQkMsR0FBbkIsQ0FBdUIsNENBQTRDaEYsWUFBWSxDQUFDakssSUFBaEYsRUFBc0Y7QUFDM0drUCxxQkFBTyxFQUFFO0FBQ0wsaUNBQWlCLFlBQVksS0FBS2xRLEtBQUwsQ0FBV0k7QUFEbkM7QUFEa0csYUFBdEYsRUFJdEI0QyxJQUpzQixDQUlqQixVQUFVbU4sUUFBVixFQUFvQjtBQUN4QixxQkFBT0EsUUFBUSxDQUFDdkssSUFBVCxDQUFjQSxJQUFkLENBQW1Cd0ssR0FBbkIsQ0FBdUIsVUFBVUMsSUFBVixFQUFnQjtBQUMxQyxvQkFBSTdILENBQUMsR0FBRyxJQUFJNkIsU0FBUyxDQUFDM0ksT0FBZCxFQUFSO0FBQ0E4RyxpQkFBQyxDQUFDekgsRUFBRixHQUFPc1AsSUFBSSxDQUFDdFAsRUFBWjtBQUNBeUgsaUJBQUMsQ0FBQ3JILEtBQUYsR0FBVWtQLElBQUksQ0FBQ2xQLEtBQWY7QUFDQXFILGlCQUFDLENBQUN4SCxJQUFGLEdBQVNxUCxJQUFJLENBQUNyUCxJQUFkO0FBQ0EsdUJBQU93SCxDQUFQO0FBQ0gsZUFOTSxDQUFQO0FBT0gsYUFad0IsQ0FBZCxDQUFQOztBQWFSLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWV4RCxFQUFFLENBQUMzQixJQUFILEVBQWYsQ0FBUDtBQWRaO0FBZ0JILE9BakJpQixDQUFsQjtBQWtCSCxLQW5CZSxDQUFoQjtBQW9CSCxHQXJCRDs7QUFzQkFpSixnQkFBYyxDQUFDak0sU0FBZixDQUF5QmtNLG1CQUF6QixHQUErQyxVQUFVeEIsT0FBVixFQUFtQjtBQUM5RCxXQUFPL0ksU0FBUyxDQUFDLElBQUQsRUFBTyxLQUFLLENBQVosRUFBZSxLQUFLLENBQXBCLEVBQXVCLFlBQVk7QUFDL0MsYUFBT2tCLFdBQVcsQ0FBQyxJQUFELEVBQU8sVUFBVThCLEVBQVYsRUFBYztBQUNuQyxnQkFBUUEsRUFBRSxDQUFDcEQsS0FBWDtBQUNJLGVBQUssQ0FBTDtBQUFRLG1CQUFPLENBQUM7QUFBRTtBQUFILGNBQWNvTyxPQUFPLENBQUMsU0FBRCxDQUFQLENBQW1CUyxJQUFuQixDQUF3QixxQ0FBcUMxRixPQUFPLENBQUMvSixJQUFyRSxFQUEyRSxFQUEzRSxFQUErRTtBQUNwR2tQLHFCQUFPLEVBQUU7QUFDTCxpQ0FBaUIsWUFBWSxLQUFLbFEsS0FBTCxDQUFXSTtBQURuQztBQUQyRixhQUEvRSxFQUl0QjRDLElBSnNCLENBSWpCLFVBQVVtTixRQUFWLEVBQW9CO0FBQ3hCLHFCQUFPQSxRQUFRLENBQUN2SyxJQUFoQjtBQUNILGFBTndCLENBQWQsQ0FBUDs7QUFPUixlQUFLLENBQUw7QUFBUSxtQkFBTyxDQUFDO0FBQUU7QUFBSCxjQUFlWixFQUFFLENBQUMzQixJQUFILEVBQWYsQ0FBUDtBQVJaO0FBVUgsT0FYaUIsQ0FBbEI7QUFZSCxLQWJlLENBQWhCO0FBY0gsR0FmRDs7QUFnQkFpSixnQkFBYyxHQUFHNEMsVUFBVSxDQUFDLENBQ3hCLENBQUMsR0FBR3ZQLFVBQVUsQ0FBQzJRLFNBQWYsR0FEd0IsRUFFeEIsQ0FBQyxHQUFHM1EsVUFBVSxDQUFDNFEsY0FBZixHQUZ3QixFQUd4QlYsVUFBVSxDQUFDLG1CQUFELEVBQXNCLENBQUNuUSxPQUFPLENBQUNTLEtBQVQsQ0FBdEIsQ0FIYyxDQUFELEVBSXhCbU0sY0FKd0IsQ0FBM0I7QUFLQSxTQUFPQSxjQUFQO0FBQ0gsQ0F0RW1DLEVBQXBDOztBQXVFQXJOLE9BQU8sQ0FBQ3FOLGNBQVIsR0FBeUJBLGNBQXpCLEM7Ozs7Ozs7Ozs7OztBQzNIYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDYixJQUFJNEMsVUFBVSxHQUFJLFFBQVEsS0FBS0EsVUFBZCxJQUE2QixVQUFVQyxVQUFWLEVBQXNCQyxNQUF0QixFQUE4QkMsR0FBOUIsRUFBbUNDLElBQW5DLEVBQXlDO0FBQ25GLE1BQUlDLENBQUMsR0FBR2hILFNBQVMsQ0FBQ2xFLE1BQWxCO0FBQUEsTUFBMEJ5SixDQUFDLEdBQUd5QixDQUFDLEdBQUcsQ0FBSixHQUFRSCxNQUFSLEdBQWlCRSxJQUFJLEtBQUssSUFBVCxHQUFnQkEsSUFBSSxHQUFHbkgsTUFBTSxDQUFDcUgsd0JBQVAsQ0FBZ0NKLE1BQWhDLEVBQXdDQyxHQUF4QyxDQUF2QixHQUFzRUMsSUFBckg7QUFBQSxNQUEySEcsQ0FBM0g7QUFDQSxNQUFJLFFBQU9DLE9BQVAseUNBQU9BLE9BQVAsT0FBbUIsUUFBbkIsSUFBK0IsT0FBT0EsT0FBTyxDQUFDQyxRQUFmLEtBQTRCLFVBQS9ELEVBQTJFN0IsQ0FBQyxHQUFHNEIsT0FBTyxDQUFDQyxRQUFSLENBQWlCUixVQUFqQixFQUE2QkMsTUFBN0IsRUFBcUNDLEdBQXJDLEVBQTBDQyxJQUExQyxDQUFKLENBQTNFLEtBQ0ssS0FBSyxJQUFJaEgsQ0FBQyxHQUFHNkcsVUFBVSxDQUFDOUssTUFBWCxHQUFvQixDQUFqQyxFQUFvQ2lFLENBQUMsSUFBSSxDQUF6QyxFQUE0Q0EsQ0FBQyxFQUE3QztBQUFpRCxRQUFJbUgsQ0FBQyxHQUFHTixVQUFVLENBQUM3RyxDQUFELENBQWxCLEVBQXVCd0YsQ0FBQyxHQUFHLENBQUN5QixDQUFDLEdBQUcsQ0FBSixHQUFRRSxDQUFDLENBQUMzQixDQUFELENBQVQsR0FBZXlCLENBQUMsR0FBRyxDQUFKLEdBQVFFLENBQUMsQ0FBQ0wsTUFBRCxFQUFTQyxHQUFULEVBQWN2QixDQUFkLENBQVQsR0FBNEIyQixDQUFDLENBQUNMLE1BQUQsRUFBU0MsR0FBVCxDQUE3QyxLQUErRHZCLENBQW5FO0FBQXhFO0FBQ0wsU0FBT3lCLENBQUMsR0FBRyxDQUFKLElBQVN6QixDQUFULElBQWMzRixNQUFNLENBQUN5SCxjQUFQLENBQXNCUixNQUF0QixFQUE4QkMsR0FBOUIsRUFBbUN2QixDQUFuQyxDQUFkLEVBQXFEQSxDQUE1RDtBQUNILENBTEQ7O0FBTUEsSUFBSStCLFVBQVUsR0FBSSxRQUFRLEtBQUtBLFVBQWQsSUFBNkIsVUFBVUMsQ0FBVixFQUFhOUwsQ0FBYixFQUFnQjtBQUMxRCxNQUFJLFFBQU8wTCxPQUFQLHlDQUFPQSxPQUFQLE9BQW1CLFFBQW5CLElBQStCLE9BQU9BLE9BQU8sQ0FBQ0ssUUFBZixLQUE0QixVQUEvRCxFQUEyRSxPQUFPTCxPQUFPLENBQUNLLFFBQVIsQ0FBaUJELENBQWpCLEVBQW9COUwsQ0FBcEIsQ0FBUDtBQUM5RSxDQUZEOztBQUdBLElBQUloQyxTQUFTLEdBQUksUUFBUSxLQUFLQSxTQUFkLElBQTRCLFVBQVVDLE9BQVYsRUFBbUJDLFVBQW5CLEVBQStCQyxDQUEvQixFQUFrQ0MsU0FBbEMsRUFBNkM7QUFDckYsV0FBU0MsS0FBVCxDQUFlQyxLQUFmLEVBQXNCO0FBQUUsV0FBT0EsS0FBSyxZQUFZSCxDQUFqQixHQUFxQkcsS0FBckIsR0FBNkIsSUFBSUgsQ0FBSixDQUFNLFVBQVVqQyxPQUFWLEVBQW1CO0FBQUVBLGFBQU8sQ0FBQ29DLEtBQUQsQ0FBUDtBQUFpQixLQUE1QyxDQUFwQztBQUFvRjs7QUFDNUcsU0FBTyxLQUFLSCxDQUFDLEtBQUtBLENBQUMsR0FBR0ksT0FBVCxDQUFOLEVBQXlCLFVBQVVyQyxPQUFWLEVBQW1Cc0MsTUFBbkIsRUFBMkI7QUFDdkQsYUFBU0MsU0FBVCxDQUFtQkgsS0FBbkIsRUFBMEI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDTyxJQUFWLENBQWVMLEtBQWYsQ0FBRCxDQUFKO0FBQThCLE9BQXBDLENBQXFDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzNGLGFBQVNDLFFBQVQsQ0FBa0JQLEtBQWxCLEVBQXlCO0FBQUUsVUFBSTtBQUFFSSxZQUFJLENBQUNOLFNBQVMsQ0FBQyxPQUFELENBQVQsQ0FBbUJFLEtBQW5CLENBQUQsQ0FBSjtBQUFrQyxPQUF4QyxDQUF5QyxPQUFPTSxDQUFQLEVBQVU7QUFBRUosY0FBTSxDQUFDSSxDQUFELENBQU47QUFBWTtBQUFFOztBQUM5RixhQUFTRixJQUFULENBQWNJLE1BQWQsRUFBc0I7QUFBRUEsWUFBTSxDQUFDQyxJQUFQLEdBQWM3QyxPQUFPLENBQUM0QyxNQUFNLENBQUNSLEtBQVIsQ0FBckIsR0FBc0NELEtBQUssQ0FBQ1MsTUFBTSxDQUFDUixLQUFSLENBQUwsQ0FBb0JVLElBQXBCLENBQXlCUCxTQUF6QixFQUFvQ0ksUUFBcEMsQ0FBdEM7QUFBc0Y7O0FBQzlHSCxRQUFJLENBQUMsQ0FBQ04sU0FBUyxHQUFHQSxTQUFTLENBQUNhLEtBQVYsQ0FBZ0JoQixPQUFoQixFQUF5QkMsVUFBVSxJQUFJLEVBQXZDLENBQWIsRUFBeURTLElBQXpELEVBQUQsQ0FBSjtBQUNILEdBTE0sQ0FBUDtBQU1ILENBUkQ7O0FBU0EsSUFBSU8sV0FBVyxHQUFJLFFBQVEsS0FBS0EsV0FBZCxJQUE4QixVQUFVakIsT0FBVixFQUFtQmtCLElBQW5CLEVBQXlCO0FBQ3JFLE1BQUlDLENBQUMsR0FBRztBQUFFeEIsU0FBSyxFQUFFLENBQVQ7QUFBWXlCLFFBQUksRUFBRSxnQkFBVztBQUFFLFVBQUlDLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBTyxDQUFYLEVBQWMsTUFBTUEsQ0FBQyxDQUFDLENBQUQsQ0FBUDtBQUFZLGFBQU9BLENBQUMsQ0FBQyxDQUFELENBQVI7QUFBYyxLQUF2RTtBQUF5RUMsUUFBSSxFQUFFLEVBQS9FO0FBQW1GQyxPQUFHLEVBQUU7QUFBeEYsR0FBUjtBQUFBLE1BQXNHQyxDQUF0RztBQUFBLE1BQXlHQyxDQUF6RztBQUFBLE1BQTRHSixDQUE1RztBQUFBLE1BQStHSyxDQUEvRztBQUNBLFNBQU9BLENBQUMsR0FBRztBQUFFaEIsUUFBSSxFQUFFaUIsSUFBSSxDQUFDLENBQUQsQ0FBWjtBQUFpQixhQUFTQSxJQUFJLENBQUMsQ0FBRCxDQUE5QjtBQUFtQyxjQUFVQSxJQUFJLENBQUMsQ0FBRDtBQUFqRCxHQUFKLEVBQTRELE9BQU9DLE1BQVAsS0FBa0IsVUFBbEIsS0FBaUNGLENBQUMsQ0FBQ0UsTUFBTSxDQUFDQyxRQUFSLENBQUQsR0FBcUIsWUFBVztBQUFFLFdBQU8sSUFBUDtBQUFjLEdBQWpGLENBQTVELEVBQWdKSCxDQUF2Sjs7QUFDQSxXQUFTQyxJQUFULENBQWNHLENBQWQsRUFBaUI7QUFBRSxXQUFPLFVBQVVDLENBQVYsRUFBYTtBQUFFLGFBQU90QixJQUFJLENBQUMsQ0FBQ3FCLENBQUQsRUFBSUMsQ0FBSixDQUFELENBQVg7QUFBc0IsS0FBNUM7QUFBK0M7O0FBQ2xFLFdBQVN0QixJQUFULENBQWN1QixFQUFkLEVBQWtCO0FBQ2QsUUFBSVIsQ0FBSixFQUFPLE1BQU0sSUFBSVMsU0FBSixDQUFjLGlDQUFkLENBQU47O0FBQ1AsV0FBT2QsQ0FBUDtBQUFVLFVBQUk7QUFDVixZQUFJSyxDQUFDLEdBQUcsQ0FBSixFQUFPQyxDQUFDLEtBQUtKLENBQUMsR0FBR1csRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVIsR0FBWVAsQ0FBQyxDQUFDLFFBQUQsQ0FBYixHQUEwQk8sRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRUCxDQUFDLENBQUMsT0FBRCxDQUFELEtBQWUsQ0FBQ0osQ0FBQyxHQUFHSSxDQUFDLENBQUMsUUFBRCxDQUFOLEtBQXFCSixDQUFDLENBQUNhLElBQUYsQ0FBT1QsQ0FBUCxDQUFyQixFQUFnQyxDQUEvQyxDQUFSLEdBQTREQSxDQUFDLENBQUNmLElBQWpHLENBQUQsSUFBMkcsQ0FBQyxDQUFDVyxDQUFDLEdBQUdBLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLEVBQVVPLEVBQUUsQ0FBQyxDQUFELENBQVosQ0FBTCxFQUF1QmxCLElBQTlJLEVBQW9KLE9BQU9PLENBQVA7QUFDcEosWUFBSUksQ0FBQyxHQUFHLENBQUosRUFBT0osQ0FBWCxFQUFjVyxFQUFFLEdBQUcsQ0FBQ0EsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRLENBQVQsRUFBWVgsQ0FBQyxDQUFDaEIsS0FBZCxDQUFMOztBQUNkLGdCQUFRMkIsRUFBRSxDQUFDLENBQUQsQ0FBVjtBQUNJLGVBQUssQ0FBTDtBQUFRLGVBQUssQ0FBTDtBQUFRWCxhQUFDLEdBQUdXLEVBQUo7QUFBUTs7QUFDeEIsZUFBSyxDQUFMO0FBQVFiLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVyxtQkFBTztBQUFFVSxtQkFBSyxFQUFFMkIsRUFBRSxDQUFDLENBQUQsQ0FBWDtBQUFnQmxCLGtCQUFJLEVBQUU7QUFBdEIsYUFBUDs7QUFDbkIsZUFBSyxDQUFMO0FBQVFLLGFBQUMsQ0FBQ3hCLEtBQUY7QUFBVzhCLGFBQUMsR0FBR08sRUFBRSxDQUFDLENBQUQsQ0FBTjtBQUFXQSxjQUFFLEdBQUcsQ0FBQyxDQUFELENBQUw7QUFBVTs7QUFDeEMsZUFBSyxDQUFMO0FBQVFBLGNBQUUsR0FBR2IsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU4sRUFBTDs7QUFBa0JoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYzs7QUFDeEM7QUFDSSxnQkFBSSxFQUFFZCxDQUFDLEdBQUdGLENBQUMsQ0FBQ0csSUFBTixFQUFZRCxDQUFDLEdBQUdBLENBQUMsQ0FBQ2UsTUFBRixHQUFXLENBQVgsSUFBZ0JmLENBQUMsQ0FBQ0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWixDQUFuQyxNQUF1REosRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsSUFBZUEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQWhGLENBQUosRUFBd0Y7QUFBRWIsZUFBQyxHQUFHLENBQUo7QUFBTztBQUFXOztBQUM1RyxnQkFBSWEsRUFBRSxDQUFDLENBQUQsQ0FBRixLQUFVLENBQVYsS0FBZ0IsQ0FBQ1gsQ0FBRCxJQUFPVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFYLENBQUMsQ0FBQyxDQUFELENBQVQsSUFBZ0JXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBaEQsQ0FBSixFQUEyRDtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVVxQyxFQUFFLENBQUMsQ0FBRCxDQUFaO0FBQWlCO0FBQVE7O0FBQ3RGLGdCQUFJQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlYixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUE5QixFQUFtQztBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYO0FBQWdCQSxlQUFDLEdBQUdXLEVBQUo7QUFBUTtBQUFROztBQUNyRSxnQkFBSVgsQ0FBQyxJQUFJRixDQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFwQixFQUF5QjtBQUFFRixlQUFDLENBQUN4QixLQUFGLEdBQVUwQixDQUFDLENBQUMsQ0FBRCxDQUFYOztBQUFnQkYsZUFBQyxDQUFDSSxHQUFGLENBQU1jLElBQU4sQ0FBV0wsRUFBWDs7QUFBZ0I7QUFBUTs7QUFDbkUsZ0JBQUlYLENBQUMsQ0FBQyxDQUFELENBQUwsRUFBVUYsQ0FBQyxDQUFDSSxHQUFGLENBQU1ZLEdBQU47O0FBQ1ZoQixhQUFDLENBQUNHLElBQUYsQ0FBT2EsR0FBUDs7QUFBYztBQVh0Qjs7QUFhQUgsVUFBRSxHQUFHZCxJQUFJLENBQUNnQixJQUFMLENBQVVsQyxPQUFWLEVBQW1CbUIsQ0FBbkIsQ0FBTDtBQUNILE9BakJTLENBaUJSLE9BQU9SLENBQVAsRUFBVTtBQUFFcUIsVUFBRSxHQUFHLENBQUMsQ0FBRCxFQUFJckIsQ0FBSixDQUFMO0FBQWFjLFNBQUMsR0FBRyxDQUFKO0FBQVEsT0FqQnpCLFNBaUJrQztBQUFFRCxTQUFDLEdBQUdILENBQUMsR0FBRyxDQUFSO0FBQVk7QUFqQjFEOztBQWtCQSxRQUFJVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBWixFQUFlLE1BQU1BLEVBQUUsQ0FBQyxDQUFELENBQVI7QUFBYSxXQUFPO0FBQUUzQixXQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFBLEVBQUUsQ0FBQyxDQUFELENBQVYsR0FBZ0IsS0FBSyxDQUE5QjtBQUFpQ2xCLFVBQUksRUFBRTtBQUF2QyxLQUFQO0FBQy9CO0FBQ0osQ0ExQkQ7O0FBMkJBOUQsT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELE9BQU8sQ0FBQ2lPLG1CQUFSLEdBQThCLEtBQUssQ0FBbkM7O0FBQ0EsSUFBSXZOLFVBQVUsR0FBR1IsbUJBQU8sQ0FBQyxtR0FBRCxDQUF4Qjs7QUFDQSxJQUFJTyxPQUFPLEdBQUdQLG1CQUFPLENBQUMsK0VBQUQsQ0FBckI7O0FBQ0EsSUFBSTZRLE9BQU8sR0FBRzdRLG1CQUFPLENBQUMsbUZBQUQsQ0FBckI7O0FBQ0EsSUFBSW1MLGNBQWMsR0FBR25MLG1CQUFPLENBQUMsaUdBQUQsQ0FBNUI7O0FBQ0EsSUFBSStOLG1CQUFtQjtBQUFHO0FBQWUsWUFBWTtBQUNqRCxXQUFTQSxtQkFBVCxDQUE2QmxOLEtBQTdCLEVBQW9DO0FBQ2hDLFNBQUtBLEtBQUwsR0FBYUEsS0FBYjtBQUNIOztBQUNEa04scUJBQW1CLENBQUM3TSxTQUFwQixDQUE4QjhNLGdCQUE5QixHQUFpRCxVQUFVMUgsV0FBVixFQUF1QkosSUFBdkIsRUFBNkI7QUFDMUUsV0FBT3JELFNBQVMsQ0FBQyxJQUFELEVBQU8sS0FBSyxDQUFaLEVBQWUsS0FBSyxDQUFwQixFQUF1QixZQUFZO0FBQy9DLGFBQU9rQixXQUFXLENBQUMsSUFBRCxFQUFPLFVBQVU4QixFQUFWLEVBQWM7QUFDbkMsZ0JBQVFBLEVBQUUsQ0FBQ3BELEtBQVg7QUFDSSxlQUFLLENBQUw7QUFBUSxtQkFBTyxDQUFDO0FBQUU7QUFBSCxjQUFjb08sT0FBTyxDQUFDLFNBQUQsQ0FBUCxDQUFtQkMsR0FBbkIsQ0FBdUIsb0NBQW9DNUssSUFBSSxDQUFDckUsSUFBekMsR0FBZ0QsUUFBaEQsR0FBMkR5RSxXQUFsRixFQUErRjtBQUNwSHlLLHFCQUFPLEVBQUU7QUFDTCxpQ0FBaUIsWUFBWSxLQUFLbFEsS0FBTCxDQUFXSTtBQURuQztBQUQyRyxhQUEvRixFQUl0QjRDLElBSnNCLENBSWpCLFVBQVVtTixRQUFWLEVBQW9CO0FBQ3hCLHFCQUFPQSxRQUFRLENBQUN2SyxJQUFULENBQWNBLElBQWQsQ0FBbUJ3SyxHQUFuQixDQUF1QixVQUFVQyxJQUFWLEVBQWdCO0FBQzFDLG9CQUFJTSxFQUFFLEdBQUcsSUFBSXJHLGNBQWMsQ0FBQzNJLFlBQW5CLEVBQVQ7QUFDQWdQLGtCQUFFLENBQUM1UCxFQUFILEdBQVFzUCxJQUFJLENBQUN0UCxFQUFiO0FBQ0E0UCxrQkFBRSxDQUFDL08sS0FBSCxHQUFXeU8sSUFBSSxDQUFDbFAsS0FBaEI7QUFDQXdQLGtCQUFFLENBQUMzUCxJQUFILEdBQVVxUCxJQUFJLENBQUNyUCxJQUFmO0FBQ0FxUCxvQkFBSSxDQUFDakssUUFBTCxDQUFjZ0ssR0FBZCxDQUFrQixVQUFVUSxHQUFWLEVBQWU7QUFDN0Isc0JBQUlDLEdBQUcsR0FBRyxJQUFJdkcsY0FBYyxDQUFDM0ksWUFBbkIsRUFBVjtBQUNBa1AscUJBQUcsQ0FBQzlQLEVBQUosR0FBUzZQLEdBQUcsQ0FBQzdQLEVBQWI7QUFDQThQLHFCQUFHLENBQUNqUCxLQUFKLEdBQVlnUCxHQUFHLENBQUN6UCxLQUFoQjtBQUNBMFAscUJBQUcsQ0FBQzdQLElBQUosR0FBVzRQLEdBQUcsQ0FBQzVQLElBQWY7QUFDQTJQLG9CQUFFLENBQUM5TyxPQUFILENBQVd5QyxJQUFYLENBQWdCdU0sR0FBaEI7QUFDSCxpQkFORDtBQU9BLHVCQUFPRixFQUFQO0FBQ0gsZUFiTSxDQUFQO0FBY0gsYUFuQndCLENBQWQsQ0FBUDs7QUFvQlIsZUFBSyxDQUFMO0FBQVEsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBZTNMLEVBQUUsQ0FBQzNCLElBQUgsRUFBZixDQUFQO0FBckJaO0FBdUJILE9BeEJpQixDQUFsQjtBQXlCSCxLQTFCZSxDQUFoQjtBQTJCSCxHQTVCRDs7QUE2QkE2SixxQkFBbUIsR0FBR2dDLFVBQVUsQ0FBQyxDQUM3QixDQUFDLEdBQUd2UCxVQUFVLENBQUMyUSxTQUFmLEdBRDZCLEVBRTdCLENBQUMsR0FBRzNRLFVBQVUsQ0FBQzRRLGNBQWYsR0FGNkIsRUFHN0JWLFVBQVUsQ0FBQyxtQkFBRCxFQUFzQixDQUFDblEsT0FBTyxDQUFDUyxLQUFULENBQXRCLENBSG1CLENBQUQsRUFJN0IrTSxtQkFKNkIsQ0FBaEM7QUFLQSxTQUFPQSxtQkFBUDtBQUNILENBdkN3QyxFQUF6Qzs7QUF3Q0FqTyxPQUFPLENBQUNpTyxtQkFBUixHQUE4QkEsbUJBQTlCLEM7Ozs7Ozs7Ozs7OztBQzVGYTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUNiLElBQUlnQyxVQUFVLEdBQUksUUFBUSxLQUFLQSxVQUFkLElBQTZCLFVBQVVDLFVBQVYsRUFBc0JDLE1BQXRCLEVBQThCQyxHQUE5QixFQUFtQ0MsSUFBbkMsRUFBeUM7QUFDbkYsTUFBSUMsQ0FBQyxHQUFHaEgsU0FBUyxDQUFDbEUsTUFBbEI7QUFBQSxNQUEwQnlKLENBQUMsR0FBR3lCLENBQUMsR0FBRyxDQUFKLEdBQVFILE1BQVIsR0FBaUJFLElBQUksS0FBSyxJQUFULEdBQWdCQSxJQUFJLEdBQUduSCxNQUFNLENBQUNxSCx3QkFBUCxDQUFnQ0osTUFBaEMsRUFBd0NDLEdBQXhDLENBQXZCLEdBQXNFQyxJQUFySDtBQUFBLE1BQTJIRyxDQUEzSDtBQUNBLE1BQUksUUFBT0MsT0FBUCx5Q0FBT0EsT0FBUCxPQUFtQixRQUFuQixJQUErQixPQUFPQSxPQUFPLENBQUNDLFFBQWYsS0FBNEIsVUFBL0QsRUFBMkU3QixDQUFDLEdBQUc0QixPQUFPLENBQUNDLFFBQVIsQ0FBaUJSLFVBQWpCLEVBQTZCQyxNQUE3QixFQUFxQ0MsR0FBckMsRUFBMENDLElBQTFDLENBQUosQ0FBM0UsS0FDSyxLQUFLLElBQUloSCxDQUFDLEdBQUc2RyxVQUFVLENBQUM5SyxNQUFYLEdBQW9CLENBQWpDLEVBQW9DaUUsQ0FBQyxJQUFJLENBQXpDLEVBQTRDQSxDQUFDLEVBQTdDO0FBQWlELFFBQUltSCxDQUFDLEdBQUdOLFVBQVUsQ0FBQzdHLENBQUQsQ0FBbEIsRUFBdUJ3RixDQUFDLEdBQUcsQ0FBQ3lCLENBQUMsR0FBRyxDQUFKLEdBQVFFLENBQUMsQ0FBQzNCLENBQUQsQ0FBVCxHQUFleUIsQ0FBQyxHQUFHLENBQUosR0FBUUUsQ0FBQyxDQUFDTCxNQUFELEVBQVNDLEdBQVQsRUFBY3ZCLENBQWQsQ0FBVCxHQUE0QjJCLENBQUMsQ0FBQ0wsTUFBRCxFQUFTQyxHQUFULENBQTdDLEtBQStEdkIsQ0FBbkU7QUFBeEU7QUFDTCxTQUFPeUIsQ0FBQyxHQUFHLENBQUosSUFBU3pCLENBQVQsSUFBYzNGLE1BQU0sQ0FBQ3lILGNBQVAsQ0FBc0JSLE1BQXRCLEVBQThCQyxHQUE5QixFQUFtQ3ZCLENBQW5DLENBQWQsRUFBcURBLENBQTVEO0FBQ0gsQ0FMRDs7QUFNQSxJQUFJK0IsVUFBVSxHQUFJLFFBQVEsS0FBS0EsVUFBZCxJQUE2QixVQUFVQyxDQUFWLEVBQWE5TCxDQUFiLEVBQWdCO0FBQzFELE1BQUksUUFBTzBMLE9BQVAseUNBQU9BLE9BQVAsT0FBbUIsUUFBbkIsSUFBK0IsT0FBT0EsT0FBTyxDQUFDSyxRQUFmLEtBQTRCLFVBQS9ELEVBQTJFLE9BQU9MLE9BQU8sQ0FBQ0ssUUFBUixDQUFpQkQsQ0FBakIsRUFBb0I5TCxDQUFwQixDQUFQO0FBQzlFLENBRkQ7O0FBR0EsSUFBSWhDLFNBQVMsR0FBSSxRQUFRLEtBQUtBLFNBQWQsSUFBNEIsVUFBVUMsT0FBVixFQUFtQkMsVUFBbkIsRUFBK0JDLENBQS9CLEVBQWtDQyxTQUFsQyxFQUE2QztBQUNyRixXQUFTQyxLQUFULENBQWVDLEtBQWYsRUFBc0I7QUFBRSxXQUFPQSxLQUFLLFlBQVlILENBQWpCLEdBQXFCRyxLQUFyQixHQUE2QixJQUFJSCxDQUFKLENBQU0sVUFBVWpDLE9BQVYsRUFBbUI7QUFBRUEsYUFBTyxDQUFDb0MsS0FBRCxDQUFQO0FBQWlCLEtBQTVDLENBQXBDO0FBQW9GOztBQUM1RyxTQUFPLEtBQUtILENBQUMsS0FBS0EsQ0FBQyxHQUFHSSxPQUFULENBQU4sRUFBeUIsVUFBVXJDLE9BQVYsRUFBbUJzQyxNQUFuQixFQUEyQjtBQUN2RCxhQUFTQyxTQUFULENBQW1CSCxLQUFuQixFQUEwQjtBQUFFLFVBQUk7QUFBRUksWUFBSSxDQUFDTixTQUFTLENBQUNPLElBQVYsQ0FBZUwsS0FBZixDQUFELENBQUo7QUFBOEIsT0FBcEMsQ0FBcUMsT0FBT00sQ0FBUCxFQUFVO0FBQUVKLGNBQU0sQ0FBQ0ksQ0FBRCxDQUFOO0FBQVk7QUFBRTs7QUFDM0YsYUFBU0MsUUFBVCxDQUFrQlAsS0FBbEIsRUFBeUI7QUFBRSxVQUFJO0FBQUVJLFlBQUksQ0FBQ04sU0FBUyxDQUFDLE9BQUQsQ0FBVCxDQUFtQkUsS0FBbkIsQ0FBRCxDQUFKO0FBQWtDLE9BQXhDLENBQXlDLE9BQU9NLENBQVAsRUFBVTtBQUFFSixjQUFNLENBQUNJLENBQUQsQ0FBTjtBQUFZO0FBQUU7O0FBQzlGLGFBQVNGLElBQVQsQ0FBY0ksTUFBZCxFQUFzQjtBQUFFQSxZQUFNLENBQUNDLElBQVAsR0FBYzdDLE9BQU8sQ0FBQzRDLE1BQU0sQ0FBQ1IsS0FBUixDQUFyQixHQUFzQ0QsS0FBSyxDQUFDUyxNQUFNLENBQUNSLEtBQVIsQ0FBTCxDQUFvQlUsSUFBcEIsQ0FBeUJQLFNBQXpCLEVBQW9DSSxRQUFwQyxDQUF0QztBQUFzRjs7QUFDOUdILFFBQUksQ0FBQyxDQUFDTixTQUFTLEdBQUdBLFNBQVMsQ0FBQ2EsS0FBVixDQUFnQmhCLE9BQWhCLEVBQXlCQyxVQUFVLElBQUksRUFBdkMsQ0FBYixFQUF5RFMsSUFBekQsRUFBRCxDQUFKO0FBQ0gsR0FMTSxDQUFQO0FBTUgsQ0FSRDs7QUFTQSxJQUFJTyxXQUFXLEdBQUksUUFBUSxLQUFLQSxXQUFkLElBQThCLFVBQVVqQixPQUFWLEVBQW1Ca0IsSUFBbkIsRUFBeUI7QUFDckUsTUFBSUMsQ0FBQyxHQUFHO0FBQUV4QixTQUFLLEVBQUUsQ0FBVDtBQUFZeUIsUUFBSSxFQUFFLGdCQUFXO0FBQUUsVUFBSUMsQ0FBQyxDQUFDLENBQUQsQ0FBRCxHQUFPLENBQVgsRUFBYyxNQUFNQSxDQUFDLENBQUMsQ0FBRCxDQUFQO0FBQVksYUFBT0EsQ0FBQyxDQUFDLENBQUQsQ0FBUjtBQUFjLEtBQXZFO0FBQXlFQyxRQUFJLEVBQUUsRUFBL0U7QUFBbUZDLE9BQUcsRUFBRTtBQUF4RixHQUFSO0FBQUEsTUFBc0dDLENBQXRHO0FBQUEsTUFBeUdDLENBQXpHO0FBQUEsTUFBNEdKLENBQTVHO0FBQUEsTUFBK0dLLENBQS9HO0FBQ0EsU0FBT0EsQ0FBQyxHQUFHO0FBQUVoQixRQUFJLEVBQUVpQixJQUFJLENBQUMsQ0FBRCxDQUFaO0FBQWlCLGFBQVNBLElBQUksQ0FBQyxDQUFELENBQTlCO0FBQW1DLGNBQVVBLElBQUksQ0FBQyxDQUFEO0FBQWpELEdBQUosRUFBNEQsT0FBT0MsTUFBUCxLQUFrQixVQUFsQixLQUFpQ0YsQ0FBQyxDQUFDRSxNQUFNLENBQUNDLFFBQVIsQ0FBRCxHQUFxQixZQUFXO0FBQUUsV0FBTyxJQUFQO0FBQWMsR0FBakYsQ0FBNUQsRUFBZ0pILENBQXZKOztBQUNBLFdBQVNDLElBQVQsQ0FBY0csQ0FBZCxFQUFpQjtBQUFFLFdBQU8sVUFBVUMsQ0FBVixFQUFhO0FBQUUsYUFBT3RCLElBQUksQ0FBQyxDQUFDcUIsQ0FBRCxFQUFJQyxDQUFKLENBQUQsQ0FBWDtBQUFzQixLQUE1QztBQUErQzs7QUFDbEUsV0FBU3RCLElBQVQsQ0FBY3VCLEVBQWQsRUFBa0I7QUFDZCxRQUFJUixDQUFKLEVBQU8sTUFBTSxJQUFJUyxTQUFKLENBQWMsaUNBQWQsQ0FBTjs7QUFDUCxXQUFPZCxDQUFQO0FBQVUsVUFBSTtBQUNWLFlBQUlLLENBQUMsR0FBRyxDQUFKLEVBQU9DLENBQUMsS0FBS0osQ0FBQyxHQUFHVyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBUixHQUFZUCxDQUFDLENBQUMsUUFBRCxDQUFiLEdBQTBCTyxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVFQLENBQUMsQ0FBQyxPQUFELENBQUQsS0FBZSxDQUFDSixDQUFDLEdBQUdJLENBQUMsQ0FBQyxRQUFELENBQU4sS0FBcUJKLENBQUMsQ0FBQ2EsSUFBRixDQUFPVCxDQUFQLENBQXJCLEVBQWdDLENBQS9DLENBQVIsR0FBNERBLENBQUMsQ0FBQ2YsSUFBakcsQ0FBRCxJQUEyRyxDQUFDLENBQUNXLENBQUMsR0FBR0EsQ0FBQyxDQUFDYSxJQUFGLENBQU9ULENBQVAsRUFBVU8sRUFBRSxDQUFDLENBQUQsQ0FBWixDQUFMLEVBQXVCbEIsSUFBOUksRUFBb0osT0FBT08sQ0FBUDtBQUNwSixZQUFJSSxDQUFDLEdBQUcsQ0FBSixFQUFPSixDQUFYLEVBQWNXLEVBQUUsR0FBRyxDQUFDQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEdBQVEsQ0FBVCxFQUFZWCxDQUFDLENBQUNoQixLQUFkLENBQUw7O0FBQ2QsZ0JBQVEyQixFQUFFLENBQUMsQ0FBRCxDQUFWO0FBQ0ksZUFBSyxDQUFMO0FBQVEsZUFBSyxDQUFMO0FBQVFYLGFBQUMsR0FBR1csRUFBSjtBQUFROztBQUN4QixlQUFLLENBQUw7QUFBUWIsYUFBQyxDQUFDeEIsS0FBRjtBQUFXLG1CQUFPO0FBQUVVLG1CQUFLLEVBQUUyQixFQUFFLENBQUMsQ0FBRCxDQUFYO0FBQWdCbEIsa0JBQUksRUFBRTtBQUF0QixhQUFQOztBQUNuQixlQUFLLENBQUw7QUFBUUssYUFBQyxDQUFDeEIsS0FBRjtBQUFXOEIsYUFBQyxHQUFHTyxFQUFFLENBQUMsQ0FBRCxDQUFOO0FBQVdBLGNBQUUsR0FBRyxDQUFDLENBQUQsQ0FBTDtBQUFVOztBQUN4QyxlQUFLLENBQUw7QUFBUUEsY0FBRSxHQUFHYixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTixFQUFMOztBQUFrQmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjOztBQUN4QztBQUNJLGdCQUFJLEVBQUVkLENBQUMsR0FBR0YsQ0FBQyxDQUFDRyxJQUFOLEVBQVlELENBQUMsR0FBR0EsQ0FBQyxDQUFDZSxNQUFGLEdBQVcsQ0FBWCxJQUFnQmYsQ0FBQyxDQUFDQSxDQUFDLENBQUNlLE1BQUYsR0FBVyxDQUFaLENBQW5DLE1BQXVESixFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixJQUFlQSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBaEYsQ0FBSixFQUF3RjtBQUFFYixlQUFDLEdBQUcsQ0FBSjtBQUFPO0FBQVc7O0FBQzVHLGdCQUFJYSxFQUFFLENBQUMsQ0FBRCxDQUFGLEtBQVUsQ0FBVixLQUFnQixDQUFDWCxDQUFELElBQU9XLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUVgsQ0FBQyxDQUFDLENBQUQsQ0FBVCxJQUFnQlcsRUFBRSxDQUFDLENBQUQsQ0FBRixHQUFRWCxDQUFDLENBQUMsQ0FBRCxDQUFoRCxDQUFKLEVBQTJEO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVXFDLEVBQUUsQ0FBQyxDQUFELENBQVo7QUFBaUI7QUFBUTs7QUFDdEYsZ0JBQUlBLEVBQUUsQ0FBQyxDQUFELENBQUYsS0FBVSxDQUFWLElBQWViLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQTlCLEVBQW1DO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7QUFBZ0JBLGVBQUMsR0FBR1csRUFBSjtBQUFRO0FBQVE7O0FBQ3JFLGdCQUFJWCxDQUFDLElBQUlGLENBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQXBCLEVBQXlCO0FBQUVGLGVBQUMsQ0FBQ3hCLEtBQUYsR0FBVTBCLENBQUMsQ0FBQyxDQUFELENBQVg7O0FBQWdCRixlQUFDLENBQUNJLEdBQUYsQ0FBTWMsSUFBTixDQUFXTCxFQUFYOztBQUFnQjtBQUFROztBQUNuRSxnQkFBSVgsQ0FBQyxDQUFDLENBQUQsQ0FBTCxFQUFVRixDQUFDLENBQUNJLEdBQUYsQ0FBTVksR0FBTjs7QUFDVmhCLGFBQUMsQ0FBQ0csSUFBRixDQUFPYSxHQUFQOztBQUFjO0FBWHRCOztBQWFBSCxVQUFFLEdBQUdkLElBQUksQ0FBQ2dCLElBQUwsQ0FBVWxDLE9BQVYsRUFBbUJtQixDQUFuQixDQUFMO0FBQ0gsT0FqQlMsQ0FpQlIsT0FBT1IsQ0FBUCxFQUFVO0FBQUVxQixVQUFFLEdBQUcsQ0FBQyxDQUFELEVBQUlyQixDQUFKLENBQUw7QUFBYWMsU0FBQyxHQUFHLENBQUo7QUFBUSxPQWpCekIsU0FpQmtDO0FBQUVELFNBQUMsR0FBR0gsQ0FBQyxHQUFHLENBQVI7QUFBWTtBQWpCMUQ7O0FBa0JBLFFBQUlXLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUSxDQUFaLEVBQWUsTUFBTUEsRUFBRSxDQUFDLENBQUQsQ0FBUjtBQUFhLFdBQU87QUFBRTNCLFdBQUssRUFBRTJCLEVBQUUsQ0FBQyxDQUFELENBQUYsR0FBUUEsRUFBRSxDQUFDLENBQUQsQ0FBVixHQUFnQixLQUFLLENBQTlCO0FBQWlDbEIsVUFBSSxFQUFFO0FBQXZDLEtBQVA7QUFDL0I7QUFDSixDQTFCRDs7QUEyQkE5RCxPQUFPLENBQUNDLFVBQVIsR0FBcUIsSUFBckI7QUFDQUQsT0FBTyxDQUFDbVAsV0FBUixHQUFzQixLQUFLLENBQTNCOztBQUNBLElBQUl6TyxVQUFVLEdBQUdSLG1CQUFPLENBQUMsbUdBQUQsQ0FBeEI7O0FBQ0EsSUFBSU8sT0FBTyxHQUFHUCxtQkFBTyxDQUFDLCtFQUFELENBQXJCOztBQUNBLElBQUk2USxPQUFPLEdBQUc3USxtQkFBTyxDQUFDLG1GQUFELENBQXJCOztBQUNBLElBQUkwRixNQUFNLEdBQUcxRixtQkFBTyxDQUFDLGlGQUFELENBQXBCOztBQUNBLElBQUlpUCxXQUFXO0FBQUc7QUFBZSxZQUFZO0FBQ3pDLFdBQVNBLFdBQVQsQ0FBcUJwTyxLQUFyQixFQUE0QjtBQUN4QixTQUFLQSxLQUFMLEdBQWFBLEtBQWI7QUFDSDs7QUFDRG9PLGFBQVcsQ0FBQy9OLFNBQVosQ0FBc0JnTyxRQUF0QixHQUFpQyxZQUFZO0FBQ3pDLFdBQU9yTSxTQUFTLENBQUMsSUFBRCxFQUFPLEtBQUssQ0FBWixFQUFlLEtBQUssQ0FBcEIsRUFBdUIsWUFBWTtBQUMvQyxhQUFPa0IsV0FBVyxDQUFDLElBQUQsRUFBTyxVQUFVOEIsRUFBVixFQUFjO0FBQ25DLGdCQUFRQSxFQUFFLENBQUNwRCxLQUFYO0FBQ0ksZUFBSyxDQUFMO0FBQVEsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBY29PLE9BQU8sQ0FBQyxTQUFELENBQVAsQ0FBbUJDLEdBQW5CLENBQXVCLGlCQUF2QixFQUEwQztBQUMvREMscUJBQU8sRUFBRTtBQUNMLGlDQUFpQixZQUFZLEtBQUtsUSxLQUFMLENBQVdJO0FBRG5DO0FBRHNELGFBQTFDLEVBSXRCNEMsSUFKc0IsQ0FJakIsVUFBVW1OLFFBQVYsRUFBb0I7QUFDeEIscUJBQU9BLFFBQVEsQ0FBQ3ZLLElBQVQsQ0FBY0EsSUFBZCxDQUFtQndLLEdBQW5CLENBQXVCLFVBQVVDLElBQVYsRUFBZ0I7QUFDMUMsb0JBQUloTCxJQUFJLEdBQUcsSUFBSVIsTUFBTSxDQUFDL0MsSUFBWCxFQUFYO0FBQ0F1RCxvQkFBSSxDQUFDdEUsRUFBTCxHQUFVc1AsSUFBSSxDQUFDdFAsRUFBZjtBQUNBc0Usb0JBQUksQ0FBQ3RELElBQUwsR0FBWXNPLElBQUksQ0FBQ3RPLElBQWpCO0FBQ0FzRCxvQkFBSSxDQUFDckUsSUFBTCxHQUFZcVAsSUFBSSxDQUFDclAsSUFBakI7QUFDQSx1QkFBT3FFLElBQVA7QUFDSCxlQU5NLENBQVA7QUFPSCxhQVp3QixDQUFkLENBQVA7O0FBYVIsZUFBSyxDQUFMO0FBQVEsbUJBQU8sQ0FBQztBQUFFO0FBQUgsY0FBZUwsRUFBRSxDQUFDM0IsSUFBSCxFQUFmLENBQVA7QUFkWjtBQWdCSCxPQWpCaUIsQ0FBbEI7QUFrQkgsS0FuQmUsQ0FBaEI7QUFvQkgsR0FyQkQ7O0FBc0JBK0ssYUFBVyxHQUFHYyxVQUFVLENBQUMsQ0FDckIsQ0FBQyxHQUFHdlAsVUFBVSxDQUFDMlEsU0FBZixHQURxQixFQUVyQixDQUFDLEdBQUczUSxVQUFVLENBQUM0USxjQUFmLEdBRnFCLEVBR3JCVixVQUFVLENBQUMsbUJBQUQsRUFBc0IsQ0FBQ25RLE9BQU8sQ0FBQ1MsS0FBVCxDQUF0QixDQUhXLENBQUQsRUFJckJpTyxXQUpxQixDQUF4QjtBQUtBLFNBQU9BLFdBQVA7QUFDSCxDQWhDZ0MsRUFBakM7O0FBaUNBblAsT0FBTyxDQUFDbVAsV0FBUixHQUFzQkEsV0FBdEIsQzs7Ozs7Ozs7Ozs7O0FDckZhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFDYixJQUFJYyxVQUFVLEdBQUksUUFBUSxLQUFLQSxVQUFkLElBQTZCLFVBQVVDLFVBQVYsRUFBc0JDLE1BQXRCLEVBQThCQyxHQUE5QixFQUFtQ0MsSUFBbkMsRUFBeUM7QUFDbkYsTUFBSUMsQ0FBQyxHQUFHaEgsU0FBUyxDQUFDbEUsTUFBbEI7QUFBQSxNQUEwQnlKLENBQUMsR0FBR3lCLENBQUMsR0FBRyxDQUFKLEdBQVFILE1BQVIsR0FBaUJFLElBQUksS0FBSyxJQUFULEdBQWdCQSxJQUFJLEdBQUduSCxNQUFNLENBQUNxSCx3QkFBUCxDQUFnQ0osTUFBaEMsRUFBd0NDLEdBQXhDLENBQXZCLEdBQXNFQyxJQUFySDtBQUFBLE1BQTJIRyxDQUEzSDtBQUNBLE1BQUksUUFBT0MsT0FBUCx5Q0FBT0EsT0FBUCxPQUFtQixRQUFuQixJQUErQixPQUFPQSxPQUFPLENBQUNDLFFBQWYsS0FBNEIsVUFBL0QsRUFBMkU3QixDQUFDLEdBQUc0QixPQUFPLENBQUNDLFFBQVIsQ0FBaUJSLFVBQWpCLEVBQTZCQyxNQUE3QixFQUFxQ0MsR0FBckMsRUFBMENDLElBQTFDLENBQUosQ0FBM0UsS0FDSyxLQUFLLElBQUloSCxDQUFDLEdBQUc2RyxVQUFVLENBQUM5SyxNQUFYLEdBQW9CLENBQWpDLEVBQW9DaUUsQ0FBQyxJQUFJLENBQXpDLEVBQTRDQSxDQUFDLEVBQTdDO0FBQWlELFFBQUltSCxDQUFDLEdBQUdOLFVBQVUsQ0FBQzdHLENBQUQsQ0FBbEIsRUFBdUJ3RixDQUFDLEdBQUcsQ0FBQ3lCLENBQUMsR0FBRyxDQUFKLEdBQVFFLENBQUMsQ0FBQzNCLENBQUQsQ0FBVCxHQUFleUIsQ0FBQyxHQUFHLENBQUosR0FBUUUsQ0FBQyxDQUFDTCxNQUFELEVBQVNDLEdBQVQsRUFBY3ZCLENBQWQsQ0FBVCxHQUE0QjJCLENBQUMsQ0FBQ0wsTUFBRCxFQUFTQyxHQUFULENBQTdDLEtBQStEdkIsQ0FBbkU7QUFBeEU7QUFDTCxTQUFPeUIsQ0FBQyxHQUFHLENBQUosSUFBU3pCLENBQVQsSUFBYzNGLE1BQU0sQ0FBQ3lILGNBQVAsQ0FBc0JSLE1BQXRCLEVBQThCQyxHQUE5QixFQUFtQ3ZCLENBQW5DLENBQWQsRUFBcURBLENBQTVEO0FBQ0gsQ0FMRDs7QUFNQTdPLE9BQU8sQ0FBQ0MsVUFBUixHQUFxQixJQUFyQjtBQUNBRCxPQUFPLENBQUNrQixLQUFSLEdBQWdCLEtBQUssQ0FBckI7O0FBQ0EsSUFBSTZQLE9BQU8sR0FBRzdRLG1CQUFPLENBQUMsbUZBQUQsQ0FBckI7O0FBQ0EsSUFBSVEsVUFBVSxHQUFHUixtQkFBTyxDQUFDLG1HQUFELENBQXhCOztBQUNBLElBQUlnQixLQUFLO0FBQUc7QUFBZSxZQUFZO0FBQ25DLFdBQVNBLEtBQVQsR0FBaUIsQ0FDaEI7O0FBQ0RBLE9BQUssQ0FBQ0UsU0FBTixDQUFnQmIsSUFBaEIsR0FBdUIsWUFBWTtBQUMvQixRQUFJc1IsS0FBSyxHQUFHLElBQVo7O0FBQ0FDLGVBQVcsQ0FBQyxZQUFZO0FBQ3BCRCxXQUFLLENBQUNFLFlBQU47QUFDSCxLQUZVLEVBRVIsSUFBSSxFQUFKLEdBQVMsSUFGRCxDQUFYO0FBR0gsR0FMRDs7QUFNQTdRLE9BQUssQ0FBQ0UsU0FBTixDQUFnQjJRLFlBQWhCLEdBQStCLFlBQVk7QUFDdkMsUUFBSUYsS0FBSyxHQUFHLElBQVo7O0FBQ0FkLFdBQU8sQ0FBQyxTQUFELENBQVAsQ0FBbUJDLEdBQW5CLENBQXVCLDJCQUF2QixFQUFvRDtBQUNoREMsYUFBTyxFQUFFO0FBQ0wseUJBQWlCLFlBQVksS0FBSzlQO0FBRDdCO0FBRHVDLEtBQXBELEVBSUc0QyxJQUpILENBSVEsVUFBVW1OLFFBQVYsRUFBb0I7QUFDeEJXLFdBQUssQ0FBQzFRLFlBQU4sR0FBcUIrUCxRQUFRLENBQUN2SyxJQUFULENBQWM1RixLQUFuQztBQUNILEtBTkQ7QUFPSCxHQVREOztBQVVBRyxPQUFLLEdBQUcrTyxVQUFVLENBQUMsQ0FDZixDQUFDLEdBQUd2UCxVQUFVLENBQUMyUSxTQUFmLEdBRGUsQ0FBRCxFQUVmblEsS0FGZSxDQUFsQjtBQUdBLFNBQU9BLEtBQVA7QUFDSCxDQXZCMEIsRUFBM0I7O0FBd0JBbEIsT0FBTyxDQUFDa0IsS0FBUixHQUFnQkEsS0FBaEIsQzs7Ozs7Ozs7Ozs7O0FDbkNhOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQ2IsSUFBSStPLFVBQVUsR0FBSSxRQUFRLEtBQUtBLFVBQWQsSUFBNkIsVUFBVUMsVUFBVixFQUFzQkMsTUFBdEIsRUFBOEJDLEdBQTlCLEVBQW1DQyxJQUFuQyxFQUF5QztBQUNuRixNQUFJQyxDQUFDLEdBQUdoSCxTQUFTLENBQUNsRSxNQUFsQjtBQUFBLE1BQTBCeUosQ0FBQyxHQUFHeUIsQ0FBQyxHQUFHLENBQUosR0FBUUgsTUFBUixHQUFpQkUsSUFBSSxLQUFLLElBQVQsR0FBZ0JBLElBQUksR0FBR25ILE1BQU0sQ0FBQ3FILHdCQUFQLENBQWdDSixNQUFoQyxFQUF3Q0MsR0FBeEMsQ0FBdkIsR0FBc0VDLElBQXJIO0FBQUEsTUFBMkhHLENBQTNIO0FBQ0EsTUFBSSxRQUFPQyxPQUFQLHlDQUFPQSxPQUFQLE9BQW1CLFFBQW5CLElBQStCLE9BQU9BLE9BQU8sQ0FBQ0MsUUFBZixLQUE0QixVQUEvRCxFQUEyRTdCLENBQUMsR0FBRzRCLE9BQU8sQ0FBQ0MsUUFBUixDQUFpQlIsVUFBakIsRUFBNkJDLE1BQTdCLEVBQXFDQyxHQUFyQyxFQUEwQ0MsSUFBMUMsQ0FBSixDQUEzRSxLQUNLLEtBQUssSUFBSWhILENBQUMsR0FBRzZHLFVBQVUsQ0FBQzlLLE1BQVgsR0FBb0IsQ0FBakMsRUFBb0NpRSxDQUFDLElBQUksQ0FBekMsRUFBNENBLENBQUMsRUFBN0M7QUFBaUQsUUFBSW1ILENBQUMsR0FBR04sVUFBVSxDQUFDN0csQ0FBRCxDQUFsQixFQUF1QndGLENBQUMsR0FBRyxDQUFDeUIsQ0FBQyxHQUFHLENBQUosR0FBUUUsQ0FBQyxDQUFDM0IsQ0FBRCxDQUFULEdBQWV5QixDQUFDLEdBQUcsQ0FBSixHQUFRRSxDQUFDLENBQUNMLE1BQUQsRUFBU0MsR0FBVCxFQUFjdkIsQ0FBZCxDQUFULEdBQTRCMkIsQ0FBQyxDQUFDTCxNQUFELEVBQVNDLEdBQVQsQ0FBN0MsS0FBK0R2QixDQUFuRTtBQUF4RTtBQUNMLFNBQU95QixDQUFDLEdBQUcsQ0FBSixJQUFTekIsQ0FBVCxJQUFjM0YsTUFBTSxDQUFDeUgsY0FBUCxDQUFzQlIsTUFBdEIsRUFBOEJDLEdBQTlCLEVBQW1DdkIsQ0FBbkMsQ0FBZCxFQUFxREEsQ0FBNUQ7QUFDSCxDQUxEOztBQU1BN08sT0FBTyxDQUFDQyxVQUFSLEdBQXFCLElBQXJCO0FBQ0FELE9BQU8sQ0FBQ3NHLEtBQVIsR0FBZ0IsS0FBSyxDQUFyQjs7QUFDQSxJQUFJNUYsVUFBVSxHQUFHUixtQkFBTyxDQUFDLG1HQUFELENBQXhCOztBQUNBLElBQUlvRyxLQUFLO0FBQUc7QUFBZSxZQUFZO0FBQ25DLFdBQVNBLEtBQVQsR0FBaUIsQ0FDaEI7O0FBQ0RBLE9BQUssR0FBRzJKLFVBQVUsQ0FBQyxDQUNmLENBQUMsR0FBR3ZQLFVBQVUsQ0FBQzJRLFNBQWYsR0FEZSxDQUFELEVBRWYvSyxLQUZlLENBQWxCO0FBR0EsU0FBT0EsS0FBUDtBQUNILENBUDBCLEVBQTNCOztBQVFBdEcsT0FBTyxDQUFDc0csS0FBUixHQUFnQkEsS0FBaEIsQyIsImZpbGUiOiJwbHVnaW5zL3N5c3RlbS9wc2MvaW52b2ljZS5qcyIsInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnJlcXVpcmUoXCJyZWZsZWN0LW1ldGFkYXRhXCIpO1xucmVxdWlyZShcImJvb3RzdHJhcC9zY3NzL2Jvb3RzdHJhcC5zY3NzXCIpO1xudmFyIGFwcF8xID0gcmVxdWlyZShcIi4vc3JjL2FwcC9hcHBcIik7XG52YXIgYXBwID0gbmV3IGFwcF8xLkFwcChqd3RfdG9rZW4pO1xuYXBwLmluaXQoKTtcbmFwcC5ydW4oKTtcbiIsIlwidXNlIHN0cmljdFwiO1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHMuQXBwID0gdm9pZCAwO1xudmFyIHRva2VuXzEgPSByZXF1aXJlKFwiLi4vc2VydmljZXMvdG9rZW5cIik7XG52YXIgdHN5cmluZ2VfMSA9IHJlcXVpcmUoXCJ0c3lyaW5nZVwiKTtcbnZhciBSZWFjdCA9IHJlcXVpcmUoXCJyZWFjdFwiKTtcbnZhciBSZWFjdERPTSA9IHJlcXVpcmUoXCJyZWFjdC1kb20vY2xpZW50XCIpO1xudmFyIEJhc2VDb21wb25lbnRfMSA9IHJlcXVpcmUoXCIuLi9tb2R1bGVzL2Jhc2UvQmFzZUNvbXBvbmVudFwiKTtcbnZhciBBcHAgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gQXBwKGp3dCkge1xuICAgICAgICB2YXIgdG9rZW4gPSB0c3lyaW5nZV8xLmNvbnRhaW5lci5yZXNvbHZlKHRva2VuXzEuVG9rZW4pO1xuICAgICAgICB0b2tlbi5jdXJyZW50VG9rZW4gPSBqd3Q7XG4gICAgICAgIHRva2VuLmluaXQoKTtcbiAgICB9XG4gICAgQXBwLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLmJ1aWxkVWkoKTtcbiAgICB9O1xuICAgIEFwcC5wcm90b3R5cGUuYnVpbGRVaSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHJvb3QgPSBSZWFjdERPTS5jcmVhdGVSb290KGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyb290JykpO1xuICAgICAgICByb290LnJlbmRlcihSZWFjdC5jcmVhdGVFbGVtZW50KEJhc2VDb21wb25lbnRfMVtcImRlZmF1bHRcIl0pKTtcbiAgICB9O1xuICAgIEFwcC5wcm90b3R5cGUucnVuID0gZnVuY3Rpb24gKCkge1xuICAgIH07XG4gICAgcmV0dXJuIEFwcDtcbn0oKSk7XG5leHBvcnRzLkFwcCA9IEFwcDtcbiIsIlwidXNlIHN0cmljdFwiO1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHMuQWNjb3VudCA9IHZvaWQgMDtcbnZhciB1dWlkXzEgPSByZXF1aXJlKFwidXVpZFwiKTtcbnZhciBBY2NvdW50ID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIEFjY291bnQoKSB7XG4gICAgICAgIHRoaXMuaWQgPSAwO1xuICAgICAgICB0aGlzLnV1aWQgPSAoMCwgdXVpZF8xLnY0KSgpO1xuICAgICAgICB0aGlzLmFwcGVuZGl4ID0gXCJcIjtcbiAgICAgICAgdGhpcy50aXRsZSA9IFwiXCI7XG4gICAgfVxuICAgIHJldHVybiBBY2NvdW50O1xufSgpKTtcbmV4cG9ydHMuQWNjb3VudCA9IEFjY291bnQ7XG4iLCJcInVzZSBzdHJpY3RcIjtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5leHBvcnRzLkFkZHJlc3MgPSB2b2lkIDA7XG52YXIgdXVpZF8xID0gcmVxdWlyZShcInV1aWRcIik7XG52YXIgQWRkcmVzcyA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBBZGRyZXNzKCkge1xuICAgICAgICB0aGlzLmlkID0gMDtcbiAgICAgICAgdGhpcy51dWlkID0gKDAsIHV1aWRfMS52NCkoKTtcbiAgICAgICAgdGhpcy5maXJzdG5hbWUgPSBcIlwiO1xuICAgICAgICB0aGlzLmxhc3RuYW1lID0gXCJcIjtcbiAgICB9XG4gICAgcmV0dXJuIEFkZHJlc3M7XG59KCkpO1xuZXhwb3J0cy5BZGRyZXNzID0gQWRkcmVzcztcbiIsIlwidXNlIHN0cmljdFwiO1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHMuQ29udGFjdCA9IHZvaWQgMDtcbnZhciB1dWlkXzEgPSByZXF1aXJlKFwidXVpZFwiKTtcbnZhciBDb250YWN0ID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIENvbnRhY3QoKSB7XG4gICAgICAgIHRoaXMuaWQgPSAwO1xuICAgICAgICB0aGlzLnV1aWQgPSAoMCwgdXVpZF8xLnY0KSgpO1xuICAgICAgICB0aGlzLnVzZXJuYW1lID0gXCJcIjtcbiAgICAgICAgdGhpcy5wYXNzd29yZCA9IFwiXCI7XG4gICAgfVxuICAgIHJldHVybiBDb250YWN0O1xufSgpKTtcbmV4cG9ydHMuQ29udGFjdCA9IENvbnRhY3Q7XG4iLCJcInVzZSBzdHJpY3RcIjtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5leHBvcnRzLlByb2R1Y3QgPSB2b2lkIDA7XG52YXIgUHJvZHVjdCA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBQcm9kdWN0KCkge1xuICAgICAgICB0aGlzLmlkID0gMDtcbiAgICAgICAgdGhpcy50aXRsZSA9IFwiXCI7XG4gICAgICAgIHRoaXMudXVpZCA9IFwiXCI7XG4gICAgfVxuICAgIHJldHVybiBQcm9kdWN0O1xufSgpKTtcbmV4cG9ydHMuUHJvZHVjdCA9IFByb2R1Y3Q7XG4iLCJcInVzZSBzdHJpY3RcIjtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5leHBvcnRzLlByb2R1Y3RHcm91cCA9IHZvaWQgMDtcbnZhciBQcm9kdWN0R3JvdXAgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gUHJvZHVjdEdyb3VwKCkge1xuICAgICAgICB0aGlzLmlkID0gMDtcbiAgICAgICAgdGhpcy5sYWJlbCA9IFwiXCI7XG4gICAgICAgIHRoaXMudXVpZCA9IFwiXCI7XG4gICAgICAgIHRoaXMub3B0aW9ucyA9IFtdO1xuICAgIH1cbiAgICByZXR1cm4gUHJvZHVjdEdyb3VwO1xufSgpKTtcbmV4cG9ydHMuUHJvZHVjdEdyb3VwID0gUHJvZHVjdEdyb3VwO1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0cy5TaG9wID0gdm9pZCAwO1xudmFyIFNob3AgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gU2hvcCgpIHtcbiAgICAgICAgdGhpcy5pZCA9IDA7XG4gICAgICAgIHRoaXMubmFtZSA9IFwiXCI7XG4gICAgICAgIHRoaXMudXVpZCA9IFwiXCI7XG4gICAgfVxuICAgIHJldHVybiBTaG9wO1xufSgpKTtcbmV4cG9ydHMuU2hvcCA9IFNob3A7XG4iLCJcInVzZSBzdHJpY3RcIjtcbnZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xuICAgIGZ1bmN0aW9uIGFkb3B0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFAgPyB2YWx1ZSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGFkb3B0KHJlc3VsdC52YWx1ZSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XG4gICAgfSk7XG59O1xudmFyIF9fZ2VuZXJhdG9yID0gKHRoaXMgJiYgdGhpcy5fX2dlbmVyYXRvcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIGJvZHkpIHtcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xuICAgIGZ1bmN0aW9uIHZlcmIobikgeyByZXR1cm4gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHN0ZXAoW24sIHZdKTsgfTsgfVxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xuICAgICAgICB3aGlsZSAoXykgdHJ5IHtcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xuICAgIH1cbn07XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGpzeF9ydW50aW1lXzEgPSByZXF1aXJlKFwicmVhY3QvanN4LXJ1bnRpbWVcIik7XG52YXIgcmVhY3Rfc2VsZWN0X2FzeW5jX3BhZ2luYXRlXzEgPSByZXF1aXJlKFwicmVhY3Qtc2VsZWN0LWFzeW5jLXBhZ2luYXRlXCIpO1xudmFyIGFjY291bnRfMSA9IHJlcXVpcmUoXCIuLi8uLi9zZXJ2aWNlcy9hY2NvdW50XCIpO1xudmFyIHRzeXJpbmdlXzEgPSByZXF1aXJlKFwidHN5cmluZ2VcIik7XG52YXIgb3JkZXJfMSA9IHJlcXVpcmUoXCIuLi8uLi9zdGF0ZS9vcmRlclwiKTtcbnZhciByZWFjdF8xID0gcmVxdWlyZShcInJlYWN0XCIpO1xudmFyIFByb3BUeXBlcyA9IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpO1xudmFyIHNob3BfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9zaG9wXCIpO1xudmFyIEFjY291bnRTZWxlY3RDb21wb25lbnQgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICB2YXIgX2EgPSAoMCwgcmVhY3RfMS51c2VTdGF0ZSkobnVsbCksIHNob3BVdWlkID0gX2FbMF0sIHNldFNob3BVdWlkID0gX2FbMV07XG4gICAgKDAsIHJlYWN0XzEudXNlRWZmZWN0KShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHNldFNob3BVdWlkKHByb3BzLnNob3AudXVpZCk7XG4gICAgfSwgW3Byb3BzLnNob3AudXVpZF0pO1xuICAgIHZhciBvcmRlciA9IHRzeXJpbmdlXzEuY29udGFpbmVyLnJlc29sdmUob3JkZXJfMS5PcmRlcik7XG4gICAgdmFyIGxvYWRPcHRpb25zID0gZnVuY3Rpb24gKHNlYXJjaFF1ZXJ5LCBsb2FkZWRPcHRpb25zKSB7IHJldHVybiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgYWNjb3VudF9zZXJ2aWNlLCBkYXRhO1xuICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XG4gICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOlxuICAgICAgICAgICAgICAgICAgICBhY2NvdW50X3NlcnZpY2UgPSBuZXcgYWNjb3VudF8xLkFjY291bnRTZXJ2aWNlKCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbNCAvKnlpZWxkKi8sIGFjY291bnRfc2VydmljZS5nZXRBY2NvdW50cyhwcm9wcy5zaG9wKV07XG4gICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICBkYXRhID0gX2Euc2VudCgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IGRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzTW9yZTogZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgIH1dO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTsgfTtcbiAgICB2YXIgb25DaGFuZ2UgPSBmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgICAgIG9yZGVyLmFjY291bnQgPSBvcHRpb247XG4gICAgfTtcbiAgICByZXR1cm4gKCgwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJkaXZcIiwgeyBjaGlsZHJlbjogKDAsIGpzeF9ydW50aW1lXzEuanN4KShyZWFjdF9zZWxlY3RfYXN5bmNfcGFnaW5hdGVfMS5Bc3luY1BhZ2luYXRlLCB7IGRlZmF1bHRPcHRpb25zOiB0cnVlLCBsb2FkT3B0aW9uczogbG9hZE9wdGlvbnMsIGdldE9wdGlvblZhbHVlOiBmdW5jdGlvbiAob3B0aW9uKSB7IHJldHVybiBvcHRpb24udXVpZDsgfSwgZ2V0T3B0aW9uTGFiZWw6IGZ1bmN0aW9uIChvcHRpb24pIHsgcmV0dXJuIG9wdGlvbi50aXRsZTsgfSwgb25DaGFuZ2U6IG9uQ2hhbmdlLCBpc1NlYXJjaGFibGU6IHRydWUgfSwgSlNPTi5zdHJpbmdpZnkoc2hvcFV1aWQpKSB9KSk7XG59O1xuQWNjb3VudFNlbGVjdENvbXBvbmVudC5wcm9wVHlwZXMgPSB7XG4gICAgc2hvcDogUHJvcFR5cGVzLmluc3RhbmNlT2Yoc2hvcF8xLlNob3ApXG59O1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBBY2NvdW50U2VsZWN0Q29tcG9uZW50O1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGpzeF9ydW50aW1lXzEgPSByZXF1aXJlKFwicmVhY3QvanN4LXJ1bnRpbWVcIik7XG52YXIgc2hvcF8xID0gcmVxdWlyZShcIi4uLy4uL21vZGVsL3Nob3BcIik7XG52YXIgQ29udGFjdENvbXBvbmVudF8xID0gcmVxdWlyZShcIi4uL2NvbnRhY3QvQ29udGFjdENvbXBvbmVudFwiKTtcbnZhciBUb3BCYXJDb21wb25lbnRfMSA9IHJlcXVpcmUoXCIuLi90b3BiYXIvVG9wQmFyQ29tcG9uZW50XCIpO1xudmFyIFBvc2l0aW9uc0NvbXBvbmVudF8xID0gcmVxdWlyZShcIi4uL3Bvc2l0aW9ucy9Qb3NpdGlvbnNDb21wb25lbnRcIik7XG52YXIgcmVhY3RfMSA9IHJlcXVpcmUoXCJyZWFjdFwiKTtcbnZhciBCYXNlQ29tcG9uZW50ID0gZnVuY3Rpb24gKHByb3BzKSB7XG4gICAgdmFyIF9hID0gKDAsIHJlYWN0XzEudXNlU3RhdGUpKG5ldyBzaG9wXzEuU2hvcCgpKSwgc2hvcCA9IF9hWzBdLCBzZXRTaG9wID0gX2FbMV07XG4gICAgcmV0dXJuICgoMCwganN4X3J1bnRpbWVfMS5qc3hzKShcImRpdlwiLCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4KShcImRpdlwiLCB7IGNoaWxkcmVuOiAoMCwganN4X3J1bnRpbWVfMS5qc3gpKFRvcEJhckNvbXBvbmVudF8xW1wiZGVmYXVsdFwiXSwgeyBzaG9wOiBzaG9wLCBjaGFuZ2U6IHNldFNob3AgfSkgfSksICgwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJkaXZcIiwgeyBjaGlsZHJlbjogc2hvcC5pZCAhPSAwICYmICgwLCBqc3hfcnVudGltZV8xLmpzeCkoQ29udGFjdENvbXBvbmVudF8xW1wiZGVmYXVsdFwiXSwgeyBzaG9wOiBzaG9wIH0pIH0pLCAoMCwganN4X3J1bnRpbWVfMS5qc3gpKFwiZGl2XCIsIHsgY2hpbGRyZW46IHNob3AuaWQgIT0gMCAmJiAoMCwganN4X3J1bnRpbWVfMS5qc3gpKFBvc2l0aW9uc0NvbXBvbmVudF8xW1wiZGVmYXVsdFwiXSwgeyBzaG9wOiBzaG9wIH0pIH0pXSB9KSk7XG59O1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBCYXNlQ29tcG9uZW50O1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19hd2FpdGVyID0gKHRoaXMgJiYgdGhpcy5fX2F3YWl0ZXIpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcbiAgICBmdW5jdGlvbiBhZG9wdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQID8gdmFsdWUgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBhZG9wdChyZXN1bHQudmFsdWUpLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xuICAgIH0pO1xufTtcbnZhciBfX2dlbmVyYXRvciA9ICh0aGlzICYmIHRoaXMuX19nZW5lcmF0b3IpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBib2R5KSB7XG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcbiAgICByZXR1cm4gZyA9IHsgbmV4dDogdmVyYigwKSwgXCJ0aHJvd1wiOiB2ZXJiKDEpLCBcInJldHVyblwiOiB2ZXJiKDIpIH0sIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiAoZ1tTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KSwgZztcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XG4gICAgICAgIGlmIChmKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgZXhlY3V0aW5nLlwiKTtcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBqc3hfcnVudGltZV8xID0gcmVxdWlyZShcInJlYWN0L2pzeC1ydW50aW1lXCIpO1xudmFyIFByb3BUeXBlcyA9IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpO1xudmFyIHJlYWN0X3NlbGVjdF9hc3luY19wYWdpbmF0ZV8xID0gcmVxdWlyZShcInJlYWN0LXNlbGVjdC1hc3luYy1wYWdpbmF0ZVwiKTtcbnZhciBjb250YWN0XzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvY29udGFjdFwiKTtcbnZhciBhZGRyZXNzXzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvYWRkcmVzc1wiKTtcbnZhciBjb250YWN0XzIgPSByZXF1aXJlKFwiLi4vLi4vc2VydmljZXMvY29udGFjdFwiKTtcbnZhciByZWFjdF8xID0gcmVxdWlyZShcInJlYWN0XCIpO1xudmFyIENvbnRhY3RBZGRyZXNzU2VsZWN0ID0gZnVuY3Rpb24gKHByb3BzKSB7XG4gICAgdmFyIF9hID0gKDAsIHJlYWN0XzEudXNlU3RhdGUpKG51bGwpLCBjb250YWN0VXVpZCA9IF9hWzBdLCBzZXRDb250YWN0VXVpZCA9IF9hWzFdO1xuICAgICgwLCByZWFjdF8xLnVzZUVmZmVjdCkoZnVuY3Rpb24gKCkge1xuICAgICAgICBzZXRDb250YWN0VXVpZChwcm9wcy5jb250YWN0LnV1aWQpO1xuICAgIH0sIFtwcm9wcy5jb250YWN0LnV1aWRdKTtcbiAgICB2YXIgbG9hZE9wdGlvbnMgPSBmdW5jdGlvbiAoc2VhcmNoUXVlcnksIGxvYWRlZE9wdGlvbnMpIHsgcmV0dXJuIF9fYXdhaXRlcih2b2lkIDAsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBjb250YWN0X3NlcnZpY2UsIGRhdGE7XG4gICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcbiAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcbiAgICAgICAgICAgICAgICBjYXNlIDA6XG4gICAgICAgICAgICAgICAgICAgIGNvbnRhY3Rfc2VydmljZSA9IG5ldyBjb250YWN0XzIuQ29udGFjdFNlcnZpY2UoKTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgY29udGFjdF9zZXJ2aWNlLmdldENvbnRhY3RBZGRyZXNzKGNvbnRhY3RVdWlkLCBwcm9wcy50eXBlKV07XG4gICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICBkYXRhID0gX2Euc2VudCgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IGRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzTW9yZTogZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgIH1dO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTsgfTtcbiAgICB2YXIgb25DaGFuZ2UgPSBmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgICAgIGlmICh0eXBlb2YgcHJvcHMub25DaGFuZ2UgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgcHJvcHMub25DaGFuZ2Uob3B0aW9uKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgcmV0dXJuICgoMCwganN4X3J1bnRpbWVfMS5qc3gpKHJlYWN0X3NlbGVjdF9hc3luY19wYWdpbmF0ZV8xLkFzeW5jUGFnaW5hdGUsIHsgZGVmYXVsdE9wdGlvbnM6IHRydWUsIHZhbHVlOiBwcm9wcy52YWx1ZSwgbG9hZE9wdGlvbnM6IGxvYWRPcHRpb25zLCBnZXRPcHRpb25WYWx1ZTogZnVuY3Rpb24gKG9wdGlvbikgeyByZXR1cm4gb3B0aW9uLnV1aWQ7IH0sIGdldE9wdGlvbkxhYmVsOiBmdW5jdGlvbiAob3B0aW9uKSB7IHJldHVybiBvcHRpb24uZmlyc3RuYW1lICsgJyAnICsgb3B0aW9uLmxhc3RuYW1lOyB9LCBvbkNoYW5nZTogb25DaGFuZ2UsIGlzU2VhcmNoYWJsZTogZmFsc2UgfSwgSlNPTi5zdHJpbmdpZnkoY29udGFjdFV1aWQpKSk7XG59O1xuQ29udGFjdEFkZHJlc3NTZWxlY3QucHJvcFR5cGVzID0ge1xuICAgIHZhbHVlOiBQcm9wVHlwZXMuaW5zdGFuY2VPZihhZGRyZXNzXzEuQWRkcmVzcyksXG4gICAgY29udGFjdDogUHJvcFR5cGVzLmluc3RhbmNlT2YoY29udGFjdF8xLkNvbnRhY3QpLFxuICAgIHR5cGU6IFByb3BUeXBlcy5udW1iZXIsXG4gICAgb25DaGFuZ2U6IFByb3BUeXBlcy5mdW5jXG59O1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBDb250YWN0QWRkcmVzc1NlbGVjdDtcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fYXNzaWduID0gKHRoaXMgJiYgdGhpcy5fX2Fzc2lnbikgfHwgZnVuY3Rpb24gKCkge1xuICAgIF9fYXNzaWduID0gT2JqZWN0LmFzc2lnbiB8fCBmdW5jdGlvbih0KSB7XG4gICAgICAgIGZvciAodmFyIHMsIGkgPSAxLCBuID0gYXJndW1lbnRzLmxlbmd0aDsgaSA8IG47IGkrKykge1xuICAgICAgICAgICAgcyA9IGFyZ3VtZW50c1tpXTtcbiAgICAgICAgICAgIGZvciAodmFyIHAgaW4gcykgaWYgKE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChzLCBwKSlcbiAgICAgICAgICAgICAgICB0W3BdID0gc1twXTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gdDtcbiAgICB9O1xuICAgIHJldHVybiBfX2Fzc2lnbi5hcHBseSh0aGlzLCBhcmd1bWVudHMpO1xufTtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIganN4X3J1bnRpbWVfMSA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtcnVudGltZVwiKTtcbnZhciBDb250YWN0U2VsZWN0XzEgPSByZXF1aXJlKFwiLi9Db250YWN0U2VsZWN0XCIpO1xudmFyIENvbnRhY3RBZGRyZXNzU2VsZWN0XzEgPSByZXF1aXJlKFwiLi9Db250YWN0QWRkcmVzc1NlbGVjdFwiKTtcbnZhciBjb250YWN0XzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvY29udGFjdFwiKTtcbnZhciBhZGRyZXNzXzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvYWRkcmVzc1wiKTtcbnZhciByZWFjdF8xID0gcmVxdWlyZShcInJlYWN0XCIpO1xudmFyIG9yZGVyXzEgPSByZXF1aXJlKFwiLi4vLi4vc3RhdGUvb3JkZXJcIik7XG52YXIgdHN5cmluZ2VfMSA9IHJlcXVpcmUoXCJ0c3lyaW5nZVwiKTtcbnZhciBBY2NvdW50U2VsZWN0Q29tcG9uZW50XzEgPSByZXF1aXJlKFwiLi4vYWNjb3VudC9BY2NvdW50U2VsZWN0Q29tcG9uZW50XCIpO1xudmFyIENvbnRhY3RDb21wb25lbnQgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICB2YXIgb3JkZXJTdGF0ZSA9IHRzeXJpbmdlXzEuY29udGFpbmVyLnJlc29sdmUob3JkZXJfMS5PcmRlcik7XG4gICAgdmFyIF9hID0gKDAsIHJlYWN0XzEudXNlU3RhdGUpKG5ldyBjb250YWN0XzEuQ29udGFjdCgpKSwgY29udGFjdCA9IF9hWzBdLCBzZXRDb250YWN0ID0gX2FbMV07XG4gICAgdmFyIF9iID0gKDAsIHJlYWN0XzEudXNlU3RhdGUpKG5ldyBhZGRyZXNzXzEuQWRkcmVzcygpKSwgY29udGFjdEFkZHJlc3NJbnZvaWNlID0gX2JbMF0sIHNldENvbnRhY3RBZGRyZXNzSW52b2ljZSA9IF9iWzFdO1xuICAgIHZhciBfYyA9ICgwLCByZWFjdF8xLnVzZVN0YXRlKShuZXcgYWRkcmVzc18xLkFkZHJlc3MoKSksIGNvbnRhY3RBZGRyZXNzRGVsaXZlcnkgPSBfY1swXSwgc2V0Q29udGFjdEFkZHJlc3NEZWxpdmVyeSA9IF9jWzFdO1xuICAgIHZhciBfZCA9ICgwLCByZWFjdF8xLnVzZVN0YXRlKShuZXcgYWRkcmVzc18xLkFkZHJlc3MoKSksIGNvbnRhY3RBZGRyZXNzU2VuZGVyID0gX2RbMF0sIHNldENvbnRhY3RBZGRyZXNzU2VuZGVyID0gX2RbMV07XG4gICAgb3JkZXJTdGF0ZS5kZWxpdmVyeUFkZHJlc3MgPSBjb250YWN0QWRkcmVzc0RlbGl2ZXJ5O1xuICAgIG9yZGVyU3RhdGUuaW52b2ljZUFkZHJlc3MgPSBjb250YWN0QWRkcmVzc0ludm9pY2U7XG4gICAgb3JkZXJTdGF0ZS5zZW5kZXJBZGRyZXNzID0gY29udGFjdEFkZHJlc3NTZW5kZXI7XG4gICAgcmV0dXJuICgoMCwganN4X3J1bnRpbWVfMS5qc3hzKShcImRpdlwiLCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4cykoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdyb3cnIH0sIHsgY2hpbGRyZW46IFsoMCwganN4X3J1bnRpbWVfMS5qc3hzKShcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogJ2NvbC02JyB9LCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4KShcImg0XCIsIHsgY2hpbGRyZW46IFwiQ29udGFjdFwiIH0pLCAoMCwganN4X3J1bnRpbWVfMS5qc3gpKENvbnRhY3RTZWxlY3RfMVtcImRlZmF1bHRcIl0sIHsgdmFsdWU6IGNvbnRhY3QsIHNob3A6IHByb3BzLnNob3AsIG9uQ2hhbmdlOiBzZXRDb250YWN0IH0pXSB9KSksICgwLCBqc3hfcnVudGltZV8xLmpzeHMpKFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiAnY29sLTYnIH0sIHsgY2hpbGRyZW46IFsoMCwganN4X3J1bnRpbWVfMS5qc3gpKFwiaDRcIiwgeyBjaGlsZHJlbjogXCJBY2NvdW50XCIgfSksICgwLCBqc3hfcnVudGltZV8xLmpzeCkoQWNjb3VudFNlbGVjdENvbXBvbmVudF8xW1wiZGVmYXVsdFwiXSwgeyBzaG9wOiBwcm9wcy5zaG9wIH0pXSB9KSldIH0pKSwgKDAsIGpzeF9ydW50aW1lXzEuanN4cykoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdyb3cnIH0sIHsgY2hpbGRyZW46IFsoMCwganN4X3J1bnRpbWVfMS5qc3hzKShcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogJ2NvbC00JyB9LCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4KShcImg0XCIsIHsgY2hpbGRyZW46IFwiSW52b2ljZVwiIH0pLCAoMCwganN4X3J1bnRpbWVfMS5qc3gpKENvbnRhY3RBZGRyZXNzU2VsZWN0XzFbXCJkZWZhdWx0XCJdLCB7IHZhbHVlOiBjb250YWN0QWRkcmVzc0ludm9pY2UsIGNvbnRhY3Q6IGNvbnRhY3QsIHR5cGU6IDEsIG9uQ2hhbmdlOiBzZXRDb250YWN0QWRkcmVzc0ludm9pY2UgfSldIH0pKSwgKDAsIGpzeF9ydW50aW1lXzEuanN4cykoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdjb2wtNCcgfSwgeyBjaGlsZHJlbjogWygwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJoNFwiLCB7IGNoaWxkcmVuOiBcIkRlbGl2ZXJ5XCIgfSksICgwLCBqc3hfcnVudGltZV8xLmpzeCkoQ29udGFjdEFkZHJlc3NTZWxlY3RfMVtcImRlZmF1bHRcIl0sIHsgdmFsdWU6IGNvbnRhY3RBZGRyZXNzRGVsaXZlcnksIGNvbnRhY3Q6IGNvbnRhY3QsIHR5cGU6IDIsIG9uQ2hhbmdlOiBzZXRDb250YWN0QWRkcmVzc0RlbGl2ZXJ5IH0pXSB9KSksICgwLCBqc3hfcnVudGltZV8xLmpzeHMpKFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiAnY29sLTQnIH0sIHsgY2hpbGRyZW46IFsoMCwganN4X3J1bnRpbWVfMS5qc3gpKFwiaDRcIiwgeyBjaGlsZHJlbjogXCJTZW5kZXJcIiB9KSwgKDAsIGpzeF9ydW50aW1lXzEuanN4KShDb250YWN0QWRkcmVzc1NlbGVjdF8xW1wiZGVmYXVsdFwiXSwgeyB2YWx1ZTogY29udGFjdEFkZHJlc3NTZW5kZXIsIGNvbnRhY3Q6IGNvbnRhY3QsIHR5cGU6IDMsIG9uQ2hhbmdlOiBzZXRDb250YWN0QWRkcmVzc1NlbmRlciB9KV0gfSkpXSB9KSldIH0pKTtcbn07XG5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IENvbnRhY3RDb21wb25lbnQ7XG4iLCJcInVzZSBzdHJpY3RcIjtcbnZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xuICAgIGZ1bmN0aW9uIGFkb3B0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFAgPyB2YWx1ZSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGFkb3B0KHJlc3VsdC52YWx1ZSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XG4gICAgfSk7XG59O1xudmFyIF9fZ2VuZXJhdG9yID0gKHRoaXMgJiYgdGhpcy5fX2dlbmVyYXRvcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIGJvZHkpIHtcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xuICAgIGZ1bmN0aW9uIHZlcmIobikgeyByZXR1cm4gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHN0ZXAoW24sIHZdKTsgfTsgfVxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xuICAgICAgICB3aGlsZSAoXykgdHJ5IHtcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xuICAgIH1cbn07XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xudmFyIGpzeF9ydW50aW1lXzEgPSByZXF1aXJlKFwicmVhY3QvanN4LXJ1bnRpbWVcIik7XG52YXIgUHJvcFR5cGVzID0gcmVxdWlyZShcInByb3AtdHlwZXNcIik7XG52YXIgcmVhY3Rfc2VsZWN0X2FzeW5jX3BhZ2luYXRlXzEgPSByZXF1aXJlKFwicmVhY3Qtc2VsZWN0LWFzeW5jLXBhZ2luYXRlXCIpO1xudmFyIGNvbnRhY3RfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9jb250YWN0XCIpO1xudmFyIGNvbnRhY3RfMiA9IHJlcXVpcmUoXCIuLi8uLi9zZXJ2aWNlcy9jb250YWN0XCIpO1xudmFyIHNob3BfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9zaG9wXCIpO1xudmFyIHJlYWN0XzEgPSByZXF1aXJlKFwicmVhY3RcIik7XG52YXIgdHN5cmluZ2VfMSA9IHJlcXVpcmUoXCJ0c3lyaW5nZVwiKTtcbnZhciBvcmRlcl8xID0gcmVxdWlyZShcIi4uLy4uL3N0YXRlL29yZGVyXCIpO1xudmFyIENvbnRhY3RTZWxlY3QgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICB2YXIgX2EgPSAoMCwgcmVhY3RfMS51c2VTdGF0ZSkobnVsbCksIHNob3BVdWlkID0gX2FbMF0sIHNldFNob3BVdWlkID0gX2FbMV07XG4gICAgKDAsIHJlYWN0XzEudXNlRWZmZWN0KShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHNldFNob3BVdWlkKHByb3BzLnNob3AudXVpZCk7XG4gICAgfSwgW3Byb3BzLnNob3AudXVpZF0pO1xuICAgIHZhciBvcmRlciA9IHRzeXJpbmdlXzEuY29udGFpbmVyLnJlc29sdmUob3JkZXJfMS5PcmRlcik7XG4gICAgdmFyIGxvYWRPcHRpb25zID0gZnVuY3Rpb24gKHNlYXJjaFF1ZXJ5LCBsb2FkZWRPcHRpb25zLCBfYSkge1xuICAgICAgICB2YXIgcGFnZSA9IF9hLnBhZ2U7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgdmFyIGNvbnRhY3RfYXBpLCBjb250YWN0cztcbiAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2IpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKF9iLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRhY3RfYXBpID0gbmV3IGNvbnRhY3RfMi5Db250YWN0U2VydmljZSgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFs0IC8qeWllbGQqLywgY29udGFjdF9hcGkuZ2V0Q29udGFjdHMoc2VhcmNoUXVlcnksIHBhZ2UsIHByb3BzLnNob3ApXTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICAgICAgY29udGFjdHMgPSBfYi5zZW50KCk7XG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zOiBjb250YWN0cyxcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzTW9yZTogZmFsc2UsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZGl0aW9uYWw6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2U6IHBhZ2UgKyAxXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICB2YXIgb25DaGFuZ2UgPSBmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgICAgIGlmICh0eXBlb2YgcHJvcHMub25DaGFuZ2UgPT09IFwiZnVuY3Rpb25cIikge1xuICAgICAgICAgICAgcHJvcHMub25DaGFuZ2Uob3B0aW9uKTtcbiAgICAgICAgfVxuICAgICAgICBvcmRlci5jb250YWN0ID0gb3B0aW9uO1xuICAgIH07XG4gICAgcmV0dXJuICgoMCwganN4X3J1bnRpbWVfMS5qc3gpKHJlYWN0X3NlbGVjdF9hc3luY19wYWdpbmF0ZV8xLkFzeW5jUGFnaW5hdGUsIHsgZGVmYXVsdE9wdGlvbnM6IHRydWUsIHZhbHVlOiBwcm9wcy52YWx1ZSwgbG9hZE9wdGlvbnM6IGxvYWRPcHRpb25zLCBnZXRPcHRpb25WYWx1ZTogZnVuY3Rpb24gKG9wdGlvbikgeyByZXR1cm4gb3B0aW9uLnV1aWQ7IH0sIGdldE9wdGlvbkxhYmVsOiBmdW5jdGlvbiAob3B0aW9uKSB7IHJldHVybiBvcHRpb24udXNlcm5hbWU7IH0sIG9uQ2hhbmdlOiBvbkNoYW5nZSwgaXNTZWFyY2hhYmxlOiB0cnVlLCBhZGRpdGlvbmFsOiB7XG4gICAgICAgICAgICBwYWdlOiAxXG4gICAgICAgIH0gfSwgSlNPTi5zdHJpbmdpZnkoc2hvcFV1aWQpKSk7XG59O1xuQ29udGFjdFNlbGVjdC5wcm9wVHlwZXMgPSB7XG4gICAgdmFsdWU6IFByb3BUeXBlcy5pbnN0YW5jZU9mKGNvbnRhY3RfMS5Db250YWN0KSxcbiAgICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmMsXG4gICAgc2hvcDogUHJvcFR5cGVzLmluc3RhbmNlT2Yoc2hvcF8xLlNob3ApXG59O1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBDb250YWN0U2VsZWN0O1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19hc3NpZ24gPSAodGhpcyAmJiB0aGlzLl9fYXNzaWduKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKVxuICAgICAgICAgICAgICAgIHRbcF0gPSBzW3BdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0O1xuICAgIH07XG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBqc3hfcnVudGltZV8xID0gcmVxdWlyZShcInJlYWN0L2pzeC1ydW50aW1lXCIpO1xudmFyIHJlYWN0XzEgPSByZXF1aXJlKFwicmVhY3RcIik7XG52YXIgQnV0dG9uXzEgPSByZXF1aXJlKFwicmVhY3QtYm9vdHN0cmFwL0J1dHRvblwiKTtcbnZhciBNb2RhbF8xID0gcmVxdWlyZShcInJlYWN0LWJvb3RzdHJhcC9Nb2RhbFwiKTtcbnZhciBQcm9wVHlwZXMgPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKTtcbnZhciBzaG9wXzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvc2hvcFwiKTtcbnZhciBwcm9kdWN0XzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvcHJvZHVjdFwiKTtcbnZhciBwcm9kdWN0R3JvdXBfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9wcm9kdWN0R3JvdXBcIik7XG52YXIgUHJvZHVjdEdyb3VwU2VsZWN0XzEgPSByZXF1aXJlKFwiLi4vcHJvZHVjdC9Qcm9kdWN0R3JvdXBTZWxlY3RcIik7XG52YXIgUHJvZHVjdFNlbGVjdF8xID0gcmVxdWlyZShcIi4uL3Byb2R1Y3QvUHJvZHVjdFNlbGVjdFwiKTtcbnZhciBQcm9kdWN0Rm9ybV8xID0gcmVxdWlyZShcIi4uL3Byb2R1Y3QvUHJvZHVjdEZvcm1cIik7XG52YXIgQWRkUG9zaXRpb25Db21wb25lbnQgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICB2YXIgX2EgPSAoMCwgcmVhY3RfMS51c2VTdGF0ZSkobnVsbCksIHNob3AgPSBfYVswXSwgc2V0U2hvcCA9IF9hWzFdO1xuICAgICgwLCByZWFjdF8xLnVzZUVmZmVjdCkoZnVuY3Rpb24gKCkge1xuICAgICAgICBzZXRTaG9wKHByb3BzLnNob3ApO1xuICAgIH0sIFtwcm9wcy5zaG9wXSk7XG4gICAgdmFyIF9iID0gKDAsIHJlYWN0XzEudXNlU3RhdGUpKGZhbHNlKSwgc2hvdyA9IF9iWzBdLCBzZXRTaG93ID0gX2JbMV07XG4gICAgdmFyIGhhbmRsZUNsb3NlID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gc2V0U2hvdyhmYWxzZSk7IH07XG4gICAgdmFyIGhhbmRsZVNob3cgPSBmdW5jdGlvbiAoKSB7IHJldHVybiBzZXRTaG93KHRydWUpOyB9O1xuICAgIHZhciBfYyA9ICgwLCByZWFjdF8xLnVzZVN0YXRlKShuZXcgcHJvZHVjdF8xLlByb2R1Y3QoKSksIHByb2R1Y3QgPSBfY1swXSwgc2V0UHJvZHVjdCA9IF9jWzFdO1xuICAgIHZhciBfZCA9ICgwLCByZWFjdF8xLnVzZVN0YXRlKShuZXcgcHJvZHVjdEdyb3VwXzEuUHJvZHVjdEdyb3VwKCkpLCBwcm9kdWN0R3JvdXAgPSBfZFswXSwgc2V0UHJvZHVjdEdyb3VwID0gX2RbMV07XG4gICAgcmV0dXJuICgoMCwganN4X3J1bnRpbWVfMS5qc3hzKShcImRpdlwiLCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4KShCdXR0b25fMVtcImRlZmF1bHRcIl0sIF9fYXNzaWduKHsgdmFyaWFudDogXCJwcmltYXJ5XCIsIG9uQ2xpY2s6IGhhbmRsZVNob3cgfSwgeyBjaGlsZHJlbjogXCJhZGQgcG9zaXRpb25cIiB9KSksICgwLCBqc3hfcnVudGltZV8xLmpzeHMpKE1vZGFsXzFbXCJkZWZhdWx0XCJdLCBfX2Fzc2lnbih7IHNob3c6IHNob3csIGZ1bGxzY3JlZW46IHRydWUsIG9uSGlkZTogaGFuZGxlQ2xvc2UgfSwgeyBjaGlsZHJlbjogWygwLCBqc3hfcnVudGltZV8xLmpzeCkoTW9kYWxfMVtcImRlZmF1bHRcIl0uSGVhZGVyLCBfX2Fzc2lnbih7IGNsb3NlQnV0dG9uOiB0cnVlIH0sIHsgY2hpbGRyZW46ICgwLCBqc3hfcnVudGltZV8xLmpzeCkoTW9kYWxfMVtcImRlZmF1bHRcIl0uVGl0bGUsIHsgY2hpbGRyZW46IFwiYWRkIHBvc2l0aW9uXCIgfSkgfSkpLCAoMCwganN4X3J1bnRpbWVfMS5qc3hzKShNb2RhbF8xW1wiZGVmYXVsdFwiXS5Cb2R5LCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4cykoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdyb3cnIH0sIHsgY2hpbGRyZW46IFsoMCwganN4X3J1bnRpbWVfMS5qc3hzKShcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogJ2NvbC02JyB9LCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4KShcImg0XCIsIHsgY2hpbGRyZW46IFwiUHJvZHVjdEdyb3VwXCIgfSksICgwLCBqc3hfcnVudGltZV8xLmpzeCkoUHJvZHVjdEdyb3VwU2VsZWN0XzFbXCJkZWZhdWx0XCJdLCB7IHZhbHVlOiBwcm9kdWN0R3JvdXAsIHNob3A6IHNob3AsIG9uQ2hhbmdlOiBzZXRQcm9kdWN0R3JvdXAgfSldIH0pKSwgKDAsIGpzeF9ydW50aW1lXzEuanN4cykoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdjb2wtNicgfSwgeyBjaGlsZHJlbjogWygwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJoNFwiLCB7IGNoaWxkcmVuOiBcIlByb2R1Y3RcIiB9KSwgKDAsIGpzeF9ydW50aW1lXzEuanN4KShQcm9kdWN0U2VsZWN0XzFbXCJkZWZhdWx0XCJdLCB7IHZhbHVlOiBwcm9kdWN0LCBzaG9wOiBzaG9wLCBwcm9kdWN0R3JvdXA6IHByb2R1Y3RHcm91cCwgb25DaGFuZ2U6IHNldFByb2R1Y3QgfSldIH0pKV0gfSkpLCAoMCwganN4X3J1bnRpbWVfMS5qc3gpKFByb2R1Y3RGb3JtXzFbXCJkZWZhdWx0XCJdLCB7IHByb2R1Y3Q6IHByb2R1Y3QgfSldIH0pXSB9KSldIH0pKTtcbn07XG5BZGRQb3NpdGlvbkNvbXBvbmVudC5wcm9wVHlwZXMgPSB7XG4gICAgc2hvcDogUHJvcFR5cGVzLmluc3RhbmNlT2Yoc2hvcF8xLlNob3ApXG59O1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBBZGRQb3NpdGlvbkNvbXBvbmVudDtcbiIsIlwidXNlIHN0cmljdFwiO1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBqc3hfcnVudGltZV8xID0gcmVxdWlyZShcInJlYWN0L2pzeC1ydW50aW1lXCIpO1xudmFyIEFkZFBvc2l0aW9uQ29tcG9uZW50XzEgPSByZXF1aXJlKFwiLi9BZGRQb3NpdGlvbkNvbXBvbmVudFwiKTtcbnZhciBQcm9wVHlwZXMgPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKTtcbnZhciBzaG9wXzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvc2hvcFwiKTtcbnZhciBQb3NpdGlvbnNDb21wb25lbnQgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICByZXR1cm4gKCgwLCBqc3hfcnVudGltZV8xLmpzeCkoanN4X3J1bnRpbWVfMS5GcmFnbWVudCwgeyBjaGlsZHJlbjogKDAsIGpzeF9ydW50aW1lXzEuanN4KShBZGRQb3NpdGlvbkNvbXBvbmVudF8xW1wiZGVmYXVsdFwiXSwgeyBzaG9wOiBwcm9wcy5zaG9wIH0pIH0pKTtcbn07XG5Qb3NpdGlvbnNDb21wb25lbnQucHJvcFR5cGVzID0ge1xuICAgIHNob3A6IFByb3BUeXBlcy5pbnN0YW5jZU9mKHNob3BfMS5TaG9wKVxufTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gUG9zaXRpb25zQ29tcG9uZW50O1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19hc3NpZ24gPSAodGhpcyAmJiB0aGlzLl9fYXNzaWduKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKVxuICAgICAgICAgICAgICAgIHRbcF0gPSBzW3BdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0O1xuICAgIH07XG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBqc3hfcnVudGltZV8xID0gcmVxdWlyZShcInJlYWN0L2pzeC1ydW50aW1lXCIpO1xudmFyIFByb3BUeXBlcyA9IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpO1xudmFyIHNob3BfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9zaG9wXCIpO1xudmFyIHByb2R1Y3RfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9wcm9kdWN0XCIpO1xudmFyIHZhbGlkYXRvcl9hanY2XzEgPSByZXF1aXJlKFwiQHJqc2YvdmFsaWRhdG9yLWFqdjZcIik7XG52YXIgY29yZV8xID0gcmVxdWlyZShcIkByanNmL2NvcmVcIik7XG52YXIgcHJvZHVjdF8yID0gcmVxdWlyZShcIi4uLy4uL3NlcnZpY2VzL3Byb2R1Y3RcIik7XG52YXIgcmVhY3RfMSA9IHJlcXVpcmUoXCJyZWFjdFwiKTtcbnZhciBQcm9kdWN0Rm9ybSA9IGZ1bmN0aW9uIChwcm9wcykge1xuICAgIHZhciBfYSA9ICgwLCByZWFjdF8xLnVzZVN0YXRlKSh7fSksIHNjaGVtYSA9IF9hWzBdLCBzZXRTY2hlbWEgPSBfYVsxXTtcbiAgICB2YXIgbG9hZFNjaGVtYSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHByb3BzLnByb2R1Y3QudXVpZCA9PSBcIlwiKSB7XG4gICAgICAgICAgICByZXR1cm4gW107XG4gICAgICAgIH1cbiAgICAgICAgdmFyIHByb2R1Y3RfYXBpID0gbmV3IHByb2R1Y3RfMi5Qcm9kdWN0U2VydmljZSgpO1xuICAgICAgICBwcm9kdWN0X2FwaS5nZXRBZGRQcm9kdWN0U2NoZW1hKHByb3BzLnByb2R1Y3QpLnRoZW4oZnVuY3Rpb24gKHZhbHVlKSB7XG4gICAgICAgICAgICBzZXRTY2hlbWEodmFsdWVbJ2pzb25Gb3JtJ10pO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgICgwLCByZWFjdF8xLnVzZUVmZmVjdCkoZnVuY3Rpb24gKCkge1xuICAgICAgICBsb2FkU2NoZW1hKCk7XG4gICAgfSwgW3Byb3BzLnByb2R1Y3RdKTtcbiAgICB2YXIgbG9nID0gZnVuY3Rpb24gKHR5cGUpIHsgcmV0dXJuIGNvbnNvbGUubG9nLmJpbmQoY29uc29sZSwgdHlwZSk7IH07XG4gICAgcmV0dXJuICgoMCwganN4X3J1bnRpbWVfMS5qc3gpKGpzeF9ydW50aW1lXzEuRnJhZ21lbnQsIHsgY2hpbGRyZW46ICgwLCBqc3hfcnVudGltZV8xLmpzeHMpKFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiAncm93JyB9LCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4KShcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogJ2NvbC02JyB9LCB7IGNoaWxkcmVuOiAoMCwganN4X3J1bnRpbWVfMS5qc3gpKGNvcmVfMVtcImRlZmF1bHRcIl0sIHsgc2NoZW1hOiBzY2hlbWEsIHZhbGlkYXRvcjogdmFsaWRhdG9yX2FqdjZfMVtcImRlZmF1bHRcIl0sIG9uQ2hhbmdlOiBsb2coXCJjaGFuZ2VkXCIpLCBvblN1Ym1pdDogbG9nKFwic3VibWl0dGVkXCIpLCBvbkVycm9yOiBsb2coXCJlcnJvcnNcIikgfSkgfSkpLCAoMCwganN4X3J1bnRpbWVfMS5qc3hzKShcImRpdlwiLCBfX2Fzc2lnbih7IGNsYXNzTmFtZTogJ2NvbC02JyB9LCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4KShcImg1XCIsIHsgY2hpbGRyZW46IFwiTmV0dG86IFwiIH0pLCAoMCwganN4X3J1bnRpbWVfMS5qc3gpKFwiaDVcIiwgeyBjaGlsZHJlbjogXCJNd2VydDogXCIgfSksICgwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJoNFwiLCB7IGNoaWxkcmVuOiBcIkJydXR0b1wiIH0pXSB9KSldIH0pKSB9KSk7XG59O1xuUHJvZHVjdEZvcm0ucHJvcFR5cGVzID0ge1xuICAgIHNob3A6IFByb3BUeXBlcy5pbnN0YW5jZU9mKHNob3BfMS5TaG9wKSxcbiAgICBwcm9kdWN0OiBQcm9wVHlwZXMuaW5zdGFuY2VPZihwcm9kdWN0XzEuUHJvZHVjdClcbn07XG5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IFByb2R1Y3RGb3JtO1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19hd2FpdGVyID0gKHRoaXMgJiYgdGhpcy5fX2F3YWl0ZXIpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcbiAgICBmdW5jdGlvbiBhZG9wdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQID8gdmFsdWUgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBhZG9wdChyZXN1bHQudmFsdWUpLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xuICAgIH0pO1xufTtcbnZhciBfX2dlbmVyYXRvciA9ICh0aGlzICYmIHRoaXMuX19nZW5lcmF0b3IpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBib2R5KSB7XG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcbiAgICByZXR1cm4gZyA9IHsgbmV4dDogdmVyYigwKSwgXCJ0aHJvd1wiOiB2ZXJiKDEpLCBcInJldHVyblwiOiB2ZXJiKDIpIH0sIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiAoZ1tTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KSwgZztcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XG4gICAgICAgIGlmIChmKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgZXhlY3V0aW5nLlwiKTtcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBqc3hfcnVudGltZV8xID0gcmVxdWlyZShcInJlYWN0L2pzeC1ydW50aW1lXCIpO1xudmFyIFByb3BUeXBlcyA9IHJlcXVpcmUoXCJwcm9wLXR5cGVzXCIpO1xudmFyIHNob3BfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9zaG9wXCIpO1xudmFyIHRzeXJpbmdlXzEgPSByZXF1aXJlKFwidHN5cmluZ2VcIik7XG52YXIgb3JkZXJfMSA9IHJlcXVpcmUoXCIuLi8uLi9zdGF0ZS9vcmRlclwiKTtcbnZhciBwcm9kdWN0R3JvdXBfMSA9IHJlcXVpcmUoXCIuLi8uLi9tb2RlbC9wcm9kdWN0R3JvdXBcIik7XG52YXIgcHJvZHVjdGdyb3VwXzEgPSByZXF1aXJlKFwiLi4vLi4vc2VydmljZXMvcHJvZHVjdGdyb3VwXCIpO1xudmFyIGFzeW5jXzEgPSByZXF1aXJlKFwicmVhY3Qtc2VsZWN0L2FzeW5jXCIpO1xudmFyIFByb2R1Y3RHcm91cFNlbGVjdCA9IGZ1bmN0aW9uIChwcm9wcykge1xuICAgIHZhciBvcmRlciA9IHRzeXJpbmdlXzEuY29udGFpbmVyLnJlc29sdmUob3JkZXJfMS5PcmRlcik7XG4gICAgdmFyIGxvYWRPcHRpb25zID0gZnVuY3Rpb24gKHNlYXJjaFF1ZXJ5KSB7IHJldHVybiBfX2F3YWl0ZXIodm9pZCAwLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgcHJvZHVjdGdyb3VwX2FwaTtcbiAgICAgICAgcmV0dXJuIF9fZ2VuZXJhdG9yKHRoaXMsIGZ1bmN0aW9uIChfYSkge1xuICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgcHJvZHVjdGdyb3VwX2FwaSA9IG5ldyBwcm9kdWN0Z3JvdXBfMS5Qcm9kdWN0R3JvdXBTZXJ2aWNlKCk7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBbNCAvKnlpZWxkKi8sIHByb2R1Y3Rncm91cF9hcGkuZ2V0UHJvZHVjdEdyb3VwcyhzZWFyY2hRdWVyeSwgcHJvcHMuc2hvcCldO1xuICAgICAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIFsyIC8qcmV0dXJuKi8sIF9hLnNlbnQoKV07XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH0pOyB9O1xuICAgIHZhciBvbkNoYW5nZSA9IGZ1bmN0aW9uIChvcHRpb24pIHtcbiAgICAgICAgaWYgKHR5cGVvZiBwcm9wcy5vbkNoYW5nZSA9PT0gXCJmdW5jdGlvblwiKSB7XG4gICAgICAgICAgICBwcm9wcy5vbkNoYW5nZShvcHRpb24pO1xuICAgICAgICB9XG4gICAgfTtcbiAgICByZXR1cm4gKCgwLCBqc3hfcnVudGltZV8xLmpzeCkoYXN5bmNfMVtcImRlZmF1bHRcIl0sIHsgZGVmYXVsdE9wdGlvbnM6IHRydWUsIHZhbHVlOiBwcm9wcy52YWx1ZSwgbG9hZE9wdGlvbnM6IGxvYWRPcHRpb25zLCBnZXRPcHRpb25WYWx1ZTogZnVuY3Rpb24gKG9wdGlvbikgeyByZXR1cm4gb3B0aW9uLnV1aWQ7IH0sIG9uQ2hhbmdlOiBvbkNoYW5nZSwgaXNTZWFyY2hhYmxlOiB0cnVlIH0sIEpTT04uc3RyaW5naWZ5KHByb3BzLnNob3AudXVpZCkpKTtcbn07XG5Qcm9kdWN0R3JvdXBTZWxlY3QucHJvcFR5cGVzID0ge1xuICAgIHNob3A6IFByb3BUeXBlcy5pbnN0YW5jZU9mKHNob3BfMS5TaG9wKSxcbiAgICB2YWx1ZTogUHJvcFR5cGVzLmluc3RhbmNlT2YocHJvZHVjdEdyb3VwXzEuUHJvZHVjdEdyb3VwKSxcbiAgICBvbkNoYW5nZTogUHJvcFR5cGVzLmZ1bmNcbn07XG5leHBvcnRzW1wiZGVmYXVsdFwiXSA9IFByb2R1Y3RHcm91cFNlbGVjdDtcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XG4gICAgZnVuY3Rpb24gYWRvcHQodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUCA/IHZhbHVlIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogYWRvcHQocmVzdWx0LnZhbHVlKS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcbiAgICB9KTtcbn07XG52YXIgX19nZW5lcmF0b3IgPSAodGhpcyAmJiB0aGlzLl9fZ2VuZXJhdG9yKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgYm9keSkge1xuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xuICAgICAgICAgICAgaWYgKGYgPSAxLCB5ICYmICh0ID0gb3BbMF0gJiAyID8geVtcInJldHVyblwiXSA6IG9wWzBdID8geVtcInRocm93XCJdIHx8ICgodCA9IHlbXCJyZXR1cm5cIl0pICYmIHQuY2FsbCh5KSwgMCkgOiB5Lm5leHQpICYmICEodCA9IHQuY2FsbCh5LCBvcFsxXSkpLmRvbmUpIHJldHVybiB0O1xuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XG4gICAgfVxufTtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIganN4X3J1bnRpbWVfMSA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtcnVudGltZVwiKTtcbnZhciBQcm9wVHlwZXMgPSByZXF1aXJlKFwicHJvcC10eXBlc1wiKTtcbnZhciBzaG9wXzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvc2hvcFwiKTtcbnZhciB0c3lyaW5nZV8xID0gcmVxdWlyZShcInRzeXJpbmdlXCIpO1xudmFyIG9yZGVyXzEgPSByZXF1aXJlKFwiLi4vLi4vc3RhdGUvb3JkZXJcIik7XG52YXIgcHJvZHVjdF8xID0gcmVxdWlyZShcIi4uLy4uL21vZGVsL3Byb2R1Y3RcIik7XG52YXIgcHJvZHVjdEdyb3VwXzEgPSByZXF1aXJlKFwiLi4vLi4vbW9kZWwvcHJvZHVjdEdyb3VwXCIpO1xudmFyIGFzeW5jXzEgPSByZXF1aXJlKFwicmVhY3Qtc2VsZWN0L2FzeW5jXCIpO1xudmFyIHByb2R1Y3RfMiA9IHJlcXVpcmUoXCIuLi8uLi9zZXJ2aWNlcy9wcm9kdWN0XCIpO1xuZnVuY3Rpb24gZGVib3VuY2UoaW5uZXIsIG1zKSB7XG4gICAgaWYgKG1zID09PSB2b2lkIDApIHsgbXMgPSAzMDA7IH1cbiAgICB2YXIgdGltZXIgPSBudWxsO1xuICAgIHZhciByZXNvbHZlcyA9IFtdO1xuICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBhcmdzID0gW107XG4gICAgICAgIGZvciAodmFyIF9pID0gMDsgX2kgPCBhcmd1bWVudHMubGVuZ3RoOyBfaSsrKSB7XG4gICAgICAgICAgICBhcmdzW19pXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgICAgIH1cbiAgICAgICAgLy8gUnVuIHRoZSBmdW5jdGlvbiBhZnRlciBhIGNlcnRhaW4gYW1vdW50IG9mIHRpbWVcbiAgICAgICAgY2xlYXJUaW1lb3V0KHRpbWVyKTtcbiAgICAgICAgdGltZXIgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIC8vIEdldCB0aGUgcmVzdWx0IG9mIHRoZSBpbm5lciBmdW5jdGlvbiwgdGhlbiBhcHBseSBpdCB0byB0aGUgcmVzb2x2ZSBmdW5jdGlvbiBvZlxuICAgICAgICAgICAgLy8gZWFjaCBwcm9taXNlIHRoYXQgaGFzIGJlZW4gY3JlYXRlZCBzaW5jZSB0aGUgbGFzdCB0aW1lIHRoZSBpbm5lciBmdW5jdGlvbiB3YXMgcnVuXG4gICAgICAgICAgICB2YXIgcmVzdWx0ID0gaW5uZXIuYXBwbHkodm9pZCAwLCBhcmdzKTtcbiAgICAgICAgICAgIHJlc29sdmVzLmZvckVhY2goZnVuY3Rpb24gKHIpIHsgcmV0dXJuIHIocmVzdWx0KTsgfSk7XG4gICAgICAgICAgICByZXNvbHZlcyA9IFtdO1xuICAgICAgICB9LCBtcyk7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShmdW5jdGlvbiAocikgeyByZXR1cm4gcmVzb2x2ZXMucHVzaChyKTsgfSk7XG4gICAgfTtcbn1cbnZhciBQcm9kdWN0U2VsZWN0ID0gZnVuY3Rpb24gKHByb3BzKSB7XG4gICAgdmFyIG9yZGVyID0gdHN5cmluZ2VfMS5jb250YWluZXIucmVzb2x2ZShvcmRlcl8xLk9yZGVyKTtcbiAgICB2YXIgbG9hZE9wdGlvbnMgPSBkZWJvdW5jZShmdW5jdGlvbiAoc2VhcmNoUXVlcnkpIHsgcmV0dXJuIF9fYXdhaXRlcih2b2lkIDAsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBwcm9kdWN0X2FwaTtcbiAgICAgICAgcmV0dXJuIF9fZ2VuZXJhdG9yKHRoaXMsIGZ1bmN0aW9uIChfYSkge1xuICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgaWYgKHNlYXJjaFF1ZXJ5ID09IFwiXCIgJiYgcHJvcHMucHJvZHVjdEdyb3VwLnV1aWQgPT0gXCJcIikge1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFsyIC8qcmV0dXJuKi8sIFtdXTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBwcm9kdWN0X2FwaSA9IG5ldyBwcm9kdWN0XzIuUHJvZHVjdFNlcnZpY2UoKTtcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEoc2VhcmNoUXVlcnkgIT0gXCJcIikpIHJldHVybiBbMyAvKmJyZWFrKi8sIDJdO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBwcm9kdWN0X2FwaS5nZXRQcm9kdWN0c0J5VGVybShzZWFyY2hRdWVyeSwgcHJvcHMuc2hvcCldO1xuICAgICAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIFsyIC8qcmV0dXJuKi8sIF9hLnNlbnQoKV07XG4gICAgICAgICAgICAgICAgY2FzZSAyOiByZXR1cm4gWzQgLyp5aWVsZCovLCBwcm9kdWN0X2FwaS5nZXRQcm9kdWN0cyhwcm9wcy5wcm9kdWN0R3JvdXApXTtcbiAgICAgICAgICAgICAgICBjYXNlIDM6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTsgfSk7XG4gICAgdmFyIG9uQ2hhbmdlID0gZnVuY3Rpb24gKG9wdGlvbikge1xuICAgICAgICBpZiAodHlwZW9mIHByb3BzLm9uQ2hhbmdlID09PSBcImZ1bmN0aW9uXCIpIHtcbiAgICAgICAgICAgIHByb3BzLm9uQ2hhbmdlKG9wdGlvbik7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHJldHVybiAoKDAsIGpzeF9ydW50aW1lXzEuanN4KShhc3luY18xW1wiZGVmYXVsdFwiXSwgeyBkZWZhdWx0T3B0aW9uczogdHJ1ZSwgdmFsdWU6IHByb3BzLnZhbHVlLCBsb2FkT3B0aW9uczogbG9hZE9wdGlvbnMsIGdldE9wdGlvblZhbHVlOiBmdW5jdGlvbiAob3B0aW9uKSB7IHJldHVybiBvcHRpb24udXVpZDsgfSwgZ2V0T3B0aW9uTGFiZWw6IGZ1bmN0aW9uIChvcHRpb24pIHsgcmV0dXJuIG9wdGlvbi50aXRsZTsgfSwgb25DaGFuZ2U6IG9uQ2hhbmdlLCBpc1NlYXJjaGFibGU6IHRydWUgfSwgSlNPTi5zdHJpbmdpZnkocHJvcHMucHJvZHVjdEdyb3VwLnV1aWQpKSk7XG59O1xuUHJvZHVjdFNlbGVjdC5wcm9wVHlwZXMgPSB7XG4gICAgc2hvcDogUHJvcFR5cGVzLmluc3RhbmNlT2Yoc2hvcF8xLlNob3ApLFxuICAgIHZhbHVlOiBQcm9wVHlwZXMuaW5zdGFuY2VPZihwcm9kdWN0XzEuUHJvZHVjdCksXG4gICAgcHJvZHVjdEdyb3VwOiBQcm9wVHlwZXMuaW5zdGFuY2VPZihwcm9kdWN0R3JvdXBfMS5Qcm9kdWN0R3JvdXApLFxuICAgIG9uQ2hhbmdlOiBQcm9wVHlwZXMuZnVuY1xufTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gUHJvZHVjdFNlbGVjdDtcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XG4gICAgZnVuY3Rpb24gYWRvcHQodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUCA/IHZhbHVlIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogYWRvcHQocmVzdWx0LnZhbHVlKS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcbiAgICB9KTtcbn07XG52YXIgX19nZW5lcmF0b3IgPSAodGhpcyAmJiB0aGlzLl9fZ2VuZXJhdG9yKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgYm9keSkge1xuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xuICAgICAgICAgICAgaWYgKGYgPSAxLCB5ICYmICh0ID0gb3BbMF0gJiAyID8geVtcInJldHVyblwiXSA6IG9wWzBdID8geVtcInRocm93XCJdIHx8ICgodCA9IHlbXCJyZXR1cm5cIl0pICYmIHQuY2FsbCh5KSwgMCkgOiB5Lm5leHQpICYmICEodCA9IHQuY2FsbCh5LCBvcFsxXSkpLmRvbmUpIHJldHVybiB0O1xuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XG4gICAgfVxufTtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIganN4X3J1bnRpbWVfMSA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtcnVudGltZVwiKTtcbnZhciByZWFjdF9zZWxlY3RfYXN5bmNfcGFnaW5hdGVfMSA9IHJlcXVpcmUoXCJyZWFjdC1zZWxlY3QtYXN5bmMtcGFnaW5hdGVcIik7XG52YXIgc2hvcF8xID0gcmVxdWlyZShcIi4uLy4uL3NlcnZpY2VzL3Nob3BcIik7XG52YXIgdHN5cmluZ2VfMSA9IHJlcXVpcmUoXCJ0c3lyaW5nZVwiKTtcbnZhciBvcmRlcl8xID0gcmVxdWlyZShcIi4uLy4uL3N0YXRlL29yZGVyXCIpO1xudmFyIFNob3BTZWxlY3RDb21wb25lbnQgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICB2YXIgb3JkZXIgPSB0c3lyaW5nZV8xLmNvbnRhaW5lci5yZXNvbHZlKG9yZGVyXzEuT3JkZXIpO1xuICAgIHZhciBsb2FkT3B0aW9ucyA9IGZ1bmN0aW9uIChzZWFyY2hRdWVyeSwgbG9hZGVkT3B0aW9ucykgeyByZXR1cm4gX19hd2FpdGVyKHZvaWQgMCwgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIHNob3Bfc2VydmljZSwgZGF0YTtcbiAgICAgICAgcmV0dXJuIF9fZ2VuZXJhdG9yKHRoaXMsIGZ1bmN0aW9uIChfYSkge1xuICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDpcbiAgICAgICAgICAgICAgICAgICAgc2hvcF9zZXJ2aWNlID0gbmV3IHNob3BfMS5TaG9wU2VydmljZSgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzQgLyp5aWVsZCovLCBzaG9wX3NlcnZpY2UuZ2V0U2hvcHMoKV07XG4gICAgICAgICAgICAgICAgY2FzZSAxOlxuICAgICAgICAgICAgICAgICAgICBkYXRhID0gX2Euc2VudCgpO1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gWzIgLypyZXR1cm4qLywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IGRhdGEsXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzTW9yZTogZmFsc2VcbiAgICAgICAgICAgICAgICAgICAgICAgIH1dO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9KTsgfTtcbiAgICB2YXIgb25DaGFuZ2UgPSBmdW5jdGlvbiAob3B0aW9uKSB7XG4gICAgICAgIG9yZGVyLnNob3AgPSBvcHRpb24uaWQ7XG4gICAgICAgIHByb3BzLmNoYW5nZShvcHRpb24pO1xuICAgIH07XG4gICAgcmV0dXJuICgoMCwganN4X3J1bnRpbWVfMS5qc3gpKFwiZGl2XCIsIHsgY2hpbGRyZW46ICgwLCBqc3hfcnVudGltZV8xLmpzeCkocmVhY3Rfc2VsZWN0X2FzeW5jX3BhZ2luYXRlXzEuQXN5bmNQYWdpbmF0ZSwgeyBkZWZhdWx0T3B0aW9uczogdHJ1ZSwgbG9hZE9wdGlvbnM6IGxvYWRPcHRpb25zLCBnZXRPcHRpb25WYWx1ZTogZnVuY3Rpb24gKG9wdGlvbikgeyByZXR1cm4gb3B0aW9uLmlkOyB9LCBnZXRPcHRpb25MYWJlbDogZnVuY3Rpb24gKG9wdGlvbikgeyByZXR1cm4gb3B0aW9uLm5hbWU7IH0sIG9uQ2hhbmdlOiBvbkNoYW5nZSwgaXNTZWFyY2hhYmxlOiBmYWxzZSB9KSB9KSk7XG59O1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBTaG9wU2VsZWN0Q29tcG9uZW50O1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19hc3NpZ24gPSAodGhpcyAmJiB0aGlzLl9fYXNzaWduKSB8fCBmdW5jdGlvbiAoKSB7XG4gICAgX19hc3NpZ24gPSBPYmplY3QuYXNzaWduIHx8IGZ1bmN0aW9uKHQpIHtcbiAgICAgICAgZm9yICh2YXIgcywgaSA9IDEsIG4gPSBhcmd1bWVudHMubGVuZ3RoOyBpIDwgbjsgaSsrKSB7XG4gICAgICAgICAgICBzID0gYXJndW1lbnRzW2ldO1xuICAgICAgICAgICAgZm9yICh2YXIgcCBpbiBzKSBpZiAoT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKHMsIHApKVxuICAgICAgICAgICAgICAgIHRbcF0gPSBzW3BdO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB0O1xuICAgIH07XG4gICAgcmV0dXJuIF9fYXNzaWduLmFwcGx5KHRoaXMsIGFyZ3VtZW50cyk7XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbnZhciBqc3hfcnVudGltZV8xID0gcmVxdWlyZShcInJlYWN0L2pzeC1ydW50aW1lXCIpO1xudmFyIG9yZGVyXzEgPSByZXF1aXJlKFwiLi4vLi4vc3RhdGUvb3JkZXJcIik7XG52YXIgdHN5cmluZ2VfMSA9IHJlcXVpcmUoXCJ0c3lyaW5nZVwiKTtcbnZhciBvcmRlcl8yID0gcmVxdWlyZShcIi4uLy4uL3NlcnZpY2VzL29yZGVyXCIpO1xudmFyIFR5cGVTZWxlY3RDb21wb25lbnRfMSA9IHJlcXVpcmUoXCIuLi90eXBlL1R5cGVTZWxlY3RDb21wb25lbnRcIik7XG52YXIgU2hvcFNlbGVjdENvbXBvbmVudF8xID0gcmVxdWlyZShcIi4uL3Nob3AvU2hvcFNlbGVjdENvbXBvbmVudFwiKTtcbnZhciBUb3BCYXJDb21wb25lbnQgPSBmdW5jdGlvbiAocHJvcHMpIHtcbiAgICB2YXIgb3JkZXJTZXJ2aWNlID0gdHN5cmluZ2VfMS5jb250YWluZXIucmVzb2x2ZShvcmRlcl8yLk9yZGVyU2VydmljZSk7XG4gICAgdmFyIG9yZGVyID0gdHN5cmluZ2VfMS5jb250YWluZXIucmVzb2x2ZShvcmRlcl8xLk9yZGVyKTtcbiAgICB2YXIgaGFuZGxlU2F2ZSA9IGZ1bmN0aW9uIChlKSB7XG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgb3JkZXJTZXJ2aWNlLnNhdmVPcmRlcihvcmRlcik7XG4gICAgfTtcbiAgICByZXR1cm4gKCgwLCBqc3hfcnVudGltZV8xLmpzeHMpKFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiAncm93JyB9LCB7IGNoaWxkcmVuOiBbKDAsIGpzeF9ydW50aW1lXzEuanN4cykoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdjb2wtNCcgfSwgeyBjaGlsZHJlbjogWygwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJoNFwiLCB7IGNoaWxkcmVuOiBcIlR5cFwiIH0pLCBcIiBcIiwgKDAsIGpzeF9ydW50aW1lXzEuanN4KShUeXBlU2VsZWN0Q29tcG9uZW50XzFbXCJkZWZhdWx0XCJdLCB7fSldIH0pKSwgKDAsIGpzeF9ydW50aW1lXzEuanN4cykoXCJkaXZcIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdjb2wtNCcgfSwgeyBjaGlsZHJlbjogWygwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJoNFwiLCB7IGNoaWxkcmVuOiBcIlNob3BcIiB9KSwgXCIgXCIsICgwLCBqc3hfcnVudGltZV8xLmpzeCkoU2hvcFNlbGVjdENvbXBvbmVudF8xW1wiZGVmYXVsdFwiXSwgeyBzaG9wOiBwcm9wcy5zaG9wLCBjaGFuZ2U6IHByb3BzLmNoYW5nZSB9KV0gfSkpLCAoMCwganN4X3J1bnRpbWVfMS5qc3gpKFwiZGl2XCIsIF9fYXNzaWduKHsgY2xhc3NOYW1lOiAnY29sLTQnIH0sIHsgY2hpbGRyZW46ICgwLCBqc3hfcnVudGltZV8xLmpzeCkoXCJidXR0b25cIiwgX19hc3NpZ24oeyBjbGFzc05hbWU6ICdidG4gYnRuLXN1Y2Nlc3MnLCBvbkNsaWNrOiBoYW5kbGVTYXZlIH0sIHsgY2hpbGRyZW46IFwiU2F2ZSBPcmRlclwiIH0pKSB9KSldIH0pKSk7XG59O1xuZXhwb3J0c1tcImRlZmF1bHRcIl0gPSBUb3BCYXJDb21wb25lbnQ7XG4iLCJcInVzZSBzdHJpY3RcIjtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG52YXIganN4X3J1bnRpbWVfMSA9IHJlcXVpcmUoXCJyZWFjdC9qc3gtcnVudGltZVwiKTtcbnZhciByZWFjdF9zZWxlY3RfMSA9IHJlcXVpcmUoXCJyZWFjdC1zZWxlY3RcIik7XG52YXIgcmVhY3RfMSA9IHJlcXVpcmUoXCJyZWFjdFwiKTtcbnZhciB0c3lyaW5nZV8xID0gcmVxdWlyZShcInRzeXJpbmdlXCIpO1xudmFyIG9yZGVyXzEgPSByZXF1aXJlKFwiLi4vLi4vc3RhdGUvb3JkZXJcIik7XG52YXIgVHlwZVNlbGVjdENvbXBvbmVudCA9IGZ1bmN0aW9uIChwcm9wcykge1xuICAgIHZhciBfYSA9ICgwLCByZWFjdF8xLnVzZVN0YXRlKSgpLCB0eXBlID0gX2FbMF0sIHNldFR5cGUgPSBfYVsxXTtcbiAgICB2YXIgb3JkZXIgPSB0c3lyaW5nZV8xLmNvbnRhaW5lci5yZXNvbHZlKG9yZGVyXzEuT3JkZXIpO1xuICAgIHZhciBvcHRpb25zID0gW1xuICAgICAgICB7IHZhbHVlOiAxLCBsYWJlbDogJ0F1ZnRyYWcnIH0sXG4gICAgICAgIHsgdmFsdWU6IDIsIGxhYmVsOiAnQW5nZWJvdCcgfSxcbiAgICAgICAgeyB2YWx1ZTogMywgbGFiZWw6ICdTdG9ybm8nIH0sXG4gICAgICAgIHsgdmFsdWU6IDQsIGxhYmVsOiAnR3V0c2NocmlmdCcgfSxcbiAgICAgICAgeyB2YWx1ZTogNSwgbGFiZWw6ICdSZWNobnVuZycgfSxcbiAgICAgICAgeyB2YWx1ZTogNiwgbGFiZWw6ICdUZWlscmVjaG51bmcnIH0sXG4gICAgICAgIHsgdmFsdWU6IDcsIGxhYmVsOiAnVGVpbHN0b3JubycgfSxcbiAgICBdO1xuICAgIHZhciBvbkNoYW5nZSA9IGZ1bmN0aW9uIChvcHRpb24pIHtcbiAgICAgICAgb3JkZXIudHlwZSA9IG9wdGlvbi52YWx1ZTtcbiAgICB9O1xuICAgIHJldHVybiAoKDAsIGpzeF9ydW50aW1lXzEuanN4KShcImRpdlwiLCB7IGNoaWxkcmVuOiAoMCwganN4X3J1bnRpbWVfMS5qc3gpKHJlYWN0X3NlbGVjdF8xW1wiZGVmYXVsdFwiXSwgeyBvcHRpb25zOiBvcHRpb25zLCBvbkNoYW5nZTogb25DaGFuZ2UgfSkgfSkpO1xufTtcbmV4cG9ydHNbXCJkZWZhdWx0XCJdID0gVHlwZVNlbGVjdENvbXBvbmVudDtcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fZGVjb3JhdGUgPSAodGhpcyAmJiB0aGlzLl9fZGVjb3JhdGUpIHx8IGZ1bmN0aW9uIChkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XG59O1xudmFyIF9fbWV0YWRhdGEgPSAodGhpcyAmJiB0aGlzLl9fbWV0YWRhdGEpIHx8IGZ1bmN0aW9uIChrLCB2KSB7XG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xufTtcbnZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xuICAgIGZ1bmN0aW9uIGFkb3B0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFAgPyB2YWx1ZSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGFkb3B0KHJlc3VsdC52YWx1ZSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XG4gICAgfSk7XG59O1xudmFyIF9fZ2VuZXJhdG9yID0gKHRoaXMgJiYgdGhpcy5fX2dlbmVyYXRvcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIGJvZHkpIHtcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xuICAgIGZ1bmN0aW9uIHZlcmIobikgeyByZXR1cm4gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHN0ZXAoW24sIHZdKTsgfTsgfVxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xuICAgICAgICB3aGlsZSAoXykgdHJ5IHtcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xuICAgIH1cbn07XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0cy5BY2NvdW50U2VydmljZSA9IHZvaWQgMDtcbnZhciB0c3lyaW5nZV8xID0gcmVxdWlyZShcInRzeXJpbmdlXCIpO1xudmFyIHRva2VuXzEgPSByZXF1aXJlKFwiLi90b2tlblwiKTtcbnZhciBheGlvc18xID0gcmVxdWlyZShcImF4aW9zXCIpO1xudmFyIGFjY291bnRfMSA9IHJlcXVpcmUoXCIuLi9tb2RlbC9hY2NvdW50XCIpO1xudmFyIEFjY291bnRTZXJ2aWNlID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIEFjY291bnRTZXJ2aWNlKHRva2VuKSB7XG4gICAgICAgIHRoaXMudG9rZW4gPSB0b2tlbjtcbiAgICB9XG4gICAgQWNjb3VudFNlcnZpY2UucHJvdG90eXBlLmdldEFjY291bnRzID0gZnVuY3Rpb24gKHNob3ApIHtcbiAgICAgICAgcmV0dXJuIF9fYXdhaXRlcih0aGlzLCB2b2lkIDAsIHZvaWQgMCwgZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgcmV0dXJuIF9fZ2VuZXJhdG9yKHRoaXMsIGZ1bmN0aW9uIChfYSkge1xuICAgICAgICAgICAgICAgIHN3aXRjaCAoX2EubGFiZWwpIHtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAwOiByZXR1cm4gWzQgLyp5aWVsZCovLCBheGlvc18xW1wiZGVmYXVsdFwiXS5nZXQoJy9hcHBzL2FwaS9hY2NvdW50L2FsbGJ5c2hvcC8nICsgc2hvcC51dWlkLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6ICdCZWFyZXIgJyArIHRoaXMudG9rZW4uY3VycmVudFRva2VuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuZGF0YS5kYXRhLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgYWNjb3VudCA9IG5ldyBhY2NvdW50XzEuQWNjb3VudCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY2NvdW50LmlkID0gaXRlbS5pZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWNjb3VudC50aXRsZSA9IGl0ZW0udGl0bGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFjY291bnQuYXBwZW5kaXggPSBpdGVtLmFwcGVuZGl4O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhY2NvdW50LnV1aWQgPSBpdGVtLnV1aWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhY2NvdW50O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIEFjY291bnRTZXJ2aWNlID0gX19kZWNvcmF0ZShbXG4gICAgICAgICgwLCB0c3lyaW5nZV8xLnNpbmdsZXRvbikoKSxcbiAgICAgICAgKDAsIHRzeXJpbmdlXzEuYXV0b0luamVjdGFibGUpKCksXG4gICAgICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbdG9rZW5fMS5Ub2tlbl0pXG4gICAgXSwgQWNjb3VudFNlcnZpY2UpO1xuICAgIHJldHVybiBBY2NvdW50U2VydmljZTtcbn0oKSk7XG5leHBvcnRzLkFjY291bnRTZXJ2aWNlID0gQWNjb3VudFNlcnZpY2U7XG4iLCJcInVzZSBzdHJpY3RcIjtcbnZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xufTtcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShrLCB2KTtcbn07XG52YXIgX19hd2FpdGVyID0gKHRoaXMgJiYgdGhpcy5fX2F3YWl0ZXIpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcbiAgICBmdW5jdGlvbiBhZG9wdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQID8gdmFsdWUgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBhZG9wdChyZXN1bHQudmFsdWUpLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xuICAgIH0pO1xufTtcbnZhciBfX2dlbmVyYXRvciA9ICh0aGlzICYmIHRoaXMuX19nZW5lcmF0b3IpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBib2R5KSB7XG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcbiAgICByZXR1cm4gZyA9IHsgbmV4dDogdmVyYigwKSwgXCJ0aHJvd1wiOiB2ZXJiKDEpLCBcInJldHVyblwiOiB2ZXJiKDIpIH0sIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiAoZ1tTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KSwgZztcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XG4gICAgICAgIGlmIChmKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgZXhlY3V0aW5nLlwiKTtcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHMuQ29udGFjdFNlcnZpY2UgPSB2b2lkIDA7XG52YXIgdHN5cmluZ2VfMSA9IHJlcXVpcmUoXCJ0c3lyaW5nZVwiKTtcbnZhciB0b2tlbl8xID0gcmVxdWlyZShcIi4vdG9rZW5cIik7XG52YXIgYXhpb3NfMSA9IHJlcXVpcmUoXCJheGlvc1wiKTtcbnZhciBjb250YWN0XzEgPSByZXF1aXJlKFwiLi4vbW9kZWwvY29udGFjdFwiKTtcbnZhciBhZGRyZXNzXzEgPSByZXF1aXJlKFwiLi4vbW9kZWwvYWRkcmVzc1wiKTtcbnZhciBDb250YWN0U2VydmljZSA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBDb250YWN0U2VydmljZSh0b2tlbikge1xuICAgICAgICB0aGlzLnRva2VuID0gdG9rZW47XG4gICAgfVxuICAgIENvbnRhY3RTZXJ2aWNlLnByb3RvdHlwZS5nZXRDb250YWN0cyA9IGZ1bmN0aW9uIChzZWFyY2hRdWVyeSwgcGFnZSwgc2hvcCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDA6IHJldHVybiBbNCAvKnlpZWxkKi8sIGF4aW9zXzFbXCJkZWZhdWx0XCJdLmdldCgnL2FwcHMvYXBpL2NvbnRhY3QvYWxsYnlzaG9wLycgKyBzaG9wLnV1aWQgKyAnP3Rlcm09JyArIHNlYXJjaFF1ZXJ5ICsgJyZwYWdlPScgKyBwYWdlLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6ICdCZWFyZXIgJyArIHRoaXMudG9rZW4uY3VycmVudFRva2VuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuZGF0YS5kYXRhLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgY29udGFjdCA9IG5ldyBjb250YWN0XzEuQ29udGFjdDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29udGFjdC5pZCA9IGl0ZW0uaWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRhY3QudXNlcm5hbWUgPSBpdGVtLnVzZXJuYW1lO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb250YWN0LnV1aWQgPSBpdGVtLnV1aWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBjb250YWN0O1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIENvbnRhY3RTZXJ2aWNlLnByb3RvdHlwZS5nZXRDb250YWN0QWRkcmVzcyA9IGZ1bmN0aW9uIChjb250YWN0LCB0eXBlKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgYXhpb3NfMVtcImRlZmF1bHRcIl0uZ2V0KCcvYXBwcy9hcGkvY29udGFjdC9hZGRyZXNzL2dldGFsbGZvcnR5cGUvJyArIGNvbnRhY3QgKyAnLycgKyB0eXBlLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6ICdCZWFyZXIgJyArIHRoaXMudG9rZW4uY3VycmVudFRva2VuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuZGF0YS5kYXRhLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgYWRkcmVzcyA9IG5ldyBhZGRyZXNzXzEuQWRkcmVzcztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYWRkcmVzcy5pZCA9IGl0ZW0uaWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZHJlc3MuZmlyc3RuYW1lID0gaXRlbS5maXJzdG5hbWU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFkZHJlc3MubGFzdG5hbWUgPSBpdGVtLmxhc3RuYW1lO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBhZGRyZXNzLnV1aWQgPSBpdGVtLnV1aWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhZGRyZXNzO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIENvbnRhY3RTZXJ2aWNlID0gX19kZWNvcmF0ZShbXG4gICAgICAgICgwLCB0c3lyaW5nZV8xLnNpbmdsZXRvbikoKSxcbiAgICAgICAgKDAsIHRzeXJpbmdlXzEuYXV0b0luamVjdGFibGUpKCksXG4gICAgICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbdG9rZW5fMS5Ub2tlbl0pXG4gICAgXSwgQ29udGFjdFNlcnZpY2UpO1xuICAgIHJldHVybiBDb250YWN0U2VydmljZTtcbn0oKSk7XG5leHBvcnRzLkNvbnRhY3RTZXJ2aWNlID0gQ29udGFjdFNlcnZpY2U7XG4iLCJcInVzZSBzdHJpY3RcIjtcbnZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xufTtcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShrLCB2KTtcbn07XG52YXIgX19hd2FpdGVyID0gKHRoaXMgJiYgdGhpcy5fX2F3YWl0ZXIpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcbiAgICBmdW5jdGlvbiBhZG9wdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQID8gdmFsdWUgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBhZG9wdChyZXN1bHQudmFsdWUpLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xuICAgIH0pO1xufTtcbnZhciBfX2dlbmVyYXRvciA9ICh0aGlzICYmIHRoaXMuX19nZW5lcmF0b3IpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBib2R5KSB7XG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcbiAgICByZXR1cm4gZyA9IHsgbmV4dDogdmVyYigwKSwgXCJ0aHJvd1wiOiB2ZXJiKDEpLCBcInJldHVyblwiOiB2ZXJiKDIpIH0sIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiAoZ1tTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KSwgZztcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XG4gICAgICAgIGlmIChmKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgZXhlY3V0aW5nLlwiKTtcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHMuT3JkZXJTZXJ2aWNlID0gdm9pZCAwO1xudmFyIHRzeXJpbmdlXzEgPSByZXF1aXJlKFwidHN5cmluZ2VcIik7XG52YXIgdG9rZW5fMSA9IHJlcXVpcmUoXCIuL3Rva2VuXCIpO1xudmFyIGF4aW9zXzEgPSByZXF1aXJlKFwiYXhpb3NcIik7XG52YXIgT3JkZXJTZXJ2aWNlID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIE9yZGVyU2VydmljZSh0b2tlbikge1xuICAgICAgICB0aGlzLnRva2VuID0gdG9rZW47XG4gICAgfVxuICAgIE9yZGVyU2VydmljZS5wcm90b3R5cGUuc2F2ZU9yZGVyID0gZnVuY3Rpb24gKG9yZGVyKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgYXhpb3NfMVtcImRlZmF1bHRcIl0ucG9zdCgnL2FwcHMvYXBpL29yZGVyL2NyZWF0ZScsIG9yZGVyLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6ICdCZWFyZXIgJyArIHRoaXMudG9rZW4uY3VycmVudFRva2VuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLmxvZyhyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmRhdGE7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KV07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIFsyIC8qcmV0dXJuKi8sIF9hLnNlbnQoKV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgT3JkZXJTZXJ2aWNlID0gX19kZWNvcmF0ZShbXG4gICAgICAgICgwLCB0c3lyaW5nZV8xLnNpbmdsZXRvbikoKSxcbiAgICAgICAgKDAsIHRzeXJpbmdlXzEuYXV0b0luamVjdGFibGUpKCksXG4gICAgICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbdG9rZW5fMS5Ub2tlbl0pXG4gICAgXSwgT3JkZXJTZXJ2aWNlKTtcbiAgICByZXR1cm4gT3JkZXJTZXJ2aWNlO1xufSgpKTtcbmV4cG9ydHMuT3JkZXJTZXJ2aWNlID0gT3JkZXJTZXJ2aWNlO1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19kZWNvcmF0ZSA9ICh0aGlzICYmIHRoaXMuX19kZWNvcmF0ZSkgfHwgZnVuY3Rpb24gKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcbn07XG52YXIgX19tZXRhZGF0YSA9ICh0aGlzICYmIHRoaXMuX19tZXRhZGF0YSkgfHwgZnVuY3Rpb24gKGssIHYpIHtcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QubWV0YWRhdGEgPT09IFwiZnVuY3Rpb25cIikgcmV0dXJuIFJlZmxlY3QubWV0YWRhdGEoaywgdik7XG59O1xudmFyIF9fYXdhaXRlciA9ICh0aGlzICYmIHRoaXMuX19hd2FpdGVyKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgX2FyZ3VtZW50cywgUCwgZ2VuZXJhdG9yKSB7XG4gICAgZnVuY3Rpb24gYWRvcHQodmFsdWUpIHsgcmV0dXJuIHZhbHVlIGluc3RhbmNlb2YgUCA/IHZhbHVlIDogbmV3IFAoZnVuY3Rpb24gKHJlc29sdmUpIHsgcmVzb2x2ZSh2YWx1ZSk7IH0pOyB9XG4gICAgcmV0dXJuIG5ldyAoUCB8fCAoUCA9IFByb21pc2UpKShmdW5jdGlvbiAocmVzb2x2ZSwgcmVqZWN0KSB7XG4gICAgICAgIGZ1bmN0aW9uIGZ1bGZpbGxlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvci5uZXh0KHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gcmVqZWN0ZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3JbXCJ0aHJvd1wiXSh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHN0ZXAocmVzdWx0KSB7IHJlc3VsdC5kb25lID8gcmVzb2x2ZShyZXN1bHQudmFsdWUpIDogYWRvcHQocmVzdWx0LnZhbHVlKS50aGVuKGZ1bGZpbGxlZCwgcmVqZWN0ZWQpOyB9XG4gICAgICAgIHN0ZXAoKGdlbmVyYXRvciA9IGdlbmVyYXRvci5hcHBseSh0aGlzQXJnLCBfYXJndW1lbnRzIHx8IFtdKSkubmV4dCgpKTtcbiAgICB9KTtcbn07XG52YXIgX19nZW5lcmF0b3IgPSAodGhpcyAmJiB0aGlzLl9fZ2VuZXJhdG9yKSB8fCBmdW5jdGlvbiAodGhpc0FyZywgYm9keSkge1xuICAgIHZhciBfID0geyBsYWJlbDogMCwgc2VudDogZnVuY3Rpb24oKSB7IGlmICh0WzBdICYgMSkgdGhyb3cgdFsxXTsgcmV0dXJuIHRbMV07IH0sIHRyeXM6IFtdLCBvcHM6IFtdIH0sIGYsIHksIHQsIGc7XG4gICAgcmV0dXJuIGcgPSB7IG5leHQ6IHZlcmIoMCksIFwidGhyb3dcIjogdmVyYigxKSwgXCJyZXR1cm5cIjogdmVyYigyKSB9LCB0eXBlb2YgU3ltYm9sID09PSBcImZ1bmN0aW9uXCIgJiYgKGdbU3ltYm9sLml0ZXJhdG9yXSA9IGZ1bmN0aW9uKCkgeyByZXR1cm4gdGhpczsgfSksIGc7XG4gICAgZnVuY3Rpb24gdmVyYihuKSB7IHJldHVybiBmdW5jdGlvbiAodikgeyByZXR1cm4gc3RlcChbbiwgdl0pOyB9OyB9XG4gICAgZnVuY3Rpb24gc3RlcChvcCkge1xuICAgICAgICBpZiAoZikgdGhyb3cgbmV3IFR5cGVFcnJvcihcIkdlbmVyYXRvciBpcyBhbHJlYWR5IGV4ZWN1dGluZy5cIik7XG4gICAgICAgIHdoaWxlIChfKSB0cnkge1xuICAgICAgICAgICAgaWYgKGYgPSAxLCB5ICYmICh0ID0gb3BbMF0gJiAyID8geVtcInJldHVyblwiXSA6IG9wWzBdID8geVtcInRocm93XCJdIHx8ICgodCA9IHlbXCJyZXR1cm5cIl0pICYmIHQuY2FsbCh5KSwgMCkgOiB5Lm5leHQpICYmICEodCA9IHQuY2FsbCh5LCBvcFsxXSkpLmRvbmUpIHJldHVybiB0O1xuICAgICAgICAgICAgaWYgKHkgPSAwLCB0KSBvcCA9IFtvcFswXSAmIDIsIHQudmFsdWVdO1xuICAgICAgICAgICAgc3dpdGNoIChvcFswXSkge1xuICAgICAgICAgICAgICAgIGNhc2UgMDogY2FzZSAxOiB0ID0gb3A7IGJyZWFrO1xuICAgICAgICAgICAgICAgIGNhc2UgNDogXy5sYWJlbCsrOyByZXR1cm4geyB2YWx1ZTogb3BbMV0sIGRvbmU6IGZhbHNlIH07XG4gICAgICAgICAgICAgICAgY2FzZSA1OiBfLmxhYmVsKys7IHkgPSBvcFsxXTsgb3AgPSBbMF07IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGNhc2UgNzogb3AgPSBfLm9wcy5wb3AoKTsgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBkZWZhdWx0OlxuICAgICAgICAgICAgICAgICAgICBpZiAoISh0ID0gXy50cnlzLCB0ID0gdC5sZW5ndGggPiAwICYmIHRbdC5sZW5ndGggLSAxXSkgJiYgKG9wWzBdID09PSA2IHx8IG9wWzBdID09PSAyKSkgeyBfID0gMDsgY29udGludWU7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSAzICYmICghdCB8fCAob3BbMV0gPiB0WzBdICYmIG9wWzFdIDwgdFszXSkpKSB7IF8ubGFiZWwgPSBvcFsxXTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKG9wWzBdID09PSA2ICYmIF8ubGFiZWwgPCB0WzFdKSB7IF8ubGFiZWwgPSB0WzFdOyB0ID0gb3A7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0ICYmIF8ubGFiZWwgPCB0WzJdKSB7IF8ubGFiZWwgPSB0WzJdOyBfLm9wcy5wdXNoKG9wKTsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHRbMl0pIF8ub3BzLnBvcCgpO1xuICAgICAgICAgICAgICAgICAgICBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgb3AgPSBib2R5LmNhbGwodGhpc0FyZywgXyk7XG4gICAgICAgIH0gY2F0Y2ggKGUpIHsgb3AgPSBbNiwgZV07IHkgPSAwOyB9IGZpbmFsbHkgeyBmID0gdCA9IDA7IH1cbiAgICAgICAgaWYgKG9wWzBdICYgNSkgdGhyb3cgb3BbMV07IHJldHVybiB7IHZhbHVlOiBvcFswXSA/IG9wWzFdIDogdm9pZCAwLCBkb25lOiB0cnVlIH07XG4gICAgfVxufTtcbmV4cG9ydHMuX19lc01vZHVsZSA9IHRydWU7XG5leHBvcnRzLlByb2R1Y3RTZXJ2aWNlID0gdm9pZCAwO1xudmFyIHRzeXJpbmdlXzEgPSByZXF1aXJlKFwidHN5cmluZ2VcIik7XG52YXIgdG9rZW5fMSA9IHJlcXVpcmUoXCIuL3Rva2VuXCIpO1xudmFyIGF4aW9zXzEgPSByZXF1aXJlKFwiYXhpb3NcIik7XG52YXIgcHJvZHVjdF8xID0gcmVxdWlyZShcIi4uL21vZGVsL3Byb2R1Y3RcIik7XG52YXIgUHJvZHVjdFNlcnZpY2UgPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gUHJvZHVjdFNlcnZpY2UodG9rZW4pIHtcbiAgICAgICAgdGhpcy50b2tlbiA9IHRva2VuO1xuICAgIH1cbiAgICBQcm9kdWN0U2VydmljZS5wcm90b3R5cGUuZ2V0UHJvZHVjdHNCeVRlcm0gPSBmdW5jdGlvbiAodGVybSwgc2hvcCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDA6IHJldHVybiBbNCAvKnlpZWxkKi8sIGF4aW9zXzFbXCJkZWZhdWx0XCJdLmdldCgnL2FwcHMvYXBpL3Byb2R1Y3QvZ2V0YWxsYnl0ZXJtLycgKyBzaG9wLnV1aWQgKyAnLycgKyB0ZXJtLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6ICdCZWFyZXIgJyArIHRoaXMudG9rZW4uY3VycmVudFRva2VuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuZGF0YS5kYXRhLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcCA9IG5ldyBwcm9kdWN0XzEuUHJvZHVjdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwLmlkID0gaXRlbS5pZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcC50aXRsZSA9IGl0ZW0udGl0bGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHAudXVpZCA9IGl0ZW0udXVpZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KV07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIFsyIC8qcmV0dXJuKi8sIF9hLnNlbnQoKV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgUHJvZHVjdFNlcnZpY2UucHJvdG90eXBlLmdldFByb2R1Y3RzID0gZnVuY3Rpb24gKHByb2R1Y3RHcm91cCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDA6IHJldHVybiBbNCAvKnlpZWxkKi8sIGF4aW9zXzFbXCJkZWZhdWx0XCJdLmdldCgnL2FwcHMvYXBpL3Byb2R1Y3QvZ2V0YWxsYnlwcm9kdWN0Z3JvdXAvJyArIHByb2R1Y3RHcm91cC51dWlkLCB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAnQXV0aG9yaXphdGlvbic6ICdCZWFyZXIgJyArIHRoaXMudG9rZW4uY3VycmVudFRva2VuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgfSkudGhlbihmdW5jdGlvbiAocmVzcG9uc2UpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2UuZGF0YS5kYXRhLm1hcChmdW5jdGlvbiAoaXRlbSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgcCA9IG5ldyBwcm9kdWN0XzEuUHJvZHVjdCgpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwLmlkID0gaXRlbS5pZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcC50aXRsZSA9IGl0ZW0udGl0bGU7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHAudXVpZCA9IGl0ZW0udXVpZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KV07XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMTogcmV0dXJuIFsyIC8qcmV0dXJuKi8sIF9hLnNlbnQoKV07XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgUHJvZHVjdFNlcnZpY2UucHJvdG90eXBlLmdldEFkZFByb2R1Y3RTY2hlbWEgPSBmdW5jdGlvbiAocHJvZHVjdCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDA6IHJldHVybiBbNCAvKnlpZWxkKi8sIGF4aW9zXzFbXCJkZWZhdWx0XCJdLnBvc3QoJy9hcHBzL2FwaS9wcm9kdWN0L2dldC9mb3JtL2pzb24vJyArIHByb2R1Y3QudXVpZCwgW10sIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdBdXRob3JpemF0aW9uJzogJ0JlYXJlciAnICsgdGhpcy50b2tlbi5jdXJyZW50VG9rZW5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5kYXRhO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSldO1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDE6IHJldHVybiBbMiAvKnJldHVybiovLCBfYS5zZW50KCldO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9KTtcbiAgICB9O1xuICAgIFByb2R1Y3RTZXJ2aWNlID0gX19kZWNvcmF0ZShbXG4gICAgICAgICgwLCB0c3lyaW5nZV8xLnNpbmdsZXRvbikoKSxcbiAgICAgICAgKDAsIHRzeXJpbmdlXzEuYXV0b0luamVjdGFibGUpKCksXG4gICAgICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbdG9rZW5fMS5Ub2tlbl0pXG4gICAgXSwgUHJvZHVjdFNlcnZpY2UpO1xuICAgIHJldHVybiBQcm9kdWN0U2VydmljZTtcbn0oKSk7XG5leHBvcnRzLlByb2R1Y3RTZXJ2aWNlID0gUHJvZHVjdFNlcnZpY2U7XG4iLCJcInVzZSBzdHJpY3RcIjtcbnZhciBfX2RlY29yYXRlID0gKHRoaXMgJiYgdGhpcy5fX2RlY29yYXRlKSB8fCBmdW5jdGlvbiAoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpIHtcbiAgICB2YXIgYyA9IGFyZ3VtZW50cy5sZW5ndGgsIHIgPSBjIDwgMyA/IHRhcmdldCA6IGRlc2MgPT09IG51bGwgPyBkZXNjID0gT2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcih0YXJnZXQsIGtleSkgOiBkZXNjLCBkO1xuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5kZWNvcmF0ZSA9PT0gXCJmdW5jdGlvblwiKSByID0gUmVmbGVjdC5kZWNvcmF0ZShkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYyk7XG4gICAgZWxzZSBmb3IgKHZhciBpID0gZGVjb3JhdG9ycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkgaWYgKGQgPSBkZWNvcmF0b3JzW2ldKSByID0gKGMgPCAzID8gZChyKSA6IGMgPiAzID8gZCh0YXJnZXQsIGtleSwgcikgOiBkKHRhcmdldCwga2V5KSkgfHwgcjtcbiAgICByZXR1cm4gYyA+IDMgJiYgciAmJiBPYmplY3QuZGVmaW5lUHJvcGVydHkodGFyZ2V0LCBrZXksIHIpLCByO1xufTtcbnZhciBfX21ldGFkYXRhID0gKHRoaXMgJiYgdGhpcy5fX21ldGFkYXRhKSB8fCBmdW5jdGlvbiAoaywgdikge1xuICAgIGlmICh0eXBlb2YgUmVmbGVjdCA9PT0gXCJvYmplY3RcIiAmJiB0eXBlb2YgUmVmbGVjdC5tZXRhZGF0YSA9PT0gXCJmdW5jdGlvblwiKSByZXR1cm4gUmVmbGVjdC5tZXRhZGF0YShrLCB2KTtcbn07XG52YXIgX19hd2FpdGVyID0gKHRoaXMgJiYgdGhpcy5fX2F3YWl0ZXIpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBfYXJndW1lbnRzLCBQLCBnZW5lcmF0b3IpIHtcbiAgICBmdW5jdGlvbiBhZG9wdCh2YWx1ZSkgeyByZXR1cm4gdmFsdWUgaW5zdGFuY2VvZiBQID8gdmFsdWUgOiBuZXcgUChmdW5jdGlvbiAocmVzb2x2ZSkgeyByZXNvbHZlKHZhbHVlKTsgfSk7IH1cbiAgICByZXR1cm4gbmV3IChQIHx8IChQID0gUHJvbWlzZSkpKGZ1bmN0aW9uIChyZXNvbHZlLCByZWplY3QpIHtcbiAgICAgICAgZnVuY3Rpb24gZnVsZmlsbGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yLm5leHQodmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiByZWplY3RlZCh2YWx1ZSkgeyB0cnkgeyBzdGVwKGdlbmVyYXRvcltcInRocm93XCJdKHZhbHVlKSk7IH0gY2F0Y2ggKGUpIHsgcmVqZWN0KGUpOyB9IH1cbiAgICAgICAgZnVuY3Rpb24gc3RlcChyZXN1bHQpIHsgcmVzdWx0LmRvbmUgPyByZXNvbHZlKHJlc3VsdC52YWx1ZSkgOiBhZG9wdChyZXN1bHQudmFsdWUpLnRoZW4oZnVsZmlsbGVkLCByZWplY3RlZCk7IH1cbiAgICAgICAgc3RlcCgoZ2VuZXJhdG9yID0gZ2VuZXJhdG9yLmFwcGx5KHRoaXNBcmcsIF9hcmd1bWVudHMgfHwgW10pKS5uZXh0KCkpO1xuICAgIH0pO1xufTtcbnZhciBfX2dlbmVyYXRvciA9ICh0aGlzICYmIHRoaXMuX19nZW5lcmF0b3IpIHx8IGZ1bmN0aW9uICh0aGlzQXJnLCBib2R5KSB7XG4gICAgdmFyIF8gPSB7IGxhYmVsOiAwLCBzZW50OiBmdW5jdGlvbigpIHsgaWYgKHRbMF0gJiAxKSB0aHJvdyB0WzFdOyByZXR1cm4gdFsxXTsgfSwgdHJ5czogW10sIG9wczogW10gfSwgZiwgeSwgdCwgZztcbiAgICByZXR1cm4gZyA9IHsgbmV4dDogdmVyYigwKSwgXCJ0aHJvd1wiOiB2ZXJiKDEpLCBcInJldHVyblwiOiB2ZXJiKDIpIH0sIHR5cGVvZiBTeW1ib2wgPT09IFwiZnVuY3Rpb25cIiAmJiAoZ1tTeW1ib2wuaXRlcmF0b3JdID0gZnVuY3Rpb24oKSB7IHJldHVybiB0aGlzOyB9KSwgZztcbiAgICBmdW5jdGlvbiB2ZXJiKG4pIHsgcmV0dXJuIGZ1bmN0aW9uICh2KSB7IHJldHVybiBzdGVwKFtuLCB2XSk7IH07IH1cbiAgICBmdW5jdGlvbiBzdGVwKG9wKSB7XG4gICAgICAgIGlmIChmKSB0aHJvdyBuZXcgVHlwZUVycm9yKFwiR2VuZXJhdG9yIGlzIGFscmVhZHkgZXhlY3V0aW5nLlwiKTtcbiAgICAgICAgd2hpbGUgKF8pIHRyeSB7XG4gICAgICAgICAgICBpZiAoZiA9IDEsIHkgJiYgKHQgPSBvcFswXSAmIDIgPyB5W1wicmV0dXJuXCJdIDogb3BbMF0gPyB5W1widGhyb3dcIl0gfHwgKCh0ID0geVtcInJldHVyblwiXSkgJiYgdC5jYWxsKHkpLCAwKSA6IHkubmV4dCkgJiYgISh0ID0gdC5jYWxsKHksIG9wWzFdKSkuZG9uZSkgcmV0dXJuIHQ7XG4gICAgICAgICAgICBpZiAoeSA9IDAsIHQpIG9wID0gW29wWzBdICYgMiwgdC52YWx1ZV07XG4gICAgICAgICAgICBzd2l0Y2ggKG9wWzBdKSB7XG4gICAgICAgICAgICAgICAgY2FzZSAwOiBjYXNlIDE6IHQgPSBvcDsgYnJlYWs7XG4gICAgICAgICAgICAgICAgY2FzZSA0OiBfLmxhYmVsKys7IHJldHVybiB7IHZhbHVlOiBvcFsxXSwgZG9uZTogZmFsc2UgfTtcbiAgICAgICAgICAgICAgICBjYXNlIDU6IF8ubGFiZWwrKzsgeSA9IG9wWzFdOyBvcCA9IFswXTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgY2FzZSA3OiBvcCA9IF8ub3BzLnBvcCgpOyBfLnRyeXMucG9wKCk7IGNvbnRpbnVlO1xuICAgICAgICAgICAgICAgIGRlZmF1bHQ6XG4gICAgICAgICAgICAgICAgICAgIGlmICghKHQgPSBfLnRyeXMsIHQgPSB0Lmxlbmd0aCA+IDAgJiYgdFt0Lmxlbmd0aCAtIDFdKSAmJiAob3BbMF0gPT09IDYgfHwgb3BbMF0gPT09IDIpKSB7IF8gPSAwOyBjb250aW51ZTsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDMgJiYgKCF0IHx8IChvcFsxXSA+IHRbMF0gJiYgb3BbMV0gPCB0WzNdKSkpIHsgXy5sYWJlbCA9IG9wWzFdOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAob3BbMF0gPT09IDYgJiYgXy5sYWJlbCA8IHRbMV0pIHsgXy5sYWJlbCA9IHRbMV07IHQgPSBvcDsgYnJlYWs7IH1cbiAgICAgICAgICAgICAgICAgICAgaWYgKHQgJiYgXy5sYWJlbCA8IHRbMl0pIHsgXy5sYWJlbCA9IHRbMl07IF8ub3BzLnB1c2gob3ApOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodFsyXSkgXy5vcHMucG9wKCk7XG4gICAgICAgICAgICAgICAgICAgIF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBvcCA9IGJvZHkuY2FsbCh0aGlzQXJnLCBfKTtcbiAgICAgICAgfSBjYXRjaCAoZSkgeyBvcCA9IFs2LCBlXTsgeSA9IDA7IH0gZmluYWxseSB7IGYgPSB0ID0gMDsgfVxuICAgICAgICBpZiAob3BbMF0gJiA1KSB0aHJvdyBvcFsxXTsgcmV0dXJuIHsgdmFsdWU6IG9wWzBdID8gb3BbMV0gOiB2b2lkIDAsIGRvbmU6IHRydWUgfTtcbiAgICB9XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHMuUHJvZHVjdEdyb3VwU2VydmljZSA9IHZvaWQgMDtcbnZhciB0c3lyaW5nZV8xID0gcmVxdWlyZShcInRzeXJpbmdlXCIpO1xudmFyIHRva2VuXzEgPSByZXF1aXJlKFwiLi90b2tlblwiKTtcbnZhciBheGlvc18xID0gcmVxdWlyZShcImF4aW9zXCIpO1xudmFyIHByb2R1Y3RHcm91cF8xID0gcmVxdWlyZShcIi4uL21vZGVsL3Byb2R1Y3RHcm91cFwiKTtcbnZhciBQcm9kdWN0R3JvdXBTZXJ2aWNlID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIFByb2R1Y3RHcm91cFNlcnZpY2UodG9rZW4pIHtcbiAgICAgICAgdGhpcy50b2tlbiA9IHRva2VuO1xuICAgIH1cbiAgICBQcm9kdWN0R3JvdXBTZXJ2aWNlLnByb3RvdHlwZS5nZXRQcm9kdWN0R3JvdXBzID0gZnVuY3Rpb24gKHNlYXJjaFF1ZXJ5LCBzaG9wKSB7XG4gICAgICAgIHJldHVybiBfX2F3YWl0ZXIodGhpcywgdm9pZCAwLCB2b2lkIDAsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHJldHVybiBfX2dlbmVyYXRvcih0aGlzLCBmdW5jdGlvbiAoX2EpIHtcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKF9hLmxhYmVsKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhc2UgMDogcmV0dXJuIFs0IC8qeWllbGQqLywgYXhpb3NfMVtcImRlZmF1bHRcIl0uZ2V0KCcvYXBwcy9hcGkvcHJvZHVjdGdyb3VwL2dldHRyZWUvJyArIHNob3AudXVpZCArICc/dGVybT0nICsgc2VhcmNoUXVlcnksIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICdBdXRob3JpemF0aW9uJzogJ0JlYXJlciAnICsgdGhpcy50b2tlbi5jdXJyZW50VG9rZW5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgICAgICB9KS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZS5kYXRhLmRhdGEubWFwKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwZyA9IG5ldyBwcm9kdWN0R3JvdXBfMS5Qcm9kdWN0R3JvdXA7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBnLmlkID0gaXRlbS5pZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcGcubGFiZWwgPSBpdGVtLnRpdGxlO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwZy51dWlkID0gaXRlbS51dWlkO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVtLmNoaWxkcmVuLm1hcChmdW5jdGlvbiAoc3ViKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB2YXIgc3BnID0gbmV3IHByb2R1Y3RHcm91cF8xLlByb2R1Y3RHcm91cDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNwZy5pZCA9IHN1Yi5pZDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNwZy5sYWJlbCA9IHN1Yi50aXRsZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNwZy51dWlkID0gc3ViLnV1aWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwZy5vcHRpb25zLnB1c2goc3BnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBwZztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAxOiByZXR1cm4gWzIgLypyZXR1cm4qLywgX2Euc2VudCgpXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICBQcm9kdWN0R3JvdXBTZXJ2aWNlID0gX19kZWNvcmF0ZShbXG4gICAgICAgICgwLCB0c3lyaW5nZV8xLnNpbmdsZXRvbikoKSxcbiAgICAgICAgKDAsIHRzeXJpbmdlXzEuYXV0b0luamVjdGFibGUpKCksXG4gICAgICAgIF9fbWV0YWRhdGEoXCJkZXNpZ246cGFyYW10eXBlc1wiLCBbdG9rZW5fMS5Ub2tlbl0pXG4gICAgXSwgUHJvZHVjdEdyb3VwU2VydmljZSk7XG4gICAgcmV0dXJuIFByb2R1Y3RHcm91cFNlcnZpY2U7XG59KCkpO1xuZXhwb3J0cy5Qcm9kdWN0R3JvdXBTZXJ2aWNlID0gUHJvZHVjdEdyb3VwU2VydmljZTtcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fZGVjb3JhdGUgPSAodGhpcyAmJiB0aGlzLl9fZGVjb3JhdGUpIHx8IGZ1bmN0aW9uIChkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XG59O1xudmFyIF9fbWV0YWRhdGEgPSAodGhpcyAmJiB0aGlzLl9fbWV0YWRhdGEpIHx8IGZ1bmN0aW9uIChrLCB2KSB7XG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0Lm1ldGFkYXRhID09PSBcImZ1bmN0aW9uXCIpIHJldHVybiBSZWZsZWN0Lm1ldGFkYXRhKGssIHYpO1xufTtcbnZhciBfX2F3YWl0ZXIgPSAodGhpcyAmJiB0aGlzLl9fYXdhaXRlcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIF9hcmd1bWVudHMsIFAsIGdlbmVyYXRvcikge1xuICAgIGZ1bmN0aW9uIGFkb3B0KHZhbHVlKSB7IHJldHVybiB2YWx1ZSBpbnN0YW5jZW9mIFAgPyB2YWx1ZSA6IG5ldyBQKGZ1bmN0aW9uIChyZXNvbHZlKSB7IHJlc29sdmUodmFsdWUpOyB9KTsgfVxuICAgIHJldHVybiBuZXcgKFAgfHwgKFAgPSBQcm9taXNlKSkoZnVuY3Rpb24gKHJlc29sdmUsIHJlamVjdCkge1xuICAgICAgICBmdW5jdGlvbiBmdWxmaWxsZWQodmFsdWUpIHsgdHJ5IHsgc3RlcChnZW5lcmF0b3IubmV4dCh2YWx1ZSkpOyB9IGNhdGNoIChlKSB7IHJlamVjdChlKTsgfSB9XG4gICAgICAgIGZ1bmN0aW9uIHJlamVjdGVkKHZhbHVlKSB7IHRyeSB7IHN0ZXAoZ2VuZXJhdG9yW1widGhyb3dcIl0odmFsdWUpKTsgfSBjYXRjaCAoZSkgeyByZWplY3QoZSk7IH0gfVxuICAgICAgICBmdW5jdGlvbiBzdGVwKHJlc3VsdCkgeyByZXN1bHQuZG9uZSA/IHJlc29sdmUocmVzdWx0LnZhbHVlKSA6IGFkb3B0KHJlc3VsdC52YWx1ZSkudGhlbihmdWxmaWxsZWQsIHJlamVjdGVkKTsgfVxuICAgICAgICBzdGVwKChnZW5lcmF0b3IgPSBnZW5lcmF0b3IuYXBwbHkodGhpc0FyZywgX2FyZ3VtZW50cyB8fCBbXSkpLm5leHQoKSk7XG4gICAgfSk7XG59O1xudmFyIF9fZ2VuZXJhdG9yID0gKHRoaXMgJiYgdGhpcy5fX2dlbmVyYXRvcikgfHwgZnVuY3Rpb24gKHRoaXNBcmcsIGJvZHkpIHtcbiAgICB2YXIgXyA9IHsgbGFiZWw6IDAsIHNlbnQ6IGZ1bmN0aW9uKCkgeyBpZiAodFswXSAmIDEpIHRocm93IHRbMV07IHJldHVybiB0WzFdOyB9LCB0cnlzOiBbXSwgb3BzOiBbXSB9LCBmLCB5LCB0LCBnO1xuICAgIHJldHVybiBnID0geyBuZXh0OiB2ZXJiKDApLCBcInRocm93XCI6IHZlcmIoMSksIFwicmV0dXJuXCI6IHZlcmIoMikgfSwgdHlwZW9mIFN5bWJvbCA9PT0gXCJmdW5jdGlvblwiICYmIChnW1N5bWJvbC5pdGVyYXRvcl0gPSBmdW5jdGlvbigpIHsgcmV0dXJuIHRoaXM7IH0pLCBnO1xuICAgIGZ1bmN0aW9uIHZlcmIobikgeyByZXR1cm4gZnVuY3Rpb24gKHYpIHsgcmV0dXJuIHN0ZXAoW24sIHZdKTsgfTsgfVxuICAgIGZ1bmN0aW9uIHN0ZXAob3ApIHtcbiAgICAgICAgaWYgKGYpIHRocm93IG5ldyBUeXBlRXJyb3IoXCJHZW5lcmF0b3IgaXMgYWxyZWFkeSBleGVjdXRpbmcuXCIpO1xuICAgICAgICB3aGlsZSAoXykgdHJ5IHtcbiAgICAgICAgICAgIGlmIChmID0gMSwgeSAmJiAodCA9IG9wWzBdICYgMiA/IHlbXCJyZXR1cm5cIl0gOiBvcFswXSA/IHlbXCJ0aHJvd1wiXSB8fCAoKHQgPSB5W1wicmV0dXJuXCJdKSAmJiB0LmNhbGwoeSksIDApIDogeS5uZXh0KSAmJiAhKHQgPSB0LmNhbGwoeSwgb3BbMV0pKS5kb25lKSByZXR1cm4gdDtcbiAgICAgICAgICAgIGlmICh5ID0gMCwgdCkgb3AgPSBbb3BbMF0gJiAyLCB0LnZhbHVlXTtcbiAgICAgICAgICAgIHN3aXRjaCAob3BbMF0pIHtcbiAgICAgICAgICAgICAgICBjYXNlIDA6IGNhc2UgMTogdCA9IG9wOyBicmVhaztcbiAgICAgICAgICAgICAgICBjYXNlIDQ6IF8ubGFiZWwrKzsgcmV0dXJuIHsgdmFsdWU6IG9wWzFdLCBkb25lOiBmYWxzZSB9O1xuICAgICAgICAgICAgICAgIGNhc2UgNTogXy5sYWJlbCsrOyB5ID0gb3BbMV07IG9wID0gWzBdOyBjb250aW51ZTtcbiAgICAgICAgICAgICAgICBjYXNlIDc6IG9wID0gXy5vcHMucG9wKCk7IF8udHJ5cy5wb3AoKTsgY29udGludWU7XG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICAgICAgaWYgKCEodCA9IF8udHJ5cywgdCA9IHQubGVuZ3RoID4gMCAmJiB0W3QubGVuZ3RoIC0gMV0pICYmIChvcFswXSA9PT0gNiB8fCBvcFswXSA9PT0gMikpIHsgXyA9IDA7IGNvbnRpbnVlOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gMyAmJiAoIXQgfHwgKG9wWzFdID4gdFswXSAmJiBvcFsxXSA8IHRbM10pKSkgeyBfLmxhYmVsID0gb3BbMV07IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmIChvcFswXSA9PT0gNiAmJiBfLmxhYmVsIDwgdFsxXSkgeyBfLmxhYmVsID0gdFsxXTsgdCA9IG9wOyBicmVhazsgfVxuICAgICAgICAgICAgICAgICAgICBpZiAodCAmJiBfLmxhYmVsIDwgdFsyXSkgeyBfLmxhYmVsID0gdFsyXTsgXy5vcHMucHVzaChvcCk7IGJyZWFrOyB9XG4gICAgICAgICAgICAgICAgICAgIGlmICh0WzJdKSBfLm9wcy5wb3AoKTtcbiAgICAgICAgICAgICAgICAgICAgXy50cnlzLnBvcCgpOyBjb250aW51ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIG9wID0gYm9keS5jYWxsKHRoaXNBcmcsIF8pO1xuICAgICAgICB9IGNhdGNoIChlKSB7IG9wID0gWzYsIGVdOyB5ID0gMDsgfSBmaW5hbGx5IHsgZiA9IHQgPSAwOyB9XG4gICAgICAgIGlmIChvcFswXSAmIDUpIHRocm93IG9wWzFdOyByZXR1cm4geyB2YWx1ZTogb3BbMF0gPyBvcFsxXSA6IHZvaWQgMCwgZG9uZTogdHJ1ZSB9O1xuICAgIH1cbn07XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0cy5TaG9wU2VydmljZSA9IHZvaWQgMDtcbnZhciB0c3lyaW5nZV8xID0gcmVxdWlyZShcInRzeXJpbmdlXCIpO1xudmFyIHRva2VuXzEgPSByZXF1aXJlKFwiLi90b2tlblwiKTtcbnZhciBheGlvc18xID0gcmVxdWlyZShcImF4aW9zXCIpO1xudmFyIHNob3BfMSA9IHJlcXVpcmUoXCIuLi9tb2RlbC9zaG9wXCIpO1xudmFyIFNob3BTZXJ2aWNlID0gLyoqIEBjbGFzcyAqLyAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIFNob3BTZXJ2aWNlKHRva2VuKSB7XG4gICAgICAgIHRoaXMudG9rZW4gPSB0b2tlbjtcbiAgICB9XG4gICAgU2hvcFNlcnZpY2UucHJvdG90eXBlLmdldFNob3BzID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gX19hd2FpdGVyKHRoaXMsIHZvaWQgMCwgdm9pZCAwLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICByZXR1cm4gX19nZW5lcmF0b3IodGhpcywgZnVuY3Rpb24gKF9hKSB7XG4gICAgICAgICAgICAgICAgc3dpdGNoIChfYS5sYWJlbCkge1xuICAgICAgICAgICAgICAgICAgICBjYXNlIDA6IHJldHVybiBbNCAvKnlpZWxkKi8sIGF4aW9zXzFbXCJkZWZhdWx0XCJdLmdldCgnL2FwcHMvYXBpL3Nob3BzJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhlYWRlcnM6IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJ0F1dGhvcml6YXRpb24nOiAnQmVhcmVyICcgKyB0aGlzLnRva2VuLmN1cnJlbnRUb2tlblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgICAgIH0pLnRoZW4oZnVuY3Rpb24gKHJlc3BvbnNlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlLmRhdGEuZGF0YS5tYXAoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFyIHNob3AgPSBuZXcgc2hvcF8xLlNob3AoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvcC5pZCA9IGl0ZW0uaWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNob3AubmFtZSA9IGl0ZW0ubmFtZTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2hvcC51dWlkID0gaXRlbS51dWlkO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gc2hvcDtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXTtcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAxOiByZXR1cm4gWzIgLypyZXR1cm4qLywgX2Euc2VudCgpXTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICBTaG9wU2VydmljZSA9IF9fZGVjb3JhdGUoW1xuICAgICAgICAoMCwgdHN5cmluZ2VfMS5zaW5nbGV0b24pKCksXG4gICAgICAgICgwLCB0c3lyaW5nZV8xLmF1dG9JbmplY3RhYmxlKSgpLFxuICAgICAgICBfX21ldGFkYXRhKFwiZGVzaWduOnBhcmFtdHlwZXNcIiwgW3Rva2VuXzEuVG9rZW5dKVxuICAgIF0sIFNob3BTZXJ2aWNlKTtcbiAgICByZXR1cm4gU2hvcFNlcnZpY2U7XG59KCkpO1xuZXhwb3J0cy5TaG9wU2VydmljZSA9IFNob3BTZXJ2aWNlO1xuIiwiXCJ1c2Ugc3RyaWN0XCI7XG52YXIgX19kZWNvcmF0ZSA9ICh0aGlzICYmIHRoaXMuX19kZWNvcmF0ZSkgfHwgZnVuY3Rpb24gKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKSB7XG4gICAgdmFyIGMgPSBhcmd1bWVudHMubGVuZ3RoLCByID0gYyA8IDMgPyB0YXJnZXQgOiBkZXNjID09PSBudWxsID8gZGVzYyA9IE9iamVjdC5nZXRPd25Qcm9wZXJ0eURlc2NyaXB0b3IodGFyZ2V0LCBrZXkpIDogZGVzYywgZDtcbiAgICBpZiAodHlwZW9mIFJlZmxlY3QgPT09IFwib2JqZWN0XCIgJiYgdHlwZW9mIFJlZmxlY3QuZGVjb3JhdGUgPT09IFwiZnVuY3Rpb25cIikgciA9IFJlZmxlY3QuZGVjb3JhdGUoZGVjb3JhdG9ycywgdGFyZ2V0LCBrZXksIGRlc2MpO1xuICAgIGVsc2UgZm9yICh2YXIgaSA9IGRlY29yYXRvcnMubGVuZ3RoIC0gMTsgaSA+PSAwOyBpLS0pIGlmIChkID0gZGVjb3JhdG9yc1tpXSkgciA9IChjIDwgMyA/IGQocikgOiBjID4gMyA/IGQodGFyZ2V0LCBrZXksIHIpIDogZCh0YXJnZXQsIGtleSkpIHx8IHI7XG4gICAgcmV0dXJuIGMgPiAzICYmIHIgJiYgT2JqZWN0LmRlZmluZVByb3BlcnR5KHRhcmdldCwga2V5LCByKSwgcjtcbn07XG5leHBvcnRzLl9fZXNNb2R1bGUgPSB0cnVlO1xuZXhwb3J0cy5Ub2tlbiA9IHZvaWQgMDtcbnZhciBheGlvc18xID0gcmVxdWlyZShcImF4aW9zXCIpO1xudmFyIHRzeXJpbmdlXzEgPSByZXF1aXJlKFwidHN5cmluZ2VcIik7XG52YXIgVG9rZW4gPSAvKiogQGNsYXNzICovIChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gVG9rZW4oKSB7XG4gICAgfVxuICAgIFRva2VuLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBzZXRJbnRlcnZhbChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBfdGhpcy5yZWZyZXNoVG9rZW4oKTtcbiAgICAgICAgfSwgMiAqIDYwICogMTAwMCk7XG4gICAgfTtcbiAgICBUb2tlbi5wcm90b3R5cGUucmVmcmVzaFRva2VuID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBheGlvc18xW1wiZGVmYXVsdFwiXS5nZXQoJy9hcHBzL2FwaS9jb250YWN0L3JlZnJlc2gnLCB7XG4gICAgICAgICAgICBoZWFkZXJzOiB7XG4gICAgICAgICAgICAgICAgJ0F1dGhvcml6YXRpb24nOiAnQmVhcmVyICcgKyB0aGlzLmN1cnJlbnRUb2tlblxuICAgICAgICAgICAgfVxuICAgICAgICB9KS50aGVuKGZ1bmN0aW9uIChyZXNwb25zZSkge1xuICAgICAgICAgICAgX3RoaXMuY3VycmVudFRva2VuID0gcmVzcG9uc2UuZGF0YS50b2tlbjtcbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICBUb2tlbiA9IF9fZGVjb3JhdGUoW1xuICAgICAgICAoMCwgdHN5cmluZ2VfMS5zaW5nbGV0b24pKClcbiAgICBdLCBUb2tlbik7XG4gICAgcmV0dXJuIFRva2VuO1xufSgpKTtcbmV4cG9ydHMuVG9rZW4gPSBUb2tlbjtcbiIsIlwidXNlIHN0cmljdFwiO1xudmFyIF9fZGVjb3JhdGUgPSAodGhpcyAmJiB0aGlzLl9fZGVjb3JhdGUpIHx8IGZ1bmN0aW9uIChkZWNvcmF0b3JzLCB0YXJnZXQsIGtleSwgZGVzYykge1xuICAgIHZhciBjID0gYXJndW1lbnRzLmxlbmd0aCwgciA9IGMgPCAzID8gdGFyZ2V0IDogZGVzYyA9PT0gbnVsbCA/IGRlc2MgPSBPYmplY3QuZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yKHRhcmdldCwga2V5KSA6IGRlc2MsIGQ7XG4gICAgaWYgKHR5cGVvZiBSZWZsZWN0ID09PSBcIm9iamVjdFwiICYmIHR5cGVvZiBSZWZsZWN0LmRlY29yYXRlID09PSBcImZ1bmN0aW9uXCIpIHIgPSBSZWZsZWN0LmRlY29yYXRlKGRlY29yYXRvcnMsIHRhcmdldCwga2V5LCBkZXNjKTtcbiAgICBlbHNlIGZvciAodmFyIGkgPSBkZWNvcmF0b3JzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSBpZiAoZCA9IGRlY29yYXRvcnNbaV0pIHIgPSAoYyA8IDMgPyBkKHIpIDogYyA+IDMgPyBkKHRhcmdldCwga2V5LCByKSA6IGQodGFyZ2V0LCBrZXkpKSB8fCByO1xuICAgIHJldHVybiBjID4gMyAmJiByICYmIE9iamVjdC5kZWZpbmVQcm9wZXJ0eSh0YXJnZXQsIGtleSwgciksIHI7XG59O1xuZXhwb3J0cy5fX2VzTW9kdWxlID0gdHJ1ZTtcbmV4cG9ydHMuT3JkZXIgPSB2b2lkIDA7XG52YXIgdHN5cmluZ2VfMSA9IHJlcXVpcmUoXCJ0c3lyaW5nZVwiKTtcbnZhciBPcmRlciA9IC8qKiBAY2xhc3MgKi8gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBPcmRlcigpIHtcbiAgICB9XG4gICAgT3JkZXIgPSBfX2RlY29yYXRlKFtcbiAgICAgICAgKDAsIHRzeXJpbmdlXzEuc2luZ2xldG9uKSgpXG4gICAgXSwgT3JkZXIpO1xuICAgIHJldHVybiBPcmRlcjtcbn0oKSk7XG5leHBvcnRzLk9yZGVyID0gT3JkZXI7XG4iXSwic291cmNlUm9vdCI6IiJ9