(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["backend/dashboard"],{

/***/ "./assets/backend/dashboard/css/tools/cargobay.toogle.min.css":
/*!********************************************************************!*\
  !*** ./assets/backend/dashboard/css/tools/cargobay.toogle.min.css ***!
  \********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/backend/dashboard/dashboard.js":
/*!***********************************************!*\
  !*** ./assets/backend/dashboard/dashboard.js ***!
  \***********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var ace_builds__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ace-builds */ "./node_modules/ace-builds/src-noconflict/ace.js");
/* harmony import */ var ace_builds__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(ace_builds__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var ace_builds_webpack_resolver__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ace-builds/webpack-resolver */ "./node_modules/ace-builds/webpack-resolver.js");
/* harmony import */ var ace_builds_webpack_resolver__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(ace_builds_webpack_resolver__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _js_tools_mediaChooser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./js/tools/mediaChooser */ "./assets/backend/dashboard/js/tools/mediaChooser.js");
/* harmony import */ var _js_tools_mediaChooser__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_js_tools_mediaChooser__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var bootstrap_scss_bootstrap_scss__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! bootstrap/scss/bootstrap.scss */ "./node_modules/bootstrap/scss/bootstrap.scss");
/* harmony import */ var bootstrap_scss_bootstrap_scss__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(bootstrap_scss_bootstrap_scss__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _css_tools_cargobay_toogle_min_css__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./css/tools/cargobay.toogle.min.css */ "./assets/backend/dashboard/css/tools/cargobay.toogle.min.css");
/* harmony import */ var _css_tools_cargobay_toogle_min_css__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_css_tools_cargobay_toogle_min_css__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _less_base_less__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./less/base.less */ "./assets/backend/dashboard/less/base.less");
/* harmony import */ var _less_base_less__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(_less_base_less__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var summernote_dist_summernote_bs4_min_css__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! summernote/dist/summernote-bs4.min.css */ "./node_modules/summernote/dist/summernote-bs4.min.css");
/* harmony import */ var summernote_dist_summernote_bs4_min_css__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(summernote_dist_summernote_bs4_min_css__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var bootstrap_toggle_css_bootstrap_toggle_min_css__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! bootstrap-toggle/css/bootstrap-toggle.min.css */ "./node_modules/bootstrap-toggle/css/bootstrap-toggle.min.css");
/* harmony import */ var bootstrap_toggle_css_bootstrap_toggle_min_css__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(bootstrap_toggle_css_bootstrap_toggle_min_css__WEBPACK_IMPORTED_MODULE_7__);
global.$ = global.jQuery = window.jQuery = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");

__webpack_require__(/*! @popperjs/core/dist/cjs/popper */ "./node_modules/@popperjs/core/dist/cjs/popper.js");

__webpack_require__(/*! bootstrap/dist/js/bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");

__webpack_require__(/*! ./js/tether.min */ "./assets/backend/dashboard/js/tether.min.js");

__webpack_require__(/*! ./js/dropzone */ "./assets/backend/dashboard/js/dropzone.js");

global.Raphael = __webpack_require__(/*! raphael/raphael.min */ "./node_modules/raphael/raphael.min.js");

__webpack_require__(/*! morris.js/morris.min */ "./node_modules/morris.js/morris.min.js");

__webpack_require__(/*! summernote/dist/summernote-bs4 */ "./node_modules/summernote/dist/summernote-bs4.js");

__webpack_require__(/*! ./js/summernote/mediabundle.plugin */ "./assets/backend/dashboard/js/summernote/mediabundle.plugin.js");

__webpack_require__(/*! multiselect-two-sides/dist/js/multiselect */ "./node_modules/multiselect-two-sides/dist/js/multiselect.js");

__webpack_require__(/*! bootstrap-toggle/js/bootstrap-toggle.min */ "./node_modules/bootstrap-toggle/js/bootstrap-toggle.min.js");



global.Dropzone = __webpack_require__(/*! ./js/dropzone */ "./assets/backend/dashboard/js/dropzone.js");
global.cargobay = {};
global.cargobay.general = __webpack_require__(/*! ./js/tools/cargobay.general.min */ "./assets/backend/dashboard/js/tools/cargobay.general.min.js");
global.cargobay.toggle = __webpack_require__(/*! ./js/tools/cargobay.toogle.min */ "./assets/backend/dashboard/js/tools/cargobay.toogle.min.js");
global.psc = {};
global.psc.ajaxModal = __webpack_require__(/*! ./js/tools/ajaxModal */ "./assets/backend/dashboard/js/tools/ajaxModal.js");
global.psc.mediaChooser = __webpack_require__(/*! ./js/tools/mediaChooser */ "./assets/backend/dashboard/js/tools/mediaChooser.js");
global.psc.app = __webpack_require__(/*! ./js/app */ "./assets/backend/dashboard/js/app.js");
global.psc.order = __webpack_require__(/*! ./js/order.js */ "./assets/backend/dashboard/js/order.js");
global.psc.app.init();




 //import 'multiselect/css/multi-select.css';


/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./assets/backend/dashboard/js/app.js":
/*!********************************************!*\
  !*** ./assets/backend/dashboard/js/app.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");

!(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
  return {
    // General App init
    init: function init() {
      psc.ajaxModal.init();
      psc.mediaChooser.init();
      psc.order.init();

      if (typeof bootstrapApp === 'function') {
        bootstrapApp();
      }
    }
  };
}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
$(function () {
  $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
    if ($(e.target).parent().parent().parent().attr('id') && $(e.target).parent().parent().parent().attr('id') == 'doc') {
      localStorage.setItem('lastTabDoc', $(this).attr('href'));
    } else {
      localStorage.setItem('lastTab', $(this).attr('href'));
    }

    ;
  }); // go to the latest tab, if it exists:

  var lastTab = localStorage.getItem('lastTab');

  if (lastTab) {
    $('[href="' + lastTab + '"]').tab('show');
  }

  var lastTabDoc = localStorage.getItem('lastTabDoc');

  if (lastTabDoc) {
    $('[href="' + lastTabDoc + '"]').tab('show');
  }

  $('.nav li ul').parent().find('a').click(function () {
    $(this).parent().find("ul").toggle();
  });
  psc.app.init();
  $('input[data-bs-toggle="toggle"], input.ajaxToogle').change(function () {
    $.getJSON($(this).data('url'));
  });
  $('select.multiple').multiselect({
    submitAllLeft: false,
    submitAllRight: true,
    search: {
      left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
      right: '<input type="text" name="q" class="form-control" placeholder="Search..." />'
    },
    fireSearch: function fireSearch(value) {
      return value.length > 1;
    }
  });
  $('.summernote').summernote({
    height: 400,
    toolbar: [['style', ['style']], ['font', ['bold', 'italic', 'underline', 'clear']], ['fontname', ['fontname']], ['color', ['color']], ['para', ['ul', 'ol', 'paragraph']], ['height', ['height']], ['table', ['table']], ['insert', ['link', 'hr']], ['view', ['fullscreen', 'codeview']], ['help', ['help']], ['media', ['media']]]
  });
});

/***/ }),

/***/ "./assets/backend/dashboard/js/dropzone.js":
/*!*************************************************!*\
  !*** ./assets/backend/dashboard/js/dropzone.js ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(module) {__webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.array.splice */ "./node_modules/core-js/modules/es.array.splice.js");

__webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/es.string.match */ "./node_modules/core-js/modules/es.string.match.js");

__webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");

__webpack_require__(/*! core-js/modules/es.string.split */ "./node_modules/core-js/modules/es.string.split.js");

__webpack_require__(/*! core-js/modules/es.string.trim */ "./node_modules/core-js/modules/es.string.trim.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");

__webpack_require__(/*! core-js/modules/web.url */ "./node_modules/core-js/modules/web.url.js");

/*
 *
 * More info at [www.dropzonejs.com](http://www.dropzonejs.com)
 *
 * Copyright (c) 2012, Matias Meno
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
(function () {
  var Dropzone,
      Emitter,
      camelize,
      contentLoaded,
      detectVerticalSquash,
      drawImageIOSFix,
      noop,
      without,
      __slice = [].slice,
      __hasProp = {}.hasOwnProperty,
      __extends = function __extends(child, parent) {
    for (var key in parent) {
      if (__hasProp.call(parent, key)) child[key] = parent[key];
    }

    function ctor() {
      this.constructor = child;
    }

    ctor.prototype = parent.prototype;
    child.prototype = new ctor();
    child.__super__ = parent.prototype;
    return child;
  };

  noop = function noop() {};

  Emitter = function () {
    function Emitter() {}

    Emitter.prototype.addEventListener = Emitter.prototype.on;

    Emitter.prototype.on = function (event, fn) {
      this._callbacks = this._callbacks || {};

      if (!this._callbacks[event]) {
        this._callbacks[event] = [];
      }

      this._callbacks[event].push(fn);

      return this;
    };

    Emitter.prototype.emit = function () {
      var args, callback, callbacks, event, _i, _len;

      event = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      this._callbacks = this._callbacks || {};
      callbacks = this._callbacks[event];

      if (callbacks) {
        for (_i = 0, _len = callbacks.length; _i < _len; _i++) {
          callback = callbacks[_i];
          callback.apply(this, args);
        }
      }

      return this;
    };

    Emitter.prototype.removeListener = Emitter.prototype.off;
    Emitter.prototype.removeAllListeners = Emitter.prototype.off;
    Emitter.prototype.removeEventListener = Emitter.prototype.off;

    Emitter.prototype.off = function (event, fn) {
      var callback, callbacks, i, _i, _len;

      if (!this._callbacks || arguments.length === 0) {
        this._callbacks = {};
        return this;
      }

      callbacks = this._callbacks[event];

      if (!callbacks) {
        return this;
      }

      if (arguments.length === 1) {
        delete this._callbacks[event];
        return this;
      }

      for (i = _i = 0, _len = callbacks.length; _i < _len; i = ++_i) {
        callback = callbacks[i];

        if (callback === fn) {
          callbacks.splice(i, 1);
          break;
        }
      }

      return this;
    };

    return Emitter;
  }();

  Dropzone = function (_super) {
    var extend, resolveOption;

    __extends(Dropzone, _super);

    Dropzone.prototype.Emitter = Emitter;
    /*
    This is a list of all available events you can register on a dropzone object.
    
    You can register an event handler like this:
    
        dropzone.on("dragEnter", function() { });
     */

    Dropzone.prototype.events = ["drop", "dragstart", "dragend", "dragenter", "dragover", "dragleave", "addedfile", "addedfiles", "removedfile", "thumbnail", "error", "errormultiple", "processing", "processingmultiple", "uploadprogress", "totaluploadprogress", "sending", "sendingmultiple", "success", "successmultiple", "canceled", "canceledmultiple", "complete", "completemultiple", "reset", "maxfilesexceeded", "maxfilesreached", "queuecomplete"];
    Dropzone.prototype.defaultOptions = {
      url: null,
      method: "post",
      withCredentials: false,
      parallelUploads: 2,
      uploadMultiple: false,
      maxFilesize: 256,
      paramName: "file",
      createImageThumbnails: true,
      maxThumbnailFilesize: 10,
      thumbnailWidth: 120,
      thumbnailHeight: 120,
      filesizeBase: 1000,
      maxFiles: null,
      params: {},
      clickable: true,
      ignoreHiddenFiles: true,
      acceptedFiles: null,
      acceptedMimeTypes: null,
      autoProcessQueue: true,
      autoQueue: true,
      addRemoveLinks: false,
      previewsContainer: null,
      hiddenInputContainer: "body",
      capture: null,
      dictDefaultMessage: "Drop files here to upload",
      dictFallbackMessage: "Your browser does not support drag'n'drop file uploads.",
      dictFallbackText: "Please use the fallback form below to upload your files like in the olden days.",
      dictFileTooBig: "File is too big ({{filesize}}MiB). Max filesize: {{maxFilesize}}MiB.",
      dictInvalidFileType: "You can't upload files of this type.",
      dictResponseError: "Server responded with {{statusCode}} code.",
      dictCancelUpload: "Cancel upload",
      dictCancelUploadConfirmation: "Are you sure you want to cancel this upload?",
      dictRemoveFile: "Remove file",
      dictRemoveFileConfirmation: null,
      dictMaxFilesExceeded: "You can not upload any more files.",
      accept: function accept(file, done) {
        return done();
      },
      init: function init() {
        return noop;
      },
      forceFallback: false,
      fallback: function fallback() {
        var child, messageElement, span, _i, _len, _ref;

        this.element.className = "" + this.element.className + " dz-browser-not-supported";
        _ref = this.element.getElementsByTagName("div");

        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          child = _ref[_i];

          if (/(^| )dz-message($| )/.test(child.className)) {
            messageElement = child;
            child.className = "dz-message";
            continue;
          }
        }

        if (!messageElement) {
          messageElement = Dropzone.createElement("<div class=\"dz-message\"><span></span></div>");
          this.element.appendChild(messageElement);
        }

        span = messageElement.getElementsByTagName("span")[0];

        if (span) {
          if (span.textContent != null) {
            span.textContent = this.options.dictFallbackMessage;
          } else if (span.innerText != null) {
            span.innerText = this.options.dictFallbackMessage;
          }
        }

        return this.element.appendChild(this.getFallbackForm());
      },
      resize: function resize(file) {
        var info, srcRatio, trgRatio;
        info = {
          srcX: 0,
          srcY: 0,
          srcWidth: file.width,
          srcHeight: file.height
        };
        srcRatio = file.width / file.height;
        info.optWidth = this.options.thumbnailWidth;
        info.optHeight = this.options.thumbnailHeight;

        if (info.optWidth == null && info.optHeight == null) {
          info.optWidth = info.srcWidth;
          info.optHeight = info.srcHeight;
        } else if (info.optWidth == null) {
          info.optWidth = srcRatio * info.optHeight;
        } else if (info.optHeight == null) {
          info.optHeight = 1 / srcRatio * info.optWidth;
        }

        trgRatio = info.optWidth / info.optHeight;

        if (file.height < info.optHeight || file.width < info.optWidth) {
          info.trgHeight = info.srcHeight;
          info.trgWidth = info.srcWidth;
        } else {
          if (srcRatio > trgRatio) {
            info.srcHeight = file.height;
            info.srcWidth = info.srcHeight * trgRatio;
          } else {
            info.srcWidth = file.width;
            info.srcHeight = info.srcWidth / trgRatio;
          }
        }

        info.srcX = (file.width - info.srcWidth) / 2;
        info.srcY = (file.height - info.srcHeight) / 2;
        return info;
      },

      /*
      Those functions register themselves to the events on init and handle all
      the user interface specific stuff. Overwriting them won't break the upload
      but can break the way it's displayed.
      You can overwrite them if you don't like the default behavior. If you just
      want to add an additional event handler, register it on the dropzone object
      and don't overwrite those options.
       */
      drop: function drop(e) {
        return this.element.classList.remove("dz-drag-hover");
      },
      dragstart: noop,
      dragend: function dragend(e) {
        return this.element.classList.remove("dz-drag-hover");
      },
      dragenter: function dragenter(e) {
        return this.element.classList.add("dz-drag-hover");
      },
      dragover: function dragover(e) {
        return this.element.classList.add("dz-drag-hover");
      },
      dragleave: function dragleave(e) {
        return this.element.classList.remove("dz-drag-hover");
      },
      paste: noop,
      reset: function reset() {
        return this.element.classList.remove("dz-started");
      },
      addedfile: function addedfile(file) {
        var node, removeFileEvent, removeLink, _i, _j, _k, _len, _len1, _len2, _ref, _ref1, _ref2, _results;

        if (this.element === this.previewsContainer) {
          this.element.classList.add("dz-started");
        }

        if (this.previewsContainer) {
          file.previewElement = Dropzone.createElement(this.options.previewTemplate.trim());
          file.previewTemplate = file.previewElement;
          this.previewsContainer.appendChild(file.previewElement);
          _ref = file.previewElement.querySelectorAll("[data-dz-name]");

          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];
            node.textContent = file.name;
          }

          _ref1 = file.previewElement.querySelectorAll("[data-dz-size]");

          for (_j = 0, _len1 = _ref1.length; _j < _len1; _j++) {
            node = _ref1[_j];
            node.innerHTML = this.filesize(file.size);
          }

          if (this.options.addRemoveLinks) {
            file._removeLink = Dropzone.createElement("<a class=\"dz-remove\" href=\"javascript:undefined;\" data-dz-remove>" + this.options.dictRemoveFile + "</a>");
            file.previewElement.appendChild(file._removeLink);
          }

          removeFileEvent = function (_this) {
            return function (e) {
              e.preventDefault();
              e.stopPropagation();

              if (file.status === Dropzone.UPLOADING) {
                return Dropzone.confirm(_this.options.dictCancelUploadConfirmation, function () {
                  return _this.removeFile(file);
                });
              } else {
                if (_this.options.dictRemoveFileConfirmation) {
                  return Dropzone.confirm(_this.options.dictRemoveFileConfirmation, function () {
                    return _this.removeFile(file);
                  });
                } else {
                  return _this.removeFile(file);
                }
              }
            };
          }(this);

          _ref2 = file.previewElement.querySelectorAll("[data-dz-remove]");
          _results = [];

          for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
            removeLink = _ref2[_k];

            _results.push(removeLink.addEventListener("click", removeFileEvent));
          }

          return _results;
        }
      },
      removedfile: function removedfile(file) {
        var _ref;

        if (file.previewElement) {
          if ((_ref = file.previewElement) != null) {
            _ref.parentNode.removeChild(file.previewElement);
          }
        }

        return this._updateMaxFilesReachedClass();
      },
      thumbnail: function thumbnail(file, dataUrl) {
        var thumbnailElement, _i, _len, _ref;

        if (file.previewElement) {
          file.previewElement.classList.remove("dz-file-preview");
          _ref = file.previewElement.querySelectorAll("[data-dz-thumbnail]");

          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            thumbnailElement = _ref[_i];
            thumbnailElement.alt = file.name;
            thumbnailElement.src = dataUrl;
          }

          return setTimeout(function (_this) {
            return function () {
              return file.previewElement.classList.add("dz-image-preview");
            };
          }(this), 1);
        }
      },
      error: function error(file, message) {
        var node, _i, _len, _ref, _results;

        if (file.previewElement) {
          file.previewElement.classList.add("dz-error");

          if (typeof message !== "String" && message.error) {
            message = message.error;
          }

          _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
          _results = [];

          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];

            _results.push(node.textContent = message);
          }

          return _results;
        }
      },
      errormultiple: noop,
      processing: function processing(file) {
        if (file.previewElement) {
          file.previewElement.classList.add("dz-processing");

          if (file._removeLink) {
            return file._removeLink.textContent = this.options.dictCancelUpload;
          }
        }
      },
      processingmultiple: noop,
      uploadprogress: function uploadprogress(file, progress, bytesSent) {
        var node, _i, _len, _ref, _results;

        if (file.previewElement) {
          _ref = file.previewElement.querySelectorAll("[data-dz-uploadprogress]");
          _results = [];

          for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];

            if (node.nodeName === 'PROGRESS') {
              _results.push(node.value = progress);
            } else {
              _results.push(node.style.width = "" + progress + "%");
            }
          }

          return _results;
        }
      },
      totaluploadprogress: noop,
      sending: noop,
      sendingmultiple: noop,
      success: function success(file) {
        if (file.previewElement) {
          return file.previewElement.classList.add("dz-success");
        }
      },
      successmultiple: noop,
      canceled: function canceled(file) {
        return this.emit("error", file, "Upload canceled.");
      },
      canceledmultiple: noop,
      complete: function complete(file) {
        if (file._removeLink) {
          file._removeLink.textContent = this.options.dictRemoveFile;
        }

        if (file.previewElement) {
          return file.previewElement.classList.add("dz-complete");
        }
      },
      completemultiple: noop,
      maxfilesexceeded: noop,
      maxfilesreached: noop,
      queuecomplete: noop,
      addedfiles: noop,
      previewTemplate: "<div class=\"dz-preview dz-file-preview\">\n  <div class=\"dz-image\"><img data-dz-thumbnail /></div>\n  <div class=\"dz-details\">\n    <div class=\"dz-size\"><span data-dz-size></span></div>\n    <div class=\"dz-filename\"><span data-dz-name></span></div>\n  </div>\n  <div class=\"dz-progress\"><span class=\"dz-upload\" data-dz-uploadprogress></span></div>\n  <div class=\"dz-error-message\"><span data-dz-errormessage></span></div>\n  <div class=\"dz-success-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Check</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <path d=\"M23.5,31.8431458 L17.5852419,25.9283877 C16.0248253,24.3679711 13.4910294,24.366835 11.9289322,25.9289322 C10.3700136,27.4878508 10.3665912,30.0234455 11.9283877,31.5852419 L20.4147581,40.0716123 C20.5133999,40.1702541 20.6159315,40.2626649 20.7218615,40.3488435 C22.2835669,41.8725651 24.794234,41.8626202 26.3461564,40.3106978 L43.3106978,23.3461564 C44.8771021,21.7797521 44.8758057,19.2483887 43.3137085,17.6862915 C41.7547899,16.1273729 39.2176035,16.1255422 37.6538436,17.6893022 L23.5,31.8431458 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" stroke-opacity=\"0.198794158\" stroke=\"#747474\" fill-opacity=\"0.816519475\" fill=\"#FFFFFF\" sketch:type=\"MSShapeGroup\"></path>\n      </g>\n    </svg>\n  </div>\n  <div class=\"dz-error-mark\">\n    <svg width=\"54px\" height=\"54px\" viewBox=\"0 0 54 54\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns:sketch=\"http://www.bohemiancoding.com/sketch/ns\">\n      <title>Error</title>\n      <defs></defs>\n      <g id=\"Page-1\" stroke=\"none\" stroke-width=\"1\" fill=\"none\" fill-rule=\"evenodd\" sketch:type=\"MSPage\">\n        <g id=\"Check-+-Oval-2\" sketch:type=\"MSLayerGroup\" stroke=\"#747474\" stroke-opacity=\"0.198794158\" fill=\"#FFFFFF\" fill-opacity=\"0.816519475\">\n          <path d=\"M32.6568542,29 L38.3106978,23.3461564 C39.8771021,21.7797521 39.8758057,19.2483887 38.3137085,17.6862915 C36.7547899,16.1273729 34.2176035,16.1255422 32.6538436,17.6893022 L27,23.3431458 L21.3461564,17.6893022 C19.7823965,16.1255422 17.2452101,16.1273729 15.6862915,17.6862915 C14.1241943,19.2483887 14.1228979,21.7797521 15.6893022,23.3461564 L21.3431458,29 L15.6893022,34.6538436 C14.1228979,36.2202479 14.1241943,38.7516113 15.6862915,40.3137085 C17.2452101,41.8726271 19.7823965,41.8744578 21.3461564,40.3106978 L27,34.6568542 L32.6538436,40.3106978 C34.2176035,41.8744578 36.7547899,41.8726271 38.3137085,40.3137085 C39.8758057,38.7516113 39.8771021,36.2202479 38.3106978,34.6538436 L32.6568542,29 Z M27,53 C41.3594035,53 53,41.3594035 53,27 C53,12.6405965 41.3594035,1 27,1 C12.6405965,1 1,12.6405965 1,27 C1,41.3594035 12.6405965,53 27,53 Z\" id=\"Oval-2\" sketch:type=\"MSShapeGroup\"></path>\n        </g>\n      </g>\n    </svg>\n  </div>\n</div>"
    };

    extend = function extend() {
      var key, object, objects, target, val, _i, _len;

      target = arguments[0], objects = 2 <= arguments.length ? __slice.call(arguments, 1) : [];

      for (_i = 0, _len = objects.length; _i < _len; _i++) {
        object = objects[_i];

        for (key in object) {
          val = object[key];
          target[key] = val;
        }
      }

      return target;
    };

    function Dropzone(element, options) {
      var elementOptions, fallback, _ref;

      this.element = element;
      this.version = Dropzone.version;
      this.defaultOptions.previewTemplate = this.defaultOptions.previewTemplate.replace(/\n*/g, "");
      this.clickableElements = [];
      this.listeners = [];
      this.files = [];

      if (typeof this.element === "string") {
        this.element = document.querySelector(this.element);
      }

      if (!(this.element && this.element.nodeType != null)) {
        throw new Error("Invalid dropzone element.");
      }

      if (this.element.dropzone) {
        throw new Error("Dropzone already attached.");
      }

      Dropzone.instances.push(this);
      this.element.dropzone = this;
      elementOptions = (_ref = Dropzone.optionsForElement(this.element)) != null ? _ref : {};
      this.options = extend({}, this.defaultOptions, elementOptions, options != null ? options : {});

      if (this.options.forceFallback || !Dropzone.isBrowserSupported()) {
        return this.options.fallback.call(this);
      }

      if (this.options.url == null) {
        this.options.url = this.element.getAttribute("action");
      }

      if (!this.options.url) {
        throw new Error("No URL provided.");
      }

      if (this.options.acceptedFiles && this.options.acceptedMimeTypes) {
        throw new Error("You can't provide both 'acceptedFiles' and 'acceptedMimeTypes'. 'acceptedMimeTypes' is deprecated.");
      }

      if (this.options.acceptedMimeTypes) {
        this.options.acceptedFiles = this.options.acceptedMimeTypes;
        delete this.options.acceptedMimeTypes;
      }

      this.options.method = this.options.method.toUpperCase();

      if ((fallback = this.getExistingFallback()) && fallback.parentNode) {
        fallback.parentNode.removeChild(fallback);
      }

      if (this.options.previewsContainer !== false) {
        if (this.options.previewsContainer) {
          this.previewsContainer = Dropzone.getElement(this.options.previewsContainer, "previewsContainer");
        } else {
          this.previewsContainer = this.element;
        }
      }

      if (this.options.clickable) {
        if (this.options.clickable === true) {
          this.clickableElements = [this.element];
        } else {
          this.clickableElements = Dropzone.getElements(this.options.clickable, "clickable");
        }
      }

      this.init();
    }

    Dropzone.prototype.getAcceptedFiles = function () {
      var file, _i, _len, _ref, _results;

      _ref = this.files;
      _results = [];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];

        if (file.accepted) {
          _results.push(file);
        }
      }

      return _results;
    };

    Dropzone.prototype.getRejectedFiles = function () {
      var file, _i, _len, _ref, _results;

      _ref = this.files;
      _results = [];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];

        if (!file.accepted) {
          _results.push(file);
        }
      }

      return _results;
    };

    Dropzone.prototype.getFilesWithStatus = function (status) {
      var file, _i, _len, _ref, _results;

      _ref = this.files;
      _results = [];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];

        if (file.status === status) {
          _results.push(file);
        }
      }

      return _results;
    };

    Dropzone.prototype.getQueuedFiles = function () {
      return this.getFilesWithStatus(Dropzone.QUEUED);
    };

    Dropzone.prototype.getUploadingFiles = function () {
      return this.getFilesWithStatus(Dropzone.UPLOADING);
    };

    Dropzone.prototype.getAddedFiles = function () {
      return this.getFilesWithStatus(Dropzone.ADDED);
    };

    Dropzone.prototype.getActiveFiles = function () {
      var file, _i, _len, _ref, _results;

      _ref = this.files;
      _results = [];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];

        if (file.status === Dropzone.UPLOADING || file.status === Dropzone.QUEUED) {
          _results.push(file);
        }
      }

      return _results;
    };

    Dropzone.prototype.init = function () {
      var eventName, noPropagation, setupHiddenFileInput, _i, _len, _ref, _ref1;

      if (this.element.tagName === "form") {
        this.element.setAttribute("enctype", "multipart/form-data");
      }

      if (this.element.classList.contains("dropzone") && !this.element.querySelector(".dz-message")) {
        this.element.appendChild(Dropzone.createElement("<div class=\"dz-default dz-message\"><span>" + this.options.dictDefaultMessage + "</span></div>"));
      }

      if (this.clickableElements.length) {
        setupHiddenFileInput = function (_this) {
          return function () {
            if (_this.hiddenFileInput) {
              _this.hiddenFileInput.parentNode.removeChild(_this.hiddenFileInput);
            }

            _this.hiddenFileInput = document.createElement("input");

            _this.hiddenFileInput.setAttribute("type", "file");

            if (_this.options.maxFiles == null || _this.options.maxFiles > 1) {
              _this.hiddenFileInput.setAttribute("multiple", "multiple");
            }

            _this.hiddenFileInput.className = "dz-hidden-input";

            if (_this.options.acceptedFiles != null) {
              _this.hiddenFileInput.setAttribute("accept", _this.options.acceptedFiles);
            }

            if (_this.options.capture != null) {
              _this.hiddenFileInput.setAttribute("capture", _this.options.capture);
            }

            _this.hiddenFileInput.style.visibility = "hidden";
            _this.hiddenFileInput.style.position = "absolute";
            _this.hiddenFileInput.style.top = "0";
            _this.hiddenFileInput.id = "upload";
            _this.hiddenFileInput.style.left = "0";
            _this.hiddenFileInput.style.height = "0";
            _this.hiddenFileInput.style.width = "0";
            document.querySelector(_this.options.hiddenInputContainer).appendChild(_this.hiddenFileInput);
            return _this.hiddenFileInput.addEventListener("change", function () {
              var file, files, _i, _len;

              files = _this.hiddenFileInput.files;

              if (files.length) {
                for (_i = 0, _len = files.length; _i < _len; _i++) {
                  file = files[_i];

                  _this.addFile(file);
                }
              }

              _this.emit("addedfiles", files);

              return setupHiddenFileInput();
            });
          };
        }(this);

        setupHiddenFileInput();
      }

      this.URL = (_ref = window.URL) != null ? _ref : window.webkitURL;
      _ref1 = this.events;

      for (_i = 0, _len = _ref1.length; _i < _len; _i++) {
        eventName = _ref1[_i];
        this.on(eventName, this.options[eventName]);
      }

      this.on("uploadprogress", function (_this) {
        return function () {
          return _this.updateTotalUploadProgress();
        };
      }(this));
      this.on("removedfile", function (_this) {
        return function () {
          return _this.updateTotalUploadProgress();
        };
      }(this));
      this.on("canceled", function (_this) {
        return function (file) {
          return _this.emit("complete", file);
        };
      }(this));
      this.on("complete", function (_this) {
        return function (file) {
          if (_this.getAddedFiles().length === 0 && _this.getUploadingFiles().length === 0 && _this.getQueuedFiles().length === 0) {
            return setTimeout(function () {
              return _this.emit("queuecomplete");
            }, 0);
          }
        };
      }(this));

      noPropagation = function noPropagation(e) {
        e.stopPropagation();

        if (e.preventDefault) {
          return e.preventDefault();
        } else {
          return e.returnValue = false;
        }
      };

      this.listeners = [{
        element: this.element,
        events: {
          "dragstart": function (_this) {
            return function (e) {
              return _this.emit("dragstart", e);
            };
          }(this),
          "dragenter": function (_this) {
            return function (e) {
              noPropagation(e);
              return _this.emit("dragenter", e);
            };
          }(this),
          "dragover": function (_this) {
            return function (e) {
              var efct;

              try {
                efct = e.dataTransfer.effectAllowed;
              } catch (_error) {}

              e.dataTransfer.dropEffect = 'move' === efct || 'linkMove' === efct ? 'move' : 'copy';
              noPropagation(e);
              return _this.emit("dragover", e);
            };
          }(this),
          "dragleave": function (_this) {
            return function (e) {
              return _this.emit("dragleave", e);
            };
          }(this),
          "drop": function (_this) {
            return function (e) {
              noPropagation(e);
              return _this.drop(e);
            };
          }(this),
          "dragend": function (_this) {
            return function (e) {
              return _this.emit("dragend", e);
            };
          }(this)
        }
      }];
      this.clickableElements.forEach(function (_this) {
        return function (clickableElement) {
          return _this.listeners.push({
            element: clickableElement,
            events: {
              "click": function click(evt) {
                if (clickableElement !== _this.element || evt.target === _this.element || Dropzone.elementInside(evt.target, _this.element.querySelector(".dz-message"))) {
                  _this.hiddenFileInput.click();
                }

                return true;
              }
            }
          });
        };
      }(this));
      this.enable();
      return this.options.init.call(this);
    };

    Dropzone.prototype.destroy = function () {
      var _ref;

      this.disable();
      this.removeAllFiles(true);

      if ((_ref = this.hiddenFileInput) != null ? _ref.parentNode : void 0) {
        this.hiddenFileInput.parentNode.removeChild(this.hiddenFileInput);
        this.hiddenFileInput = null;
      }

      delete this.element.dropzone;
      return Dropzone.instances.splice(Dropzone.instances.indexOf(this), 1);
    };

    Dropzone.prototype.updateTotalUploadProgress = function () {
      var activeFiles, file, totalBytes, totalBytesSent, totalUploadProgress, _i, _len, _ref;

      totalBytesSent = 0;
      totalBytes = 0;
      activeFiles = this.getActiveFiles();

      if (activeFiles.length) {
        _ref = this.getActiveFiles();

        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          file = _ref[_i];
          totalBytesSent += file.upload.bytesSent;
          totalBytes += file.upload.total;
        }

        totalUploadProgress = 100 * totalBytesSent / totalBytes;
      } else {
        totalUploadProgress = 100;
      }

      return this.emit("totaluploadprogress", totalUploadProgress, totalBytes, totalBytesSent);
    };

    Dropzone.prototype._getParamName = function (n) {
      if (typeof this.options.paramName === "function") {
        return this.options.paramName(n);
      } else {
        return "" + this.options.paramName + (this.options.uploadMultiple ? "[" + n + "]" : "");
      }
    };

    Dropzone.prototype.getFallbackForm = function () {
      var existingFallback, fields, fieldsString, form;

      if (existingFallback = this.getExistingFallback()) {
        return existingFallback;
      }

      fieldsString = "<div class=\"dz-fallback\">";

      if (this.options.dictFallbackText) {
        fieldsString += "<p>" + this.options.dictFallbackText + "</p>";
      }

      fieldsString += "<input type=\"file\" name=\"" + this._getParamName(0) + "\" " + (this.options.uploadMultiple ? 'multiple="multiple"' : void 0) + " /><input type=\"submit\" value=\"Upload!\"></div>";
      fields = Dropzone.createElement(fieldsString);

      if (this.element.tagName !== "FORM") {
        form = Dropzone.createElement("<form action=\"" + this.options.url + "\" enctype=\"multipart/form-data\" method=\"" + this.options.method + "\"></form>");
        form.appendChild(fields);
      } else {
        this.element.setAttribute("enctype", "multipart/form-data");
        this.element.setAttribute("method", this.options.method);
      }

      return form != null ? form : fields;
    };

    Dropzone.prototype.getExistingFallback = function () {
      var fallback, getFallback, tagName, _i, _len, _ref;

      getFallback = function getFallback(elements) {
        var el, _i, _len;

        for (_i = 0, _len = elements.length; _i < _len; _i++) {
          el = elements[_i];

          if (/(^| )fallback($| )/.test(el.className)) {
            return el;
          }
        }
      };

      _ref = ["div", "form"];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        tagName = _ref[_i];

        if (fallback = getFallback(this.element.getElementsByTagName(tagName))) {
          return fallback;
        }
      }
    };

    Dropzone.prototype.setupEventListeners = function () {
      var elementListeners, event, listener, _i, _len, _ref, _results;

      _ref = this.listeners;
      _results = [];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        elementListeners = _ref[_i];

        _results.push(function () {
          var _ref1, _results1;

          _ref1 = elementListeners.events;
          _results1 = [];

          for (event in _ref1) {
            listener = _ref1[event];

            _results1.push(elementListeners.element.addEventListener(event, listener, false));
          }

          return _results1;
        }());
      }

      return _results;
    };

    Dropzone.prototype.removeEventListeners = function () {
      var elementListeners, event, listener, _i, _len, _ref, _results;

      _ref = this.listeners;
      _results = [];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        elementListeners = _ref[_i];

        _results.push(function () {
          var _ref1, _results1;

          _ref1 = elementListeners.events;
          _results1 = [];

          for (event in _ref1) {
            listener = _ref1[event];

            _results1.push(elementListeners.element.removeEventListener(event, listener, false));
          }

          return _results1;
        }());
      }

      return _results;
    };

    Dropzone.prototype.disable = function () {
      var file, _i, _len, _ref, _results;

      this.clickableElements.forEach(function (element) {
        return element.classList.remove("dz-clickable");
      });
      this.removeEventListeners();
      _ref = this.files;
      _results = [];

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];

        _results.push(this.cancelUpload(file));
      }

      return _results;
    };

    Dropzone.prototype.enable = function () {
      this.clickableElements.forEach(function (element) {
        return element.classList.add("dz-clickable");
      });
      return this.setupEventListeners();
    };

    Dropzone.prototype.filesize = function (size) {
      var cutoff, i, selectedSize, selectedUnit, unit, units, _i, _len;

      selectedSize = 0;
      selectedUnit = "b";

      if (size > 0) {
        units = ['TB', 'GB', 'MB', 'KB', 'b'];

        for (i = _i = 0, _len = units.length; _i < _len; i = ++_i) {
          unit = units[i];
          cutoff = Math.pow(this.options.filesizeBase, 4 - i) / 10;

          if (size >= cutoff) {
            selectedSize = size / Math.pow(this.options.filesizeBase, 4 - i);
            selectedUnit = unit;
            break;
          }
        }

        selectedSize = Math.round(10 * selectedSize) / 10;
      }

      return "<strong>" + selectedSize + "</strong> " + selectedUnit;
    };

    Dropzone.prototype._updateMaxFilesReachedClass = function () {
      if (this.options.maxFiles != null && this.getAcceptedFiles().length >= this.options.maxFiles) {
        if (this.getAcceptedFiles().length === this.options.maxFiles) {
          this.emit('maxfilesreached', this.files);
        }

        return this.element.classList.add("dz-max-files-reached");
      } else {
        return this.element.classList.remove("dz-max-files-reached");
      }
    };

    Dropzone.prototype.drop = function (e) {
      var files, items;

      if (!e.dataTransfer) {
        return;
      }

      this.emit("drop", e);
      files = e.dataTransfer.files;
      this.emit("addedfiles", files);

      if (files.length) {
        items = e.dataTransfer.items;

        if (items && items.length && items[0].webkitGetAsEntry != null) {
          this._addFilesFromItems(items);
        } else {
          this.handleFiles(files);
        }
      }
    };

    Dropzone.prototype.paste = function (e) {
      var items, _ref;

      if ((e != null ? (_ref = e.clipboardData) != null ? _ref.items : void 0 : void 0) == null) {
        return;
      }

      this.emit("paste", e);
      items = e.clipboardData.items;

      if (items.length) {
        return this._addFilesFromItems(items);
      }
    };

    Dropzone.prototype.handleFiles = function (files) {
      var file, _i, _len, _results;

      _results = [];

      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];

        _results.push(this.addFile(file));
      }

      return _results;
    };

    Dropzone.prototype._addFilesFromItems = function (items) {
      var entry, item, _i, _len, _results;

      _results = [];

      for (_i = 0, _len = items.length; _i < _len; _i++) {
        item = items[_i];

        if (item.webkitGetAsEntry != null && (entry = item.webkitGetAsEntry())) {
          if (entry.isFile) {
            _results.push(this.addFile(item.getAsFile()));
          } else if (entry.isDirectory) {
            _results.push(this._addFilesFromDirectory(entry, entry.name));
          } else {
            _results.push(void 0);
          }
        } else if (item.getAsFile != null) {
          if (item.kind == null || item.kind === "file") {
            _results.push(this.addFile(item.getAsFile()));
          } else {
            _results.push(void 0);
          }
        } else {
          _results.push(void 0);
        }
      }

      return _results;
    };

    Dropzone.prototype._addFilesFromDirectory = function (directory, path) {
      var dirReader, entriesReader;
      dirReader = directory.createReader();

      entriesReader = function (_this) {
        return function (entries) {
          var entry, _i, _len;

          for (_i = 0, _len = entries.length; _i < _len; _i++) {
            entry = entries[_i];

            if (entry.isFile) {
              entry.file(function (file) {
                if (_this.options.ignoreHiddenFiles && file.name.substring(0, 1) === '.') {
                  return;
                }

                file.fullPath = "" + path + "/" + file.name;
                return _this.addFile(file);
              });
            } else if (entry.isDirectory) {
              _this._addFilesFromDirectory(entry, "" + path + "/" + entry.name);
            }
          }
        };
      }(this);

      return dirReader.readEntries(entriesReader, function (error) {
        return typeof console !== "undefined" && console !== null ? typeof console.log === "function" ? console.log(error) : void 0 : void 0;
      });
    };

    Dropzone.prototype.accept = function (file, done) {
      if (file.size > this.options.maxFilesize * 1024 * 1024) {
        return done(this.options.dictFileTooBig.replace("{{filesize}}", Math.round(file.size / 1024 / 10.24) / 100).replace("{{maxFilesize}}", this.options.maxFilesize));
      } else if (!Dropzone.isValidFile(file, this.options.acceptedFiles)) {
        return done(this.options.dictInvalidFileType);
      } else if (this.options.maxFiles != null && this.getAcceptedFiles().length >= this.options.maxFiles) {
        done(this.options.dictMaxFilesExceeded.replace("{{maxFiles}}", this.options.maxFiles));
        return this.emit("maxfilesexceeded", file);
      } else {
        return this.options.accept.call(this, file, done);
      }
    };

    Dropzone.prototype.addFile = function (file) {
      file.upload = {
        progress: 0,
        total: file.size,
        bytesSent: 0
      };
      this.files.push(file);
      file.status = Dropzone.ADDED;
      this.emit("addedfile", file);

      this._enqueueThumbnail(file);

      return this.accept(file, function (_this) {
        return function (error) {
          if (error) {
            file.accepted = false;

            _this._errorProcessing([file], error);
          } else {
            file.accepted = true;

            if (_this.options.autoQueue) {
              _this.enqueueFile(file);
            }
          }

          return _this._updateMaxFilesReachedClass();
        };
      }(this));
    };

    Dropzone.prototype.enqueueFiles = function (files) {
      var file, _i, _len;

      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        this.enqueueFile(file);
      }

      return null;
    };

    Dropzone.prototype.enqueueFile = function (file) {
      if (file.status === Dropzone.ADDED && file.accepted === true) {
        file.status = Dropzone.QUEUED;

        if (this.options.autoProcessQueue) {
          return setTimeout(function (_this) {
            return function () {
              return _this.processQueue();
            };
          }(this), 0);
        }
      } else {
        throw new Error("This file can't be queued because it has already been processed or was rejected.");
      }
    };

    Dropzone.prototype._thumbnailQueue = [];
    Dropzone.prototype._processingThumbnail = false;

    Dropzone.prototype._enqueueThumbnail = function (file) {
      if (this.options.createImageThumbnails && file.type.match(/image.*/) && file.size <= this.options.maxThumbnailFilesize * 1024 * 1024) {
        this._thumbnailQueue.push(file);

        return setTimeout(function (_this) {
          return function () {
            return _this._processThumbnailQueue();
          };
        }(this), 0);
      }
    };

    Dropzone.prototype._processThumbnailQueue = function () {
      if (this._processingThumbnail || this._thumbnailQueue.length === 0) {
        return;
      }

      this._processingThumbnail = true;
      return this.createThumbnail(this._thumbnailQueue.shift(), function (_this) {
        return function () {
          _this._processingThumbnail = false;
          return _this._processThumbnailQueue();
        };
      }(this));
    };

    Dropzone.prototype.removeFile = function (file) {
      if (file.status === Dropzone.UPLOADING) {
        this.cancelUpload(file);
      }

      this.files = without(this.files, file);
      this.emit("removedfile", file);

      if (this.files.length === 0) {
        return this.emit("reset");
      }
    };

    Dropzone.prototype.removeAllFiles = function (cancelIfNecessary) {
      var file, _i, _len, _ref;

      if (cancelIfNecessary == null) {
        cancelIfNecessary = false;
      }

      _ref = this.files.slice();

      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        file = _ref[_i];

        if (file.status !== Dropzone.UPLOADING || cancelIfNecessary) {
          this.removeFile(file);
        }
      }

      return null;
    };

    Dropzone.prototype.createThumbnail = function (file, callback) {
      var fileReader;
      fileReader = new FileReader();

      fileReader.onload = function (_this) {
        return function () {
          if (file.type === "image/svg+xml") {
            _this.emit("thumbnail", file, fileReader.result);

            if (callback != null) {
              callback();
            }

            return;
          }

          return _this.createThumbnailFromUrl(file, fileReader.result, callback);
        };
      }(this);

      return fileReader.readAsDataURL(file);
    };

    Dropzone.prototype.createThumbnailFromUrl = function (file, imageUrl, callback, crossOrigin) {
      var img;
      img = document.createElement("img");

      if (crossOrigin) {
        img.crossOrigin = crossOrigin;
      }

      img.onload = function (_this) {
        return function () {
          var canvas, ctx, resizeInfo, thumbnail, _ref, _ref1, _ref2, _ref3;

          file.width = img.width;
          file.height = img.height;
          resizeInfo = _this.options.resize.call(_this, file);

          if (resizeInfo.trgWidth == null) {
            resizeInfo.trgWidth = resizeInfo.optWidth;
          }

          if (resizeInfo.trgHeight == null) {
            resizeInfo.trgHeight = resizeInfo.optHeight;
          }

          canvas = document.createElement("canvas");
          ctx = canvas.getContext("2d");
          canvas.width = resizeInfo.trgWidth;
          canvas.height = resizeInfo.trgHeight;
          drawImageIOSFix(ctx, img, (_ref = resizeInfo.srcX) != null ? _ref : 0, (_ref1 = resizeInfo.srcY) != null ? _ref1 : 0, resizeInfo.srcWidth, resizeInfo.srcHeight, (_ref2 = resizeInfo.trgX) != null ? _ref2 : 0, (_ref3 = resizeInfo.trgY) != null ? _ref3 : 0, resizeInfo.trgWidth, resizeInfo.trgHeight);
          thumbnail = canvas.toDataURL("image/png");

          _this.emit("thumbnail", file, thumbnail);

          if (callback != null) {
            return callback();
          }
        };
      }(this);

      if (callback != null) {
        img.onerror = callback;
      }

      return img.src = imageUrl;
    };

    Dropzone.prototype.processQueue = function () {
      var i, parallelUploads, processingLength, queuedFiles;
      parallelUploads = this.options.parallelUploads;
      processingLength = this.getUploadingFiles().length;
      i = processingLength;

      if (processingLength >= parallelUploads) {
        return;
      }

      queuedFiles = this.getQueuedFiles();

      if (!(queuedFiles.length > 0)) {
        return;
      }

      if (this.options.uploadMultiple) {
        return this.processFiles(queuedFiles.slice(0, parallelUploads - processingLength));
      } else {
        while (i < parallelUploads) {
          if (!queuedFiles.length) {
            return;
          }

          this.processFile(queuedFiles.shift());
          i++;
        }
      }
    };

    Dropzone.prototype.processFile = function (file) {
      return this.processFiles([file]);
    };

    Dropzone.prototype.processFiles = function (files) {
      var file, _i, _len;

      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.processing = true;
        file.status = Dropzone.UPLOADING;
        this.emit("processing", file);
      }

      if (this.options.uploadMultiple) {
        this.emit("processingmultiple", files);
      }

      return this.uploadFiles(files);
    };

    Dropzone.prototype._getFilesWithXhr = function (xhr) {
      var file, files;
      return files = function () {
        var _i, _len, _ref, _results;

        _ref = this.files;
        _results = [];

        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          file = _ref[_i];

          if (file.xhr === xhr) {
            _results.push(file);
          }
        }

        return _results;
      }.call(this);
    };

    Dropzone.prototype.cancelUpload = function (file) {
      var groupedFile, groupedFiles, _i, _j, _len, _len1, _ref;

      if (file.status === Dropzone.UPLOADING) {
        groupedFiles = this._getFilesWithXhr(file.xhr);

        for (_i = 0, _len = groupedFiles.length; _i < _len; _i++) {
          groupedFile = groupedFiles[_i];
          groupedFile.status = Dropzone.CANCELED;
        }

        file.xhr.abort();

        for (_j = 0, _len1 = groupedFiles.length; _j < _len1; _j++) {
          groupedFile = groupedFiles[_j];
          this.emit("canceled", groupedFile);
        }

        if (this.options.uploadMultiple) {
          this.emit("canceledmultiple", groupedFiles);
        }
      } else if ((_ref = file.status) === Dropzone.ADDED || _ref === Dropzone.QUEUED) {
        file.status = Dropzone.CANCELED;
        this.emit("canceled", file);

        if (this.options.uploadMultiple) {
          this.emit("canceledmultiple", [file]);
        }
      }

      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    };

    resolveOption = function resolveOption() {
      var args, option;
      option = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];

      if (typeof option === 'function') {
        return option.apply(this, args);
      }

      return option;
    };

    Dropzone.prototype.uploadFile = function (file) {
      return this.uploadFiles([file]);
    };

    Dropzone.prototype.uploadFiles = function (files) {
      var file, formData, handleError, headerName, headerValue, headers, i, input, inputName, inputType, key, method, option, progressObj, response, updateProgress, url, value, xhr, _i, _j, _k, _l, _len, _len1, _len2, _len3, _m, _ref, _ref1, _ref2, _ref3, _ref4, _ref5;

      xhr = new XMLHttpRequest();

      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.xhr = xhr;
      }

      method = resolveOption(this.options.method, files);
      url = resolveOption(this.options.url, files);
      xhr.open(method, url, true);
      xhr.withCredentials = !!this.options.withCredentials;
      response = null;

      handleError = function (_this) {
        return function () {
          var _j, _len1, _results;

          _results = [];

          for (_j = 0, _len1 = files.length; _j < _len1; _j++) {
            file = files[_j];

            _results.push(_this._errorProcessing(files, response || _this.options.dictResponseError.replace("{{statusCode}}", xhr.status), xhr));
          }

          return _results;
        };
      }(this);

      updateProgress = function (_this) {
        return function (e) {
          var allFilesFinished, progress, _j, _k, _l, _len1, _len2, _len3, _results;

          if (e != null) {
            progress = 100 * e.loaded / e.total;

            for (_j = 0, _len1 = files.length; _j < _len1; _j++) {
              file = files[_j];
              file.upload = {
                progress: progress,
                total: e.total,
                bytesSent: e.loaded
              };
            }
          } else {
            allFilesFinished = true;
            progress = 100;

            for (_k = 0, _len2 = files.length; _k < _len2; _k++) {
              file = files[_k];

              if (!(file.upload.progress === 100 && file.upload.bytesSent === file.upload.total)) {
                allFilesFinished = false;
              }

              file.upload.progress = progress;
              file.upload.bytesSent = file.upload.total;
            }

            if (allFilesFinished) {
              return;
            }
          }

          _results = [];

          for (_l = 0, _len3 = files.length; _l < _len3; _l++) {
            file = files[_l];

            _results.push(_this.emit("uploadprogress", file, progress, file.upload.bytesSent));
          }

          return _results;
        };
      }(this);

      xhr.onload = function (_this) {
        return function (e) {
          var _ref;

          if (files[0].status === Dropzone.CANCELED) {
            return;
          }

          if (xhr.readyState !== 4) {
            return;
          }

          response = xhr.responseText;

          if (xhr.getResponseHeader("content-type") && ~xhr.getResponseHeader("content-type").indexOf("application/json")) {
            try {
              response = JSON.parse(response);
            } catch (_error) {
              e = _error;
              response = "Invalid JSON response from server.";
            }
          }

          updateProgress();

          if (!(200 <= (_ref = xhr.status) && _ref < 300)) {
            return handleError();
          } else {
            return _this._finished(files, response, e);
          }
        };
      }(this);

      xhr.onerror = function (_this) {
        return function () {
          if (files[0].status === Dropzone.CANCELED) {
            return;
          }

          return handleError();
        };
      }(this);

      progressObj = (_ref = xhr.upload) != null ? _ref : xhr;
      progressObj.onprogress = updateProgress;
      headers = {
        "Accept": "application/json",
        "Cache-Control": "no-cache",
        "X-Requested-With": "XMLHttpRequest"
      };

      if (this.options.headers) {
        extend(headers, this.options.headers);
      }

      for (headerName in headers) {
        headerValue = headers[headerName];

        if (headerValue) {
          xhr.setRequestHeader(headerName, headerValue);
        }
      }

      formData = new FormData();

      if (this.options.params) {
        _ref1 = this.options.params;

        for (key in _ref1) {
          value = _ref1[key];
          formData.append(key, value);
        }
      }

      for (_j = 0, _len1 = files.length; _j < _len1; _j++) {
        file = files[_j];
        this.emit("sending", file, xhr, formData);
      }

      if (this.options.uploadMultiple) {
        this.emit("sendingmultiple", files, xhr, formData);
      }

      if (this.element.tagName === "FORM") {
        _ref2 = this.element.querySelectorAll("input, textarea, select, button");

        for (_k = 0, _len2 = _ref2.length; _k < _len2; _k++) {
          input = _ref2[_k];
          inputName = input.getAttribute("name");
          inputType = input.getAttribute("type");

          if (input.tagName === "SELECT" && input.hasAttribute("multiple")) {
            _ref3 = input.options;

            for (_l = 0, _len3 = _ref3.length; _l < _len3; _l++) {
              option = _ref3[_l];

              if (option.selected) {
                formData.append(inputName, option.value);
              }
            }
          } else if (!inputType || (_ref4 = inputType.toLowerCase()) !== "checkbox" && _ref4 !== "radio" || input.checked) {
            formData.append(inputName, input.value);
          }
        }
      }

      for (i = _m = 0, _ref5 = files.length - 1; 0 <= _ref5 ? _m <= _ref5 : _m >= _ref5; i = 0 <= _ref5 ? ++_m : --_m) {
        formData.append(this._getParamName(i), files[i], files[i].name);
      }

      return this.submitRequest(xhr, formData, files);
    };

    Dropzone.prototype.submitRequest = function (xhr, formData, files) {
      return xhr.send(formData);
    };

    Dropzone.prototype._finished = function (files, responseText, e) {
      var file, _i, _len;

      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.status = Dropzone.SUCCESS;
        this.emit("success", file, responseText, e);
        this.emit("complete", file);
      }

      if (this.options.uploadMultiple) {
        this.emit("successmultiple", files, responseText, e);
        this.emit("completemultiple", files);
      }

      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    };

    Dropzone.prototype._errorProcessing = function (files, message, xhr) {
      var file, _i, _len;

      for (_i = 0, _len = files.length; _i < _len; _i++) {
        file = files[_i];
        file.status = Dropzone.ERROR;
        this.emit("error", file, message, xhr);
        this.emit("complete", file);
      }

      if (this.options.uploadMultiple) {
        this.emit("errormultiple", files, message, xhr);
        this.emit("completemultiple", files);
      }

      if (this.options.autoProcessQueue) {
        return this.processQueue();
      }
    };

    return Dropzone;
  }(Emitter);

  Dropzone.version = "4.2.0";
  Dropzone.options = {};

  Dropzone.optionsForElement = function (element) {
    if (element.getAttribute("id")) {
      return Dropzone.options[camelize(element.getAttribute("id"))];
    } else {
      return void 0;
    }
  };

  Dropzone.instances = [];

  Dropzone.forElement = function (element) {
    if (typeof element === "string") {
      element = document.querySelector(element);
    }

    if ((element != null ? element.dropzone : void 0) == null) {
      throw new Error("No Dropzone found for given element. This is probably because you're trying to access it before Dropzone had the time to initialize. Use the `init` option to setup any additional observers on your Dropzone.");
    }

    return element.dropzone;
  };

  Dropzone.autoDiscover = true;

  Dropzone.discover = function () {
    var checkElements, dropzone, dropzones, _i, _len, _results;

    if (document.querySelectorAll) {
      dropzones = document.querySelectorAll(".dropzone");
    } else {
      dropzones = [];

      checkElements = function checkElements(elements) {
        var el, _i, _len, _results;

        _results = [];

        for (_i = 0, _len = elements.length; _i < _len; _i++) {
          el = elements[_i];

          if (/(^| )dropzone($| )/.test(el.className)) {
            _results.push(dropzones.push(el));
          } else {
            _results.push(void 0);
          }
        }

        return _results;
      };

      checkElements(document.getElementsByTagName("div"));
      checkElements(document.getElementsByTagName("form"));
    }

    _results = [];

    for (_i = 0, _len = dropzones.length; _i < _len; _i++) {
      dropzone = dropzones[_i];

      if (Dropzone.optionsForElement(dropzone) !== false) {
        _results.push(new Dropzone(dropzone));
      } else {
        _results.push(void 0);
      }
    }

    return _results;
  };

  Dropzone.blacklistedBrowsers = [/opera.*Macintosh.*version\/12/i];

  Dropzone.isBrowserSupported = function () {
    var capableBrowser, regex, _i, _len, _ref;

    capableBrowser = true;

    if (window.File && window.FileReader && window.FileList && window.Blob && window.FormData && document.querySelector) {
      if (!("classList" in document.createElement("a"))) {
        capableBrowser = false;
      } else {
        _ref = Dropzone.blacklistedBrowsers;

        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          regex = _ref[_i];

          if (regex.test(navigator.userAgent)) {
            capableBrowser = false;
            continue;
          }
        }
      }
    } else {
      capableBrowser = false;
    }

    return capableBrowser;
  };

  without = function without(list, rejectedItem) {
    var item, _i, _len, _results;

    _results = [];

    for (_i = 0, _len = list.length; _i < _len; _i++) {
      item = list[_i];

      if (item !== rejectedItem) {
        _results.push(item);
      }
    }

    return _results;
  };

  camelize = function camelize(str) {
    return str.replace(/[\-_](\w)/g, function (match) {
      return match.charAt(1).toUpperCase();
    });
  };

  Dropzone.createElement = function (string) {
    var div;
    div = document.createElement("div");
    div.innerHTML = string;
    return div.childNodes[0];
  };

  Dropzone.elementInside = function (element, container) {
    if (element === container) {
      return true;
    }

    while (element = element.parentNode) {
      if (element === container) {
        return true;
      }
    }

    return false;
  };

  Dropzone.getElement = function (el, name) {
    var element;

    if (typeof el === "string") {
      element = document.querySelector(el);
    } else if (el.nodeType != null) {
      element = el;
    }

    if (element == null) {
      throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector or a plain HTML element.");
    }

    return element;
  };

  Dropzone.getElements = function (els, name) {
    var e, el, elements, _i, _j, _len, _len1, _ref;

    if (els instanceof Array) {
      elements = [];

      try {
        for (_i = 0, _len = els.length; _i < _len; _i++) {
          el = els[_i];
          elements.push(this.getElement(el, name));
        }
      } catch (_error) {
        e = _error;
        elements = null;
      }
    } else if (typeof els === "string") {
      elements = [];
      _ref = document.querySelectorAll(els);

      for (_j = 0, _len1 = _ref.length; _j < _len1; _j++) {
        el = _ref[_j];
        elements.push(el);
      }
    } else if (els.nodeType != null) {
      elements = [els];
    }

    if (!(elements != null && elements.length)) {
      throw new Error("Invalid `" + name + "` option provided. Please provide a CSS selector, a plain HTML element or a list of those.");
    }

    return elements;
  };

  Dropzone.confirm = function (question, accepted, rejected) {
    if (window.confirm(question)) {
      return accepted();
    } else if (rejected != null) {
      return rejected();
    }
  };

  Dropzone.isValidFile = function (file, acceptedFiles) {
    var baseMimeType, mimeType, validType, _i, _len;

    if (!acceptedFiles) {
      return true;
    }

    acceptedFiles = acceptedFiles.split(",");
    mimeType = file.type;
    baseMimeType = mimeType.replace(/\/.*$/, "");

    for (_i = 0, _len = acceptedFiles.length; _i < _len; _i++) {
      validType = acceptedFiles[_i];
      validType = validType.trim();

      if (validType.charAt(0) === ".") {
        if (file.name.toLowerCase().indexOf(validType.toLowerCase(), file.name.length - validType.length) !== -1) {
          return true;
        }
      } else if (/\/\*$/.test(validType)) {
        if (baseMimeType === validType.replace(/\/.*$/, "")) {
          return true;
        }
      } else {
        if (mimeType === validType) {
          return true;
        }
      }
    }

    return false;
  };

  if (typeof jQuery !== "undefined" && jQuery !== null) {
    jQuery.fn.dropzone = function (options) {
      return this.each(function () {
        return new Dropzone(this, options);
      });
    };
  }

  if ( true && module !== null) {
    module.exports = Dropzone;
  } else {
    window.Dropzone = Dropzone;
  }

  Dropzone.ADDED = "added";
  Dropzone.QUEUED = "queued";
  Dropzone.ACCEPTED = Dropzone.QUEUED;
  Dropzone.UPLOADING = "uploading";
  Dropzone.PROCESSING = Dropzone.UPLOADING;
  Dropzone.CANCELED = "canceled";
  Dropzone.ERROR = "error";
  Dropzone.SUCCESS = "success";
  /*
  
  Bugfix for iOS 6 and 7
  Source: http://stackoverflow.com/questions/11929099/html5-canvas-drawimage-ratio-bug-ios
  based on the work of https://github.com/stomita/ios-imagefile-megapixel
   */

  detectVerticalSquash = function detectVerticalSquash(img) {
    var alpha, canvas, ctx, data, ey, ih, iw, py, ratio, sy;
    iw = img.naturalWidth;
    ih = img.naturalHeight;
    canvas = document.createElement("canvas");
    canvas.width = 1;
    canvas.height = ih;
    ctx = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    data = ctx.getImageData(0, 0, 1, ih).data;
    sy = 0;
    ey = ih;
    py = ih;

    while (py > sy) {
      alpha = data[(py - 1) * 4 + 3];

      if (alpha === 0) {
        ey = py;
      } else {
        sy = py;
      }

      py = ey + sy >> 1;
    }

    ratio = py / ih;

    if (ratio === 0) {
      return 1;
    } else {
      return ratio;
    }
  };

  drawImageIOSFix = function drawImageIOSFix(ctx, img, sx, sy, sw, sh, dx, dy, dw, dh) {
    var vertSquashRatio;
    vertSquashRatio = detectVerticalSquash(img);
    return ctx.drawImage(img, sx, sy, sw, sh, dx, dy, dw, dh / vertSquashRatio);
  };
  /*
   * contentloaded.js
   *
   * Author: Diego Perini (diego.perini at gmail.com)
   * Summary: cross-browser wrapper for DOMContentLoaded
   * Updated: 20101020
   * License: MIT
   * Version: 1.2
   *
   * URL:
   * http://javascript.nwbox.com/ContentLoaded/
   * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
   */


  contentLoaded = function contentLoaded(win, fn) {
    var add, doc, done, _init, _poll, pre, rem, root, top;

    done = false;
    top = true;
    doc = win.document;
    root = doc.documentElement;
    add = doc.addEventListener ? "addEventListener" : "attachEvent";
    rem = doc.addEventListener ? "removeEventListener" : "detachEvent";
    pre = doc.addEventListener ? "" : "on";

    _init = function init(e) {
      if (e.type === "readystatechange" && doc.readyState !== "complete") {
        return;
      }

      (e.type === "load" ? win : doc)[rem](pre + e.type, _init, false);

      if (!done && (done = true)) {
        return fn.call(win, e.type || e);
      }
    };

    _poll = function poll() {
      var e;

      try {
        root.doScroll("left");
      } catch (_error) {
        e = _error;
        setTimeout(_poll, 50);
        return;
      }

      return _init("poll");
    };

    if (doc.readyState !== "complete") {
      if (doc.createEventObject && root.doScroll) {
        try {
          top = !win.frameElement;
        } catch (_error) {}

        if (top) {
          _poll();
        }
      }

      doc[add](pre + "DOMContentLoaded", _init, false);
      doc[add](pre + "readystatechange", _init, false);
      return win[add](pre + "load", _init, false);
    }
  };

  Dropzone._autoDiscoverFunction = function () {
    if (Dropzone.autoDiscover) {
      return Dropzone.discover();
    }
  };

  contentLoaded(window, Dropzone._autoDiscoverFunction);
}).call(this);
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../../../node_modules/webpack/buildin/module.js */ "./node_modules/webpack/buildin/module.js")(module)))

/***/ }),

/***/ "./assets/backend/dashboard/js/order.js":
/*!**********************************************!*\
  !*** ./assets/backend/dashboard/js/order.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.array.concat */ "./node_modules/core-js/modules/es.array.concat.js");

__webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");

__webpack_require__(/*! core-js/modules/es.array.join */ "./node_modules/core-js/modules/es.array.join.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.function.name */ "./node_modules/core-js/modules/es.function.name.js");

!(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
  var Order_List_Detail = function Order_List_Detail(_ref, orderUuid) {
    var uuid = _ref.uuid,
        basketField1 = _ref.basketField1,
        customerInfo = _ref.customerInfo,
        specialProductTypeObject = _ref.specialProductTypeObject,
        basketField2 = _ref.basketField2,
        pos = _ref.pos,
        count = _ref.count,
        allGross = _ref.allGross,
        product = _ref.product,
        status = _ref.status,
        allNet = _ref.allNet;
    return "\n        <tr style=\"".concat(psc.order.get_pos_bg_color(status), "\" class=\"").concat(psc.order.get_pos_bg_color_class(status), "\">\n        <td>-</td>\n        <td></td>\n        <td>").concat(pos, "</td>\n        <td colspan=\"4\"><a href=\"/apps/backend/product/edit/index/").concat(product.uuid, "\">").concat(product.title, "</a><br/>ArtNr intern: ").concat(product.nrIntern, "<br/>\n        ").concat(psc.order.get_special_product_options(specialProductTypeObject), "\n        </td>\n        <td>Auflage: ").concat(count, "</td>\n        <td>Kunden Info: ").concat(customerInfo, "</td>\n        <td>").concat(basketField1, "</td>\n        <td>").concat(basketField2, "</td>\n        <td></td>\n        <td class=\"text-end\">").concat(new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: 'EUR'
    }).format(allNet), " <strong>(").concat(new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: 'EUR'
    }).format(allGross), ")</strong></td>\n        <td><div class=\"btn-group btn-group-sm\">\n                <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-bs-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n                ").concat(psc.order.get_pos_status(status).internalName, "\n                </button>\n                <div class=\"dropdown-menu\">\n                    ").concat(psc.order.get_pos_status_loop(orderUuid, uuid), "\n                </div>\n            </div></td>\n        <td></td>\n    </tr>\n    ");
  };

  var Order_List_Detail_Simple = function Order_List_Detail_Simple(_ref2, orderUuid) {
    var uuid = _ref2.uuid,
        basketField1 = _ref2.basketField1,
        customerInfo = _ref2.customerInfo,
        specialProductTypeObject = _ref2.specialProductTypeObject,
        basketField2 = _ref2.basketField2,
        pos = _ref2.pos,
        count = _ref2.count,
        allGross = _ref2.allGross,
        product = _ref2.product,
        status = _ref2.status,
        allNet = _ref2.allNet;
    return "\n        <tr style=\"".concat(psc.order.get_pos_bg_color(status), "\" class=\"").concat(psc.order.get_pos_bg_color_class(status), "\">\n        <td>-</td>\n        <td></td>\n        <td>").concat(pos, "</td>\n        <td colspan=\"4\"><a href=\"/apps/backend/product/edit/index/").concat(product.uuid, "\">").concat(product.title, "</a><br/>ArtNr intern: ").concat(product.nrIntern, "<br/>\n        ").concat(psc.order.get_special_product_options(specialProductTypeObject), "</td>\n        <td>Auflage: ").concat(count, "</td>\n        <td>Kunden Info: ").concat(customerInfo, "</td>\n        <td>").concat(basketField1, "</td>\n        <td>").concat(basketField2, "</td>\n        <td></td>\n        <td class=\"text-end\">").concat(new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: 'EUR'
    }).format(allNet), " <strong>(").concat(new Intl.NumberFormat('de-DE', {
      style: 'currency',
      currency: 'EUR'
    }).format(allGross), ")</strong></td>\n        <td>").concat(psc.order.get_pos_status(status).internalName, "</td>\n        <td></td>\n    </tr>\n    ");
  };

  var Order_List_Pos_Status = function Order_List_Pos_Status(status, orderUuid, posUuid) {
    return "\n        <button type=\"button\" class=\"dropdown-item switch_pos\" data-order-uuid=\"".concat(orderUuid, "\" data-pos-uuid=\"").concat(posUuid, "\" data-status=\"").concat(status.code, "\">").concat(status.internalName, "</button>\n    ");
  };

  var Order_List_Pos_Calc_Option = function Order_List_Pos_Calc_Option(elm) {
    return "\n        <tr><td>".concat(elm.name, "</td><td>").concat(elm.value, "</td></tr>\n    ");
  };

  return {
    init: function init() {
      this.load_status();
      this.bind_buttons();
    },
    get_special_product_options: function get_special_product_options(specialProductTypeObject) {
      if (specialProductTypeObject.typ != 6) {
        return '';
      }

      return '<table>' + specialProductTypeObject.options.map(function (elm) {
        if (elm.type != 'hidden') {
          return Order_List_Pos_Calc_Option(elm);
        }
      }).join('') + '</table>';
    },
    load_status: function load_status() {
      $.ajax({
        url: "/apps/api/system/status",
        contentType: "application/json",
        headers: {
          "Authorization": "Bearer " + jwt_token
        },
        method: 'GET',
        success: function success(result) {
          psc.order.status_order = result.order;
          psc.order.status_pos = result.position;
        }
      });
    },
    bind_buttons: function bind_buttons() {
      $('.toogle_list_detail_view').off();
      $('.toogle_list_detail_view').on('click', function () {
        var uuid = $(this).data().uuid;

        if ($('#rows-' + uuid + ' > tr').length > 0) {
          $('#rows-' + uuid).html('');
        } else {
          psc.order.load_detail(uuid);
        }
      });
      $('.toogle_list_simple_detail_view').off();
      $('.toogle_list_simple_detail_view').on('click', function () {
        var uuid = $(this).data().uuid;

        if ($('#rows-' + uuid + ' > tr').length > 0) {
          $('#rows-' + uuid).html('');
        } else {
          psc.order.load_detail(uuid, true);
        }
      });
    },
    load_detail: function load_detail(orderUuid) {
      var simple = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      $.ajax({
        url: "/apps/api/order/getonebyuuid",
        contentType: "application/json",
        headers: {
          "Authorization": "Bearer " + jwt_token
        },
        method: 'POST',
        data: JSON.stringify({
          uuid: orderUuid
        }),
        success: function success(result) {
          psc.order.build_details(orderUuid, result, simple);
        }
      });
    },
    build_details: function build_details(orderUuid, result) {
      var simple = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

      if (simple) {
        $('#rows-' + result.uuid).html(result.positions.map(function (x) {
          return Order_List_Detail_Simple(x, orderUuid);
        }).join(''));
      } else {
        $('#rows-' + result.uuid).html(result.positions.map(function (x) {
          return Order_List_Detail(x, orderUuid);
        }).join(''));
      }

      this.bind_status_buttons();
    },
    bind_status_buttons: function bind_status_buttons() {
      $('.switch_pos').off();
      $('.switch_pos').on('click', function () {
        var orderUuid = $(this).data().orderUuid;
        var posUuid = $(this).data().posUuid;
        var status = $(this).data().status;
        $.ajax({
          url: "/apps/api/position/status/change",
          contentType: "application/json",
          headers: {
            "Authorization": "Bearer " + jwt_token
          },
          data: JSON.stringify({
            position: posUuid,
            status: status
          }),
          method: 'POST',
          success: function success(result) {
            $('#rows-' + orderUuid).html('');
            psc.order.load_detail(orderUuid);
          }
        });
      });
    },
    get_pos_bg_color: function get_pos_bg_color(status) {
      /*
      {% if orderStatuse.getPosStatusColor(pos.status) != '' %}background-color: {{ orderStatuse.getPosStatusColor(pos.status) }}{% endif %}"
      */
      var found = psc.order.status_pos.find(function (stat) {
        return stat.code == status;
      });

      if (found && found.color != "") {
        return 'background-color: ' + found.color;
      }
    },
    get_pos_status: function get_pos_status(status) {
      /*
      {% if orderStatuse.getPosStatusColor(pos.status) != '' %}background-color: {{ orderStatuse.getPosStatusColor(pos.status) }}{% endif %}"
      */
      var found = psc.order.status_pos.find(function (stat) {
        return stat.code == status;
      });

      if (found) {
        return found;
      }

      return {
        internalName: 'notFound'
      };
    },
    get_pos_bg_color_class: function get_pos_bg_color_class(status) {
      /*
      class="{% if pos.status == 170 %}bg-lightdark{% elseif pos.status == 200 or pos.status == 210 %}bg-lightsuccess{% else %}bg-lightdanger{% endif %}
      */
      if (status == 170) {
        return 'bg-lightdark';
      }

      if (status == 200 || status == 210) {
        return 'bg-lightsuccess';
      }

      return 'bg-lightdanger';
    },
    get_pos_status_loop: function get_pos_status_loop(orderUuid, posUuid) {
      return psc.order.status_pos.map(function (x) {
        return Order_List_Pos_Status(x, orderUuid, posUuid);
      }).join('');
    }
  };
}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),

/***/ "./assets/backend/dashboard/js/summernote/mediabundle.plugin.js":
/*!**********************************************************************!*\
  !*** ./assets/backend/dashboard/js/summernote/mediabundle.plugin.js ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;(function (factory) {
  /* global define */
  if (true) {
    // AMD. Register as an anonymous module.
    !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js")], __WEBPACK_AMD_DEFINE_FACTORY__ = (factory),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__)) : __WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));
  } else {}
})(function ($) {
  $.extend($.summernote.plugins, {
    'media': function media(context) {
      var self = this; // ui has renders to build ui elements.
      //  - you can create a button with `ui.button`

      var ui = $.summernote.ui; // add hello button

      context.memo('button.media', function () {
        // create button
        var button = ui.button({
          contents: '<i class="fa fa-images"/> Media',
          tooltip: 'Media',
          click: function click() {
            mediaBundleBrowser(context.$note.attr('id'));
          }
        }); // create jQuery object from button instance.

        var $media = button.render();
        return $media;
      });
    }
  });
});

/***/ }),

/***/ "./assets/backend/dashboard/js/tether.min.js":
/*!***************************************************!*\
  !*** ./assets/backend/dashboard/js/tether.min.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_FACTORY__, __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.symbol */ "./node_modules/core-js/modules/es.symbol.js");

__webpack_require__(/*! core-js/modules/es.symbol.description */ "./node_modules/core-js/modules/es.symbol.description.js");

__webpack_require__(/*! core-js/modules/es.symbol.iterator */ "./node_modules/core-js/modules/es.symbol.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/es.array.index-of */ "./node_modules/core-js/modules/es.array.index-of.js");

__webpack_require__(/*! core-js/modules/es.array.is-array */ "./node_modules/core-js/modules/es.array.is-array.js");

__webpack_require__(/*! core-js/modules/es.array.iterator */ "./node_modules/core-js/modules/es.array.iterator.js");

__webpack_require__(/*! core-js/modules/es.array.join */ "./node_modules/core-js/modules/es.array.join.js");

__webpack_require__(/*! core-js/modules/es.array.map */ "./node_modules/core-js/modules/es.array.map.js");

__webpack_require__(/*! core-js/modules/es.array.slice */ "./node_modules/core-js/modules/es.array.slice.js");

__webpack_require__(/*! core-js/modules/es.array.splice */ "./node_modules/core-js/modules/es.array.splice.js");

__webpack_require__(/*! core-js/modules/es.date.to-string */ "./node_modules/core-js/modules/es.date.to-string.js");

__webpack_require__(/*! core-js/modules/es.function.bind */ "./node_modules/core-js/modules/es.function.bind.js");

__webpack_require__(/*! core-js/modules/es.object.create */ "./node_modules/core-js/modules/es.object.create.js");

__webpack_require__(/*! core-js/modules/es.object.define-property */ "./node_modules/core-js/modules/es.object.define-property.js");

__webpack_require__(/*! core-js/modules/es.object.get-own-property-descriptor */ "./node_modules/core-js/modules/es.object.get-own-property-descriptor.js");

__webpack_require__(/*! core-js/modules/es.object.get-prototype-of */ "./node_modules/core-js/modules/es.object.get-prototype-of.js");

__webpack_require__(/*! core-js/modules/es.object.set-prototype-of */ "./node_modules/core-js/modules/es.object.set-prototype-of.js");

__webpack_require__(/*! core-js/modules/es.object.to-string */ "./node_modules/core-js/modules/es.object.to-string.js");

__webpack_require__(/*! core-js/modules/es.parse-float */ "./node_modules/core-js/modules/es.parse-float.js");

__webpack_require__(/*! core-js/modules/es.regexp.constructor */ "./node_modules/core-js/modules/es.regexp.constructor.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");

__webpack_require__(/*! core-js/modules/es.string.iterator */ "./node_modules/core-js/modules/es.string.iterator.js");

__webpack_require__(/*! core-js/modules/es.string.replace */ "./node_modules/core-js/modules/es.string.replace.js");

__webpack_require__(/*! core-js/modules/es.string.split */ "./node_modules/core-js/modules/es.string.split.js");

__webpack_require__(/*! core-js/modules/es.string.trim */ "./node_modules/core-js/modules/es.string.trim.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.for-each */ "./node_modules/core-js/modules/web.dom-collections.for-each.js");

__webpack_require__(/*! core-js/modules/web.dom-collections.iterator */ "./node_modules/core-js/modules/web.dom-collections.iterator.js");

__webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

!function (t, e) {
   true ? !(__WEBPACK_AMD_DEFINE_FACTORY__ = (e),
				__WEBPACK_AMD_DEFINE_RESULT__ = (typeof __WEBPACK_AMD_DEFINE_FACTORY__ === 'function' ?
				(__WEBPACK_AMD_DEFINE_FACTORY__.call(exports, __webpack_require__, exports, module)) :
				__WEBPACK_AMD_DEFINE_FACTORY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : undefined;
}(this, function (t, e, o) {
  "use strict";

  function n(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
  }

  function i(t) {
    var e = t.getBoundingClientRect(),
        o = {};

    for (var n in e) {
      o[n] = e[n];
    }

    if (t.ownerDocument !== document) {
      var r = t.ownerDocument.defaultView.frameElement;

      if (r) {
        var s = i(r);
        o.top += s.top, o.bottom += s.top, o.left += s.left, o.right += s.left;
      }
    }

    return o;
  }

  function r(t) {
    var e = getComputedStyle(t) || {},
        o = e.position,
        n = [];
    if ("fixed" === o) return [t];

    for (var i = t; (i = i.parentNode) && i && 1 === i.nodeType;) {
      var r = void 0;

      try {
        r = getComputedStyle(i);
      } catch (s) {}

      if ("undefined" == typeof r || null === r) return n.push(i), n;
      var a = r,
          f = a.overflow,
          l = a.overflowX,
          h = a.overflowY;
      /(auto|scroll)/.test(f + h + l) && ("absolute" !== o || ["relative", "absolute", "fixed"].indexOf(r.position) >= 0) && n.push(i);
    }

    return n.push(t.ownerDocument.body), t.ownerDocument !== document && n.push(t.ownerDocument.defaultView), n;
  }

  function s() {
    A && document.body.removeChild(A), A = null;
  }

  function a(t) {
    var e = void 0;
    t === document ? (e = document, t = document.documentElement) : e = t.ownerDocument;
    var o = e.documentElement,
        n = i(t),
        r = P();
    return n.top -= r.top, n.left -= r.left, "undefined" == typeof n.width && (n.width = document.body.scrollWidth - n.left - n.right), "undefined" == typeof n.height && (n.height = document.body.scrollHeight - n.top - n.bottom), n.top = n.top - o.clientTop, n.left = n.left - o.clientLeft, n.right = e.body.clientWidth - n.width - n.left, n.bottom = e.body.clientHeight - n.height - n.top, n;
  }

  function f(t) {
    return t.offsetParent || document.documentElement;
  }

  function l() {
    var t = document.createElement("div");
    t.style.width = "100%", t.style.height = "200px";
    var e = document.createElement("div");
    h(e.style, {
      position: "absolute",
      top: 0,
      left: 0,
      pointerEvents: "none",
      visibility: "hidden",
      width: "200px",
      height: "150px",
      overflow: "hidden"
    }), e.appendChild(t), document.body.appendChild(e);
    var o = t.offsetWidth;
    e.style.overflow = "scroll";
    var n = t.offsetWidth;
    o === n && (n = e.clientWidth), document.body.removeChild(e);
    var i = o - n;
    return {
      width: i,
      height: i
    };
  }

  function h() {
    var t = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0],
        e = [];
    return Array.prototype.push.apply(e, arguments), e.slice(1).forEach(function (e) {
      if (e) for (var o in e) {
        ({}).hasOwnProperty.call(e, o) && (t[o] = e[o]);
      }
    }), t;
  }

  function u(t, e) {
    if ("undefined" != typeof t.classList) e.split(" ").forEach(function (e) {
      e.trim() && t.classList.remove(e);
    });else {
      var o = new RegExp("(^| )" + e.split(" ").join("|") + "( |$)", "gi"),
          n = c(t).replace(o, " ");
      g(t, n);
    }
  }

  function d(t, e) {
    if ("undefined" != typeof t.classList) e.split(" ").forEach(function (e) {
      e.trim() && t.classList.add(e);
    });else {
      u(t, e);
      var o = c(t) + (" " + e);
      g(t, o);
    }
  }

  function p(t, e) {
    if ("undefined" != typeof t.classList) return t.classList.contains(e);
    var o = c(t);
    return new RegExp("(^| )" + e + "( |$)", "gi").test(o);
  }

  function c(t) {
    return t.className instanceof t.ownerDocument.defaultView.SVGAnimatedString ? t.className.baseVal : t.className;
  }

  function g(t, e) {
    t.setAttribute("class", e);
  }

  function m(t, e, o) {
    o.forEach(function (o) {
      -1 === e.indexOf(o) && p(t, o) && u(t, o);
    }), e.forEach(function (e) {
      p(t, e) || d(t, e);
    });
  }

  function n(t, e) {
    if (!(t instanceof e)) throw new TypeError("Cannot call a class as a function");
  }

  function v(t, e) {
    if ("function" != typeof e && null !== e) throw new TypeError("Super expression must either be null or a function, not " + _typeof(e));
    t.prototype = Object.create(e && e.prototype, {
      constructor: {
        value: t,
        enumerable: !1,
        writable: !0,
        configurable: !0
      }
    }), e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e);
  }

  function y(t, e) {
    var o = arguments.length <= 2 || void 0 === arguments[2] ? 1 : arguments[2];
    return t + o >= e && e >= t - o;
  }

  function b() {
    return "undefined" != typeof performance && "undefined" != typeof performance.now ? performance.now() : +new Date();
  }

  function w() {
    for (var t = {
      top: 0,
      left: 0
    }, e = arguments.length, o = Array(e), n = 0; e > n; n++) {
      o[n] = arguments[n];
    }

    return o.forEach(function (e) {
      var o = e.top,
          n = e.left;
      "string" == typeof o && (o = parseFloat(o, 10)), "string" == typeof n && (n = parseFloat(n, 10)), t.top += o, t.left += n;
    }), t;
  }

  function C(t, e) {
    return "string" == typeof t.left && -1 !== t.left.indexOf("%") && (t.left = parseFloat(t.left, 10) / 100 * e.width), "string" == typeof t.top && -1 !== t.top.indexOf("%") && (t.top = parseFloat(t.top, 10) / 100 * e.height), t;
  }

  function O(t, e) {
    return "scrollParent" === e ? e = t.scrollParents[0] : "window" === e && (e = [pageXOffset, pageYOffset, innerWidth + pageXOffset, innerHeight + pageYOffset]), e === document && (e = e.documentElement), "undefined" != typeof e.nodeType && !function () {
      var t = e,
          o = a(e),
          n = o,
          i = getComputedStyle(e);

      if (e = [n.left, n.top, o.width + n.left, o.height + n.top], t.ownerDocument !== document) {
        var r = t.ownerDocument.defaultView;
        e[0] += r.pageXOffset, e[1] += r.pageYOffset, e[2] += r.pageXOffset, e[3] += r.pageYOffset;
      }

      $.forEach(function (t, o) {
        t = t[0].toUpperCase() + t.substr(1), "Top" === t || "Left" === t ? e[o] += parseFloat(i["border" + t + "Width"]) : e[o] -= parseFloat(i["border" + t + "Width"]);
      });
    }(), e;
  }

  var E = function () {
    function t(t, e) {
      for (var o = 0; o < e.length; o++) {
        var n = e[o];
        n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
      }
    }

    return function (e, o, n) {
      return o && t(e.prototype, o), n && t(e, n), e;
    };
  }(),
      x = void 0;

  "undefined" == typeof x && (x = {
    modules: []
  });

  var A = null,
      T = function () {
    var t = 0;
    return function () {
      return ++t;
    };
  }(),
      S = {},
      P = function P() {
    var t = A;
    t || (t = document.createElement("div"), t.setAttribute("data-tether-id", T()), h(t.style, {
      top: 0,
      left: 0,
      position: "absolute"
    }), document.body.appendChild(t), A = t);
    var e = t.getAttribute("data-tether-id");
    return "undefined" == typeof S[e] && (S[e] = i(t), M(function () {
      delete S[e];
    })), S[e];
  },
      W = [],
      M = function M(t) {
    W.push(t);
  },
      _ = function _() {
    for (var t = void 0; t = W.pop();) {
      t();
    }
  },
      k = function () {
    function t() {
      n(this, t);
    }

    return E(t, [{
      key: "on",
      value: function value(t, e, o) {
        var n = arguments.length <= 3 || void 0 === arguments[3] ? !1 : arguments[3];
        "undefined" == typeof this.bindings && (this.bindings = {}), "undefined" == typeof this.bindings[t] && (this.bindings[t] = []), this.bindings[t].push({
          handler: e,
          ctx: o,
          once: n
        });
      }
    }, {
      key: "once",
      value: function value(t, e, o) {
        this.on(t, e, o, !0);
      }
    }, {
      key: "off",
      value: function value(t, e) {
        if ("undefined" != typeof this.bindings && "undefined" != typeof this.bindings[t]) if ("undefined" == typeof e) delete this.bindings[t];else for (var o = 0; o < this.bindings[t].length;) {
          this.bindings[t][o].handler === e ? this.bindings[t].splice(o, 1) : ++o;
        }
      }
    }, {
      key: "trigger",
      value: function value(t) {
        if ("undefined" != typeof this.bindings && this.bindings[t]) {
          for (var e = 0, o = arguments.length, n = Array(o > 1 ? o - 1 : 0), i = 1; o > i; i++) {
            n[i - 1] = arguments[i];
          }

          for (; e < this.bindings[t].length;) {
            var r = this.bindings[t][e],
                s = r.handler,
                a = r.ctx,
                f = r.once,
                l = a;
            "undefined" == typeof l && (l = this), s.apply(l, n), f ? this.bindings[t].splice(e, 1) : ++e;
          }
        }
      }
    }]), t;
  }();

  x.Utils = {
    getActualBoundingClientRect: i,
    getScrollParents: r,
    getBounds: a,
    getOffsetParent: f,
    extend: h,
    addClass: d,
    removeClass: u,
    hasClass: p,
    updateClasses: m,
    defer: M,
    flush: _,
    uniqueId: T,
    Evented: k,
    getScrollBarSize: l,
    removeUtilElements: s
  };

  var B = function () {
    function t(t, e) {
      var o = [],
          n = !0,
          i = !1,
          r = void 0;

      try {
        for (var s, a = t[Symbol.iterator](); !(n = (s = a.next()).done) && (o.push(s.value), !e || o.length !== e); n = !0) {
          ;
        }
      } catch (f) {
        i = !0, r = f;
      } finally {
        try {
          !n && a["return"] && a["return"]();
        } finally {
          if (i) throw r;
        }
      }

      return o;
    }

    return function (e, o) {
      if (Array.isArray(e)) return e;
      if (Symbol.iterator in Object(e)) return t(e, o);
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    };
  }(),
      E = function () {
    function t(t, e) {
      for (var o = 0; o < e.length; o++) {
        var n = e[o];
        n.enumerable = n.enumerable || !1, n.configurable = !0, "value" in n && (n.writable = !0), Object.defineProperty(t, n.key, n);
      }
    }

    return function (e, o, n) {
      return o && t(e.prototype, o), n && t(e, n), e;
    };
  }(),
      z = function z(t, e, o) {
    for (var n = !0; n;) {
      var i = t,
          r = e,
          s = o;
      n = !1, null === i && (i = Function.prototype);
      var a = Object.getOwnPropertyDescriptor(i, r);

      if (void 0 !== a) {
        if ("value" in a) return a.value;
        var f = a.get;
        if (void 0 === f) return;
        return f.call(s);
      }

      var l = Object.getPrototypeOf(i);
      if (null === l) return;
      t = l, e = r, o = s, n = !0, a = l = void 0;
    }
  };

  if ("undefined" == typeof x) throw new Error("You must include the utils.js file before tether.js");

  var j = x.Utils,
      r = j.getScrollParents,
      a = j.getBounds,
      f = j.getOffsetParent,
      h = j.extend,
      d = j.addClass,
      u = j.removeClass,
      m = j.updateClasses,
      M = j.defer,
      _ = j.flush,
      l = j.getScrollBarSize,
      s = j.removeUtilElements,
      Y = function () {
    if ("undefined" == typeof document) return "";

    for (var t = document.createElement("div"), e = ["transform", "WebkitTransform", "OTransform", "MozTransform", "msTransform"], o = 0; o < e.length; ++o) {
      var n = e[o];
      if (void 0 !== t.style[n]) return n;
    }
  }(),
      L = [],
      D = function D() {
    L.forEach(function (t) {
      t.position(!1);
    }), _();
  };

  !function () {
    var t = null,
        e = null,
        o = null,
        n = function i() {
      return "undefined" != typeof e && e > 16 ? (e = Math.min(e - 16, 250), void (o = setTimeout(i, 250))) : void ("undefined" != typeof t && b() - t < 10 || (null != o && (clearTimeout(o), o = null), t = b(), D(), e = b() - t));
    };

    "undefined" != typeof window && "undefined" != typeof window.addEventListener && ["resize", "scroll", "touchmove"].forEach(function (t) {
      window.addEventListener(t, n);
    });
  }();

  var X = {
    center: "center",
    left: "right",
    right: "left"
  },
      F = {
    middle: "middle",
    top: "bottom",
    bottom: "top"
  },
      H = {
    top: 0,
    left: 0,
    middle: "50%",
    center: "50%",
    bottom: "100%",
    right: "100%"
  },
      N = function N(t, e) {
    var o = t.left,
        n = t.top;
    return "auto" === o && (o = X[e.left]), "auto" === n && (n = F[e.top]), {
      left: o,
      top: n
    };
  },
      U = function U(t) {
    var e = t.left,
        o = t.top;
    return "undefined" != typeof H[t.left] && (e = H[t.left]), "undefined" != typeof H[t.top] && (o = H[t.top]), {
      left: e,
      top: o
    };
  },
      V = function V(t) {
    var e = t.split(" "),
        o = B(e, 2),
        n = o[0],
        i = o[1];
    return {
      top: n,
      left: i
    };
  },
      R = V,
      q = function (t) {
    function e(t) {
      var o = this;
      n(this, e), z(Object.getPrototypeOf(e.prototype), "constructor", this).call(this), this.position = this.position.bind(this), L.push(this), this.history = [], this.setOptions(t, !1), x.modules.forEach(function (t) {
        "undefined" != typeof t.initialize && t.initialize.call(o);
      }), this.position();
    }

    return v(e, t), E(e, [{
      key: "getClass",
      value: function value() {
        var t = arguments.length <= 0 || void 0 === arguments[0] ? "" : arguments[0],
            e = this.options.classes;
        return "undefined" != typeof e && e[t] ? this.options.classes[t] : this.options.classPrefix ? this.options.classPrefix + "-" + t : t;
      }
    }, {
      key: "setOptions",
      value: function value(t) {
        var e = this,
            o = arguments.length <= 1 || void 0 === arguments[1] ? !0 : arguments[1],
            n = {
          offset: "0 0",
          targetOffset: "0 0",
          targetAttachment: "auto auto",
          classPrefix: "tether"
        };
        this.options = h(n, t);
        var i = this.options,
            s = i.element,
            a = i.target,
            f = i.targetModifier;
        if (this.element = s, this.target = a, this.targetModifier = f, "viewport" === this.target ? (this.target = document.body, this.targetModifier = "visible") : "scroll-handle" === this.target && (this.target = document.body, this.targetModifier = "scroll-handle"), ["element", "target"].forEach(function (t) {
          if ("undefined" == typeof e[t]) throw new Error("Tether Error: Both element and target must be defined");
          "undefined" != typeof e[t].jquery ? e[t] = e[t][0] : "string" == typeof e[t] && (e[t] = document.querySelector(e[t]));
        }), d(this.element, this.getClass("element")), this.options.addTargetClasses !== !1 && d(this.target, this.getClass("target")), !this.options.attachment) throw new Error("Tether Error: You must provide an attachment");
        this.targetAttachment = R(this.options.targetAttachment), this.attachment = R(this.options.attachment), this.offset = V(this.options.offset), this.targetOffset = V(this.options.targetOffset), "undefined" != typeof this.scrollParents && this.disable(), "scroll-handle" === this.targetModifier ? this.scrollParents = [this.target] : this.scrollParents = r(this.target), this.options.enabled !== !1 && this.enable(o);
      }
    }, {
      key: "getTargetBounds",
      value: function value() {
        if ("undefined" == typeof this.targetModifier) return a(this.target);

        if ("visible" === this.targetModifier) {
          if (this.target === document.body) return {
            top: pageYOffset,
            left: pageXOffset,
            height: innerHeight,
            width: innerWidth
          };
          var t = a(this.target),
              e = {
            height: t.height,
            width: t.width,
            top: t.top,
            left: t.left
          };
          return e.height = Math.min(e.height, t.height - (pageYOffset - t.top)), e.height = Math.min(e.height, t.height - (t.top + t.height - (pageYOffset + innerHeight))), e.height = Math.min(innerHeight, e.height), e.height -= 2, e.width = Math.min(e.width, t.width - (pageXOffset - t.left)), e.width = Math.min(e.width, t.width - (t.left + t.width - (pageXOffset + innerWidth))), e.width = Math.min(innerWidth, e.width), e.width -= 2, e.top < pageYOffset && (e.top = pageYOffset), e.left < pageXOffset && (e.left = pageXOffset), e;
        }

        if ("scroll-handle" === this.targetModifier) {
          var t = void 0,
              o = this.target;
          o === document.body ? (o = document.documentElement, t = {
            left: pageXOffset,
            top: pageYOffset,
            height: innerHeight,
            width: innerWidth
          }) : t = a(o);
          var n = getComputedStyle(o),
              i = o.scrollWidth > o.clientWidth || [n.overflow, n.overflowX].indexOf("scroll") >= 0 || this.target !== document.body,
              r = 0;
          i && (r = 15);
          var s = t.height - parseFloat(n.borderTopWidth) - parseFloat(n.borderBottomWidth) - r,
              e = {
            width: 15,
            height: .975 * s * (s / o.scrollHeight),
            left: t.left + t.width - parseFloat(n.borderLeftWidth) - 15
          },
              f = 0;
          408 > s && this.target === document.body && (f = -11e-5 * Math.pow(s, 2) - .00727 * s + 22.58), this.target !== document.body && (e.height = Math.max(e.height, 24));
          var l = this.target.scrollTop / (o.scrollHeight - s);
          return e.top = l * (s - e.height - f) + t.top + parseFloat(n.borderTopWidth), this.target === document.body && (e.height = Math.max(e.height, 24)), e;
        }
      }
    }, {
      key: "clearCache",
      value: function value() {
        this._cache = {};
      }
    }, {
      key: "cache",
      value: function value(t, e) {
        return "undefined" == typeof this._cache && (this._cache = {}), "undefined" == typeof this._cache[t] && (this._cache[t] = e.call(this)), this._cache[t];
      }
    }, {
      key: "enable",
      value: function value() {
        var t = this,
            e = arguments.length <= 0 || void 0 === arguments[0] ? !0 : arguments[0];
        this.options.addTargetClasses !== !1 && d(this.target, this.getClass("enabled")), d(this.element, this.getClass("enabled")), this.enabled = !0, this.scrollParents.forEach(function (e) {
          e !== t.target.ownerDocument && e.addEventListener("scroll", t.position);
        }), e && this.position();
      }
    }, {
      key: "disable",
      value: function value() {
        var t = this;
        u(this.target, this.getClass("enabled")), u(this.element, this.getClass("enabled")), this.enabled = !1, "undefined" != typeof this.scrollParents && this.scrollParents.forEach(function (e) {
          e.removeEventListener("scroll", t.position);
        });
      }
    }, {
      key: "destroy",
      value: function value() {
        var t = this;
        this.disable(), L.forEach(function (e, o) {
          e === t && L.splice(o, 1);
        }), 0 === L.length && s();
      }
    }, {
      key: "updateAttachClasses",
      value: function value(t, e) {
        var o = this;
        t = t || this.attachment, e = e || this.targetAttachment;
        var n = ["left", "top", "bottom", "right", "middle", "center"];
        "undefined" != typeof this._addAttachClasses && this._addAttachClasses.length && this._addAttachClasses.splice(0, this._addAttachClasses.length), "undefined" == typeof this._addAttachClasses && (this._addAttachClasses = []);
        var i = this._addAttachClasses;
        t.top && i.push(this.getClass("element-attached") + "-" + t.top), t.left && i.push(this.getClass("element-attached") + "-" + t.left), e.top && i.push(this.getClass("target-attached") + "-" + e.top), e.left && i.push(this.getClass("target-attached") + "-" + e.left);
        var r = [];
        n.forEach(function (t) {
          r.push(o.getClass("element-attached") + "-" + t), r.push(o.getClass("target-attached") + "-" + t);
        }), M(function () {
          "undefined" != typeof o._addAttachClasses && (m(o.element, o._addAttachClasses, r), o.options.addTargetClasses !== !1 && m(o.target, o._addAttachClasses, r), delete o._addAttachClasses);
        });
      }
    }, {
      key: "position",
      value: function value() {
        var t = this,
            e = arguments.length <= 0 || void 0 === arguments[0] ? !0 : arguments[0];

        if (this.enabled) {
          this.clearCache();
          var o = N(this.targetAttachment, this.attachment);
          this.updateAttachClasses(this.attachment, o);
          var n = this.cache("element-bounds", function () {
            return a(t.element);
          }),
              i = n.width,
              r = n.height;

          if (0 === i && 0 === r && "undefined" != typeof this.lastSize) {
            var s = this.lastSize;
            i = s.width, r = s.height;
          } else this.lastSize = {
            width: i,
            height: r
          };

          var h = this.cache("target-bounds", function () {
            return t.getTargetBounds();
          }),
              u = h,
              d = C(U(this.attachment), {
            width: i,
            height: r
          }),
              p = C(U(o), u),
              c = C(this.offset, {
            width: i,
            height: r
          }),
              g = C(this.targetOffset, u);
          d = w(d, c), p = w(p, g);

          for (var m = h.left + p.left - d.left, v = h.top + p.top - d.top, y = 0; y < x.modules.length; ++y) {
            var b = x.modules[y],
                O = b.position.call(this, {
              left: m,
              top: v,
              targetAttachment: o,
              targetPos: h,
              elementPos: n,
              offset: d,
              targetOffset: p,
              manualOffset: c,
              manualTargetOffset: g,
              scrollbarSize: S,
              attachment: this.attachment
            });
            if (O === !1) return !1;
            "undefined" != typeof O && "object" == _typeof(O) && (v = O.top, m = O.left);
          }

          var E = {
            page: {
              top: v,
              left: m
            },
            viewport: {
              top: v - pageYOffset,
              bottom: pageYOffset - v - r + innerHeight,
              left: m - pageXOffset,
              right: pageXOffset - m - i + innerWidth
            }
          },
              A = this.target.ownerDocument,
              T = A.defaultView,
              S = void 0;
          return A.body.scrollWidth > T.innerWidth && (S = this.cache("scrollbar-size", l), E.viewport.bottom -= S.height), A.body.scrollHeight > T.innerHeight && (S = this.cache("scrollbar-size", l), E.viewport.right -= S.width), (-1 === ["", "static"].indexOf(A.body.style.position) || -1 === ["", "static"].indexOf(A.body.parentElement.style.position)) && (E.page.bottom = A.body.scrollHeight - v - r, E.page.right = A.body.scrollWidth - m - i), "undefined" != typeof this.options.optimizations && this.options.optimizations.moveElement !== !1 && "undefined" == typeof this.targetModifier && !function () {
            var e = t.cache("target-offsetparent", function () {
              return f(t.target);
            }),
                o = t.cache("target-offsetparent-bounds", function () {
              return a(e);
            }),
                n = getComputedStyle(e),
                i = o,
                r = {};

            if (["Top", "Left", "Bottom", "Right"].forEach(function (t) {
              r[t.toLowerCase()] = parseFloat(n["border" + t + "Width"]);
            }), o.right = A.body.scrollWidth - o.left - i.width + r.right, o.bottom = A.body.scrollHeight - o.top - i.height + r.bottom, E.page.top >= o.top + r.top && E.page.bottom >= o.bottom && E.page.left >= o.left + r.left && E.page.right >= o.right) {
              var s = e.scrollTop,
                  l = e.scrollLeft;
              E.offset = {
                top: E.page.top - o.top + s - r.top,
                left: E.page.left - o.left + l - r.left
              };
            }
          }(), this.move(E), this.history.unshift(E), this.history.length > 3 && this.history.pop(), e && _(), !0;
        }
      }
    }, {
      key: "move",
      value: function value(t) {
        var e = this;

        if ("undefined" != typeof this.element.parentNode) {
          var o = {};

          for (var n in t) {
            o[n] = {};

            for (var i in t[n]) {
              for (var r = !1, s = 0; s < this.history.length; ++s) {
                var a = this.history[s];

                if ("undefined" != typeof a[n] && !y(a[n][i], t[n][i])) {
                  r = !0;
                  break;
                }
              }

              r || (o[n][i] = !0);
            }
          }

          var l = {
            top: "",
            left: "",
            right: "",
            bottom: ""
          },
              u = function u(t, o) {
            var n = "undefined" != typeof e.options.optimizations,
                i = n ? e.options.optimizations.gpu : null;

            if (i !== !1) {
              var r = void 0,
                  s = void 0;
              t.top ? (l.top = 0, r = o.top) : (l.bottom = 0, r = -o.bottom), t.left ? (l.left = 0, s = o.left) : (l.right = 0, s = -o.right), l[Y] = "translateX(" + Math.round(s) + "px) translateY(" + Math.round(r) + "px)", "msTransform" !== Y && (l[Y] += " translateZ(0)");
            } else t.top ? l.top = o.top + "px" : l.bottom = o.bottom + "px", t.left ? l.left = o.left + "px" : l.right = o.right + "px";
          },
              d = !1;

          if ((o.page.top || o.page.bottom) && (o.page.left || o.page.right) ? (l.position = "absolute", u(o.page, t.page)) : (o.viewport.top || o.viewport.bottom) && (o.viewport.left || o.viewport.right) ? (l.position = "fixed", u(o.viewport, t.viewport)) : "undefined" != typeof o.offset && o.offset.top && o.offset.left ? !function () {
            l.position = "absolute";
            var n = e.cache("target-offsetparent", function () {
              return f(e.target);
            });
            f(e.element) !== n && M(function () {
              e.element.parentNode.removeChild(e.element), n.appendChild(e.element);
            }), u(o.offset, t.offset), d = !0;
          }() : (l.position = "absolute", u({
            top: !0,
            left: !0
          }, t.page)), !d) {
            for (var p = !0, c = this.element.parentNode; c && 1 === c.nodeType && "BODY" !== c.tagName;) {
              if ("static" !== getComputedStyle(c).position) {
                p = !1;
                break;
              }

              c = c.parentNode;
            }

            p || (this.element.parentNode.removeChild(this.element), this.element.ownerDocument.body.appendChild(this.element));
          }

          var g = {},
              m = !1;

          for (var i in l) {
            var v = l[i],
                b = this.element.style[i];
            b !== v && (m = !0, g[i] = v);
          }

          m && M(function () {
            h(e.element.style, g);
          });
        }
      }
    }]), e;
  }(k);

  q.modules = [], x.position = D;

  var I = h(q, x),
      B = function () {
    function t(t, e) {
      var o = [],
          n = !0,
          i = !1,
          r = void 0;

      try {
        for (var s, a = t[Symbol.iterator](); !(n = (s = a.next()).done) && (o.push(s.value), !e || o.length !== e); n = !0) {
          ;
        }
      } catch (f) {
        i = !0, r = f;
      } finally {
        try {
          !n && a["return"] && a["return"]();
        } finally {
          if (i) throw r;
        }
      }

      return o;
    }

    return function (e, o) {
      if (Array.isArray(e)) return e;
      if (Symbol.iterator in Object(e)) return t(e, o);
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    };
  }(),
      j = x.Utils,
      a = j.getBounds,
      h = j.extend,
      m = j.updateClasses,
      M = j.defer,
      $ = ["left", "top", "right", "bottom"];

  x.modules.push({
    position: function position(t) {
      var e = this,
          o = t.top,
          n = t.left,
          i = t.targetAttachment;
      if (!this.options.constraints) return !0;
      var r = this.cache("element-bounds", function () {
        return a(e.element);
      }),
          s = r.height,
          f = r.width;

      if (0 === f && 0 === s && "undefined" != typeof this.lastSize) {
        var l = this.lastSize;
        f = l.width, s = l.height;
      }

      var u = this.cache("target-bounds", function () {
        return e.getTargetBounds();
      }),
          d = u.height,
          p = u.width,
          c = [this.getClass("pinned"), this.getClass("out-of-bounds")];
      this.options.constraints.forEach(function (t) {
        var e = t.outOfBoundsClass,
            o = t.pinnedClass;
        e && c.push(e), o && c.push(o);
      }), c.forEach(function (t) {
        ["left", "top", "right", "bottom"].forEach(function (e) {
          c.push(t + "-" + e);
        });
      });
      var g = [],
          v = h({}, i),
          y = h({}, this.attachment);
      return this.options.constraints.forEach(function (t) {
        var r = t.to,
            a = t.attachment,
            l = t.pin;
        "undefined" == typeof a && (a = "");
        var h = void 0,
            u = void 0;

        if (a.indexOf(" ") >= 0) {
          var c = a.split(" "),
              m = B(c, 2);
          u = m[0], h = m[1];
        } else h = u = a;

        var b = O(e, r);
        ("target" === u || "both" === u) && (o < b[1] && "top" === v.top && (o += d, v.top = "bottom"), o + s > b[3] && "bottom" === v.top && (o -= d, v.top = "top")), "together" === u && ("top" === v.top && ("bottom" === y.top && o < b[1] ? (o += d, v.top = "bottom", o += s, y.top = "top") : "top" === y.top && o + s > b[3] && o - (s - d) >= b[1] && (o -= s - d, v.top = "bottom", y.top = "bottom")), "bottom" === v.top && ("top" === y.top && o + s > b[3] ? (o -= d, v.top = "top", o -= s, y.top = "bottom") : "bottom" === y.top && o < b[1] && o + (2 * s - d) <= b[3] && (o += s - d, v.top = "top", y.top = "top")), "middle" === v.top && (o + s > b[3] && "top" === y.top ? (o -= s, y.top = "bottom") : o < b[1] && "bottom" === y.top && (o += s, y.top = "top"))), ("target" === h || "both" === h) && (n < b[0] && "left" === v.left && (n += p, v.left = "right"), n + f > b[2] && "right" === v.left && (n -= p, v.left = "left")), "together" === h && (n < b[0] && "left" === v.left ? "right" === y.left ? (n += p, v.left = "right", n += f, y.left = "left") : "left" === y.left && (n += p, v.left = "right", n -= f, y.left = "right") : n + f > b[2] && "right" === v.left ? "left" === y.left ? (n -= p, v.left = "left", n -= f, y.left = "right") : "right" === y.left && (n -= p, v.left = "left", n += f, y.left = "left") : "center" === v.left && (n + f > b[2] && "left" === y.left ? (n -= f, y.left = "right") : n < b[0] && "right" === y.left && (n += f, y.left = "left"))), ("element" === u || "both" === u) && (o < b[1] && "bottom" === y.top && (o += s, y.top = "top"), o + s > b[3] && "top" === y.top && (o -= s, y.top = "bottom")), ("element" === h || "both" === h) && (n < b[0] && ("right" === y.left ? (n += f, y.left = "left") : "center" === y.left && (n += f / 2, y.left = "left")), n + f > b[2] && ("left" === y.left ? (n -= f, y.left = "right") : "center" === y.left && (n -= f / 2, y.left = "right"))), "string" == typeof l ? l = l.split(",").map(function (t) {
          return t.trim();
        }) : l === !0 && (l = ["top", "left", "right", "bottom"]), l = l || [];
        var w = [],
            C = [];
        o < b[1] && (l.indexOf("top") >= 0 ? (o = b[1], w.push("top")) : C.push("top")), o + s > b[3] && (l.indexOf("bottom") >= 0 ? (o = b[3] - s, w.push("bottom")) : C.push("bottom")), n < b[0] && (l.indexOf("left") >= 0 ? (n = b[0], w.push("left")) : C.push("left")), n + f > b[2] && (l.indexOf("right") >= 0 ? (n = b[2] - f, w.push("right")) : C.push("right")), w.length && !function () {
          var t = void 0;
          t = "undefined" != typeof e.options.pinnedClass ? e.options.pinnedClass : e.getClass("pinned"), g.push(t), w.forEach(function (e) {
            g.push(t + "-" + e);
          });
        }(), C.length && !function () {
          var t = void 0;
          t = "undefined" != typeof e.options.outOfBoundsClass ? e.options.outOfBoundsClass : e.getClass("out-of-bounds"), g.push(t), C.forEach(function (e) {
            g.push(t + "-" + e);
          });
        }(), (w.indexOf("left") >= 0 || w.indexOf("right") >= 0) && (y.left = v.left = !1), (w.indexOf("top") >= 0 || w.indexOf("bottom") >= 0) && (y.top = v.top = !1), (v.top !== i.top || v.left !== i.left || y.top !== e.attachment.top || y.left !== e.attachment.left) && (e.updateAttachClasses(y, v), e.trigger("update", {
          attachment: y,
          targetAttachment: v
        }));
      }), M(function () {
        e.options.addTargetClasses !== !1 && m(e.target, g, c), m(e.element, g, c);
      }), {
        top: o,
        left: n
      };
    }
  });
  var j = x.Utils,
      a = j.getBounds,
      m = j.updateClasses,
      M = j.defer;
  x.modules.push({
    position: function position(t) {
      var e = this,
          o = t.top,
          n = t.left,
          i = this.cache("element-bounds", function () {
        return a(e.element);
      }),
          r = i.height,
          s = i.width,
          f = this.getTargetBounds(),
          l = o + r,
          h = n + s,
          u = [];
      o <= f.bottom && l >= f.top && ["left", "right"].forEach(function (t) {
        var e = f[t];
        (e === n || e === h) && u.push(t);
      }), n <= f.right && h >= f.left && ["top", "bottom"].forEach(function (t) {
        var e = f[t];
        (e === o || e === l) && u.push(t);
      });
      var d = [],
          p = [],
          c = ["left", "top", "right", "bottom"];
      return d.push(this.getClass("abutted")), c.forEach(function (t) {
        d.push(e.getClass("abutted") + "-" + t);
      }), u.length && p.push(this.getClass("abutted")), u.forEach(function (t) {
        p.push(e.getClass("abutted") + "-" + t);
      }), M(function () {
        e.options.addTargetClasses !== !1 && m(e.target, p, d), m(e.element, p, d);
      }), !0;
    }
  });

  var B = function () {
    function t(t, e) {
      var o = [],
          n = !0,
          i = !1,
          r = void 0;

      try {
        for (var s, a = t[Symbol.iterator](); !(n = (s = a.next()).done) && (o.push(s.value), !e || o.length !== e); n = !0) {
          ;
        }
      } catch (f) {
        i = !0, r = f;
      } finally {
        try {
          !n && a["return"] && a["return"]();
        } finally {
          if (i) throw r;
        }
      }

      return o;
    }

    return function (e, o) {
      if (Array.isArray(e)) return e;
      if (Symbol.iterator in Object(e)) return t(e, o);
      throw new TypeError("Invalid attempt to destructure non-iterable instance");
    };
  }();

  return x.modules.push({
    position: function position(t) {
      var e = t.top,
          o = t.left;

      if (this.options.shift) {
        var n = this.options.shift;
        "function" == typeof this.options.shift && (n = this.options.shift.call(this, {
          top: e,
          left: o
        }));
        var i = void 0,
            r = void 0;

        if ("string" == typeof n) {
          n = n.split(" "), n[1] = n[1] || n[0];
          var s = n,
              a = B(s, 2);
          i = a[0], r = a[1], i = parseFloat(i, 10), r = parseFloat(r, 10);
        } else i = n.top, r = n.left;

        return e += i, o += r, {
          top: e,
          left: o
        };
      }
    }
  }), I;
});

/***/ }),

/***/ "./assets/backend/dashboard/js/tools/ajaxModal.js":
/*!********************************************************!*\
  !*** ./assets/backend/dashboard/js/tools/ajaxModal.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");

!(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
  return {
    init: function init() {
      $(document).on('show.bs.modal', '.js-ajax-modal', this.initModals);
    },
    initModals: function initModals(e) {
      var $modal = $(this);

      if (!$modal.data('loaded')) {
        var $btn = $(e.relatedTarget),
            link = $btn.data('link');
        $modal.data('loaded', true);
        $modal.find('.js-ajax-modal-body').append('<iframe class="ajax-modal__body__iframe" frameborder="0" src="' + link + '" width="100%" height="100%" scrolling="auto"></iframe>');
      }
    },
    resetAjaxModals: function resetAjaxModals() {
      $('.js-ajax-modal').off('show.bs.modal', this.initModals);
      $('.js-ajax-modal').on('show.bs.modal', this.initModals);
    }
  };
}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),

/***/ "./assets/backend/dashboard/js/tools/cargobay.general.min.js":
/*!*******************************************************************!*\
  !*** ./assets/backend/dashboard/js/tools/cargobay.general.min.js ***!
  \*******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/web.timers */ "./node_modules/core-js/modules/web.timers.js");

!(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
  return function (n) {
    var a = this.general;
    a.debounce = function (n, a, e) {
      var t;
      return function () {
        var r = this,
            u = arguments,
            i = function i() {
          t = null, e || n.apply(r, u);
        },
            o = e && !t;

        clearTimeout(t), t = setTimeout(i, a), o && n.apply(r, u);
      };
    }, a.easeInOutQuad = function (n, a, e, t) {
      return n /= t / 2, 1 > n ? e / 2 * n * n + a : (n--, -e / 2 * (n * (n - 2) - 1) + a);
    }, a.animate = function (n, a, e, t, r, u) {
      n && (window.requestAnimationFrame(function () {
        n.style[a] = r;
      }, u), n.style[a] = t);
    };
  };
}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),

/***/ "./assets/backend/dashboard/js/tools/cargobay.toogle.min.js":
/*!******************************************************************!*\
  !*** ./assets/backend/dashboard/js/tools/cargobay.toogle.min.js ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.array.for-each */ "./node_modules/core-js/modules/es.array.for-each.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.string.split */ "./node_modules/core-js/modules/es.string.split.js");

!(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
  return function (t, e) {
    var a,
        o,
        n,
        c,
        i,
        l,
        r = 150,
        u = 0,
        s = "js-toggle-btn",
        g = "toggle-btn--active",
        d = "toggle-item--active",
        f = "toggle-item__content";
    o = function o(t, e, a) {
      for (var o = e.split(" "), n = 0, c = o.length; c > n; n++) {
        t.addEventListener(o[n], a, !1);
      }
    }, n = function n() {
      [].forEach.call(document.querySelectorAll("." + s), function (t) {
        o(t, "click touchstart mousedown", function (t) {
          t.preventDefault();
        }), o(t, "touchend mouseup", function (e) {
          var a = document.querySelectorAll(t.getAttribute("data-target"))[0],
              o = a.querySelectorAll("." + f)[0],
              n = o.offsetHeight,
              v = a.classList.contains(d),
              b = t.getAttribute("data-hide-others");
          if (u = null !== t.getAttribute("data-duration") ? t.getAttribute("data-duration") : r, v) i(t, a);else {
            if (b) {
              var h = t.getAttribute("data-target"),
                  m = t.getAttribute("data-level");
              [].forEach.call(document.querySelectorAll("." + s + '[data-level="' + m + '"]'), function (t) {
                var e = t.getAttribute("data-target");
                h !== e && t.classList.contains(g) && l(t, document.querySelectorAll(e)[0]);
              });
            }

            c(t, a, o, n);
          }
        });
      });
    }, c = function c(t, e, a, o) {
      cargobay.general.animate(e, "height", "px", o, "auto", u), t.classList.add(g), e.classList.add(d);
    }, i = function i(t, e, a, o) {
      cargobay.general.animate(e, "height", "px", 0, o, u), t.classList.remove(g), e.classList.remove(d);
    }, l = function l(t, e) {
      t.classList.remove(g), e.style.height = 0, e.classList.remove(d);
    }, a = function () {
      n();
    }();
  };
}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),

/***/ "./assets/backend/dashboard/js/tools/mediaChooser.js":
/*!***********************************************************!*\
  !*** ./assets/backend/dashboard/js/tools/mediaChooser.js ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_RESULT__;__webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");

__webpack_require__(/*! core-js/modules/es.regexp.constructor */ "./node_modules/core-js/modules/es.regexp.constructor.js");

__webpack_require__(/*! core-js/modules/es.regexp.exec */ "./node_modules/core-js/modules/es.regexp.exec.js");

__webpack_require__(/*! core-js/modules/es.regexp.to-string */ "./node_modules/core-js/modules/es.regexp.to-string.js");

__webpack_require__(/*! core-js/modules/es.string.match */ "./node_modules/core-js/modules/es.string.match.js");

__webpack_require__(/*! core-js/modules/es.string.search */ "./node_modules/core-js/modules/es.string.search.js");

!(__WEBPACK_AMD_DEFINE_RESULT__ = (function () {
  return {
    init: function init() {
      this.urlChooser();
      this.initDelBtn();
    },
    // Del btn
    initDelBtn: function initDelBtn() {
      $('body').on('click', '.js-media-chooser-del-preview-btn', function (e) {
        var $this = $(this),
            linkedID = $this.data('linked-id'),
            $widget = $('#' + linkedID + '-widget'),
            $input = $('#' + linkedID);
        $this.parent().find('.media-chooser__preview').remove();
        $(".media-thumbnail__icon").remove();
        $widget.removeClass('media-chooser--choosen');
        $input.val('');
      });
    },
    // URL-Chooser
    urlChooser: function urlChooser() {
      // Link Chooser select
      $('body').on('click', '.js-url-chooser-link-select', function (e) {
        e.preventDefault();
        var $this = $(this),
            slug = $this.data('slug'),
            id = $this.data('id'),
            replaceUrl = $this.closest('nav').data('replace-url'); // Store values

        itemUrl = slug ? slug : '';
        itemId = id; // Replace URL

        $.ajax({
          url: replaceUrl,
          type: 'GET',
          data: {
            'text': itemUrl
          },
          success: function success(response) {
            replacedUrl = response.text; // Update preview

            $('#url-chooser__selection-preview').text('Selection: ' + replacedUrl);
          }
        });
      }); // Media Chooser select

      $('body').on('click', '.js-url-chooser-media-select', function (e) {
        e.preventDefault();
        var $this = $(this),
            path = $this.data('path'),
            thumbPath = $this.data('thumb-path'),
            id = $this.data('id'),
            title = $this.data('title'),
            cke = $this.data('cke'),
            replaceUrl = $this.closest('.thumbnail-wrapper').data('replace-url'); // Store values

        itemUrl = path;
        itemId = id;
        itemTitle = title;
        itemThumbPath = thumbPath; // Save

        if (!cke) {
          var isMediaChooser = $(window.frameElement).closest('.js-ajax-modal').data('media-chooser');

          if (isMediaChooser) {
            psc.mediaChooser.saveMediaChooserModal(false);
          } else {
            // Replace URL
            $.ajax({
              url: replaceUrl,
              type: 'GET',
              data: {
                'text': itemUrl
              },
              success: function success(response) {
                replacedUrl = response.text;
              }
            }).done(function () {
              psc.mediaChooser.saveUrlChooserModal(false);
            });
          }
        } else {
          psc.mediaChooser.saveMediaChooserModal(true);
        }
      }); // Cancel

      $('#cancel-url-chooser-modal').on('click', function () {
        var cke = $(this).data('cke');

        if (!cke) {
          var $parentModal = $(window.frameElement).closest('.js-ajax-modal'),
              parentModalId = $parentModal.attr('id');
          parent.$('#' + parentModalId).modal('hide');
        } else {
          window.close();
        }
      }); // OK

      $(document).on('click', '#save-url-chooser-modal', function () {
        var cke = $(this).data('cke');
        psc.mediaChooser.saveUrlChooserModal(cke);
      });
    },
    // Save for URL-chooser
    saveUrlChooserModal: function saveUrlChooserModal(cke) {
      if (!cke) {
        var $parentModal = $(window.frameElement).closest('.js-ajax-modal'),
            linkedInputId = $parentModal.data('linked-input-id'),
            parentModalId = $parentModal.attr('id'); // Set val

        parent.$('#' + linkedInputId).val(itemUrl).change(); // Set proper URL

        parent.$('#' + linkedInputId).parent().find('.js-urlchooser-value').val(replacedUrl); // Close modal

        parent.$('#' + parentModalId).modal('hide');
      } else {
        var funcNum = getUrlParam('CKEditorFuncNum'); // Set val

        window.opener.CKEDITOR.tools.callFunction(funcNum, itemUrl); // Close window

        window.close();
      }
    },
    // Save for Media-chooser
    saveMediaChooserModal: function saveMediaChooserModal(cke) {
      if (!cke) {
        var $parentModal = $(window.frameElement).closest('.js-ajax-modal'),
            linkedInputId = $parentModal.data('linked-input-id'),
            parentModalId = $parentModal.attr('id'); // Set val

        parent.$('#' + linkedInputId).val(itemId).change(); // Update preview

        var $mediaChooser = parent.$('#' + linkedInputId + '-widget'),
            $previewImg = parent.$('#' + linkedInputId + '__preview__img'),
            $previewTitle = parent.$('#' + linkedInputId + '__preview__title');
        $mediaChooser.addClass('media-chooser--choosen');
        $previewTitle.html(itemTitle);

        if (itemThumbPath === "") {
          var $parent = $previewTitle.parent();
          $parent.prepend('<i class="fa fa-file-o media-thumbnail__icon"></i>');
        } else {
          $previewImg.attr('src', itemThumbPath);
        } // Close modal


        parent.$('#' + parentModalId).modal('hide');
      } else {
        var funcNum = getUrlParam('CKEditorFuncNum'); // Set val

        window.opener.CKEDITOR.tools.callFunction(funcNum, itemUrl); // Close window

        window.close();
      }
    },
    // Get Url Parameters
    getUrlParam: function getUrlParam(paramName) {
      var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i'),
          match = window.location.search.match(reParam);
      return match && match.length > 1 ? match[1] : '';
    }
  };
}).call(exports, __webpack_require__, exports, module),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));

/***/ }),

/***/ "./assets/backend/dashboard/less/base.less":
/*!*************************************************!*\
  !*** ./assets/backend/dashboard/less/base.less ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ })

},[["./assets/backend/dashboard/dashboard.js","runtime","vendors~backend/dashboard~plugins/system/psc/invoice","vendors~backend/dashboard"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvYmFja2VuZC9kYXNoYm9hcmQvY3NzL3Rvb2xzL2NhcmdvYmF5LnRvb2dsZS5taW4uY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9iYWNrZW5kL2Rhc2hib2FyZC9kYXNoYm9hcmQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2JhY2tlbmQvZGFzaGJvYXJkL2pzL2FwcC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvYmFja2VuZC9kYXNoYm9hcmQvanMvZHJvcHpvbmUuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2JhY2tlbmQvZGFzaGJvYXJkL2pzL29yZGVyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9iYWNrZW5kL2Rhc2hib2FyZC9qcy9zdW1tZXJub3RlL21lZGlhYnVuZGxlLnBsdWdpbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvYmFja2VuZC9kYXNoYm9hcmQvanMvdGV0aGVyLm1pbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvYmFja2VuZC9kYXNoYm9hcmQvanMvdG9vbHMvYWpheE1vZGFsLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9iYWNrZW5kL2Rhc2hib2FyZC9qcy90b29scy9jYXJnb2JheS5nZW5lcmFsLm1pbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvYmFja2VuZC9kYXNoYm9hcmQvanMvdG9vbHMvY2FyZ29iYXkudG9vZ2xlLm1pbi5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvYmFja2VuZC9kYXNoYm9hcmQvanMvdG9vbHMvbWVkaWFDaG9vc2VyLmpzIiwid2VicGFjazovLy8uL2Fzc2V0cy9iYWNrZW5kL2Rhc2hib2FyZC9sZXNzL2Jhc2UubGVzcz84MWQ4Il0sIm5hbWVzIjpbImdsb2JhbCIsIiQiLCJqUXVlcnkiLCJ3aW5kb3ciLCJyZXF1aXJlIiwiUmFwaGFlbCIsIkRyb3B6b25lIiwiY2FyZ29iYXkiLCJnZW5lcmFsIiwidG9nZ2xlIiwicHNjIiwiYWpheE1vZGFsIiwibWVkaWFDaG9vc2VyIiwiYXBwIiwib3JkZXIiLCJpbml0IiwiZGVmaW5lIiwiYm9vdHN0cmFwQXBwIiwib24iLCJlIiwidGFyZ2V0IiwicGFyZW50IiwiYXR0ciIsImxvY2FsU3RvcmFnZSIsInNldEl0ZW0iLCJsYXN0VGFiIiwiZ2V0SXRlbSIsInRhYiIsImxhc3RUYWJEb2MiLCJmaW5kIiwiY2xpY2siLCJjaGFuZ2UiLCJnZXRKU09OIiwiZGF0YSIsIm11bHRpc2VsZWN0Iiwic3VibWl0QWxsTGVmdCIsInN1Ym1pdEFsbFJpZ2h0Iiwic2VhcmNoIiwibGVmdCIsInJpZ2h0IiwiZmlyZVNlYXJjaCIsInZhbHVlIiwibGVuZ3RoIiwic3VtbWVybm90ZSIsImhlaWdodCIsInRvb2xiYXIiLCJFbWl0dGVyIiwiY2FtZWxpemUiLCJjb250ZW50TG9hZGVkIiwiZGV0ZWN0VmVydGljYWxTcXVhc2giLCJkcmF3SW1hZ2VJT1NGaXgiLCJub29wIiwid2l0aG91dCIsIl9fc2xpY2UiLCJzbGljZSIsIl9faGFzUHJvcCIsImhhc093blByb3BlcnR5IiwiX19leHRlbmRzIiwiY2hpbGQiLCJrZXkiLCJjYWxsIiwiY3RvciIsImNvbnN0cnVjdG9yIiwicHJvdG90eXBlIiwiX19zdXBlcl9fIiwiYWRkRXZlbnRMaXN0ZW5lciIsImV2ZW50IiwiZm4iLCJfY2FsbGJhY2tzIiwicHVzaCIsImVtaXQiLCJhcmdzIiwiY2FsbGJhY2siLCJjYWxsYmFja3MiLCJfaSIsIl9sZW4iLCJhcmd1bWVudHMiLCJhcHBseSIsInJlbW92ZUxpc3RlbmVyIiwib2ZmIiwicmVtb3ZlQWxsTGlzdGVuZXJzIiwicmVtb3ZlRXZlbnRMaXN0ZW5lciIsImkiLCJzcGxpY2UiLCJfc3VwZXIiLCJleHRlbmQiLCJyZXNvbHZlT3B0aW9uIiwiZXZlbnRzIiwiZGVmYXVsdE9wdGlvbnMiLCJ1cmwiLCJtZXRob2QiLCJ3aXRoQ3JlZGVudGlhbHMiLCJwYXJhbGxlbFVwbG9hZHMiLCJ1cGxvYWRNdWx0aXBsZSIsIm1heEZpbGVzaXplIiwicGFyYW1OYW1lIiwiY3JlYXRlSW1hZ2VUaHVtYm5haWxzIiwibWF4VGh1bWJuYWlsRmlsZXNpemUiLCJ0aHVtYm5haWxXaWR0aCIsInRodW1ibmFpbEhlaWdodCIsImZpbGVzaXplQmFzZSIsIm1heEZpbGVzIiwicGFyYW1zIiwiY2xpY2thYmxlIiwiaWdub3JlSGlkZGVuRmlsZXMiLCJhY2NlcHRlZEZpbGVzIiwiYWNjZXB0ZWRNaW1lVHlwZXMiLCJhdXRvUHJvY2Vzc1F1ZXVlIiwiYXV0b1F1ZXVlIiwiYWRkUmVtb3ZlTGlua3MiLCJwcmV2aWV3c0NvbnRhaW5lciIsImhpZGRlbklucHV0Q29udGFpbmVyIiwiY2FwdHVyZSIsImRpY3REZWZhdWx0TWVzc2FnZSIsImRpY3RGYWxsYmFja01lc3NhZ2UiLCJkaWN0RmFsbGJhY2tUZXh0IiwiZGljdEZpbGVUb29CaWciLCJkaWN0SW52YWxpZEZpbGVUeXBlIiwiZGljdFJlc3BvbnNlRXJyb3IiLCJkaWN0Q2FuY2VsVXBsb2FkIiwiZGljdENhbmNlbFVwbG9hZENvbmZpcm1hdGlvbiIsImRpY3RSZW1vdmVGaWxlIiwiZGljdFJlbW92ZUZpbGVDb25maXJtYXRpb24iLCJkaWN0TWF4RmlsZXNFeGNlZWRlZCIsImFjY2VwdCIsImZpbGUiLCJkb25lIiwiZm9yY2VGYWxsYmFjayIsImZhbGxiYWNrIiwibWVzc2FnZUVsZW1lbnQiLCJzcGFuIiwiX3JlZiIsImVsZW1lbnQiLCJjbGFzc05hbWUiLCJnZXRFbGVtZW50c0J5VGFnTmFtZSIsInRlc3QiLCJjcmVhdGVFbGVtZW50IiwiYXBwZW5kQ2hpbGQiLCJ0ZXh0Q29udGVudCIsIm9wdGlvbnMiLCJpbm5lclRleHQiLCJnZXRGYWxsYmFja0Zvcm0iLCJyZXNpemUiLCJpbmZvIiwic3JjUmF0aW8iLCJ0cmdSYXRpbyIsInNyY1giLCJzcmNZIiwic3JjV2lkdGgiLCJ3aWR0aCIsInNyY0hlaWdodCIsIm9wdFdpZHRoIiwib3B0SGVpZ2h0IiwidHJnSGVpZ2h0IiwidHJnV2lkdGgiLCJkcm9wIiwiY2xhc3NMaXN0IiwicmVtb3ZlIiwiZHJhZ3N0YXJ0IiwiZHJhZ2VuZCIsImRyYWdlbnRlciIsImFkZCIsImRyYWdvdmVyIiwiZHJhZ2xlYXZlIiwicGFzdGUiLCJyZXNldCIsImFkZGVkZmlsZSIsIm5vZGUiLCJyZW1vdmVGaWxlRXZlbnQiLCJyZW1vdmVMaW5rIiwiX2oiLCJfayIsIl9sZW4xIiwiX2xlbjIiLCJfcmVmMSIsIl9yZWYyIiwiX3Jlc3VsdHMiLCJwcmV2aWV3RWxlbWVudCIsInByZXZpZXdUZW1wbGF0ZSIsInRyaW0iLCJxdWVyeVNlbGVjdG9yQWxsIiwibmFtZSIsImlubmVySFRNTCIsImZpbGVzaXplIiwic2l6ZSIsIl9yZW1vdmVMaW5rIiwiX3RoaXMiLCJwcmV2ZW50RGVmYXVsdCIsInN0b3BQcm9wYWdhdGlvbiIsInN0YXR1cyIsIlVQTE9BRElORyIsImNvbmZpcm0iLCJyZW1vdmVGaWxlIiwicmVtb3ZlZGZpbGUiLCJwYXJlbnROb2RlIiwicmVtb3ZlQ2hpbGQiLCJfdXBkYXRlTWF4RmlsZXNSZWFjaGVkQ2xhc3MiLCJ0aHVtYm5haWwiLCJkYXRhVXJsIiwidGh1bWJuYWlsRWxlbWVudCIsImFsdCIsInNyYyIsInNldFRpbWVvdXQiLCJlcnJvciIsIm1lc3NhZ2UiLCJlcnJvcm11bHRpcGxlIiwicHJvY2Vzc2luZyIsInByb2Nlc3NpbmdtdWx0aXBsZSIsInVwbG9hZHByb2dyZXNzIiwicHJvZ3Jlc3MiLCJieXRlc1NlbnQiLCJub2RlTmFtZSIsInN0eWxlIiwidG90YWx1cGxvYWRwcm9ncmVzcyIsInNlbmRpbmciLCJzZW5kaW5nbXVsdGlwbGUiLCJzdWNjZXNzIiwic3VjY2Vzc211bHRpcGxlIiwiY2FuY2VsZWQiLCJjYW5jZWxlZG11bHRpcGxlIiwiY29tcGxldGUiLCJjb21wbGV0ZW11bHRpcGxlIiwibWF4ZmlsZXNleGNlZWRlZCIsIm1heGZpbGVzcmVhY2hlZCIsInF1ZXVlY29tcGxldGUiLCJhZGRlZGZpbGVzIiwib2JqZWN0Iiwib2JqZWN0cyIsInZhbCIsImVsZW1lbnRPcHRpb25zIiwidmVyc2lvbiIsInJlcGxhY2UiLCJjbGlja2FibGVFbGVtZW50cyIsImxpc3RlbmVycyIsImZpbGVzIiwiZG9jdW1lbnQiLCJxdWVyeVNlbGVjdG9yIiwibm9kZVR5cGUiLCJFcnJvciIsImRyb3B6b25lIiwiaW5zdGFuY2VzIiwib3B0aW9uc0ZvckVsZW1lbnQiLCJpc0Jyb3dzZXJTdXBwb3J0ZWQiLCJnZXRBdHRyaWJ1dGUiLCJ0b1VwcGVyQ2FzZSIsImdldEV4aXN0aW5nRmFsbGJhY2siLCJnZXRFbGVtZW50IiwiZ2V0RWxlbWVudHMiLCJnZXRBY2NlcHRlZEZpbGVzIiwiYWNjZXB0ZWQiLCJnZXRSZWplY3RlZEZpbGVzIiwiZ2V0RmlsZXNXaXRoU3RhdHVzIiwiZ2V0UXVldWVkRmlsZXMiLCJRVUVVRUQiLCJnZXRVcGxvYWRpbmdGaWxlcyIsImdldEFkZGVkRmlsZXMiLCJBRERFRCIsImdldEFjdGl2ZUZpbGVzIiwiZXZlbnROYW1lIiwibm9Qcm9wYWdhdGlvbiIsInNldHVwSGlkZGVuRmlsZUlucHV0IiwidGFnTmFtZSIsInNldEF0dHJpYnV0ZSIsImNvbnRhaW5zIiwiaGlkZGVuRmlsZUlucHV0IiwidmlzaWJpbGl0eSIsInBvc2l0aW9uIiwidG9wIiwiaWQiLCJhZGRGaWxlIiwiVVJMIiwid2Via2l0VVJMIiwidXBkYXRlVG90YWxVcGxvYWRQcm9ncmVzcyIsInJldHVyblZhbHVlIiwiZWZjdCIsImRhdGFUcmFuc2ZlciIsImVmZmVjdEFsbG93ZWQiLCJfZXJyb3IiLCJkcm9wRWZmZWN0IiwiZm9yRWFjaCIsImNsaWNrYWJsZUVsZW1lbnQiLCJldnQiLCJlbGVtZW50SW5zaWRlIiwiZW5hYmxlIiwiZGVzdHJveSIsImRpc2FibGUiLCJyZW1vdmVBbGxGaWxlcyIsImluZGV4T2YiLCJhY3RpdmVGaWxlcyIsInRvdGFsQnl0ZXMiLCJ0b3RhbEJ5dGVzU2VudCIsInRvdGFsVXBsb2FkUHJvZ3Jlc3MiLCJ1cGxvYWQiLCJ0b3RhbCIsIl9nZXRQYXJhbU5hbWUiLCJuIiwiZXhpc3RpbmdGYWxsYmFjayIsImZpZWxkcyIsImZpZWxkc1N0cmluZyIsImZvcm0iLCJnZXRGYWxsYmFjayIsImVsZW1lbnRzIiwiZWwiLCJzZXR1cEV2ZW50TGlzdGVuZXJzIiwiZWxlbWVudExpc3RlbmVycyIsImxpc3RlbmVyIiwiX3Jlc3VsdHMxIiwicmVtb3ZlRXZlbnRMaXN0ZW5lcnMiLCJjYW5jZWxVcGxvYWQiLCJjdXRvZmYiLCJzZWxlY3RlZFNpemUiLCJzZWxlY3RlZFVuaXQiLCJ1bml0IiwidW5pdHMiLCJNYXRoIiwicG93Iiwicm91bmQiLCJpdGVtcyIsIndlYmtpdEdldEFzRW50cnkiLCJfYWRkRmlsZXNGcm9tSXRlbXMiLCJoYW5kbGVGaWxlcyIsImNsaXBib2FyZERhdGEiLCJlbnRyeSIsIml0ZW0iLCJpc0ZpbGUiLCJnZXRBc0ZpbGUiLCJpc0RpcmVjdG9yeSIsIl9hZGRGaWxlc0Zyb21EaXJlY3RvcnkiLCJraW5kIiwiZGlyZWN0b3J5IiwicGF0aCIsImRpclJlYWRlciIsImVudHJpZXNSZWFkZXIiLCJjcmVhdGVSZWFkZXIiLCJlbnRyaWVzIiwic3Vic3RyaW5nIiwiZnVsbFBhdGgiLCJyZWFkRW50cmllcyIsImNvbnNvbGUiLCJsb2ciLCJpc1ZhbGlkRmlsZSIsIl9lbnF1ZXVlVGh1bWJuYWlsIiwiX2Vycm9yUHJvY2Vzc2luZyIsImVucXVldWVGaWxlIiwiZW5xdWV1ZUZpbGVzIiwicHJvY2Vzc1F1ZXVlIiwiX3RodW1ibmFpbFF1ZXVlIiwiX3Byb2Nlc3NpbmdUaHVtYm5haWwiLCJ0eXBlIiwibWF0Y2giLCJfcHJvY2Vzc1RodW1ibmFpbFF1ZXVlIiwiY3JlYXRlVGh1bWJuYWlsIiwic2hpZnQiLCJjYW5jZWxJZk5lY2Vzc2FyeSIsImZpbGVSZWFkZXIiLCJGaWxlUmVhZGVyIiwib25sb2FkIiwicmVzdWx0IiwiY3JlYXRlVGh1bWJuYWlsRnJvbVVybCIsInJlYWRBc0RhdGFVUkwiLCJpbWFnZVVybCIsImNyb3NzT3JpZ2luIiwiaW1nIiwiY2FudmFzIiwiY3R4IiwicmVzaXplSW5mbyIsIl9yZWYzIiwiZ2V0Q29udGV4dCIsInRyZ1giLCJ0cmdZIiwidG9EYXRhVVJMIiwib25lcnJvciIsInByb2Nlc3NpbmdMZW5ndGgiLCJxdWV1ZWRGaWxlcyIsInByb2Nlc3NGaWxlcyIsInByb2Nlc3NGaWxlIiwidXBsb2FkRmlsZXMiLCJfZ2V0RmlsZXNXaXRoWGhyIiwieGhyIiwiZ3JvdXBlZEZpbGUiLCJncm91cGVkRmlsZXMiLCJDQU5DRUxFRCIsImFib3J0Iiwib3B0aW9uIiwidXBsb2FkRmlsZSIsImZvcm1EYXRhIiwiaGFuZGxlRXJyb3IiLCJoZWFkZXJOYW1lIiwiaGVhZGVyVmFsdWUiLCJoZWFkZXJzIiwiaW5wdXQiLCJpbnB1dE5hbWUiLCJpbnB1dFR5cGUiLCJwcm9ncmVzc09iaiIsInJlc3BvbnNlIiwidXBkYXRlUHJvZ3Jlc3MiLCJfbCIsIl9sZW4zIiwiX20iLCJfcmVmNCIsIl9yZWY1IiwiWE1MSHR0cFJlcXVlc3QiLCJvcGVuIiwiYWxsRmlsZXNGaW5pc2hlZCIsImxvYWRlZCIsInJlYWR5U3RhdGUiLCJyZXNwb25zZVRleHQiLCJnZXRSZXNwb25zZUhlYWRlciIsIkpTT04iLCJwYXJzZSIsIl9maW5pc2hlZCIsIm9ucHJvZ3Jlc3MiLCJzZXRSZXF1ZXN0SGVhZGVyIiwiRm9ybURhdGEiLCJhcHBlbmQiLCJoYXNBdHRyaWJ1dGUiLCJzZWxlY3RlZCIsInRvTG93ZXJDYXNlIiwiY2hlY2tlZCIsInN1Ym1pdFJlcXVlc3QiLCJzZW5kIiwiU1VDQ0VTUyIsIkVSUk9SIiwiZm9yRWxlbWVudCIsImF1dG9EaXNjb3ZlciIsImRpc2NvdmVyIiwiY2hlY2tFbGVtZW50cyIsImRyb3B6b25lcyIsImJsYWNrbGlzdGVkQnJvd3NlcnMiLCJjYXBhYmxlQnJvd3NlciIsInJlZ2V4IiwiRmlsZSIsIkZpbGVMaXN0IiwiQmxvYiIsIm5hdmlnYXRvciIsInVzZXJBZ2VudCIsImxpc3QiLCJyZWplY3RlZEl0ZW0iLCJzdHIiLCJjaGFyQXQiLCJzdHJpbmciLCJkaXYiLCJjaGlsZE5vZGVzIiwiY29udGFpbmVyIiwiZWxzIiwiQXJyYXkiLCJxdWVzdGlvbiIsInJlamVjdGVkIiwiYmFzZU1pbWVUeXBlIiwibWltZVR5cGUiLCJ2YWxpZFR5cGUiLCJzcGxpdCIsImVhY2giLCJtb2R1bGUiLCJleHBvcnRzIiwiQUNDRVBURUQiLCJQUk9DRVNTSU5HIiwiYWxwaGEiLCJleSIsImloIiwiaXciLCJweSIsInJhdGlvIiwic3kiLCJuYXR1cmFsV2lkdGgiLCJuYXR1cmFsSGVpZ2h0IiwiZHJhd0ltYWdlIiwiZ2V0SW1hZ2VEYXRhIiwic3giLCJzdyIsInNoIiwiZHgiLCJkeSIsImR3IiwiZGgiLCJ2ZXJ0U3F1YXNoUmF0aW8iLCJ3aW4iLCJkb2MiLCJwb2xsIiwicHJlIiwicmVtIiwicm9vdCIsImRvY3VtZW50RWxlbWVudCIsImRvU2Nyb2xsIiwiY3JlYXRlRXZlbnRPYmplY3QiLCJmcmFtZUVsZW1lbnQiLCJfYXV0b0Rpc2NvdmVyRnVuY3Rpb24iLCJPcmRlcl9MaXN0X0RldGFpbCIsIm9yZGVyVXVpZCIsInV1aWQiLCJiYXNrZXRGaWVsZDEiLCJjdXN0b21lckluZm8iLCJzcGVjaWFsUHJvZHVjdFR5cGVPYmplY3QiLCJiYXNrZXRGaWVsZDIiLCJwb3MiLCJjb3VudCIsImFsbEdyb3NzIiwicHJvZHVjdCIsImFsbE5ldCIsImdldF9wb3NfYmdfY29sb3IiLCJnZXRfcG9zX2JnX2NvbG9yX2NsYXNzIiwidGl0bGUiLCJuckludGVybiIsImdldF9zcGVjaWFsX3Byb2R1Y3Rfb3B0aW9ucyIsIkludGwiLCJOdW1iZXJGb3JtYXQiLCJjdXJyZW5jeSIsImZvcm1hdCIsImdldF9wb3Nfc3RhdHVzIiwiaW50ZXJuYWxOYW1lIiwiZ2V0X3Bvc19zdGF0dXNfbG9vcCIsIk9yZGVyX0xpc3RfRGV0YWlsX1NpbXBsZSIsIk9yZGVyX0xpc3RfUG9zX1N0YXR1cyIsInBvc1V1aWQiLCJjb2RlIiwiT3JkZXJfTGlzdF9Qb3NfQ2FsY19PcHRpb24iLCJlbG0iLCJsb2FkX3N0YXR1cyIsImJpbmRfYnV0dG9ucyIsInR5cCIsIm1hcCIsImpvaW4iLCJhamF4IiwiY29udGVudFR5cGUiLCJqd3RfdG9rZW4iLCJzdGF0dXNfb3JkZXIiLCJzdGF0dXNfcG9zIiwiaHRtbCIsImxvYWRfZGV0YWlsIiwic2ltcGxlIiwic3RyaW5naWZ5IiwiYnVpbGRfZGV0YWlscyIsInBvc2l0aW9ucyIsIngiLCJiaW5kX3N0YXR1c19idXR0b25zIiwiZm91bmQiLCJzdGF0IiwiY29sb3IiLCJmYWN0b3J5IiwicGx1Z2lucyIsImNvbnRleHQiLCJzZWxmIiwidWkiLCJtZW1vIiwiYnV0dG9uIiwiY29udGVudHMiLCJ0b29sdGlwIiwibWVkaWFCdW5kbGVCcm93c2VyIiwiJG5vdGUiLCIkbWVkaWEiLCJyZW5kZXIiLCJ0IiwibyIsIlR5cGVFcnJvciIsImdldEJvdW5kaW5nQ2xpZW50UmVjdCIsIm93bmVyRG9jdW1lbnQiLCJyIiwiZGVmYXVsdFZpZXciLCJzIiwiYm90dG9tIiwiZ2V0Q29tcHV0ZWRTdHlsZSIsImEiLCJmIiwib3ZlcmZsb3ciLCJsIiwib3ZlcmZsb3dYIiwiaCIsIm92ZXJmbG93WSIsImJvZHkiLCJBIiwiUCIsInNjcm9sbFdpZHRoIiwic2Nyb2xsSGVpZ2h0IiwiY2xpZW50VG9wIiwiY2xpZW50TGVmdCIsImNsaWVudFdpZHRoIiwiY2xpZW50SGVpZ2h0Iiwib2Zmc2V0UGFyZW50IiwicG9pbnRlckV2ZW50cyIsIm9mZnNldFdpZHRoIiwidSIsIlJlZ0V4cCIsImMiLCJnIiwiZCIsInAiLCJTVkdBbmltYXRlZFN0cmluZyIsImJhc2VWYWwiLCJtIiwidiIsIk9iamVjdCIsImNyZWF0ZSIsImVudW1lcmFibGUiLCJ3cml0YWJsZSIsImNvbmZpZ3VyYWJsZSIsInNldFByb3RvdHlwZU9mIiwiX19wcm90b19fIiwieSIsImIiLCJwZXJmb3JtYW5jZSIsIm5vdyIsIkRhdGUiLCJ3IiwicGFyc2VGbG9hdCIsIkMiLCJPIiwic2Nyb2xsUGFyZW50cyIsInBhZ2VYT2Zmc2V0IiwicGFnZVlPZmZzZXQiLCJpbm5lcldpZHRoIiwiaW5uZXJIZWlnaHQiLCJzdWJzdHIiLCJFIiwiZGVmaW5lUHJvcGVydHkiLCJtb2R1bGVzIiwiVCIsIlMiLCJNIiwiVyIsIl8iLCJwb3AiLCJrIiwiYmluZGluZ3MiLCJoYW5kbGVyIiwib25jZSIsIlV0aWxzIiwiZ2V0QWN0dWFsQm91bmRpbmdDbGllbnRSZWN0IiwiZ2V0U2Nyb2xsUGFyZW50cyIsImdldEJvdW5kcyIsImdldE9mZnNldFBhcmVudCIsImFkZENsYXNzIiwicmVtb3ZlQ2xhc3MiLCJoYXNDbGFzcyIsInVwZGF0ZUNsYXNzZXMiLCJkZWZlciIsImZsdXNoIiwidW5pcXVlSWQiLCJFdmVudGVkIiwiZ2V0U2Nyb2xsQmFyU2l6ZSIsInJlbW92ZVV0aWxFbGVtZW50cyIsIkIiLCJTeW1ib2wiLCJpdGVyYXRvciIsIm5leHQiLCJpc0FycmF5IiwieiIsIkZ1bmN0aW9uIiwiZ2V0T3duUHJvcGVydHlEZXNjcmlwdG9yIiwiZ2V0IiwiZ2V0UHJvdG90eXBlT2YiLCJqIiwiWSIsIkwiLCJEIiwibWluIiwiY2xlYXJUaW1lb3V0IiwiWCIsImNlbnRlciIsIkYiLCJtaWRkbGUiLCJIIiwiTiIsIlUiLCJWIiwiUiIsInEiLCJiaW5kIiwiaGlzdG9yeSIsInNldE9wdGlvbnMiLCJpbml0aWFsaXplIiwiY2xhc3NlcyIsImNsYXNzUHJlZml4Iiwib2Zmc2V0IiwidGFyZ2V0T2Zmc2V0IiwidGFyZ2V0QXR0YWNobWVudCIsInRhcmdldE1vZGlmaWVyIiwianF1ZXJ5IiwiZ2V0Q2xhc3MiLCJhZGRUYXJnZXRDbGFzc2VzIiwiYXR0YWNobWVudCIsImVuYWJsZWQiLCJib3JkZXJUb3BXaWR0aCIsImJvcmRlckJvdHRvbVdpZHRoIiwiYm9yZGVyTGVmdFdpZHRoIiwibWF4Iiwic2Nyb2xsVG9wIiwiX2NhY2hlIiwiX2FkZEF0dGFjaENsYXNzZXMiLCJjbGVhckNhY2hlIiwidXBkYXRlQXR0YWNoQ2xhc3NlcyIsImNhY2hlIiwibGFzdFNpemUiLCJnZXRUYXJnZXRCb3VuZHMiLCJ0YXJnZXRQb3MiLCJlbGVtZW50UG9zIiwibWFudWFsT2Zmc2V0IiwibWFudWFsVGFyZ2V0T2Zmc2V0Iiwic2Nyb2xsYmFyU2l6ZSIsInBhZ2UiLCJ2aWV3cG9ydCIsInBhcmVudEVsZW1lbnQiLCJvcHRpbWl6YXRpb25zIiwibW92ZUVsZW1lbnQiLCJzY3JvbGxMZWZ0IiwibW92ZSIsInVuc2hpZnQiLCJncHUiLCJJIiwiY29uc3RyYWludHMiLCJvdXRPZkJvdW5kc0NsYXNzIiwicGlubmVkQ2xhc3MiLCJ0byIsInBpbiIsInRyaWdnZXIiLCJpbml0TW9kYWxzIiwiJG1vZGFsIiwiJGJ0biIsInJlbGF0ZWRUYXJnZXQiLCJsaW5rIiwicmVzZXRBamF4TW9kYWxzIiwiZGVib3VuY2UiLCJlYXNlSW5PdXRRdWFkIiwiYW5pbWF0ZSIsInJlcXVlc3RBbmltYXRpb25GcmFtZSIsIm9mZnNldEhlaWdodCIsInVybENob29zZXIiLCJpbml0RGVsQnRuIiwiJHRoaXMiLCJsaW5rZWRJRCIsIiR3aWRnZXQiLCIkaW5wdXQiLCJzbHVnIiwicmVwbGFjZVVybCIsImNsb3Nlc3QiLCJpdGVtVXJsIiwiaXRlbUlkIiwicmVwbGFjZWRVcmwiLCJ0ZXh0IiwidGh1bWJQYXRoIiwiY2tlIiwiaXRlbVRpdGxlIiwiaXRlbVRodW1iUGF0aCIsImlzTWVkaWFDaG9vc2VyIiwic2F2ZU1lZGlhQ2hvb3Nlck1vZGFsIiwic2F2ZVVybENob29zZXJNb2RhbCIsIiRwYXJlbnRNb2RhbCIsInBhcmVudE1vZGFsSWQiLCJtb2RhbCIsImNsb3NlIiwibGlua2VkSW5wdXRJZCIsImZ1bmNOdW0iLCJnZXRVcmxQYXJhbSIsIm9wZW5lciIsIkNLRURJVE9SIiwidG9vbHMiLCJjYWxsRnVuY3Rpb24iLCIkbWVkaWFDaG9vc2VyIiwiJHByZXZpZXdJbWciLCIkcHJldmlld1RpdGxlIiwiJHBhcmVudCIsInByZXBlbmQiLCJyZVBhcmFtIiwibG9jYXRpb24iXSwibWFwcGluZ3MiOiI7Ozs7Ozs7OztBQUFBLHVDOzs7Ozs7Ozs7Ozs7QUNBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUE7QUFBQUEsTUFBTSxDQUFDQyxDQUFQLEdBQVdELE1BQU0sQ0FBQ0UsTUFBUCxHQUFnQkMsTUFBTSxDQUFDRCxNQUFQLEdBQWdCRSxtQkFBTyxDQUFDLG9EQUFELENBQWxEOztBQUNBQSxtQkFBTyxDQUFDLHdGQUFELENBQVA7O0FBQ0FBLG1CQUFPLENBQUMsa0ZBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxvRUFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLGdFQUFELENBQVA7O0FBQ0FKLE1BQU0sQ0FBQ0ssT0FBUCxHQUFpQkQsbUJBQU8sQ0FBQyxrRUFBRCxDQUF4Qjs7QUFDQUEsbUJBQU8sQ0FBQyxvRUFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLHdGQUFELENBQVA7O0FBQ0FBLG1CQUFPLENBQUMsMEdBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyw4R0FBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLDRHQUFELENBQVA7O0FBQ0E7QUFDQTtBQUNBSixNQUFNLENBQUNNLFFBQVAsR0FBa0JGLG1CQUFPLENBQUMsZ0VBQUQsQ0FBekI7QUFFQUosTUFBTSxDQUFDTyxRQUFQLEdBQWtCLEVBQWxCO0FBQ0FQLE1BQU0sQ0FBQ08sUUFBUCxDQUFnQkMsT0FBaEIsR0FBMEJKLG1CQUFPLENBQUMsb0dBQUQsQ0FBakM7QUFDQUosTUFBTSxDQUFDTyxRQUFQLENBQWdCRSxNQUFoQixHQUEwQkwsbUJBQU8sQ0FBQyxrR0FBRCxDQUFqQztBQUVBSixNQUFNLENBQUNVLEdBQVAsR0FBYSxFQUFiO0FBQ0FWLE1BQU0sQ0FBQ1UsR0FBUCxDQUFXQyxTQUFYLEdBQXVCUCxtQkFBTyxDQUFDLDhFQUFELENBQTlCO0FBQ0FKLE1BQU0sQ0FBQ1UsR0FBUCxDQUFXRSxZQUFYLEdBQTBCUixtQkFBTyxDQUFDLG9GQUFELENBQWpDO0FBQ0FKLE1BQU0sQ0FBQ1UsR0FBUCxDQUFXRyxHQUFYLEdBQWlCVCxtQkFBTyxDQUFDLHNEQUFELENBQXhCO0FBQ0FKLE1BQU0sQ0FBQ1UsR0FBUCxDQUFXSSxLQUFYLEdBQW1CVixtQkFBTyxDQUFDLDZEQUFELENBQTFCO0FBRUFKLE1BQU0sQ0FBQ1UsR0FBUCxDQUFXRyxHQUFYLENBQWVFLElBQWY7QUFFQTtBQUNBO0FBQ0E7QUFDQTtDQUVBOzs7Ozs7Ozs7Ozs7Ozs7O0FDaENBQyxtQ0FBTyxZQUFXO0FBRWQsU0FBTztBQUNIO0FBQ0FELFFBRkcsa0JBRUk7QUFFSEwsU0FBRyxDQUFDQyxTQUFKLENBQWNJLElBQWQ7QUFDQUwsU0FBRyxDQUFDRSxZQUFKLENBQWlCRyxJQUFqQjtBQUNBTCxTQUFHLENBQUNJLEtBQUosQ0FBVUMsSUFBVjs7QUFDQSxVQUFJLE9BQU9FLFlBQVAsS0FBd0IsVUFBNUIsRUFBd0M7QUFDcENBLG9CQUFZO0FBQ2Y7QUFDSjtBQVZFLEdBQVA7QUFZSCxDQWRLO0FBQUEsb0dBQU47QUFnQkFoQixDQUFDLENBQUMsWUFBVztBQUVUQSxHQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QmlCLEVBQTdCLENBQWdDLGNBQWhDLEVBQWdELFVBQVVDLENBQVYsRUFBYTtBQUN6RCxRQUFHbEIsQ0FBQyxDQUFDa0IsQ0FBQyxDQUFDQyxNQUFILENBQUQsQ0FBWUMsTUFBWixHQUFxQkEsTUFBckIsR0FBOEJBLE1BQTlCLEdBQXVDQyxJQUF2QyxDQUE0QyxJQUE1QyxLQUFxRHJCLENBQUMsQ0FBQ2tCLENBQUMsQ0FBQ0MsTUFBSCxDQUFELENBQVlDLE1BQVosR0FBcUJBLE1BQXJCLEdBQThCQSxNQUE5QixHQUF1Q0MsSUFBdkMsQ0FBNEMsSUFBNUMsS0FBcUQsS0FBN0csRUFBb0g7QUFDaEhDLGtCQUFZLENBQUNDLE9BQWIsQ0FBcUIsWUFBckIsRUFBbUN2QixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFxQixJQUFSLENBQWEsTUFBYixDQUFuQztBQUNILEtBRkQsTUFFSztBQUNEQyxrQkFBWSxDQUFDQyxPQUFiLENBQXFCLFNBQXJCLEVBQWdDdkIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRcUIsSUFBUixDQUFhLE1BQWIsQ0FBaEM7QUFDSDs7QUFBQTtBQUVKLEdBUEQsRUFGUyxDQVdUOztBQUNBLE1BQUlHLE9BQU8sR0FBR0YsWUFBWSxDQUFDRyxPQUFiLENBQXFCLFNBQXJCLENBQWQ7O0FBQ0EsTUFBSUQsT0FBSixFQUFhO0FBQ1R4QixLQUFDLENBQUMsWUFBWXdCLE9BQVosR0FBc0IsSUFBdkIsQ0FBRCxDQUE4QkUsR0FBOUIsQ0FBa0MsTUFBbEM7QUFDSDs7QUFFRCxNQUFJQyxVQUFVLEdBQUdMLFlBQVksQ0FBQ0csT0FBYixDQUFxQixZQUFyQixDQUFqQjs7QUFDQSxNQUFJRSxVQUFKLEVBQWdCO0FBQ1ozQixLQUFDLENBQUMsWUFBWTJCLFVBQVosR0FBeUIsSUFBMUIsQ0FBRCxDQUFpQ0QsR0FBakMsQ0FBcUMsTUFBckM7QUFDSDs7QUFFRDFCLEdBQUMsQ0FBQyxZQUFELENBQUQsQ0FBZ0JvQixNQUFoQixHQUF5QlEsSUFBekIsQ0FBOEIsR0FBOUIsRUFBbUNDLEtBQW5DLENBQXlDLFlBQVc7QUFDaEQ3QixLQUFDLENBQUMsSUFBRCxDQUFELENBQVFvQixNQUFSLEdBQWlCUSxJQUFqQixDQUFzQixJQUF0QixFQUE0QnBCLE1BQTVCO0FBQ0gsR0FGRDtBQUlBQyxLQUFHLENBQUNHLEdBQUosQ0FBUUUsSUFBUjtBQUVBZCxHQUFDLENBQUMsa0RBQUQsQ0FBRCxDQUFzRDhCLE1BQXRELENBQTZELFlBQVc7QUFDcEU5QixLQUFDLENBQUMrQixPQUFGLENBQVUvQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFnQyxJQUFSLENBQWEsS0FBYixDQUFWO0FBQ0gsR0FGRDtBQUlBaEMsR0FBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJpQyxXQUFyQixDQUFpQztBQUM3QkMsaUJBQWEsRUFBRSxLQURjO0FBRTdCQyxrQkFBYyxFQUFFLElBRmE7QUFHN0JDLFVBQU0sRUFBRTtBQUNKQyxVQUFJLEVBQUUsNkVBREY7QUFFSkMsV0FBSyxFQUFFO0FBRkgsS0FIcUI7QUFPN0JDLGNBQVUsRUFBRSxvQkFBU0MsS0FBVCxFQUFnQjtBQUN4QixhQUFPQSxLQUFLLENBQUNDLE1BQU4sR0FBZSxDQUF0QjtBQUNIO0FBVDRCLEdBQWpDO0FBWUF6QyxHQUFDLENBQUMsYUFBRCxDQUFELENBQWlCMEMsVUFBakIsQ0FBNEI7QUFDeEJDLFVBQU0sRUFBRSxHQURnQjtBQUV4QkMsV0FBTyxFQUFFLENBQ0QsQ0FBQyxPQUFELEVBQVUsQ0FBQyxPQUFELENBQVYsQ0FEQyxFQUVELENBQUMsTUFBRCxFQUFTLENBQUMsTUFBRCxFQUFTLFFBQVQsRUFBbUIsV0FBbkIsRUFBZ0MsT0FBaEMsQ0FBVCxDQUZDLEVBR0QsQ0FBQyxVQUFELEVBQWEsQ0FBQyxVQUFELENBQWIsQ0FIQyxFQUlELENBQUMsT0FBRCxFQUFVLENBQUMsT0FBRCxDQUFWLENBSkMsRUFLRCxDQUFDLE1BQUQsRUFBUyxDQUFDLElBQUQsRUFBTyxJQUFQLEVBQWEsV0FBYixDQUFULENBTEMsRUFNRCxDQUFDLFFBQUQsRUFBVyxDQUFDLFFBQUQsQ0FBWCxDQU5DLEVBT0QsQ0FBQyxPQUFELEVBQVUsQ0FBQyxPQUFELENBQVYsQ0FQQyxFQVFELENBQUMsUUFBRCxFQUFXLENBQUMsTUFBRCxFQUFTLElBQVQsQ0FBWCxDQVJDLEVBU0QsQ0FBQyxNQUFELEVBQVMsQ0FBQyxZQUFELEVBQWUsVUFBZixDQUFULENBVEMsRUFVRCxDQUFDLE1BQUQsRUFBUyxDQUFDLE1BQUQsQ0FBVCxDQVZDLEVBV0QsQ0FBQyxPQUFELEVBQVUsQ0FBQyxPQUFELENBQVYsQ0FYQztBQUZlLEdBQTVCO0FBZ0JILENBNURBLENBQUQsQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUEwQkEsQ0FBQyxZQUFXO0FBQ1YsTUFBSXZDLFFBQUo7QUFBQSxNQUFjd0MsT0FBZDtBQUFBLE1BQXVCQyxRQUF2QjtBQUFBLE1BQWlDQyxhQUFqQztBQUFBLE1BQWdEQyxvQkFBaEQ7QUFBQSxNQUFzRUMsZUFBdEU7QUFBQSxNQUF1RkMsSUFBdkY7QUFBQSxNQUE2RkMsT0FBN0Y7QUFBQSxNQUNFQyxPQUFPLEdBQUcsR0FBR0MsS0FEZjtBQUFBLE1BRUVDLFNBQVMsR0FBRyxHQUFHQyxjQUZqQjtBQUFBLE1BR0VDLFNBQVMsR0FBRyxTQUFaQSxTQUFZLENBQVNDLEtBQVQsRUFBZ0JyQyxNQUFoQixFQUF3QjtBQUFFLFNBQUssSUFBSXNDLEdBQVQsSUFBZ0J0QyxNQUFoQixFQUF3QjtBQUFFLFVBQUlrQyxTQUFTLENBQUNLLElBQVYsQ0FBZXZDLE1BQWYsRUFBdUJzQyxHQUF2QixDQUFKLEVBQWlDRCxLQUFLLENBQUNDLEdBQUQsQ0FBTCxHQUFhdEMsTUFBTSxDQUFDc0MsR0FBRCxDQUFuQjtBQUEyQjs7QUFBQyxhQUFTRSxJQUFULEdBQWdCO0FBQUUsV0FBS0MsV0FBTCxHQUFtQkosS0FBbkI7QUFBMkI7O0FBQUNHLFFBQUksQ0FBQ0UsU0FBTCxHQUFpQjFDLE1BQU0sQ0FBQzBDLFNBQXhCO0FBQW1DTCxTQUFLLENBQUNLLFNBQU4sR0FBa0IsSUFBSUYsSUFBSixFQUFsQjtBQUE4QkgsU0FBSyxDQUFDTSxTQUFOLEdBQWtCM0MsTUFBTSxDQUFDMEMsU0FBekI7QUFBb0MsV0FBT0wsS0FBUDtBQUFlLEdBSGpTOztBQUtBUCxNQUFJLEdBQUcsZ0JBQVcsQ0FBRSxDQUFwQjs7QUFFQUwsU0FBTyxHQUFJLFlBQVc7QUFDcEIsYUFBU0EsT0FBVCxHQUFtQixDQUFFOztBQUVyQkEsV0FBTyxDQUFDaUIsU0FBUixDQUFrQkUsZ0JBQWxCLEdBQXFDbkIsT0FBTyxDQUFDaUIsU0FBUixDQUFrQjdDLEVBQXZEOztBQUVBNEIsV0FBTyxDQUFDaUIsU0FBUixDQUFrQjdDLEVBQWxCLEdBQXVCLFVBQVNnRCxLQUFULEVBQWdCQyxFQUFoQixFQUFvQjtBQUN6QyxXQUFLQyxVQUFMLEdBQWtCLEtBQUtBLFVBQUwsSUFBbUIsRUFBckM7O0FBQ0EsVUFBSSxDQUFDLEtBQUtBLFVBQUwsQ0FBZ0JGLEtBQWhCLENBQUwsRUFBNkI7QUFDM0IsYUFBS0UsVUFBTCxDQUFnQkYsS0FBaEIsSUFBeUIsRUFBekI7QUFDRDs7QUFDRCxXQUFLRSxVQUFMLENBQWdCRixLQUFoQixFQUF1QkcsSUFBdkIsQ0FBNEJGLEVBQTVCOztBQUNBLGFBQU8sSUFBUDtBQUNELEtBUEQ7O0FBU0FyQixXQUFPLENBQUNpQixTQUFSLENBQWtCTyxJQUFsQixHQUF5QixZQUFXO0FBQ2xDLFVBQUlDLElBQUosRUFBVUMsUUFBVixFQUFvQkMsU0FBcEIsRUFBK0JQLEtBQS9CLEVBQXNDUSxFQUF0QyxFQUEwQ0MsSUFBMUM7O0FBQ0FULFdBQUssR0FBR1UsU0FBUyxDQUFDLENBQUQsQ0FBakIsRUFBc0JMLElBQUksR0FBRyxLQUFLSyxTQUFTLENBQUNsQyxNQUFmLEdBQXdCVyxPQUFPLENBQUNPLElBQVIsQ0FBYWdCLFNBQWIsRUFBd0IsQ0FBeEIsQ0FBeEIsR0FBcUQsRUFBbEY7QUFDQSxXQUFLUixVQUFMLEdBQWtCLEtBQUtBLFVBQUwsSUFBbUIsRUFBckM7QUFDQUssZUFBUyxHQUFHLEtBQUtMLFVBQUwsQ0FBZ0JGLEtBQWhCLENBQVo7O0FBQ0EsVUFBSU8sU0FBSixFQUFlO0FBQ2IsYUFBS0MsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHRixTQUFTLENBQUMvQixNQUE5QixFQUFzQ2dDLEVBQUUsR0FBR0MsSUFBM0MsRUFBaURELEVBQUUsRUFBbkQsRUFBdUQ7QUFDckRGLGtCQUFRLEdBQUdDLFNBQVMsQ0FBQ0MsRUFBRCxDQUFwQjtBQUNBRixrQkFBUSxDQUFDSyxLQUFULENBQWUsSUFBZixFQUFxQk4sSUFBckI7QUFDRDtBQUNGOztBQUNELGFBQU8sSUFBUDtBQUNELEtBWkQ7O0FBY0F6QixXQUFPLENBQUNpQixTQUFSLENBQWtCZSxjQUFsQixHQUFtQ2hDLE9BQU8sQ0FBQ2lCLFNBQVIsQ0FBa0JnQixHQUFyRDtBQUVBakMsV0FBTyxDQUFDaUIsU0FBUixDQUFrQmlCLGtCQUFsQixHQUF1Q2xDLE9BQU8sQ0FBQ2lCLFNBQVIsQ0FBa0JnQixHQUF6RDtBQUVBakMsV0FBTyxDQUFDaUIsU0FBUixDQUFrQmtCLG1CQUFsQixHQUF3Q25DLE9BQU8sQ0FBQ2lCLFNBQVIsQ0FBa0JnQixHQUExRDs7QUFFQWpDLFdBQU8sQ0FBQ2lCLFNBQVIsQ0FBa0JnQixHQUFsQixHQUF3QixVQUFTYixLQUFULEVBQWdCQyxFQUFoQixFQUFvQjtBQUMxQyxVQUFJSyxRQUFKLEVBQWNDLFNBQWQsRUFBeUJTLENBQXpCLEVBQTRCUixFQUE1QixFQUFnQ0MsSUFBaEM7O0FBQ0EsVUFBSSxDQUFDLEtBQUtQLFVBQU4sSUFBb0JRLFNBQVMsQ0FBQ2xDLE1BQVYsS0FBcUIsQ0FBN0MsRUFBZ0Q7QUFDOUMsYUFBSzBCLFVBQUwsR0FBa0IsRUFBbEI7QUFDQSxlQUFPLElBQVA7QUFDRDs7QUFDREssZUFBUyxHQUFHLEtBQUtMLFVBQUwsQ0FBZ0JGLEtBQWhCLENBQVo7O0FBQ0EsVUFBSSxDQUFDTyxTQUFMLEVBQWdCO0FBQ2QsZUFBTyxJQUFQO0FBQ0Q7O0FBQ0QsVUFBSUcsU0FBUyxDQUFDbEMsTUFBVixLQUFxQixDQUF6QixFQUE0QjtBQUMxQixlQUFPLEtBQUswQixVQUFMLENBQWdCRixLQUFoQixDQUFQO0FBQ0EsZUFBTyxJQUFQO0FBQ0Q7O0FBQ0QsV0FBS2dCLENBQUMsR0FBR1IsRUFBRSxHQUFHLENBQVQsRUFBWUMsSUFBSSxHQUFHRixTQUFTLENBQUMvQixNQUFsQyxFQUEwQ2dDLEVBQUUsR0FBR0MsSUFBL0MsRUFBcURPLENBQUMsR0FBRyxFQUFFUixFQUEzRCxFQUErRDtBQUM3REYsZ0JBQVEsR0FBR0MsU0FBUyxDQUFDUyxDQUFELENBQXBCOztBQUNBLFlBQUlWLFFBQVEsS0FBS0wsRUFBakIsRUFBcUI7QUFDbkJNLG1CQUFTLENBQUNVLE1BQVYsQ0FBaUJELENBQWpCLEVBQW9CLENBQXBCO0FBQ0E7QUFDRDtBQUNGOztBQUNELGFBQU8sSUFBUDtBQUNELEtBdEJEOztBQXdCQSxXQUFPcEMsT0FBUDtBQUVELEdBNURTLEVBQVY7O0FBOERBeEMsVUFBUSxHQUFJLFVBQVM4RSxNQUFULEVBQWlCO0FBQzNCLFFBQUlDLE1BQUosRUFBWUMsYUFBWjs7QUFFQTdCLGFBQVMsQ0FBQ25ELFFBQUQsRUFBVzhFLE1BQVgsQ0FBVDs7QUFFQTlFLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJqQixPQUFuQixHQUE2QkEsT0FBN0I7QUFHQTs7Ozs7Ozs7QUFRQXhDLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJ3QixNQUFuQixHQUE0QixDQUFDLE1BQUQsRUFBUyxXQUFULEVBQXNCLFNBQXRCLEVBQWlDLFdBQWpDLEVBQThDLFVBQTlDLEVBQTBELFdBQTFELEVBQXVFLFdBQXZFLEVBQW9GLFlBQXBGLEVBQWtHLGFBQWxHLEVBQWlILFdBQWpILEVBQThILE9BQTlILEVBQXVJLGVBQXZJLEVBQXdKLFlBQXhKLEVBQXNLLG9CQUF0SyxFQUE0TCxnQkFBNUwsRUFBOE0scUJBQTlNLEVBQXFPLFNBQXJPLEVBQWdQLGlCQUFoUCxFQUFtUSxTQUFuUSxFQUE4USxpQkFBOVEsRUFBaVMsVUFBalMsRUFBNlMsa0JBQTdTLEVBQWlVLFVBQWpVLEVBQTZVLGtCQUE3VSxFQUFpVyxPQUFqVyxFQUEwVyxrQkFBMVcsRUFBOFgsaUJBQTlYLEVBQWlaLGVBQWpaLENBQTVCO0FBRUFqRixZQUFRLENBQUN5RCxTQUFULENBQW1CeUIsY0FBbkIsR0FBb0M7QUFDbENDLFNBQUcsRUFBRSxJQUQ2QjtBQUVsQ0MsWUFBTSxFQUFFLE1BRjBCO0FBR2xDQyxxQkFBZSxFQUFFLEtBSGlCO0FBSWxDQyxxQkFBZSxFQUFFLENBSmlCO0FBS2xDQyxvQkFBYyxFQUFFLEtBTGtCO0FBTWxDQyxpQkFBVyxFQUFFLEdBTnFCO0FBT2xDQyxlQUFTLEVBQUUsTUFQdUI7QUFRbENDLDJCQUFxQixFQUFFLElBUlc7QUFTbENDLDBCQUFvQixFQUFFLEVBVFk7QUFVbENDLG9CQUFjLEVBQUUsR0FWa0I7QUFXbENDLHFCQUFlLEVBQUUsR0FYaUI7QUFZbENDLGtCQUFZLEVBQUUsSUFab0I7QUFhbENDLGNBQVEsRUFBRSxJQWJ3QjtBQWNsQ0MsWUFBTSxFQUFFLEVBZDBCO0FBZWxDQyxlQUFTLEVBQUUsSUFmdUI7QUFnQmxDQyx1QkFBaUIsRUFBRSxJQWhCZTtBQWlCbENDLG1CQUFhLEVBQUUsSUFqQm1CO0FBa0JsQ0MsdUJBQWlCLEVBQUUsSUFsQmU7QUFtQmxDQyxzQkFBZ0IsRUFBRSxJQW5CZ0I7QUFvQmxDQyxlQUFTLEVBQUUsSUFwQnVCO0FBcUJsQ0Msb0JBQWMsRUFBRSxLQXJCa0I7QUFzQmxDQyx1QkFBaUIsRUFBRSxJQXRCZTtBQXVCbENDLDBCQUFvQixFQUFFLE1BdkJZO0FBd0JsQ0MsYUFBTyxFQUFFLElBeEJ5QjtBQXlCbENDLHdCQUFrQixFQUFFLDJCQXpCYztBQTBCbENDLHlCQUFtQixFQUFFLHlEQTFCYTtBQTJCbENDLHNCQUFnQixFQUFFLGlGQTNCZ0I7QUE0QmxDQyxvQkFBYyxFQUFFLHNFQTVCa0I7QUE2QmxDQyx5QkFBbUIsRUFBRSxzQ0E3QmE7QUE4QmxDQyx1QkFBaUIsRUFBRSw0Q0E5QmU7QUErQmxDQyxzQkFBZ0IsRUFBRSxlQS9CZ0I7QUFnQ2xDQyxrQ0FBNEIsRUFBRSw4Q0FoQ0k7QUFpQ2xDQyxvQkFBYyxFQUFFLGFBakNrQjtBQWtDbENDLGdDQUEwQixFQUFFLElBbENNO0FBbUNsQ0MsMEJBQW9CLEVBQUUsb0NBbkNZO0FBb0NsQ0MsWUFBTSxFQUFFLGdCQUFTQyxJQUFULEVBQWVDLElBQWYsRUFBcUI7QUFDM0IsZUFBT0EsSUFBSSxFQUFYO0FBQ0QsT0F0Q2lDO0FBdUNsQy9HLFVBQUksRUFBRSxnQkFBVztBQUNmLGVBQU9vQyxJQUFQO0FBQ0QsT0F6Q2lDO0FBMENsQzRFLG1CQUFhLEVBQUUsS0ExQ21CO0FBMkNsQ0MsY0FBUSxFQUFFLG9CQUFXO0FBQ25CLFlBQUl0RSxLQUFKLEVBQVd1RSxjQUFYLEVBQTJCQyxJQUEzQixFQUFpQ3hELEVBQWpDLEVBQXFDQyxJQUFyQyxFQUEyQ3dELElBQTNDOztBQUNBLGFBQUtDLE9BQUwsQ0FBYUMsU0FBYixHQUF5QixLQUFLLEtBQUtELE9BQUwsQ0FBYUMsU0FBbEIsR0FBOEIsMkJBQXZEO0FBQ0FGLFlBQUksR0FBRyxLQUFLQyxPQUFMLENBQWFFLG9CQUFiLENBQWtDLEtBQWxDLENBQVA7O0FBQ0EsYUFBSzVELEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBR3dELElBQUksQ0FBQ3pGLE1BQXpCLEVBQWlDZ0MsRUFBRSxHQUFHQyxJQUF0QyxFQUE0Q0QsRUFBRSxFQUE5QyxFQUFrRDtBQUNoRGhCLGVBQUssR0FBR3lFLElBQUksQ0FBQ3pELEVBQUQsQ0FBWjs7QUFDQSxjQUFJLHVCQUF1QjZELElBQXZCLENBQTRCN0UsS0FBSyxDQUFDMkUsU0FBbEMsQ0FBSixFQUFrRDtBQUNoREosMEJBQWMsR0FBR3ZFLEtBQWpCO0FBQ0FBLGlCQUFLLENBQUMyRSxTQUFOLEdBQWtCLFlBQWxCO0FBQ0E7QUFDRDtBQUNGOztBQUNELFlBQUksQ0FBQ0osY0FBTCxFQUFxQjtBQUNuQkEsd0JBQWMsR0FBRzNILFFBQVEsQ0FBQ2tJLGFBQVQsQ0FBdUIsK0NBQXZCLENBQWpCO0FBQ0EsZUFBS0osT0FBTCxDQUFhSyxXQUFiLENBQXlCUixjQUF6QjtBQUNEOztBQUNEQyxZQUFJLEdBQUdELGNBQWMsQ0FBQ0ssb0JBQWYsQ0FBb0MsTUFBcEMsRUFBNEMsQ0FBNUMsQ0FBUDs7QUFDQSxZQUFJSixJQUFKLEVBQVU7QUFDUixjQUFJQSxJQUFJLENBQUNRLFdBQUwsSUFBb0IsSUFBeEIsRUFBOEI7QUFDNUJSLGdCQUFJLENBQUNRLFdBQUwsR0FBbUIsS0FBS0MsT0FBTCxDQUFhekIsbUJBQWhDO0FBQ0QsV0FGRCxNQUVPLElBQUlnQixJQUFJLENBQUNVLFNBQUwsSUFBa0IsSUFBdEIsRUFBNEI7QUFDakNWLGdCQUFJLENBQUNVLFNBQUwsR0FBaUIsS0FBS0QsT0FBTCxDQUFhekIsbUJBQTlCO0FBQ0Q7QUFDRjs7QUFDRCxlQUFPLEtBQUtrQixPQUFMLENBQWFLLFdBQWIsQ0FBeUIsS0FBS0ksZUFBTCxFQUF6QixDQUFQO0FBQ0QsT0FwRWlDO0FBcUVsQ0MsWUFBTSxFQUFFLGdCQUFTakIsSUFBVCxFQUFlO0FBQ3JCLFlBQUlrQixJQUFKLEVBQVVDLFFBQVYsRUFBb0JDLFFBQXBCO0FBQ0FGLFlBQUksR0FBRztBQUNMRyxjQUFJLEVBQUUsQ0FERDtBQUVMQyxjQUFJLEVBQUUsQ0FGRDtBQUdMQyxrQkFBUSxFQUFFdkIsSUFBSSxDQUFDd0IsS0FIVjtBQUlMQyxtQkFBUyxFQUFFekIsSUFBSSxDQUFDakY7QUFKWCxTQUFQO0FBTUFvRyxnQkFBUSxHQUFHbkIsSUFBSSxDQUFDd0IsS0FBTCxHQUFheEIsSUFBSSxDQUFDakYsTUFBN0I7QUFDQW1HLFlBQUksQ0FBQ1EsUUFBTCxHQUFnQixLQUFLWixPQUFMLENBQWF6QyxjQUE3QjtBQUNBNkMsWUFBSSxDQUFDUyxTQUFMLEdBQWlCLEtBQUtiLE9BQUwsQ0FBYXhDLGVBQTlCOztBQUNBLFlBQUs0QyxJQUFJLENBQUNRLFFBQUwsSUFBaUIsSUFBbEIsSUFBNEJSLElBQUksQ0FBQ1MsU0FBTCxJQUFrQixJQUFsRCxFQUF5RDtBQUN2RFQsY0FBSSxDQUFDUSxRQUFMLEdBQWdCUixJQUFJLENBQUNLLFFBQXJCO0FBQ0FMLGNBQUksQ0FBQ1MsU0FBTCxHQUFpQlQsSUFBSSxDQUFDTyxTQUF0QjtBQUNELFNBSEQsTUFHTyxJQUFJUCxJQUFJLENBQUNRLFFBQUwsSUFBaUIsSUFBckIsRUFBMkI7QUFDaENSLGNBQUksQ0FBQ1EsUUFBTCxHQUFnQlAsUUFBUSxHQUFHRCxJQUFJLENBQUNTLFNBQWhDO0FBQ0QsU0FGTSxNQUVBLElBQUlULElBQUksQ0FBQ1MsU0FBTCxJQUFrQixJQUF0QixFQUE0QjtBQUNqQ1QsY0FBSSxDQUFDUyxTQUFMLEdBQWtCLElBQUlSLFFBQUwsR0FBaUJELElBQUksQ0FBQ1EsUUFBdkM7QUFDRDs7QUFDRE4sZ0JBQVEsR0FBR0YsSUFBSSxDQUFDUSxRQUFMLEdBQWdCUixJQUFJLENBQUNTLFNBQWhDOztBQUNBLFlBQUkzQixJQUFJLENBQUNqRixNQUFMLEdBQWNtRyxJQUFJLENBQUNTLFNBQW5CLElBQWdDM0IsSUFBSSxDQUFDd0IsS0FBTCxHQUFhTixJQUFJLENBQUNRLFFBQXRELEVBQWdFO0FBQzlEUixjQUFJLENBQUNVLFNBQUwsR0FBaUJWLElBQUksQ0FBQ08sU0FBdEI7QUFDQVAsY0FBSSxDQUFDVyxRQUFMLEdBQWdCWCxJQUFJLENBQUNLLFFBQXJCO0FBQ0QsU0FIRCxNQUdPO0FBQ0wsY0FBSUosUUFBUSxHQUFHQyxRQUFmLEVBQXlCO0FBQ3ZCRixnQkFBSSxDQUFDTyxTQUFMLEdBQWlCekIsSUFBSSxDQUFDakYsTUFBdEI7QUFDQW1HLGdCQUFJLENBQUNLLFFBQUwsR0FBZ0JMLElBQUksQ0FBQ08sU0FBTCxHQUFpQkwsUUFBakM7QUFDRCxXQUhELE1BR087QUFDTEYsZ0JBQUksQ0FBQ0ssUUFBTCxHQUFnQnZCLElBQUksQ0FBQ3dCLEtBQXJCO0FBQ0FOLGdCQUFJLENBQUNPLFNBQUwsR0FBaUJQLElBQUksQ0FBQ0ssUUFBTCxHQUFnQkgsUUFBakM7QUFDRDtBQUNGOztBQUNERixZQUFJLENBQUNHLElBQUwsR0FBWSxDQUFDckIsSUFBSSxDQUFDd0IsS0FBTCxHQUFhTixJQUFJLENBQUNLLFFBQW5CLElBQStCLENBQTNDO0FBQ0FMLFlBQUksQ0FBQ0ksSUFBTCxHQUFZLENBQUN0QixJQUFJLENBQUNqRixNQUFMLEdBQWNtRyxJQUFJLENBQUNPLFNBQXBCLElBQWlDLENBQTdDO0FBQ0EsZUFBT1AsSUFBUDtBQUNELE9BeEdpQzs7QUEwR2xDOzs7Ozs7OztBQVFBWSxVQUFJLEVBQUUsY0FBU3hJLENBQVQsRUFBWTtBQUNoQixlQUFPLEtBQUtpSCxPQUFMLENBQWF3QixTQUFiLENBQXVCQyxNQUF2QixDQUE4QixlQUE5QixDQUFQO0FBQ0QsT0FwSGlDO0FBcUhsQ0MsZUFBUyxFQUFFM0csSUFySHVCO0FBc0hsQzRHLGFBQU8sRUFBRSxpQkFBUzVJLENBQVQsRUFBWTtBQUNuQixlQUFPLEtBQUtpSCxPQUFMLENBQWF3QixTQUFiLENBQXVCQyxNQUF2QixDQUE4QixlQUE5QixDQUFQO0FBQ0QsT0F4SGlDO0FBeUhsQ0csZUFBUyxFQUFFLG1CQUFTN0ksQ0FBVCxFQUFZO0FBQ3JCLGVBQU8sS0FBS2lILE9BQUwsQ0FBYXdCLFNBQWIsQ0FBdUJLLEdBQXZCLENBQTJCLGVBQTNCLENBQVA7QUFDRCxPQTNIaUM7QUE0SGxDQyxjQUFRLEVBQUUsa0JBQVMvSSxDQUFULEVBQVk7QUFDcEIsZUFBTyxLQUFLaUgsT0FBTCxDQUFhd0IsU0FBYixDQUF1QkssR0FBdkIsQ0FBMkIsZUFBM0IsQ0FBUDtBQUNELE9BOUhpQztBQStIbENFLGVBQVMsRUFBRSxtQkFBU2hKLENBQVQsRUFBWTtBQUNyQixlQUFPLEtBQUtpSCxPQUFMLENBQWF3QixTQUFiLENBQXVCQyxNQUF2QixDQUE4QixlQUE5QixDQUFQO0FBQ0QsT0FqSWlDO0FBa0lsQ08sV0FBSyxFQUFFakgsSUFsSTJCO0FBbUlsQ2tILFdBQUssRUFBRSxpQkFBVztBQUNoQixlQUFPLEtBQUtqQyxPQUFMLENBQWF3QixTQUFiLENBQXVCQyxNQUF2QixDQUE4QixZQUE5QixDQUFQO0FBQ0QsT0FySWlDO0FBc0lsQ1MsZUFBUyxFQUFFLG1CQUFTekMsSUFBVCxFQUFlO0FBQ3hCLFlBQUkwQyxJQUFKLEVBQVVDLGVBQVYsRUFBMkJDLFVBQTNCLEVBQXVDL0YsRUFBdkMsRUFBMkNnRyxFQUEzQyxFQUErQ0MsRUFBL0MsRUFBbURoRyxJQUFuRCxFQUF5RGlHLEtBQXpELEVBQWdFQyxLQUFoRSxFQUF1RTFDLElBQXZFLEVBQTZFMkMsS0FBN0UsRUFBb0ZDLEtBQXBGLEVBQTJGQyxRQUEzRjs7QUFDQSxZQUFJLEtBQUs1QyxPQUFMLEtBQWlCLEtBQUt0QixpQkFBMUIsRUFBNkM7QUFDM0MsZUFBS3NCLE9BQUwsQ0FBYXdCLFNBQWIsQ0FBdUJLLEdBQXZCLENBQTJCLFlBQTNCO0FBQ0Q7O0FBQ0QsWUFBSSxLQUFLbkQsaUJBQVQsRUFBNEI7QUFDMUJlLGNBQUksQ0FBQ29ELGNBQUwsR0FBc0IzSyxRQUFRLENBQUNrSSxhQUFULENBQXVCLEtBQUtHLE9BQUwsQ0FBYXVDLGVBQWIsQ0FBNkJDLElBQTdCLEVBQXZCLENBQXRCO0FBQ0F0RCxjQUFJLENBQUNxRCxlQUFMLEdBQXVCckQsSUFBSSxDQUFDb0QsY0FBNUI7QUFDQSxlQUFLbkUsaUJBQUwsQ0FBdUIyQixXQUF2QixDQUFtQ1osSUFBSSxDQUFDb0QsY0FBeEM7QUFDQTlDLGNBQUksR0FBR04sSUFBSSxDQUFDb0QsY0FBTCxDQUFvQkcsZ0JBQXBCLENBQXFDLGdCQUFyQyxDQUFQOztBQUNBLGVBQUsxRyxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3RCxJQUFJLENBQUN6RixNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaEQ2RixnQkFBSSxHQUFHcEMsSUFBSSxDQUFDekQsRUFBRCxDQUFYO0FBQ0E2RixnQkFBSSxDQUFDN0IsV0FBTCxHQUFtQmIsSUFBSSxDQUFDd0QsSUFBeEI7QUFDRDs7QUFDRFAsZUFBSyxHQUFHakQsSUFBSSxDQUFDb0QsY0FBTCxDQUFvQkcsZ0JBQXBCLENBQXFDLGdCQUFyQyxDQUFSOztBQUNBLGVBQUtWLEVBQUUsR0FBRyxDQUFMLEVBQVFFLEtBQUssR0FBR0UsS0FBSyxDQUFDcEksTUFBM0IsRUFBbUNnSSxFQUFFLEdBQUdFLEtBQXhDLEVBQStDRixFQUFFLEVBQWpELEVBQXFEO0FBQ25ESCxnQkFBSSxHQUFHTyxLQUFLLENBQUNKLEVBQUQsQ0FBWjtBQUNBSCxnQkFBSSxDQUFDZSxTQUFMLEdBQWlCLEtBQUtDLFFBQUwsQ0FBYzFELElBQUksQ0FBQzJELElBQW5CLENBQWpCO0FBQ0Q7O0FBQ0QsY0FBSSxLQUFLN0MsT0FBTCxDQUFhOUIsY0FBakIsRUFBaUM7QUFDL0JnQixnQkFBSSxDQUFDNEQsV0FBTCxHQUFtQm5MLFFBQVEsQ0FBQ2tJLGFBQVQsQ0FBdUIsMEVBQTBFLEtBQUtHLE9BQUwsQ0FBYWxCLGNBQXZGLEdBQXdHLE1BQS9ILENBQW5CO0FBQ0FJLGdCQUFJLENBQUNvRCxjQUFMLENBQW9CeEMsV0FBcEIsQ0FBZ0NaLElBQUksQ0FBQzRELFdBQXJDO0FBQ0Q7O0FBQ0RqQix5QkFBZSxHQUFJLFVBQVNrQixLQUFULEVBQWdCO0FBQ2pDLG1CQUFPLFVBQVN2SyxDQUFULEVBQVk7QUFDakJBLGVBQUMsQ0FBQ3dLLGNBQUY7QUFDQXhLLGVBQUMsQ0FBQ3lLLGVBQUY7O0FBQ0Esa0JBQUkvRCxJQUFJLENBQUNnRSxNQUFMLEtBQWdCdkwsUUFBUSxDQUFDd0wsU0FBN0IsRUFBd0M7QUFDdEMsdUJBQU94TCxRQUFRLENBQUN5TCxPQUFULENBQWlCTCxLQUFLLENBQUMvQyxPQUFOLENBQWNuQiw0QkFBL0IsRUFBNkQsWUFBVztBQUM3RSx5QkFBT2tFLEtBQUssQ0FBQ00sVUFBTixDQUFpQm5FLElBQWpCLENBQVA7QUFDRCxpQkFGTSxDQUFQO0FBR0QsZUFKRCxNQUlPO0FBQ0wsb0JBQUk2RCxLQUFLLENBQUMvQyxPQUFOLENBQWNqQiwwQkFBbEIsRUFBOEM7QUFDNUMseUJBQU9wSCxRQUFRLENBQUN5TCxPQUFULENBQWlCTCxLQUFLLENBQUMvQyxPQUFOLENBQWNqQiwwQkFBL0IsRUFBMkQsWUFBVztBQUMzRSwyQkFBT2dFLEtBQUssQ0FBQ00sVUFBTixDQUFpQm5FLElBQWpCLENBQVA7QUFDRCxtQkFGTSxDQUFQO0FBR0QsaUJBSkQsTUFJTztBQUNMLHlCQUFPNkQsS0FBSyxDQUFDTSxVQUFOLENBQWlCbkUsSUFBakIsQ0FBUDtBQUNEO0FBQ0Y7QUFDRixhQWhCRDtBQWlCRCxXQWxCaUIsQ0FrQmYsSUFsQmUsQ0FBbEI7O0FBbUJBa0QsZUFBSyxHQUFHbEQsSUFBSSxDQUFDb0QsY0FBTCxDQUFvQkcsZ0JBQXBCLENBQXFDLGtCQUFyQyxDQUFSO0FBQ0FKLGtCQUFRLEdBQUcsRUFBWDs7QUFDQSxlQUFLTCxFQUFFLEdBQUcsQ0FBTCxFQUFRRSxLQUFLLEdBQUdFLEtBQUssQ0FBQ3JJLE1BQTNCLEVBQW1DaUksRUFBRSxHQUFHRSxLQUF4QyxFQUErQ0YsRUFBRSxFQUFqRCxFQUFxRDtBQUNuREYsc0JBQVUsR0FBR00sS0FBSyxDQUFDSixFQUFELENBQWxCOztBQUNBSyxvQkFBUSxDQUFDM0csSUFBVCxDQUFjb0csVUFBVSxDQUFDeEcsZ0JBQVgsQ0FBNEIsT0FBNUIsRUFBcUN1RyxlQUFyQyxDQUFkO0FBQ0Q7O0FBQ0QsaUJBQU9RLFFBQVA7QUFDRDtBQUNGLE9BeExpQztBQXlMbENpQixpQkFBVyxFQUFFLHFCQUFTcEUsSUFBVCxFQUFlO0FBQzFCLFlBQUlNLElBQUo7O0FBQ0EsWUFBSU4sSUFBSSxDQUFDb0QsY0FBVCxFQUF5QjtBQUN2QixjQUFJLENBQUM5QyxJQUFJLEdBQUdOLElBQUksQ0FBQ29ELGNBQWIsS0FBZ0MsSUFBcEMsRUFBMEM7QUFDeEM5QyxnQkFBSSxDQUFDK0QsVUFBTCxDQUFnQkMsV0FBaEIsQ0FBNEJ0RSxJQUFJLENBQUNvRCxjQUFqQztBQUNEO0FBQ0Y7O0FBQ0QsZUFBTyxLQUFLbUIsMkJBQUwsRUFBUDtBQUNELE9Bak1pQztBQWtNbENDLGVBQVMsRUFBRSxtQkFBU3hFLElBQVQsRUFBZXlFLE9BQWYsRUFBd0I7QUFDakMsWUFBSUMsZ0JBQUosRUFBc0I3SCxFQUF0QixFQUEwQkMsSUFBMUIsRUFBZ0N3RCxJQUFoQzs7QUFDQSxZQUFJTixJQUFJLENBQUNvRCxjQUFULEVBQXlCO0FBQ3ZCcEQsY0FBSSxDQUFDb0QsY0FBTCxDQUFvQnJCLFNBQXBCLENBQThCQyxNQUE5QixDQUFxQyxpQkFBckM7QUFDQTFCLGNBQUksR0FBR04sSUFBSSxDQUFDb0QsY0FBTCxDQUFvQkcsZ0JBQXBCLENBQXFDLHFCQUFyQyxDQUFQOztBQUNBLGVBQUsxRyxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3RCxJQUFJLENBQUN6RixNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaEQ2SCw0QkFBZ0IsR0FBR3BFLElBQUksQ0FBQ3pELEVBQUQsQ0FBdkI7QUFDQTZILDRCQUFnQixDQUFDQyxHQUFqQixHQUF1QjNFLElBQUksQ0FBQ3dELElBQTVCO0FBQ0FrQiw0QkFBZ0IsQ0FBQ0UsR0FBakIsR0FBdUJILE9BQXZCO0FBQ0Q7O0FBQ0QsaUJBQU9JLFVBQVUsQ0FBRyxVQUFTaEIsS0FBVCxFQUFnQjtBQUNsQyxtQkFBTyxZQUFXO0FBQ2hCLHFCQUFPN0QsSUFBSSxDQUFDb0QsY0FBTCxDQUFvQnJCLFNBQXBCLENBQThCSyxHQUE5QixDQUFrQyxrQkFBbEMsQ0FBUDtBQUNELGFBRkQ7QUFHRCxXQUprQixDQUloQixJQUpnQixDQUFGLEVBSU4sQ0FKTSxDQUFqQjtBQUtEO0FBQ0YsT0FsTmlDO0FBbU5sQzBDLFdBQUssRUFBRSxlQUFTOUUsSUFBVCxFQUFlK0UsT0FBZixFQUF3QjtBQUM3QixZQUFJckMsSUFBSixFQUFVN0YsRUFBVixFQUFjQyxJQUFkLEVBQW9Cd0QsSUFBcEIsRUFBMEI2QyxRQUExQjs7QUFDQSxZQUFJbkQsSUFBSSxDQUFDb0QsY0FBVCxFQUF5QjtBQUN2QnBELGNBQUksQ0FBQ29ELGNBQUwsQ0FBb0JyQixTQUFwQixDQUE4QkssR0FBOUIsQ0FBa0MsVUFBbEM7O0FBQ0EsY0FBSSxPQUFPMkMsT0FBUCxLQUFtQixRQUFuQixJQUErQkEsT0FBTyxDQUFDRCxLQUEzQyxFQUFrRDtBQUNoREMsbUJBQU8sR0FBR0EsT0FBTyxDQUFDRCxLQUFsQjtBQUNEOztBQUNEeEUsY0FBSSxHQUFHTixJQUFJLENBQUNvRCxjQUFMLENBQW9CRyxnQkFBcEIsQ0FBcUMsd0JBQXJDLENBQVA7QUFDQUosa0JBQVEsR0FBRyxFQUFYOztBQUNBLGVBQUt0RyxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3RCxJQUFJLENBQUN6RixNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaEQ2RixnQkFBSSxHQUFHcEMsSUFBSSxDQUFDekQsRUFBRCxDQUFYOztBQUNBc0csb0JBQVEsQ0FBQzNHLElBQVQsQ0FBY2tHLElBQUksQ0FBQzdCLFdBQUwsR0FBbUJrRSxPQUFqQztBQUNEOztBQUNELGlCQUFPNUIsUUFBUDtBQUNEO0FBQ0YsT0FsT2lDO0FBbU9sQzZCLG1CQUFhLEVBQUUxSixJQW5PbUI7QUFvT2xDMkosZ0JBQVUsRUFBRSxvQkFBU2pGLElBQVQsRUFBZTtBQUN6QixZQUFJQSxJQUFJLENBQUNvRCxjQUFULEVBQXlCO0FBQ3ZCcEQsY0FBSSxDQUFDb0QsY0FBTCxDQUFvQnJCLFNBQXBCLENBQThCSyxHQUE5QixDQUFrQyxlQUFsQzs7QUFDQSxjQUFJcEMsSUFBSSxDQUFDNEQsV0FBVCxFQUFzQjtBQUNwQixtQkFBTzVELElBQUksQ0FBQzRELFdBQUwsQ0FBaUIvQyxXQUFqQixHQUErQixLQUFLQyxPQUFMLENBQWFwQixnQkFBbkQ7QUFDRDtBQUNGO0FBQ0YsT0EzT2lDO0FBNE9sQ3dGLHdCQUFrQixFQUFFNUosSUE1T2M7QUE2T2xDNkosb0JBQWMsRUFBRSx3QkFBU25GLElBQVQsRUFBZW9GLFFBQWYsRUFBeUJDLFNBQXpCLEVBQW9DO0FBQ2xELFlBQUkzQyxJQUFKLEVBQVU3RixFQUFWLEVBQWNDLElBQWQsRUFBb0J3RCxJQUFwQixFQUEwQjZDLFFBQTFCOztBQUNBLFlBQUluRCxJQUFJLENBQUNvRCxjQUFULEVBQXlCO0FBQ3ZCOUMsY0FBSSxHQUFHTixJQUFJLENBQUNvRCxjQUFMLENBQW9CRyxnQkFBcEIsQ0FBcUMsMEJBQXJDLENBQVA7QUFDQUosa0JBQVEsR0FBRyxFQUFYOztBQUNBLGVBQUt0RyxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3RCxJQUFJLENBQUN6RixNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaEQ2RixnQkFBSSxHQUFHcEMsSUFBSSxDQUFDekQsRUFBRCxDQUFYOztBQUNBLGdCQUFJNkYsSUFBSSxDQUFDNEMsUUFBTCxLQUFrQixVQUF0QixFQUFrQztBQUNoQ25DLHNCQUFRLENBQUMzRyxJQUFULENBQWNrRyxJQUFJLENBQUM5SCxLQUFMLEdBQWF3SyxRQUEzQjtBQUNELGFBRkQsTUFFTztBQUNMakMsc0JBQVEsQ0FBQzNHLElBQVQsQ0FBY2tHLElBQUksQ0FBQzZDLEtBQUwsQ0FBVy9ELEtBQVgsR0FBbUIsS0FBSzRELFFBQUwsR0FBZ0IsR0FBakQ7QUFDRDtBQUNGOztBQUNELGlCQUFPakMsUUFBUDtBQUNEO0FBQ0YsT0E1UGlDO0FBNlBsQ3FDLHlCQUFtQixFQUFFbEssSUE3UGE7QUE4UGxDbUssYUFBTyxFQUFFbkssSUE5UHlCO0FBK1BsQ29LLHFCQUFlLEVBQUVwSyxJQS9QaUI7QUFnUWxDcUssYUFBTyxFQUFFLGlCQUFTM0YsSUFBVCxFQUFlO0FBQ3RCLFlBQUlBLElBQUksQ0FBQ29ELGNBQVQsRUFBeUI7QUFDdkIsaUJBQU9wRCxJQUFJLENBQUNvRCxjQUFMLENBQW9CckIsU0FBcEIsQ0FBOEJLLEdBQTlCLENBQWtDLFlBQWxDLENBQVA7QUFDRDtBQUNGLE9BcFFpQztBQXFRbEN3RCxxQkFBZSxFQUFFdEssSUFyUWlCO0FBc1FsQ3VLLGNBQVEsRUFBRSxrQkFBUzdGLElBQVQsRUFBZTtBQUN2QixlQUFPLEtBQUt2RCxJQUFMLENBQVUsT0FBVixFQUFtQnVELElBQW5CLEVBQXlCLGtCQUF6QixDQUFQO0FBQ0QsT0F4UWlDO0FBeVFsQzhGLHNCQUFnQixFQUFFeEssSUF6UWdCO0FBMFFsQ3lLLGNBQVEsRUFBRSxrQkFBUy9GLElBQVQsRUFBZTtBQUN2QixZQUFJQSxJQUFJLENBQUM0RCxXQUFULEVBQXNCO0FBQ3BCNUQsY0FBSSxDQUFDNEQsV0FBTCxDQUFpQi9DLFdBQWpCLEdBQStCLEtBQUtDLE9BQUwsQ0FBYWxCLGNBQTVDO0FBQ0Q7O0FBQ0QsWUFBSUksSUFBSSxDQUFDb0QsY0FBVCxFQUF5QjtBQUN2QixpQkFBT3BELElBQUksQ0FBQ29ELGNBQUwsQ0FBb0JyQixTQUFwQixDQUE4QkssR0FBOUIsQ0FBa0MsYUFBbEMsQ0FBUDtBQUNEO0FBQ0YsT0FqUmlDO0FBa1JsQzRELHNCQUFnQixFQUFFMUssSUFsUmdCO0FBbVJsQzJLLHNCQUFnQixFQUFFM0ssSUFuUmdCO0FBb1JsQzRLLHFCQUFlLEVBQUU1SyxJQXBSaUI7QUFxUmxDNkssbUJBQWEsRUFBRTdLLElBclJtQjtBQXNSbEM4SyxnQkFBVSxFQUFFOUssSUF0UnNCO0FBdVJsQytILHFCQUFlLEVBQUU7QUF2UmlCLEtBQXBDOztBQTBSQTdGLFVBQU0sR0FBRyxrQkFBVztBQUNsQixVQUFJMUIsR0FBSixFQUFTdUssTUFBVCxFQUFpQkMsT0FBakIsRUFBMEIvTSxNQUExQixFQUFrQ2dOLEdBQWxDLEVBQXVDMUosRUFBdkMsRUFBMkNDLElBQTNDOztBQUNBdkQsWUFBTSxHQUFHd0QsU0FBUyxDQUFDLENBQUQsQ0FBbEIsRUFBdUJ1SixPQUFPLEdBQUcsS0FBS3ZKLFNBQVMsQ0FBQ2xDLE1BQWYsR0FBd0JXLE9BQU8sQ0FBQ08sSUFBUixDQUFhZ0IsU0FBYixFQUF3QixDQUF4QixDQUF4QixHQUFxRCxFQUF0Rjs7QUFDQSxXQUFLRixFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3SixPQUFPLENBQUN6TCxNQUE1QixFQUFvQ2dDLEVBQUUsR0FBR0MsSUFBekMsRUFBK0NELEVBQUUsRUFBakQsRUFBcUQ7QUFDbkR3SixjQUFNLEdBQUdDLE9BQU8sQ0FBQ3pKLEVBQUQsQ0FBaEI7O0FBQ0EsYUFBS2YsR0FBTCxJQUFZdUssTUFBWixFQUFvQjtBQUNsQkUsYUFBRyxHQUFHRixNQUFNLENBQUN2SyxHQUFELENBQVo7QUFDQXZDLGdCQUFNLENBQUN1QyxHQUFELENBQU4sR0FBY3lLLEdBQWQ7QUFDRDtBQUNGOztBQUNELGFBQU9oTixNQUFQO0FBQ0QsS0FYRDs7QUFhQSxhQUFTZCxRQUFULENBQWtCOEgsT0FBbEIsRUFBMkJPLE9BQTNCLEVBQW9DO0FBQ2xDLFVBQUkwRixjQUFKLEVBQW9CckcsUUFBcEIsRUFBOEJHLElBQTlCOztBQUNBLFdBQUtDLE9BQUwsR0FBZUEsT0FBZjtBQUNBLFdBQUtrRyxPQUFMLEdBQWVoTyxRQUFRLENBQUNnTyxPQUF4QjtBQUNBLFdBQUs5SSxjQUFMLENBQW9CMEYsZUFBcEIsR0FBc0MsS0FBSzFGLGNBQUwsQ0FBb0IwRixlQUFwQixDQUFvQ3FELE9BQXBDLENBQTRDLE1BQTVDLEVBQW9ELEVBQXBELENBQXRDO0FBQ0EsV0FBS0MsaUJBQUwsR0FBeUIsRUFBekI7QUFDQSxXQUFLQyxTQUFMLEdBQWlCLEVBQWpCO0FBQ0EsV0FBS0MsS0FBTCxHQUFhLEVBQWI7O0FBQ0EsVUFBSSxPQUFPLEtBQUt0RyxPQUFaLEtBQXdCLFFBQTVCLEVBQXNDO0FBQ3BDLGFBQUtBLE9BQUwsR0FBZXVHLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QixLQUFLeEcsT0FBNUIsQ0FBZjtBQUNEOztBQUNELFVBQUksRUFBRSxLQUFLQSxPQUFMLElBQWlCLEtBQUtBLE9BQUwsQ0FBYXlHLFFBQWIsSUFBeUIsSUFBNUMsQ0FBSixFQUF3RDtBQUN0RCxjQUFNLElBQUlDLEtBQUosQ0FBVSwyQkFBVixDQUFOO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLMUcsT0FBTCxDQUFhMkcsUUFBakIsRUFBMkI7QUFDekIsY0FBTSxJQUFJRCxLQUFKLENBQVUsNEJBQVYsQ0FBTjtBQUNEOztBQUNEeE8sY0FBUSxDQUFDME8sU0FBVCxDQUFtQjNLLElBQW5CLENBQXdCLElBQXhCO0FBQ0EsV0FBSytELE9BQUwsQ0FBYTJHLFFBQWIsR0FBd0IsSUFBeEI7QUFDQVYsb0JBQWMsR0FBRyxDQUFDbEcsSUFBSSxHQUFHN0gsUUFBUSxDQUFDMk8saUJBQVQsQ0FBMkIsS0FBSzdHLE9BQWhDLENBQVIsS0FBcUQsSUFBckQsR0FBNERELElBQTVELEdBQW1FLEVBQXBGO0FBQ0EsV0FBS1EsT0FBTCxHQUFldEQsTUFBTSxDQUFDLEVBQUQsRUFBSyxLQUFLRyxjQUFWLEVBQTBCNkksY0FBMUIsRUFBMEMxRixPQUFPLElBQUksSUFBWCxHQUFrQkEsT0FBbEIsR0FBNEIsRUFBdEUsQ0FBckI7O0FBQ0EsVUFBSSxLQUFLQSxPQUFMLENBQWFaLGFBQWIsSUFBOEIsQ0FBQ3pILFFBQVEsQ0FBQzRPLGtCQUFULEVBQW5DLEVBQWtFO0FBQ2hFLGVBQU8sS0FBS3ZHLE9BQUwsQ0FBYVgsUUFBYixDQUFzQnBFLElBQXRCLENBQTJCLElBQTNCLENBQVA7QUFDRDs7QUFDRCxVQUFJLEtBQUsrRSxPQUFMLENBQWFsRCxHQUFiLElBQW9CLElBQXhCLEVBQThCO0FBQzVCLGFBQUtrRCxPQUFMLENBQWFsRCxHQUFiLEdBQW1CLEtBQUsyQyxPQUFMLENBQWErRyxZQUFiLENBQTBCLFFBQTFCLENBQW5CO0FBQ0Q7O0FBQ0QsVUFBSSxDQUFDLEtBQUt4RyxPQUFMLENBQWFsRCxHQUFsQixFQUF1QjtBQUNyQixjQUFNLElBQUlxSixLQUFKLENBQVUsa0JBQVYsQ0FBTjtBQUNEOztBQUNELFVBQUksS0FBS25HLE9BQUwsQ0FBYWxDLGFBQWIsSUFBOEIsS0FBS2tDLE9BQUwsQ0FBYWpDLGlCQUEvQyxFQUFrRTtBQUNoRSxjQUFNLElBQUlvSSxLQUFKLENBQVUsb0dBQVYsQ0FBTjtBQUNEOztBQUNELFVBQUksS0FBS25HLE9BQUwsQ0FBYWpDLGlCQUFqQixFQUFvQztBQUNsQyxhQUFLaUMsT0FBTCxDQUFhbEMsYUFBYixHQUE2QixLQUFLa0MsT0FBTCxDQUFhakMsaUJBQTFDO0FBQ0EsZUFBTyxLQUFLaUMsT0FBTCxDQUFhakMsaUJBQXBCO0FBQ0Q7O0FBQ0QsV0FBS2lDLE9BQUwsQ0FBYWpELE1BQWIsR0FBc0IsS0FBS2lELE9BQUwsQ0FBYWpELE1BQWIsQ0FBb0IwSixXQUFwQixFQUF0Qjs7QUFDQSxVQUFJLENBQUNwSCxRQUFRLEdBQUcsS0FBS3FILG1CQUFMLEVBQVosS0FBMkNySCxRQUFRLENBQUNrRSxVQUF4RCxFQUFvRTtBQUNsRWxFLGdCQUFRLENBQUNrRSxVQUFULENBQW9CQyxXQUFwQixDQUFnQ25FLFFBQWhDO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLVyxPQUFMLENBQWE3QixpQkFBYixLQUFtQyxLQUF2QyxFQUE4QztBQUM1QyxZQUFJLEtBQUs2QixPQUFMLENBQWE3QixpQkFBakIsRUFBb0M7QUFDbEMsZUFBS0EsaUJBQUwsR0FBeUJ4RyxRQUFRLENBQUNnUCxVQUFULENBQW9CLEtBQUszRyxPQUFMLENBQWE3QixpQkFBakMsRUFBb0QsbUJBQXBELENBQXpCO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsZUFBS0EsaUJBQUwsR0FBeUIsS0FBS3NCLE9BQTlCO0FBQ0Q7QUFDRjs7QUFDRCxVQUFJLEtBQUtPLE9BQUwsQ0FBYXBDLFNBQWpCLEVBQTRCO0FBQzFCLFlBQUksS0FBS29DLE9BQUwsQ0FBYXBDLFNBQWIsS0FBMkIsSUFBL0IsRUFBcUM7QUFDbkMsZUFBS2lJLGlCQUFMLEdBQXlCLENBQUMsS0FBS3BHLE9BQU4sQ0FBekI7QUFDRCxTQUZELE1BRU87QUFDTCxlQUFLb0csaUJBQUwsR0FBeUJsTyxRQUFRLENBQUNpUCxXQUFULENBQXFCLEtBQUs1RyxPQUFMLENBQWFwQyxTQUFsQyxFQUE2QyxXQUE3QyxDQUF6QjtBQUNEO0FBQ0Y7O0FBQ0QsV0FBS3hGLElBQUw7QUFDRDs7QUFFRFQsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQnlMLGdCQUFuQixHQUFzQyxZQUFXO0FBQy9DLFVBQUkzSCxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQsRUFBb0J3RCxJQUFwQixFQUEwQjZDLFFBQTFCOztBQUNBN0MsVUFBSSxHQUFHLEtBQUt1RyxLQUFaO0FBQ0ExRCxjQUFRLEdBQUcsRUFBWDs7QUFDQSxXQUFLdEcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHd0QsSUFBSSxDQUFDekYsTUFBekIsRUFBaUNnQyxFQUFFLEdBQUdDLElBQXRDLEVBQTRDRCxFQUFFLEVBQTlDLEVBQWtEO0FBQ2hEbUQsWUFBSSxHQUFHTSxJQUFJLENBQUN6RCxFQUFELENBQVg7O0FBQ0EsWUFBSW1ELElBQUksQ0FBQzRILFFBQVQsRUFBbUI7QUFDakJ6RSxrQkFBUSxDQUFDM0csSUFBVCxDQUFjd0QsSUFBZDtBQUNEO0FBQ0Y7O0FBQ0QsYUFBT21ELFFBQVA7QUFDRCxLQVhEOztBQWFBMUssWUFBUSxDQUFDeUQsU0FBVCxDQUFtQjJMLGdCQUFuQixHQUFzQyxZQUFXO0FBQy9DLFVBQUk3SCxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQsRUFBb0J3RCxJQUFwQixFQUEwQjZDLFFBQTFCOztBQUNBN0MsVUFBSSxHQUFHLEtBQUt1RyxLQUFaO0FBQ0ExRCxjQUFRLEdBQUcsRUFBWDs7QUFDQSxXQUFLdEcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHd0QsSUFBSSxDQUFDekYsTUFBekIsRUFBaUNnQyxFQUFFLEdBQUdDLElBQXRDLEVBQTRDRCxFQUFFLEVBQTlDLEVBQWtEO0FBQ2hEbUQsWUFBSSxHQUFHTSxJQUFJLENBQUN6RCxFQUFELENBQVg7O0FBQ0EsWUFBSSxDQUFDbUQsSUFBSSxDQUFDNEgsUUFBVixFQUFvQjtBQUNsQnpFLGtCQUFRLENBQUMzRyxJQUFULENBQWN3RCxJQUFkO0FBQ0Q7QUFDRjs7QUFDRCxhQUFPbUQsUUFBUDtBQUNELEtBWEQ7O0FBYUExSyxZQUFRLENBQUN5RCxTQUFULENBQW1CNEwsa0JBQW5CLEdBQXdDLFVBQVM5RCxNQUFULEVBQWlCO0FBQ3ZELFVBQUloRSxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQsRUFBb0J3RCxJQUFwQixFQUEwQjZDLFFBQTFCOztBQUNBN0MsVUFBSSxHQUFHLEtBQUt1RyxLQUFaO0FBQ0ExRCxjQUFRLEdBQUcsRUFBWDs7QUFDQSxXQUFLdEcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHd0QsSUFBSSxDQUFDekYsTUFBekIsRUFBaUNnQyxFQUFFLEdBQUdDLElBQXRDLEVBQTRDRCxFQUFFLEVBQTlDLEVBQWtEO0FBQ2hEbUQsWUFBSSxHQUFHTSxJQUFJLENBQUN6RCxFQUFELENBQVg7O0FBQ0EsWUFBSW1ELElBQUksQ0FBQ2dFLE1BQUwsS0FBZ0JBLE1BQXBCLEVBQTRCO0FBQzFCYixrQkFBUSxDQUFDM0csSUFBVCxDQUFjd0QsSUFBZDtBQUNEO0FBQ0Y7O0FBQ0QsYUFBT21ELFFBQVA7QUFDRCxLQVhEOztBQWFBMUssWUFBUSxDQUFDeUQsU0FBVCxDQUFtQjZMLGNBQW5CLEdBQW9DLFlBQVc7QUFDN0MsYUFBTyxLQUFLRCxrQkFBTCxDQUF3QnJQLFFBQVEsQ0FBQ3VQLE1BQWpDLENBQVA7QUFDRCxLQUZEOztBQUlBdlAsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQitMLGlCQUFuQixHQUF1QyxZQUFXO0FBQ2hELGFBQU8sS0FBS0gsa0JBQUwsQ0FBd0JyUCxRQUFRLENBQUN3TCxTQUFqQyxDQUFQO0FBQ0QsS0FGRDs7QUFJQXhMLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJnTSxhQUFuQixHQUFtQyxZQUFXO0FBQzVDLGFBQU8sS0FBS0osa0JBQUwsQ0FBd0JyUCxRQUFRLENBQUMwUCxLQUFqQyxDQUFQO0FBQ0QsS0FGRDs7QUFJQTFQLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJrTSxjQUFuQixHQUFvQyxZQUFXO0FBQzdDLFVBQUlwSSxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQsRUFBb0J3RCxJQUFwQixFQUEwQjZDLFFBQTFCOztBQUNBN0MsVUFBSSxHQUFHLEtBQUt1RyxLQUFaO0FBQ0ExRCxjQUFRLEdBQUcsRUFBWDs7QUFDQSxXQUFLdEcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHd0QsSUFBSSxDQUFDekYsTUFBekIsRUFBaUNnQyxFQUFFLEdBQUdDLElBQXRDLEVBQTRDRCxFQUFFLEVBQTlDLEVBQWtEO0FBQ2hEbUQsWUFBSSxHQUFHTSxJQUFJLENBQUN6RCxFQUFELENBQVg7O0FBQ0EsWUFBSW1ELElBQUksQ0FBQ2dFLE1BQUwsS0FBZ0J2TCxRQUFRLENBQUN3TCxTQUF6QixJQUFzQ2pFLElBQUksQ0FBQ2dFLE1BQUwsS0FBZ0J2TCxRQUFRLENBQUN1UCxNQUFuRSxFQUEyRTtBQUN6RTdFLGtCQUFRLENBQUMzRyxJQUFULENBQWN3RCxJQUFkO0FBQ0Q7QUFDRjs7QUFDRCxhQUFPbUQsUUFBUDtBQUNELEtBWEQ7O0FBYUExSyxZQUFRLENBQUN5RCxTQUFULENBQW1CaEQsSUFBbkIsR0FBMEIsWUFBVztBQUNuQyxVQUFJbVAsU0FBSixFQUFlQyxhQUFmLEVBQThCQyxvQkFBOUIsRUFBb0QxTCxFQUFwRCxFQUF3REMsSUFBeEQsRUFBOER3RCxJQUE5RCxFQUFvRTJDLEtBQXBFOztBQUNBLFVBQUksS0FBSzFDLE9BQUwsQ0FBYWlJLE9BQWIsS0FBeUIsTUFBN0IsRUFBcUM7QUFDbkMsYUFBS2pJLE9BQUwsQ0FBYWtJLFlBQWIsQ0FBMEIsU0FBMUIsRUFBcUMscUJBQXJDO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLbEksT0FBTCxDQUFhd0IsU0FBYixDQUF1QjJHLFFBQXZCLENBQWdDLFVBQWhDLEtBQStDLENBQUMsS0FBS25JLE9BQUwsQ0FBYXdHLGFBQWIsQ0FBMkIsYUFBM0IsQ0FBcEQsRUFBK0Y7QUFDN0YsYUFBS3hHLE9BQUwsQ0FBYUssV0FBYixDQUF5Qm5JLFFBQVEsQ0FBQ2tJLGFBQVQsQ0FBdUIsZ0RBQWdELEtBQUtHLE9BQUwsQ0FBYTFCLGtCQUE3RCxHQUFrRixlQUF6RyxDQUF6QjtBQUNEOztBQUNELFVBQUksS0FBS3VILGlCQUFMLENBQXVCOUwsTUFBM0IsRUFBbUM7QUFDakMwTiw0QkFBb0IsR0FBSSxVQUFTMUUsS0FBVCxFQUFnQjtBQUN0QyxpQkFBTyxZQUFXO0FBQ2hCLGdCQUFJQSxLQUFLLENBQUM4RSxlQUFWLEVBQTJCO0FBQ3pCOUUsbUJBQUssQ0FBQzhFLGVBQU4sQ0FBc0J0RSxVQUF0QixDQUFpQ0MsV0FBakMsQ0FBNkNULEtBQUssQ0FBQzhFLGVBQW5EO0FBQ0Q7O0FBQ0Q5RSxpQkFBSyxDQUFDOEUsZUFBTixHQUF3QjdCLFFBQVEsQ0FBQ25HLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBeEI7O0FBQ0FrRCxpQkFBSyxDQUFDOEUsZUFBTixDQUFzQkYsWUFBdEIsQ0FBbUMsTUFBbkMsRUFBMkMsTUFBM0M7O0FBQ0EsZ0JBQUs1RSxLQUFLLENBQUMvQyxPQUFOLENBQWN0QyxRQUFkLElBQTBCLElBQTNCLElBQW9DcUYsS0FBSyxDQUFDL0MsT0FBTixDQUFjdEMsUUFBZCxHQUF5QixDQUFqRSxFQUFvRTtBQUNsRXFGLG1CQUFLLENBQUM4RSxlQUFOLENBQXNCRixZQUF0QixDQUFtQyxVQUFuQyxFQUErQyxVQUEvQztBQUNEOztBQUNENUUsaUJBQUssQ0FBQzhFLGVBQU4sQ0FBc0JuSSxTQUF0QixHQUFrQyxpQkFBbEM7O0FBQ0EsZ0JBQUlxRCxLQUFLLENBQUMvQyxPQUFOLENBQWNsQyxhQUFkLElBQStCLElBQW5DLEVBQXlDO0FBQ3ZDaUYsbUJBQUssQ0FBQzhFLGVBQU4sQ0FBc0JGLFlBQXRCLENBQW1DLFFBQW5DLEVBQTZDNUUsS0FBSyxDQUFDL0MsT0FBTixDQUFjbEMsYUFBM0Q7QUFDRDs7QUFDRCxnQkFBSWlGLEtBQUssQ0FBQy9DLE9BQU4sQ0FBYzNCLE9BQWQsSUFBeUIsSUFBN0IsRUFBbUM7QUFDakMwRSxtQkFBSyxDQUFDOEUsZUFBTixDQUFzQkYsWUFBdEIsQ0FBbUMsU0FBbkMsRUFBOEM1RSxLQUFLLENBQUMvQyxPQUFOLENBQWMzQixPQUE1RDtBQUNEOztBQUNEMEUsaUJBQUssQ0FBQzhFLGVBQU4sQ0FBc0JwRCxLQUF0QixDQUE0QnFELFVBQTVCLEdBQXlDLFFBQXpDO0FBQ0EvRSxpQkFBSyxDQUFDOEUsZUFBTixDQUFzQnBELEtBQXRCLENBQTRCc0QsUUFBNUIsR0FBdUMsVUFBdkM7QUFDQWhGLGlCQUFLLENBQUM4RSxlQUFOLENBQXNCcEQsS0FBdEIsQ0FBNEJ1RCxHQUE1QixHQUFrQyxHQUFsQztBQUNFakYsaUJBQUssQ0FBQzhFLGVBQU4sQ0FBc0JJLEVBQXRCLEdBQTJCLFFBQTNCO0FBQ0FsRixpQkFBSyxDQUFDOEUsZUFBTixDQUFzQnBELEtBQXRCLENBQTRCOUssSUFBNUIsR0FBbUMsR0FBbkM7QUFDRm9KLGlCQUFLLENBQUM4RSxlQUFOLENBQXNCcEQsS0FBdEIsQ0FBNEJ4SyxNQUE1QixHQUFxQyxHQUFyQztBQUNBOEksaUJBQUssQ0FBQzhFLGVBQU4sQ0FBc0JwRCxLQUF0QixDQUE0Qi9ELEtBQTVCLEdBQW9DLEdBQXBDO0FBQ0FzRixvQkFBUSxDQUFDQyxhQUFULENBQXVCbEQsS0FBSyxDQUFDL0MsT0FBTixDQUFjNUIsb0JBQXJDLEVBQTJEMEIsV0FBM0QsQ0FBdUVpRCxLQUFLLENBQUM4RSxlQUE3RTtBQUNBLG1CQUFPOUUsS0FBSyxDQUFDOEUsZUFBTixDQUFzQnZNLGdCQUF0QixDQUF1QyxRQUF2QyxFQUFpRCxZQUFXO0FBQ2pFLGtCQUFJNEQsSUFBSixFQUFVNkcsS0FBVixFQUFpQmhLLEVBQWpCLEVBQXFCQyxJQUFyQjs7QUFDQStKLG1CQUFLLEdBQUdoRCxLQUFLLENBQUM4RSxlQUFOLENBQXNCOUIsS0FBOUI7O0FBQ0Esa0JBQUlBLEtBQUssQ0FBQ2hNLE1BQVYsRUFBa0I7QUFDaEIscUJBQUtnQyxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUcrSixLQUFLLENBQUNoTSxNQUExQixFQUFrQ2dDLEVBQUUsR0FBR0MsSUFBdkMsRUFBNkNELEVBQUUsRUFBL0MsRUFBbUQ7QUFDakRtRCxzQkFBSSxHQUFHNkcsS0FBSyxDQUFDaEssRUFBRCxDQUFaOztBQUNBZ0gsdUJBQUssQ0FBQ21GLE9BQU4sQ0FBY2hKLElBQWQ7QUFDRDtBQUNGOztBQUNENkQsbUJBQUssQ0FBQ3BILElBQU4sQ0FBVyxZQUFYLEVBQXlCb0ssS0FBekI7O0FBQ0EscUJBQU8wQixvQkFBb0IsRUFBM0I7QUFDRCxhQVhNLENBQVA7QUFZRCxXQXBDRDtBQXFDRCxTQXRDc0IsQ0FzQ3BCLElBdENvQixDQUF2Qjs7QUF1Q0FBLDRCQUFvQjtBQUNyQjs7QUFDRCxXQUFLVSxHQUFMLEdBQVcsQ0FBQzNJLElBQUksR0FBR2hJLE1BQU0sQ0FBQzJRLEdBQWYsS0FBdUIsSUFBdkIsR0FBOEIzSSxJQUE5QixHQUFxQ2hJLE1BQU0sQ0FBQzRRLFNBQXZEO0FBQ0FqRyxXQUFLLEdBQUcsS0FBS3ZGLE1BQWI7O0FBQ0EsV0FBS2IsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHbUcsS0FBSyxDQUFDcEksTUFBMUIsRUFBa0NnQyxFQUFFLEdBQUdDLElBQXZDLEVBQTZDRCxFQUFFLEVBQS9DLEVBQW1EO0FBQ2pEd0wsaUJBQVMsR0FBR3BGLEtBQUssQ0FBQ3BHLEVBQUQsQ0FBakI7QUFDQSxhQUFLeEQsRUFBTCxDQUFRZ1AsU0FBUixFQUFtQixLQUFLdkgsT0FBTCxDQUFhdUgsU0FBYixDQUFuQjtBQUNEOztBQUNELFdBQUtoUCxFQUFMLENBQVEsZ0JBQVIsRUFBMkIsVUFBU3dLLEtBQVQsRUFBZ0I7QUFDekMsZUFBTyxZQUFXO0FBQ2hCLGlCQUFPQSxLQUFLLENBQUNzRix5QkFBTixFQUFQO0FBQ0QsU0FGRDtBQUdELE9BSnlCLENBSXZCLElBSnVCLENBQTFCO0FBS0EsV0FBSzlQLEVBQUwsQ0FBUSxhQUFSLEVBQXdCLFVBQVN3SyxLQUFULEVBQWdCO0FBQ3RDLGVBQU8sWUFBVztBQUNoQixpQkFBT0EsS0FBSyxDQUFDc0YseUJBQU4sRUFBUDtBQUNELFNBRkQ7QUFHRCxPQUpzQixDQUlwQixJQUpvQixDQUF2QjtBQUtBLFdBQUs5UCxFQUFMLENBQVEsVUFBUixFQUFxQixVQUFTd0ssS0FBVCxFQUFnQjtBQUNuQyxlQUFPLFVBQVM3RCxJQUFULEVBQWU7QUFDcEIsaUJBQU82RCxLQUFLLENBQUNwSCxJQUFOLENBQVcsVUFBWCxFQUF1QnVELElBQXZCLENBQVA7QUFDRCxTQUZEO0FBR0QsT0FKbUIsQ0FJakIsSUFKaUIsQ0FBcEI7QUFLQSxXQUFLM0csRUFBTCxDQUFRLFVBQVIsRUFBcUIsVUFBU3dLLEtBQVQsRUFBZ0I7QUFDbkMsZUFBTyxVQUFTN0QsSUFBVCxFQUFlO0FBQ3BCLGNBQUk2RCxLQUFLLENBQUNxRSxhQUFOLEdBQXNCck4sTUFBdEIsS0FBaUMsQ0FBakMsSUFBc0NnSixLQUFLLENBQUNvRSxpQkFBTixHQUEwQnBOLE1BQTFCLEtBQXFDLENBQTNFLElBQWdGZ0osS0FBSyxDQUFDa0UsY0FBTixHQUF1QmxOLE1BQXZCLEtBQWtDLENBQXRILEVBQXlIO0FBQ3ZILG1CQUFPZ0ssVUFBVSxDQUFFLFlBQVc7QUFDNUIscUJBQU9oQixLQUFLLENBQUNwSCxJQUFOLENBQVcsZUFBWCxDQUFQO0FBQ0QsYUFGZ0IsRUFFYixDQUZhLENBQWpCO0FBR0Q7QUFDRixTQU5EO0FBT0QsT0FSbUIsQ0FRakIsSUFSaUIsQ0FBcEI7O0FBU0E2TCxtQkFBYSxHQUFHLHVCQUFTaFAsQ0FBVCxFQUFZO0FBQzFCQSxTQUFDLENBQUN5SyxlQUFGOztBQUNBLFlBQUl6SyxDQUFDLENBQUN3SyxjQUFOLEVBQXNCO0FBQ3BCLGlCQUFPeEssQ0FBQyxDQUFDd0ssY0FBRixFQUFQO0FBQ0QsU0FGRCxNQUVPO0FBQ0wsaUJBQU94SyxDQUFDLENBQUM4UCxXQUFGLEdBQWdCLEtBQXZCO0FBQ0Q7QUFDRixPQVBEOztBQVFBLFdBQUt4QyxTQUFMLEdBQWlCLENBQ2Y7QUFDRXJHLGVBQU8sRUFBRSxLQUFLQSxPQURoQjtBQUVFN0MsY0FBTSxFQUFFO0FBQ04sdUJBQWMsVUFBU21HLEtBQVQsRUFBZ0I7QUFDNUIsbUJBQU8sVUFBU3ZLLENBQVQsRUFBWTtBQUNqQixxQkFBT3VLLEtBQUssQ0FBQ3BILElBQU4sQ0FBVyxXQUFYLEVBQXdCbkQsQ0FBeEIsQ0FBUDtBQUNELGFBRkQ7QUFHRCxXQUpZLENBSVYsSUFKVSxDQURQO0FBTU4sdUJBQWMsVUFBU3VLLEtBQVQsRUFBZ0I7QUFDNUIsbUJBQU8sVUFBU3ZLLENBQVQsRUFBWTtBQUNqQmdQLDJCQUFhLENBQUNoUCxDQUFELENBQWI7QUFDQSxxQkFBT3VLLEtBQUssQ0FBQ3BILElBQU4sQ0FBVyxXQUFYLEVBQXdCbkQsQ0FBeEIsQ0FBUDtBQUNELGFBSEQ7QUFJRCxXQUxZLENBS1YsSUFMVSxDQU5QO0FBWU4sc0JBQWEsVUFBU3VLLEtBQVQsRUFBZ0I7QUFDM0IsbUJBQU8sVUFBU3ZLLENBQVQsRUFBWTtBQUNqQixrQkFBSStQLElBQUo7O0FBQ0Esa0JBQUk7QUFDRkEsb0JBQUksR0FBRy9QLENBQUMsQ0FBQ2dRLFlBQUYsQ0FBZUMsYUFBdEI7QUFDRCxlQUZELENBRUUsT0FBT0MsTUFBUCxFQUFlLENBQUU7O0FBQ25CbFEsZUFBQyxDQUFDZ1EsWUFBRixDQUFlRyxVQUFmLEdBQTRCLFdBQVdKLElBQVgsSUFBbUIsZUFBZUEsSUFBbEMsR0FBeUMsTUFBekMsR0FBa0QsTUFBOUU7QUFDQWYsMkJBQWEsQ0FBQ2hQLENBQUQsQ0FBYjtBQUNBLHFCQUFPdUssS0FBSyxDQUFDcEgsSUFBTixDQUFXLFVBQVgsRUFBdUJuRCxDQUF2QixDQUFQO0FBQ0QsYUFSRDtBQVNELFdBVlcsQ0FVVCxJQVZTLENBWk47QUF1Qk4sdUJBQWMsVUFBU3VLLEtBQVQsRUFBZ0I7QUFDNUIsbUJBQU8sVUFBU3ZLLENBQVQsRUFBWTtBQUNqQixxQkFBT3VLLEtBQUssQ0FBQ3BILElBQU4sQ0FBVyxXQUFYLEVBQXdCbkQsQ0FBeEIsQ0FBUDtBQUNELGFBRkQ7QUFHRCxXQUpZLENBSVYsSUFKVSxDQXZCUDtBQTRCTixrQkFBUyxVQUFTdUssS0FBVCxFQUFnQjtBQUN2QixtQkFBTyxVQUFTdkssQ0FBVCxFQUFZO0FBQ2pCZ1AsMkJBQWEsQ0FBQ2hQLENBQUQsQ0FBYjtBQUNBLHFCQUFPdUssS0FBSyxDQUFDL0IsSUFBTixDQUFXeEksQ0FBWCxDQUFQO0FBQ0QsYUFIRDtBQUlELFdBTE8sQ0FLTCxJQUxLLENBNUJGO0FBa0NOLHFCQUFZLFVBQVN1SyxLQUFULEVBQWdCO0FBQzFCLG1CQUFPLFVBQVN2SyxDQUFULEVBQVk7QUFDakIscUJBQU91SyxLQUFLLENBQUNwSCxJQUFOLENBQVcsU0FBWCxFQUFzQm5ELENBQXRCLENBQVA7QUFDRCxhQUZEO0FBR0QsV0FKVSxDQUlSLElBSlE7QUFsQ0w7QUFGVixPQURlLENBQWpCO0FBNkNBLFdBQUtxTixpQkFBTCxDQUF1QitDLE9BQXZCLENBQWdDLFVBQVM3RixLQUFULEVBQWdCO0FBQzlDLGVBQU8sVUFBUzhGLGdCQUFULEVBQTJCO0FBQ2hDLGlCQUFPOUYsS0FBSyxDQUFDK0MsU0FBTixDQUFnQnBLLElBQWhCLENBQXFCO0FBQzFCK0QsbUJBQU8sRUFBRW9KLGdCQURpQjtBQUUxQmpNLGtCQUFNLEVBQUU7QUFDTix1QkFBUyxlQUFTa00sR0FBVCxFQUFjO0FBQ3JCLG9CQUFLRCxnQkFBZ0IsS0FBSzlGLEtBQUssQ0FBQ3RELE9BQTVCLElBQXlDcUosR0FBRyxDQUFDclEsTUFBSixLQUFlc0ssS0FBSyxDQUFDdEQsT0FBckIsSUFBZ0M5SCxRQUFRLENBQUNvUixhQUFULENBQXVCRCxHQUFHLENBQUNyUSxNQUEzQixFQUFtQ3NLLEtBQUssQ0FBQ3RELE9BQU4sQ0FBY3dHLGFBQWQsQ0FBNEIsYUFBNUIsQ0FBbkMsQ0FBN0UsRUFBOEo7QUFDNUpsRCx1QkFBSyxDQUFDOEUsZUFBTixDQUFzQjFPLEtBQXRCO0FBQ0Q7O0FBQ0QsdUJBQU8sSUFBUDtBQUNEO0FBTks7QUFGa0IsV0FBckIsQ0FBUDtBQVdELFNBWkQ7QUFhRCxPQWQ4QixDQWM1QixJQWQ0QixDQUEvQjtBQWVBLFdBQUs2UCxNQUFMO0FBQ0EsYUFBTyxLQUFLaEosT0FBTCxDQUFhNUgsSUFBYixDQUFrQjZDLElBQWxCLENBQXVCLElBQXZCLENBQVA7QUFDRCxLQXRKRDs7QUF3SkF0RCxZQUFRLENBQUN5RCxTQUFULENBQW1CNk4sT0FBbkIsR0FBNkIsWUFBVztBQUN0QyxVQUFJekosSUFBSjs7QUFDQSxXQUFLMEosT0FBTDtBQUNBLFdBQUtDLGNBQUwsQ0FBb0IsSUFBcEI7O0FBQ0EsVUFBSSxDQUFDM0osSUFBSSxHQUFHLEtBQUtxSSxlQUFiLEtBQWlDLElBQWpDLEdBQXdDckksSUFBSSxDQUFDK0QsVUFBN0MsR0FBMEQsS0FBSyxDQUFuRSxFQUFzRTtBQUNwRSxhQUFLc0UsZUFBTCxDQUFxQnRFLFVBQXJCLENBQWdDQyxXQUFoQyxDQUE0QyxLQUFLcUUsZUFBakQ7QUFDQSxhQUFLQSxlQUFMLEdBQXVCLElBQXZCO0FBQ0Q7O0FBQ0QsYUFBTyxLQUFLcEksT0FBTCxDQUFhMkcsUUFBcEI7QUFDQSxhQUFPek8sUUFBUSxDQUFDME8sU0FBVCxDQUFtQjdKLE1BQW5CLENBQTBCN0UsUUFBUSxDQUFDME8sU0FBVCxDQUFtQitDLE9BQW5CLENBQTJCLElBQTNCLENBQTFCLEVBQTRELENBQTVELENBQVA7QUFDRCxLQVZEOztBQVlBelIsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQmlOLHlCQUFuQixHQUErQyxZQUFXO0FBQ3hELFVBQUlnQixXQUFKLEVBQWlCbkssSUFBakIsRUFBdUJvSyxVQUF2QixFQUFtQ0MsY0FBbkMsRUFBbURDLG1CQUFuRCxFQUF3RXpOLEVBQXhFLEVBQTRFQyxJQUE1RSxFQUFrRndELElBQWxGOztBQUNBK0osb0JBQWMsR0FBRyxDQUFqQjtBQUNBRCxnQkFBVSxHQUFHLENBQWI7QUFDQUQsaUJBQVcsR0FBRyxLQUFLL0IsY0FBTCxFQUFkOztBQUNBLFVBQUkrQixXQUFXLENBQUN0UCxNQUFoQixFQUF3QjtBQUN0QnlGLFlBQUksR0FBRyxLQUFLOEgsY0FBTCxFQUFQOztBQUNBLGFBQUt2TCxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3RCxJQUFJLENBQUN6RixNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaERtRCxjQUFJLEdBQUdNLElBQUksQ0FBQ3pELEVBQUQsQ0FBWDtBQUNBd04sd0JBQWMsSUFBSXJLLElBQUksQ0FBQ3VLLE1BQUwsQ0FBWWxGLFNBQTlCO0FBQ0ErRSxvQkFBVSxJQUFJcEssSUFBSSxDQUFDdUssTUFBTCxDQUFZQyxLQUExQjtBQUNEOztBQUNERiwyQkFBbUIsR0FBRyxNQUFNRCxjQUFOLEdBQXVCRCxVQUE3QztBQUNELE9BUkQsTUFRTztBQUNMRSwyQkFBbUIsR0FBRyxHQUF0QjtBQUNEOztBQUNELGFBQU8sS0FBSzdOLElBQUwsQ0FBVSxxQkFBVixFQUFpQzZOLG1CQUFqQyxFQUFzREYsVUFBdEQsRUFBa0VDLGNBQWxFLENBQVA7QUFDRCxLQWpCRDs7QUFtQkE1UixZQUFRLENBQUN5RCxTQUFULENBQW1CdU8sYUFBbkIsR0FBbUMsVUFBU0MsQ0FBVCxFQUFZO0FBQzdDLFVBQUksT0FBTyxLQUFLNUosT0FBTCxDQUFhNUMsU0FBcEIsS0FBa0MsVUFBdEMsRUFBa0Q7QUFDaEQsZUFBTyxLQUFLNEMsT0FBTCxDQUFhNUMsU0FBYixDQUF1QndNLENBQXZCLENBQVA7QUFDRCxPQUZELE1BRU87QUFDTCxlQUFPLEtBQUssS0FBSzVKLE9BQUwsQ0FBYTVDLFNBQWxCLElBQStCLEtBQUs0QyxPQUFMLENBQWE5QyxjQUFiLEdBQThCLE1BQU0wTSxDQUFOLEdBQVUsR0FBeEMsR0FBOEMsRUFBN0UsQ0FBUDtBQUNEO0FBQ0YsS0FORDs7QUFRQWpTLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUI4RSxlQUFuQixHQUFxQyxZQUFXO0FBQzlDLFVBQUkySixnQkFBSixFQUFzQkMsTUFBdEIsRUFBOEJDLFlBQTlCLEVBQTRDQyxJQUE1Qzs7QUFDQSxVQUFJSCxnQkFBZ0IsR0FBRyxLQUFLbkQsbUJBQUwsRUFBdkIsRUFBbUQ7QUFDakQsZUFBT21ELGdCQUFQO0FBQ0Q7O0FBQ0RFLGtCQUFZLEdBQUcsNkJBQWY7O0FBQ0EsVUFBSSxLQUFLL0osT0FBTCxDQUFheEIsZ0JBQWpCLEVBQW1DO0FBQ2pDdUwsb0JBQVksSUFBSSxRQUFRLEtBQUsvSixPQUFMLENBQWF4QixnQkFBckIsR0FBd0MsTUFBeEQ7QUFDRDs7QUFDRHVMLGtCQUFZLElBQUksaUNBQWtDLEtBQUtKLGFBQUwsQ0FBbUIsQ0FBbkIsQ0FBbEMsR0FBMkQsS0FBM0QsSUFBb0UsS0FBSzNKLE9BQUwsQ0FBYTlDLGNBQWIsR0FBOEIscUJBQTlCLEdBQXNELEtBQUssQ0FBL0gsSUFBb0ksb0RBQXBKO0FBQ0E0TSxZQUFNLEdBQUduUyxRQUFRLENBQUNrSSxhQUFULENBQXVCa0ssWUFBdkIsQ0FBVDs7QUFDQSxVQUFJLEtBQUt0SyxPQUFMLENBQWFpSSxPQUFiLEtBQXlCLE1BQTdCLEVBQXFDO0FBQ25Dc0MsWUFBSSxHQUFHclMsUUFBUSxDQUFDa0ksYUFBVCxDQUF1QixvQkFBb0IsS0FBS0csT0FBTCxDQUFhbEQsR0FBakMsR0FBdUMsOENBQXZDLEdBQXdGLEtBQUtrRCxPQUFMLENBQWFqRCxNQUFyRyxHQUE4RyxZQUFySSxDQUFQO0FBQ0FpTixZQUFJLENBQUNsSyxXQUFMLENBQWlCZ0ssTUFBakI7QUFDRCxPQUhELE1BR087QUFDTCxhQUFLckssT0FBTCxDQUFha0ksWUFBYixDQUEwQixTQUExQixFQUFxQyxxQkFBckM7QUFDQSxhQUFLbEksT0FBTCxDQUFha0ksWUFBYixDQUEwQixRQUExQixFQUFvQyxLQUFLM0gsT0FBTCxDQUFhakQsTUFBakQ7QUFDRDs7QUFDRCxhQUFPaU4sSUFBSSxJQUFJLElBQVIsR0FBZUEsSUFBZixHQUFzQkYsTUFBN0I7QUFDRCxLQW5CRDs7QUFxQkFuUyxZQUFRLENBQUN5RCxTQUFULENBQW1Cc0wsbUJBQW5CLEdBQXlDLFlBQVc7QUFDbEQsVUFBSXJILFFBQUosRUFBYzRLLFdBQWQsRUFBMkJ2QyxPQUEzQixFQUFvQzNMLEVBQXBDLEVBQXdDQyxJQUF4QyxFQUE4Q3dELElBQTlDOztBQUNBeUssaUJBQVcsR0FBRyxxQkFBU0MsUUFBVCxFQUFtQjtBQUMvQixZQUFJQyxFQUFKLEVBQVFwTyxFQUFSLEVBQVlDLElBQVo7O0FBQ0EsYUFBS0QsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHa08sUUFBUSxDQUFDblEsTUFBN0IsRUFBcUNnQyxFQUFFLEdBQUdDLElBQTFDLEVBQWdERCxFQUFFLEVBQWxELEVBQXNEO0FBQ3BEb08sWUFBRSxHQUFHRCxRQUFRLENBQUNuTyxFQUFELENBQWI7O0FBQ0EsY0FBSSxxQkFBcUI2RCxJQUFyQixDQUEwQnVLLEVBQUUsQ0FBQ3pLLFNBQTdCLENBQUosRUFBNkM7QUFDM0MsbUJBQU95SyxFQUFQO0FBQ0Q7QUFDRjtBQUNGLE9BUkQ7O0FBU0EzSyxVQUFJLEdBQUcsQ0FBQyxLQUFELEVBQVEsTUFBUixDQUFQOztBQUNBLFdBQUt6RCxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3RCxJQUFJLENBQUN6RixNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaEQyTCxlQUFPLEdBQUdsSSxJQUFJLENBQUN6RCxFQUFELENBQWQ7O0FBQ0EsWUFBSXNELFFBQVEsR0FBRzRLLFdBQVcsQ0FBQyxLQUFLeEssT0FBTCxDQUFhRSxvQkFBYixDQUFrQytILE9BQWxDLENBQUQsQ0FBMUIsRUFBd0U7QUFDdEUsaUJBQU9ySSxRQUFQO0FBQ0Q7QUFDRjtBQUNGLEtBbEJEOztBQW9CQTFILFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJnUCxtQkFBbkIsR0FBeUMsWUFBVztBQUNsRCxVQUFJQyxnQkFBSixFQUFzQjlPLEtBQXRCLEVBQTZCK08sUUFBN0IsRUFBdUN2TyxFQUF2QyxFQUEyQ0MsSUFBM0MsRUFBaUR3RCxJQUFqRCxFQUF1RDZDLFFBQXZEOztBQUNBN0MsVUFBSSxHQUFHLEtBQUtzRyxTQUFaO0FBQ0F6RCxjQUFRLEdBQUcsRUFBWDs7QUFDQSxXQUFLdEcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHd0QsSUFBSSxDQUFDekYsTUFBekIsRUFBaUNnQyxFQUFFLEdBQUdDLElBQXRDLEVBQTRDRCxFQUFFLEVBQTlDLEVBQWtEO0FBQ2hEc08sd0JBQWdCLEdBQUc3SyxJQUFJLENBQUN6RCxFQUFELENBQXZCOztBQUNBc0csZ0JBQVEsQ0FBQzNHLElBQVQsQ0FBZSxZQUFXO0FBQ3hCLGNBQUl5RyxLQUFKLEVBQVdvSSxTQUFYOztBQUNBcEksZUFBSyxHQUFHa0ksZ0JBQWdCLENBQUN6TixNQUF6QjtBQUNBMk4sbUJBQVMsR0FBRyxFQUFaOztBQUNBLGVBQUtoUCxLQUFMLElBQWM0RyxLQUFkLEVBQXFCO0FBQ25CbUksb0JBQVEsR0FBR25JLEtBQUssQ0FBQzVHLEtBQUQsQ0FBaEI7O0FBQ0FnUCxxQkFBUyxDQUFDN08sSUFBVixDQUFlMk8sZ0JBQWdCLENBQUM1SyxPQUFqQixDQUF5Qm5FLGdCQUF6QixDQUEwQ0MsS0FBMUMsRUFBaUQrTyxRQUFqRCxFQUEyRCxLQUEzRCxDQUFmO0FBQ0Q7O0FBQ0QsaUJBQU9DLFNBQVA7QUFDRCxTQVRhLEVBQWQ7QUFVRDs7QUFDRCxhQUFPbEksUUFBUDtBQUNELEtBbEJEOztBQW9CQTFLLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJvUCxvQkFBbkIsR0FBMEMsWUFBVztBQUNuRCxVQUFJSCxnQkFBSixFQUFzQjlPLEtBQXRCLEVBQTZCK08sUUFBN0IsRUFBdUN2TyxFQUF2QyxFQUEyQ0MsSUFBM0MsRUFBaUR3RCxJQUFqRCxFQUF1RDZDLFFBQXZEOztBQUNBN0MsVUFBSSxHQUFHLEtBQUtzRyxTQUFaO0FBQ0F6RCxjQUFRLEdBQUcsRUFBWDs7QUFDQSxXQUFLdEcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHd0QsSUFBSSxDQUFDekYsTUFBekIsRUFBaUNnQyxFQUFFLEdBQUdDLElBQXRDLEVBQTRDRCxFQUFFLEVBQTlDLEVBQWtEO0FBQ2hEc08sd0JBQWdCLEdBQUc3SyxJQUFJLENBQUN6RCxFQUFELENBQXZCOztBQUNBc0csZ0JBQVEsQ0FBQzNHLElBQVQsQ0FBZSxZQUFXO0FBQ3hCLGNBQUl5RyxLQUFKLEVBQVdvSSxTQUFYOztBQUNBcEksZUFBSyxHQUFHa0ksZ0JBQWdCLENBQUN6TixNQUF6QjtBQUNBMk4sbUJBQVMsR0FBRyxFQUFaOztBQUNBLGVBQUtoUCxLQUFMLElBQWM0RyxLQUFkLEVBQXFCO0FBQ25CbUksb0JBQVEsR0FBR25JLEtBQUssQ0FBQzVHLEtBQUQsQ0FBaEI7O0FBQ0FnUCxxQkFBUyxDQUFDN08sSUFBVixDQUFlMk8sZ0JBQWdCLENBQUM1SyxPQUFqQixDQUF5Qm5ELG1CQUF6QixDQUE2Q2YsS0FBN0MsRUFBb0QrTyxRQUFwRCxFQUE4RCxLQUE5RCxDQUFmO0FBQ0Q7O0FBQ0QsaUJBQU9DLFNBQVA7QUFDRCxTQVRhLEVBQWQ7QUFVRDs7QUFDRCxhQUFPbEksUUFBUDtBQUNELEtBbEJEOztBQW9CQTFLLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUI4TixPQUFuQixHQUE2QixZQUFXO0FBQ3RDLFVBQUloSyxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQsRUFBb0J3RCxJQUFwQixFQUEwQjZDLFFBQTFCOztBQUNBLFdBQUt3RCxpQkFBTCxDQUF1QitDLE9BQXZCLENBQStCLFVBQVNuSixPQUFULEVBQWtCO0FBQy9DLGVBQU9BLE9BQU8sQ0FBQ3dCLFNBQVIsQ0FBa0JDLE1BQWxCLENBQXlCLGNBQXpCLENBQVA7QUFDRCxPQUZEO0FBR0EsV0FBS3NKLG9CQUFMO0FBQ0FoTCxVQUFJLEdBQUcsS0FBS3VHLEtBQVo7QUFDQTFELGNBQVEsR0FBRyxFQUFYOztBQUNBLFdBQUt0RyxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd3RCxJQUFJLENBQUN6RixNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaERtRCxZQUFJLEdBQUdNLElBQUksQ0FBQ3pELEVBQUQsQ0FBWDs7QUFDQXNHLGdCQUFRLENBQUMzRyxJQUFULENBQWMsS0FBSytPLFlBQUwsQ0FBa0J2TCxJQUFsQixDQUFkO0FBQ0Q7O0FBQ0QsYUFBT21ELFFBQVA7QUFDRCxLQWJEOztBQWVBMUssWUFBUSxDQUFDeUQsU0FBVCxDQUFtQjROLE1BQW5CLEdBQTRCLFlBQVc7QUFDckMsV0FBS25ELGlCQUFMLENBQXVCK0MsT0FBdkIsQ0FBK0IsVUFBU25KLE9BQVQsRUFBa0I7QUFDL0MsZUFBT0EsT0FBTyxDQUFDd0IsU0FBUixDQUFrQkssR0FBbEIsQ0FBc0IsY0FBdEIsQ0FBUDtBQUNELE9BRkQ7QUFHQSxhQUFPLEtBQUs4SSxtQkFBTCxFQUFQO0FBQ0QsS0FMRDs7QUFPQXpTLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJ3SCxRQUFuQixHQUE4QixVQUFTQyxJQUFULEVBQWU7QUFDM0MsVUFBSTZILE1BQUosRUFBWW5PLENBQVosRUFBZW9PLFlBQWYsRUFBNkJDLFlBQTdCLEVBQTJDQyxJQUEzQyxFQUFpREMsS0FBakQsRUFBd0QvTyxFQUF4RCxFQUE0REMsSUFBNUQ7O0FBQ0EyTyxrQkFBWSxHQUFHLENBQWY7QUFDQUMsa0JBQVksR0FBRyxHQUFmOztBQUNBLFVBQUkvSCxJQUFJLEdBQUcsQ0FBWCxFQUFjO0FBQ1ppSSxhQUFLLEdBQUcsQ0FBQyxJQUFELEVBQU8sSUFBUCxFQUFhLElBQWIsRUFBbUIsSUFBbkIsRUFBeUIsR0FBekIsQ0FBUjs7QUFDQSxhQUFLdk8sQ0FBQyxHQUFHUixFQUFFLEdBQUcsQ0FBVCxFQUFZQyxJQUFJLEdBQUc4TyxLQUFLLENBQUMvUSxNQUE5QixFQUFzQ2dDLEVBQUUsR0FBR0MsSUFBM0MsRUFBaURPLENBQUMsR0FBRyxFQUFFUixFQUF2RCxFQUEyRDtBQUN6RDhPLGNBQUksR0FBR0MsS0FBSyxDQUFDdk8sQ0FBRCxDQUFaO0FBQ0FtTyxnQkFBTSxHQUFHSyxJQUFJLENBQUNDLEdBQUwsQ0FBUyxLQUFLaEwsT0FBTCxDQUFhdkMsWUFBdEIsRUFBb0MsSUFBSWxCLENBQXhDLElBQTZDLEVBQXREOztBQUNBLGNBQUlzRyxJQUFJLElBQUk2SCxNQUFaLEVBQW9CO0FBQ2xCQyx3QkFBWSxHQUFHOUgsSUFBSSxHQUFHa0ksSUFBSSxDQUFDQyxHQUFMLENBQVMsS0FBS2hMLE9BQUwsQ0FBYXZDLFlBQXRCLEVBQW9DLElBQUlsQixDQUF4QyxDQUF0QjtBQUNBcU8sd0JBQVksR0FBR0MsSUFBZjtBQUNBO0FBQ0Q7QUFDRjs7QUFDREYsb0JBQVksR0FBR0ksSUFBSSxDQUFDRSxLQUFMLENBQVcsS0FBS04sWUFBaEIsSUFBZ0MsRUFBL0M7QUFDRDs7QUFDRCxhQUFPLGFBQWFBLFlBQWIsR0FBNEIsWUFBNUIsR0FBMkNDLFlBQWxEO0FBQ0QsS0FsQkQ7O0FBb0JBalQsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQnFJLDJCQUFuQixHQUFpRCxZQUFXO0FBQzFELFVBQUssS0FBS3pELE9BQUwsQ0FBYXRDLFFBQWIsSUFBeUIsSUFBMUIsSUFBbUMsS0FBS21KLGdCQUFMLEdBQXdCOU0sTUFBeEIsSUFBa0MsS0FBS2lHLE9BQUwsQ0FBYXRDLFFBQXRGLEVBQWdHO0FBQzlGLFlBQUksS0FBS21KLGdCQUFMLEdBQXdCOU0sTUFBeEIsS0FBbUMsS0FBS2lHLE9BQUwsQ0FBYXRDLFFBQXBELEVBQThEO0FBQzVELGVBQUsvQixJQUFMLENBQVUsaUJBQVYsRUFBNkIsS0FBS29LLEtBQWxDO0FBQ0Q7O0FBQ0QsZUFBTyxLQUFLdEcsT0FBTCxDQUFhd0IsU0FBYixDQUF1QkssR0FBdkIsQ0FBMkIsc0JBQTNCLENBQVA7QUFDRCxPQUxELE1BS087QUFDTCxlQUFPLEtBQUs3QixPQUFMLENBQWF3QixTQUFiLENBQXVCQyxNQUF2QixDQUE4QixzQkFBOUIsQ0FBUDtBQUNEO0FBQ0YsS0FURDs7QUFXQXZKLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUI0RixJQUFuQixHQUEwQixVQUFTeEksQ0FBVCxFQUFZO0FBQ3BDLFVBQUl1TixLQUFKLEVBQVdtRixLQUFYOztBQUNBLFVBQUksQ0FBQzFTLENBQUMsQ0FBQ2dRLFlBQVAsRUFBcUI7QUFDbkI7QUFDRDs7QUFDRCxXQUFLN00sSUFBTCxDQUFVLE1BQVYsRUFBa0JuRCxDQUFsQjtBQUNBdU4sV0FBSyxHQUFHdk4sQ0FBQyxDQUFDZ1EsWUFBRixDQUFlekMsS0FBdkI7QUFDQSxXQUFLcEssSUFBTCxDQUFVLFlBQVYsRUFBd0JvSyxLQUF4Qjs7QUFDQSxVQUFJQSxLQUFLLENBQUNoTSxNQUFWLEVBQWtCO0FBQ2hCbVIsYUFBSyxHQUFHMVMsQ0FBQyxDQUFDZ1EsWUFBRixDQUFlMEMsS0FBdkI7O0FBQ0EsWUFBSUEsS0FBSyxJQUFJQSxLQUFLLENBQUNuUixNQUFmLElBQTBCbVIsS0FBSyxDQUFDLENBQUQsQ0FBTCxDQUFTQyxnQkFBVCxJQUE2QixJQUEzRCxFQUFrRTtBQUNoRSxlQUFLQyxrQkFBTCxDQUF3QkYsS0FBeEI7QUFDRCxTQUZELE1BRU87QUFDTCxlQUFLRyxXQUFMLENBQWlCdEYsS0FBakI7QUFDRDtBQUNGO0FBQ0YsS0FoQkQ7O0FBa0JBcE8sWUFBUSxDQUFDeUQsU0FBVCxDQUFtQnFHLEtBQW5CLEdBQTJCLFVBQVNqSixDQUFULEVBQVk7QUFDckMsVUFBSTBTLEtBQUosRUFBVzFMLElBQVg7O0FBQ0EsVUFBSSxDQUFDaEgsQ0FBQyxJQUFJLElBQUwsR0FBWSxDQUFDZ0gsSUFBSSxHQUFHaEgsQ0FBQyxDQUFDOFMsYUFBVixLQUE0QixJQUE1QixHQUFtQzlMLElBQUksQ0FBQzBMLEtBQXhDLEdBQWdELEtBQUssQ0FBakUsR0FBcUUsS0FBSyxDQUEzRSxLQUFpRixJQUFyRixFQUEyRjtBQUN6RjtBQUNEOztBQUNELFdBQUt2UCxJQUFMLENBQVUsT0FBVixFQUFtQm5ELENBQW5CO0FBQ0EwUyxXQUFLLEdBQUcxUyxDQUFDLENBQUM4UyxhQUFGLENBQWdCSixLQUF4Qjs7QUFDQSxVQUFJQSxLQUFLLENBQUNuUixNQUFWLEVBQWtCO0FBQ2hCLGVBQU8sS0FBS3FSLGtCQUFMLENBQXdCRixLQUF4QixDQUFQO0FBQ0Q7QUFDRixLQVZEOztBQVlBdlQsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQmlRLFdBQW5CLEdBQWlDLFVBQVN0RixLQUFULEVBQWdCO0FBQy9DLFVBQUk3RyxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQsRUFBb0JxRyxRQUFwQjs7QUFDQUEsY0FBUSxHQUFHLEVBQVg7O0FBQ0EsV0FBS3RHLEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBRytKLEtBQUssQ0FBQ2hNLE1BQTFCLEVBQWtDZ0MsRUFBRSxHQUFHQyxJQUF2QyxFQUE2Q0QsRUFBRSxFQUEvQyxFQUFtRDtBQUNqRG1ELFlBQUksR0FBRzZHLEtBQUssQ0FBQ2hLLEVBQUQsQ0FBWjs7QUFDQXNHLGdCQUFRLENBQUMzRyxJQUFULENBQWMsS0FBS3dNLE9BQUwsQ0FBYWhKLElBQWIsQ0FBZDtBQUNEOztBQUNELGFBQU9tRCxRQUFQO0FBQ0QsS0FSRDs7QUFVQTFLLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJnUSxrQkFBbkIsR0FBd0MsVUFBU0YsS0FBVCxFQUFnQjtBQUN0RCxVQUFJSyxLQUFKLEVBQVdDLElBQVgsRUFBaUJ6UCxFQUFqQixFQUFxQkMsSUFBckIsRUFBMkJxRyxRQUEzQjs7QUFDQUEsY0FBUSxHQUFHLEVBQVg7O0FBQ0EsV0FBS3RHLEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBR2tQLEtBQUssQ0FBQ25SLE1BQTFCLEVBQWtDZ0MsRUFBRSxHQUFHQyxJQUF2QyxFQUE2Q0QsRUFBRSxFQUEvQyxFQUFtRDtBQUNqRHlQLFlBQUksR0FBR04sS0FBSyxDQUFDblAsRUFBRCxDQUFaOztBQUNBLFlBQUt5UCxJQUFJLENBQUNMLGdCQUFMLElBQXlCLElBQTFCLEtBQW9DSSxLQUFLLEdBQUdDLElBQUksQ0FBQ0wsZ0JBQUwsRUFBNUMsQ0FBSixFQUEwRTtBQUN4RSxjQUFJSSxLQUFLLENBQUNFLE1BQVYsRUFBa0I7QUFDaEJwSixvQkFBUSxDQUFDM0csSUFBVCxDQUFjLEtBQUt3TSxPQUFMLENBQWFzRCxJQUFJLENBQUNFLFNBQUwsRUFBYixDQUFkO0FBQ0QsV0FGRCxNQUVPLElBQUlILEtBQUssQ0FBQ0ksV0FBVixFQUF1QjtBQUM1QnRKLG9CQUFRLENBQUMzRyxJQUFULENBQWMsS0FBS2tRLHNCQUFMLENBQTRCTCxLQUE1QixFQUFtQ0EsS0FBSyxDQUFDN0ksSUFBekMsQ0FBZDtBQUNELFdBRk0sTUFFQTtBQUNMTCxvQkFBUSxDQUFDM0csSUFBVCxDQUFjLEtBQUssQ0FBbkI7QUFDRDtBQUNGLFNBUkQsTUFRTyxJQUFJOFAsSUFBSSxDQUFDRSxTQUFMLElBQWtCLElBQXRCLEVBQTRCO0FBQ2pDLGNBQUtGLElBQUksQ0FBQ0ssSUFBTCxJQUFhLElBQWQsSUFBdUJMLElBQUksQ0FBQ0ssSUFBTCxLQUFjLE1BQXpDLEVBQWlEO0FBQy9DeEosb0JBQVEsQ0FBQzNHLElBQVQsQ0FBYyxLQUFLd00sT0FBTCxDQUFhc0QsSUFBSSxDQUFDRSxTQUFMLEVBQWIsQ0FBZDtBQUNELFdBRkQsTUFFTztBQUNMckosb0JBQVEsQ0FBQzNHLElBQVQsQ0FBYyxLQUFLLENBQW5CO0FBQ0Q7QUFDRixTQU5NLE1BTUE7QUFDTDJHLGtCQUFRLENBQUMzRyxJQUFULENBQWMsS0FBSyxDQUFuQjtBQUNEO0FBQ0Y7O0FBQ0QsYUFBTzJHLFFBQVA7QUFDRCxLQXhCRDs7QUEwQkExSyxZQUFRLENBQUN5RCxTQUFULENBQW1Cd1Esc0JBQW5CLEdBQTRDLFVBQVNFLFNBQVQsRUFBb0JDLElBQXBCLEVBQTBCO0FBQ3BFLFVBQUlDLFNBQUosRUFBZUMsYUFBZjtBQUNBRCxlQUFTLEdBQUdGLFNBQVMsQ0FBQ0ksWUFBVixFQUFaOztBQUNBRCxtQkFBYSxHQUFJLFVBQVNsSixLQUFULEVBQWdCO0FBQy9CLGVBQU8sVUFBU29KLE9BQVQsRUFBa0I7QUFDdkIsY0FBSVosS0FBSixFQUFXeFAsRUFBWCxFQUFlQyxJQUFmOztBQUNBLGVBQUtELEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBR21RLE9BQU8sQ0FBQ3BTLE1BQTVCLEVBQW9DZ0MsRUFBRSxHQUFHQyxJQUF6QyxFQUErQ0QsRUFBRSxFQUFqRCxFQUFxRDtBQUNuRHdQLGlCQUFLLEdBQUdZLE9BQU8sQ0FBQ3BRLEVBQUQsQ0FBZjs7QUFDQSxnQkFBSXdQLEtBQUssQ0FBQ0UsTUFBVixFQUFrQjtBQUNoQkYsbUJBQUssQ0FBQ3JNLElBQU4sQ0FBVyxVQUFTQSxJQUFULEVBQWU7QUFDeEIsb0JBQUk2RCxLQUFLLENBQUMvQyxPQUFOLENBQWNuQyxpQkFBZCxJQUFtQ3FCLElBQUksQ0FBQ3dELElBQUwsQ0FBVTBKLFNBQVYsQ0FBb0IsQ0FBcEIsRUFBdUIsQ0FBdkIsTUFBOEIsR0FBckUsRUFBMEU7QUFDeEU7QUFDRDs7QUFDRGxOLG9CQUFJLENBQUNtTixRQUFMLEdBQWdCLEtBQUtOLElBQUwsR0FBWSxHQUFaLEdBQWtCN00sSUFBSSxDQUFDd0QsSUFBdkM7QUFDQSx1QkFBT0ssS0FBSyxDQUFDbUYsT0FBTixDQUFjaEosSUFBZCxDQUFQO0FBQ0QsZUFORDtBQU9ELGFBUkQsTUFRTyxJQUFJcU0sS0FBSyxDQUFDSSxXQUFWLEVBQXVCO0FBQzVCNUksbUJBQUssQ0FBQzZJLHNCQUFOLENBQTZCTCxLQUE3QixFQUFvQyxLQUFLUSxJQUFMLEdBQVksR0FBWixHQUFrQlIsS0FBSyxDQUFDN0ksSUFBNUQ7QUFDRDtBQUNGO0FBQ0YsU0FoQkQ7QUFpQkQsT0FsQmUsQ0FrQmIsSUFsQmEsQ0FBaEI7O0FBbUJBLGFBQU9zSixTQUFTLENBQUNNLFdBQVYsQ0FBc0JMLGFBQXRCLEVBQXFDLFVBQVNqSSxLQUFULEVBQWdCO0FBQzFELGVBQU8sT0FBT3VJLE9BQVAsS0FBbUIsV0FBbkIsSUFBa0NBLE9BQU8sS0FBSyxJQUE5QyxHQUFxRCxPQUFPQSxPQUFPLENBQUNDLEdBQWYsS0FBdUIsVUFBdkIsR0FBb0NELE9BQU8sQ0FBQ0MsR0FBUixDQUFZeEksS0FBWixDQUFwQyxHQUF5RCxLQUFLLENBQW5ILEdBQXVILEtBQUssQ0FBbkk7QUFDRCxPQUZNLENBQVA7QUFHRCxLQXpCRDs7QUEyQkFyTSxZQUFRLENBQUN5RCxTQUFULENBQW1CNkQsTUFBbkIsR0FBNEIsVUFBU0MsSUFBVCxFQUFlQyxJQUFmLEVBQXFCO0FBQy9DLFVBQUlELElBQUksQ0FBQzJELElBQUwsR0FBWSxLQUFLN0MsT0FBTCxDQUFhN0MsV0FBYixHQUEyQixJQUEzQixHQUFrQyxJQUFsRCxFQUF3RDtBQUN0RCxlQUFPZ0MsSUFBSSxDQUFDLEtBQUthLE9BQUwsQ0FBYXZCLGNBQWIsQ0FBNEJtSCxPQUE1QixDQUFvQyxjQUFwQyxFQUFvRG1GLElBQUksQ0FBQ0UsS0FBTCxDQUFXL0wsSUFBSSxDQUFDMkQsSUFBTCxHQUFZLElBQVosR0FBbUIsS0FBOUIsSUFBdUMsR0FBM0YsRUFBZ0crQyxPQUFoRyxDQUF3RyxpQkFBeEcsRUFBMkgsS0FBSzVGLE9BQUwsQ0FBYTdDLFdBQXhJLENBQUQsQ0FBWDtBQUNELE9BRkQsTUFFTyxJQUFJLENBQUN4RixRQUFRLENBQUM4VSxXQUFULENBQXFCdk4sSUFBckIsRUFBMkIsS0FBS2MsT0FBTCxDQUFhbEMsYUFBeEMsQ0FBTCxFQUE2RDtBQUNsRSxlQUFPcUIsSUFBSSxDQUFDLEtBQUthLE9BQUwsQ0FBYXRCLG1CQUFkLENBQVg7QUFDRCxPQUZNLE1BRUEsSUFBSyxLQUFLc0IsT0FBTCxDQUFhdEMsUUFBYixJQUF5QixJQUExQixJQUFtQyxLQUFLbUosZ0JBQUwsR0FBd0I5TSxNQUF4QixJQUFrQyxLQUFLaUcsT0FBTCxDQUFhdEMsUUFBdEYsRUFBZ0c7QUFDckd5QixZQUFJLENBQUMsS0FBS2EsT0FBTCxDQUFhaEIsb0JBQWIsQ0FBa0M0RyxPQUFsQyxDQUEwQyxjQUExQyxFQUEwRCxLQUFLNUYsT0FBTCxDQUFhdEMsUUFBdkUsQ0FBRCxDQUFKO0FBQ0EsZUFBTyxLQUFLL0IsSUFBTCxDQUFVLGtCQUFWLEVBQThCdUQsSUFBOUIsQ0FBUDtBQUNELE9BSE0sTUFHQTtBQUNMLGVBQU8sS0FBS2MsT0FBTCxDQUFhZixNQUFiLENBQW9CaEUsSUFBcEIsQ0FBeUIsSUFBekIsRUFBK0JpRSxJQUEvQixFQUFxQ0MsSUFBckMsQ0FBUDtBQUNEO0FBQ0YsS0FYRDs7QUFhQXhILFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUI4TSxPQUFuQixHQUE2QixVQUFTaEosSUFBVCxFQUFlO0FBQzFDQSxVQUFJLENBQUN1SyxNQUFMLEdBQWM7QUFDWm5GLGdCQUFRLEVBQUUsQ0FERTtBQUVab0YsYUFBSyxFQUFFeEssSUFBSSxDQUFDMkQsSUFGQTtBQUdaMEIsaUJBQVMsRUFBRTtBQUhDLE9BQWQ7QUFLQSxXQUFLd0IsS0FBTCxDQUFXckssSUFBWCxDQUFnQndELElBQWhCO0FBQ0FBLFVBQUksQ0FBQ2dFLE1BQUwsR0FBY3ZMLFFBQVEsQ0FBQzBQLEtBQXZCO0FBQ0EsV0FBSzFMLElBQUwsQ0FBVSxXQUFWLEVBQXVCdUQsSUFBdkI7O0FBQ0EsV0FBS3dOLGlCQUFMLENBQXVCeE4sSUFBdkI7O0FBQ0EsYUFBTyxLQUFLRCxNQUFMLENBQVlDLElBQVosRUFBbUIsVUFBUzZELEtBQVQsRUFBZ0I7QUFDeEMsZUFBTyxVQUFTaUIsS0FBVCxFQUFnQjtBQUNyQixjQUFJQSxLQUFKLEVBQVc7QUFDVDlFLGdCQUFJLENBQUM0SCxRQUFMLEdBQWdCLEtBQWhCOztBQUNBL0QsaUJBQUssQ0FBQzRKLGdCQUFOLENBQXVCLENBQUN6TixJQUFELENBQXZCLEVBQStCOEUsS0FBL0I7QUFDRCxXQUhELE1BR087QUFDTDlFLGdCQUFJLENBQUM0SCxRQUFMLEdBQWdCLElBQWhCOztBQUNBLGdCQUFJL0QsS0FBSyxDQUFDL0MsT0FBTixDQUFjL0IsU0FBbEIsRUFBNkI7QUFDM0I4RSxtQkFBSyxDQUFDNkosV0FBTixDQUFrQjFOLElBQWxCO0FBQ0Q7QUFDRjs7QUFDRCxpQkFBTzZELEtBQUssQ0FBQ1UsMkJBQU4sRUFBUDtBQUNELFNBWEQ7QUFZRCxPQWJ3QixDQWF0QixJQWJzQixDQUFsQixDQUFQO0FBY0QsS0F4QkQ7O0FBMEJBOUwsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQnlSLFlBQW5CLEdBQWtDLFVBQVM5RyxLQUFULEVBQWdCO0FBQ2hELFVBQUk3RyxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQ7O0FBQ0EsV0FBS0QsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHK0osS0FBSyxDQUFDaE0sTUFBMUIsRUFBa0NnQyxFQUFFLEdBQUdDLElBQXZDLEVBQTZDRCxFQUFFLEVBQS9DLEVBQW1EO0FBQ2pEbUQsWUFBSSxHQUFHNkcsS0FBSyxDQUFDaEssRUFBRCxDQUFaO0FBQ0EsYUFBSzZRLFdBQUwsQ0FBaUIxTixJQUFqQjtBQUNEOztBQUNELGFBQU8sSUFBUDtBQUNELEtBUEQ7O0FBU0F2SCxZQUFRLENBQUN5RCxTQUFULENBQW1Cd1IsV0FBbkIsR0FBaUMsVUFBUzFOLElBQVQsRUFBZTtBQUM5QyxVQUFJQSxJQUFJLENBQUNnRSxNQUFMLEtBQWdCdkwsUUFBUSxDQUFDMFAsS0FBekIsSUFBa0NuSSxJQUFJLENBQUM0SCxRQUFMLEtBQWtCLElBQXhELEVBQThEO0FBQzVENUgsWUFBSSxDQUFDZ0UsTUFBTCxHQUFjdkwsUUFBUSxDQUFDdVAsTUFBdkI7O0FBQ0EsWUFBSSxLQUFLbEgsT0FBTCxDQUFhaEMsZ0JBQWpCLEVBQW1DO0FBQ2pDLGlCQUFPK0YsVUFBVSxDQUFHLFVBQVNoQixLQUFULEVBQWdCO0FBQ2xDLG1CQUFPLFlBQVc7QUFDaEIscUJBQU9BLEtBQUssQ0FBQytKLFlBQU4sRUFBUDtBQUNELGFBRkQ7QUFHRCxXQUprQixDQUloQixJQUpnQixDQUFGLEVBSU4sQ0FKTSxDQUFqQjtBQUtEO0FBQ0YsT0FURCxNQVNPO0FBQ0wsY0FBTSxJQUFJM0csS0FBSixDQUFVLGtGQUFWLENBQU47QUFDRDtBQUNGLEtBYkQ7O0FBZUF4TyxZQUFRLENBQUN5RCxTQUFULENBQW1CMlIsZUFBbkIsR0FBcUMsRUFBckM7QUFFQXBWLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUI0UixvQkFBbkIsR0FBMEMsS0FBMUM7O0FBRUFyVixZQUFRLENBQUN5RCxTQUFULENBQW1Cc1IsaUJBQW5CLEdBQXVDLFVBQVN4TixJQUFULEVBQWU7QUFDcEQsVUFBSSxLQUFLYyxPQUFMLENBQWEzQyxxQkFBYixJQUFzQzZCLElBQUksQ0FBQytOLElBQUwsQ0FBVUMsS0FBVixDQUFnQixTQUFoQixDQUF0QyxJQUFvRWhPLElBQUksQ0FBQzJELElBQUwsSUFBYSxLQUFLN0MsT0FBTCxDQUFhMUMsb0JBQWIsR0FBb0MsSUFBcEMsR0FBMkMsSUFBaEksRUFBc0k7QUFDcEksYUFBS3lQLGVBQUwsQ0FBcUJyUixJQUFyQixDQUEwQndELElBQTFCOztBQUNBLGVBQU82RSxVQUFVLENBQUcsVUFBU2hCLEtBQVQsRUFBZ0I7QUFDbEMsaUJBQU8sWUFBVztBQUNoQixtQkFBT0EsS0FBSyxDQUFDb0ssc0JBQU4sRUFBUDtBQUNELFdBRkQ7QUFHRCxTQUprQixDQUloQixJQUpnQixDQUFGLEVBSU4sQ0FKTSxDQUFqQjtBQUtEO0FBQ0YsS0FURDs7QUFXQXhWLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUIrUixzQkFBbkIsR0FBNEMsWUFBVztBQUNyRCxVQUFJLEtBQUtILG9CQUFMLElBQTZCLEtBQUtELGVBQUwsQ0FBcUJoVCxNQUFyQixLQUFnQyxDQUFqRSxFQUFvRTtBQUNsRTtBQUNEOztBQUNELFdBQUtpVCxvQkFBTCxHQUE0QixJQUE1QjtBQUNBLGFBQU8sS0FBS0ksZUFBTCxDQUFxQixLQUFLTCxlQUFMLENBQXFCTSxLQUFyQixFQUFyQixFQUFvRCxVQUFTdEssS0FBVCxFQUFnQjtBQUN6RSxlQUFPLFlBQVc7QUFDaEJBLGVBQUssQ0FBQ2lLLG9CQUFOLEdBQTZCLEtBQTdCO0FBQ0EsaUJBQU9qSyxLQUFLLENBQUNvSyxzQkFBTixFQUFQO0FBQ0QsU0FIRDtBQUlELE9BTHlELENBS3ZELElBTHVELENBQW5ELENBQVA7QUFNRCxLQVhEOztBQWFBeFYsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQmlJLFVBQW5CLEdBQWdDLFVBQVNuRSxJQUFULEVBQWU7QUFDN0MsVUFBSUEsSUFBSSxDQUFDZ0UsTUFBTCxLQUFnQnZMLFFBQVEsQ0FBQ3dMLFNBQTdCLEVBQXdDO0FBQ3RDLGFBQUtzSCxZQUFMLENBQWtCdkwsSUFBbEI7QUFDRDs7QUFDRCxXQUFLNkcsS0FBTCxHQUFhdEwsT0FBTyxDQUFDLEtBQUtzTCxLQUFOLEVBQWE3RyxJQUFiLENBQXBCO0FBQ0EsV0FBS3ZELElBQUwsQ0FBVSxhQUFWLEVBQXlCdUQsSUFBekI7O0FBQ0EsVUFBSSxLQUFLNkcsS0FBTCxDQUFXaE0sTUFBWCxLQUFzQixDQUExQixFQUE2QjtBQUMzQixlQUFPLEtBQUs0QixJQUFMLENBQVUsT0FBVixDQUFQO0FBQ0Q7QUFDRixLQVREOztBQVdBaEUsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQitOLGNBQW5CLEdBQW9DLFVBQVNtRSxpQkFBVCxFQUE0QjtBQUM5RCxVQUFJcE8sSUFBSixFQUFVbkQsRUFBVixFQUFjQyxJQUFkLEVBQW9Cd0QsSUFBcEI7O0FBQ0EsVUFBSThOLGlCQUFpQixJQUFJLElBQXpCLEVBQStCO0FBQzdCQSx5QkFBaUIsR0FBRyxLQUFwQjtBQUNEOztBQUNEOU4sVUFBSSxHQUFHLEtBQUt1RyxLQUFMLENBQVdwTCxLQUFYLEVBQVA7O0FBQ0EsV0FBS29CLEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBR3dELElBQUksQ0FBQ3pGLE1BQXpCLEVBQWlDZ0MsRUFBRSxHQUFHQyxJQUF0QyxFQUE0Q0QsRUFBRSxFQUE5QyxFQUFrRDtBQUNoRG1ELFlBQUksR0FBR00sSUFBSSxDQUFDekQsRUFBRCxDQUFYOztBQUNBLFlBQUltRCxJQUFJLENBQUNnRSxNQUFMLEtBQWdCdkwsUUFBUSxDQUFDd0wsU0FBekIsSUFBc0NtSyxpQkFBMUMsRUFBNkQ7QUFDM0QsZUFBS2pLLFVBQUwsQ0FBZ0JuRSxJQUFoQjtBQUNEO0FBQ0Y7O0FBQ0QsYUFBTyxJQUFQO0FBQ0QsS0FiRDs7QUFlQXZILFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJnUyxlQUFuQixHQUFxQyxVQUFTbE8sSUFBVCxFQUFlckQsUUFBZixFQUF5QjtBQUM1RCxVQUFJMFIsVUFBSjtBQUNBQSxnQkFBVSxHQUFHLElBQUlDLFVBQUosRUFBYjs7QUFDQUQsZ0JBQVUsQ0FBQ0UsTUFBWCxHQUFxQixVQUFTMUssS0FBVCxFQUFnQjtBQUNuQyxlQUFPLFlBQVc7QUFDaEIsY0FBSTdELElBQUksQ0FBQytOLElBQUwsS0FBYyxlQUFsQixFQUFtQztBQUNqQ2xLLGlCQUFLLENBQUNwSCxJQUFOLENBQVcsV0FBWCxFQUF3QnVELElBQXhCLEVBQThCcU8sVUFBVSxDQUFDRyxNQUF6Qzs7QUFDQSxnQkFBSTdSLFFBQVEsSUFBSSxJQUFoQixFQUFzQjtBQUNwQkEsc0JBQVE7QUFDVDs7QUFDRDtBQUNEOztBQUNELGlCQUFPa0gsS0FBSyxDQUFDNEssc0JBQU4sQ0FBNkJ6TyxJQUE3QixFQUFtQ3FPLFVBQVUsQ0FBQ0csTUFBOUMsRUFBc0Q3UixRQUF0RCxDQUFQO0FBQ0QsU0FURDtBQVVELE9BWG1CLENBV2pCLElBWGlCLENBQXBCOztBQVlBLGFBQU8wUixVQUFVLENBQUNLLGFBQVgsQ0FBeUIxTyxJQUF6QixDQUFQO0FBQ0QsS0FoQkQ7O0FBa0JBdkgsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQnVTLHNCQUFuQixHQUE0QyxVQUFTek8sSUFBVCxFQUFlMk8sUUFBZixFQUF5QmhTLFFBQXpCLEVBQW1DaVMsV0FBbkMsRUFBZ0Q7QUFDMUYsVUFBSUMsR0FBSjtBQUNBQSxTQUFHLEdBQUcvSCxRQUFRLENBQUNuRyxhQUFULENBQXVCLEtBQXZCLENBQU47O0FBQ0EsVUFBSWlPLFdBQUosRUFBaUI7QUFDZkMsV0FBRyxDQUFDRCxXQUFKLEdBQWtCQSxXQUFsQjtBQUNEOztBQUNEQyxTQUFHLENBQUNOLE1BQUosR0FBYyxVQUFTMUssS0FBVCxFQUFnQjtBQUM1QixlQUFPLFlBQVc7QUFDaEIsY0FBSWlMLE1BQUosRUFBWUMsR0FBWixFQUFpQkMsVUFBakIsRUFBNkJ4SyxTQUE3QixFQUF3Q2xFLElBQXhDLEVBQThDMkMsS0FBOUMsRUFBcURDLEtBQXJELEVBQTREK0wsS0FBNUQ7O0FBQ0FqUCxjQUFJLENBQUN3QixLQUFMLEdBQWFxTixHQUFHLENBQUNyTixLQUFqQjtBQUNBeEIsY0FBSSxDQUFDakYsTUFBTCxHQUFjOFQsR0FBRyxDQUFDOVQsTUFBbEI7QUFDQWlVLG9CQUFVLEdBQUduTCxLQUFLLENBQUMvQyxPQUFOLENBQWNHLE1BQWQsQ0FBcUJsRixJQUFyQixDQUEwQjhILEtBQTFCLEVBQWlDN0QsSUFBakMsQ0FBYjs7QUFDQSxjQUFJZ1AsVUFBVSxDQUFDbk4sUUFBWCxJQUF1QixJQUEzQixFQUFpQztBQUMvQm1OLHNCQUFVLENBQUNuTixRQUFYLEdBQXNCbU4sVUFBVSxDQUFDdE4sUUFBakM7QUFDRDs7QUFDRCxjQUFJc04sVUFBVSxDQUFDcE4sU0FBWCxJQUF3QixJQUE1QixFQUFrQztBQUNoQ29OLHNCQUFVLENBQUNwTixTQUFYLEdBQXVCb04sVUFBVSxDQUFDck4sU0FBbEM7QUFDRDs7QUFDRG1OLGdCQUFNLEdBQUdoSSxRQUFRLENBQUNuRyxhQUFULENBQXVCLFFBQXZCLENBQVQ7QUFDQW9PLGFBQUcsR0FBR0QsTUFBTSxDQUFDSSxVQUFQLENBQWtCLElBQWxCLENBQU47QUFDQUosZ0JBQU0sQ0FBQ3ROLEtBQVAsR0FBZXdOLFVBQVUsQ0FBQ25OLFFBQTFCO0FBQ0FpTixnQkFBTSxDQUFDL1QsTUFBUCxHQUFnQmlVLFVBQVUsQ0FBQ3BOLFNBQTNCO0FBQ0F2Ryx5QkFBZSxDQUFDMFQsR0FBRCxFQUFNRixHQUFOLEVBQVcsQ0FBQ3ZPLElBQUksR0FBRzBPLFVBQVUsQ0FBQzNOLElBQW5CLEtBQTRCLElBQTVCLEdBQW1DZixJQUFuQyxHQUEwQyxDQUFyRCxFQUF3RCxDQUFDMkMsS0FBSyxHQUFHK0wsVUFBVSxDQUFDMU4sSUFBcEIsS0FBNkIsSUFBN0IsR0FBb0MyQixLQUFwQyxHQUE0QyxDQUFwRyxFQUF1RytMLFVBQVUsQ0FBQ3pOLFFBQWxILEVBQTRIeU4sVUFBVSxDQUFDdk4sU0FBdkksRUFBa0osQ0FBQ3lCLEtBQUssR0FBRzhMLFVBQVUsQ0FBQ0csSUFBcEIsS0FBNkIsSUFBN0IsR0FBb0NqTSxLQUFwQyxHQUE0QyxDQUE5TCxFQUFpTSxDQUFDK0wsS0FBSyxHQUFHRCxVQUFVLENBQUNJLElBQXBCLEtBQTZCLElBQTdCLEdBQW9DSCxLQUFwQyxHQUE0QyxDQUE3TyxFQUFnUEQsVUFBVSxDQUFDbk4sUUFBM1AsRUFBcVFtTixVQUFVLENBQUNwTixTQUFoUixDQUFmO0FBQ0E0QyxtQkFBUyxHQUFHc0ssTUFBTSxDQUFDTyxTQUFQLENBQWlCLFdBQWpCLENBQVo7O0FBQ0F4TCxlQUFLLENBQUNwSCxJQUFOLENBQVcsV0FBWCxFQUF3QnVELElBQXhCLEVBQThCd0UsU0FBOUI7O0FBQ0EsY0FBSTdILFFBQVEsSUFBSSxJQUFoQixFQUFzQjtBQUNwQixtQkFBT0EsUUFBUSxFQUFmO0FBQ0Q7QUFDRixTQXJCRDtBQXNCRCxPQXZCWSxDQXVCVixJQXZCVSxDQUFiOztBQXdCQSxVQUFJQSxRQUFRLElBQUksSUFBaEIsRUFBc0I7QUFDcEJrUyxXQUFHLENBQUNTLE9BQUosR0FBYzNTLFFBQWQ7QUFDRDs7QUFDRCxhQUFPa1MsR0FBRyxDQUFDakssR0FBSixHQUFVK0osUUFBakI7QUFDRCxLQWxDRDs7QUFvQ0FsVyxZQUFRLENBQUN5RCxTQUFULENBQW1CMFIsWUFBbkIsR0FBa0MsWUFBVztBQUMzQyxVQUFJdlEsQ0FBSixFQUFPVSxlQUFQLEVBQXdCd1IsZ0JBQXhCLEVBQTBDQyxXQUExQztBQUNBelIscUJBQWUsR0FBRyxLQUFLK0MsT0FBTCxDQUFhL0MsZUFBL0I7QUFDQXdSLHNCQUFnQixHQUFHLEtBQUt0SCxpQkFBTCxHQUF5QnBOLE1BQTVDO0FBQ0F3QyxPQUFDLEdBQUdrUyxnQkFBSjs7QUFDQSxVQUFJQSxnQkFBZ0IsSUFBSXhSLGVBQXhCLEVBQXlDO0FBQ3ZDO0FBQ0Q7O0FBQ0R5UixpQkFBVyxHQUFHLEtBQUt6SCxjQUFMLEVBQWQ7O0FBQ0EsVUFBSSxFQUFFeUgsV0FBVyxDQUFDM1UsTUFBWixHQUFxQixDQUF2QixDQUFKLEVBQStCO0FBQzdCO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLaUcsT0FBTCxDQUFhOUMsY0FBakIsRUFBaUM7QUFDL0IsZUFBTyxLQUFLeVIsWUFBTCxDQUFrQkQsV0FBVyxDQUFDL1QsS0FBWixDQUFrQixDQUFsQixFQUFxQnNDLGVBQWUsR0FBR3dSLGdCQUF2QyxDQUFsQixDQUFQO0FBQ0QsT0FGRCxNQUVPO0FBQ0wsZUFBT2xTLENBQUMsR0FBR1UsZUFBWCxFQUE0QjtBQUMxQixjQUFJLENBQUN5UixXQUFXLENBQUMzVSxNQUFqQixFQUF5QjtBQUN2QjtBQUNEOztBQUNELGVBQUs2VSxXQUFMLENBQWlCRixXQUFXLENBQUNyQixLQUFaLEVBQWpCO0FBQ0E5USxXQUFDO0FBQ0Y7QUFDRjtBQUNGLEtBdkJEOztBQXlCQTVFLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJ3VCxXQUFuQixHQUFpQyxVQUFTMVAsSUFBVCxFQUFlO0FBQzlDLGFBQU8sS0FBS3lQLFlBQUwsQ0FBa0IsQ0FBQ3pQLElBQUQsQ0FBbEIsQ0FBUDtBQUNELEtBRkQ7O0FBSUF2SCxZQUFRLENBQUN5RCxTQUFULENBQW1CdVQsWUFBbkIsR0FBa0MsVUFBUzVJLEtBQVQsRUFBZ0I7QUFDaEQsVUFBSTdHLElBQUosRUFBVW5ELEVBQVYsRUFBY0MsSUFBZDs7QUFDQSxXQUFLRCxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUcrSixLQUFLLENBQUNoTSxNQUExQixFQUFrQ2dDLEVBQUUsR0FBR0MsSUFBdkMsRUFBNkNELEVBQUUsRUFBL0MsRUFBbUQ7QUFDakRtRCxZQUFJLEdBQUc2RyxLQUFLLENBQUNoSyxFQUFELENBQVo7QUFDQW1ELFlBQUksQ0FBQ2lGLFVBQUwsR0FBa0IsSUFBbEI7QUFDQWpGLFlBQUksQ0FBQ2dFLE1BQUwsR0FBY3ZMLFFBQVEsQ0FBQ3dMLFNBQXZCO0FBQ0EsYUFBS3hILElBQUwsQ0FBVSxZQUFWLEVBQXdCdUQsSUFBeEI7QUFDRDs7QUFDRCxVQUFJLEtBQUtjLE9BQUwsQ0FBYTlDLGNBQWpCLEVBQWlDO0FBQy9CLGFBQUt2QixJQUFMLENBQVUsb0JBQVYsRUFBZ0NvSyxLQUFoQztBQUNEOztBQUNELGFBQU8sS0FBSzhJLFdBQUwsQ0FBaUI5SSxLQUFqQixDQUFQO0FBQ0QsS0FaRDs7QUFjQXBPLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUIwVCxnQkFBbkIsR0FBc0MsVUFBU0MsR0FBVCxFQUFjO0FBQ2xELFVBQUk3UCxJQUFKLEVBQVU2RyxLQUFWO0FBQ0EsYUFBT0EsS0FBSyxHQUFJLFlBQVc7QUFDekIsWUFBSWhLLEVBQUosRUFBUUMsSUFBUixFQUFjd0QsSUFBZCxFQUFvQjZDLFFBQXBCOztBQUNBN0MsWUFBSSxHQUFHLEtBQUt1RyxLQUFaO0FBQ0ExRCxnQkFBUSxHQUFHLEVBQVg7O0FBQ0EsYUFBS3RHLEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBR3dELElBQUksQ0FBQ3pGLE1BQXpCLEVBQWlDZ0MsRUFBRSxHQUFHQyxJQUF0QyxFQUE0Q0QsRUFBRSxFQUE5QyxFQUFrRDtBQUNoRG1ELGNBQUksR0FBR00sSUFBSSxDQUFDekQsRUFBRCxDQUFYOztBQUNBLGNBQUltRCxJQUFJLENBQUM2UCxHQUFMLEtBQWFBLEdBQWpCLEVBQXNCO0FBQ3BCMU0sb0JBQVEsQ0FBQzNHLElBQVQsQ0FBY3dELElBQWQ7QUFDRDtBQUNGOztBQUNELGVBQU9tRCxRQUFQO0FBQ0QsT0FYYyxDQVdacEgsSUFYWSxDQVdQLElBWE8sQ0FBZjtBQVlELEtBZEQ7O0FBZ0JBdEQsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQnFQLFlBQW5CLEdBQWtDLFVBQVN2TCxJQUFULEVBQWU7QUFDL0MsVUFBSThQLFdBQUosRUFBaUJDLFlBQWpCLEVBQStCbFQsRUFBL0IsRUFBbUNnRyxFQUFuQyxFQUF1Qy9GLElBQXZDLEVBQTZDaUcsS0FBN0MsRUFBb0R6QyxJQUFwRDs7QUFDQSxVQUFJTixJQUFJLENBQUNnRSxNQUFMLEtBQWdCdkwsUUFBUSxDQUFDd0wsU0FBN0IsRUFBd0M7QUFDdEM4TCxvQkFBWSxHQUFHLEtBQUtILGdCQUFMLENBQXNCNVAsSUFBSSxDQUFDNlAsR0FBM0IsQ0FBZjs7QUFDQSxhQUFLaFQsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHaVQsWUFBWSxDQUFDbFYsTUFBakMsRUFBeUNnQyxFQUFFLEdBQUdDLElBQTlDLEVBQW9ERCxFQUFFLEVBQXRELEVBQTBEO0FBQ3hEaVQscUJBQVcsR0FBR0MsWUFBWSxDQUFDbFQsRUFBRCxDQUExQjtBQUNBaVQscUJBQVcsQ0FBQzlMLE1BQVosR0FBcUJ2TCxRQUFRLENBQUN1WCxRQUE5QjtBQUNEOztBQUNEaFEsWUFBSSxDQUFDNlAsR0FBTCxDQUFTSSxLQUFUOztBQUNBLGFBQUtwTixFQUFFLEdBQUcsQ0FBTCxFQUFRRSxLQUFLLEdBQUdnTixZQUFZLENBQUNsVixNQUFsQyxFQUEwQ2dJLEVBQUUsR0FBR0UsS0FBL0MsRUFBc0RGLEVBQUUsRUFBeEQsRUFBNEQ7QUFDMURpTixxQkFBVyxHQUFHQyxZQUFZLENBQUNsTixFQUFELENBQTFCO0FBQ0EsZUFBS3BHLElBQUwsQ0FBVSxVQUFWLEVBQXNCcVQsV0FBdEI7QUFDRDs7QUFDRCxZQUFJLEtBQUtoUCxPQUFMLENBQWE5QyxjQUFqQixFQUFpQztBQUMvQixlQUFLdkIsSUFBTCxDQUFVLGtCQUFWLEVBQThCc1QsWUFBOUI7QUFDRDtBQUNGLE9BZEQsTUFjTyxJQUFJLENBQUN6UCxJQUFJLEdBQUdOLElBQUksQ0FBQ2dFLE1BQWIsTUFBeUJ2TCxRQUFRLENBQUMwUCxLQUFsQyxJQUEyQzdILElBQUksS0FBSzdILFFBQVEsQ0FBQ3VQLE1BQWpFLEVBQXlFO0FBQzlFaEksWUFBSSxDQUFDZ0UsTUFBTCxHQUFjdkwsUUFBUSxDQUFDdVgsUUFBdkI7QUFDQSxhQUFLdlQsSUFBTCxDQUFVLFVBQVYsRUFBc0J1RCxJQUF0Qjs7QUFDQSxZQUFJLEtBQUtjLE9BQUwsQ0FBYTlDLGNBQWpCLEVBQWlDO0FBQy9CLGVBQUt2QixJQUFMLENBQVUsa0JBQVYsRUFBOEIsQ0FBQ3VELElBQUQsQ0FBOUI7QUFDRDtBQUNGOztBQUNELFVBQUksS0FBS2MsT0FBTCxDQUFhaEMsZ0JBQWpCLEVBQW1DO0FBQ2pDLGVBQU8sS0FBSzhPLFlBQUwsRUFBUDtBQUNEO0FBQ0YsS0ExQkQ7O0FBNEJBblEsaUJBQWEsR0FBRyx5QkFBVztBQUN6QixVQUFJZixJQUFKLEVBQVV3VCxNQUFWO0FBQ0FBLFlBQU0sR0FBR25ULFNBQVMsQ0FBQyxDQUFELENBQWxCLEVBQXVCTCxJQUFJLEdBQUcsS0FBS0ssU0FBUyxDQUFDbEMsTUFBZixHQUF3QlcsT0FBTyxDQUFDTyxJQUFSLENBQWFnQixTQUFiLEVBQXdCLENBQXhCLENBQXhCLEdBQXFELEVBQW5GOztBQUNBLFVBQUksT0FBT21ULE1BQVAsS0FBa0IsVUFBdEIsRUFBa0M7QUFDaEMsZUFBT0EsTUFBTSxDQUFDbFQsS0FBUCxDQUFhLElBQWIsRUFBbUJOLElBQW5CLENBQVA7QUFDRDs7QUFDRCxhQUFPd1QsTUFBUDtBQUNELEtBUEQ7O0FBU0F6WCxZQUFRLENBQUN5RCxTQUFULENBQW1CaVUsVUFBbkIsR0FBZ0MsVUFBU25RLElBQVQsRUFBZTtBQUM3QyxhQUFPLEtBQUsyUCxXQUFMLENBQWlCLENBQUMzUCxJQUFELENBQWpCLENBQVA7QUFDRCxLQUZEOztBQUlBdkgsWUFBUSxDQUFDeUQsU0FBVCxDQUFtQnlULFdBQW5CLEdBQWlDLFVBQVM5SSxLQUFULEVBQWdCO0FBQy9DLFVBQUk3RyxJQUFKLEVBQVVvUSxRQUFWLEVBQW9CQyxXQUFwQixFQUFpQ0MsVUFBakMsRUFBNkNDLFdBQTdDLEVBQTBEQyxPQUExRCxFQUFtRW5ULENBQW5FLEVBQXNFb1QsS0FBdEUsRUFBNkVDLFNBQTdFLEVBQXdGQyxTQUF4RixFQUFtRzdVLEdBQW5HLEVBQXdHK0IsTUFBeEcsRUFBZ0hxUyxNQUFoSCxFQUF3SFUsV0FBeEgsRUFBcUlDLFFBQXJJLEVBQStJQyxjQUEvSSxFQUErSmxULEdBQS9KLEVBQW9LaEQsS0FBcEssRUFBMktpVixHQUEzSyxFQUFnTGhULEVBQWhMLEVBQW9MZ0csRUFBcEwsRUFBd0xDLEVBQXhMLEVBQTRMaU8sRUFBNUwsRUFBZ01qVSxJQUFoTSxFQUFzTWlHLEtBQXRNLEVBQTZNQyxLQUE3TSxFQUFvTmdPLEtBQXBOLEVBQTJOQyxFQUEzTixFQUErTjNRLElBQS9OLEVBQXFPMkMsS0FBck8sRUFBNE9DLEtBQTVPLEVBQW1QK0wsS0FBblAsRUFBMFBpQyxLQUExUCxFQUFpUUMsS0FBalE7O0FBQ0F0QixTQUFHLEdBQUcsSUFBSXVCLGNBQUosRUFBTjs7QUFDQSxXQUFLdlUsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHK0osS0FBSyxDQUFDaE0sTUFBMUIsRUFBa0NnQyxFQUFFLEdBQUdDLElBQXZDLEVBQTZDRCxFQUFFLEVBQS9DLEVBQW1EO0FBQ2pEbUQsWUFBSSxHQUFHNkcsS0FBSyxDQUFDaEssRUFBRCxDQUFaO0FBQ0FtRCxZQUFJLENBQUM2UCxHQUFMLEdBQVdBLEdBQVg7QUFDRDs7QUFDRGhTLFlBQU0sR0FBR0osYUFBYSxDQUFDLEtBQUtxRCxPQUFMLENBQWFqRCxNQUFkLEVBQXNCZ0osS0FBdEIsQ0FBdEI7QUFDQWpKLFNBQUcsR0FBR0gsYUFBYSxDQUFDLEtBQUtxRCxPQUFMLENBQWFsRCxHQUFkLEVBQW1CaUosS0FBbkIsQ0FBbkI7QUFDQWdKLFNBQUcsQ0FBQ3dCLElBQUosQ0FBU3hULE1BQVQsRUFBaUJELEdBQWpCLEVBQXNCLElBQXRCO0FBQ0FpUyxTQUFHLENBQUMvUixlQUFKLEdBQXNCLENBQUMsQ0FBQyxLQUFLZ0QsT0FBTCxDQUFhaEQsZUFBckM7QUFDQStTLGNBQVEsR0FBRyxJQUFYOztBQUNBUixpQkFBVyxHQUFJLFVBQVN4TSxLQUFULEVBQWdCO0FBQzdCLGVBQU8sWUFBVztBQUNoQixjQUFJaEIsRUFBSixFQUFRRSxLQUFSLEVBQWVJLFFBQWY7O0FBQ0FBLGtCQUFRLEdBQUcsRUFBWDs7QUFDQSxlQUFLTixFQUFFLEdBQUcsQ0FBTCxFQUFRRSxLQUFLLEdBQUc4RCxLQUFLLENBQUNoTSxNQUEzQixFQUFtQ2dJLEVBQUUsR0FBR0UsS0FBeEMsRUFBK0NGLEVBQUUsRUFBakQsRUFBcUQ7QUFDbkQ3QyxnQkFBSSxHQUFHNkcsS0FBSyxDQUFDaEUsRUFBRCxDQUFaOztBQUNBTSxvQkFBUSxDQUFDM0csSUFBVCxDQUFjcUgsS0FBSyxDQUFDNEosZ0JBQU4sQ0FBdUI1RyxLQUF2QixFQUE4QmdLLFFBQVEsSUFBSWhOLEtBQUssQ0FBQy9DLE9BQU4sQ0FBY3JCLGlCQUFkLENBQWdDaUgsT0FBaEMsQ0FBd0MsZ0JBQXhDLEVBQTBEbUosR0FBRyxDQUFDN0wsTUFBOUQsQ0FBMUMsRUFBaUg2TCxHQUFqSCxDQUFkO0FBQ0Q7O0FBQ0QsaUJBQU8xTSxRQUFQO0FBQ0QsU0FSRDtBQVNELE9BVmEsQ0FVWCxJQVZXLENBQWQ7O0FBV0EyTixvQkFBYyxHQUFJLFVBQVNqTixLQUFULEVBQWdCO0FBQ2hDLGVBQU8sVUFBU3ZLLENBQVQsRUFBWTtBQUNqQixjQUFJZ1ksZ0JBQUosRUFBc0JsTSxRQUF0QixFQUFnQ3ZDLEVBQWhDLEVBQW9DQyxFQUFwQyxFQUF3Q2lPLEVBQXhDLEVBQTRDaE8sS0FBNUMsRUFBbURDLEtBQW5ELEVBQTBEZ08sS0FBMUQsRUFBaUU3TixRQUFqRTs7QUFDQSxjQUFJN0osQ0FBQyxJQUFJLElBQVQsRUFBZTtBQUNiOEwsb0JBQVEsR0FBRyxNQUFNOUwsQ0FBQyxDQUFDaVksTUFBUixHQUFpQmpZLENBQUMsQ0FBQ2tSLEtBQTlCOztBQUNBLGlCQUFLM0gsRUFBRSxHQUFHLENBQUwsRUFBUUUsS0FBSyxHQUFHOEQsS0FBSyxDQUFDaE0sTUFBM0IsRUFBbUNnSSxFQUFFLEdBQUdFLEtBQXhDLEVBQStDRixFQUFFLEVBQWpELEVBQXFEO0FBQ25EN0Msa0JBQUksR0FBRzZHLEtBQUssQ0FBQ2hFLEVBQUQsQ0FBWjtBQUNBN0Msa0JBQUksQ0FBQ3VLLE1BQUwsR0FBYztBQUNabkYsd0JBQVEsRUFBRUEsUUFERTtBQUVab0YscUJBQUssRUFBRWxSLENBQUMsQ0FBQ2tSLEtBRkc7QUFHWm5GLHlCQUFTLEVBQUUvTCxDQUFDLENBQUNpWTtBQUhELGVBQWQ7QUFLRDtBQUNGLFdBVkQsTUFVTztBQUNMRCw0QkFBZ0IsR0FBRyxJQUFuQjtBQUNBbE0sb0JBQVEsR0FBRyxHQUFYOztBQUNBLGlCQUFLdEMsRUFBRSxHQUFHLENBQUwsRUFBUUUsS0FBSyxHQUFHNkQsS0FBSyxDQUFDaE0sTUFBM0IsRUFBbUNpSSxFQUFFLEdBQUdFLEtBQXhDLEVBQStDRixFQUFFLEVBQWpELEVBQXFEO0FBQ25EOUMsa0JBQUksR0FBRzZHLEtBQUssQ0FBQy9ELEVBQUQsQ0FBWjs7QUFDQSxrQkFBSSxFQUFFOUMsSUFBSSxDQUFDdUssTUFBTCxDQUFZbkYsUUFBWixLQUF5QixHQUF6QixJQUFnQ3BGLElBQUksQ0FBQ3VLLE1BQUwsQ0FBWWxGLFNBQVosS0FBMEJyRixJQUFJLENBQUN1SyxNQUFMLENBQVlDLEtBQXhFLENBQUosRUFBb0Y7QUFDbEY4RyxnQ0FBZ0IsR0FBRyxLQUFuQjtBQUNEOztBQUNEdFIsa0JBQUksQ0FBQ3VLLE1BQUwsQ0FBWW5GLFFBQVosR0FBdUJBLFFBQXZCO0FBQ0FwRixrQkFBSSxDQUFDdUssTUFBTCxDQUFZbEYsU0FBWixHQUF3QnJGLElBQUksQ0FBQ3VLLE1BQUwsQ0FBWUMsS0FBcEM7QUFDRDs7QUFDRCxnQkFBSThHLGdCQUFKLEVBQXNCO0FBQ3BCO0FBQ0Q7QUFDRjs7QUFDRG5PLGtCQUFRLEdBQUcsRUFBWDs7QUFDQSxlQUFLNE4sRUFBRSxHQUFHLENBQUwsRUFBUUMsS0FBSyxHQUFHbkssS0FBSyxDQUFDaE0sTUFBM0IsRUFBbUNrVyxFQUFFLEdBQUdDLEtBQXhDLEVBQStDRCxFQUFFLEVBQWpELEVBQXFEO0FBQ25EL1EsZ0JBQUksR0FBRzZHLEtBQUssQ0FBQ2tLLEVBQUQsQ0FBWjs7QUFDQTVOLG9CQUFRLENBQUMzRyxJQUFULENBQWNxSCxLQUFLLENBQUNwSCxJQUFOLENBQVcsZ0JBQVgsRUFBNkJ1RCxJQUE3QixFQUFtQ29GLFFBQW5DLEVBQTZDcEYsSUFBSSxDQUFDdUssTUFBTCxDQUFZbEYsU0FBekQsQ0FBZDtBQUNEOztBQUNELGlCQUFPbEMsUUFBUDtBQUNELFNBakNEO0FBa0NELE9BbkNnQixDQW1DZCxJQW5DYyxDQUFqQjs7QUFvQ0EwTSxTQUFHLENBQUN0QixNQUFKLEdBQWMsVUFBUzFLLEtBQVQsRUFBZ0I7QUFDNUIsZUFBTyxVQUFTdkssQ0FBVCxFQUFZO0FBQ2pCLGNBQUlnSCxJQUFKOztBQUNBLGNBQUl1RyxLQUFLLENBQUMsQ0FBRCxDQUFMLENBQVM3QyxNQUFULEtBQW9CdkwsUUFBUSxDQUFDdVgsUUFBakMsRUFBMkM7QUFDekM7QUFDRDs7QUFDRCxjQUFJSCxHQUFHLENBQUMyQixVQUFKLEtBQW1CLENBQXZCLEVBQTBCO0FBQ3hCO0FBQ0Q7O0FBQ0RYLGtCQUFRLEdBQUdoQixHQUFHLENBQUM0QixZQUFmOztBQUNBLGNBQUk1QixHQUFHLENBQUM2QixpQkFBSixDQUFzQixjQUF0QixLQUF5QyxDQUFDN0IsR0FBRyxDQUFDNkIsaUJBQUosQ0FBc0IsY0FBdEIsRUFBc0N4SCxPQUF0QyxDQUE4QyxrQkFBOUMsQ0FBOUMsRUFBaUg7QUFDL0csZ0JBQUk7QUFDRjJHLHNCQUFRLEdBQUdjLElBQUksQ0FBQ0MsS0FBTCxDQUFXZixRQUFYLENBQVg7QUFDRCxhQUZELENBRUUsT0FBT3JILE1BQVAsRUFBZTtBQUNmbFEsZUFBQyxHQUFHa1EsTUFBSjtBQUNBcUgsc0JBQVEsR0FBRyxvQ0FBWDtBQUNEO0FBQ0Y7O0FBQ0RDLHdCQUFjOztBQUNkLGNBQUksRUFBRyxRQUFReFEsSUFBSSxHQUFHdVAsR0FBRyxDQUFDN0wsTUFBbkIsS0FBOEIxRCxJQUFJLEdBQUcsR0FBeEMsQ0FBSixFQUFtRDtBQUNqRCxtQkFBTytQLFdBQVcsRUFBbEI7QUFDRCxXQUZELE1BRU87QUFDTCxtQkFBT3hNLEtBQUssQ0FBQ2dPLFNBQU4sQ0FBZ0JoTCxLQUFoQixFQUF1QmdLLFFBQXZCLEVBQWlDdlgsQ0FBakMsQ0FBUDtBQUNEO0FBQ0YsU0F2QkQ7QUF3QkQsT0F6QlksQ0F5QlYsSUF6QlUsQ0FBYjs7QUEwQkF1VyxTQUFHLENBQUNQLE9BQUosR0FBZSxVQUFTekwsS0FBVCxFQUFnQjtBQUM3QixlQUFPLFlBQVc7QUFDaEIsY0FBSWdELEtBQUssQ0FBQyxDQUFELENBQUwsQ0FBUzdDLE1BQVQsS0FBb0J2TCxRQUFRLENBQUN1WCxRQUFqQyxFQUEyQztBQUN6QztBQUNEOztBQUNELGlCQUFPSyxXQUFXLEVBQWxCO0FBQ0QsU0FMRDtBQU1ELE9BUGEsQ0FPWCxJQVBXLENBQWQ7O0FBUUFPLGlCQUFXLEdBQUcsQ0FBQ3RRLElBQUksR0FBR3VQLEdBQUcsQ0FBQ3RGLE1BQVosS0FBdUIsSUFBdkIsR0FBOEJqSyxJQUE5QixHQUFxQ3VQLEdBQW5EO0FBQ0FlLGlCQUFXLENBQUNrQixVQUFaLEdBQXlCaEIsY0FBekI7QUFDQU4sYUFBTyxHQUFHO0FBQ1Isa0JBQVUsa0JBREY7QUFFUix5QkFBaUIsVUFGVDtBQUdSLDRCQUFvQjtBQUhaLE9BQVY7O0FBS0EsVUFBSSxLQUFLMVAsT0FBTCxDQUFhMFAsT0FBakIsRUFBMEI7QUFDeEJoVCxjQUFNLENBQUNnVCxPQUFELEVBQVUsS0FBSzFQLE9BQUwsQ0FBYTBQLE9BQXZCLENBQU47QUFDRDs7QUFDRCxXQUFLRixVQUFMLElBQW1CRSxPQUFuQixFQUE0QjtBQUMxQkQsbUJBQVcsR0FBR0MsT0FBTyxDQUFDRixVQUFELENBQXJCOztBQUNBLFlBQUlDLFdBQUosRUFBaUI7QUFDZlYsYUFBRyxDQUFDa0MsZ0JBQUosQ0FBcUJ6QixVQUFyQixFQUFpQ0MsV0FBakM7QUFDRDtBQUNGOztBQUNESCxjQUFRLEdBQUcsSUFBSTRCLFFBQUosRUFBWDs7QUFDQSxVQUFJLEtBQUtsUixPQUFMLENBQWFyQyxNQUFqQixFQUF5QjtBQUN2QndFLGFBQUssR0FBRyxLQUFLbkMsT0FBTCxDQUFhckMsTUFBckI7O0FBQ0EsYUFBSzNDLEdBQUwsSUFBWW1ILEtBQVosRUFBbUI7QUFDakJySSxlQUFLLEdBQUdxSSxLQUFLLENBQUNuSCxHQUFELENBQWI7QUFDQXNVLGtCQUFRLENBQUM2QixNQUFULENBQWdCblcsR0FBaEIsRUFBcUJsQixLQUFyQjtBQUNEO0FBQ0Y7O0FBQ0QsV0FBS2lJLEVBQUUsR0FBRyxDQUFMLEVBQVFFLEtBQUssR0FBRzhELEtBQUssQ0FBQ2hNLE1BQTNCLEVBQW1DZ0ksRUFBRSxHQUFHRSxLQUF4QyxFQUErQ0YsRUFBRSxFQUFqRCxFQUFxRDtBQUNuRDdDLFlBQUksR0FBRzZHLEtBQUssQ0FBQ2hFLEVBQUQsQ0FBWjtBQUNBLGFBQUtwRyxJQUFMLENBQVUsU0FBVixFQUFxQnVELElBQXJCLEVBQTJCNlAsR0FBM0IsRUFBZ0NPLFFBQWhDO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLdFAsT0FBTCxDQUFhOUMsY0FBakIsRUFBaUM7QUFDL0IsYUFBS3ZCLElBQUwsQ0FBVSxpQkFBVixFQUE2Qm9LLEtBQTdCLEVBQW9DZ0osR0FBcEMsRUFBeUNPLFFBQXpDO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLN1AsT0FBTCxDQUFhaUksT0FBYixLQUF5QixNQUE3QixFQUFxQztBQUNuQ3RGLGFBQUssR0FBRyxLQUFLM0MsT0FBTCxDQUFhZ0QsZ0JBQWIsQ0FBOEIsaUNBQTlCLENBQVI7O0FBQ0EsYUFBS1QsRUFBRSxHQUFHLENBQUwsRUFBUUUsS0FBSyxHQUFHRSxLQUFLLENBQUNySSxNQUEzQixFQUFtQ2lJLEVBQUUsR0FBR0UsS0FBeEMsRUFBK0NGLEVBQUUsRUFBakQsRUFBcUQ7QUFDbkQyTixlQUFLLEdBQUd2TixLQUFLLENBQUNKLEVBQUQsQ0FBYjtBQUNBNE4sbUJBQVMsR0FBR0QsS0FBSyxDQUFDbkosWUFBTixDQUFtQixNQUFuQixDQUFaO0FBQ0FxSixtQkFBUyxHQUFHRixLQUFLLENBQUNuSixZQUFOLENBQW1CLE1BQW5CLENBQVo7O0FBQ0EsY0FBSW1KLEtBQUssQ0FBQ2pJLE9BQU4sS0FBa0IsUUFBbEIsSUFBOEJpSSxLQUFLLENBQUN5QixZQUFOLENBQW1CLFVBQW5CLENBQWxDLEVBQWtFO0FBQ2hFakQsaUJBQUssR0FBR3dCLEtBQUssQ0FBQzNQLE9BQWQ7O0FBQ0EsaUJBQUtpUSxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxLQUFLLEdBQUcvQixLQUFLLENBQUNwVSxNQUEzQixFQUFtQ2tXLEVBQUUsR0FBR0MsS0FBeEMsRUFBK0NELEVBQUUsRUFBakQsRUFBcUQ7QUFDbkRiLG9CQUFNLEdBQUdqQixLQUFLLENBQUM4QixFQUFELENBQWQ7O0FBQ0Esa0JBQUliLE1BQU0sQ0FBQ2lDLFFBQVgsRUFBcUI7QUFDbkIvQix3QkFBUSxDQUFDNkIsTUFBVCxDQUFnQnZCLFNBQWhCLEVBQTJCUixNQUFNLENBQUN0VixLQUFsQztBQUNEO0FBQ0Y7QUFDRixXQVJELE1BUU8sSUFBSSxDQUFDK1YsU0FBRCxJQUFlLENBQUNPLEtBQUssR0FBR1AsU0FBUyxDQUFDeUIsV0FBVixFQUFULE1BQXNDLFVBQXRDLElBQW9EbEIsS0FBSyxLQUFLLE9BQTdFLElBQXlGVCxLQUFLLENBQUM0QixPQUFuRyxFQUE0RztBQUNqSGpDLG9CQUFRLENBQUM2QixNQUFULENBQWdCdkIsU0FBaEIsRUFBMkJELEtBQUssQ0FBQzdWLEtBQWpDO0FBQ0Q7QUFDRjtBQUNGOztBQUNELFdBQUt5QyxDQUFDLEdBQUc0VCxFQUFFLEdBQUcsQ0FBVCxFQUFZRSxLQUFLLEdBQUd0SyxLQUFLLENBQUNoTSxNQUFOLEdBQWUsQ0FBeEMsRUFBMkMsS0FBS3NXLEtBQUwsR0FBYUYsRUFBRSxJQUFJRSxLQUFuQixHQUEyQkYsRUFBRSxJQUFJRSxLQUE1RSxFQUFtRjlULENBQUMsR0FBRyxLQUFLOFQsS0FBTCxHQUFhLEVBQUVGLEVBQWYsR0FBb0IsRUFBRUEsRUFBN0csRUFBaUg7QUFDL0diLGdCQUFRLENBQUM2QixNQUFULENBQWdCLEtBQUt4SCxhQUFMLENBQW1CcE4sQ0FBbkIsQ0FBaEIsRUFBdUN3SixLQUFLLENBQUN4SixDQUFELENBQTVDLEVBQWlEd0osS0FBSyxDQUFDeEosQ0FBRCxDQUFMLENBQVNtRyxJQUExRDtBQUNEOztBQUNELGFBQU8sS0FBSzhPLGFBQUwsQ0FBbUJ6QyxHQUFuQixFQUF3Qk8sUUFBeEIsRUFBa0N2SixLQUFsQyxDQUFQO0FBQ0QsS0FuSkQ7O0FBcUpBcE8sWUFBUSxDQUFDeUQsU0FBVCxDQUFtQm9XLGFBQW5CLEdBQW1DLFVBQVN6QyxHQUFULEVBQWNPLFFBQWQsRUFBd0J2SixLQUF4QixFQUErQjtBQUNoRSxhQUFPZ0osR0FBRyxDQUFDMEMsSUFBSixDQUFTbkMsUUFBVCxDQUFQO0FBQ0QsS0FGRDs7QUFJQTNYLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUIyVixTQUFuQixHQUErQixVQUFTaEwsS0FBVCxFQUFnQjRLLFlBQWhCLEVBQThCblksQ0FBOUIsRUFBaUM7QUFDOUQsVUFBSTBHLElBQUosRUFBVW5ELEVBQVYsRUFBY0MsSUFBZDs7QUFDQSxXQUFLRCxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUcrSixLQUFLLENBQUNoTSxNQUExQixFQUFrQ2dDLEVBQUUsR0FBR0MsSUFBdkMsRUFBNkNELEVBQUUsRUFBL0MsRUFBbUQ7QUFDakRtRCxZQUFJLEdBQUc2RyxLQUFLLENBQUNoSyxFQUFELENBQVo7QUFDQW1ELFlBQUksQ0FBQ2dFLE1BQUwsR0FBY3ZMLFFBQVEsQ0FBQytaLE9BQXZCO0FBQ0EsYUFBSy9WLElBQUwsQ0FBVSxTQUFWLEVBQXFCdUQsSUFBckIsRUFBMkJ5UixZQUEzQixFQUF5Q25ZLENBQXpDO0FBQ0EsYUFBS21ELElBQUwsQ0FBVSxVQUFWLEVBQXNCdUQsSUFBdEI7QUFDRDs7QUFDRCxVQUFJLEtBQUtjLE9BQUwsQ0FBYTlDLGNBQWpCLEVBQWlDO0FBQy9CLGFBQUt2QixJQUFMLENBQVUsaUJBQVYsRUFBNkJvSyxLQUE3QixFQUFvQzRLLFlBQXBDLEVBQWtEblksQ0FBbEQ7QUFDQSxhQUFLbUQsSUFBTCxDQUFVLGtCQUFWLEVBQThCb0ssS0FBOUI7QUFDRDs7QUFDRCxVQUFJLEtBQUsvRixPQUFMLENBQWFoQyxnQkFBakIsRUFBbUM7QUFDakMsZUFBTyxLQUFLOE8sWUFBTCxFQUFQO0FBQ0Q7QUFDRixLQWZEOztBQWlCQW5WLFlBQVEsQ0FBQ3lELFNBQVQsQ0FBbUJ1UixnQkFBbkIsR0FBc0MsVUFBUzVHLEtBQVQsRUFBZ0I5QixPQUFoQixFQUF5QjhLLEdBQXpCLEVBQThCO0FBQ2xFLFVBQUk3UCxJQUFKLEVBQVVuRCxFQUFWLEVBQWNDLElBQWQ7O0FBQ0EsV0FBS0QsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHK0osS0FBSyxDQUFDaE0sTUFBMUIsRUFBa0NnQyxFQUFFLEdBQUdDLElBQXZDLEVBQTZDRCxFQUFFLEVBQS9DLEVBQW1EO0FBQ2pEbUQsWUFBSSxHQUFHNkcsS0FBSyxDQUFDaEssRUFBRCxDQUFaO0FBQ0FtRCxZQUFJLENBQUNnRSxNQUFMLEdBQWN2TCxRQUFRLENBQUNnYSxLQUF2QjtBQUNBLGFBQUtoVyxJQUFMLENBQVUsT0FBVixFQUFtQnVELElBQW5CLEVBQXlCK0UsT0FBekIsRUFBa0M4SyxHQUFsQztBQUNBLGFBQUtwVCxJQUFMLENBQVUsVUFBVixFQUFzQnVELElBQXRCO0FBQ0Q7O0FBQ0QsVUFBSSxLQUFLYyxPQUFMLENBQWE5QyxjQUFqQixFQUFpQztBQUMvQixhQUFLdkIsSUFBTCxDQUFVLGVBQVYsRUFBMkJvSyxLQUEzQixFQUFrQzlCLE9BQWxDLEVBQTJDOEssR0FBM0M7QUFDQSxhQUFLcFQsSUFBTCxDQUFVLGtCQUFWLEVBQThCb0ssS0FBOUI7QUFDRDs7QUFDRCxVQUFJLEtBQUsvRixPQUFMLENBQWFoQyxnQkFBakIsRUFBbUM7QUFDakMsZUFBTyxLQUFLOE8sWUFBTCxFQUFQO0FBQ0Q7QUFDRixLQWZEOztBQWlCQSxXQUFPblYsUUFBUDtBQUVELEdBanlDVSxDQWl5Q1J3QyxPQWp5Q1EsQ0FBWDs7QUFteUNBeEMsVUFBUSxDQUFDZ08sT0FBVCxHQUFtQixPQUFuQjtBQUVBaE8sVUFBUSxDQUFDcUksT0FBVCxHQUFtQixFQUFuQjs7QUFFQXJJLFVBQVEsQ0FBQzJPLGlCQUFULEdBQTZCLFVBQVM3RyxPQUFULEVBQWtCO0FBQzdDLFFBQUlBLE9BQU8sQ0FBQytHLFlBQVIsQ0FBcUIsSUFBckIsQ0FBSixFQUFnQztBQUM5QixhQUFPN08sUUFBUSxDQUFDcUksT0FBVCxDQUFpQjVGLFFBQVEsQ0FBQ3FGLE9BQU8sQ0FBQytHLFlBQVIsQ0FBcUIsSUFBckIsQ0FBRCxDQUF6QixDQUFQO0FBQ0QsS0FGRCxNQUVPO0FBQ0wsYUFBTyxLQUFLLENBQVo7QUFDRDtBQUNGLEdBTkQ7O0FBUUE3TyxVQUFRLENBQUMwTyxTQUFULEdBQXFCLEVBQXJCOztBQUVBMU8sVUFBUSxDQUFDaWEsVUFBVCxHQUFzQixVQUFTblMsT0FBVCxFQUFrQjtBQUN0QyxRQUFJLE9BQU9BLE9BQVAsS0FBbUIsUUFBdkIsRUFBaUM7QUFDL0JBLGFBQU8sR0FBR3VHLFFBQVEsQ0FBQ0MsYUFBVCxDQUF1QnhHLE9BQXZCLENBQVY7QUFDRDs7QUFDRCxRQUFJLENBQUNBLE9BQU8sSUFBSSxJQUFYLEdBQWtCQSxPQUFPLENBQUMyRyxRQUExQixHQUFxQyxLQUFLLENBQTNDLEtBQWlELElBQXJELEVBQTJEO0FBQ3pELFlBQU0sSUFBSUQsS0FBSixDQUFVLGdOQUFWLENBQU47QUFDRDs7QUFDRCxXQUFPMUcsT0FBTyxDQUFDMkcsUUFBZjtBQUNELEdBUkQ7O0FBVUF6TyxVQUFRLENBQUNrYSxZQUFULEdBQXdCLElBQXhCOztBQUVBbGEsVUFBUSxDQUFDbWEsUUFBVCxHQUFvQixZQUFXO0FBQzdCLFFBQUlDLGFBQUosRUFBbUIzTCxRQUFuQixFQUE2QjRMLFNBQTdCLEVBQXdDalcsRUFBeEMsRUFBNENDLElBQTVDLEVBQWtEcUcsUUFBbEQ7O0FBQ0EsUUFBSTJELFFBQVEsQ0FBQ3ZELGdCQUFiLEVBQStCO0FBQzdCdVAsZUFBUyxHQUFHaE0sUUFBUSxDQUFDdkQsZ0JBQVQsQ0FBMEIsV0FBMUIsQ0FBWjtBQUNELEtBRkQsTUFFTztBQUNMdVAsZUFBUyxHQUFHLEVBQVo7O0FBQ0FELG1CQUFhLEdBQUcsdUJBQVM3SCxRQUFULEVBQW1CO0FBQ2pDLFlBQUlDLEVBQUosRUFBUXBPLEVBQVIsRUFBWUMsSUFBWixFQUFrQnFHLFFBQWxCOztBQUNBQSxnQkFBUSxHQUFHLEVBQVg7O0FBQ0EsYUFBS3RHLEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBR2tPLFFBQVEsQ0FBQ25RLE1BQTdCLEVBQXFDZ0MsRUFBRSxHQUFHQyxJQUExQyxFQUFnREQsRUFBRSxFQUFsRCxFQUFzRDtBQUNwRG9PLFlBQUUsR0FBR0QsUUFBUSxDQUFDbk8sRUFBRCxDQUFiOztBQUNBLGNBQUkscUJBQXFCNkQsSUFBckIsQ0FBMEJ1SyxFQUFFLENBQUN6SyxTQUE3QixDQUFKLEVBQTZDO0FBQzNDMkMsb0JBQVEsQ0FBQzNHLElBQVQsQ0FBY3NXLFNBQVMsQ0FBQ3RXLElBQVYsQ0FBZXlPLEVBQWYsQ0FBZDtBQUNELFdBRkQsTUFFTztBQUNMOUgsb0JBQVEsQ0FBQzNHLElBQVQsQ0FBYyxLQUFLLENBQW5CO0FBQ0Q7QUFDRjs7QUFDRCxlQUFPMkcsUUFBUDtBQUNELE9BWkQ7O0FBYUEwUCxtQkFBYSxDQUFDL0wsUUFBUSxDQUFDckcsb0JBQVQsQ0FBOEIsS0FBOUIsQ0FBRCxDQUFiO0FBQ0FvUyxtQkFBYSxDQUFDL0wsUUFBUSxDQUFDckcsb0JBQVQsQ0FBOEIsTUFBOUIsQ0FBRCxDQUFiO0FBQ0Q7O0FBQ0QwQyxZQUFRLEdBQUcsRUFBWDs7QUFDQSxTQUFLdEcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHZ1csU0FBUyxDQUFDalksTUFBOUIsRUFBc0NnQyxFQUFFLEdBQUdDLElBQTNDLEVBQWlERCxFQUFFLEVBQW5ELEVBQXVEO0FBQ3JEcUssY0FBUSxHQUFHNEwsU0FBUyxDQUFDalcsRUFBRCxDQUFwQjs7QUFDQSxVQUFJcEUsUUFBUSxDQUFDMk8saUJBQVQsQ0FBMkJGLFFBQTNCLE1BQXlDLEtBQTdDLEVBQW9EO0FBQ2xEL0QsZ0JBQVEsQ0FBQzNHLElBQVQsQ0FBYyxJQUFJL0QsUUFBSixDQUFheU8sUUFBYixDQUFkO0FBQ0QsT0FGRCxNQUVPO0FBQ0wvRCxnQkFBUSxDQUFDM0csSUFBVCxDQUFjLEtBQUssQ0FBbkI7QUFDRDtBQUNGOztBQUNELFdBQU8yRyxRQUFQO0FBQ0QsR0FoQ0Q7O0FBa0NBMUssVUFBUSxDQUFDc2EsbUJBQVQsR0FBK0IsQ0FBQyxnQ0FBRCxDQUEvQjs7QUFFQXRhLFVBQVEsQ0FBQzRPLGtCQUFULEdBQThCLFlBQVc7QUFDdkMsUUFBSTJMLGNBQUosRUFBb0JDLEtBQXBCLEVBQTJCcFcsRUFBM0IsRUFBK0JDLElBQS9CLEVBQXFDd0QsSUFBckM7O0FBQ0EwUyxrQkFBYyxHQUFHLElBQWpCOztBQUNBLFFBQUkxYSxNQUFNLENBQUM0YSxJQUFQLElBQWU1YSxNQUFNLENBQUNnVyxVQUF0QixJQUFvQ2hXLE1BQU0sQ0FBQzZhLFFBQTNDLElBQXVEN2EsTUFBTSxDQUFDOGEsSUFBOUQsSUFBc0U5YSxNQUFNLENBQUMwWixRQUE3RSxJQUF5RmxMLFFBQVEsQ0FBQ0MsYUFBdEcsRUFBcUg7QUFDbkgsVUFBSSxFQUFFLGVBQWVELFFBQVEsQ0FBQ25HLGFBQVQsQ0FBdUIsR0FBdkIsQ0FBakIsQ0FBSixFQUFtRDtBQUNqRHFTLHNCQUFjLEdBQUcsS0FBakI7QUFDRCxPQUZELE1BRU87QUFDTDFTLFlBQUksR0FBRzdILFFBQVEsQ0FBQ3NhLG1CQUFoQjs7QUFDQSxhQUFLbFcsRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHd0QsSUFBSSxDQUFDekYsTUFBekIsRUFBaUNnQyxFQUFFLEdBQUdDLElBQXRDLEVBQTRDRCxFQUFFLEVBQTlDLEVBQWtEO0FBQ2hEb1csZUFBSyxHQUFHM1MsSUFBSSxDQUFDekQsRUFBRCxDQUFaOztBQUNBLGNBQUlvVyxLQUFLLENBQUN2UyxJQUFOLENBQVcyUyxTQUFTLENBQUNDLFNBQXJCLENBQUosRUFBcUM7QUFDbkNOLDBCQUFjLEdBQUcsS0FBakI7QUFDQTtBQUNEO0FBQ0Y7QUFDRjtBQUNGLEtBYkQsTUFhTztBQUNMQSxvQkFBYyxHQUFHLEtBQWpCO0FBQ0Q7O0FBQ0QsV0FBT0EsY0FBUDtBQUNELEdBcEJEOztBQXNCQXpYLFNBQU8sR0FBRyxpQkFBU2dZLElBQVQsRUFBZUMsWUFBZixFQUE2QjtBQUNyQyxRQUFJbEgsSUFBSixFQUFVelAsRUFBVixFQUFjQyxJQUFkLEVBQW9CcUcsUUFBcEI7O0FBQ0FBLFlBQVEsR0FBRyxFQUFYOztBQUNBLFNBQUt0RyxFQUFFLEdBQUcsQ0FBTCxFQUFRQyxJQUFJLEdBQUd5VyxJQUFJLENBQUMxWSxNQUF6QixFQUFpQ2dDLEVBQUUsR0FBR0MsSUFBdEMsRUFBNENELEVBQUUsRUFBOUMsRUFBa0Q7QUFDaER5UCxVQUFJLEdBQUdpSCxJQUFJLENBQUMxVyxFQUFELENBQVg7O0FBQ0EsVUFBSXlQLElBQUksS0FBS2tILFlBQWIsRUFBMkI7QUFDekJyUSxnQkFBUSxDQUFDM0csSUFBVCxDQUFjOFAsSUFBZDtBQUNEO0FBQ0Y7O0FBQ0QsV0FBT25KLFFBQVA7QUFDRCxHQVZEOztBQVlBakksVUFBUSxHQUFHLGtCQUFTdVksR0FBVCxFQUFjO0FBQ3ZCLFdBQU9BLEdBQUcsQ0FBQy9NLE9BQUosQ0FBWSxZQUFaLEVBQTBCLFVBQVNzSCxLQUFULEVBQWdCO0FBQy9DLGFBQU9BLEtBQUssQ0FBQzBGLE1BQU4sQ0FBYSxDQUFiLEVBQWdCbk0sV0FBaEIsRUFBUDtBQUNELEtBRk0sQ0FBUDtBQUdELEdBSkQ7O0FBTUE5TyxVQUFRLENBQUNrSSxhQUFULEdBQXlCLFVBQVNnVCxNQUFULEVBQWlCO0FBQ3hDLFFBQUlDLEdBQUo7QUFDQUEsT0FBRyxHQUFHOU0sUUFBUSxDQUFDbkcsYUFBVCxDQUF1QixLQUF2QixDQUFOO0FBQ0FpVCxPQUFHLENBQUNuUSxTQUFKLEdBQWdCa1EsTUFBaEI7QUFDQSxXQUFPQyxHQUFHLENBQUNDLFVBQUosQ0FBZSxDQUFmLENBQVA7QUFDRCxHQUxEOztBQU9BcGIsVUFBUSxDQUFDb1IsYUFBVCxHQUF5QixVQUFTdEosT0FBVCxFQUFrQnVULFNBQWxCLEVBQTZCO0FBQ3BELFFBQUl2VCxPQUFPLEtBQUt1VCxTQUFoQixFQUEyQjtBQUN6QixhQUFPLElBQVA7QUFDRDs7QUFDRCxXQUFPdlQsT0FBTyxHQUFHQSxPQUFPLENBQUM4RCxVQUF6QixFQUFxQztBQUNuQyxVQUFJOUQsT0FBTyxLQUFLdVQsU0FBaEIsRUFBMkI7QUFDekIsZUFBTyxJQUFQO0FBQ0Q7QUFDRjs7QUFDRCxXQUFPLEtBQVA7QUFDRCxHQVZEOztBQVlBcmIsVUFBUSxDQUFDZ1AsVUFBVCxHQUFzQixVQUFTd0QsRUFBVCxFQUFhekgsSUFBYixFQUFtQjtBQUN2QyxRQUFJakQsT0FBSjs7QUFDQSxRQUFJLE9BQU8wSyxFQUFQLEtBQWMsUUFBbEIsRUFBNEI7QUFDMUIxSyxhQUFPLEdBQUd1RyxRQUFRLENBQUNDLGFBQVQsQ0FBdUJrRSxFQUF2QixDQUFWO0FBQ0QsS0FGRCxNQUVPLElBQUlBLEVBQUUsQ0FBQ2pFLFFBQUgsSUFBZSxJQUFuQixFQUF5QjtBQUM5QnpHLGFBQU8sR0FBRzBLLEVBQVY7QUFDRDs7QUFDRCxRQUFJMUssT0FBTyxJQUFJLElBQWYsRUFBcUI7QUFDbkIsWUFBTSxJQUFJMEcsS0FBSixDQUFVLGNBQWN6RCxJQUFkLEdBQXFCLDJFQUEvQixDQUFOO0FBQ0Q7O0FBQ0QsV0FBT2pELE9BQVA7QUFDRCxHQVhEOztBQWFBOUgsVUFBUSxDQUFDaVAsV0FBVCxHQUF1QixVQUFTcU0sR0FBVCxFQUFjdlEsSUFBZCxFQUFvQjtBQUN6QyxRQUFJbEssQ0FBSixFQUFPMlIsRUFBUCxFQUFXRCxRQUFYLEVBQXFCbk8sRUFBckIsRUFBeUJnRyxFQUF6QixFQUE2Qi9GLElBQTdCLEVBQW1DaUcsS0FBbkMsRUFBMEN6QyxJQUExQzs7QUFDQSxRQUFJeVQsR0FBRyxZQUFZQyxLQUFuQixFQUEwQjtBQUN4QmhKLGNBQVEsR0FBRyxFQUFYOztBQUNBLFVBQUk7QUFDRixhQUFLbk8sRUFBRSxHQUFHLENBQUwsRUFBUUMsSUFBSSxHQUFHaVgsR0FBRyxDQUFDbFosTUFBeEIsRUFBZ0NnQyxFQUFFLEdBQUdDLElBQXJDLEVBQTJDRCxFQUFFLEVBQTdDLEVBQWlEO0FBQy9Db08sWUFBRSxHQUFHOEksR0FBRyxDQUFDbFgsRUFBRCxDQUFSO0FBQ0FtTyxrQkFBUSxDQUFDeE8sSUFBVCxDQUFjLEtBQUtpTCxVQUFMLENBQWdCd0QsRUFBaEIsRUFBb0J6SCxJQUFwQixDQUFkO0FBQ0Q7QUFDRixPQUxELENBS0UsT0FBT2dHLE1BQVAsRUFBZTtBQUNmbFEsU0FBQyxHQUFHa1EsTUFBSjtBQUNBd0IsZ0JBQVEsR0FBRyxJQUFYO0FBQ0Q7QUFDRixLQVhELE1BV08sSUFBSSxPQUFPK0ksR0FBUCxLQUFlLFFBQW5CLEVBQTZCO0FBQ2xDL0ksY0FBUSxHQUFHLEVBQVg7QUFDQTFLLFVBQUksR0FBR3dHLFFBQVEsQ0FBQ3ZELGdCQUFULENBQTBCd1EsR0FBMUIsQ0FBUDs7QUFDQSxXQUFLbFIsRUFBRSxHQUFHLENBQUwsRUFBUUUsS0FBSyxHQUFHekMsSUFBSSxDQUFDekYsTUFBMUIsRUFBa0NnSSxFQUFFLEdBQUdFLEtBQXZDLEVBQThDRixFQUFFLEVBQWhELEVBQW9EO0FBQ2xEb0ksVUFBRSxHQUFHM0ssSUFBSSxDQUFDdUMsRUFBRCxDQUFUO0FBQ0FtSSxnQkFBUSxDQUFDeE8sSUFBVCxDQUFjeU8sRUFBZDtBQUNEO0FBQ0YsS0FQTSxNQU9BLElBQUk4SSxHQUFHLENBQUMvTSxRQUFKLElBQWdCLElBQXBCLEVBQTBCO0FBQy9CZ0UsY0FBUSxHQUFHLENBQUMrSSxHQUFELENBQVg7QUFDRDs7QUFDRCxRQUFJLEVBQUcvSSxRQUFRLElBQUksSUFBYixJQUFzQkEsUUFBUSxDQUFDblEsTUFBakMsQ0FBSixFQUE4QztBQUM1QyxZQUFNLElBQUlvTSxLQUFKLENBQVUsY0FBY3pELElBQWQsR0FBcUIsNEZBQS9CLENBQU47QUFDRDs7QUFDRCxXQUFPd0gsUUFBUDtBQUNELEdBM0JEOztBQTZCQXZTLFVBQVEsQ0FBQ3lMLE9BQVQsR0FBbUIsVUFBUytQLFFBQVQsRUFBbUJyTSxRQUFuQixFQUE2QnNNLFFBQTdCLEVBQXVDO0FBQ3hELFFBQUk1YixNQUFNLENBQUM0TCxPQUFQLENBQWUrUCxRQUFmLENBQUosRUFBOEI7QUFDNUIsYUFBT3JNLFFBQVEsRUFBZjtBQUNELEtBRkQsTUFFTyxJQUFJc00sUUFBUSxJQUFJLElBQWhCLEVBQXNCO0FBQzNCLGFBQU9BLFFBQVEsRUFBZjtBQUNEO0FBQ0YsR0FORDs7QUFRQXpiLFVBQVEsQ0FBQzhVLFdBQVQsR0FBdUIsVUFBU3ZOLElBQVQsRUFBZXBCLGFBQWYsRUFBOEI7QUFDbkQsUUFBSXVWLFlBQUosRUFBa0JDLFFBQWxCLEVBQTRCQyxTQUE1QixFQUF1Q3hYLEVBQXZDLEVBQTJDQyxJQUEzQzs7QUFDQSxRQUFJLENBQUM4QixhQUFMLEVBQW9CO0FBQ2xCLGFBQU8sSUFBUDtBQUNEOztBQUNEQSxpQkFBYSxHQUFHQSxhQUFhLENBQUMwVixLQUFkLENBQW9CLEdBQXBCLENBQWhCO0FBQ0FGLFlBQVEsR0FBR3BVLElBQUksQ0FBQytOLElBQWhCO0FBQ0FvRyxnQkFBWSxHQUFHQyxRQUFRLENBQUMxTixPQUFULENBQWlCLE9BQWpCLEVBQTBCLEVBQTFCLENBQWY7O0FBQ0EsU0FBSzdKLEVBQUUsR0FBRyxDQUFMLEVBQVFDLElBQUksR0FBRzhCLGFBQWEsQ0FBQy9ELE1BQWxDLEVBQTBDZ0MsRUFBRSxHQUFHQyxJQUEvQyxFQUFxREQsRUFBRSxFQUF2RCxFQUEyRDtBQUN6RHdYLGVBQVMsR0FBR3pWLGFBQWEsQ0FBQy9CLEVBQUQsQ0FBekI7QUFDQXdYLGVBQVMsR0FBR0EsU0FBUyxDQUFDL1EsSUFBVixFQUFaOztBQUNBLFVBQUkrUSxTQUFTLENBQUNYLE1BQVYsQ0FBaUIsQ0FBakIsTUFBd0IsR0FBNUIsRUFBaUM7QUFDL0IsWUFBSTFULElBQUksQ0FBQ3dELElBQUwsQ0FBVTRPLFdBQVYsR0FBd0JsSSxPQUF4QixDQUFnQ21LLFNBQVMsQ0FBQ2pDLFdBQVYsRUFBaEMsRUFBeURwUyxJQUFJLENBQUN3RCxJQUFMLENBQVUzSSxNQUFWLEdBQW1Cd1osU0FBUyxDQUFDeFosTUFBdEYsTUFBa0csQ0FBQyxDQUF2RyxFQUEwRztBQUN4RyxpQkFBTyxJQUFQO0FBQ0Q7QUFDRixPQUpELE1BSU8sSUFBSSxRQUFRNkYsSUFBUixDQUFhMlQsU0FBYixDQUFKLEVBQTZCO0FBQ2xDLFlBQUlGLFlBQVksS0FBS0UsU0FBUyxDQUFDM04sT0FBVixDQUFrQixPQUFsQixFQUEyQixFQUEzQixDQUFyQixFQUFxRDtBQUNuRCxpQkFBTyxJQUFQO0FBQ0Q7QUFDRixPQUpNLE1BSUE7QUFDTCxZQUFJME4sUUFBUSxLQUFLQyxTQUFqQixFQUE0QjtBQUMxQixpQkFBTyxJQUFQO0FBQ0Q7QUFDRjtBQUNGOztBQUNELFdBQU8sS0FBUDtBQUNELEdBMUJEOztBQTRCQSxNQUFJLE9BQU9oYyxNQUFQLEtBQWtCLFdBQWxCLElBQWlDQSxNQUFNLEtBQUssSUFBaEQsRUFBc0Q7QUFDcERBLFVBQU0sQ0FBQ2lFLEVBQVAsQ0FBVTRLLFFBQVYsR0FBcUIsVUFBU3BHLE9BQVQsRUFBa0I7QUFDckMsYUFBTyxLQUFLeVQsSUFBTCxDQUFVLFlBQVc7QUFDMUIsZUFBTyxJQUFJOWIsUUFBSixDQUFhLElBQWIsRUFBbUJxSSxPQUFuQixDQUFQO0FBQ0QsT0FGTSxDQUFQO0FBR0QsS0FKRDtBQUtEOztBQUVELE1BQUksU0FBaUMwVCxNQUFNLEtBQUssSUFBaEQsRUFBc0Q7QUFDcERBLFVBQU0sQ0FBQ0MsT0FBUCxHQUFpQmhjLFFBQWpCO0FBQ0QsR0FGRCxNQUVPO0FBQ0xILFVBQU0sQ0FBQ0csUUFBUCxHQUFrQkEsUUFBbEI7QUFDRDs7QUFFREEsVUFBUSxDQUFDMFAsS0FBVCxHQUFpQixPQUFqQjtBQUVBMVAsVUFBUSxDQUFDdVAsTUFBVCxHQUFrQixRQUFsQjtBQUVBdlAsVUFBUSxDQUFDaWMsUUFBVCxHQUFvQmpjLFFBQVEsQ0FBQ3VQLE1BQTdCO0FBRUF2UCxVQUFRLENBQUN3TCxTQUFULEdBQXFCLFdBQXJCO0FBRUF4TCxVQUFRLENBQUNrYyxVQUFULEdBQXNCbGMsUUFBUSxDQUFDd0wsU0FBL0I7QUFFQXhMLFVBQVEsQ0FBQ3VYLFFBQVQsR0FBb0IsVUFBcEI7QUFFQXZYLFVBQVEsQ0FBQ2dhLEtBQVQsR0FBaUIsT0FBakI7QUFFQWhhLFVBQVEsQ0FBQytaLE9BQVQsR0FBbUIsU0FBbkI7QUFHQTs7Ozs7OztBQU9BcFgsc0JBQW9CLEdBQUcsOEJBQVN5VCxHQUFULEVBQWM7QUFDbkMsUUFBSStGLEtBQUosRUFBVzlGLE1BQVgsRUFBbUJDLEdBQW5CLEVBQXdCM1UsSUFBeEIsRUFBOEJ5YSxFQUE5QixFQUFrQ0MsRUFBbEMsRUFBc0NDLEVBQXRDLEVBQTBDQyxFQUExQyxFQUE4Q0MsS0FBOUMsRUFBcURDLEVBQXJEO0FBQ0FILE1BQUUsR0FBR2xHLEdBQUcsQ0FBQ3NHLFlBQVQ7QUFDQUwsTUFBRSxHQUFHakcsR0FBRyxDQUFDdUcsYUFBVDtBQUNBdEcsVUFBTSxHQUFHaEksUUFBUSxDQUFDbkcsYUFBVCxDQUF1QixRQUF2QixDQUFUO0FBQ0FtTyxVQUFNLENBQUN0TixLQUFQLEdBQWUsQ0FBZjtBQUNBc04sVUFBTSxDQUFDL1QsTUFBUCxHQUFnQitaLEVBQWhCO0FBQ0EvRixPQUFHLEdBQUdELE1BQU0sQ0FBQ0ksVUFBUCxDQUFrQixJQUFsQixDQUFOO0FBQ0FILE9BQUcsQ0FBQ3NHLFNBQUosQ0FBY3hHLEdBQWQsRUFBbUIsQ0FBbkIsRUFBc0IsQ0FBdEI7QUFDQXpVLFFBQUksR0FBRzJVLEdBQUcsQ0FBQ3VHLFlBQUosQ0FBaUIsQ0FBakIsRUFBb0IsQ0FBcEIsRUFBdUIsQ0FBdkIsRUFBMEJSLEVBQTFCLEVBQThCMWEsSUFBckM7QUFDQThhLE1BQUUsR0FBRyxDQUFMO0FBQ0FMLE1BQUUsR0FBR0MsRUFBTDtBQUNBRSxNQUFFLEdBQUdGLEVBQUw7O0FBQ0EsV0FBT0UsRUFBRSxHQUFHRSxFQUFaLEVBQWdCO0FBQ2ROLFdBQUssR0FBR3hhLElBQUksQ0FBQyxDQUFDNGEsRUFBRSxHQUFHLENBQU4sSUFBVyxDQUFYLEdBQWUsQ0FBaEIsQ0FBWjs7QUFDQSxVQUFJSixLQUFLLEtBQUssQ0FBZCxFQUFpQjtBQUNmQyxVQUFFLEdBQUdHLEVBQUw7QUFDRCxPQUZELE1BRU87QUFDTEUsVUFBRSxHQUFHRixFQUFMO0FBQ0Q7O0FBQ0RBLFFBQUUsR0FBSUgsRUFBRSxHQUFHSyxFQUFOLElBQWEsQ0FBbEI7QUFDRDs7QUFDREQsU0FBSyxHQUFHRCxFQUFFLEdBQUdGLEVBQWI7O0FBQ0EsUUFBSUcsS0FBSyxLQUFLLENBQWQsRUFBaUI7QUFDZixhQUFPLENBQVA7QUFDRCxLQUZELE1BRU87QUFDTCxhQUFPQSxLQUFQO0FBQ0Q7QUFDRixHQTVCRDs7QUE4QkE1WixpQkFBZSxHQUFHLHlCQUFTMFQsR0FBVCxFQUFjRixHQUFkLEVBQW1CMEcsRUFBbkIsRUFBdUJMLEVBQXZCLEVBQTJCTSxFQUEzQixFQUErQkMsRUFBL0IsRUFBbUNDLEVBQW5DLEVBQXVDQyxFQUF2QyxFQUEyQ0MsRUFBM0MsRUFBK0NDLEVBQS9DLEVBQW1EO0FBQ25FLFFBQUlDLGVBQUo7QUFDQUEsbUJBQWUsR0FBRzFhLG9CQUFvQixDQUFDeVQsR0FBRCxDQUF0QztBQUNBLFdBQU9FLEdBQUcsQ0FBQ3NHLFNBQUosQ0FBY3hHLEdBQWQsRUFBbUIwRyxFQUFuQixFQUF1QkwsRUFBdkIsRUFBMkJNLEVBQTNCLEVBQStCQyxFQUEvQixFQUFtQ0MsRUFBbkMsRUFBdUNDLEVBQXZDLEVBQTJDQyxFQUEzQyxFQUErQ0MsRUFBRSxHQUFHQyxlQUFwRCxDQUFQO0FBQ0QsR0FKRDtBQU9BOzs7Ozs7Ozs7Ozs7Ozs7QUFjQTNhLGVBQWEsR0FBRyx1QkFBUzRhLEdBQVQsRUFBY3paLEVBQWQsRUFBa0I7QUFDaEMsUUFBSThGLEdBQUosRUFBUzRULEdBQVQsRUFBYy9WLElBQWQsRUFBb0IvRyxLQUFwQixFQUEwQitjLEtBQTFCLEVBQWdDQyxHQUFoQyxFQUFxQ0MsR0FBckMsRUFBMENDLElBQTFDLEVBQWdEdE4sR0FBaEQ7O0FBQ0E3SSxRQUFJLEdBQUcsS0FBUDtBQUNBNkksT0FBRyxHQUFHLElBQU47QUFDQWtOLE9BQUcsR0FBR0QsR0FBRyxDQUFDalAsUUFBVjtBQUNBc1AsUUFBSSxHQUFHSixHQUFHLENBQUNLLGVBQVg7QUFDQWpVLE9BQUcsR0FBSTRULEdBQUcsQ0FBQzVaLGdCQUFKLEdBQXVCLGtCQUF2QixHQUE0QyxhQUFuRDtBQUNBK1osT0FBRyxHQUFJSCxHQUFHLENBQUM1WixnQkFBSixHQUF1QixxQkFBdkIsR0FBK0MsYUFBdEQ7QUFDQThaLE9BQUcsR0FBSUYsR0FBRyxDQUFDNVosZ0JBQUosR0FBdUIsRUFBdkIsR0FBNEIsSUFBbkM7O0FBQ0FsRCxTQUFJLEdBQUcsY0FBU0ksQ0FBVCxFQUFZO0FBQ2pCLFVBQUlBLENBQUMsQ0FBQ3lVLElBQUYsS0FBVyxrQkFBWCxJQUFpQ2lJLEdBQUcsQ0FBQ3hFLFVBQUosS0FBbUIsVUFBeEQsRUFBb0U7QUFDbEU7QUFDRDs7QUFDRCxPQUFDbFksQ0FBQyxDQUFDeVUsSUFBRixLQUFXLE1BQVgsR0FBb0JnSSxHQUFwQixHQUEwQkMsR0FBM0IsRUFBZ0NHLEdBQWhDLEVBQXFDRCxHQUFHLEdBQUc1YyxDQUFDLENBQUN5VSxJQUE3QyxFQUFtRDdVLEtBQW5ELEVBQXlELEtBQXpEOztBQUNBLFVBQUksQ0FBQytHLElBQUQsS0FBVUEsSUFBSSxHQUFHLElBQWpCLENBQUosRUFBNEI7QUFDMUIsZUFBTzNELEVBQUUsQ0FBQ1AsSUFBSCxDQUFRZ2EsR0FBUixFQUFhemMsQ0FBQyxDQUFDeVUsSUFBRixJQUFVelUsQ0FBdkIsQ0FBUDtBQUNEO0FBQ0YsS0FSRDs7QUFTQTJjLFNBQUksR0FBRyxnQkFBVztBQUNoQixVQUFJM2MsQ0FBSjs7QUFDQSxVQUFJO0FBQ0Y4YyxZQUFJLENBQUNFLFFBQUwsQ0FBYyxNQUFkO0FBQ0QsT0FGRCxDQUVFLE9BQU85TSxNQUFQLEVBQWU7QUFDZmxRLFNBQUMsR0FBR2tRLE1BQUo7QUFDQTNFLGtCQUFVLENBQUNvUixLQUFELEVBQU8sRUFBUCxDQUFWO0FBQ0E7QUFDRDs7QUFDRCxhQUFPL2MsS0FBSSxDQUFDLE1BQUQsQ0FBWDtBQUNELEtBVkQ7O0FBV0EsUUFBSThjLEdBQUcsQ0FBQ3hFLFVBQUosS0FBbUIsVUFBdkIsRUFBbUM7QUFDakMsVUFBSXdFLEdBQUcsQ0FBQ08saUJBQUosSUFBeUJILElBQUksQ0FBQ0UsUUFBbEMsRUFBNEM7QUFDMUMsWUFBSTtBQUNGeE4sYUFBRyxHQUFHLENBQUNpTixHQUFHLENBQUNTLFlBQVg7QUFDRCxTQUZELENBRUUsT0FBT2hOLE1BQVAsRUFBZSxDQUFFOztBQUNuQixZQUFJVixHQUFKLEVBQVM7QUFDUG1OLGVBQUk7QUFDTDtBQUNGOztBQUNERCxTQUFHLENBQUM1VCxHQUFELENBQUgsQ0FBUzhULEdBQUcsR0FBRyxrQkFBZixFQUFtQ2hkLEtBQW5DLEVBQXlDLEtBQXpDO0FBQ0E4YyxTQUFHLENBQUM1VCxHQUFELENBQUgsQ0FBUzhULEdBQUcsR0FBRyxrQkFBZixFQUFtQ2hkLEtBQW5DLEVBQXlDLEtBQXpDO0FBQ0EsYUFBTzZjLEdBQUcsQ0FBQzNULEdBQUQsQ0FBSCxDQUFTOFQsR0FBRyxHQUFHLE1BQWYsRUFBdUJoZCxLQUF2QixFQUE2QixLQUE3QixDQUFQO0FBQ0Q7QUFDRixHQTFDRDs7QUE0Q0FULFVBQVEsQ0FBQ2dlLHFCQUFULEdBQWlDLFlBQVc7QUFDMUMsUUFBSWhlLFFBQVEsQ0FBQ2thLFlBQWIsRUFBMkI7QUFDekIsYUFBT2xhLFFBQVEsQ0FBQ21hLFFBQVQsRUFBUDtBQUNEO0FBQ0YsR0FKRDs7QUFNQXpYLGVBQWEsQ0FBQzdDLE1BQUQsRUFBU0csUUFBUSxDQUFDZ2UscUJBQWxCLENBQWI7QUFFRCxDQTdyREQsRUE2ckRHMWEsSUE3ckRILENBNnJEUSxJQTdyRFIsRTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzNCQTVDLG1DQUFPLFlBQVc7QUFDZCxNQUFNdWQsaUJBQWlCLEdBQUcsU0FBcEJBLGlCQUFvQixPQUE4SEMsU0FBOUg7QUFBQSxRQUFHQyxJQUFILFFBQUdBLElBQUg7QUFBQSxRQUFTQyxZQUFULFFBQVNBLFlBQVQ7QUFBQSxRQUF1QkMsWUFBdkIsUUFBdUJBLFlBQXZCO0FBQUEsUUFBcUNDLHdCQUFyQyxRQUFxQ0Esd0JBQXJDO0FBQUEsUUFBK0RDLFlBQS9ELFFBQStEQSxZQUEvRDtBQUFBLFFBQTZFQyxHQUE3RSxRQUE2RUEsR0FBN0U7QUFBQSxRQUFrRkMsS0FBbEYsUUFBa0ZBLEtBQWxGO0FBQUEsUUFBeUZDLFFBQXpGLFFBQXlGQSxRQUF6RjtBQUFBLFFBQW1HQyxPQUFuRyxRQUFtR0EsT0FBbkc7QUFBQSxRQUE0R3BULE1BQTVHLFFBQTRHQSxNQUE1RztBQUFBLFFBQW9IcVQsTUFBcEgsUUFBb0hBLE1BQXBIO0FBQUEsMkNBQ1R4ZSxHQUFHLENBQUNJLEtBQUosQ0FBVXFlLGdCQUFWLENBQTJCdFQsTUFBM0IsQ0FEUyx3QkFDcUNuTCxHQUFHLENBQUNJLEtBQUosQ0FBVXNlLHNCQUFWLENBQWlDdlQsTUFBakMsQ0FEckMscUVBSWhCaVQsR0FKZ0IseUZBS3NDRyxPQUFPLENBQUNSLElBTDlDLGdCQUt1RFEsT0FBTyxDQUFDSSxLQUwvRCxvQ0FLOEZKLE9BQU8sQ0FBQ0ssUUFMdEcsNEJBTXBCNWUsR0FBRyxDQUFDSSxLQUFKLENBQVV5ZSwyQkFBVixDQUFzQ1gsd0JBQXRDLENBTm9CLG1EQVFQRyxLQVJPLDZDQVNISixZQVRHLGdDQVVoQkQsWUFWZ0IsZ0NBV2hCRyxZQVhnQixzRUFhQyxJQUFJVyxJQUFJLENBQUNDLFlBQVQsQ0FBc0IsT0FBdEIsRUFBK0I7QUFBRXJTLFdBQUssRUFBRSxVQUFUO0FBQXFCc1MsY0FBUSxFQUFFO0FBQS9CLEtBQS9CLEVBQXVFQyxNQUF2RSxDQUE4RVQsTUFBOUUsQ0FiRCx1QkFhbUcsSUFBSU0sSUFBSSxDQUFDQyxZQUFULENBQXNCLE9BQXRCLEVBQStCO0FBQUVyUyxXQUFLLEVBQUUsVUFBVDtBQUFxQnNTLGNBQVEsRUFBRTtBQUEvQixLQUEvQixFQUF1RUMsTUFBdkUsQ0FBOEVYLFFBQTlFLENBYm5HLGlRQWdCWnRlLEdBQUcsQ0FBQ0ksS0FBSixDQUFVOGUsY0FBVixDQUF5Qi9ULE1BQXpCLEVBQWlDZ1UsWUFoQnJCLDZHQW1CUm5mLEdBQUcsQ0FBQ0ksS0FBSixDQUFVZ2YsbUJBQVYsQ0FBOEJ0QixTQUE5QixFQUF5Q0MsSUFBekMsQ0FuQlE7QUFBQSxHQUExQjs7QUEwQkEsTUFBTXNCLHdCQUF3QixHQUFHLFNBQTNCQSx3QkFBMkIsUUFBOEh2QixTQUE5SDtBQUFBLFFBQUdDLElBQUgsU0FBR0EsSUFBSDtBQUFBLFFBQVNDLFlBQVQsU0FBU0EsWUFBVDtBQUFBLFFBQXVCQyxZQUF2QixTQUF1QkEsWUFBdkI7QUFBQSxRQUFxQ0Msd0JBQXJDLFNBQXFDQSx3QkFBckM7QUFBQSxRQUErREMsWUFBL0QsU0FBK0RBLFlBQS9EO0FBQUEsUUFBNkVDLEdBQTdFLFNBQTZFQSxHQUE3RTtBQUFBLFFBQWtGQyxLQUFsRixTQUFrRkEsS0FBbEY7QUFBQSxRQUF5RkMsUUFBekYsU0FBeUZBLFFBQXpGO0FBQUEsUUFBbUdDLE9BQW5HLFNBQW1HQSxPQUFuRztBQUFBLFFBQTRHcFQsTUFBNUcsU0FBNEdBLE1BQTVHO0FBQUEsUUFBb0hxVCxNQUFwSCxTQUFvSEEsTUFBcEg7QUFBQSwyQ0FDaEJ4ZSxHQUFHLENBQUNJLEtBQUosQ0FBVXFlLGdCQUFWLENBQTJCdFQsTUFBM0IsQ0FEZ0Isd0JBQzhCbkwsR0FBRyxDQUFDSSxLQUFKLENBQVVzZSxzQkFBVixDQUFpQ3ZULE1BQWpDLENBRDlCLHFFQUl2QmlULEdBSnVCLHlGQUsrQkcsT0FBTyxDQUFDUixJQUx2QyxnQkFLZ0RRLE9BQU8sQ0FBQ0ksS0FMeEQsb0NBS3VGSixPQUFPLENBQUNLLFFBTC9GLDRCQU0zQjVlLEdBQUcsQ0FBQ0ksS0FBSixDQUFVeWUsMkJBQVYsQ0FBc0NYLHdCQUF0QyxDQU4yQix5Q0FPZEcsS0FQYyw2Q0FRVkosWUFSVSxnQ0FTdkJELFlBVHVCLGdDQVV2QkcsWUFWdUIsc0VBWU4sSUFBSVcsSUFBSSxDQUFDQyxZQUFULENBQXNCLE9BQXRCLEVBQStCO0FBQUVyUyxXQUFLLEVBQUUsVUFBVDtBQUFxQnNTLGNBQVEsRUFBRTtBQUEvQixLQUEvQixFQUF1RUMsTUFBdkUsQ0FBOEVULE1BQTlFLENBWk0sdUJBWTRGLElBQUlNLElBQUksQ0FBQ0MsWUFBVCxDQUFzQixPQUF0QixFQUErQjtBQUFFclMsV0FBSyxFQUFFLFVBQVQ7QUFBcUJzUyxjQUFRLEVBQUU7QUFBL0IsS0FBL0IsRUFBdUVDLE1BQXZFLENBQThFWCxRQUE5RSxDQVo1RiwwQ0FhdkJ0ZSxHQUFHLENBQUNJLEtBQUosQ0FBVThlLGNBQVYsQ0FBeUIvVCxNQUF6QixFQUFpQ2dVLFlBYlY7QUFBQSxHQUFqQzs7QUFrQkEsTUFBTUcscUJBQXFCLEdBQUcsU0FBeEJBLHFCQUF3QixDQUFDblUsTUFBRCxFQUFTMlMsU0FBVCxFQUFvQnlCLE9BQXBCO0FBQUEsNEdBQ2dEekIsU0FEaEQsZ0NBQzZFeUIsT0FEN0UsOEJBQ3NHcFUsTUFBTSxDQUFDcVUsSUFEN0csZ0JBQ3NIclUsTUFBTSxDQUFDZ1UsWUFEN0g7QUFBQSxHQUE5Qjs7QUFJQSxNQUFNTSwwQkFBMEIsR0FBRyxTQUE3QkEsMEJBQTZCLENBQUNDLEdBQUQ7QUFBQSx1Q0FDckJBLEdBQUcsQ0FBQy9VLElBRGlCLHNCQUNEK1UsR0FBRyxDQUFDM2QsS0FESDtBQUFBLEdBQW5DOztBQUlBLFNBQU87QUFFSDFCLFFBRkcsa0JBRUk7QUFFSCxXQUFLc2YsV0FBTDtBQUNBLFdBQUtDLFlBQUw7QUFDSCxLQU5FO0FBUUhmLCtCQVJHLHVDQVF5Qlgsd0JBUnpCLEVBUW1EO0FBQ2xELFVBQUdBLHdCQUF3QixDQUFDMkIsR0FBekIsSUFBZ0MsQ0FBbkMsRUFBc0M7QUFDbEMsZUFBTyxFQUFQO0FBQ0g7O0FBRUQsYUFBTyxZQUFZM0Isd0JBQXdCLENBQUNqVyxPQUF6QixDQUFpQzZYLEdBQWpDLENBQXFDLFVBQUFKLEdBQUcsRUFBSTtBQUMzRCxZQUFHQSxHQUFHLENBQUN4SyxJQUFKLElBQVksUUFBZixFQUF5QjtBQUNyQixpQkFBT3VLLDBCQUEwQixDQUFDQyxHQUFELENBQWpDO0FBQ0g7QUFDSixPQUprQixFQUloQkssSUFKZ0IsQ0FJWCxFQUpXLENBQVosR0FJTyxVQUpkO0FBS0gsS0FsQkU7QUFvQkhKLGVBcEJHLHlCQW9CVztBQUNWcGdCLE9BQUMsQ0FBQ3lnQixJQUFGLENBQU87QUFDSGpiLFdBQUcsRUFBRSx5QkFERjtBQUVIa2IsbUJBQVcsRUFBRSxrQkFGVjtBQUdIdEksZUFBTyxFQUFFO0FBQ0wsMkJBQWlCLFlBQVl1STtBQUR4QixTQUhOO0FBTUhsYixjQUFNLEVBQUUsS0FOTDtBQU9IOEgsZUFBTyxFQUFFLGlCQUFTNkksTUFBVCxFQUFpQjtBQUN0QjNWLGFBQUcsQ0FBQ0ksS0FBSixDQUFVK2YsWUFBVixHQUF5QnhLLE1BQU0sQ0FBQ3ZWLEtBQWhDO0FBQ0FKLGFBQUcsQ0FBQ0ksS0FBSixDQUFVZ2dCLFVBQVYsR0FBdUJ6SyxNQUFNLENBQUMzRixRQUE5QjtBQUNIO0FBVkUsT0FBUDtBQVlILEtBakNFO0FBbUNINFAsZ0JBbkNHLDBCQW1DWTtBQUNYcmdCLE9BQUMsQ0FBQywwQkFBRCxDQUFELENBQThCOEUsR0FBOUI7QUFDQTlFLE9BQUMsQ0FBQywwQkFBRCxDQUFELENBQThCaUIsRUFBOUIsQ0FBaUMsT0FBakMsRUFBMEMsWUFBVztBQUNqRCxZQUFJdWQsSUFBSSxHQUFHeGUsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0MsSUFBUixHQUFld2MsSUFBMUI7O0FBQ0EsWUFBR3hlLENBQUMsQ0FBQyxXQUFXd2UsSUFBWCxHQUFrQixPQUFuQixDQUFELENBQTZCL2IsTUFBN0IsR0FBc0MsQ0FBekMsRUFBNEM7QUFDeEN6QyxXQUFDLENBQUMsV0FBV3dlLElBQVosQ0FBRCxDQUFtQnNDLElBQW5CLENBQXdCLEVBQXhCO0FBQ0gsU0FGRCxNQUVLO0FBQ0RyZ0IsYUFBRyxDQUFDSSxLQUFKLENBQVVrZ0IsV0FBVixDQUFzQnZDLElBQXRCO0FBQ0g7QUFDSixPQVBEO0FBU0F4ZSxPQUFDLENBQUMsaUNBQUQsQ0FBRCxDQUFxQzhFLEdBQXJDO0FBQ0E5RSxPQUFDLENBQUMsaUNBQUQsQ0FBRCxDQUFxQ2lCLEVBQXJDLENBQXdDLE9BQXhDLEVBQWlELFlBQVc7QUFDeEQsWUFBSXVkLElBQUksR0FBR3hlLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdDLElBQVIsR0FBZXdjLElBQTFCOztBQUNBLFlBQUd4ZSxDQUFDLENBQUMsV0FBV3dlLElBQVgsR0FBa0IsT0FBbkIsQ0FBRCxDQUE2Qi9iLE1BQTdCLEdBQXNDLENBQXpDLEVBQTRDO0FBQ3hDekMsV0FBQyxDQUFDLFdBQVd3ZSxJQUFaLENBQUQsQ0FBbUJzQyxJQUFuQixDQUF3QixFQUF4QjtBQUNILFNBRkQsTUFFSztBQUNEcmdCLGFBQUcsQ0FBQ0ksS0FBSixDQUFVa2dCLFdBQVYsQ0FBc0J2QyxJQUF0QixFQUE0QixJQUE1QjtBQUNIO0FBQ0osT0FQRDtBQVVILEtBekRFO0FBMkRIdUMsZUEzREcsdUJBMkRTeEMsU0EzRFQsRUEyRG9DO0FBQUEsVUFBaEJ5QyxNQUFnQix1RUFBUCxLQUFPO0FBQ25DaGhCLE9BQUMsQ0FBQ3lnQixJQUFGLENBQU87QUFDSGpiLFdBQUcsRUFBRSw4QkFERjtBQUVIa2IsbUJBQVcsRUFBRSxrQkFGVjtBQUdIdEksZUFBTyxFQUFFO0FBQ0wsMkJBQWlCLFlBQVl1STtBQUR4QixTQUhOO0FBTUhsYixjQUFNLEVBQUUsTUFOTDtBQU9IekQsWUFBSSxFQUFFdVgsSUFBSSxDQUFDMEgsU0FBTCxDQUFlO0FBQ2pCekMsY0FBSSxFQUFFRDtBQURXLFNBQWYsQ0FQSDtBQVVIaFIsZUFBTyxFQUFFLGlCQUFTNkksTUFBVCxFQUFpQjtBQUN0QjNWLGFBQUcsQ0FBQ0ksS0FBSixDQUFVcWdCLGFBQVYsQ0FBd0IzQyxTQUF4QixFQUFtQ25JLE1BQW5DLEVBQTJDNEssTUFBM0M7QUFDSDtBQVpFLE9BQVA7QUFjSCxLQTFFRTtBQTRFSEUsaUJBNUVHLHlCQTRFVzNDLFNBNUVYLEVBNEVzQm5JLE1BNUV0QixFQTRFOEM7QUFBQSxVQUFoQjRLLE1BQWdCLHVFQUFQLEtBQU87O0FBQzdDLFVBQUdBLE1BQUgsRUFBVztBQUNQaGhCLFNBQUMsQ0FBQyxXQUFXb1csTUFBTSxDQUFDb0ksSUFBbkIsQ0FBRCxDQUEwQnNDLElBQTFCLENBQStCMUssTUFBTSxDQUFDK0ssU0FBUCxDQUFpQlosR0FBakIsQ0FBcUIsVUFBQWEsQ0FBQztBQUFBLGlCQUFJdEIsd0JBQXdCLENBQUNzQixDQUFELEVBQUk3QyxTQUFKLENBQTVCO0FBQUEsU0FBdEIsRUFBa0VpQyxJQUFsRSxDQUF1RSxFQUF2RSxDQUEvQjtBQUNILE9BRkQsTUFFSztBQUNEeGdCLFNBQUMsQ0FBQyxXQUFXb1csTUFBTSxDQUFDb0ksSUFBbkIsQ0FBRCxDQUEwQnNDLElBQTFCLENBQStCMUssTUFBTSxDQUFDK0ssU0FBUCxDQUFpQlosR0FBakIsQ0FBcUIsVUFBQWEsQ0FBQztBQUFBLGlCQUFJOUMsaUJBQWlCLENBQUM4QyxDQUFELEVBQUk3QyxTQUFKLENBQXJCO0FBQUEsU0FBdEIsRUFBMkRpQyxJQUEzRCxDQUFnRSxFQUFoRSxDQUEvQjtBQUNIOztBQUVELFdBQUthLG1CQUFMO0FBQ0gsS0FwRkU7QUFzRkhBLHVCQXRGRyxpQ0FzRm1CO0FBQ2xCcmhCLE9BQUMsQ0FBQyxhQUFELENBQUQsQ0FBaUI4RSxHQUFqQjtBQUNBOUUsT0FBQyxDQUFDLGFBQUQsQ0FBRCxDQUFpQmlCLEVBQWpCLENBQW9CLE9BQXBCLEVBQTZCLFlBQVc7QUFFcEMsWUFBSXNkLFNBQVMsR0FBR3ZlLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdDLElBQVIsR0FBZXVjLFNBQS9CO0FBQ0EsWUFBSXlCLE9BQU8sR0FBR2hnQixDQUFDLENBQUMsSUFBRCxDQUFELENBQVFnQyxJQUFSLEdBQWVnZSxPQUE3QjtBQUNBLFlBQUlwVSxNQUFNLEdBQUc1TCxDQUFDLENBQUMsSUFBRCxDQUFELENBQVFnQyxJQUFSLEdBQWU0SixNQUE1QjtBQUVBNUwsU0FBQyxDQUFDeWdCLElBQUYsQ0FBTztBQUNIamIsYUFBRyxFQUFFLGtDQURGO0FBRUhrYixxQkFBVyxFQUFFLGtCQUZWO0FBR0h0SSxpQkFBTyxFQUFFO0FBQ0wsNkJBQWlCLFlBQVl1STtBQUR4QixXQUhOO0FBTUgzZSxjQUFJLEVBQUV1WCxJQUFJLENBQUMwSCxTQUFMLENBQWU7QUFDakJ4USxvQkFBUSxFQUFFdVAsT0FETztBQUVqQnBVLGtCQUFNLEVBQUVBO0FBRlMsV0FBZixDQU5IO0FBVUhuRyxnQkFBTSxFQUFFLE1BVkw7QUFXSDhILGlCQUFPLEVBQUUsaUJBQVM2SSxNQUFULEVBQWlCO0FBQ3RCcFcsYUFBQyxDQUFDLFdBQVd1ZSxTQUFaLENBQUQsQ0FBd0J1QyxJQUF4QixDQUE2QixFQUE3QjtBQUNBcmdCLGVBQUcsQ0FBQ0ksS0FBSixDQUFVa2dCLFdBQVYsQ0FBc0J4QyxTQUF0QjtBQUNIO0FBZEUsU0FBUDtBQWdCSCxPQXRCRDtBQXVCSCxLQS9HRTtBQWlISFcsb0JBakhHLDRCQWlIY3RULE1BakhkLEVBaUhzQjtBQUVyQjs7O0FBR0EsVUFBTTBWLEtBQUssR0FBRzdnQixHQUFHLENBQUNJLEtBQUosQ0FBVWdnQixVQUFWLENBQXFCamYsSUFBckIsQ0FBMEIsVUFBQTJmLElBQUk7QUFBQSxlQUFJQSxJQUFJLENBQUN0QixJQUFMLElBQWFyVSxNQUFqQjtBQUFBLE9BQTlCLENBQWQ7O0FBRUEsVUFBRzBWLEtBQUssSUFBSUEsS0FBSyxDQUFDRSxLQUFOLElBQWUsRUFBM0IsRUFBK0I7QUFDM0IsZUFBTyx1QkFBdUJGLEtBQUssQ0FBQ0UsS0FBcEM7QUFDSDtBQUVKLEtBNUhFO0FBOEhIN0Isa0JBOUhHLDBCQThIWS9ULE1BOUhaLEVBOEhvQjtBQUVuQjs7O0FBR0EsVUFBTTBWLEtBQUssR0FBRzdnQixHQUFHLENBQUNJLEtBQUosQ0FBVWdnQixVQUFWLENBQXFCamYsSUFBckIsQ0FBMEIsVUFBQTJmLElBQUk7QUFBQSxlQUFJQSxJQUFJLENBQUN0QixJQUFMLElBQWFyVSxNQUFqQjtBQUFBLE9BQTlCLENBQWQ7O0FBQ0EsVUFBRzBWLEtBQUgsRUFBVTtBQUNOLGVBQU9BLEtBQVA7QUFDSDs7QUFFRCxhQUFPO0FBQUMxQixvQkFBWSxFQUFFO0FBQWYsT0FBUDtBQUVILEtBMUlFO0FBNElIVCwwQkE1SUcsa0NBNElvQnZULE1BNUlwQixFQTRJNEI7QUFFM0I7OztBQUlBLFVBQUdBLE1BQU0sSUFBSSxHQUFiLEVBQWtCO0FBQ2QsZUFBTyxjQUFQO0FBQ0g7O0FBQ0QsVUFBR0EsTUFBTSxJQUFJLEdBQVYsSUFBaUJBLE1BQU0sSUFBSSxHQUE5QixFQUFtQztBQUMvQixlQUFPLGlCQUFQO0FBQ0g7O0FBRUQsYUFBTyxnQkFBUDtBQUNILEtBMUpFO0FBNEpIaVUsdUJBNUpHLCtCQTRKaUJ0QixTQTVKakIsRUE0SjRCeUIsT0E1SjVCLEVBNEpxQztBQUNwQyxhQUFPdmYsR0FBRyxDQUFDSSxLQUFKLENBQVVnZ0IsVUFBVixDQUFxQk4sR0FBckIsQ0FBeUIsVUFBQWEsQ0FBQztBQUFBLGVBQUlyQixxQkFBcUIsQ0FBQ3FCLENBQUQsRUFBSTdDLFNBQUosRUFBZXlCLE9BQWYsQ0FBekI7QUFBQSxPQUExQixFQUE0RVEsSUFBNUUsQ0FBaUYsRUFBakYsQ0FBUDtBQUNIO0FBOUpFLEdBQVA7QUFpS0gsQ0F0Tks7QUFBQSxvR0FBTixDOzs7Ozs7Ozs7OztBQ0FDLDJHQUFVaUIsT0FBVixFQUFtQjtBQUNoQjtBQUNBLE1BQUksSUFBSixFQUFnRDtBQUM1QztBQUNBMWdCLHFDQUFPLENBQUMseUVBQUQsQ0FBRCxvQ0FBYTBnQixPQUFiO0FBQUE7QUFBQTtBQUFBLG9HQUFOO0FBQ0gsR0FIRCxNQUdPLEVBR047QUFDSixDQVRBLEVBU0MsVUFBVXpoQixDQUFWLEVBQVk7QUFDVkEsR0FBQyxDQUFDb0YsTUFBRixDQUFTcEYsQ0FBQyxDQUFDMEMsVUFBRixDQUFhZ2YsT0FBdEIsRUFBK0I7QUFDM0IsYUFBUyxlQUFTQyxPQUFULEVBQWtCO0FBQ3ZCLFVBQUlDLElBQUksR0FBRyxJQUFYLENBRHVCLENBR3ZCO0FBQ0E7O0FBQ0EsVUFBSUMsRUFBRSxHQUFHN2hCLENBQUMsQ0FBQzBDLFVBQUYsQ0FBYW1mLEVBQXRCLENBTHVCLENBT3ZCOztBQUNBRixhQUFPLENBQUNHLElBQVIsQ0FBYSxjQUFiLEVBQTZCLFlBQVc7QUFDcEM7QUFDQSxZQUFJQyxNQUFNLEdBQUdGLEVBQUUsQ0FBQ0UsTUFBSCxDQUFVO0FBQ25CQyxrQkFBUSxFQUFFLGlDQURTO0FBRW5CQyxpQkFBTyxFQUFFLE9BRlU7QUFHbkJwZ0IsZUFBSyxFQUFFLGlCQUFXO0FBQ2RxZ0IsOEJBQWtCLENBQUNQLE9BQU8sQ0FBQ1EsS0FBUixDQUFjOWdCLElBQWQsQ0FBbUIsSUFBbkIsQ0FBRCxDQUFsQjtBQUNIO0FBTGtCLFNBQVYsQ0FBYixDQUZvQyxDQVVwQzs7QUFDQSxZQUFJK2dCLE1BQU0sR0FBR0wsTUFBTSxDQUFDTSxNQUFQLEVBQWI7QUFDQSxlQUFPRCxNQUFQO0FBQ0gsT0FiRDtBQWNIO0FBdkIwQixHQUEvQjtBQTBCSCxDQXBDQSxDQUFELEM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQSxDQUFDLFVBQVNFLENBQVQsRUFBV3BoQixDQUFYLEVBQWE7QUFBQyxVQUFzQ0gsb0NBQU9HLENBQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxvR0FBNUMsR0FBZ0QsU0FBaEQ7QUFBK0gsQ0FBN0ksQ0FBOEksSUFBOUksRUFBbUosVUFBU29oQixDQUFULEVBQVdwaEIsQ0FBWCxFQUFhcWhCLENBQWIsRUFBZTtBQUFDOztBQUFhLFdBQVNqUSxDQUFULENBQVdnUSxDQUFYLEVBQWFwaEIsQ0FBYixFQUFlO0FBQUMsUUFBRyxFQUFFb2hCLENBQUMsWUFBWXBoQixDQUFmLENBQUgsRUFBcUIsTUFBTSxJQUFJc2hCLFNBQUosQ0FBYyxtQ0FBZCxDQUFOO0FBQXlEOztBQUFBLFdBQVN2ZCxDQUFULENBQVdxZCxDQUFYLEVBQWE7QUFBQyxRQUFJcGhCLENBQUMsR0FBQ29oQixDQUFDLENBQUNHLHFCQUFGLEVBQU47QUFBQSxRQUFnQ0YsQ0FBQyxHQUFDLEVBQWxDOztBQUFxQyxTQUFJLElBQUlqUSxDQUFSLElBQWFwUixDQUFiO0FBQWVxaEIsT0FBQyxDQUFDalEsQ0FBRCxDQUFELEdBQUtwUixDQUFDLENBQUNvUixDQUFELENBQU47QUFBZjs7QUFBeUIsUUFBR2dRLENBQUMsQ0FBQ0ksYUFBRixLQUFrQmhVLFFBQXJCLEVBQThCO0FBQUMsVUFBSWlVLENBQUMsR0FBQ0wsQ0FBQyxDQUFDSSxhQUFGLENBQWdCRSxXQUFoQixDQUE0QnhFLFlBQWxDOztBQUErQyxVQUFHdUUsQ0FBSCxFQUFLO0FBQUMsWUFBSUUsQ0FBQyxHQUFDNWQsQ0FBQyxDQUFDMGQsQ0FBRCxDQUFQO0FBQVdKLFNBQUMsQ0FBQzdSLEdBQUYsSUFBT21TLENBQUMsQ0FBQ25TLEdBQVQsRUFBYTZSLENBQUMsQ0FBQ08sTUFBRixJQUFVRCxDQUFDLENBQUNuUyxHQUF6QixFQUE2QjZSLENBQUMsQ0FBQ2xnQixJQUFGLElBQVF3Z0IsQ0FBQyxDQUFDeGdCLElBQXZDLEVBQTRDa2dCLENBQUMsQ0FBQ2pnQixLQUFGLElBQVN1Z0IsQ0FBQyxDQUFDeGdCLElBQXZEO0FBQTREO0FBQUM7O0FBQUEsV0FBT2tnQixDQUFQO0FBQVM7O0FBQUEsV0FBU0ksQ0FBVCxDQUFXTCxDQUFYLEVBQWE7QUFBQyxRQUFJcGhCLENBQUMsR0FBQzZoQixnQkFBZ0IsQ0FBQ1QsQ0FBRCxDQUFoQixJQUFxQixFQUEzQjtBQUFBLFFBQThCQyxDQUFDLEdBQUNyaEIsQ0FBQyxDQUFDdVAsUUFBbEM7QUFBQSxRQUEyQzZCLENBQUMsR0FBQyxFQUE3QztBQUFnRCxRQUFHLFlBQVVpUSxDQUFiLEVBQWUsT0FBTSxDQUFDRCxDQUFELENBQU47O0FBQVUsU0FBSSxJQUFJcmQsQ0FBQyxHQUFDcWQsQ0FBVixFQUFZLENBQUNyZCxDQUFDLEdBQUNBLENBQUMsQ0FBQ2dILFVBQUwsS0FBa0JoSCxDQUFsQixJQUFxQixNQUFJQSxDQUFDLENBQUMySixRQUF2QyxHQUFpRDtBQUFDLFVBQUkrVCxDQUFDLEdBQUMsS0FBSyxDQUFYOztBQUFhLFVBQUc7QUFBQ0EsU0FBQyxHQUFDSSxnQkFBZ0IsQ0FBQzlkLENBQUQsQ0FBbEI7QUFBc0IsT0FBMUIsQ0FBMEIsT0FBTTRkLENBQU4sRUFBUSxDQUFFOztBQUFBLFVBQUcsZUFBYSxPQUFPRixDQUFwQixJQUF1QixTQUFPQSxDQUFqQyxFQUFtQyxPQUFPclEsQ0FBQyxDQUFDbE8sSUFBRixDQUFPYSxDQUFQLEdBQVVxTixDQUFqQjtBQUFtQixVQUFJMFEsQ0FBQyxHQUFDTCxDQUFOO0FBQUEsVUFBUU0sQ0FBQyxHQUFDRCxDQUFDLENBQUNFLFFBQVo7QUFBQSxVQUFxQkMsQ0FBQyxHQUFDSCxDQUFDLENBQUNJLFNBQXpCO0FBQUEsVUFBbUNDLENBQUMsR0FBQ0wsQ0FBQyxDQUFDTSxTQUF2QztBQUFpRCxzQkFBZ0JoYixJQUFoQixDQUFxQjJhLENBQUMsR0FBQ0ksQ0FBRixHQUFJRixDQUF6QixNQUE4QixlQUFhWixDQUFiLElBQWdCLENBQUMsVUFBRCxFQUFZLFVBQVosRUFBdUIsT0FBdkIsRUFBZ0N6USxPQUFoQyxDQUF3QzZRLENBQUMsQ0FBQ2xTLFFBQTFDLEtBQXFELENBQW5HLEtBQXVHNkIsQ0FBQyxDQUFDbE8sSUFBRixDQUFPYSxDQUFQLENBQXZHO0FBQWlIOztBQUFBLFdBQU9xTixDQUFDLENBQUNsTyxJQUFGLENBQU9rZSxDQUFDLENBQUNJLGFBQUYsQ0FBZ0JhLElBQXZCLEdBQTZCakIsQ0FBQyxDQUFDSSxhQUFGLEtBQWtCaFUsUUFBbEIsSUFBNEI0RCxDQUFDLENBQUNsTyxJQUFGLENBQU9rZSxDQUFDLENBQUNJLGFBQUYsQ0FBZ0JFLFdBQXZCLENBQXpELEVBQTZGdFEsQ0FBcEc7QUFBc0c7O0FBQUEsV0FBU3VRLENBQVQsR0FBWTtBQUFDVyxLQUFDLElBQUU5VSxRQUFRLENBQUM2VSxJQUFULENBQWNyWCxXQUFkLENBQTBCc1gsQ0FBMUIsQ0FBSCxFQUFnQ0EsQ0FBQyxHQUFDLElBQWxDO0FBQXVDOztBQUFBLFdBQVNSLENBQVQsQ0FBV1YsQ0FBWCxFQUFhO0FBQUMsUUFBSXBoQixDQUFDLEdBQUMsS0FBSyxDQUFYO0FBQWFvaEIsS0FBQyxLQUFHNVQsUUFBSixJQUFjeE4sQ0FBQyxHQUFDd04sUUFBRixFQUFXNFQsQ0FBQyxHQUFDNVQsUUFBUSxDQUFDdVAsZUFBcEMsSUFBcUQvYyxDQUFDLEdBQUNvaEIsQ0FBQyxDQUFDSSxhQUF6RDtBQUF1RSxRQUFJSCxDQUFDLEdBQUNyaEIsQ0FBQyxDQUFDK2MsZUFBUjtBQUFBLFFBQXdCM0wsQ0FBQyxHQUFDck4sQ0FBQyxDQUFDcWQsQ0FBRCxDQUEzQjtBQUFBLFFBQStCSyxDQUFDLEdBQUNjLENBQUMsRUFBbEM7QUFBcUMsV0FBT25SLENBQUMsQ0FBQzVCLEdBQUYsSUFBT2lTLENBQUMsQ0FBQ2pTLEdBQVQsRUFBYTRCLENBQUMsQ0FBQ2pRLElBQUYsSUFBUXNnQixDQUFDLENBQUN0Z0IsSUFBdkIsRUFBNEIsZUFBYSxPQUFPaVEsQ0FBQyxDQUFDbEosS0FBdEIsS0FBOEJrSixDQUFDLENBQUNsSixLQUFGLEdBQVFzRixRQUFRLENBQUM2VSxJQUFULENBQWNHLFdBQWQsR0FBMEJwUixDQUFDLENBQUNqUSxJQUE1QixHQUFpQ2lRLENBQUMsQ0FBQ2hRLEtBQXpFLENBQTVCLEVBQTRHLGVBQWEsT0FBT2dRLENBQUMsQ0FBQzNQLE1BQXRCLEtBQStCMlAsQ0FBQyxDQUFDM1AsTUFBRixHQUFTK0wsUUFBUSxDQUFDNlUsSUFBVCxDQUFjSSxZQUFkLEdBQTJCclIsQ0FBQyxDQUFDNUIsR0FBN0IsR0FBaUM0QixDQUFDLENBQUN3USxNQUEzRSxDQUE1RyxFQUErTHhRLENBQUMsQ0FBQzVCLEdBQUYsR0FBTTRCLENBQUMsQ0FBQzVCLEdBQUYsR0FBTTZSLENBQUMsQ0FBQ3FCLFNBQTdNLEVBQXVOdFIsQ0FBQyxDQUFDalEsSUFBRixHQUFPaVEsQ0FBQyxDQUFDalEsSUFBRixHQUFPa2dCLENBQUMsQ0FBQ3NCLFVBQXZPLEVBQWtQdlIsQ0FBQyxDQUFDaFEsS0FBRixHQUFRcEIsQ0FBQyxDQUFDcWlCLElBQUYsQ0FBT08sV0FBUCxHQUFtQnhSLENBQUMsQ0FBQ2xKLEtBQXJCLEdBQTJCa0osQ0FBQyxDQUFDalEsSUFBdlIsRUFBNFJpUSxDQUFDLENBQUN3USxNQUFGLEdBQVM1aEIsQ0FBQyxDQUFDcWlCLElBQUYsQ0FBT1EsWUFBUCxHQUFvQnpSLENBQUMsQ0FBQzNQLE1BQXRCLEdBQTZCMlAsQ0FBQyxDQUFDNUIsR0FBcFUsRUFBd1U0QixDQUEvVTtBQUFpVjs7QUFBQSxXQUFTMlEsQ0FBVCxDQUFXWCxDQUFYLEVBQWE7QUFBQyxXQUFPQSxDQUFDLENBQUMwQixZQUFGLElBQWdCdFYsUUFBUSxDQUFDdVAsZUFBaEM7QUFBZ0Q7O0FBQUEsV0FBU2tGLENBQVQsR0FBWTtBQUFDLFFBQUliLENBQUMsR0FBQzVULFFBQVEsQ0FBQ25HLGFBQVQsQ0FBdUIsS0FBdkIsQ0FBTjtBQUFvQytaLEtBQUMsQ0FBQ25WLEtBQUYsQ0FBUS9ELEtBQVIsR0FBYyxNQUFkLEVBQXFCa1osQ0FBQyxDQUFDblYsS0FBRixDQUFReEssTUFBUixHQUFlLE9BQXBDO0FBQTRDLFFBQUl6QixDQUFDLEdBQUN3TixRQUFRLENBQUNuRyxhQUFULENBQXVCLEtBQXZCLENBQU47QUFBb0M4YSxLQUFDLENBQUNuaUIsQ0FBQyxDQUFDaU0sS0FBSCxFQUFTO0FBQUNzRCxjQUFRLEVBQUMsVUFBVjtBQUFxQkMsU0FBRyxFQUFDLENBQXpCO0FBQTJCck8sVUFBSSxFQUFDLENBQWhDO0FBQWtDNGhCLG1CQUFhLEVBQUMsTUFBaEQ7QUFBdUR6VCxnQkFBVSxFQUFDLFFBQWxFO0FBQTJFcEgsV0FBSyxFQUFDLE9BQWpGO0FBQXlGekcsWUFBTSxFQUFDLE9BQWhHO0FBQXdHdWdCLGNBQVEsRUFBQztBQUFqSCxLQUFULENBQUQsRUFBc0loaUIsQ0FBQyxDQUFDc0gsV0FBRixDQUFjOFosQ0FBZCxDQUF0SSxFQUF1SjVULFFBQVEsQ0FBQzZVLElBQVQsQ0FBYy9hLFdBQWQsQ0FBMEJ0SCxDQUExQixDQUF2SjtBQUFvTCxRQUFJcWhCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDNEIsV0FBUjtBQUFvQmhqQixLQUFDLENBQUNpTSxLQUFGLENBQVErVixRQUFSLEdBQWlCLFFBQWpCO0FBQTBCLFFBQUk1USxDQUFDLEdBQUNnUSxDQUFDLENBQUM0QixXQUFSO0FBQW9CM0IsS0FBQyxLQUFHalEsQ0FBSixLQUFRQSxDQUFDLEdBQUNwUixDQUFDLENBQUM0aUIsV0FBWixHQUF5QnBWLFFBQVEsQ0FBQzZVLElBQVQsQ0FBY3JYLFdBQWQsQ0FBMEJoTCxDQUExQixDQUF6QjtBQUFzRCxRQUFJK0QsQ0FBQyxHQUFDc2QsQ0FBQyxHQUFDalEsQ0FBUjtBQUFVLFdBQU07QUFBQ2xKLFdBQUssRUFBQ25FLENBQVA7QUFBU3RDLFlBQU0sRUFBQ3NDO0FBQWhCLEtBQU47QUFBeUI7O0FBQUEsV0FBU29lLENBQVQsR0FBWTtBQUFDLFFBQUlmLENBQUMsR0FBQzNkLFNBQVMsQ0FBQ2xDLE1BQVYsSUFBa0IsQ0FBbEIsSUFBcUIsS0FBSyxDQUFMLEtBQVNrQyxTQUFTLENBQUMsQ0FBRCxDQUF2QyxHQUEyQyxFQUEzQyxHQUE4Q0EsU0FBUyxDQUFDLENBQUQsQ0FBN0Q7QUFBQSxRQUFpRXpELENBQUMsR0FBQyxFQUFuRTtBQUFzRSxXQUFPMGEsS0FBSyxDQUFDOVgsU0FBTixDQUFnQk0sSUFBaEIsQ0FBcUJRLEtBQXJCLENBQTJCMUQsQ0FBM0IsRUFBNkJ5RCxTQUE3QixHQUF3Q3pELENBQUMsQ0FBQ21DLEtBQUYsQ0FBUSxDQUFSLEVBQVdpTyxPQUFYLENBQW1CLFVBQVNwUSxDQUFULEVBQVc7QUFBQyxVQUFHQSxDQUFILEVBQUssS0FBSSxJQUFJcWhCLENBQVIsSUFBYXJoQixDQUFiO0FBQWUsU0FBQyxFQUFELEVBQUtxQyxjQUFMLENBQW9CSSxJQUFwQixDQUF5QnpDLENBQXpCLEVBQTJCcWhCLENBQTNCLE1BQWdDRCxDQUFDLENBQUNDLENBQUQsQ0FBRCxHQUFLcmhCLENBQUMsQ0FBQ3FoQixDQUFELENBQXRDO0FBQWY7QUFBMEQsS0FBOUYsQ0FBeEMsRUFBd0lELENBQS9JO0FBQWlKOztBQUFBLFdBQVM2QixDQUFULENBQVc3QixDQUFYLEVBQWFwaEIsQ0FBYixFQUFlO0FBQUMsUUFBRyxlQUFhLE9BQU9vaEIsQ0FBQyxDQUFDM1ksU0FBekIsRUFBbUN6SSxDQUFDLENBQUNnYixLQUFGLENBQVEsR0FBUixFQUFhNUssT0FBYixDQUFxQixVQUFTcFEsQ0FBVCxFQUFXO0FBQUNBLE9BQUMsQ0FBQ2dLLElBQUYsTUFBVW9YLENBQUMsQ0FBQzNZLFNBQUYsQ0FBWUMsTUFBWixDQUFtQjFJLENBQW5CLENBQVY7QUFBZ0MsS0FBakUsRUFBbkMsS0FBMEc7QUFBQyxVQUFJcWhCLENBQUMsR0FBQyxJQUFJNkIsTUFBSixDQUFXLFVBQVFsakIsQ0FBQyxDQUFDZ2IsS0FBRixDQUFRLEdBQVIsRUFBYXNFLElBQWIsQ0FBa0IsR0FBbEIsQ0FBUixHQUErQixPQUExQyxFQUFrRCxJQUFsRCxDQUFOO0FBQUEsVUFBOERsTyxDQUFDLEdBQUMrUixDQUFDLENBQUMvQixDQUFELENBQUQsQ0FBS2hVLE9BQUwsQ0FBYWlVLENBQWIsRUFBZSxHQUFmLENBQWhFO0FBQW9GK0IsT0FBQyxDQUFDaEMsQ0FBRCxFQUFHaFEsQ0FBSCxDQUFEO0FBQU87QUFBQzs7QUFBQSxXQUFTaVMsQ0FBVCxDQUFXakMsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZTtBQUFDLFFBQUcsZUFBYSxPQUFPb2hCLENBQUMsQ0FBQzNZLFNBQXpCLEVBQW1DekksQ0FBQyxDQUFDZ2IsS0FBRixDQUFRLEdBQVIsRUFBYTVLLE9BQWIsQ0FBcUIsVUFBU3BRLENBQVQsRUFBVztBQUFDQSxPQUFDLENBQUNnSyxJQUFGLE1BQVVvWCxDQUFDLENBQUMzWSxTQUFGLENBQVlLLEdBQVosQ0FBZ0I5SSxDQUFoQixDQUFWO0FBQTZCLEtBQTlELEVBQW5DLEtBQXVHO0FBQUNpakIsT0FBQyxDQUFDN0IsQ0FBRCxFQUFHcGhCLENBQUgsQ0FBRDtBQUFPLFVBQUlxaEIsQ0FBQyxHQUFDOEIsQ0FBQyxDQUFDL0IsQ0FBRCxDQUFELElBQU0sTUFBSXBoQixDQUFWLENBQU47QUFBbUJvakIsT0FBQyxDQUFDaEMsQ0FBRCxFQUFHQyxDQUFILENBQUQ7QUFBTztBQUFDOztBQUFBLFdBQVNpQyxDQUFULENBQVdsQyxDQUFYLEVBQWFwaEIsQ0FBYixFQUFlO0FBQUMsUUFBRyxlQUFhLE9BQU9vaEIsQ0FBQyxDQUFDM1ksU0FBekIsRUFBbUMsT0FBTzJZLENBQUMsQ0FBQzNZLFNBQUYsQ0FBWTJHLFFBQVosQ0FBcUJwUCxDQUFyQixDQUFQO0FBQStCLFFBQUlxaEIsQ0FBQyxHQUFDOEIsQ0FBQyxDQUFDL0IsQ0FBRCxDQUFQO0FBQVcsV0FBTyxJQUFJOEIsTUFBSixDQUFXLFVBQVFsakIsQ0FBUixHQUFVLE9BQXJCLEVBQTZCLElBQTdCLEVBQW1Db0gsSUFBbkMsQ0FBd0NpYSxDQUF4QyxDQUFQO0FBQWtEOztBQUFBLFdBQVM4QixDQUFULENBQVcvQixDQUFYLEVBQWE7QUFBQyxXQUFPQSxDQUFDLENBQUNsYSxTQUFGLFlBQXVCa2EsQ0FBQyxDQUFDSSxhQUFGLENBQWdCRSxXQUFoQixDQUE0QjZCLGlCQUFuRCxHQUFxRW5DLENBQUMsQ0FBQ2xhLFNBQUYsQ0FBWXNjLE9BQWpGLEdBQXlGcEMsQ0FBQyxDQUFDbGEsU0FBbEc7QUFBNEc7O0FBQUEsV0FBU2tjLENBQVQsQ0FBV2hDLENBQVgsRUFBYXBoQixDQUFiLEVBQWU7QUFBQ29oQixLQUFDLENBQUNqUyxZQUFGLENBQWUsT0FBZixFQUF1Qm5QLENBQXZCO0FBQTBCOztBQUFBLFdBQVN5akIsQ0FBVCxDQUFXckMsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZXFoQixDQUFmLEVBQWlCO0FBQUNBLEtBQUMsQ0FBQ2pSLE9BQUYsQ0FBVSxVQUFTaVIsQ0FBVCxFQUFXO0FBQUMsT0FBQyxDQUFELEtBQUtyaEIsQ0FBQyxDQUFDNFEsT0FBRixDQUFVeVEsQ0FBVixDQUFMLElBQW1CaUMsQ0FBQyxDQUFDbEMsQ0FBRCxFQUFHQyxDQUFILENBQXBCLElBQTJCNEIsQ0FBQyxDQUFDN0IsQ0FBRCxFQUFHQyxDQUFILENBQTVCO0FBQWtDLEtBQXhELEdBQTBEcmhCLENBQUMsQ0FBQ29RLE9BQUYsQ0FBVSxVQUFTcFEsQ0FBVCxFQUFXO0FBQUNzakIsT0FBQyxDQUFDbEMsQ0FBRCxFQUFHcGhCLENBQUgsQ0FBRCxJQUFRcWpCLENBQUMsQ0FBQ2pDLENBQUQsRUFBR3BoQixDQUFILENBQVQ7QUFBZSxLQUFyQyxDQUExRDtBQUFpRzs7QUFBQSxXQUFTb1IsQ0FBVCxDQUFXZ1EsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZTtBQUFDLFFBQUcsRUFBRW9oQixDQUFDLFlBQVlwaEIsQ0FBZixDQUFILEVBQXFCLE1BQU0sSUFBSXNoQixTQUFKLENBQWMsbUNBQWQsQ0FBTjtBQUF5RDs7QUFBQSxXQUFTb0MsQ0FBVCxDQUFXdEMsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZTtBQUFDLFFBQUcsY0FBWSxPQUFPQSxDQUFuQixJQUFzQixTQUFPQSxDQUFoQyxFQUFrQyxNQUFNLElBQUlzaEIsU0FBSixDQUFjLHFFQUFrRXRoQixDQUFsRSxDQUFkLENBQU47QUFBeUZvaEIsS0FBQyxDQUFDeGUsU0FBRixHQUFZK2dCLE1BQU0sQ0FBQ0MsTUFBUCxDQUFjNWpCLENBQUMsSUFBRUEsQ0FBQyxDQUFDNEMsU0FBbkIsRUFBNkI7QUFBQ0QsaUJBQVcsRUFBQztBQUFDckIsYUFBSyxFQUFDOGYsQ0FBUDtBQUFTeUMsa0JBQVUsRUFBQyxDQUFDLENBQXJCO0FBQXVCQyxnQkFBUSxFQUFDLENBQUMsQ0FBakM7QUFBbUNDLG9CQUFZLEVBQUMsQ0FBQztBQUFqRDtBQUFiLEtBQTdCLENBQVosRUFBNEcvakIsQ0FBQyxLQUFHMmpCLE1BQU0sQ0FBQ0ssY0FBUCxHQUFzQkwsTUFBTSxDQUFDSyxjQUFQLENBQXNCNUMsQ0FBdEIsRUFBd0JwaEIsQ0FBeEIsQ0FBdEIsR0FBaURvaEIsQ0FBQyxDQUFDNkMsU0FBRixHQUFZamtCLENBQWhFLENBQTdHO0FBQWdMOztBQUFBLFdBQVNra0IsQ0FBVCxDQUFXOUMsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZTtBQUFDLFFBQUlxaEIsQ0FBQyxHQUFDNWQsU0FBUyxDQUFDbEMsTUFBVixJQUFrQixDQUFsQixJQUFxQixLQUFLLENBQUwsS0FBU2tDLFNBQVMsQ0FBQyxDQUFELENBQXZDLEdBQTJDLENBQTNDLEdBQTZDQSxTQUFTLENBQUMsQ0FBRCxDQUE1RDtBQUFnRSxXQUFPMmQsQ0FBQyxHQUFDQyxDQUFGLElBQUtyaEIsQ0FBTCxJQUFRQSxDQUFDLElBQUVvaEIsQ0FBQyxHQUFDQyxDQUFwQjtBQUFzQjs7QUFBQSxXQUFTOEMsQ0FBVCxHQUFZO0FBQUMsV0FBTSxlQUFhLE9BQU9DLFdBQXBCLElBQWlDLGVBQWEsT0FBT0EsV0FBVyxDQUFDQyxHQUFqRSxHQUFxRUQsV0FBVyxDQUFDQyxHQUFaLEVBQXJFLEdBQXVGLENBQUMsSUFBSUMsSUFBSixFQUE5RjtBQUF1Rzs7QUFBQSxXQUFTQyxDQUFULEdBQVk7QUFBQyxTQUFJLElBQUluRCxDQUFDLEdBQUM7QUFBQzVSLFNBQUcsRUFBQyxDQUFMO0FBQU9yTyxVQUFJLEVBQUM7QUFBWixLQUFOLEVBQXFCbkIsQ0FBQyxHQUFDeUQsU0FBUyxDQUFDbEMsTUFBakMsRUFBd0M4ZixDQUFDLEdBQUMzRyxLQUFLLENBQUMxYSxDQUFELENBQS9DLEVBQW1Eb1IsQ0FBQyxHQUFDLENBQXpELEVBQTJEcFIsQ0FBQyxHQUFDb1IsQ0FBN0QsRUFBK0RBLENBQUMsRUFBaEU7QUFBbUVpUSxPQUFDLENBQUNqUSxDQUFELENBQUQsR0FBSzNOLFNBQVMsQ0FBQzJOLENBQUQsQ0FBZDtBQUFuRTs7QUFBcUYsV0FBT2lRLENBQUMsQ0FBQ2pSLE9BQUYsQ0FBVSxVQUFTcFEsQ0FBVCxFQUFXO0FBQUMsVUFBSXFoQixDQUFDLEdBQUNyaEIsQ0FBQyxDQUFDd1AsR0FBUjtBQUFBLFVBQVk0QixDQUFDLEdBQUNwUixDQUFDLENBQUNtQixJQUFoQjtBQUFxQixrQkFBVSxPQUFPa2dCLENBQWpCLEtBQXFCQSxDQUFDLEdBQUNtRCxVQUFVLENBQUNuRCxDQUFELEVBQUcsRUFBSCxDQUFqQyxHQUF5QyxZQUFVLE9BQU9qUSxDQUFqQixLQUFxQkEsQ0FBQyxHQUFDb1QsVUFBVSxDQUFDcFQsQ0FBRCxFQUFHLEVBQUgsQ0FBakMsQ0FBekMsRUFBa0ZnUSxDQUFDLENBQUM1UixHQUFGLElBQU82UixDQUF6RixFQUEyRkQsQ0FBQyxDQUFDamdCLElBQUYsSUFBUWlRLENBQW5HO0FBQXFHLEtBQWhKLEdBQWtKZ1EsQ0FBeko7QUFBMko7O0FBQUEsV0FBU3FELENBQVQsQ0FBV3JELENBQVgsRUFBYXBoQixDQUFiLEVBQWU7QUFBQyxXQUFNLFlBQVUsT0FBT29oQixDQUFDLENBQUNqZ0IsSUFBbkIsSUFBeUIsQ0FBQyxDQUFELEtBQUtpZ0IsQ0FBQyxDQUFDamdCLElBQUYsQ0FBT3lQLE9BQVAsQ0FBZSxHQUFmLENBQTlCLEtBQW9Ed1EsQ0FBQyxDQUFDamdCLElBQUYsR0FBT3FqQixVQUFVLENBQUNwRCxDQUFDLENBQUNqZ0IsSUFBSCxFQUFRLEVBQVIsQ0FBVixHQUFzQixHQUF0QixHQUEwQm5CLENBQUMsQ0FBQ2tJLEtBQXZGLEdBQThGLFlBQVUsT0FBT2taLENBQUMsQ0FBQzVSLEdBQW5CLElBQXdCLENBQUMsQ0FBRCxLQUFLNFIsQ0FBQyxDQUFDNVIsR0FBRixDQUFNb0IsT0FBTixDQUFjLEdBQWQsQ0FBN0IsS0FBa0R3USxDQUFDLENBQUM1UixHQUFGLEdBQU1nVixVQUFVLENBQUNwRCxDQUFDLENBQUM1UixHQUFILEVBQU8sRUFBUCxDQUFWLEdBQXFCLEdBQXJCLEdBQXlCeFAsQ0FBQyxDQUFDeUIsTUFBbkYsQ0FBOUYsRUFBeUwyZixDQUEvTDtBQUFpTTs7QUFBQSxXQUFTc0QsQ0FBVCxDQUFXdEQsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZTtBQUFDLFdBQU0sbUJBQWlCQSxDQUFqQixHQUFtQkEsQ0FBQyxHQUFDb2hCLENBQUMsQ0FBQ3VELGFBQUYsQ0FBZ0IsQ0FBaEIsQ0FBckIsR0FBd0MsYUFBVzNrQixDQUFYLEtBQWVBLENBQUMsR0FBQyxDQUFDNGtCLFdBQUQsRUFBYUMsV0FBYixFQUF5QkMsVUFBVSxHQUFDRixXQUFwQyxFQUFnREcsV0FBVyxHQUFDRixXQUE1RCxDQUFqQixDQUF4QyxFQUFtSTdrQixDQUFDLEtBQUd3TixRQUFKLEtBQWV4TixDQUFDLEdBQUNBLENBQUMsQ0FBQytjLGVBQW5CLENBQW5JLEVBQXVLLGVBQWEsT0FBTy9jLENBQUMsQ0FBQzBOLFFBQXRCLElBQWdDLENBQUMsWUFBVTtBQUFDLFVBQUkwVCxDQUFDLEdBQUNwaEIsQ0FBTjtBQUFBLFVBQVFxaEIsQ0FBQyxHQUFDUyxDQUFDLENBQUM5aEIsQ0FBRCxDQUFYO0FBQUEsVUFBZW9SLENBQUMsR0FBQ2lRLENBQWpCO0FBQUEsVUFBbUJ0ZCxDQUFDLEdBQUM4ZCxnQkFBZ0IsQ0FBQzdoQixDQUFELENBQXJDOztBQUF5QyxVQUFHQSxDQUFDLEdBQUMsQ0FBQ29SLENBQUMsQ0FBQ2pRLElBQUgsRUFBUWlRLENBQUMsQ0FBQzVCLEdBQVYsRUFBYzZSLENBQUMsQ0FBQ25aLEtBQUYsR0FBUWtKLENBQUMsQ0FBQ2pRLElBQXhCLEVBQTZCa2dCLENBQUMsQ0FBQzVmLE1BQUYsR0FBUzJQLENBQUMsQ0FBQzVCLEdBQXhDLENBQUYsRUFBK0M0UixDQUFDLENBQUNJLGFBQUYsS0FBa0JoVSxRQUFwRSxFQUE2RTtBQUFDLFlBQUlpVSxDQUFDLEdBQUNMLENBQUMsQ0FBQ0ksYUFBRixDQUFnQkUsV0FBdEI7QUFBa0MxaEIsU0FBQyxDQUFDLENBQUQsQ0FBRCxJQUFNeWhCLENBQUMsQ0FBQ21ELFdBQVIsRUFBb0I1a0IsQ0FBQyxDQUFDLENBQUQsQ0FBRCxJQUFNeWhCLENBQUMsQ0FBQ29ELFdBQTVCLEVBQXdDN2tCLENBQUMsQ0FBQyxDQUFELENBQUQsSUFBTXloQixDQUFDLENBQUNtRCxXQUFoRCxFQUE0RDVrQixDQUFDLENBQUMsQ0FBRCxDQUFELElBQU15aEIsQ0FBQyxDQUFDb0QsV0FBcEU7QUFBZ0Y7O0FBQUEvbEIsT0FBQyxDQUFDc1IsT0FBRixDQUFVLFVBQVNnUixDQUFULEVBQVdDLENBQVgsRUFBYTtBQUFDRCxTQUFDLEdBQUNBLENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBS25ULFdBQUwsS0FBbUJtVCxDQUFDLENBQUM0RCxNQUFGLENBQVMsQ0FBVCxDQUFyQixFQUFpQyxVQUFRNUQsQ0FBUixJQUFXLFdBQVNBLENBQXBCLEdBQXNCcGhCLENBQUMsQ0FBQ3FoQixDQUFELENBQUQsSUFBTW1ELFVBQVUsQ0FBQ3pnQixDQUFDLENBQUMsV0FBU3FkLENBQVQsR0FBVyxPQUFaLENBQUYsQ0FBdEMsR0FBOERwaEIsQ0FBQyxDQUFDcWhCLENBQUQsQ0FBRCxJQUFNbUQsVUFBVSxDQUFDemdCLENBQUMsQ0FBQyxXQUFTcWQsQ0FBVCxHQUFXLE9BQVosQ0FBRixDQUEvRztBQUF1SSxPQUEvSjtBQUFpSyxLQUFyWixFQUF4TSxFQUFnbUJwaEIsQ0FBdG1CO0FBQXdtQjs7QUFBQSxNQUFJaWxCLENBQUMsR0FBQyxZQUFVO0FBQUMsYUFBUzdELENBQVQsQ0FBV0EsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZTtBQUFDLFdBQUksSUFBSXFoQixDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUNyaEIsQ0FBQyxDQUFDdUIsTUFBaEIsRUFBdUI4ZixDQUFDLEVBQXhCLEVBQTJCO0FBQUMsWUFBSWpRLENBQUMsR0FBQ3BSLENBQUMsQ0FBQ3FoQixDQUFELENBQVA7QUFBV2pRLFNBQUMsQ0FBQ3lTLFVBQUYsR0FBYXpTLENBQUMsQ0FBQ3lTLFVBQUYsSUFBYyxDQUFDLENBQTVCLEVBQThCelMsQ0FBQyxDQUFDMlMsWUFBRixHQUFlLENBQUMsQ0FBOUMsRUFBZ0QsV0FBVTNTLENBQVYsS0FBY0EsQ0FBQyxDQUFDMFMsUUFBRixHQUFXLENBQUMsQ0FBMUIsQ0FBaEQsRUFBNkVILE1BQU0sQ0FBQ3VCLGNBQVAsQ0FBc0I5RCxDQUF0QixFQUF3QmhRLENBQUMsQ0FBQzVPLEdBQTFCLEVBQThCNE8sQ0FBOUIsQ0FBN0U7QUFBOEc7QUFBQzs7QUFBQSxXQUFPLFVBQVNwUixDQUFULEVBQVdxaEIsQ0FBWCxFQUFhalEsQ0FBYixFQUFlO0FBQUMsYUFBT2lRLENBQUMsSUFBRUQsQ0FBQyxDQUFDcGhCLENBQUMsQ0FBQzRDLFNBQUgsRUFBYXllLENBQWIsQ0FBSixFQUFvQmpRLENBQUMsSUFBRWdRLENBQUMsQ0FBQ3BoQixDQUFELEVBQUdvUixDQUFILENBQXhCLEVBQThCcFIsQ0FBckM7QUFBdUMsS0FBOUQ7QUFBK0QsR0FBaFAsRUFBTjtBQUFBLE1BQXlQa2dCLENBQUMsR0FBQyxLQUFLLENBQWhROztBQUFrUSxpQkFBYSxPQUFPQSxDQUFwQixLQUF3QkEsQ0FBQyxHQUFDO0FBQUNpRixXQUFPLEVBQUM7QUFBVCxHQUExQjs7QUFBd0MsTUFBSTdDLENBQUMsR0FBQyxJQUFOO0FBQUEsTUFBVzhDLENBQUMsR0FBQyxZQUFVO0FBQUMsUUFBSWhFLENBQUMsR0FBQyxDQUFOO0FBQVEsV0FBTyxZQUFVO0FBQUMsYUFBTSxFQUFFQSxDQUFSO0FBQVUsS0FBNUI7QUFBNkIsR0FBaEQsRUFBYjtBQUFBLE1BQWdFaUUsQ0FBQyxHQUFDLEVBQWxFO0FBQUEsTUFBcUU5QyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxHQUFVO0FBQUMsUUFBSW5CLENBQUMsR0FBQ2tCLENBQU47QUFBUWxCLEtBQUMsS0FBR0EsQ0FBQyxHQUFDNVQsUUFBUSxDQUFDbkcsYUFBVCxDQUF1QixLQUF2QixDQUFGLEVBQWdDK1osQ0FBQyxDQUFDalMsWUFBRixDQUFlLGdCQUFmLEVBQWdDaVcsQ0FBQyxFQUFqQyxDQUFoQyxFQUFxRWpELENBQUMsQ0FBQ2YsQ0FBQyxDQUFDblYsS0FBSCxFQUFTO0FBQUN1RCxTQUFHLEVBQUMsQ0FBTDtBQUFPck8sVUFBSSxFQUFDLENBQVo7QUFBY29PLGNBQVEsRUFBQztBQUF2QixLQUFULENBQXRFLEVBQW1IL0IsUUFBUSxDQUFDNlUsSUFBVCxDQUFjL2EsV0FBZCxDQUEwQjhaLENBQTFCLENBQW5ILEVBQWdKa0IsQ0FBQyxHQUFDbEIsQ0FBckosQ0FBRDtBQUF5SixRQUFJcGhCLENBQUMsR0FBQ29oQixDQUFDLENBQUNwVCxZQUFGLENBQWUsZ0JBQWYsQ0FBTjtBQUF1QyxXQUFNLGVBQWEsT0FBT3FYLENBQUMsQ0FBQ3JsQixDQUFELENBQXJCLEtBQTJCcWxCLENBQUMsQ0FBQ3JsQixDQUFELENBQUQsR0FBSytELENBQUMsQ0FBQ3FkLENBQUQsQ0FBTixFQUFVa0UsQ0FBQyxDQUFDLFlBQVU7QUFBQyxhQUFPRCxDQUFDLENBQUNybEIsQ0FBRCxDQUFSO0FBQVksS0FBeEIsQ0FBdEMsR0FBaUVxbEIsQ0FBQyxDQUFDcmxCLENBQUQsQ0FBeEU7QUFBNEUsR0FBdFc7QUFBQSxNQUF1V3VsQixDQUFDLEdBQUMsRUFBelc7QUFBQSxNQUE0V0QsQ0FBQyxHQUFDLFdBQVNsRSxDQUFULEVBQVc7QUFBQ21FLEtBQUMsQ0FBQ3JpQixJQUFGLENBQU9rZSxDQUFQO0FBQVUsR0FBcFk7QUFBQSxNQUFxWW9FLENBQUMsR0FBQyxhQUFVO0FBQUMsU0FBSSxJQUFJcEUsQ0FBQyxHQUFDLEtBQUssQ0FBZixFQUFpQkEsQ0FBQyxHQUFDbUUsQ0FBQyxDQUFDRSxHQUFGLEVBQW5CO0FBQTRCckUsT0FBQztBQUE3QjtBQUFnQyxHQUFsYjtBQUFBLE1BQW1ic0UsQ0FBQyxHQUFDLFlBQVU7QUFBQyxhQUFTdEUsQ0FBVCxHQUFZO0FBQUNoUSxPQUFDLENBQUMsSUFBRCxFQUFNZ1EsQ0FBTixDQUFEO0FBQVU7O0FBQUEsV0FBTzZELENBQUMsQ0FBQzdELENBQUQsRUFBRyxDQUFDO0FBQUM1ZSxTQUFHLEVBQUMsSUFBTDtBQUFVbEIsV0FBSyxFQUFDLGVBQVM4ZixDQUFULEVBQVdwaEIsQ0FBWCxFQUFhcWhCLENBQWIsRUFBZTtBQUFDLFlBQUlqUSxDQUFDLEdBQUMzTixTQUFTLENBQUNsQyxNQUFWLElBQWtCLENBQWxCLElBQXFCLEtBQUssQ0FBTCxLQUFTa0MsU0FBUyxDQUFDLENBQUQsQ0FBdkMsR0FBMkMsQ0FBQyxDQUE1QyxHQUE4Q0EsU0FBUyxDQUFDLENBQUQsQ0FBN0Q7QUFBaUUsdUJBQWEsT0FBTyxLQUFLa2lCLFFBQXpCLEtBQW9DLEtBQUtBLFFBQUwsR0FBYyxFQUFsRCxHQUFzRCxlQUFhLE9BQU8sS0FBS0EsUUFBTCxDQUFjdkUsQ0FBZCxDQUFwQixLQUF1QyxLQUFLdUUsUUFBTCxDQUFjdkUsQ0FBZCxJQUFpQixFQUF4RCxDQUF0RCxFQUFrSCxLQUFLdUUsUUFBTCxDQUFjdkUsQ0FBZCxFQUFpQmxlLElBQWpCLENBQXNCO0FBQUMwaUIsaUJBQU8sRUFBQzVsQixDQUFUO0FBQVd5VixhQUFHLEVBQUM0TCxDQUFmO0FBQWlCd0UsY0FBSSxFQUFDelU7QUFBdEIsU0FBdEIsQ0FBbEg7QUFBa0s7QUFBblEsS0FBRCxFQUFzUTtBQUFDNU8sU0FBRyxFQUFDLE1BQUw7QUFBWWxCLFdBQUssRUFBQyxlQUFTOGYsQ0FBVCxFQUFXcGhCLENBQVgsRUFBYXFoQixDQUFiLEVBQWU7QUFBQyxhQUFLdGhCLEVBQUwsQ0FBUXFoQixDQUFSLEVBQVVwaEIsQ0FBVixFQUFZcWhCLENBQVosRUFBYyxDQUFDLENBQWY7QUFBa0I7QUFBcEQsS0FBdFEsRUFBNFQ7QUFBQzdlLFNBQUcsRUFBQyxLQUFMO0FBQVdsQixXQUFLLEVBQUMsZUFBUzhmLENBQVQsRUFBV3BoQixDQUFYLEVBQWE7QUFBQyxZQUFHLGVBQWEsT0FBTyxLQUFLMmxCLFFBQXpCLElBQW1DLGVBQWEsT0FBTyxLQUFLQSxRQUFMLENBQWN2RSxDQUFkLENBQTFELEVBQTJFLElBQUcsZUFBYSxPQUFPcGhCLENBQXZCLEVBQXlCLE9BQU8sS0FBSzJsQixRQUFMLENBQWN2RSxDQUFkLENBQVAsQ0FBekIsS0FBc0QsS0FBSSxJQUFJQyxDQUFDLEdBQUMsQ0FBVixFQUFZQSxDQUFDLEdBQUMsS0FBS3NFLFFBQUwsQ0FBY3ZFLENBQWQsRUFBaUI3ZixNQUEvQjtBQUF1QyxlQUFLb2tCLFFBQUwsQ0FBY3ZFLENBQWQsRUFBaUJDLENBQWpCLEVBQW9CdUUsT0FBcEIsS0FBOEI1bEIsQ0FBOUIsR0FBZ0MsS0FBSzJsQixRQUFMLENBQWN2RSxDQUFkLEVBQWlCcGQsTUFBakIsQ0FBd0JxZCxDQUF4QixFQUEwQixDQUExQixDQUFoQyxHQUE2RCxFQUFFQSxDQUEvRDtBQUF2QztBQUF3RztBQUF4USxLQUE1VCxFQUFza0I7QUFBQzdlLFNBQUcsRUFBQyxTQUFMO0FBQWVsQixXQUFLLEVBQUMsZUFBUzhmLENBQVQsRUFBVztBQUFDLFlBQUcsZUFBYSxPQUFPLEtBQUt1RSxRQUF6QixJQUFtQyxLQUFLQSxRQUFMLENBQWN2RSxDQUFkLENBQXRDLEVBQXVEO0FBQUMsZUFBSSxJQUFJcGhCLENBQUMsR0FBQyxDQUFOLEVBQVFxaEIsQ0FBQyxHQUFDNWQsU0FBUyxDQUFDbEMsTUFBcEIsRUFBMkI2UCxDQUFDLEdBQUNzSixLQUFLLENBQUMyRyxDQUFDLEdBQUMsQ0FBRixHQUFJQSxDQUFDLEdBQUMsQ0FBTixHQUFRLENBQVQsQ0FBbEMsRUFBOEN0ZCxDQUFDLEdBQUMsQ0FBcEQsRUFBc0RzZCxDQUFDLEdBQUN0ZCxDQUF4RCxFQUEwREEsQ0FBQyxFQUEzRDtBQUE4RHFOLGFBQUMsQ0FBQ3JOLENBQUMsR0FBQyxDQUFILENBQUQsR0FBT04sU0FBUyxDQUFDTSxDQUFELENBQWhCO0FBQTlEOztBQUFrRixpQkFBSy9ELENBQUMsR0FBQyxLQUFLMmxCLFFBQUwsQ0FBY3ZFLENBQWQsRUFBaUI3ZixNQUF4QixHQUFnQztBQUFDLGdCQUFJa2dCLENBQUMsR0FBQyxLQUFLa0UsUUFBTCxDQUFjdkUsQ0FBZCxFQUFpQnBoQixDQUFqQixDQUFOO0FBQUEsZ0JBQTBCMmhCLENBQUMsR0FBQ0YsQ0FBQyxDQUFDbUUsT0FBOUI7QUFBQSxnQkFBc0M5RCxDQUFDLEdBQUNMLENBQUMsQ0FBQ2hNLEdBQTFDO0FBQUEsZ0JBQThDc00sQ0FBQyxHQUFDTixDQUFDLENBQUNvRSxJQUFsRDtBQUFBLGdCQUF1RDVELENBQUMsR0FBQ0gsQ0FBekQ7QUFBMkQsMkJBQWEsT0FBT0csQ0FBcEIsS0FBd0JBLENBQUMsR0FBQyxJQUExQixHQUFnQ04sQ0FBQyxDQUFDamUsS0FBRixDQUFRdWUsQ0FBUixFQUFVN1EsQ0FBVixDQUFoQyxFQUE2QzJRLENBQUMsR0FBQyxLQUFLNEQsUUFBTCxDQUFjdkUsQ0FBZCxFQUFpQnBkLE1BQWpCLENBQXdCaEUsQ0FBeEIsRUFBMEIsQ0FBMUIsQ0FBRCxHQUE4QixFQUFFQSxDQUE5RTtBQUFnRjtBQUFDO0FBQUM7QUFBelYsS0FBdGtCLENBQUgsQ0FBRCxFQUF1NkJvaEIsQ0FBOTZCO0FBQWc3QixHQUFsOUIsRUFBcmI7O0FBQTA0Q2xCLEdBQUMsQ0FBQzRGLEtBQUYsR0FBUTtBQUFDQywrQkFBMkIsRUFBQ2hpQixDQUE3QjtBQUErQmlpQixvQkFBZ0IsRUFBQ3ZFLENBQWhEO0FBQWtEd0UsYUFBUyxFQUFDbkUsQ0FBNUQ7QUFBOERvRSxtQkFBZSxFQUFDbkUsQ0FBOUU7QUFBZ0Y3ZCxVQUFNLEVBQUNpZSxDQUF2RjtBQUF5RmdFLFlBQVEsRUFBQzlDLENBQWxHO0FBQW9HK0MsZUFBVyxFQUFDbkQsQ0FBaEg7QUFBa0hvRCxZQUFRLEVBQUMvQyxDQUEzSDtBQUE2SGdELGlCQUFhLEVBQUM3QyxDQUEzSTtBQUE2SThDLFNBQUssRUFBQ2pCLENBQW5KO0FBQXFKa0IsU0FBSyxFQUFDaEIsQ0FBM0o7QUFBNkppQixZQUFRLEVBQUNyQixDQUF0SztBQUF3S3NCLFdBQU8sRUFBQ2hCLENBQWhMO0FBQWtMaUIsb0JBQWdCLEVBQUMxRSxDQUFuTTtBQUFxTTJFLHNCQUFrQixFQUFDakY7QUFBeE4sR0FBUjs7QUFBbU8sTUFBSWtGLENBQUMsR0FBQyxZQUFVO0FBQUMsYUFBU3pGLENBQVQsQ0FBV0EsQ0FBWCxFQUFhcGhCLENBQWIsRUFBZTtBQUFDLFVBQUlxaEIsQ0FBQyxHQUFDLEVBQU47QUFBQSxVQUFTalEsQ0FBQyxHQUFDLENBQUMsQ0FBWjtBQUFBLFVBQWNyTixDQUFDLEdBQUMsQ0FBQyxDQUFqQjtBQUFBLFVBQW1CMGQsQ0FBQyxHQUFDLEtBQUssQ0FBMUI7O0FBQTRCLFVBQUc7QUFBQyxhQUFJLElBQUlFLENBQUosRUFBTUcsQ0FBQyxHQUFDVixDQUFDLENBQUMwRixNQUFNLENBQUNDLFFBQVIsQ0FBRCxFQUFaLEVBQWlDLEVBQUUzVixDQUFDLEdBQUMsQ0FBQ3VRLENBQUMsR0FBQ0csQ0FBQyxDQUFDa0YsSUFBRixFQUFILEVBQWFyZ0IsSUFBakIsTUFBeUIwYSxDQUFDLENBQUNuZSxJQUFGLENBQU95ZSxDQUFDLENBQUNyZ0IsS0FBVCxHQUFnQixDQUFDdEIsQ0FBRCxJQUFJcWhCLENBQUMsQ0FBQzlmLE1BQUYsS0FBV3ZCLENBQXhELENBQWpDLEVBQTRGb1IsQ0FBQyxHQUFDLENBQUMsQ0FBL0Y7QUFBaUc7QUFBakc7QUFBbUcsT0FBdkcsQ0FBdUcsT0FBTTJRLENBQU4sRUFBUTtBQUFDaGUsU0FBQyxHQUFDLENBQUMsQ0FBSCxFQUFLMGQsQ0FBQyxHQUFDTSxDQUFQO0FBQVMsT0FBekgsU0FBZ0k7QUFBQyxZQUFHO0FBQUMsV0FBQzNRLENBQUQsSUFBSTBRLENBQUMsQ0FBQyxRQUFELENBQUwsSUFBaUJBLENBQUMsQ0FBQyxRQUFELENBQUQsRUFBakI7QUFBK0IsU0FBbkMsU0FBMEM7QUFBQyxjQUFHL2QsQ0FBSCxFQUFLLE1BQU0wZCxDQUFOO0FBQVE7QUFBQzs7QUFBQSxhQUFPSixDQUFQO0FBQVM7O0FBQUEsV0FBTyxVQUFTcmhCLENBQVQsRUFBV3FoQixDQUFYLEVBQWE7QUFBQyxVQUFHM0csS0FBSyxDQUFDdU0sT0FBTixDQUFjam5CLENBQWQsQ0FBSCxFQUFvQixPQUFPQSxDQUFQO0FBQVMsVUFBRzhtQixNQUFNLENBQUNDLFFBQVAsSUFBbUJwRCxNQUFNLENBQUMzakIsQ0FBRCxDQUE1QixFQUFnQyxPQUFPb2hCLENBQUMsQ0FBQ3BoQixDQUFELEVBQUdxaEIsQ0FBSCxDQUFSO0FBQWMsWUFBTSxJQUFJQyxTQUFKLENBQWMsc0RBQWQsQ0FBTjtBQUE0RSxLQUE1SztBQUE2SyxHQUF2YSxFQUFOO0FBQUEsTUFBZ2IyRCxDQUFDLEdBQUMsWUFBVTtBQUFDLGFBQVM3RCxDQUFULENBQVdBLENBQVgsRUFBYXBoQixDQUFiLEVBQWU7QUFBQyxXQUFJLElBQUlxaEIsQ0FBQyxHQUFDLENBQVYsRUFBWUEsQ0FBQyxHQUFDcmhCLENBQUMsQ0FBQ3VCLE1BQWhCLEVBQXVCOGYsQ0FBQyxFQUF4QixFQUEyQjtBQUFDLFlBQUlqUSxDQUFDLEdBQUNwUixDQUFDLENBQUNxaEIsQ0FBRCxDQUFQO0FBQVdqUSxTQUFDLENBQUN5UyxVQUFGLEdBQWF6UyxDQUFDLENBQUN5UyxVQUFGLElBQWMsQ0FBQyxDQUE1QixFQUE4QnpTLENBQUMsQ0FBQzJTLFlBQUYsR0FBZSxDQUFDLENBQTlDLEVBQWdELFdBQVUzUyxDQUFWLEtBQWNBLENBQUMsQ0FBQzBTLFFBQUYsR0FBVyxDQUFDLENBQTFCLENBQWhELEVBQTZFSCxNQUFNLENBQUN1QixjQUFQLENBQXNCOUQsQ0FBdEIsRUFBd0JoUSxDQUFDLENBQUM1TyxHQUExQixFQUE4QjRPLENBQTlCLENBQTdFO0FBQThHO0FBQUM7O0FBQUEsV0FBTyxVQUFTcFIsQ0FBVCxFQUFXcWhCLENBQVgsRUFBYWpRLENBQWIsRUFBZTtBQUFDLGFBQU9pUSxDQUFDLElBQUVELENBQUMsQ0FBQ3BoQixDQUFDLENBQUM0QyxTQUFILEVBQWF5ZSxDQUFiLENBQUosRUFBb0JqUSxDQUFDLElBQUVnUSxDQUFDLENBQUNwaEIsQ0FBRCxFQUFHb1IsQ0FBSCxDQUF4QixFQUE4QnBSLENBQXJDO0FBQXVDLEtBQTlEO0FBQStELEdBQWhQLEVBQWxiO0FBQUEsTUFBcXFCa25CLENBQUMsR0FBQyxTQUFGQSxDQUFFLENBQVM5RixDQUFULEVBQVdwaEIsQ0FBWCxFQUFhcWhCLENBQWIsRUFBZTtBQUFDLFNBQUksSUFBSWpRLENBQUMsR0FBQyxDQUFDLENBQVgsRUFBYUEsQ0FBYixHQUFnQjtBQUFDLFVBQUlyTixDQUFDLEdBQUNxZCxDQUFOO0FBQUEsVUFBUUssQ0FBQyxHQUFDemhCLENBQVY7QUFBQSxVQUFZMmhCLENBQUMsR0FBQ04sQ0FBZDtBQUFnQmpRLE9BQUMsR0FBQyxDQUFDLENBQUgsRUFBSyxTQUFPck4sQ0FBUCxLQUFXQSxDQUFDLEdBQUNvakIsUUFBUSxDQUFDdmtCLFNBQXRCLENBQUw7QUFBc0MsVUFBSWtmLENBQUMsR0FBQzZCLE1BQU0sQ0FBQ3lELHdCQUFQLENBQWdDcmpCLENBQWhDLEVBQWtDMGQsQ0FBbEMsQ0FBTjs7QUFBMkMsVUFBRyxLQUFLLENBQUwsS0FBU0ssQ0FBWixFQUFjO0FBQUMsWUFBRyxXQUFVQSxDQUFiLEVBQWUsT0FBT0EsQ0FBQyxDQUFDeGdCLEtBQVQ7QUFBZSxZQUFJeWdCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDdUYsR0FBUjtBQUFZLFlBQUcsS0FBSyxDQUFMLEtBQVN0RixDQUFaLEVBQWM7QUFBTyxlQUFPQSxDQUFDLENBQUN0ZixJQUFGLENBQU9rZixDQUFQLENBQVA7QUFBaUI7O0FBQUEsVUFBSU0sQ0FBQyxHQUFDMEIsTUFBTSxDQUFDMkQsY0FBUCxDQUFzQnZqQixDQUF0QixDQUFOO0FBQStCLFVBQUcsU0FBT2tlLENBQVYsRUFBWTtBQUFPYixPQUFDLEdBQUNhLENBQUYsRUFBSWppQixDQUFDLEdBQUN5aEIsQ0FBTixFQUFRSixDQUFDLEdBQUNNLENBQVYsRUFBWXZRLENBQUMsR0FBQyxDQUFDLENBQWYsRUFBaUIwUSxDQUFDLEdBQUNHLENBQUMsR0FBQyxLQUFLLENBQTFCO0FBQTRCO0FBQUMsR0FBdjlCOztBQUF3OUIsTUFBRyxlQUFhLE9BQU8vQixDQUF2QixFQUF5QixNQUFNLElBQUl2UyxLQUFKLENBQVUscURBQVYsQ0FBTjs7QUFBdUUsTUFBSTRaLENBQUMsR0FBQ3JILENBQUMsQ0FBQzRGLEtBQVI7QUFBQSxNQUFjckUsQ0FBQyxHQUFDOEYsQ0FBQyxDQUFDdkIsZ0JBQWxCO0FBQUEsTUFBbUNsRSxDQUFDLEdBQUN5RixDQUFDLENBQUN0QixTQUF2QztBQUFBLE1BQWlEbEUsQ0FBQyxHQUFDd0YsQ0FBQyxDQUFDckIsZUFBckQ7QUFBQSxNQUFxRS9ELENBQUMsR0FBQ29GLENBQUMsQ0FBQ3JqQixNQUF6RTtBQUFBLE1BQWdGbWYsQ0FBQyxHQUFDa0UsQ0FBQyxDQUFDcEIsUUFBcEY7QUFBQSxNQUE2RmxELENBQUMsR0FBQ3NFLENBQUMsQ0FBQ25CLFdBQWpHO0FBQUEsTUFBNkczQyxDQUFDLEdBQUM4RCxDQUFDLENBQUNqQixhQUFqSDtBQUFBLE1BQStIaEIsQ0FBQyxHQUFDaUMsQ0FBQyxDQUFDaEIsS0FBbkk7QUFBQSxNQUF5SWYsQ0FBQyxHQUFDK0IsQ0FBQyxDQUFDZixLQUE3STtBQUFBLE1BQW1KdkUsQ0FBQyxHQUFDc0YsQ0FBQyxDQUFDWixnQkFBdko7QUFBQSxNQUF3S2hGLENBQUMsR0FBQzRGLENBQUMsQ0FBQ1gsa0JBQTVLO0FBQUEsTUFBK0xZLENBQUMsR0FBQyxZQUFVO0FBQUMsUUFBRyxlQUFhLE9BQU9oYSxRQUF2QixFQUFnQyxPQUFNLEVBQU47O0FBQVMsU0FBSSxJQUFJNFQsQ0FBQyxHQUFDNVQsUUFBUSxDQUFDbkcsYUFBVCxDQUF1QixLQUF2QixDQUFOLEVBQW9DckgsQ0FBQyxHQUFDLENBQUMsV0FBRCxFQUFhLGlCQUFiLEVBQStCLFlBQS9CLEVBQTRDLGNBQTVDLEVBQTJELGFBQTNELENBQXRDLEVBQWdIcWhCLENBQUMsR0FBQyxDQUF0SCxFQUF3SEEsQ0FBQyxHQUFDcmhCLENBQUMsQ0FBQ3VCLE1BQTVILEVBQW1JLEVBQUU4ZixDQUFySSxFQUF1STtBQUFDLFVBQUlqUSxDQUFDLEdBQUNwUixDQUFDLENBQUNxaEIsQ0FBRCxDQUFQO0FBQVcsVUFBRyxLQUFLLENBQUwsS0FBU0QsQ0FBQyxDQUFDblYsS0FBRixDQUFRbUYsQ0FBUixDQUFaLEVBQXVCLE9BQU9BLENBQVA7QUFBUztBQUFDLEdBQXhPLEVBQWpNO0FBQUEsTUFBNGFxVyxDQUFDLEdBQUMsRUFBOWE7QUFBQSxNQUFpYkMsQ0FBQyxHQUFDLFNBQUZBLENBQUUsR0FBVTtBQUFDRCxLQUFDLENBQUNyWCxPQUFGLENBQVUsVUFBU2dSLENBQVQsRUFBVztBQUFDQSxPQUFDLENBQUM3UixRQUFGLENBQVcsQ0FBQyxDQUFaO0FBQWUsS0FBckMsR0FBdUNpVyxDQUFDLEVBQXhDO0FBQTJDLEdBQXplOztBQUEwZSxHQUFDLFlBQVU7QUFBQyxRQUFJcEUsQ0FBQyxHQUFDLElBQU47QUFBQSxRQUFXcGhCLENBQUMsR0FBQyxJQUFiO0FBQUEsUUFBa0JxaEIsQ0FBQyxHQUFDLElBQXBCO0FBQUEsUUFBeUJqUSxDQUFDLEdBQUMsU0FBU3JOLENBQVQsR0FBWTtBQUFDLGFBQU0sZUFBYSxPQUFPL0QsQ0FBcEIsSUFBdUJBLENBQUMsR0FBQyxFQUF6QixJQUE2QkEsQ0FBQyxHQUFDdVMsSUFBSSxDQUFDb1YsR0FBTCxDQUFTM25CLENBQUMsR0FBQyxFQUFYLEVBQWMsR0FBZCxDQUFGLEVBQXFCLE1BQUtxaEIsQ0FBQyxHQUFDOVYsVUFBVSxDQUFDeEgsQ0FBRCxFQUFHLEdBQUgsQ0FBakIsQ0FBbEQsSUFBNkUsTUFBSyxlQUFhLE9BQU9xZCxDQUFwQixJQUF1QitDLENBQUMsS0FBRy9DLENBQUosR0FBTSxFQUE3QixLQUFrQyxRQUFNQyxDQUFOLEtBQVV1RyxZQUFZLENBQUN2RyxDQUFELENBQVosRUFBZ0JBLENBQUMsR0FBQyxJQUE1QixHQUFrQ0QsQ0FBQyxHQUFDK0MsQ0FBQyxFQUFyQyxFQUF3Q3VELENBQUMsRUFBekMsRUFBNEMxbkIsQ0FBQyxHQUFDbWtCLENBQUMsS0FBRy9DLENBQXBGLENBQUwsQ0FBbkY7QUFBZ0wsS0FBeE47O0FBQXlOLG1CQUFhLE9BQU9waUIsTUFBcEIsSUFBNEIsZUFBYSxPQUFPQSxNQUFNLENBQUM4RCxnQkFBdkQsSUFBeUUsQ0FBQyxRQUFELEVBQVUsUUFBVixFQUFtQixXQUFuQixFQUFnQ3NOLE9BQWhDLENBQXdDLFVBQVNnUixDQUFULEVBQVc7QUFBQ3BpQixZQUFNLENBQUM4RCxnQkFBUCxDQUF3QnNlLENBQXhCLEVBQTBCaFEsQ0FBMUI7QUFBNkIsS0FBakYsQ0FBekU7QUFBNEosR0FBaFksRUFBRDs7QUFBb1ksTUFBSXlXLENBQUMsR0FBQztBQUFDQyxVQUFNLEVBQUMsUUFBUjtBQUFpQjNtQixRQUFJLEVBQUMsT0FBdEI7QUFBOEJDLFNBQUssRUFBQztBQUFwQyxHQUFOO0FBQUEsTUFBa0QybUIsQ0FBQyxHQUFDO0FBQUNDLFVBQU0sRUFBQyxRQUFSO0FBQWlCeFksT0FBRyxFQUFDLFFBQXJCO0FBQThCb1MsVUFBTSxFQUFDO0FBQXJDLEdBQXBEO0FBQUEsTUFBZ0dxRyxDQUFDLEdBQUM7QUFBQ3pZLE9BQUcsRUFBQyxDQUFMO0FBQU9yTyxRQUFJLEVBQUMsQ0FBWjtBQUFjNm1CLFVBQU0sRUFBQyxLQUFyQjtBQUEyQkYsVUFBTSxFQUFDLEtBQWxDO0FBQXdDbEcsVUFBTSxFQUFDLE1BQS9DO0FBQXNEeGdCLFNBQUssRUFBQztBQUE1RCxHQUFsRztBQUFBLE1BQXNLOG1CLENBQUMsR0FBQyxTQUFGQSxDQUFFLENBQVM5RyxDQUFULEVBQVdwaEIsQ0FBWCxFQUFhO0FBQUMsUUFBSXFoQixDQUFDLEdBQUNELENBQUMsQ0FBQ2pnQixJQUFSO0FBQUEsUUFBYWlRLENBQUMsR0FBQ2dRLENBQUMsQ0FBQzVSLEdBQWpCO0FBQXFCLFdBQU0sV0FBUzZSLENBQVQsS0FBYUEsQ0FBQyxHQUFDd0csQ0FBQyxDQUFDN25CLENBQUMsQ0FBQ21CLElBQUgsQ0FBaEIsR0FBMEIsV0FBU2lRLENBQVQsS0FBYUEsQ0FBQyxHQUFDMlcsQ0FBQyxDQUFDL25CLENBQUMsQ0FBQ3dQLEdBQUgsQ0FBaEIsQ0FBMUIsRUFBbUQ7QUFBQ3JPLFVBQUksRUFBQ2tnQixDQUFOO0FBQVE3UixTQUFHLEVBQUM0QjtBQUFaLEtBQXpEO0FBQXdFLEdBQW5SO0FBQUEsTUFBb1IrVyxDQUFDLEdBQUMsU0FBRkEsQ0FBRSxDQUFTL0csQ0FBVCxFQUFXO0FBQUMsUUFBSXBoQixDQUFDLEdBQUNvaEIsQ0FBQyxDQUFDamdCLElBQVI7QUFBQSxRQUFha2dCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDNVIsR0FBakI7QUFBcUIsV0FBTSxlQUFhLE9BQU95WSxDQUFDLENBQUM3RyxDQUFDLENBQUNqZ0IsSUFBSCxDQUFyQixLQUFnQ25CLENBQUMsR0FBQ2lvQixDQUFDLENBQUM3RyxDQUFDLENBQUNqZ0IsSUFBSCxDQUFuQyxHQUE2QyxlQUFhLE9BQU84bUIsQ0FBQyxDQUFDN0csQ0FBQyxDQUFDNVIsR0FBSCxDQUFyQixLQUErQjZSLENBQUMsR0FBQzRHLENBQUMsQ0FBQzdHLENBQUMsQ0FBQzVSLEdBQUgsQ0FBbEMsQ0FBN0MsRUFBd0Y7QUFBQ3JPLFVBQUksRUFBQ25CLENBQU47QUFBUXdQLFNBQUcsRUFBQzZSO0FBQVosS0FBOUY7QUFBNkcsR0FBcGE7QUFBQSxNQUFxYStHLENBQUMsR0FBQyxTQUFGQSxDQUFFLENBQVNoSCxDQUFULEVBQVc7QUFBQyxRQUFJcGhCLENBQUMsR0FBQ29oQixDQUFDLENBQUNwRyxLQUFGLENBQVEsR0FBUixDQUFOO0FBQUEsUUFBbUJxRyxDQUFDLEdBQUN3RixDQUFDLENBQUM3bUIsQ0FBRCxFQUFHLENBQUgsQ0FBdEI7QUFBQSxRQUE0Qm9SLENBQUMsR0FBQ2lRLENBQUMsQ0FBQyxDQUFELENBQS9CO0FBQUEsUUFBbUN0ZCxDQUFDLEdBQUNzZCxDQUFDLENBQUMsQ0FBRCxDQUF0QztBQUEwQyxXQUFNO0FBQUM3UixTQUFHLEVBQUM0QixDQUFMO0FBQU9qUSxVQUFJLEVBQUM0QztBQUFaLEtBQU47QUFBcUIsR0FBbGY7QUFBQSxNQUFtZnNrQixDQUFDLEdBQUNELENBQXJmO0FBQUEsTUFBdWZFLENBQUMsR0FBQyxVQUFTbEgsQ0FBVCxFQUFXO0FBQUMsYUFBU3BoQixDQUFULENBQVdvaEIsQ0FBWCxFQUFhO0FBQUMsVUFBSUMsQ0FBQyxHQUFDLElBQU47QUFBV2pRLE9BQUMsQ0FBQyxJQUFELEVBQU1wUixDQUFOLENBQUQsRUFBVWtuQixDQUFDLENBQUN2RCxNQUFNLENBQUMyRCxjQUFQLENBQXNCdG5CLENBQUMsQ0FBQzRDLFNBQXhCLENBQUQsRUFBb0MsYUFBcEMsRUFBa0QsSUFBbEQsQ0FBRCxDQUF5REgsSUFBekQsQ0FBOEQsSUFBOUQsQ0FBVixFQUE4RSxLQUFLOE0sUUFBTCxHQUFjLEtBQUtBLFFBQUwsQ0FBY2daLElBQWQsQ0FBbUIsSUFBbkIsQ0FBNUYsRUFBcUhkLENBQUMsQ0FBQ3ZrQixJQUFGLENBQU8sSUFBUCxDQUFySCxFQUFrSSxLQUFLc2xCLE9BQUwsR0FBYSxFQUEvSSxFQUFrSixLQUFLQyxVQUFMLENBQWdCckgsQ0FBaEIsRUFBa0IsQ0FBQyxDQUFuQixDQUFsSixFQUF3S2xCLENBQUMsQ0FBQ2lGLE9BQUYsQ0FBVS9VLE9BQVYsQ0FBa0IsVUFBU2dSLENBQVQsRUFBVztBQUFDLHVCQUFhLE9BQU9BLENBQUMsQ0FBQ3NILFVBQXRCLElBQWtDdEgsQ0FBQyxDQUFDc0gsVUFBRixDQUFham1CLElBQWIsQ0FBa0I0ZSxDQUFsQixDQUFsQztBQUF1RCxPQUFyRixDQUF4SyxFQUErUCxLQUFLOVIsUUFBTCxFQUEvUDtBQUErUTs7QUFBQSxXQUFPbVUsQ0FBQyxDQUFDMWpCLENBQUQsRUFBR29oQixDQUFILENBQUQsRUFBTzZELENBQUMsQ0FBQ2psQixDQUFELEVBQUcsQ0FBQztBQUFDd0MsU0FBRyxFQUFDLFVBQUw7QUFBZ0JsQixXQUFLLEVBQUMsaUJBQVU7QUFBQyxZQUFJOGYsQ0FBQyxHQUFDM2QsU0FBUyxDQUFDbEMsTUFBVixJQUFrQixDQUFsQixJQUFxQixLQUFLLENBQUwsS0FBU2tDLFNBQVMsQ0FBQyxDQUFELENBQXZDLEdBQTJDLEVBQTNDLEdBQThDQSxTQUFTLENBQUMsQ0FBRCxDQUE3RDtBQUFBLFlBQWlFekQsQ0FBQyxHQUFDLEtBQUt3SCxPQUFMLENBQWFtaEIsT0FBaEY7QUFBd0YsZUFBTSxlQUFhLE9BQU8zb0IsQ0FBcEIsSUFBdUJBLENBQUMsQ0FBQ29oQixDQUFELENBQXhCLEdBQTRCLEtBQUs1WixPQUFMLENBQWFtaEIsT0FBYixDQUFxQnZILENBQXJCLENBQTVCLEdBQW9ELEtBQUs1WixPQUFMLENBQWFvaEIsV0FBYixHQUF5QixLQUFLcGhCLE9BQUwsQ0FBYW9oQixXQUFiLEdBQXlCLEdBQXpCLEdBQTZCeEgsQ0FBdEQsR0FBd0RBLENBQWxIO0FBQW9IO0FBQTdPLEtBQUQsRUFBZ1A7QUFBQzVlLFNBQUcsRUFBQyxZQUFMO0FBQWtCbEIsV0FBSyxFQUFDLGVBQVM4ZixDQUFULEVBQVc7QUFBQyxZQUFJcGhCLENBQUMsR0FBQyxJQUFOO0FBQUEsWUFBV3FoQixDQUFDLEdBQUM1ZCxTQUFTLENBQUNsQyxNQUFWLElBQWtCLENBQWxCLElBQXFCLEtBQUssQ0FBTCxLQUFTa0MsU0FBUyxDQUFDLENBQUQsQ0FBdkMsR0FBMkMsQ0FBQyxDQUE1QyxHQUE4Q0EsU0FBUyxDQUFDLENBQUQsQ0FBcEU7QUFBQSxZQUF3RTJOLENBQUMsR0FBQztBQUFDeVgsZ0JBQU0sRUFBQyxLQUFSO0FBQWNDLHNCQUFZLEVBQUMsS0FBM0I7QUFBaUNDLDBCQUFnQixFQUFDLFdBQWxEO0FBQThESCxxQkFBVyxFQUFDO0FBQTFFLFNBQTFFO0FBQThKLGFBQUtwaEIsT0FBTCxHQUFhMmEsQ0FBQyxDQUFDL1EsQ0FBRCxFQUFHZ1EsQ0FBSCxDQUFkO0FBQW9CLFlBQUlyZCxDQUFDLEdBQUMsS0FBS3lELE9BQVg7QUFBQSxZQUFtQm1hLENBQUMsR0FBQzVkLENBQUMsQ0FBQ2tELE9BQXZCO0FBQUEsWUFBK0I2YSxDQUFDLEdBQUMvZCxDQUFDLENBQUM5RCxNQUFuQztBQUFBLFlBQTBDOGhCLENBQUMsR0FBQ2hlLENBQUMsQ0FBQ2lsQixjQUE5QztBQUE2RCxZQUFHLEtBQUsvaEIsT0FBTCxHQUFhMGEsQ0FBYixFQUFlLEtBQUsxaEIsTUFBTCxHQUFZNmhCLENBQTNCLEVBQTZCLEtBQUtrSCxjQUFMLEdBQW9CakgsQ0FBakQsRUFBbUQsZUFBYSxLQUFLOWhCLE1BQWxCLElBQTBCLEtBQUtBLE1BQUwsR0FBWXVOLFFBQVEsQ0FBQzZVLElBQXJCLEVBQTBCLEtBQUsyRyxjQUFMLEdBQW9CLFNBQXhFLElBQW1GLG9CQUFrQixLQUFLL29CLE1BQXZCLEtBQWdDLEtBQUtBLE1BQUwsR0FBWXVOLFFBQVEsQ0FBQzZVLElBQXJCLEVBQTBCLEtBQUsyRyxjQUFMLEdBQW9CLGVBQTlFLENBQXRJLEVBQXFPLENBQUMsU0FBRCxFQUFXLFFBQVgsRUFBcUI1WSxPQUFyQixDQUE2QixVQUFTZ1IsQ0FBVCxFQUFXO0FBQUMsY0FBRyxlQUFhLE9BQU9waEIsQ0FBQyxDQUFDb2hCLENBQUQsQ0FBeEIsRUFBNEIsTUFBTSxJQUFJelQsS0FBSixDQUFVLHVEQUFWLENBQU47QUFBeUUseUJBQWEsT0FBTzNOLENBQUMsQ0FBQ29oQixDQUFELENBQUQsQ0FBSzZILE1BQXpCLEdBQWdDanBCLENBQUMsQ0FBQ29oQixDQUFELENBQUQsR0FBS3BoQixDQUFDLENBQUNvaEIsQ0FBRCxDQUFELENBQUssQ0FBTCxDQUFyQyxHQUE2QyxZQUFVLE9BQU9waEIsQ0FBQyxDQUFDb2hCLENBQUQsQ0FBbEIsS0FBd0JwaEIsQ0FBQyxDQUFDb2hCLENBQUQsQ0FBRCxHQUFLNVQsUUFBUSxDQUFDQyxhQUFULENBQXVCek4sQ0FBQyxDQUFDb2hCLENBQUQsQ0FBeEIsQ0FBN0IsQ0FBN0M7QUFBd0csU0FBdFAsQ0FBck8sRUFBNmRpQyxDQUFDLENBQUMsS0FBS3BjLE9BQU4sRUFBYyxLQUFLaWlCLFFBQUwsQ0FBYyxTQUFkLENBQWQsQ0FBOWQsRUFBc2dCLEtBQUsxaEIsT0FBTCxDQUFhMmhCLGdCQUFiLEtBQWdDLENBQUMsQ0FBakMsSUFBb0M5RixDQUFDLENBQUMsS0FBS3BqQixNQUFOLEVBQWEsS0FBS2lwQixRQUFMLENBQWMsUUFBZCxDQUFiLENBQTNpQixFQUFpbEIsQ0FBQyxLQUFLMWhCLE9BQUwsQ0FBYTRoQixVQUFsbUIsRUFBNm1CLE1BQU0sSUFBSXpiLEtBQUosQ0FBVSw4Q0FBVixDQUFOO0FBQWdFLGFBQUtvYixnQkFBTCxHQUFzQlYsQ0FBQyxDQUFDLEtBQUs3Z0IsT0FBTCxDQUFhdWhCLGdCQUFkLENBQXZCLEVBQXVELEtBQUtLLFVBQUwsR0FBZ0JmLENBQUMsQ0FBQyxLQUFLN2dCLE9BQUwsQ0FBYTRoQixVQUFkLENBQXhFLEVBQWtHLEtBQUtQLE1BQUwsR0FBWVQsQ0FBQyxDQUFDLEtBQUs1Z0IsT0FBTCxDQUFhcWhCLE1BQWQsQ0FBL0csRUFBcUksS0FBS0MsWUFBTCxHQUFrQlYsQ0FBQyxDQUFDLEtBQUs1Z0IsT0FBTCxDQUFhc2hCLFlBQWQsQ0FBeEosRUFBb0wsZUFBYSxPQUFPLEtBQUtuRSxhQUF6QixJQUF3QyxLQUFLalUsT0FBTCxFQUE1TixFQUEyTyxvQkFBa0IsS0FBS3NZLGNBQXZCLEdBQXNDLEtBQUtyRSxhQUFMLEdBQW1CLENBQUMsS0FBSzFrQixNQUFOLENBQXpELEdBQXVFLEtBQUswa0IsYUFBTCxHQUFtQmxELENBQUMsQ0FBQyxLQUFLeGhCLE1BQU4sQ0FBdFUsRUFBb1YsS0FBS3VILE9BQUwsQ0FBYTZoQixPQUFiLEtBQXVCLENBQUMsQ0FBeEIsSUFBMkIsS0FBSzdZLE1BQUwsQ0FBWTZRLENBQVosQ0FBL1c7QUFBOFg7QUFBOXpDLEtBQWhQLEVBQWdqRDtBQUFDN2UsU0FBRyxFQUFDLGlCQUFMO0FBQXVCbEIsV0FBSyxFQUFDLGlCQUFVO0FBQUMsWUFBRyxlQUFhLE9BQU8sS0FBSzBuQixjQUE1QixFQUEyQyxPQUFPbEgsQ0FBQyxDQUFDLEtBQUs3aEIsTUFBTixDQUFSOztBQUFzQixZQUFHLGNBQVksS0FBSytvQixjQUFwQixFQUFtQztBQUFDLGNBQUcsS0FBSy9vQixNQUFMLEtBQWN1TixRQUFRLENBQUM2VSxJQUExQixFQUErQixPQUFNO0FBQUM3UyxlQUFHLEVBQUNxVixXQUFMO0FBQWlCMWpCLGdCQUFJLEVBQUN5akIsV0FBdEI7QUFBa0NuakIsa0JBQU0sRUFBQ3NqQixXQUF6QztBQUFxRDdjLGlCQUFLLEVBQUM0YztBQUEzRCxXQUFOO0FBQTZFLGNBQUkxRCxDQUFDLEdBQUNVLENBQUMsQ0FBQyxLQUFLN2hCLE1BQU4sQ0FBUDtBQUFBLGNBQXFCRCxDQUFDLEdBQUM7QUFBQ3lCLGtCQUFNLEVBQUMyZixDQUFDLENBQUMzZixNQUFWO0FBQWlCeUcsaUJBQUssRUFBQ2taLENBQUMsQ0FBQ2xaLEtBQXpCO0FBQStCc0gsZUFBRyxFQUFDNFIsQ0FBQyxDQUFDNVIsR0FBckM7QUFBeUNyTyxnQkFBSSxFQUFDaWdCLENBQUMsQ0FBQ2pnQjtBQUFoRCxXQUF2QjtBQUE2RSxpQkFBT25CLENBQUMsQ0FBQ3lCLE1BQUYsR0FBUzhRLElBQUksQ0FBQ29WLEdBQUwsQ0FBUzNuQixDQUFDLENBQUN5QixNQUFYLEVBQWtCMmYsQ0FBQyxDQUFDM2YsTUFBRixJQUFVb2pCLFdBQVcsR0FBQ3pELENBQUMsQ0FBQzVSLEdBQXhCLENBQWxCLENBQVQsRUFBeUR4UCxDQUFDLENBQUN5QixNQUFGLEdBQVM4USxJQUFJLENBQUNvVixHQUFMLENBQVMzbkIsQ0FBQyxDQUFDeUIsTUFBWCxFQUFrQjJmLENBQUMsQ0FBQzNmLE1BQUYsSUFBVTJmLENBQUMsQ0FBQzVSLEdBQUYsR0FBTTRSLENBQUMsQ0FBQzNmLE1BQVIsSUFBZ0JvakIsV0FBVyxHQUFDRSxXQUE1QixDQUFWLENBQWxCLENBQWxFLEVBQXlJL2tCLENBQUMsQ0FBQ3lCLE1BQUYsR0FBUzhRLElBQUksQ0FBQ29WLEdBQUwsQ0FBUzVDLFdBQVQsRUFBcUIva0IsQ0FBQyxDQUFDeUIsTUFBdkIsQ0FBbEosRUFBaUx6QixDQUFDLENBQUN5QixNQUFGLElBQVUsQ0FBM0wsRUFBNkx6QixDQUFDLENBQUNrSSxLQUFGLEdBQVFxSyxJQUFJLENBQUNvVixHQUFMLENBQVMzbkIsQ0FBQyxDQUFDa0ksS0FBWCxFQUFpQmtaLENBQUMsQ0FBQ2xaLEtBQUYsSUFBUzBjLFdBQVcsR0FBQ3hELENBQUMsQ0FBQ2pnQixJQUF2QixDQUFqQixDQUFyTSxFQUFvUG5CLENBQUMsQ0FBQ2tJLEtBQUYsR0FBUXFLLElBQUksQ0FBQ29WLEdBQUwsQ0FBUzNuQixDQUFDLENBQUNrSSxLQUFYLEVBQWlCa1osQ0FBQyxDQUFDbFosS0FBRixJQUFTa1osQ0FBQyxDQUFDamdCLElBQUYsR0FBT2lnQixDQUFDLENBQUNsWixLQUFULElBQWdCMGMsV0FBVyxHQUFDRSxVQUE1QixDQUFULENBQWpCLENBQTVQLEVBQWdVOWtCLENBQUMsQ0FBQ2tJLEtBQUYsR0FBUXFLLElBQUksQ0FBQ29WLEdBQUwsQ0FBUzdDLFVBQVQsRUFBb0I5a0IsQ0FBQyxDQUFDa0ksS0FBdEIsQ0FBeFUsRUFBcVdsSSxDQUFDLENBQUNrSSxLQUFGLElBQVMsQ0FBOVcsRUFBZ1hsSSxDQUFDLENBQUN3UCxHQUFGLEdBQU1xVixXQUFOLEtBQW9CN2tCLENBQUMsQ0FBQ3dQLEdBQUYsR0FBTXFWLFdBQTFCLENBQWhYLEVBQXVaN2tCLENBQUMsQ0FBQ21CLElBQUYsR0FBT3lqQixXQUFQLEtBQXFCNWtCLENBQUMsQ0FBQ21CLElBQUYsR0FBT3lqQixXQUE1QixDQUF2WixFQUFnYzVrQixDQUF2YztBQUF5Yzs7QUFBQSxZQUFHLG9CQUFrQixLQUFLZ3BCLGNBQTFCLEVBQXlDO0FBQUMsY0FBSTVILENBQUMsR0FBQyxLQUFLLENBQVg7QUFBQSxjQUFhQyxDQUFDLEdBQUMsS0FBS3BoQixNQUFwQjtBQUEyQm9oQixXQUFDLEtBQUc3VCxRQUFRLENBQUM2VSxJQUFiLElBQW1CaEIsQ0FBQyxHQUFDN1QsUUFBUSxDQUFDdVAsZUFBWCxFQUEyQnFFLENBQUMsR0FBQztBQUFDamdCLGdCQUFJLEVBQUN5akIsV0FBTjtBQUFrQnBWLGVBQUcsRUFBQ3FWLFdBQXRCO0FBQWtDcGpCLGtCQUFNLEVBQUNzakIsV0FBekM7QUFBcUQ3YyxpQkFBSyxFQUFDNGM7QUFBM0QsV0FBaEQsSUFBd0gxRCxDQUFDLEdBQUNVLENBQUMsQ0FBQ1QsQ0FBRCxDQUEzSDtBQUErSCxjQUFJalEsQ0FBQyxHQUFDeVEsZ0JBQWdCLENBQUNSLENBQUQsQ0FBdEI7QUFBQSxjQUEwQnRkLENBQUMsR0FBQ3NkLENBQUMsQ0FBQ21CLFdBQUYsR0FBY25CLENBQUMsQ0FBQ3VCLFdBQWhCLElBQTZCLENBQUN4UixDQUFDLENBQUM0USxRQUFILEVBQVk1USxDQUFDLENBQUM4USxTQUFkLEVBQXlCdFIsT0FBekIsQ0FBaUMsUUFBakMsS0FBNEMsQ0FBekUsSUFBNEUsS0FBSzNRLE1BQUwsS0FBY3VOLFFBQVEsQ0FBQzZVLElBQS9IO0FBQUEsY0FBb0laLENBQUMsR0FBQyxDQUF0STtBQUF3STFkLFdBQUMsS0FBRzBkLENBQUMsR0FBQyxFQUFMLENBQUQ7QUFBVSxjQUFJRSxDQUFDLEdBQUNQLENBQUMsQ0FBQzNmLE1BQUYsR0FBUytpQixVQUFVLENBQUNwVCxDQUFDLENBQUNrWSxjQUFILENBQW5CLEdBQXNDOUUsVUFBVSxDQUFDcFQsQ0FBQyxDQUFDbVksaUJBQUgsQ0FBaEQsR0FBc0U5SCxDQUE1RTtBQUFBLGNBQThFemhCLENBQUMsR0FBQztBQUFDa0ksaUJBQUssRUFBQyxFQUFQO0FBQVV6RyxrQkFBTSxFQUFDLE9BQUtrZ0IsQ0FBTCxJQUFRQSxDQUFDLEdBQUNOLENBQUMsQ0FBQ29CLFlBQVosQ0FBakI7QUFBMkN0aEIsZ0JBQUksRUFBQ2lnQixDQUFDLENBQUNqZ0IsSUFBRixHQUFPaWdCLENBQUMsQ0FBQ2xaLEtBQVQsR0FBZXNjLFVBQVUsQ0FBQ3BULENBQUMsQ0FBQ29ZLGVBQUgsQ0FBekIsR0FBNkM7QUFBN0YsV0FBaEY7QUFBQSxjQUFpTHpILENBQUMsR0FBQyxDQUFuTDtBQUFxTCxnQkFBSUosQ0FBSixJQUFPLEtBQUsxaEIsTUFBTCxLQUFjdU4sUUFBUSxDQUFDNlUsSUFBOUIsS0FBcUNOLENBQUMsR0FBQyxDQUFDLEtBQUQsR0FBT3hQLElBQUksQ0FBQ0MsR0FBTCxDQUFTbVAsQ0FBVCxFQUFXLENBQVgsQ0FBUCxHQUFxQixTQUFPQSxDQUE1QixHQUE4QixLQUFyRSxHQUE0RSxLQUFLMWhCLE1BQUwsS0FBY3VOLFFBQVEsQ0FBQzZVLElBQXZCLEtBQThCcmlCLENBQUMsQ0FBQ3lCLE1BQUYsR0FBUzhRLElBQUksQ0FBQ2tYLEdBQUwsQ0FBU3pwQixDQUFDLENBQUN5QixNQUFYLEVBQWtCLEVBQWxCLENBQXZDLENBQTVFO0FBQTBJLGNBQUl3Z0IsQ0FBQyxHQUFDLEtBQUtoaUIsTUFBTCxDQUFZeXBCLFNBQVosSUFBdUJySSxDQUFDLENBQUNvQixZQUFGLEdBQWVkLENBQXRDLENBQU47QUFBK0MsaUJBQU8zaEIsQ0FBQyxDQUFDd1AsR0FBRixHQUFNeVMsQ0FBQyxJQUFFTixDQUFDLEdBQUMzaEIsQ0FBQyxDQUFDeUIsTUFBSixHQUFXc2dCLENBQWIsQ0FBRCxHQUFpQlgsQ0FBQyxDQUFDNVIsR0FBbkIsR0FBdUJnVixVQUFVLENBQUNwVCxDQUFDLENBQUNrWSxjQUFILENBQXZDLEVBQTBELEtBQUtycEIsTUFBTCxLQUFjdU4sUUFBUSxDQUFDNlUsSUFBdkIsS0FBOEJyaUIsQ0FBQyxDQUFDeUIsTUFBRixHQUFTOFEsSUFBSSxDQUFDa1gsR0FBTCxDQUFTenBCLENBQUMsQ0FBQ3lCLE1BQVgsRUFBa0IsRUFBbEIsQ0FBdkMsQ0FBMUQsRUFBd0h6QixDQUEvSDtBQUFpSTtBQUFDO0FBQXJsRCxLQUFoakQsRUFBdW9HO0FBQUN3QyxTQUFHLEVBQUMsWUFBTDtBQUFrQmxCLFdBQUssRUFBQyxpQkFBVTtBQUFDLGFBQUtxb0IsTUFBTCxHQUFZLEVBQVo7QUFBZTtBQUFsRCxLQUF2b0csRUFBMnJHO0FBQUNubkIsU0FBRyxFQUFDLE9BQUw7QUFBYWxCLFdBQUssRUFBQyxlQUFTOGYsQ0FBVCxFQUFXcGhCLENBQVgsRUFBYTtBQUFDLGVBQU0sZUFBYSxPQUFPLEtBQUsycEIsTUFBekIsS0FBa0MsS0FBS0EsTUFBTCxHQUFZLEVBQTlDLEdBQWtELGVBQWEsT0FBTyxLQUFLQSxNQUFMLENBQVl2SSxDQUFaLENBQXBCLEtBQXFDLEtBQUt1SSxNQUFMLENBQVl2SSxDQUFaLElBQWVwaEIsQ0FBQyxDQUFDeUMsSUFBRixDQUFPLElBQVAsQ0FBcEQsQ0FBbEQsRUFBb0gsS0FBS2tuQixNQUFMLENBQVl2SSxDQUFaLENBQTFIO0FBQXlJO0FBQTFLLEtBQTNyRyxFQUF1Mkc7QUFBQzVlLFNBQUcsRUFBQyxRQUFMO0FBQWNsQixXQUFLLEVBQUMsaUJBQVU7QUFBQyxZQUFJOGYsQ0FBQyxHQUFDLElBQU47QUFBQSxZQUFXcGhCLENBQUMsR0FBQ3lELFNBQVMsQ0FBQ2xDLE1BQVYsSUFBa0IsQ0FBbEIsSUFBcUIsS0FBSyxDQUFMLEtBQVNrQyxTQUFTLENBQUMsQ0FBRCxDQUF2QyxHQUEyQyxDQUFDLENBQTVDLEdBQThDQSxTQUFTLENBQUMsQ0FBRCxDQUFwRTtBQUF3RSxhQUFLK0QsT0FBTCxDQUFhMmhCLGdCQUFiLEtBQWdDLENBQUMsQ0FBakMsSUFBb0M5RixDQUFDLENBQUMsS0FBS3BqQixNQUFOLEVBQWEsS0FBS2lwQixRQUFMLENBQWMsU0FBZCxDQUFiLENBQXJDLEVBQTRFN0YsQ0FBQyxDQUFDLEtBQUtwYyxPQUFOLEVBQWMsS0FBS2lpQixRQUFMLENBQWMsU0FBZCxDQUFkLENBQTdFLEVBQXFILEtBQUtHLE9BQUwsR0FBYSxDQUFDLENBQW5JLEVBQXFJLEtBQUsxRSxhQUFMLENBQW1CdlUsT0FBbkIsQ0FBMkIsVUFBU3BRLENBQVQsRUFBVztBQUFDQSxXQUFDLEtBQUdvaEIsQ0FBQyxDQUFDbmhCLE1BQUYsQ0FBU3VoQixhQUFiLElBQTRCeGhCLENBQUMsQ0FBQzhDLGdCQUFGLENBQW1CLFFBQW5CLEVBQTRCc2UsQ0FBQyxDQUFDN1IsUUFBOUIsQ0FBNUI7QUFBb0UsU0FBM0csQ0FBckksRUFBa1B2UCxDQUFDLElBQUUsS0FBS3VQLFFBQUwsRUFBclA7QUFBcVE7QUFBNVcsS0FBdjJHLEVBQXF0SDtBQUFDL00sU0FBRyxFQUFDLFNBQUw7QUFBZWxCLFdBQUssRUFBQyxpQkFBVTtBQUFDLFlBQUk4ZixDQUFDLEdBQUMsSUFBTjtBQUFXNkIsU0FBQyxDQUFDLEtBQUtoakIsTUFBTixFQUFhLEtBQUtpcEIsUUFBTCxDQUFjLFNBQWQsQ0FBYixDQUFELEVBQXdDakcsQ0FBQyxDQUFDLEtBQUtoYyxPQUFOLEVBQWMsS0FBS2lpQixRQUFMLENBQWMsU0FBZCxDQUFkLENBQXpDLEVBQWlGLEtBQUtHLE9BQUwsR0FBYSxDQUFDLENBQS9GLEVBQWlHLGVBQWEsT0FBTyxLQUFLMUUsYUFBekIsSUFBd0MsS0FBS0EsYUFBTCxDQUFtQnZVLE9BQW5CLENBQTJCLFVBQVNwUSxDQUFULEVBQVc7QUFBQ0EsV0FBQyxDQUFDOEQsbUJBQUYsQ0FBc0IsUUFBdEIsRUFBK0JzZCxDQUFDLENBQUM3UixRQUFqQztBQUEyQyxTQUFsRixDQUF6STtBQUE2TjtBQUF4USxLQUFydEgsRUFBKzlIO0FBQUMvTSxTQUFHLEVBQUMsU0FBTDtBQUFlbEIsV0FBSyxFQUFDLGlCQUFVO0FBQUMsWUFBSThmLENBQUMsR0FBQyxJQUFOO0FBQVcsYUFBSzFRLE9BQUwsSUFBZStXLENBQUMsQ0FBQ3JYLE9BQUYsQ0FBVSxVQUFTcFEsQ0FBVCxFQUFXcWhCLENBQVgsRUFBYTtBQUFDcmhCLFdBQUMsS0FBR29oQixDQUFKLElBQU9xRyxDQUFDLENBQUN6akIsTUFBRixDQUFTcWQsQ0FBVCxFQUFXLENBQVgsQ0FBUDtBQUFxQixTQUE3QyxDQUFmLEVBQThELE1BQUlvRyxDQUFDLENBQUNsbUIsTUFBTixJQUFjb2dCLENBQUMsRUFBN0U7QUFBZ0Y7QUFBM0gsS0FBLzlILEVBQTRsSTtBQUFDbmYsU0FBRyxFQUFDLHFCQUFMO0FBQTJCbEIsV0FBSyxFQUFDLGVBQVM4ZixDQUFULEVBQVdwaEIsQ0FBWCxFQUFhO0FBQUMsWUFBSXFoQixDQUFDLEdBQUMsSUFBTjtBQUFXRCxTQUFDLEdBQUNBLENBQUMsSUFBRSxLQUFLZ0ksVUFBVixFQUFxQnBwQixDQUFDLEdBQUNBLENBQUMsSUFBRSxLQUFLK29CLGdCQUEvQjtBQUFnRCxZQUFJM1gsQ0FBQyxHQUFDLENBQUMsTUFBRCxFQUFRLEtBQVIsRUFBYyxRQUFkLEVBQXVCLE9BQXZCLEVBQStCLFFBQS9CLEVBQXdDLFFBQXhDLENBQU47QUFBd0QsdUJBQWEsT0FBTyxLQUFLd1ksaUJBQXpCLElBQTRDLEtBQUtBLGlCQUFMLENBQXVCcm9CLE1BQW5FLElBQTJFLEtBQUtxb0IsaUJBQUwsQ0FBdUI1bEIsTUFBdkIsQ0FBOEIsQ0FBOUIsRUFBZ0MsS0FBSzRsQixpQkFBTCxDQUF1QnJvQixNQUF2RCxDQUEzRSxFQUEwSSxlQUFhLE9BQU8sS0FBS3FvQixpQkFBekIsS0FBNkMsS0FBS0EsaUJBQUwsR0FBdUIsRUFBcEUsQ0FBMUk7QUFBa04sWUFBSTdsQixDQUFDLEdBQUMsS0FBSzZsQixpQkFBWDtBQUE2QnhJLFNBQUMsQ0FBQzVSLEdBQUYsSUFBT3pMLENBQUMsQ0FBQ2IsSUFBRixDQUFPLEtBQUtnbUIsUUFBTCxDQUFjLGtCQUFkLElBQWtDLEdBQWxDLEdBQXNDOUgsQ0FBQyxDQUFDNVIsR0FBL0MsQ0FBUCxFQUEyRDRSLENBQUMsQ0FBQ2pnQixJQUFGLElBQVE0QyxDQUFDLENBQUNiLElBQUYsQ0FBTyxLQUFLZ21CLFFBQUwsQ0FBYyxrQkFBZCxJQUFrQyxHQUFsQyxHQUFzQzlILENBQUMsQ0FBQ2pnQixJQUEvQyxDQUFuRSxFQUF3SG5CLENBQUMsQ0FBQ3dQLEdBQUYsSUFBT3pMLENBQUMsQ0FBQ2IsSUFBRixDQUFPLEtBQUtnbUIsUUFBTCxDQUFjLGlCQUFkLElBQWlDLEdBQWpDLEdBQXFDbHBCLENBQUMsQ0FBQ3dQLEdBQTlDLENBQS9ILEVBQWtMeFAsQ0FBQyxDQUFDbUIsSUFBRixJQUFRNEMsQ0FBQyxDQUFDYixJQUFGLENBQU8sS0FBS2dtQixRQUFMLENBQWMsaUJBQWQsSUFBaUMsR0FBakMsR0FBcUNscEIsQ0FBQyxDQUFDbUIsSUFBOUMsQ0FBMUw7QUFBOE8sWUFBSXNnQixDQUFDLEdBQUMsRUFBTjtBQUFTclEsU0FBQyxDQUFDaEIsT0FBRixDQUFVLFVBQVNnUixDQUFULEVBQVc7QUFBQ0ssV0FBQyxDQUFDdmUsSUFBRixDQUFPbWUsQ0FBQyxDQUFDNkgsUUFBRixDQUFXLGtCQUFYLElBQStCLEdBQS9CLEdBQW1DOUgsQ0FBMUMsR0FBNkNLLENBQUMsQ0FBQ3ZlLElBQUYsQ0FBT21lLENBQUMsQ0FBQzZILFFBQUYsQ0FBVyxpQkFBWCxJQUE4QixHQUE5QixHQUFrQzlILENBQXpDLENBQTdDO0FBQXlGLFNBQS9HLEdBQWlIa0UsQ0FBQyxDQUFDLFlBQVU7QUFBQyx5QkFBYSxPQUFPakUsQ0FBQyxDQUFDdUksaUJBQXRCLEtBQTBDbkcsQ0FBQyxDQUFDcEMsQ0FBQyxDQUFDcGEsT0FBSCxFQUFXb2EsQ0FBQyxDQUFDdUksaUJBQWIsRUFBK0JuSSxDQUEvQixDQUFELEVBQW1DSixDQUFDLENBQUM3WixPQUFGLENBQVUyaEIsZ0JBQVYsS0FBNkIsQ0FBQyxDQUE5QixJQUFpQzFGLENBQUMsQ0FBQ3BDLENBQUMsQ0FBQ3BoQixNQUFILEVBQVVvaEIsQ0FBQyxDQUFDdUksaUJBQVosRUFBOEJuSSxDQUE5QixDQUFyRSxFQUFzRyxPQUFPSixDQUFDLENBQUN1SSxpQkFBeko7QUFBNEssU0FBeEwsQ0FBbEg7QUFBNFM7QUFBcDdCLEtBQTVsSSxFQUFraEs7QUFBQ3BuQixTQUFHLEVBQUMsVUFBTDtBQUFnQmxCLFdBQUssRUFBQyxpQkFBVTtBQUFDLFlBQUk4ZixDQUFDLEdBQUMsSUFBTjtBQUFBLFlBQVdwaEIsQ0FBQyxHQUFDeUQsU0FBUyxDQUFDbEMsTUFBVixJQUFrQixDQUFsQixJQUFxQixLQUFLLENBQUwsS0FBU2tDLFNBQVMsQ0FBQyxDQUFELENBQXZDLEdBQTJDLENBQUMsQ0FBNUMsR0FBOENBLFNBQVMsQ0FBQyxDQUFELENBQXBFOztBQUF3RSxZQUFHLEtBQUs0bEIsT0FBUixFQUFnQjtBQUFDLGVBQUtRLFVBQUw7QUFBa0IsY0FBSXhJLENBQUMsR0FBQzZHLENBQUMsQ0FBQyxLQUFLYSxnQkFBTixFQUF1QixLQUFLSyxVQUE1QixDQUFQO0FBQStDLGVBQUtVLG1CQUFMLENBQXlCLEtBQUtWLFVBQTlCLEVBQXlDL0gsQ0FBekM7QUFBNEMsY0FBSWpRLENBQUMsR0FBQyxLQUFLMlksS0FBTCxDQUFXLGdCQUFYLEVBQTRCLFlBQVU7QUFBQyxtQkFBT2pJLENBQUMsQ0FBQ1YsQ0FBQyxDQUFDbmEsT0FBSCxDQUFSO0FBQW9CLFdBQTNELENBQU47QUFBQSxjQUFtRWxELENBQUMsR0FBQ3FOLENBQUMsQ0FBQ2xKLEtBQXZFO0FBQUEsY0FBNkV1WixDQUFDLEdBQUNyUSxDQUFDLENBQUMzUCxNQUFqRjs7QUFBd0YsY0FBRyxNQUFJc0MsQ0FBSixJQUFPLE1BQUkwZCxDQUFYLElBQWMsZUFBYSxPQUFPLEtBQUt1SSxRQUExQyxFQUFtRDtBQUFDLGdCQUFJckksQ0FBQyxHQUFDLEtBQUtxSSxRQUFYO0FBQW9Cam1CLGFBQUMsR0FBQzRkLENBQUMsQ0FBQ3paLEtBQUosRUFBVXVaLENBQUMsR0FBQ0UsQ0FBQyxDQUFDbGdCLE1BQWQ7QUFBcUIsV0FBN0YsTUFBa0csS0FBS3VvQixRQUFMLEdBQWM7QUFBQzloQixpQkFBSyxFQUFDbkUsQ0FBUDtBQUFTdEMsa0JBQU0sRUFBQ2dnQjtBQUFoQixXQUFkOztBQUFpQyxjQUFJVSxDQUFDLEdBQUMsS0FBSzRILEtBQUwsQ0FBVyxlQUFYLEVBQTJCLFlBQVU7QUFBQyxtQkFBTzNJLENBQUMsQ0FBQzZJLGVBQUYsRUFBUDtBQUEyQixXQUFqRSxDQUFOO0FBQUEsY0FBeUVoSCxDQUFDLEdBQUNkLENBQTNFO0FBQUEsY0FBNkVrQixDQUFDLEdBQUNvQixDQUFDLENBQUMwRCxDQUFDLENBQUMsS0FBS2lCLFVBQU4sQ0FBRixFQUFvQjtBQUFDbGhCLGlCQUFLLEVBQUNuRSxDQUFQO0FBQVN0QyxrQkFBTSxFQUFDZ2dCO0FBQWhCLFdBQXBCLENBQWhGO0FBQUEsY0FBd0g2QixDQUFDLEdBQUNtQixDQUFDLENBQUMwRCxDQUFDLENBQUM5RyxDQUFELENBQUYsRUFBTTRCLENBQU4sQ0FBM0g7QUFBQSxjQUFvSUUsQ0FBQyxHQUFDc0IsQ0FBQyxDQUFDLEtBQUtvRSxNQUFOLEVBQWE7QUFBQzNnQixpQkFBSyxFQUFDbkUsQ0FBUDtBQUFTdEMsa0JBQU0sRUFBQ2dnQjtBQUFoQixXQUFiLENBQXZJO0FBQUEsY0FBd0syQixDQUFDLEdBQUNxQixDQUFDLENBQUMsS0FBS3FFLFlBQU4sRUFBbUI3RixDQUFuQixDQUEzSztBQUFpTUksV0FBQyxHQUFDa0IsQ0FBQyxDQUFDbEIsQ0FBRCxFQUFHRixDQUFILENBQUgsRUFBU0csQ0FBQyxHQUFDaUIsQ0FBQyxDQUFDakIsQ0FBRCxFQUFHRixDQUFILENBQVo7O0FBQWtCLGVBQUksSUFBSUssQ0FBQyxHQUFDdEIsQ0FBQyxDQUFDaGhCLElBQUYsR0FBT21pQixDQUFDLENBQUNuaUIsSUFBVCxHQUFja2lCLENBQUMsQ0FBQ2xpQixJQUF0QixFQUEyQnVpQixDQUFDLEdBQUN2QixDQUFDLENBQUMzUyxHQUFGLEdBQU04VCxDQUFDLENBQUM5VCxHQUFSLEdBQVk2VCxDQUFDLENBQUM3VCxHQUEzQyxFQUErQzBVLENBQUMsR0FBQyxDQUFyRCxFQUF1REEsQ0FBQyxHQUFDaEUsQ0FBQyxDQUFDaUYsT0FBRixDQUFVNWpCLE1BQW5FLEVBQTBFLEVBQUUyaUIsQ0FBNUUsRUFBOEU7QUFBQyxnQkFBSUMsQ0FBQyxHQUFDakUsQ0FBQyxDQUFDaUYsT0FBRixDQUFVakIsQ0FBVixDQUFOO0FBQUEsZ0JBQW1CUSxDQUFDLEdBQUNQLENBQUMsQ0FBQzVVLFFBQUYsQ0FBVzlNLElBQVgsQ0FBZ0IsSUFBaEIsRUFBcUI7QUFBQ3RCLGtCQUFJLEVBQUNzaUIsQ0FBTjtBQUFRalUsaUJBQUcsRUFBQ2tVLENBQVo7QUFBY3FGLDhCQUFnQixFQUFDMUgsQ0FBL0I7QUFBaUM2SSx1QkFBUyxFQUFDL0gsQ0FBM0M7QUFBNkNnSSx3QkFBVSxFQUFDL1ksQ0FBeEQ7QUFBMER5WCxvQkFBTSxFQUFDeEYsQ0FBakU7QUFBbUV5RiwwQkFBWSxFQUFDeEYsQ0FBaEY7QUFBa0Y4RywwQkFBWSxFQUFDakgsQ0FBL0Y7QUFBaUdrSCxnQ0FBa0IsRUFBQ2pILENBQXBIO0FBQXNIa0gsMkJBQWEsRUFBQ2pGLENBQXBJO0FBQXNJK0Qsd0JBQVUsRUFBQyxLQUFLQTtBQUF0SixhQUFyQixDQUFyQjtBQUE2TSxnQkFBRzFFLENBQUMsS0FBRyxDQUFDLENBQVIsRUFBVSxPQUFNLENBQUMsQ0FBUDtBQUFTLDJCQUFhLE9BQU9BLENBQXBCLElBQXVCLG9CQUFpQkEsQ0FBakIsQ0FBdkIsS0FBNENoQixDQUFDLEdBQUNnQixDQUFDLENBQUNsVixHQUFKLEVBQVFpVSxDQUFDLEdBQUNpQixDQUFDLENBQUN2akIsSUFBeEQ7QUFBOEQ7O0FBQUEsY0FBSThqQixDQUFDLEdBQUM7QUFBQ3NGLGdCQUFJLEVBQUM7QUFBQy9hLGlCQUFHLEVBQUNrVSxDQUFMO0FBQU92aUIsa0JBQUksRUFBQ3NpQjtBQUFaLGFBQU47QUFBcUIrRyxvQkFBUSxFQUFDO0FBQUNoYixpQkFBRyxFQUFDa1UsQ0FBQyxHQUFDbUIsV0FBUDtBQUFtQmpELG9CQUFNLEVBQUNpRCxXQUFXLEdBQUNuQixDQUFaLEdBQWNqQyxDQUFkLEdBQWdCc0QsV0FBMUM7QUFBc0Q1akIsa0JBQUksRUFBQ3NpQixDQUFDLEdBQUNtQixXQUE3RDtBQUF5RXhqQixtQkFBSyxFQUFDd2pCLFdBQVcsR0FBQ25CLENBQVosR0FBYzFmLENBQWQsR0FBZ0IrZ0I7QUFBL0Y7QUFBOUIsV0FBTjtBQUFBLGNBQWdKeEMsQ0FBQyxHQUFDLEtBQUtyaUIsTUFBTCxDQUFZdWhCLGFBQTlKO0FBQUEsY0FBNEs0RCxDQUFDLEdBQUM5QyxDQUFDLENBQUNaLFdBQWhMO0FBQUEsY0FBNEwyRCxDQUFDLEdBQUMsS0FBSyxDQUFuTTtBQUFxTSxpQkFBTy9DLENBQUMsQ0FBQ0QsSUFBRixDQUFPRyxXQUFQLEdBQW1CNEMsQ0FBQyxDQUFDTixVQUFyQixLQUFrQ08sQ0FBQyxHQUFDLEtBQUswRSxLQUFMLENBQVcsZ0JBQVgsRUFBNEI5SCxDQUE1QixDQUFGLEVBQWlDZ0QsQ0FBQyxDQUFDdUYsUUFBRixDQUFXNUksTUFBWCxJQUFtQnlELENBQUMsQ0FBQzVqQixNQUF4RixHQUFnRzZnQixDQUFDLENBQUNELElBQUYsQ0FBT0ksWUFBUCxHQUFvQjJDLENBQUMsQ0FBQ0wsV0FBdEIsS0FBb0NNLENBQUMsR0FBQyxLQUFLMEUsS0FBTCxDQUFXLGdCQUFYLEVBQTRCOUgsQ0FBNUIsQ0FBRixFQUFpQ2dELENBQUMsQ0FBQ3VGLFFBQUYsQ0FBV3BwQixLQUFYLElBQWtCaWtCLENBQUMsQ0FBQ25kLEtBQXpGLENBQWhHLEVBQWdNLENBQUMsQ0FBQyxDQUFELEtBQUssQ0FBQyxFQUFELEVBQUksUUFBSixFQUFjMEksT0FBZCxDQUFzQjBSLENBQUMsQ0FBQ0QsSUFBRixDQUFPcFcsS0FBUCxDQUFhc0QsUUFBbkMsQ0FBTCxJQUFtRCxDQUFDLENBQUQsS0FBSyxDQUFDLEVBQUQsRUFBSSxRQUFKLEVBQWNxQixPQUFkLENBQXNCMFIsQ0FBQyxDQUFDRCxJQUFGLENBQU9vSSxhQUFQLENBQXFCeGUsS0FBckIsQ0FBMkJzRCxRQUFqRCxDQUF6RCxNQUF1SDBWLENBQUMsQ0FBQ3NGLElBQUYsQ0FBTzNJLE1BQVAsR0FBY1UsQ0FBQyxDQUFDRCxJQUFGLENBQU9JLFlBQVAsR0FBb0JpQixDQUFwQixHQUFzQmpDLENBQXBDLEVBQXNDd0QsQ0FBQyxDQUFDc0YsSUFBRixDQUFPbnBCLEtBQVAsR0FBYWtoQixDQUFDLENBQUNELElBQUYsQ0FBT0csV0FBUCxHQUFtQmlCLENBQW5CLEdBQXFCMWYsQ0FBL0wsQ0FBaE0sRUFBa1ksZUFBYSxPQUFPLEtBQUt5RCxPQUFMLENBQWFrakIsYUFBakMsSUFBZ0QsS0FBS2xqQixPQUFMLENBQWFrakIsYUFBYixDQUEyQkMsV0FBM0IsS0FBeUMsQ0FBQyxDQUExRixJQUE2RixlQUFhLE9BQU8sS0FBSzNCLGNBQXRILElBQXNJLENBQUMsWUFBVTtBQUFDLGdCQUFJaHBCLENBQUMsR0FBQ29oQixDQUFDLENBQUMySSxLQUFGLENBQVEscUJBQVIsRUFBOEIsWUFBVTtBQUFDLHFCQUFPaEksQ0FBQyxDQUFDWCxDQUFDLENBQUNuaEIsTUFBSCxDQUFSO0FBQW1CLGFBQTVELENBQU47QUFBQSxnQkFBb0VvaEIsQ0FBQyxHQUFDRCxDQUFDLENBQUMySSxLQUFGLENBQVEsNEJBQVIsRUFBcUMsWUFBVTtBQUFDLHFCQUFPakksQ0FBQyxDQUFDOWhCLENBQUQsQ0FBUjtBQUFZLGFBQTVELENBQXRFO0FBQUEsZ0JBQW9Jb1IsQ0FBQyxHQUFDeVEsZ0JBQWdCLENBQUM3aEIsQ0FBRCxDQUF0SjtBQUFBLGdCQUEwSitELENBQUMsR0FBQ3NkLENBQTVKO0FBQUEsZ0JBQThKSSxDQUFDLEdBQUMsRUFBaEs7O0FBQW1LLGdCQUFHLENBQUMsS0FBRCxFQUFPLE1BQVAsRUFBYyxRQUFkLEVBQXVCLE9BQXZCLEVBQWdDclIsT0FBaEMsQ0FBd0MsVUFBU2dSLENBQVQsRUFBVztBQUFDSyxlQUFDLENBQUNMLENBQUMsQ0FBQ3RJLFdBQUYsRUFBRCxDQUFELEdBQW1CMEwsVUFBVSxDQUFDcFQsQ0FBQyxDQUFDLFdBQVNnUSxDQUFULEdBQVcsT0FBWixDQUFGLENBQTdCO0FBQXFELGFBQXpHLEdBQTJHQyxDQUFDLENBQUNqZ0IsS0FBRixHQUFRa2hCLENBQUMsQ0FBQ0QsSUFBRixDQUFPRyxXQUFQLEdBQW1CbkIsQ0FBQyxDQUFDbGdCLElBQXJCLEdBQTBCNEMsQ0FBQyxDQUFDbUUsS0FBNUIsR0FBa0N1WixDQUFDLENBQUNyZ0IsS0FBdkosRUFBNkppZ0IsQ0FBQyxDQUFDTyxNQUFGLEdBQVNVLENBQUMsQ0FBQ0QsSUFBRixDQUFPSSxZQUFQLEdBQW9CcEIsQ0FBQyxDQUFDN1IsR0FBdEIsR0FBMEJ6TCxDQUFDLENBQUN0QyxNQUE1QixHQUFtQ2dnQixDQUFDLENBQUNHLE1BQTNNLEVBQWtOcUQsQ0FBQyxDQUFDc0YsSUFBRixDQUFPL2EsR0FBUCxJQUFZNlIsQ0FBQyxDQUFDN1IsR0FBRixHQUFNaVMsQ0FBQyxDQUFDalMsR0FBcEIsSUFBeUJ5VixDQUFDLENBQUNzRixJQUFGLENBQU8zSSxNQUFQLElBQWVQLENBQUMsQ0FBQ08sTUFBMUMsSUFBa0RxRCxDQUFDLENBQUNzRixJQUFGLENBQU9wcEIsSUFBUCxJQUFha2dCLENBQUMsQ0FBQ2xnQixJQUFGLEdBQU9zZ0IsQ0FBQyxDQUFDdGdCLElBQXhFLElBQThFOGpCLENBQUMsQ0FBQ3NGLElBQUYsQ0FBT25wQixLQUFQLElBQWNpZ0IsQ0FBQyxDQUFDamdCLEtBQW5ULEVBQXlUO0FBQUMsa0JBQUl1Z0IsQ0FBQyxHQUFDM2hCLENBQUMsQ0FBQzBwQixTQUFSO0FBQUEsa0JBQWtCekgsQ0FBQyxHQUFDamlCLENBQUMsQ0FBQzRxQixVQUF0QjtBQUFpQzNGLGVBQUMsQ0FBQzRELE1BQUYsR0FBUztBQUFDclosbUJBQUcsRUFBQ3lWLENBQUMsQ0FBQ3NGLElBQUYsQ0FBTy9hLEdBQVAsR0FBVzZSLENBQUMsQ0FBQzdSLEdBQWIsR0FBaUJtUyxDQUFqQixHQUFtQkYsQ0FBQyxDQUFDalMsR0FBMUI7QUFBOEJyTyxvQkFBSSxFQUFDOGpCLENBQUMsQ0FBQ3NGLElBQUYsQ0FBT3BwQixJQUFQLEdBQVlrZ0IsQ0FBQyxDQUFDbGdCLElBQWQsR0FBbUI4Z0IsQ0FBbkIsR0FBcUJSLENBQUMsQ0FBQ3RnQjtBQUExRCxlQUFUO0FBQXlFO0FBQUMsV0FBbmxCLEVBQXpnQixFQUErbEMsS0FBSzBwQixJQUFMLENBQVU1RixDQUFWLENBQS9sQyxFQUE0bUMsS0FBS3VELE9BQUwsQ0FBYXNDLE9BQWIsQ0FBcUI3RixDQUFyQixDQUE1bUMsRUFBb29DLEtBQUt1RCxPQUFMLENBQWFqbkIsTUFBYixHQUFvQixDQUFwQixJQUF1QixLQUFLaW5CLE9BQUwsQ0FBYS9DLEdBQWIsRUFBM3BDLEVBQThxQ3psQixDQUFDLElBQUV3bEIsQ0FBQyxFQUFsckMsRUFBcXJDLENBQUMsQ0FBN3JDO0FBQStyQztBQUFDO0FBQXY0RSxLQUFsaEssRUFBMjVPO0FBQUNoakIsU0FBRyxFQUFDLE1BQUw7QUFBWWxCLFdBQUssRUFBQyxlQUFTOGYsQ0FBVCxFQUFXO0FBQUMsWUFBSXBoQixDQUFDLEdBQUMsSUFBTjs7QUFBVyxZQUFHLGVBQWEsT0FBTyxLQUFLaUgsT0FBTCxDQUFhOEQsVUFBcEMsRUFBK0M7QUFBQyxjQUFJc1csQ0FBQyxHQUFDLEVBQU47O0FBQVMsZUFBSSxJQUFJalEsQ0FBUixJQUFhZ1EsQ0FBYixFQUFlO0FBQUNDLGFBQUMsQ0FBQ2pRLENBQUQsQ0FBRCxHQUFLLEVBQUw7O0FBQVEsaUJBQUksSUFBSXJOLENBQVIsSUFBYXFkLENBQUMsQ0FBQ2hRLENBQUQsQ0FBZCxFQUFrQjtBQUFDLG1CQUFJLElBQUlxUSxDQUFDLEdBQUMsQ0FBQyxDQUFQLEVBQVNFLENBQUMsR0FBQyxDQUFmLEVBQWlCQSxDQUFDLEdBQUMsS0FBSzZHLE9BQUwsQ0FBYWpuQixNQUFoQyxFQUF1QyxFQUFFb2dCLENBQXpDLEVBQTJDO0FBQUMsb0JBQUlHLENBQUMsR0FBQyxLQUFLMEcsT0FBTCxDQUFhN0csQ0FBYixDQUFOOztBQUFzQixvQkFBRyxlQUFhLE9BQU9HLENBQUMsQ0FBQzFRLENBQUQsQ0FBckIsSUFBMEIsQ0FBQzhTLENBQUMsQ0FBQ3BDLENBQUMsQ0FBQzFRLENBQUQsQ0FBRCxDQUFLck4sQ0FBTCxDQUFELEVBQVNxZCxDQUFDLENBQUNoUSxDQUFELENBQUQsQ0FBS3JOLENBQUwsQ0FBVCxDQUEvQixFQUFpRDtBQUFDMGQsbUJBQUMsR0FBQyxDQUFDLENBQUg7QUFBSztBQUFNO0FBQUM7O0FBQUFBLGVBQUMsS0FBR0osQ0FBQyxDQUFDalEsQ0FBRCxDQUFELENBQUtyTixDQUFMLElBQVEsQ0FBQyxDQUFaLENBQUQ7QUFBZ0I7QUFBQzs7QUFBQSxjQUFJa2UsQ0FBQyxHQUFDO0FBQUN6UyxlQUFHLEVBQUMsRUFBTDtBQUFRck8sZ0JBQUksRUFBQyxFQUFiO0FBQWdCQyxpQkFBSyxFQUFDLEVBQXRCO0FBQXlCd2dCLGtCQUFNLEVBQUM7QUFBaEMsV0FBTjtBQUFBLGNBQTBDcUIsQ0FBQyxHQUFDLFNBQUZBLENBQUUsQ0FBUzdCLENBQVQsRUFBV0MsQ0FBWCxFQUFhO0FBQUMsZ0JBQUlqUSxDQUFDLEdBQUMsZUFBYSxPQUFPcFIsQ0FBQyxDQUFDd0gsT0FBRixDQUFVa2pCLGFBQXBDO0FBQUEsZ0JBQWtEM21CLENBQUMsR0FBQ3FOLENBQUMsR0FBQ3BSLENBQUMsQ0FBQ3dILE9BQUYsQ0FBVWtqQixhQUFWLENBQXdCSyxHQUF6QixHQUE2QixJQUFsRjs7QUFBdUYsZ0JBQUdobkIsQ0FBQyxLQUFHLENBQUMsQ0FBUixFQUFVO0FBQUMsa0JBQUkwZCxDQUFDLEdBQUMsS0FBSyxDQUFYO0FBQUEsa0JBQWFFLENBQUMsR0FBQyxLQUFLLENBQXBCO0FBQXNCUCxlQUFDLENBQUM1UixHQUFGLElBQU95UyxDQUFDLENBQUN6UyxHQUFGLEdBQU0sQ0FBTixFQUFRaVMsQ0FBQyxHQUFDSixDQUFDLENBQUM3UixHQUFuQixLQUF5QnlTLENBQUMsQ0FBQ0wsTUFBRixHQUFTLENBQVQsRUFBV0gsQ0FBQyxHQUFDLENBQUNKLENBQUMsQ0FBQ08sTUFBekMsR0FBaURSLENBQUMsQ0FBQ2pnQixJQUFGLElBQVE4Z0IsQ0FBQyxDQUFDOWdCLElBQUYsR0FBTyxDQUFQLEVBQVN3Z0IsQ0FBQyxHQUFDTixDQUFDLENBQUNsZ0IsSUFBckIsS0FBNEI4Z0IsQ0FBQyxDQUFDN2dCLEtBQUYsR0FBUSxDQUFSLEVBQVV1Z0IsQ0FBQyxHQUFDLENBQUNOLENBQUMsQ0FBQ2pnQixLQUEzQyxDQUFqRCxFQUFtRzZnQixDQUFDLENBQUN1RixDQUFELENBQUQsR0FBSyxnQkFBY2pWLElBQUksQ0FBQ0UsS0FBTCxDQUFXa1AsQ0FBWCxDQUFkLEdBQTRCLGlCQUE1QixHQUE4Q3BQLElBQUksQ0FBQ0UsS0FBTCxDQUFXZ1AsQ0FBWCxDQUE5QyxHQUE0RCxLQUFwSyxFQUEwSyxrQkFBZ0IrRixDQUFoQixLQUFvQnZGLENBQUMsQ0FBQ3VGLENBQUQsQ0FBRCxJQUFNLGdCQUExQixDQUExSztBQUFzTixhQUF2UCxNQUE0UHBHLENBQUMsQ0FBQzVSLEdBQUYsR0FBTXlTLENBQUMsQ0FBQ3pTLEdBQUYsR0FBTTZSLENBQUMsQ0FBQzdSLEdBQUYsR0FBTSxJQUFsQixHQUF1QnlTLENBQUMsQ0FBQ0wsTUFBRixHQUFTUCxDQUFDLENBQUNPLE1BQUYsR0FBUyxJQUF6QyxFQUE4Q1IsQ0FBQyxDQUFDamdCLElBQUYsR0FBTzhnQixDQUFDLENBQUM5Z0IsSUFBRixHQUFPa2dCLENBQUMsQ0FBQ2xnQixJQUFGLEdBQU8sSUFBckIsR0FBMEI4Z0IsQ0FBQyxDQUFDN2dCLEtBQUYsR0FBUWlnQixDQUFDLENBQUNqZ0IsS0FBRixHQUFRLElBQXhGO0FBQTZGLFdBQTFlO0FBQUEsY0FBMmVpaUIsQ0FBQyxHQUFDLENBQUMsQ0FBOWU7O0FBQWdmLGNBQUcsQ0FBQ2hDLENBQUMsQ0FBQ2tKLElBQUYsQ0FBTy9hLEdBQVAsSUFBWTZSLENBQUMsQ0FBQ2tKLElBQUYsQ0FBTzNJLE1BQXBCLE1BQThCUCxDQUFDLENBQUNrSixJQUFGLENBQU9wcEIsSUFBUCxJQUFha2dCLENBQUMsQ0FBQ2tKLElBQUYsQ0FBT25wQixLQUFsRCxLQUEwRDZnQixDQUFDLENBQUMxUyxRQUFGLEdBQVcsVUFBWCxFQUFzQjBULENBQUMsQ0FBQzVCLENBQUMsQ0FBQ2tKLElBQUgsRUFBUW5KLENBQUMsQ0FBQ21KLElBQVYsQ0FBakYsSUFBa0csQ0FBQ2xKLENBQUMsQ0FBQ21KLFFBQUYsQ0FBV2hiLEdBQVgsSUFBZ0I2UixDQUFDLENBQUNtSixRQUFGLENBQVc1SSxNQUE1QixNQUFzQ1AsQ0FBQyxDQUFDbUosUUFBRixDQUFXcnBCLElBQVgsSUFBaUJrZ0IsQ0FBQyxDQUFDbUosUUFBRixDQUFXcHBCLEtBQWxFLEtBQTBFNmdCLENBQUMsQ0FBQzFTLFFBQUYsR0FBVyxPQUFYLEVBQW1CMFQsQ0FBQyxDQUFDNUIsQ0FBQyxDQUFDbUosUUFBSCxFQUFZcEosQ0FBQyxDQUFDb0osUUFBZCxDQUE5RixJQUF1SCxlQUFhLE9BQU9uSixDQUFDLENBQUN3SCxNQUF0QixJQUE4QnhILENBQUMsQ0FBQ3dILE1BQUYsQ0FBU3JaLEdBQXZDLElBQTRDNlIsQ0FBQyxDQUFDd0gsTUFBRixDQUFTMW5CLElBQXJELEdBQTBELENBQUMsWUFBVTtBQUFDOGdCLGFBQUMsQ0FBQzFTLFFBQUYsR0FBVyxVQUFYO0FBQXNCLGdCQUFJNkIsQ0FBQyxHQUFDcFIsQ0FBQyxDQUFDK3BCLEtBQUYsQ0FBUSxxQkFBUixFQUE4QixZQUFVO0FBQUMscUJBQU9oSSxDQUFDLENBQUMvaEIsQ0FBQyxDQUFDQyxNQUFILENBQVI7QUFBbUIsYUFBNUQsQ0FBTjtBQUFvRThoQixhQUFDLENBQUMvaEIsQ0FBQyxDQUFDaUgsT0FBSCxDQUFELEtBQWVtSyxDQUFmLElBQWtCa1UsQ0FBQyxDQUFDLFlBQVU7QUFBQ3RsQixlQUFDLENBQUNpSCxPQUFGLENBQVU4RCxVQUFWLENBQXFCQyxXQUFyQixDQUFpQ2hMLENBQUMsQ0FBQ2lILE9BQW5DLEdBQTRDbUssQ0FBQyxDQUFDOUosV0FBRixDQUFjdEgsQ0FBQyxDQUFDaUgsT0FBaEIsQ0FBNUM7QUFBcUUsYUFBakYsQ0FBbkIsRUFBc0dnYyxDQUFDLENBQUM1QixDQUFDLENBQUN3SCxNQUFILEVBQVV6SCxDQUFDLENBQUN5SCxNQUFaLENBQXZHLEVBQTJIeEYsQ0FBQyxHQUFDLENBQUMsQ0FBOUg7QUFBZ0ksV0FBck8sRUFBM0QsSUFBb1NwQixDQUFDLENBQUMxUyxRQUFGLEdBQVcsVUFBWCxFQUFzQjBULENBQUMsQ0FBQztBQUFDelQsZUFBRyxFQUFDLENBQUMsQ0FBTjtBQUFRck8sZ0JBQUksRUFBQyxDQUFDO0FBQWQsV0FBRCxFQUFrQmlnQixDQUFDLENBQUNtSixJQUFwQixDQUEzVCxDQUF6TixFQUEraUIsQ0FBQ2xILENBQW5qQixFQUFxakI7QUFBQyxpQkFBSSxJQUFJQyxDQUFDLEdBQUMsQ0FBQyxDQUFQLEVBQVNILENBQUMsR0FBQyxLQUFLbGMsT0FBTCxDQUFhOEQsVUFBNUIsRUFBdUNvWSxDQUFDLElBQUUsTUFBSUEsQ0FBQyxDQUFDelYsUUFBVCxJQUFtQixXQUFTeVYsQ0FBQyxDQUFDalUsT0FBckUsR0FBOEU7QUFBQyxrQkFBRyxhQUFXMlMsZ0JBQWdCLENBQUNzQixDQUFELENBQWhCLENBQW9CNVQsUUFBbEMsRUFBMkM7QUFBQytULGlCQUFDLEdBQUMsQ0FBQyxDQUFIO0FBQUs7QUFBTTs7QUFBQUgsZUFBQyxHQUFDQSxDQUFDLENBQUNwWSxVQUFKO0FBQWU7O0FBQUF1WSxhQUFDLEtBQUcsS0FBS3JjLE9BQUwsQ0FBYThELFVBQWIsQ0FBd0JDLFdBQXhCLENBQW9DLEtBQUsvRCxPQUF6QyxHQUFrRCxLQUFLQSxPQUFMLENBQWF1YSxhQUFiLENBQTJCYSxJQUEzQixDQUFnQy9hLFdBQWhDLENBQTRDLEtBQUtMLE9BQWpELENBQXJELENBQUQ7QUFBaUg7O0FBQUEsY0FBSW1jLENBQUMsR0FBQyxFQUFOO0FBQUEsY0FBU0ssQ0FBQyxHQUFDLENBQUMsQ0FBWjs7QUFBYyxlQUFJLElBQUkxZixDQUFSLElBQWFrZSxDQUFiLEVBQWU7QUFBQyxnQkFBSXlCLENBQUMsR0FBQ3pCLENBQUMsQ0FBQ2xlLENBQUQsQ0FBUDtBQUFBLGdCQUFXb2dCLENBQUMsR0FBQyxLQUFLbGQsT0FBTCxDQUFhZ0YsS0FBYixDQUFtQmxJLENBQW5CLENBQWI7QUFBbUNvZ0IsYUFBQyxLQUFHVCxDQUFKLEtBQVFELENBQUMsR0FBQyxDQUFDLENBQUgsRUFBS0wsQ0FBQyxDQUFDcmYsQ0FBRCxDQUFELEdBQUsyZixDQUFsQjtBQUFxQjs7QUFBQUQsV0FBQyxJQUFFNkIsQ0FBQyxDQUFDLFlBQVU7QUFBQ25ELGFBQUMsQ0FBQ25pQixDQUFDLENBQUNpSCxPQUFGLENBQVVnRixLQUFYLEVBQWlCbVgsQ0FBakIsQ0FBRDtBQUFxQixXQUFqQyxDQUFKO0FBQXVDO0FBQUM7QUFBeHNELEtBQTM1TyxDQUFILENBQVIsRUFBa25TcGpCLENBQXpuUztBQUEyblMsR0FBLzZTLENBQWc3UzBsQixDQUFoN1MsQ0FBemY7O0FBQTQ2VDRDLEdBQUMsQ0FBQ25ELE9BQUYsR0FBVSxFQUFWLEVBQWFqRixDQUFDLENBQUMzUSxRQUFGLEdBQVdtWSxDQUF4Qjs7QUFBMEIsTUFBSXNELENBQUMsR0FBQzdJLENBQUMsQ0FBQ21HLENBQUQsRUFBR3BJLENBQUgsQ0FBUDtBQUFBLE1BQWEyRyxDQUFDLEdBQUMsWUFBVTtBQUFDLGFBQVN6RixDQUFULENBQVdBLENBQVgsRUFBYXBoQixDQUFiLEVBQWU7QUFBQyxVQUFJcWhCLENBQUMsR0FBQyxFQUFOO0FBQUEsVUFBU2pRLENBQUMsR0FBQyxDQUFDLENBQVo7QUFBQSxVQUFjck4sQ0FBQyxHQUFDLENBQUMsQ0FBakI7QUFBQSxVQUFtQjBkLENBQUMsR0FBQyxLQUFLLENBQTFCOztBQUE0QixVQUFHO0FBQUMsYUFBSSxJQUFJRSxDQUFKLEVBQU1HLENBQUMsR0FBQ1YsQ0FBQyxDQUFDMEYsTUFBTSxDQUFDQyxRQUFSLENBQUQsRUFBWixFQUFpQyxFQUFFM1YsQ0FBQyxHQUFDLENBQUN1USxDQUFDLEdBQUNHLENBQUMsQ0FBQ2tGLElBQUYsRUFBSCxFQUFhcmdCLElBQWpCLE1BQXlCMGEsQ0FBQyxDQUFDbmUsSUFBRixDQUFPeWUsQ0FBQyxDQUFDcmdCLEtBQVQsR0FBZ0IsQ0FBQ3RCLENBQUQsSUFBSXFoQixDQUFDLENBQUM5ZixNQUFGLEtBQVd2QixDQUF4RCxDQUFqQyxFQUE0Rm9SLENBQUMsR0FBQyxDQUFDLENBQS9GO0FBQWlHO0FBQWpHO0FBQW1HLE9BQXZHLENBQXVHLE9BQU0yUSxDQUFOLEVBQVE7QUFBQ2hlLFNBQUMsR0FBQyxDQUFDLENBQUgsRUFBSzBkLENBQUMsR0FBQ00sQ0FBUDtBQUFTLE9BQXpILFNBQWdJO0FBQUMsWUFBRztBQUFDLFdBQUMzUSxDQUFELElBQUkwUSxDQUFDLENBQUMsUUFBRCxDQUFMLElBQWlCQSxDQUFDLENBQUMsUUFBRCxDQUFELEVBQWpCO0FBQStCLFNBQW5DLFNBQTBDO0FBQUMsY0FBRy9kLENBQUgsRUFBSyxNQUFNMGQsQ0FBTjtBQUFRO0FBQUM7O0FBQUEsYUFBT0osQ0FBUDtBQUFTOztBQUFBLFdBQU8sVUFBU3JoQixDQUFULEVBQVdxaEIsQ0FBWCxFQUFhO0FBQUMsVUFBRzNHLEtBQUssQ0FBQ3VNLE9BQU4sQ0FBY2puQixDQUFkLENBQUgsRUFBb0IsT0FBT0EsQ0FBUDtBQUFTLFVBQUc4bUIsTUFBTSxDQUFDQyxRQUFQLElBQW1CcEQsTUFBTSxDQUFDM2pCLENBQUQsQ0FBNUIsRUFBZ0MsT0FBT29oQixDQUFDLENBQUNwaEIsQ0FBRCxFQUFHcWhCLENBQUgsQ0FBUjtBQUFjLFlBQU0sSUFBSUMsU0FBSixDQUFjLHNEQUFkLENBQU47QUFBNEUsS0FBNUs7QUFBNkssR0FBdmEsRUFBZjtBQUFBLE1BQXliaUcsQ0FBQyxHQUFDckgsQ0FBQyxDQUFDNEYsS0FBN2I7QUFBQSxNQUFtY2hFLENBQUMsR0FBQ3lGLENBQUMsQ0FBQ3RCLFNBQXZjO0FBQUEsTUFBaWQ5RCxDQUFDLEdBQUNvRixDQUFDLENBQUNyakIsTUFBcmQ7QUFBQSxNQUE0ZHVmLENBQUMsR0FBQzhELENBQUMsQ0FBQ2pCLGFBQWhlO0FBQUEsTUFBOGVoQixDQUFDLEdBQUNpQyxDQUFDLENBQUNoQixLQUFsZjtBQUFBLE1BQXdmem5CLENBQUMsR0FBQyxDQUFDLE1BQUQsRUFBUSxLQUFSLEVBQWMsT0FBZCxFQUFzQixRQUF0QixDQUExZjs7QUFBMGhCb2hCLEdBQUMsQ0FBQ2lGLE9BQUYsQ0FBVWppQixJQUFWLENBQWU7QUFBQ3FNLFlBQVEsRUFBQyxrQkFBUzZSLENBQVQsRUFBVztBQUFDLFVBQUlwaEIsQ0FBQyxHQUFDLElBQU47QUFBQSxVQUFXcWhCLENBQUMsR0FBQ0QsQ0FBQyxDQUFDNVIsR0FBZjtBQUFBLFVBQW1CNEIsQ0FBQyxHQUFDZ1EsQ0FBQyxDQUFDamdCLElBQXZCO0FBQUEsVUFBNEI0QyxDQUFDLEdBQUNxZCxDQUFDLENBQUMySCxnQkFBaEM7QUFBaUQsVUFBRyxDQUFDLEtBQUt2aEIsT0FBTCxDQUFheWpCLFdBQWpCLEVBQTZCLE9BQU0sQ0FBQyxDQUFQO0FBQVMsVUFBSXhKLENBQUMsR0FBQyxLQUFLc0ksS0FBTCxDQUFXLGdCQUFYLEVBQTRCLFlBQVU7QUFBQyxlQUFPakksQ0FBQyxDQUFDOWhCLENBQUMsQ0FBQ2lILE9BQUgsQ0FBUjtBQUFvQixPQUEzRCxDQUFOO0FBQUEsVUFBbUUwYSxDQUFDLEdBQUNGLENBQUMsQ0FBQ2hnQixNQUF2RTtBQUFBLFVBQThFc2dCLENBQUMsR0FBQ04sQ0FBQyxDQUFDdlosS0FBbEY7O0FBQXdGLFVBQUcsTUFBSTZaLENBQUosSUFBTyxNQUFJSixDQUFYLElBQWMsZUFBYSxPQUFPLEtBQUtxSSxRQUExQyxFQUFtRDtBQUFDLFlBQUkvSCxDQUFDLEdBQUMsS0FBSytILFFBQVg7QUFBb0JqSSxTQUFDLEdBQUNFLENBQUMsQ0FBQy9aLEtBQUosRUFBVXlaLENBQUMsR0FBQ00sQ0FBQyxDQUFDeGdCLE1BQWQ7QUFBcUI7O0FBQUEsVUFBSXdoQixDQUFDLEdBQUMsS0FBSzhHLEtBQUwsQ0FBVyxlQUFYLEVBQTJCLFlBQVU7QUFBQyxlQUFPL3BCLENBQUMsQ0FBQ2lxQixlQUFGLEVBQVA7QUFBMkIsT0FBakUsQ0FBTjtBQUFBLFVBQXlFNUcsQ0FBQyxHQUFDSixDQUFDLENBQUN4aEIsTUFBN0U7QUFBQSxVQUFvRjZoQixDQUFDLEdBQUNMLENBQUMsQ0FBQy9hLEtBQXhGO0FBQUEsVUFBOEZpYixDQUFDLEdBQUMsQ0FBQyxLQUFLK0YsUUFBTCxDQUFjLFFBQWQsQ0FBRCxFQUF5QixLQUFLQSxRQUFMLENBQWMsZUFBZCxDQUF6QixDQUFoRztBQUF5SixXQUFLMWhCLE9BQUwsQ0FBYXlqQixXQUFiLENBQXlCN2EsT0FBekIsQ0FBaUMsVUFBU2dSLENBQVQsRUFBVztBQUFDLFlBQUlwaEIsQ0FBQyxHQUFDb2hCLENBQUMsQ0FBQzhKLGdCQUFSO0FBQUEsWUFBeUI3SixDQUFDLEdBQUNELENBQUMsQ0FBQytKLFdBQTdCO0FBQXlDbnJCLFNBQUMsSUFBRW1qQixDQUFDLENBQUNqZ0IsSUFBRixDQUFPbEQsQ0FBUCxDQUFILEVBQWFxaEIsQ0FBQyxJQUFFOEIsQ0FBQyxDQUFDamdCLElBQUYsQ0FBT21lLENBQVAsQ0FBaEI7QUFBMEIsT0FBaEgsR0FBa0g4QixDQUFDLENBQUMvUyxPQUFGLENBQVUsVUFBU2dSLENBQVQsRUFBVztBQUFDLFNBQUMsTUFBRCxFQUFRLEtBQVIsRUFBYyxPQUFkLEVBQXNCLFFBQXRCLEVBQWdDaFIsT0FBaEMsQ0FBd0MsVUFBU3BRLENBQVQsRUFBVztBQUFDbWpCLFdBQUMsQ0FBQ2pnQixJQUFGLENBQU9rZSxDQUFDLEdBQUMsR0FBRixHQUFNcGhCLENBQWI7QUFBZ0IsU0FBcEU7QUFBc0UsT0FBNUYsQ0FBbEg7QUFBZ04sVUFBSW9qQixDQUFDLEdBQUMsRUFBTjtBQUFBLFVBQVNNLENBQUMsR0FBQ3ZCLENBQUMsQ0FBQyxFQUFELEVBQUlwZSxDQUFKLENBQVo7QUFBQSxVQUFtQm1nQixDQUFDLEdBQUMvQixDQUFDLENBQUMsRUFBRCxFQUFJLEtBQUtpSCxVQUFULENBQXRCO0FBQTJDLGFBQU8sS0FBSzVoQixPQUFMLENBQWF5akIsV0FBYixDQUF5QjdhLE9BQXpCLENBQWlDLFVBQVNnUixDQUFULEVBQVc7QUFBQyxZQUFJSyxDQUFDLEdBQUNMLENBQUMsQ0FBQ2dLLEVBQVI7QUFBQSxZQUFXdEosQ0FBQyxHQUFDVixDQUFDLENBQUNnSSxVQUFmO0FBQUEsWUFBMEJuSCxDQUFDLEdBQUNiLENBQUMsQ0FBQ2lLLEdBQTlCO0FBQWtDLHVCQUFhLE9BQU92SixDQUFwQixLQUF3QkEsQ0FBQyxHQUFDLEVBQTFCO0FBQThCLFlBQUlLLENBQUMsR0FBQyxLQUFLLENBQVg7QUFBQSxZQUFhYyxDQUFDLEdBQUMsS0FBSyxDQUFwQjs7QUFBc0IsWUFBR25CLENBQUMsQ0FBQ2xSLE9BQUYsQ0FBVSxHQUFWLEtBQWdCLENBQW5CLEVBQXFCO0FBQUMsY0FBSXVTLENBQUMsR0FBQ3JCLENBQUMsQ0FBQzlHLEtBQUYsQ0FBUSxHQUFSLENBQU47QUFBQSxjQUFtQnlJLENBQUMsR0FBQ29ELENBQUMsQ0FBQzFELENBQUQsRUFBRyxDQUFILENBQXRCO0FBQTRCRixXQUFDLEdBQUNRLENBQUMsQ0FBQyxDQUFELENBQUgsRUFBT3RCLENBQUMsR0FBQ3NCLENBQUMsQ0FBQyxDQUFELENBQVY7QUFBYyxTQUFoRSxNQUFxRXRCLENBQUMsR0FBQ2MsQ0FBQyxHQUFDbkIsQ0FBSjs7QUFBTSxZQUFJcUMsQ0FBQyxHQUFDTyxDQUFDLENBQUMxa0IsQ0FBRCxFQUFHeWhCLENBQUgsQ0FBUDtBQUFhLFNBQUMsYUFBV3dCLENBQVgsSUFBYyxXQUFTQSxDQUF4QixNQUE2QjVCLENBQUMsR0FBQzhDLENBQUMsQ0FBQyxDQUFELENBQUgsSUFBUSxVQUFRVCxDQUFDLENBQUNsVSxHQUFsQixLQUF3QjZSLENBQUMsSUFBRWdDLENBQUgsRUFBS0ssQ0FBQyxDQUFDbFUsR0FBRixHQUFNLFFBQW5DLEdBQTZDNlIsQ0FBQyxHQUFDTSxDQUFGLEdBQUl3QyxDQUFDLENBQUMsQ0FBRCxDQUFMLElBQVUsYUFBV1QsQ0FBQyxDQUFDbFUsR0FBdkIsS0FBNkI2UixDQUFDLElBQUVnQyxDQUFILEVBQUtLLENBQUMsQ0FBQ2xVLEdBQUYsR0FBTSxLQUF4QyxDQUExRSxHQUEwSCxlQUFheVQsQ0FBYixLQUFpQixVQUFRUyxDQUFDLENBQUNsVSxHQUFWLEtBQWdCLGFBQVcwVSxDQUFDLENBQUMxVSxHQUFiLElBQWtCNlIsQ0FBQyxHQUFDOEMsQ0FBQyxDQUFDLENBQUQsQ0FBckIsSUFBMEI5QyxDQUFDLElBQUVnQyxDQUFILEVBQUtLLENBQUMsQ0FBQ2xVLEdBQUYsR0FBTSxRQUFYLEVBQW9CNlIsQ0FBQyxJQUFFTSxDQUF2QixFQUF5QnVDLENBQUMsQ0FBQzFVLEdBQUYsR0FBTSxLQUF6RCxJQUFnRSxVQUFRMFUsQ0FBQyxDQUFDMVUsR0FBVixJQUFlNlIsQ0FBQyxHQUFDTSxDQUFGLEdBQUl3QyxDQUFDLENBQUMsQ0FBRCxDQUFwQixJQUF5QjlDLENBQUMsSUFBRU0sQ0FBQyxHQUFDMEIsQ0FBSixDQUFELElBQVNjLENBQUMsQ0FBQyxDQUFELENBQW5DLEtBQXlDOUMsQ0FBQyxJQUFFTSxDQUFDLEdBQUMwQixDQUFMLEVBQU9LLENBQUMsQ0FBQ2xVLEdBQUYsR0FBTSxRQUFiLEVBQXNCMFUsQ0FBQyxDQUFDMVUsR0FBRixHQUFNLFFBQXJFLENBQWhGLEdBQWdLLGFBQVdrVSxDQUFDLENBQUNsVSxHQUFiLEtBQW1CLFVBQVEwVSxDQUFDLENBQUMxVSxHQUFWLElBQWU2UixDQUFDLEdBQUNNLENBQUYsR0FBSXdDLENBQUMsQ0FBQyxDQUFELENBQXBCLElBQXlCOUMsQ0FBQyxJQUFFZ0MsQ0FBSCxFQUFLSyxDQUFDLENBQUNsVSxHQUFGLEdBQU0sS0FBWCxFQUFpQjZSLENBQUMsSUFBRU0sQ0FBcEIsRUFBc0J1QyxDQUFDLENBQUMxVSxHQUFGLEdBQU0sUUFBckQsSUFBK0QsYUFBVzBVLENBQUMsQ0FBQzFVLEdBQWIsSUFBa0I2UixDQUFDLEdBQUM4QyxDQUFDLENBQUMsQ0FBRCxDQUFyQixJQUEwQjlDLENBQUMsSUFBRSxJQUFFTSxDQUFGLEdBQUkwQixDQUFOLENBQUQsSUFBV2MsQ0FBQyxDQUFDLENBQUQsQ0FBdEMsS0FBNEM5QyxDQUFDLElBQUVNLENBQUMsR0FBQzBCLENBQUwsRUFBT0ssQ0FBQyxDQUFDbFUsR0FBRixHQUFNLEtBQWIsRUFBbUIwVSxDQUFDLENBQUMxVSxHQUFGLEdBQU0sS0FBckUsQ0FBbEYsQ0FBaEssRUFBK1QsYUFBV2tVLENBQUMsQ0FBQ2xVLEdBQWIsS0FBbUI2UixDQUFDLEdBQUNNLENBQUYsR0FBSXdDLENBQUMsQ0FBQyxDQUFELENBQUwsSUFBVSxVQUFRRCxDQUFDLENBQUMxVSxHQUFwQixJQUF5QjZSLENBQUMsSUFBRU0sQ0FBSCxFQUFLdUMsQ0FBQyxDQUFDMVUsR0FBRixHQUFNLFFBQXBDLElBQThDNlIsQ0FBQyxHQUFDOEMsQ0FBQyxDQUFDLENBQUQsQ0FBSCxJQUFRLGFBQVdELENBQUMsQ0FBQzFVLEdBQXJCLEtBQTJCNlIsQ0FBQyxJQUFFTSxDQUFILEVBQUt1QyxDQUFDLENBQUMxVSxHQUFGLEdBQU0sS0FBdEMsQ0FBakUsQ0FBaFYsQ0FBMUgsRUFBMGpCLENBQUMsYUFBVzJTLENBQVgsSUFBYyxXQUFTQSxDQUF4QixNQUE2Qi9RLENBQUMsR0FBQytTLENBQUMsQ0FBQyxDQUFELENBQUgsSUFBUSxXQUFTVCxDQUFDLENBQUN2aUIsSUFBbkIsS0FBMEJpUSxDQUFDLElBQUVrUyxDQUFILEVBQUtJLENBQUMsQ0FBQ3ZpQixJQUFGLEdBQU8sT0FBdEMsR0FBK0NpUSxDQUFDLEdBQUMyUSxDQUFGLEdBQUlvQyxDQUFDLENBQUMsQ0FBRCxDQUFMLElBQVUsWUFBVVQsQ0FBQyxDQUFDdmlCLElBQXRCLEtBQTZCaVEsQ0FBQyxJQUFFa1MsQ0FBSCxFQUFLSSxDQUFDLENBQUN2aUIsSUFBRixHQUFPLE1BQXpDLENBQTVFLENBQTFqQixFQUF3ckIsZUFBYWdoQixDQUFiLEtBQWlCL1EsQ0FBQyxHQUFDK1MsQ0FBQyxDQUFDLENBQUQsQ0FBSCxJQUFRLFdBQVNULENBQUMsQ0FBQ3ZpQixJQUFuQixHQUF3QixZQUFVK2lCLENBQUMsQ0FBQy9pQixJQUFaLElBQWtCaVEsQ0FBQyxJQUFFa1MsQ0FBSCxFQUFLSSxDQUFDLENBQUN2aUIsSUFBRixHQUFPLE9BQVosRUFBb0JpUSxDQUFDLElBQUUyUSxDQUF2QixFQUF5Qm1DLENBQUMsQ0FBQy9pQixJQUFGLEdBQU8sTUFBbEQsSUFBMEQsV0FBUytpQixDQUFDLENBQUMvaUIsSUFBWCxLQUFrQmlRLENBQUMsSUFBRWtTLENBQUgsRUFBS0ksQ0FBQyxDQUFDdmlCLElBQUYsR0FBTyxPQUFaLEVBQW9CaVEsQ0FBQyxJQUFFMlEsQ0FBdkIsRUFBeUJtQyxDQUFDLENBQUMvaUIsSUFBRixHQUFPLE9BQWxELENBQWxGLEdBQTZJaVEsQ0FBQyxHQUFDMlEsQ0FBRixHQUFJb0MsQ0FBQyxDQUFDLENBQUQsQ0FBTCxJQUFVLFlBQVVULENBQUMsQ0FBQ3ZpQixJQUF0QixHQUEyQixXQUFTK2lCLENBQUMsQ0FBQy9pQixJQUFYLElBQWlCaVEsQ0FBQyxJQUFFa1MsQ0FBSCxFQUFLSSxDQUFDLENBQUN2aUIsSUFBRixHQUFPLE1BQVosRUFBbUJpUSxDQUFDLElBQUUyUSxDQUF0QixFQUF3Qm1DLENBQUMsQ0FBQy9pQixJQUFGLEdBQU8sT0FBaEQsSUFBeUQsWUFBVStpQixDQUFDLENBQUMvaUIsSUFBWixLQUFtQmlRLENBQUMsSUFBRWtTLENBQUgsRUFBS0ksQ0FBQyxDQUFDdmlCLElBQUYsR0FBTyxNQUFaLEVBQW1CaVEsQ0FBQyxJQUFFMlEsQ0FBdEIsRUFBd0JtQyxDQUFDLENBQUMvaUIsSUFBRixHQUFPLE1BQWxELENBQXBGLEdBQThJLGFBQVd1aUIsQ0FBQyxDQUFDdmlCLElBQWIsS0FBb0JpUSxDQUFDLEdBQUMyUSxDQUFGLEdBQUlvQyxDQUFDLENBQUMsQ0FBRCxDQUFMLElBQVUsV0FBU0QsQ0FBQyxDQUFDL2lCLElBQXJCLElBQTJCaVEsQ0FBQyxJQUFFMlEsQ0FBSCxFQUFLbUMsQ0FBQyxDQUFDL2lCLElBQUYsR0FBTyxPQUF2QyxJQUFnRGlRLENBQUMsR0FBQytTLENBQUMsQ0FBQyxDQUFELENBQUgsSUFBUSxZQUFVRCxDQUFDLENBQUMvaUIsSUFBcEIsS0FBMkJpUSxDQUFDLElBQUUyUSxDQUFILEVBQUttQyxDQUFDLENBQUMvaUIsSUFBRixHQUFPLE1BQXZDLENBQXBFLENBQTVTLENBQXhyQixFQUF5bEMsQ0FBQyxjQUFZOGhCLENBQVosSUFBZSxXQUFTQSxDQUF6QixNQUE4QjVCLENBQUMsR0FBQzhDLENBQUMsQ0FBQyxDQUFELENBQUgsSUFBUSxhQUFXRCxDQUFDLENBQUMxVSxHQUFyQixLQUEyQjZSLENBQUMsSUFBRU0sQ0FBSCxFQUFLdUMsQ0FBQyxDQUFDMVUsR0FBRixHQUFNLEtBQXRDLEdBQTZDNlIsQ0FBQyxHQUFDTSxDQUFGLEdBQUl3QyxDQUFDLENBQUMsQ0FBRCxDQUFMLElBQVUsVUFBUUQsQ0FBQyxDQUFDMVUsR0FBcEIsS0FBMEI2UixDQUFDLElBQUVNLENBQUgsRUFBS3VDLENBQUMsQ0FBQzFVLEdBQUYsR0FBTSxRQUFyQyxDQUEzRSxDQUF6bEMsRUFBb3RDLENBQUMsY0FBWTJTLENBQVosSUFBZSxXQUFTQSxDQUF6QixNQUE4Qi9RLENBQUMsR0FBQytTLENBQUMsQ0FBQyxDQUFELENBQUgsS0FBUyxZQUFVRCxDQUFDLENBQUMvaUIsSUFBWixJQUFrQmlRLENBQUMsSUFBRTJRLENBQUgsRUFBS21DLENBQUMsQ0FBQy9pQixJQUFGLEdBQU8sTUFBOUIsSUFBc0MsYUFBVytpQixDQUFDLENBQUMvaUIsSUFBYixLQUFvQmlRLENBQUMsSUFBRTJRLENBQUMsR0FBQyxDQUFMLEVBQU9tQyxDQUFDLENBQUMvaUIsSUFBRixHQUFPLE1BQWxDLENBQS9DLEdBQTBGaVEsQ0FBQyxHQUFDMlEsQ0FBRixHQUFJb0MsQ0FBQyxDQUFDLENBQUQsQ0FBTCxLQUFXLFdBQVNELENBQUMsQ0FBQy9pQixJQUFYLElBQWlCaVEsQ0FBQyxJQUFFMlEsQ0FBSCxFQUFLbUMsQ0FBQyxDQUFDL2lCLElBQUYsR0FBTyxPQUE3QixJQUFzQyxhQUFXK2lCLENBQUMsQ0FBQy9pQixJQUFiLEtBQW9CaVEsQ0FBQyxJQUFFMlEsQ0FBQyxHQUFDLENBQUwsRUFBT21DLENBQUMsQ0FBQy9pQixJQUFGLEdBQU8sT0FBbEMsQ0FBakQsQ0FBeEgsQ0FBcHRDLEVBQTA2QyxZQUFVLE9BQU84Z0IsQ0FBakIsR0FBbUJBLENBQUMsR0FBQ0EsQ0FBQyxDQUFDakgsS0FBRixDQUFRLEdBQVIsRUFBYXFFLEdBQWIsQ0FBaUIsVUFBUytCLENBQVQsRUFBVztBQUFDLGlCQUFPQSxDQUFDLENBQUNwWCxJQUFGLEVBQVA7QUFBZ0IsU0FBN0MsQ0FBckIsR0FBb0VpWSxDQUFDLEtBQUcsQ0FBQyxDQUFMLEtBQVNBLENBQUMsR0FBQyxDQUFDLEtBQUQsRUFBTyxNQUFQLEVBQWMsT0FBZCxFQUFzQixRQUF0QixDQUFYLENBQTkrQyxFQUEwaERBLENBQUMsR0FBQ0EsQ0FBQyxJQUFFLEVBQS9oRDtBQUFraUQsWUFBSXNDLENBQUMsR0FBQyxFQUFOO0FBQUEsWUFBU0UsQ0FBQyxHQUFDLEVBQVg7QUFBY3BELFNBQUMsR0FBQzhDLENBQUMsQ0FBQyxDQUFELENBQUgsS0FBU2xDLENBQUMsQ0FBQ3JSLE9BQUYsQ0FBVSxLQUFWLEtBQWtCLENBQWxCLElBQXFCeVEsQ0FBQyxHQUFDOEMsQ0FBQyxDQUFDLENBQUQsQ0FBSCxFQUFPSSxDQUFDLENBQUNyaEIsSUFBRixDQUFPLEtBQVAsQ0FBNUIsSUFBMkN1aEIsQ0FBQyxDQUFDdmhCLElBQUYsQ0FBTyxLQUFQLENBQXBELEdBQW1FbWUsQ0FBQyxHQUFDTSxDQUFGLEdBQUl3QyxDQUFDLENBQUMsQ0FBRCxDQUFMLEtBQVdsQyxDQUFDLENBQUNyUixPQUFGLENBQVUsUUFBVixLQUFxQixDQUFyQixJQUF3QnlRLENBQUMsR0FBQzhDLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS3hDLENBQVAsRUFBUzRDLENBQUMsQ0FBQ3JoQixJQUFGLENBQU8sUUFBUCxDQUFqQyxJQUFtRHVoQixDQUFDLENBQUN2aEIsSUFBRixDQUFPLFFBQVAsQ0FBOUQsQ0FBbkUsRUFBbUprTyxDQUFDLEdBQUMrUyxDQUFDLENBQUMsQ0FBRCxDQUFILEtBQVNsQyxDQUFDLENBQUNyUixPQUFGLENBQVUsTUFBVixLQUFtQixDQUFuQixJQUFzQlEsQ0FBQyxHQUFDK1MsQ0FBQyxDQUFDLENBQUQsQ0FBSCxFQUFPSSxDQUFDLENBQUNyaEIsSUFBRixDQUFPLE1BQVAsQ0FBN0IsSUFBNkN1aEIsQ0FBQyxDQUFDdmhCLElBQUYsQ0FBTyxNQUFQLENBQXRELENBQW5KLEVBQXlOa08sQ0FBQyxHQUFDMlEsQ0FBRixHQUFJb0MsQ0FBQyxDQUFDLENBQUQsQ0FBTCxLQUFXbEMsQ0FBQyxDQUFDclIsT0FBRixDQUFVLE9BQVYsS0FBb0IsQ0FBcEIsSUFBdUJRLENBQUMsR0FBQytTLENBQUMsQ0FBQyxDQUFELENBQUQsR0FBS3BDLENBQVAsRUFBU3dDLENBQUMsQ0FBQ3JoQixJQUFGLENBQU8sT0FBUCxDQUFoQyxJQUFpRHVoQixDQUFDLENBQUN2aEIsSUFBRixDQUFPLE9BQVAsQ0FBNUQsQ0FBek4sRUFBc1NxaEIsQ0FBQyxDQUFDaGpCLE1BQUYsSUFBVSxDQUFDLFlBQVU7QUFBQyxjQUFJNmYsQ0FBQyxHQUFDLEtBQUssQ0FBWDtBQUFhQSxXQUFDLEdBQUMsZUFBYSxPQUFPcGhCLENBQUMsQ0FBQ3dILE9BQUYsQ0FBVTJqQixXQUE5QixHQUEwQ25yQixDQUFDLENBQUN3SCxPQUFGLENBQVUyakIsV0FBcEQsR0FBZ0VuckIsQ0FBQyxDQUFDa3BCLFFBQUYsQ0FBVyxRQUFYLENBQWxFLEVBQXVGOUYsQ0FBQyxDQUFDbGdCLElBQUYsQ0FBT2tlLENBQVAsQ0FBdkYsRUFBaUdtRCxDQUFDLENBQUNuVSxPQUFGLENBQVUsVUFBU3BRLENBQVQsRUFBVztBQUFDb2pCLGFBQUMsQ0FBQ2xnQixJQUFGLENBQU9rZSxDQUFDLEdBQUMsR0FBRixHQUFNcGhCLENBQWI7QUFBZ0IsV0FBdEMsQ0FBakc7QUFBeUksU0FBakssRUFBalQsRUFBcWR5a0IsQ0FBQyxDQUFDbGpCLE1BQUYsSUFBVSxDQUFDLFlBQVU7QUFBQyxjQUFJNmYsQ0FBQyxHQUFDLEtBQUssQ0FBWDtBQUFhQSxXQUFDLEdBQUMsZUFBYSxPQUFPcGhCLENBQUMsQ0FBQ3dILE9BQUYsQ0FBVTBqQixnQkFBOUIsR0FBK0NsckIsQ0FBQyxDQUFDd0gsT0FBRixDQUFVMGpCLGdCQUF6RCxHQUEwRWxyQixDQUFDLENBQUNrcEIsUUFBRixDQUFXLGVBQVgsQ0FBNUUsRUFBd0c5RixDQUFDLENBQUNsZ0IsSUFBRixDQUFPa2UsQ0FBUCxDQUF4RyxFQUFrSHFELENBQUMsQ0FBQ3JVLE9BQUYsQ0FBVSxVQUFTcFEsQ0FBVCxFQUFXO0FBQUNvakIsYUFBQyxDQUFDbGdCLElBQUYsQ0FBT2tlLENBQUMsR0FBQyxHQUFGLEdBQU1waEIsQ0FBYjtBQUFnQixXQUF0QyxDQUFsSDtBQUEwSixTQUFsTCxFQUFoZSxFQUFxcEIsQ0FBQ3VrQixDQUFDLENBQUMzVCxPQUFGLENBQVUsTUFBVixLQUFtQixDQUFuQixJQUFzQjJULENBQUMsQ0FBQzNULE9BQUYsQ0FBVSxPQUFWLEtBQW9CLENBQTNDLE1BQWdEc1QsQ0FBQyxDQUFDL2lCLElBQUYsR0FBT3VpQixDQUFDLENBQUN2aUIsSUFBRixHQUFPLENBQUMsQ0FBL0QsQ0FBcnBCLEVBQXV0QixDQUFDb2pCLENBQUMsQ0FBQzNULE9BQUYsQ0FBVSxLQUFWLEtBQWtCLENBQWxCLElBQXFCMlQsQ0FBQyxDQUFDM1QsT0FBRixDQUFVLFFBQVYsS0FBcUIsQ0FBM0MsTUFBZ0RzVCxDQUFDLENBQUMxVSxHQUFGLEdBQU1rVSxDQUFDLENBQUNsVSxHQUFGLEdBQU0sQ0FBQyxDQUE3RCxDQUF2dEIsRUFBdXhCLENBQUNrVSxDQUFDLENBQUNsVSxHQUFGLEtBQVF6TCxDQUFDLENBQUN5TCxHQUFWLElBQWVrVSxDQUFDLENBQUN2aUIsSUFBRixLQUFTNEMsQ0FBQyxDQUFDNUMsSUFBMUIsSUFBZ0MraUIsQ0FBQyxDQUFDMVUsR0FBRixLQUFReFAsQ0FBQyxDQUFDb3BCLFVBQUYsQ0FBYTVaLEdBQXJELElBQTBEMFUsQ0FBQyxDQUFDL2lCLElBQUYsS0FBU25CLENBQUMsQ0FBQ29wQixVQUFGLENBQWFqb0IsSUFBakYsTUFBeUZuQixDQUFDLENBQUM4cEIsbUJBQUYsQ0FBc0I1RixDQUF0QixFQUF3QlIsQ0FBeEIsR0FBMkIxakIsQ0FBQyxDQUFDc3JCLE9BQUYsQ0FBVSxRQUFWLEVBQW1CO0FBQUNsQyxvQkFBVSxFQUFDbEYsQ0FBWjtBQUFjNkUsMEJBQWdCLEVBQUNyRjtBQUEvQixTQUFuQixDQUFwSCxDQUF2eEI7QUFBazhCLE9BQTdzRixHQUErc0Y0QixDQUFDLENBQUMsWUFBVTtBQUFDdGxCLFNBQUMsQ0FBQ3dILE9BQUYsQ0FBVTJoQixnQkFBVixLQUE2QixDQUFDLENBQTlCLElBQWlDMUYsQ0FBQyxDQUFDempCLENBQUMsQ0FBQ0MsTUFBSCxFQUFVbWpCLENBQVYsRUFBWUQsQ0FBWixDQUFsQyxFQUFpRE0sQ0FBQyxDQUFDempCLENBQUMsQ0FBQ2lILE9BQUgsRUFBV21jLENBQVgsRUFBYUQsQ0FBYixDQUFsRDtBQUFrRSxPQUE5RSxDQUFodEYsRUFBZ3lGO0FBQUMzVCxXQUFHLEVBQUM2UixDQUFMO0FBQU9sZ0IsWUFBSSxFQUFDaVE7QUFBWixPQUF2eUY7QUFBc3pGO0FBQTUrRyxHQUFmO0FBQTgvRyxNQUFJbVcsQ0FBQyxHQUFDckgsQ0FBQyxDQUFDNEYsS0FBUjtBQUFBLE1BQWNoRSxDQUFDLEdBQUN5RixDQUFDLENBQUN0QixTQUFsQjtBQUFBLE1BQTRCeEMsQ0FBQyxHQUFDOEQsQ0FBQyxDQUFDakIsYUFBaEM7QUFBQSxNQUE4Q2hCLENBQUMsR0FBQ2lDLENBQUMsQ0FBQ2hCLEtBQWxEO0FBQXdEckcsR0FBQyxDQUFDaUYsT0FBRixDQUFVamlCLElBQVYsQ0FBZTtBQUFDcU0sWUFBUSxFQUFDLGtCQUFTNlIsQ0FBVCxFQUFXO0FBQUMsVUFBSXBoQixDQUFDLEdBQUMsSUFBTjtBQUFBLFVBQVdxaEIsQ0FBQyxHQUFDRCxDQUFDLENBQUM1UixHQUFmO0FBQUEsVUFBbUI0QixDQUFDLEdBQUNnUSxDQUFDLENBQUNqZ0IsSUFBdkI7QUFBQSxVQUE0QjRDLENBQUMsR0FBQyxLQUFLZ21CLEtBQUwsQ0FBVyxnQkFBWCxFQUE0QixZQUFVO0FBQUMsZUFBT2pJLENBQUMsQ0FBQzloQixDQUFDLENBQUNpSCxPQUFILENBQVI7QUFBb0IsT0FBM0QsQ0FBOUI7QUFBQSxVQUEyRndhLENBQUMsR0FBQzFkLENBQUMsQ0FBQ3RDLE1BQS9GO0FBQUEsVUFBc0drZ0IsQ0FBQyxHQUFDNWQsQ0FBQyxDQUFDbUUsS0FBMUc7QUFBQSxVQUFnSDZaLENBQUMsR0FBQyxLQUFLa0ksZUFBTCxFQUFsSDtBQUFBLFVBQXlJaEksQ0FBQyxHQUFDWixDQUFDLEdBQUNJLENBQTdJO0FBQUEsVUFBK0lVLENBQUMsR0FBQy9RLENBQUMsR0FBQ3VRLENBQW5KO0FBQUEsVUFBcUpzQixDQUFDLEdBQUMsRUFBdko7QUFBMEo1QixPQUFDLElBQUVVLENBQUMsQ0FBQ0gsTUFBTCxJQUFhSyxDQUFDLElBQUVGLENBQUMsQ0FBQ3ZTLEdBQWxCLElBQXVCLENBQUMsTUFBRCxFQUFRLE9BQVIsRUFBaUJZLE9BQWpCLENBQXlCLFVBQVNnUixDQUFULEVBQVc7QUFBQyxZQUFJcGhCLENBQUMsR0FBQytoQixDQUFDLENBQUNYLENBQUQsQ0FBUDtBQUFXLFNBQUNwaEIsQ0FBQyxLQUFHb1IsQ0FBSixJQUFPcFIsQ0FBQyxLQUFHbWlCLENBQVosS0FBZ0JjLENBQUMsQ0FBQy9mLElBQUYsQ0FBT2tlLENBQVAsQ0FBaEI7QUFBMEIsT0FBMUUsQ0FBdkIsRUFBbUdoUSxDQUFDLElBQUUyUSxDQUFDLENBQUMzZ0IsS0FBTCxJQUFZK2dCLENBQUMsSUFBRUosQ0FBQyxDQUFDNWdCLElBQWpCLElBQXVCLENBQUMsS0FBRCxFQUFPLFFBQVAsRUFBaUJpUCxPQUFqQixDQUF5QixVQUFTZ1IsQ0FBVCxFQUFXO0FBQUMsWUFBSXBoQixDQUFDLEdBQUMraEIsQ0FBQyxDQUFDWCxDQUFELENBQVA7QUFBVyxTQUFDcGhCLENBQUMsS0FBR3FoQixDQUFKLElBQU9yaEIsQ0FBQyxLQUFHaWlCLENBQVosS0FBZ0JnQixDQUFDLENBQUMvZixJQUFGLENBQU9rZSxDQUFQLENBQWhCO0FBQTBCLE9BQTFFLENBQTFIO0FBQXNNLFVBQUlpQyxDQUFDLEdBQUMsRUFBTjtBQUFBLFVBQVNDLENBQUMsR0FBQyxFQUFYO0FBQUEsVUFBY0gsQ0FBQyxHQUFDLENBQUMsTUFBRCxFQUFRLEtBQVIsRUFBYyxPQUFkLEVBQXNCLFFBQXRCLENBQWhCO0FBQWdELGFBQU9FLENBQUMsQ0FBQ25nQixJQUFGLENBQU8sS0FBS2dtQixRQUFMLENBQWMsU0FBZCxDQUFQLEdBQWlDL0YsQ0FBQyxDQUFDL1MsT0FBRixDQUFVLFVBQVNnUixDQUFULEVBQVc7QUFBQ2lDLFNBQUMsQ0FBQ25nQixJQUFGLENBQU9sRCxDQUFDLENBQUNrcEIsUUFBRixDQUFXLFNBQVgsSUFBc0IsR0FBdEIsR0FBMEI5SCxDQUFqQztBQUFvQyxPQUExRCxDQUFqQyxFQUE2RjZCLENBQUMsQ0FBQzFoQixNQUFGLElBQVUraEIsQ0FBQyxDQUFDcGdCLElBQUYsQ0FBTyxLQUFLZ21CLFFBQUwsQ0FBYyxTQUFkLENBQVAsQ0FBdkcsRUFBd0lqRyxDQUFDLENBQUM3UyxPQUFGLENBQVUsVUFBU2dSLENBQVQsRUFBVztBQUFDa0MsU0FBQyxDQUFDcGdCLElBQUYsQ0FBT2xELENBQUMsQ0FBQ2twQixRQUFGLENBQVcsU0FBWCxJQUFzQixHQUF0QixHQUEwQjlILENBQWpDO0FBQW9DLE9BQTFELENBQXhJLEVBQW9Na0UsQ0FBQyxDQUFDLFlBQVU7QUFBQ3RsQixTQUFDLENBQUN3SCxPQUFGLENBQVUyaEIsZ0JBQVYsS0FBNkIsQ0FBQyxDQUE5QixJQUFpQzFGLENBQUMsQ0FBQ3pqQixDQUFDLENBQUNDLE1BQUgsRUFBVXFqQixDQUFWLEVBQVlELENBQVosQ0FBbEMsRUFBaURJLENBQUMsQ0FBQ3pqQixDQUFDLENBQUNpSCxPQUFILEVBQVdxYyxDQUFYLEVBQWFELENBQWIsQ0FBbEQ7QUFBa0UsT0FBOUUsQ0FBck0sRUFBcVIsQ0FBQyxDQUE3UjtBQUErUjtBQUFyc0IsR0FBZjs7QUFBdXRCLE1BQUl3RCxDQUFDLEdBQUMsWUFBVTtBQUFDLGFBQVN6RixDQUFULENBQVdBLENBQVgsRUFBYXBoQixDQUFiLEVBQWU7QUFBQyxVQUFJcWhCLENBQUMsR0FBQyxFQUFOO0FBQUEsVUFBU2pRLENBQUMsR0FBQyxDQUFDLENBQVo7QUFBQSxVQUFjck4sQ0FBQyxHQUFDLENBQUMsQ0FBakI7QUFBQSxVQUFtQjBkLENBQUMsR0FBQyxLQUFLLENBQTFCOztBQUE0QixVQUFHO0FBQUMsYUFBSSxJQUFJRSxDQUFKLEVBQU1HLENBQUMsR0FBQ1YsQ0FBQyxDQUFDMEYsTUFBTSxDQUFDQyxRQUFSLENBQUQsRUFBWixFQUFpQyxFQUFFM1YsQ0FBQyxHQUFDLENBQUN1USxDQUFDLEdBQUNHLENBQUMsQ0FBQ2tGLElBQUYsRUFBSCxFQUFhcmdCLElBQWpCLE1BQXlCMGEsQ0FBQyxDQUFDbmUsSUFBRixDQUFPeWUsQ0FBQyxDQUFDcmdCLEtBQVQsR0FBZ0IsQ0FBQ3RCLENBQUQsSUFBSXFoQixDQUFDLENBQUM5ZixNQUFGLEtBQVd2QixDQUF4RCxDQUFqQyxFQUE0Rm9SLENBQUMsR0FBQyxDQUFDLENBQS9GO0FBQWlHO0FBQWpHO0FBQW1HLE9BQXZHLENBQXVHLE9BQU0yUSxDQUFOLEVBQVE7QUFBQ2hlLFNBQUMsR0FBQyxDQUFDLENBQUgsRUFBSzBkLENBQUMsR0FBQ00sQ0FBUDtBQUFTLE9BQXpILFNBQWdJO0FBQUMsWUFBRztBQUFDLFdBQUMzUSxDQUFELElBQUkwUSxDQUFDLENBQUMsUUFBRCxDQUFMLElBQWlCQSxDQUFDLENBQUMsUUFBRCxDQUFELEVBQWpCO0FBQStCLFNBQW5DLFNBQTBDO0FBQUMsY0FBRy9kLENBQUgsRUFBSyxNQUFNMGQsQ0FBTjtBQUFRO0FBQUM7O0FBQUEsYUFBT0osQ0FBUDtBQUFTOztBQUFBLFdBQU8sVUFBU3JoQixDQUFULEVBQVdxaEIsQ0FBWCxFQUFhO0FBQUMsVUFBRzNHLEtBQUssQ0FBQ3VNLE9BQU4sQ0FBY2puQixDQUFkLENBQUgsRUFBb0IsT0FBT0EsQ0FBUDtBQUFTLFVBQUc4bUIsTUFBTSxDQUFDQyxRQUFQLElBQW1CcEQsTUFBTSxDQUFDM2pCLENBQUQsQ0FBNUIsRUFBZ0MsT0FBT29oQixDQUFDLENBQUNwaEIsQ0FBRCxFQUFHcWhCLENBQUgsQ0FBUjtBQUFjLFlBQU0sSUFBSUMsU0FBSixDQUFjLHNEQUFkLENBQU47QUFBNEUsS0FBNUs7QUFBNkssR0FBdmEsRUFBTjs7QUFBZ2IsU0FBT3BCLENBQUMsQ0FBQ2lGLE9BQUYsQ0FBVWppQixJQUFWLENBQWU7QUFBQ3FNLFlBQVEsRUFBQyxrQkFBUzZSLENBQVQsRUFBVztBQUFDLFVBQUlwaEIsQ0FBQyxHQUFDb2hCLENBQUMsQ0FBQzVSLEdBQVI7QUFBQSxVQUFZNlIsQ0FBQyxHQUFDRCxDQUFDLENBQUNqZ0IsSUFBaEI7O0FBQXFCLFVBQUcsS0FBS3FHLE9BQUwsQ0FBYXFOLEtBQWhCLEVBQXNCO0FBQUMsWUFBSXpELENBQUMsR0FBQyxLQUFLNUosT0FBTCxDQUFhcU4sS0FBbkI7QUFBeUIsc0JBQVksT0FBTyxLQUFLck4sT0FBTCxDQUFhcU4sS0FBaEMsS0FBd0N6RCxDQUFDLEdBQUMsS0FBSzVKLE9BQUwsQ0FBYXFOLEtBQWIsQ0FBbUJwUyxJQUFuQixDQUF3QixJQUF4QixFQUE2QjtBQUFDK00sYUFBRyxFQUFDeFAsQ0FBTDtBQUFPbUIsY0FBSSxFQUFDa2dCO0FBQVosU0FBN0IsQ0FBMUM7QUFBd0YsWUFBSXRkLENBQUMsR0FBQyxLQUFLLENBQVg7QUFBQSxZQUFhMGQsQ0FBQyxHQUFDLEtBQUssQ0FBcEI7O0FBQXNCLFlBQUcsWUFBVSxPQUFPclEsQ0FBcEIsRUFBc0I7QUFBQ0EsV0FBQyxHQUFDQSxDQUFDLENBQUM0SixLQUFGLENBQVEsR0FBUixDQUFGLEVBQWU1SixDQUFDLENBQUMsQ0FBRCxDQUFELEdBQUtBLENBQUMsQ0FBQyxDQUFELENBQUQsSUFBTUEsQ0FBQyxDQUFDLENBQUQsQ0FBM0I7QUFBK0IsY0FBSXVRLENBQUMsR0FBQ3ZRLENBQU47QUFBQSxjQUFRMFEsQ0FBQyxHQUFDK0UsQ0FBQyxDQUFDbEYsQ0FBRCxFQUFHLENBQUgsQ0FBWDtBQUFpQjVkLFdBQUMsR0FBQytkLENBQUMsQ0FBQyxDQUFELENBQUgsRUFBT0wsQ0FBQyxHQUFDSyxDQUFDLENBQUMsQ0FBRCxDQUFWLEVBQWMvZCxDQUFDLEdBQUN5Z0IsVUFBVSxDQUFDemdCLENBQUQsRUFBRyxFQUFILENBQTFCLEVBQWlDMGQsQ0FBQyxHQUFDK0MsVUFBVSxDQUFDL0MsQ0FBRCxFQUFHLEVBQUgsQ0FBN0M7QUFBb0QsU0FBM0gsTUFBZ0kxZCxDQUFDLEdBQUNxTixDQUFDLENBQUM1QixHQUFKLEVBQVFpUyxDQUFDLEdBQUNyUSxDQUFDLENBQUNqUSxJQUFaOztBQUFpQixlQUFPbkIsQ0FBQyxJQUFFK0QsQ0FBSCxFQUFLc2QsQ0FBQyxJQUFFSSxDQUFSLEVBQVU7QUFBQ2pTLGFBQUcsRUFBQ3hQLENBQUw7QUFBT21CLGNBQUksRUFBQ2tnQjtBQUFaLFNBQWpCO0FBQWdDO0FBQUM7QUFBM1gsR0FBZixHQUE2WTJKLENBQXBaO0FBQXNaLENBQXJqd0IsQ0FBRCxDOzs7Ozs7Ozs7Ozs7O0FDQUFuckIsbUNBQU8sWUFBVztBQUVkLFNBQU87QUFFSEQsUUFGRyxrQkFFSTtBQUNIZCxPQUFDLENBQUMwTyxRQUFELENBQUQsQ0FBWXpOLEVBQVosQ0FBZSxlQUFmLEVBQWdDLGdCQUFoQyxFQUFrRCxLQUFLd3JCLFVBQXZEO0FBQ0gsS0FKRTtBQU9IQSxjQVBHLHNCQU9RdnJCLENBUFIsRUFPVztBQUNWLFVBQUl3ckIsTUFBTSxHQUFHMXNCLENBQUMsQ0FBQyxJQUFELENBQWQ7O0FBR0EsVUFBSSxDQUFDMHNCLE1BQU0sQ0FBQzFxQixJQUFQLENBQVksUUFBWixDQUFMLEVBQTRCO0FBQ3hCLFlBQUkycUIsSUFBSSxHQUFHM3NCLENBQUMsQ0FBQ2tCLENBQUMsQ0FBQzByQixhQUFILENBQVo7QUFBQSxZQUNJQyxJQUFJLEdBQUdGLElBQUksQ0FBQzNxQixJQUFMLENBQVUsTUFBVixDQURYO0FBR0EwcUIsY0FBTSxDQUFDMXFCLElBQVAsQ0FBWSxRQUFaLEVBQXNCLElBQXRCO0FBQ0EwcUIsY0FBTSxDQUFDOXFCLElBQVAsQ0FBWSxxQkFBWixFQUFtQ2lZLE1BQW5DLENBQTBDLG1FQUFtRWdULElBQW5FLEdBQTBFLHlEQUFwSDtBQUNIO0FBQ0osS0FsQkU7QUFxQkhDLG1CQXJCRyw2QkFxQmU7QUFDZDlzQixPQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQjhFLEdBQXBCLENBQXdCLGVBQXhCLEVBQXlDLEtBQUsybkIsVUFBOUM7QUFDQXpzQixPQUFDLENBQUMsZ0JBQUQsQ0FBRCxDQUFvQmlCLEVBQXBCLENBQXVCLGVBQXZCLEVBQXdDLEtBQUt3ckIsVUFBN0M7QUFDSDtBQXhCRSxHQUFQO0FBMEJILENBNUJLO0FBQUEsb0dBQU4sQzs7Ozs7Ozs7Ozs7OztBQ0FBMXJCLG1DQUFPLFlBQVk7QUFDZixTQUFPLFVBQVV1UixDQUFWLEVBQWE7QUFDaEIsUUFBSTBRLENBQUMsR0FBRyxLQUFLemlCLE9BQWI7QUFDQXlpQixLQUFDLENBQUMrSixRQUFGLEdBQWEsVUFBVXphLENBQVYsRUFBYTBRLENBQWIsRUFBZ0I5aEIsQ0FBaEIsRUFBbUI7QUFDNUIsVUFBSW9oQixDQUFKO0FBQ0EsYUFBTyxZQUFZO0FBQ2YsWUFBSUssQ0FBQyxHQUFHLElBQVI7QUFBQSxZQUFjd0IsQ0FBQyxHQUFHeGYsU0FBbEI7QUFBQSxZQUE2Qk0sQ0FBQyxHQUFHLFNBQUpBLENBQUksR0FBWTtBQUN6Q3FkLFdBQUMsR0FBRyxJQUFKLEVBQVVwaEIsQ0FBQyxJQUFJb1IsQ0FBQyxDQUFDMU4sS0FBRixDQUFRK2QsQ0FBUixFQUFXd0IsQ0FBWCxDQUFmO0FBQ0gsU0FGRDtBQUFBLFlBRUc1QixDQUFDLEdBQUdyaEIsQ0FBQyxJQUFJLENBQUNvaEIsQ0FGYjs7QUFHQXdHLG9CQUFZLENBQUN4RyxDQUFELENBQVosRUFBaUJBLENBQUMsR0FBRzdWLFVBQVUsQ0FBQ3hILENBQUQsRUFBSStkLENBQUosQ0FBL0IsRUFBdUNULENBQUMsSUFBSWpRLENBQUMsQ0FBQzFOLEtBQUYsQ0FBUStkLENBQVIsRUFBV3dCLENBQVgsQ0FBNUM7QUFDSCxPQUxEO0FBTUgsS0FSRCxFQVFHbkIsQ0FBQyxDQUFDZ0ssYUFBRixHQUFrQixVQUFVMWEsQ0FBVixFQUFhMFEsQ0FBYixFQUFnQjloQixDQUFoQixFQUFtQm9oQixDQUFuQixFQUFzQjtBQUN2QyxhQUFPaFEsQ0FBQyxJQUFJZ1EsQ0FBQyxHQUFHLENBQVQsRUFBWSxJQUFJaFEsQ0FBSixHQUFRcFIsQ0FBQyxHQUFHLENBQUosR0FBUW9SLENBQVIsR0FBWUEsQ0FBWixHQUFnQjBRLENBQXhCLElBQTZCMVEsQ0FBQyxJQUFJLENBQUNwUixDQUFELEdBQUssQ0FBTCxJQUFVb1IsQ0FBQyxJQUFJQSxDQUFDLEdBQUcsQ0FBUixDQUFELEdBQWMsQ0FBeEIsSUFBNkIwUSxDQUEvRCxDQUFuQjtBQUNILEtBVkQsRUFVR0EsQ0FBQyxDQUFDaUssT0FBRixHQUFZLFVBQVUzYSxDQUFWLEVBQWEwUSxDQUFiLEVBQWdCOWhCLENBQWhCLEVBQW1Cb2hCLENBQW5CLEVBQXNCSyxDQUF0QixFQUF5QndCLENBQXpCLEVBQTRCO0FBQ3ZDN1IsT0FBQyxLQUFLcFMsTUFBTSxDQUFDZ3RCLHFCQUFQLENBQTZCLFlBQVk7QUFDM0M1YSxTQUFDLENBQUNuRixLQUFGLENBQVE2VixDQUFSLElBQWFMLENBQWI7QUFDSCxPQUZLLEVBRUh3QixDQUZHLEdBRUM3UixDQUFDLENBQUNuRixLQUFGLENBQVE2VixDQUFSLElBQWFWLENBRm5CLENBQUQ7QUFHSCxLQWREO0FBZUgsR0FqQkQ7QUFrQkgsQ0FuQks7QUFBQSxvR0FBTixDOzs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBdmhCLG1DQUFPLFlBQVk7QUFDZixTQUFPLFVBQVV1aEIsQ0FBVixFQUFhcGhCLENBQWIsRUFBZ0I7QUFDbkIsUUFBSThoQixDQUFKO0FBQUEsUUFBT1QsQ0FBUDtBQUFBLFFBQVVqUSxDQUFWO0FBQUEsUUFBYStSLENBQWI7QUFBQSxRQUFnQnBmLENBQWhCO0FBQUEsUUFBbUJrZSxDQUFuQjtBQUFBLFFBQXNCUixDQUFDLEdBQUcsR0FBMUI7QUFBQSxRQUErQndCLENBQUMsR0FBRyxDQUFuQztBQUFBLFFBQXNDdEIsQ0FBQyxHQUFHLGVBQTFDO0FBQUEsUUFBMkR5QixDQUFDLEdBQUcsb0JBQS9EO0FBQUEsUUFBcUZDLENBQUMsR0FBRyxxQkFBekY7QUFBQSxRQUNJdEIsQ0FBQyxHQUFHLHNCQURSO0FBRUFWLEtBQUMsR0FBRyxXQUFVRCxDQUFWLEVBQWFwaEIsQ0FBYixFQUFnQjhoQixDQUFoQixFQUFtQjtBQUNuQixXQUFLLElBQUlULENBQUMsR0FBR3JoQixDQUFDLENBQUNnYixLQUFGLENBQVEsR0FBUixDQUFSLEVBQXNCNUosQ0FBQyxHQUFHLENBQTFCLEVBQTZCK1IsQ0FBQyxHQUFHOUIsQ0FBQyxDQUFDOWYsTUFBeEMsRUFBZ0Q0aEIsQ0FBQyxHQUFHL1IsQ0FBcEQsRUFBdURBLENBQUMsRUFBeEQ7QUFBNERnUSxTQUFDLENBQUN0ZSxnQkFBRixDQUFtQnVlLENBQUMsQ0FBQ2pRLENBQUQsQ0FBcEIsRUFBeUIwUSxDQUF6QixFQUE0QixDQUFDLENBQTdCO0FBQTVEO0FBQ0gsS0FGRCxFQUVHMVEsQ0FBQyxHQUFHLGFBQVk7QUFDZixTQUFHaEIsT0FBSCxDQUFXM04sSUFBWCxDQUFnQitLLFFBQVEsQ0FBQ3ZELGdCQUFULENBQTBCLE1BQU0wWCxDQUFoQyxDQUFoQixFQUFvRCxVQUFVUCxDQUFWLEVBQWE7QUFDN0RDLFNBQUMsQ0FBQ0QsQ0FBRCxFQUFJLDRCQUFKLEVBQWtDLFVBQVVBLENBQVYsRUFBYTtBQUM1Q0EsV0FBQyxDQUFDNVcsY0FBRjtBQUNILFNBRkEsQ0FBRCxFQUVJNlcsQ0FBQyxDQUFDRCxDQUFELEVBQUksa0JBQUosRUFBd0IsVUFBVXBoQixDQUFWLEVBQWE7QUFDdEMsY0FBSThoQixDQUFDLEdBQUd0VSxRQUFRLENBQUN2RCxnQkFBVCxDQUEwQm1YLENBQUMsQ0FBQ3BULFlBQUYsQ0FBZSxhQUFmLENBQTFCLEVBQXlELENBQXpELENBQVI7QUFBQSxjQUNJcVQsQ0FBQyxHQUFHUyxDQUFDLENBQUM3WCxnQkFBRixDQUFtQixNQUFNOFgsQ0FBekIsRUFBNEIsQ0FBNUIsQ0FEUjtBQUFBLGNBRUkzUSxDQUFDLEdBQUdpUSxDQUFDLENBQUM0SyxZQUZWO0FBQUEsY0FFd0J2SSxDQUFDLEdBQUc1QixDQUFDLENBQUNyWixTQUFGLENBQVkyRyxRQUFaLENBQXFCaVUsQ0FBckIsQ0FGNUI7QUFBQSxjQUVxRGMsQ0FBQyxHQUFHL0MsQ0FBQyxDQUFDcFQsWUFBRixDQUFlLGtCQUFmLENBRnpEO0FBR0EsY0FBSWlWLENBQUMsR0FBRyxTQUFTN0IsQ0FBQyxDQUFDcFQsWUFBRixDQUFlLGVBQWYsQ0FBVCxHQUEyQ29ULENBQUMsQ0FBQ3BULFlBQUYsQ0FBZSxlQUFmLENBQTNDLEdBQTZFeVQsQ0FBakYsRUFBb0ZpQyxDQUF4RixFQUEyRjNmLENBQUMsQ0FBQ3FkLENBQUQsRUFBSVUsQ0FBSixDQUFELENBQTNGLEtBQXlHO0FBQ3JHLGdCQUFJcUMsQ0FBSixFQUFPO0FBQ0gsa0JBQUloQyxDQUFDLEdBQUdmLENBQUMsQ0FBQ3BULFlBQUYsQ0FBZSxhQUFmLENBQVI7QUFBQSxrQkFBdUN5VixDQUFDLEdBQUdyQyxDQUFDLENBQUNwVCxZQUFGLENBQWUsWUFBZixDQUEzQztBQUNBLGlCQUFHb0MsT0FBSCxDQUFXM04sSUFBWCxDQUFnQitLLFFBQVEsQ0FBQ3ZELGdCQUFULENBQTBCLE1BQU0wWCxDQUFOLEdBQVUsZUFBVixHQUE0QjhCLENBQTVCLEdBQWdDLElBQTFELENBQWhCLEVBQWlGLFVBQVVyQyxDQUFWLEVBQWE7QUFDMUYsb0JBQUlwaEIsQ0FBQyxHQUFHb2hCLENBQUMsQ0FBQ3BULFlBQUYsQ0FBZSxhQUFmLENBQVI7QUFDQW1VLGlCQUFDLEtBQUtuaUIsQ0FBTixJQUFXb2hCLENBQUMsQ0FBQzNZLFNBQUYsQ0FBWTJHLFFBQVosQ0FBcUJnVSxDQUFyQixDQUFYLElBQXNDbkIsQ0FBQyxDQUFDYixDQUFELEVBQUk1VCxRQUFRLENBQUN2RCxnQkFBVCxDQUEwQmpLLENBQTFCLEVBQTZCLENBQTdCLENBQUosQ0FBdkM7QUFDSCxlQUhEO0FBSUg7O0FBQ0RtakIsYUFBQyxDQUFDL0IsQ0FBRCxFQUFJVSxDQUFKLEVBQU9ULENBQVAsRUFBVWpRLENBQVYsQ0FBRDtBQUNIO0FBQ0osU0FkSSxDQUZMO0FBaUJILE9BbEJEO0FBbUJILEtBdEJELEVBc0JHK1IsQ0FBQyxHQUFHLFdBQVUvQixDQUFWLEVBQWFwaEIsQ0FBYixFQUFnQjhoQixDQUFoQixFQUFtQlQsQ0FBbkIsRUFBc0I7QUFDekJqaUIsY0FBUSxDQUFDQyxPQUFULENBQWlCMHNCLE9BQWpCLENBQXlCL3JCLENBQXpCLEVBQTRCLFFBQTVCLEVBQXNDLElBQXRDLEVBQTRDcWhCLENBQTVDLEVBQStDLE1BQS9DLEVBQXVENEIsQ0FBdkQsR0FBMkQ3QixDQUFDLENBQUMzWSxTQUFGLENBQVlLLEdBQVosQ0FBZ0JzYSxDQUFoQixDQUEzRCxFQUErRXBqQixDQUFDLENBQUN5SSxTQUFGLENBQVlLLEdBQVosQ0FBZ0J1YSxDQUFoQixDQUEvRTtBQUNILEtBeEJELEVBd0JHdGYsQ0FBQyxHQUFHLFdBQVVxZCxDQUFWLEVBQWFwaEIsQ0FBYixFQUFnQjhoQixDQUFoQixFQUFtQlQsQ0FBbkIsRUFBc0I7QUFDekJqaUIsY0FBUSxDQUFDQyxPQUFULENBQWlCMHNCLE9BQWpCLENBQXlCL3JCLENBQXpCLEVBQTRCLFFBQTVCLEVBQXNDLElBQXRDLEVBQTRDLENBQTVDLEVBQStDcWhCLENBQS9DLEVBQWtENEIsQ0FBbEQsR0FBc0Q3QixDQUFDLENBQUMzWSxTQUFGLENBQVlDLE1BQVosQ0FBbUIwYSxDQUFuQixDQUF0RCxFQUE2RXBqQixDQUFDLENBQUN5SSxTQUFGLENBQVlDLE1BQVosQ0FBbUIyYSxDQUFuQixDQUE3RTtBQUNILEtBMUJELEVBMEJHcEIsQ0FBQyxHQUFHLFdBQVViLENBQVYsRUFBYXBoQixDQUFiLEVBQWdCO0FBQ25Cb2hCLE9BQUMsQ0FBQzNZLFNBQUYsQ0FBWUMsTUFBWixDQUFtQjBhLENBQW5CLEdBQXVCcGpCLENBQUMsQ0FBQ2lNLEtBQUYsQ0FBUXhLLE1BQVIsR0FBaUIsQ0FBeEMsRUFBMkN6QixDQUFDLENBQUN5SSxTQUFGLENBQVlDLE1BQVosQ0FBbUIyYSxDQUFuQixDQUEzQztBQUNILEtBNUJELEVBNEJHdkIsQ0FBQyxHQUFHLFlBQVk7QUFDZjFRLE9BQUM7QUFDSixLQUZNLEVBNUJQO0FBK0JILEdBbENEO0FBbUNILENBcENLO0FBQUEsb0dBQU4sQzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNBQXZSLG1DQUFPLFlBQVc7QUFFZCxTQUFPO0FBRUhELFFBRkcsa0JBRUk7QUFDSCxXQUFLc3NCLFVBQUw7QUFDQSxXQUFLQyxVQUFMO0FBQ0gsS0FMRTtBQU9IO0FBQ0FBLGNBUkcsd0JBUVU7QUFDVHJ0QixPQUFDLENBQUMsTUFBRCxDQUFELENBQVVpQixFQUFWLENBQWEsT0FBYixFQUFzQixtQ0FBdEIsRUFBMkQsVUFBVUMsQ0FBVixFQUFhO0FBQ3BFLFlBQUlvc0IsS0FBSyxHQUFHdHRCLENBQUMsQ0FBQyxJQUFELENBQWI7QUFBQSxZQUNJdXRCLFFBQVEsR0FBR0QsS0FBSyxDQUFDdHJCLElBQU4sQ0FBVyxXQUFYLENBRGY7QUFBQSxZQUVJd3JCLE9BQU8sR0FBR3h0QixDQUFDLENBQUMsTUFBTXV0QixRQUFOLEdBQWlCLFNBQWxCLENBRmY7QUFBQSxZQUdJRSxNQUFNLEdBQUd6dEIsQ0FBQyxDQUFDLE1BQU11dEIsUUFBUCxDQUhkO0FBS0FELGFBQUssQ0FBQ2xzQixNQUFOLEdBQWVRLElBQWYsQ0FBb0IseUJBQXBCLEVBQStDZ0ksTUFBL0M7QUFFQTVKLFNBQUMsQ0FBQyx3QkFBRCxDQUFELENBQTRCNEosTUFBNUI7QUFFQTRqQixlQUFPLENBQUNsRyxXQUFSLENBQW9CLHdCQUFwQjtBQUNBbUcsY0FBTSxDQUFDdGYsR0FBUCxDQUFXLEVBQVg7QUFDSCxPQVpEO0FBYUgsS0F0QkU7QUF3Qkg7QUFDQWlmLGNBekJHLHdCQXlCVTtBQUVUO0FBQ0FwdEIsT0FBQyxDQUFDLE1BQUQsQ0FBRCxDQUFVaUIsRUFBVixDQUFhLE9BQWIsRUFBc0IsNkJBQXRCLEVBQXFELFVBQVVDLENBQVYsRUFBYTtBQUM5REEsU0FBQyxDQUFDd0ssY0FBRjtBQUVBLFlBQUk0aEIsS0FBSyxHQUFHdHRCLENBQUMsQ0FBQyxJQUFELENBQWI7QUFBQSxZQUNJMHRCLElBQUksR0FBR0osS0FBSyxDQUFDdHJCLElBQU4sQ0FBVyxNQUFYLENBRFg7QUFBQSxZQUVJMk8sRUFBRSxHQUFHMmMsS0FBSyxDQUFDdHJCLElBQU4sQ0FBVyxJQUFYLENBRlQ7QUFBQSxZQUdJMnJCLFVBQVUsR0FBR0wsS0FBSyxDQUFDTSxPQUFOLENBQWMsS0FBZCxFQUFxQjVyQixJQUFyQixDQUEwQixhQUExQixDQUhqQixDQUg4RCxDQVE5RDs7QUFDQTZyQixlQUFPLEdBQUlILElBQUksR0FBR0EsSUFBSCxHQUFVLEVBQXpCO0FBQ0FJLGNBQU0sR0FBR25kLEVBQVQsQ0FWOEQsQ0FZOUQ7O0FBQ0EzUSxTQUFDLENBQUN5Z0IsSUFBRixDQUFPO0FBQ0hqYixhQUFHLEVBQUVtb0IsVUFERjtBQUVIaFksY0FBSSxFQUFFLEtBRkg7QUFHSDNULGNBQUksRUFBRTtBQUFDLG9CQUFRNnJCO0FBQVQsV0FISDtBQUlIdGdCLGlCQUFPLEVBQUUsaUJBQVVrTCxRQUFWLEVBQW9CO0FBQ3pCc1YsdUJBQVcsR0FBR3RWLFFBQVEsQ0FBQ3VWLElBQXZCLENBRHlCLENBR3pCOztBQUNBaHVCLGFBQUMsQ0FBQyxpQ0FBRCxDQUFELENBQXFDZ3VCLElBQXJDLENBQTBDLGdCQUFnQkQsV0FBMUQ7QUFDSDtBQVRFLFNBQVA7QUFXSCxPQXhCRCxFQUhTLENBNkJUOztBQUNBL3RCLE9BQUMsQ0FBQyxNQUFELENBQUQsQ0FBVWlCLEVBQVYsQ0FBYSxPQUFiLEVBQXNCLDhCQUF0QixFQUFzRCxVQUFVQyxDQUFWLEVBQWE7QUFDL0RBLFNBQUMsQ0FBQ3dLLGNBQUY7QUFFQSxZQUFJNGhCLEtBQUssR0FBR3R0QixDQUFDLENBQUMsSUFBRCxDQUFiO0FBQUEsWUFDSXlVLElBQUksR0FBRzZZLEtBQUssQ0FBQ3RyQixJQUFOLENBQVcsTUFBWCxDQURYO0FBQUEsWUFFSWlzQixTQUFTLEdBQUdYLEtBQUssQ0FBQ3RyQixJQUFOLENBQVcsWUFBWCxDQUZoQjtBQUFBLFlBR0kyTyxFQUFFLEdBQUcyYyxLQUFLLENBQUN0ckIsSUFBTixDQUFXLElBQVgsQ0FIVDtBQUFBLFlBSUlvZCxLQUFLLEdBQUdrTyxLQUFLLENBQUN0ckIsSUFBTixDQUFXLE9BQVgsQ0FKWjtBQUFBLFlBS0lrc0IsR0FBRyxHQUFHWixLQUFLLENBQUN0ckIsSUFBTixDQUFXLEtBQVgsQ0FMVjtBQUFBLFlBTUkyckIsVUFBVSxHQUFHTCxLQUFLLENBQUNNLE9BQU4sQ0FBYyxvQkFBZCxFQUFvQzVyQixJQUFwQyxDQUF5QyxhQUF6QyxDQU5qQixDQUgrRCxDQVcvRDs7QUFDQTZyQixlQUFPLEdBQUdwWixJQUFWO0FBQ0FxWixjQUFNLEdBQUduZCxFQUFUO0FBQ0F3ZCxpQkFBUyxHQUFHL08sS0FBWjtBQUNBZ1AscUJBQWEsR0FBR0gsU0FBaEIsQ0FmK0QsQ0FpQi9EOztBQUNBLFlBQUksQ0FBQ0MsR0FBTCxFQUFVO0FBQ04sY0FBSUcsY0FBYyxHQUFHcnVCLENBQUMsQ0FBQ0UsTUFBTSxDQUFDa2UsWUFBUixDQUFELENBQXVCd1AsT0FBdkIsQ0FBK0IsZ0JBQS9CLEVBQWlENXJCLElBQWpELENBQXNELGVBQXRELENBQXJCOztBQUVBLGNBQUlxc0IsY0FBSixFQUFvQjtBQUNoQjV0QixlQUFHLENBQUNFLFlBQUosQ0FBaUIydEIscUJBQWpCLENBQXVDLEtBQXZDO0FBQ0gsV0FGRCxNQUVPO0FBQ0g7QUFDQXR1QixhQUFDLENBQUN5Z0IsSUFBRixDQUFPO0FBQ0hqYixpQkFBRyxFQUFFbW9CLFVBREY7QUFFSGhZLGtCQUFJLEVBQUUsS0FGSDtBQUdIM1Qsa0JBQUksRUFBRTtBQUFDLHdCQUFRNnJCO0FBQVQsZUFISDtBQUlIdGdCLHFCQUFPLEVBQUUsaUJBQVVrTCxRQUFWLEVBQW9CO0FBQ3pCc1YsMkJBQVcsR0FBR3RWLFFBQVEsQ0FBQ3VWLElBQXZCO0FBQ0g7QUFORSxhQUFQLEVBT0dubUIsSUFQSCxDQU9RLFlBQVk7QUFDaEJwSCxpQkFBRyxDQUFDRSxZQUFKLENBQWlCNHRCLG1CQUFqQixDQUFxQyxLQUFyQztBQUNILGFBVEQ7QUFVSDtBQUVKLFNBbkJELE1BbUJPO0FBQ0g5dEIsYUFBRyxDQUFDRSxZQUFKLENBQWlCMnRCLHFCQUFqQixDQUF1QyxJQUF2QztBQUNIO0FBQ0osT0F4Q0QsRUE5QlMsQ0F5RVQ7O0FBQ0F0dUIsT0FBQyxDQUFDLDJCQUFELENBQUQsQ0FBK0JpQixFQUEvQixDQUFrQyxPQUFsQyxFQUEyQyxZQUFZO0FBQ25ELFlBQUlpdEIsR0FBRyxHQUFHbHVCLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdDLElBQVIsQ0FBYSxLQUFiLENBQVY7O0FBRUEsWUFBSSxDQUFDa3NCLEdBQUwsRUFBVTtBQUNOLGNBQUlNLFlBQVksR0FBR3h1QixDQUFDLENBQUNFLE1BQU0sQ0FBQ2tlLFlBQVIsQ0FBRCxDQUF1QndQLE9BQXZCLENBQStCLGdCQUEvQixDQUFuQjtBQUFBLGNBQ0lhLGFBQWEsR0FBR0QsWUFBWSxDQUFDbnRCLElBQWIsQ0FBa0IsSUFBbEIsQ0FEcEI7QUFHQUQsZ0JBQU0sQ0FBQ3BCLENBQVAsQ0FBUyxNQUFNeXVCLGFBQWYsRUFBOEJDLEtBQTlCLENBQW9DLE1BQXBDO0FBRUgsU0FORCxNQU1PO0FBQ0h4dUIsZ0JBQU0sQ0FBQ3l1QixLQUFQO0FBQ0g7QUFDSixPQVpELEVBMUVTLENBeUZUOztBQUNBM3VCLE9BQUMsQ0FBQzBPLFFBQUQsQ0FBRCxDQUFZek4sRUFBWixDQUFlLE9BQWYsRUFBd0IseUJBQXhCLEVBQW1ELFlBQVk7QUFDM0QsWUFBSWl0QixHQUFHLEdBQUdsdUIsQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRZ0MsSUFBUixDQUFhLEtBQWIsQ0FBVjtBQUVBdkIsV0FBRyxDQUFDRSxZQUFKLENBQWlCNHRCLG1CQUFqQixDQUFxQ0wsR0FBckM7QUFDSCxPQUpEO0FBS0gsS0F4SEU7QUEwSEg7QUFDQUssdUJBM0hHLCtCQTJIaUJMLEdBM0hqQixFQTJIc0I7QUFDckIsVUFBSSxDQUFDQSxHQUFMLEVBQVU7QUFDTixZQUFJTSxZQUFZLEdBQUd4dUIsQ0FBQyxDQUFDRSxNQUFNLENBQUNrZSxZQUFSLENBQUQsQ0FBdUJ3UCxPQUF2QixDQUErQixnQkFBL0IsQ0FBbkI7QUFBQSxZQUNJZ0IsYUFBYSxHQUFHSixZQUFZLENBQUN4c0IsSUFBYixDQUFrQixpQkFBbEIsQ0FEcEI7QUFBQSxZQUVJeXNCLGFBQWEsR0FBR0QsWUFBWSxDQUFDbnRCLElBQWIsQ0FBa0IsSUFBbEIsQ0FGcEIsQ0FETSxDQUtOOztBQUNBRCxjQUFNLENBQUNwQixDQUFQLENBQVMsTUFBTTR1QixhQUFmLEVBQThCemdCLEdBQTlCLENBQWtDMGYsT0FBbEMsRUFBMkMvckIsTUFBM0MsR0FOTSxDQVFOOztBQUNBVixjQUFNLENBQUNwQixDQUFQLENBQVMsTUFBTTR1QixhQUFmLEVBQThCeHRCLE1BQTlCLEdBQXVDUSxJQUF2QyxDQUE0QyxzQkFBNUMsRUFBb0V1TSxHQUFwRSxDQUF3RTRmLFdBQXhFLEVBVE0sQ0FXTjs7QUFDQTNzQixjQUFNLENBQUNwQixDQUFQLENBQVMsTUFBTXl1QixhQUFmLEVBQThCQyxLQUE5QixDQUFvQyxNQUFwQztBQUVILE9BZEQsTUFjTztBQUNILFlBQUlHLE9BQU8sR0FBR0MsV0FBVyxDQUFDLGlCQUFELENBQXpCLENBREcsQ0FHSDs7QUFDQTV1QixjQUFNLENBQUM2dUIsTUFBUCxDQUFjQyxRQUFkLENBQXVCQyxLQUF2QixDQUE2QkMsWUFBN0IsQ0FBMENMLE9BQTFDLEVBQW1EaEIsT0FBbkQsRUFKRyxDQU1IOztBQUNBM3RCLGNBQU0sQ0FBQ3l1QixLQUFQO0FBQ0g7QUFDSixLQW5KRTtBQXNKSDtBQUNBTCx5QkF2SkcsaUNBdUptQkosR0F2Sm5CLEVBdUp3QjtBQUN2QixVQUFJLENBQUNBLEdBQUwsRUFBVTtBQUNOLFlBQUlNLFlBQVksR0FBR3h1QixDQUFDLENBQUNFLE1BQU0sQ0FBQ2tlLFlBQVIsQ0FBRCxDQUF1QndQLE9BQXZCLENBQStCLGdCQUEvQixDQUFuQjtBQUFBLFlBQ0lnQixhQUFhLEdBQUdKLFlBQVksQ0FBQ3hzQixJQUFiLENBQWtCLGlCQUFsQixDQURwQjtBQUFBLFlBRUl5c0IsYUFBYSxHQUFHRCxZQUFZLENBQUNudEIsSUFBYixDQUFrQixJQUFsQixDQUZwQixDQURNLENBS047O0FBQ0FELGNBQU0sQ0FBQ3BCLENBQVAsQ0FBUyxNQUFNNHVCLGFBQWYsRUFBOEJ6Z0IsR0FBOUIsQ0FBa0MyZixNQUFsQyxFQUEwQ2hzQixNQUExQyxHQU5NLENBUU47O0FBQ0EsWUFBSXF0QixhQUFhLEdBQUcvdEIsTUFBTSxDQUFDcEIsQ0FBUCxDQUFTLE1BQU00dUIsYUFBTixHQUFzQixTQUEvQixDQUFwQjtBQUFBLFlBQ0lRLFdBQVcsR0FBR2h1QixNQUFNLENBQUNwQixDQUFQLENBQVMsTUFBTTR1QixhQUFOLEdBQXNCLGdCQUEvQixDQURsQjtBQUFBLFlBRUlTLGFBQWEsR0FBR2p1QixNQUFNLENBQUNwQixDQUFQLENBQVMsTUFBTTR1QixhQUFOLEdBQXNCLGtCQUEvQixDQUZwQjtBQUlBTyxxQkFBYSxDQUFDOUgsUUFBZCxDQUF1Qix3QkFBdkI7QUFDQWdJLHFCQUFhLENBQUN2TyxJQUFkLENBQW1CcU4sU0FBbkI7O0FBRUEsWUFBSUMsYUFBYSxLQUFLLEVBQXRCLEVBQTBCO0FBQ3RCLGNBQUlrQixPQUFPLEdBQUdELGFBQWEsQ0FBQ2p1QixNQUFkLEVBQWQ7QUFDQWt1QixpQkFBTyxDQUFDQyxPQUFSLENBQWdCLG9EQUFoQjtBQUNILFNBSEQsTUFHTztBQUNISCxxQkFBVyxDQUFDL3RCLElBQVosQ0FBaUIsS0FBakIsRUFBd0Irc0IsYUFBeEI7QUFDSCxTQXJCSyxDQXVCTjs7O0FBQ0FodEIsY0FBTSxDQUFDcEIsQ0FBUCxDQUFTLE1BQU15dUIsYUFBZixFQUE4QkMsS0FBOUIsQ0FBb0MsTUFBcEM7QUFFSCxPQTFCRCxNQTBCTztBQUNILFlBQUlHLE9BQU8sR0FBR0MsV0FBVyxDQUFDLGlCQUFELENBQXpCLENBREcsQ0FHSDs7QUFDQTV1QixjQUFNLENBQUM2dUIsTUFBUCxDQUFjQyxRQUFkLENBQXVCQyxLQUF2QixDQUE2QkMsWUFBN0IsQ0FBMENMLE9BQTFDLEVBQW1EaEIsT0FBbkQsRUFKRyxDQU1IOztBQUNBM3RCLGNBQU0sQ0FBQ3l1QixLQUFQO0FBQ0g7QUFDSixLQTNMRTtBQThMSDtBQUNBRyxlQS9MRyx1QkErTFNocEIsU0EvTFQsRUErTG9CO0FBQ25CLFVBQUkwcEIsT0FBTyxHQUFHLElBQUlwTCxNQUFKLENBQVcsb0JBQW9CdGUsU0FBcEIsR0FBZ0MsVUFBM0MsRUFBdUQsR0FBdkQsQ0FBZDtBQUFBLFVBQ0k4UCxLQUFLLEdBQUcxVixNQUFNLENBQUN1dkIsUUFBUCxDQUFnQnJ0QixNQUFoQixDQUF1QndULEtBQXZCLENBQTZCNFosT0FBN0IsQ0FEWjtBQUdBLGFBQVE1WixLQUFLLElBQUlBLEtBQUssQ0FBQ25ULE1BQU4sR0FBZSxDQUF6QixHQUE4Qm1ULEtBQUssQ0FBQyxDQUFELENBQW5DLEdBQXlDLEVBQWhEO0FBQ0g7QUFwTUUsR0FBUDtBQXVNSCxDQXpNSztBQUFBLG9HQUFOLEM7Ozs7Ozs7Ozs7O0FDQUEsdUMiLCJmaWxlIjoiYmFja2VuZC9kYXNoYm9hcmQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCJnbG9iYWwuJCA9IGdsb2JhbC5qUXVlcnkgPSB3aW5kb3cualF1ZXJ5ID0gcmVxdWlyZSgnanF1ZXJ5Jyk7XHJcbnJlcXVpcmUoJ0Bwb3BwZXJqcy9jb3JlL2Rpc3QvY2pzL3BvcHBlcicpO1xyXG5yZXF1aXJlKCdib290c3RyYXAvZGlzdC9qcy9ib290c3RyYXAnKTtcclxucmVxdWlyZSgnLi9qcy90ZXRoZXIubWluJyk7XHJcbnJlcXVpcmUoJy4vanMvZHJvcHpvbmUnKTtcclxuZ2xvYmFsLlJhcGhhZWwgPSByZXF1aXJlKCdyYXBoYWVsL3JhcGhhZWwubWluJyk7XHJcbnJlcXVpcmUoJ21vcnJpcy5qcy9tb3JyaXMubWluJyk7XHJcbnJlcXVpcmUoJ3N1bW1lcm5vdGUvZGlzdC9zdW1tZXJub3RlLWJzNCcpO1xyXG5yZXF1aXJlKCcuL2pzL3N1bW1lcm5vdGUvbWVkaWFidW5kbGUucGx1Z2luJyk7XHJcbnJlcXVpcmUoJ211bHRpc2VsZWN0LXR3by1zaWRlcy9kaXN0L2pzL211bHRpc2VsZWN0Jyk7XHJcbnJlcXVpcmUoJ2Jvb3RzdHJhcC10b2dnbGUvanMvYm9vdHN0cmFwLXRvZ2dsZS5taW4nKTtcclxuaW1wb3J0ICdhY2UtYnVpbGRzJztcclxuaW1wb3J0ICdhY2UtYnVpbGRzL3dlYnBhY2stcmVzb2x2ZXInO1xyXG5nbG9iYWwuRHJvcHpvbmUgPSByZXF1aXJlKCcuL2pzL2Ryb3B6b25lJyk7XHJcblxyXG5nbG9iYWwuY2FyZ29iYXkgPSB7fTtcclxuZ2xvYmFsLmNhcmdvYmF5LmdlbmVyYWwgPSByZXF1aXJlKCcuL2pzL3Rvb2xzL2NhcmdvYmF5LmdlbmVyYWwubWluJyk7XHJcbmdsb2JhbC5jYXJnb2JheS50b2dnbGUgPSAgcmVxdWlyZSgnLi9qcy90b29scy9jYXJnb2JheS50b29nbGUubWluJyk7XHJcblxyXG5nbG9iYWwucHNjID0ge307XHJcbmdsb2JhbC5wc2MuYWpheE1vZGFsID0gcmVxdWlyZSgnLi9qcy90b29scy9hamF4TW9kYWwnKTtcclxuZ2xvYmFsLnBzYy5tZWRpYUNob29zZXIgPSByZXF1aXJlKCcuL2pzL3Rvb2xzL21lZGlhQ2hvb3NlcicpO1xyXG5nbG9iYWwucHNjLmFwcCA9IHJlcXVpcmUoJy4vanMvYXBwJyk7XHJcbmdsb2JhbC5wc2Mub3JkZXIgPSByZXF1aXJlKCcuL2pzL29yZGVyLmpzJyk7XHJcblxyXG5nbG9iYWwucHNjLmFwcC5pbml0KCk7XHJcblxyXG5pbXBvcnQgJy4vanMvdG9vbHMvbWVkaWFDaG9vc2VyJztcclxuaW1wb3J0ICdib290c3RyYXAvc2Nzcy9ib290c3RyYXAuc2Nzcyc7XHJcbmltcG9ydCAnLi9jc3MvdG9vbHMvY2FyZ29iYXkudG9vZ2xlLm1pbi5jc3MnO1xyXG5pbXBvcnQgJy4vbGVzcy9iYXNlLmxlc3MnO1xyXG5pbXBvcnQgJ3N1bW1lcm5vdGUvZGlzdC9zdW1tZXJub3RlLWJzNC5taW4uY3NzJztcclxuLy9pbXBvcnQgJ211bHRpc2VsZWN0L2Nzcy9tdWx0aS1zZWxlY3QuY3NzJztcclxuaW1wb3J0ICdib290c3RyYXAtdG9nZ2xlL2Nzcy9ib290c3RyYXAtdG9nZ2xlLm1pbi5jc3MnOyIsImRlZmluZShmdW5jdGlvbigpIHtcclxuXHJcbiAgICByZXR1cm4ge1xyXG4gICAgICAgIC8vIEdlbmVyYWwgQXBwIGluaXRcclxuICAgICAgICBpbml0KCkge1xyXG5cclxuICAgICAgICAgICAgcHNjLmFqYXhNb2RhbC5pbml0KCk7XHJcbiAgICAgICAgICAgIHBzYy5tZWRpYUNob29zZXIuaW5pdCgpO1xyXG4gICAgICAgICAgICBwc2Mub3JkZXIuaW5pdCgpO1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIGJvb3RzdHJhcEFwcCA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgICAgICAgICAgYm9vdHN0cmFwQXBwKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn0pO1xyXG5cclxuJChmdW5jdGlvbigpIHtcclxuXHJcbiAgICAkKCdhW2RhdGEtYnMtdG9nZ2xlPVwidGFiXCJdJykub24oJ3Nob3duLmJzLnRhYicsIGZ1bmN0aW9uIChlKSB7XHJcbiAgICAgICAgaWYoJChlLnRhcmdldCkucGFyZW50KCkucGFyZW50KCkucGFyZW50KCkuYXR0cignaWQnKSAmJiAkKGUudGFyZ2V0KS5wYXJlbnQoKS5wYXJlbnQoKS5wYXJlbnQoKS5hdHRyKCdpZCcpID09ICdkb2MnKSB7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdsYXN0VGFiRG9jJywgJCh0aGlzKS5hdHRyKCdocmVmJykpO1xyXG4gICAgICAgIH1lbHNle1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSgnbGFzdFRhYicsICQodGhpcykuYXR0cignaHJlZicpKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgIC8vIGdvIHRvIHRoZSBsYXRlc3QgdGFiLCBpZiBpdCBleGlzdHM6XHJcbiAgICB2YXIgbGFzdFRhYiA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdsYXN0VGFiJyk7XHJcbiAgICBpZiAobGFzdFRhYikge1xyXG4gICAgICAgICQoJ1tocmVmPVwiJyArIGxhc3RUYWIgKyAnXCJdJykudGFiKCdzaG93Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFyIGxhc3RUYWJEb2MgPSBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnbGFzdFRhYkRvYycpO1xyXG4gICAgaWYgKGxhc3RUYWJEb2MpIHtcclxuICAgICAgICAkKCdbaHJlZj1cIicgKyBsYXN0VGFiRG9jICsgJ1wiXScpLnRhYignc2hvdycpO1xyXG4gICAgfVxyXG5cclxuICAgICQoJy5uYXYgbGkgdWwnKS5wYXJlbnQoKS5maW5kKCdhJykuY2xpY2soZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgJCh0aGlzKS5wYXJlbnQoKS5maW5kKFwidWxcIikudG9nZ2xlKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICBwc2MuYXBwLmluaXQoKTtcclxuXHJcbiAgICAkKCdpbnB1dFtkYXRhLWJzLXRvZ2dsZT1cInRvZ2dsZVwiXSwgaW5wdXQuYWpheFRvb2dsZScpLmNoYW5nZShmdW5jdGlvbigpIHtcclxuICAgICAgICAkLmdldEpTT04oJCh0aGlzKS5kYXRhKCd1cmwnKSk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCdzZWxlY3QubXVsdGlwbGUnKS5tdWx0aXNlbGVjdCh7XHJcbiAgICAgICAgc3VibWl0QWxsTGVmdDogZmFsc2UsXHJcbiAgICAgICAgc3VibWl0QWxsUmlnaHQ6IHRydWUsXHJcbiAgICAgICAgc2VhcmNoOiB7XHJcbiAgICAgICAgICAgIGxlZnQ6ICc8aW5wdXQgdHlwZT1cInRleHRcIiBuYW1lPVwicVwiIGNsYXNzPVwiZm9ybS1jb250cm9sXCIgcGxhY2Vob2xkZXI9XCJTZWFyY2guLi5cIiAvPicsXHJcbiAgICAgICAgICAgIHJpZ2h0OiAnPGlucHV0IHR5cGU9XCJ0ZXh0XCIgbmFtZT1cInFcIiBjbGFzcz1cImZvcm0tY29udHJvbFwiIHBsYWNlaG9sZGVyPVwiU2VhcmNoLi4uXCIgLz4nLFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZmlyZVNlYXJjaDogZnVuY3Rpb24odmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHZhbHVlLmxlbmd0aCA+IDE7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgJCgnLnN1bW1lcm5vdGUnKS5zdW1tZXJub3RlKHtcclxuICAgICAgICBoZWlnaHQ6IDQwMCxcclxuICAgICAgICB0b29sYmFyOiBbXHJcbiAgICAgICAgICAgICAgICBbJ3N0eWxlJywgWydzdHlsZSddXSxcclxuICAgICAgICAgICAgICAgIFsnZm9udCcsIFsnYm9sZCcsICdpdGFsaWMnLCAndW5kZXJsaW5lJywgJ2NsZWFyJ11dLFxyXG4gICAgICAgICAgICAgICAgWydmb250bmFtZScsIFsnZm9udG5hbWUnXV0sXHJcbiAgICAgICAgICAgICAgICBbJ2NvbG9yJywgWydjb2xvciddXSxcclxuICAgICAgICAgICAgICAgIFsncGFyYScsIFsndWwnLCAnb2wnLCAncGFyYWdyYXBoJ11dLFxyXG4gICAgICAgICAgICAgICAgWydoZWlnaHQnLCBbJ2hlaWdodCddXSxcclxuICAgICAgICAgICAgICAgIFsndGFibGUnLCBbJ3RhYmxlJ11dLFxyXG4gICAgICAgICAgICAgICAgWydpbnNlcnQnLCBbJ2xpbmsnLCAnaHInXV0sXHJcbiAgICAgICAgICAgICAgICBbJ3ZpZXcnLCBbJ2Z1bGxzY3JlZW4nLCAnY29kZXZpZXcnXV0sXHJcbiAgICAgICAgICAgICAgICBbJ2hlbHAnLCBbJ2hlbHAnXV0sXHJcbiAgICAgICAgICAgICAgICBbJ21lZGlhJywgWydtZWRpYSddXVxyXG4gICAgICAgIF1cclxuICAgIH0pO1xyXG59KTsiLCJcclxuLypcclxuICpcclxuICogTW9yZSBpbmZvIGF0IFt3d3cuZHJvcHpvbmVqcy5jb21dKGh0dHA6Ly93d3cuZHJvcHpvbmVqcy5jb20pXHJcbiAqXHJcbiAqIENvcHlyaWdodCAoYykgMjAxMiwgTWF0aWFzIE1lbm9cclxuICpcclxuICogUGVybWlzc2lvbiBpcyBoZXJlYnkgZ3JhbnRlZCwgZnJlZSBvZiBjaGFyZ2UsIHRvIGFueSBwZXJzb24gb2J0YWluaW5nIGEgY29weVxyXG4gKiBvZiB0aGlzIHNvZnR3YXJlIGFuZCBhc3NvY2lhdGVkIGRvY3VtZW50YXRpb24gZmlsZXMgKHRoZSBcIlNvZnR3YXJlXCIpLCB0byBkZWFsXHJcbiAqIGluIHRoZSBTb2Z0d2FyZSB3aXRob3V0IHJlc3RyaWN0aW9uLCBpbmNsdWRpbmcgd2l0aG91dCBsaW1pdGF0aW9uIHRoZSByaWdodHNcclxuICogdG8gdXNlLCBjb3B5LCBtb2RpZnksIG1lcmdlLCBwdWJsaXNoLCBkaXN0cmlidXRlLCBzdWJsaWNlbnNlLCBhbmQvb3Igc2VsbFxyXG4gKiBjb3BpZXMgb2YgdGhlIFNvZnR3YXJlLCBhbmQgdG8gcGVybWl0IHBlcnNvbnMgdG8gd2hvbSB0aGUgU29mdHdhcmUgaXNcclxuICogZnVybmlzaGVkIHRvIGRvIHNvLCBzdWJqZWN0IHRvIHRoZSBmb2xsb3dpbmcgY29uZGl0aW9uczpcclxuICpcclxuICogVGhlIGFib3ZlIGNvcHlyaWdodCBub3RpY2UgYW5kIHRoaXMgcGVybWlzc2lvbiBub3RpY2Ugc2hhbGwgYmUgaW5jbHVkZWQgaW5cclxuICogYWxsIGNvcGllcyBvciBzdWJzdGFudGlhbCBwb3J0aW9ucyBvZiB0aGUgU29mdHdhcmUuXHJcbiAqXHJcbiAqIFRIRSBTT0ZUV0FSRSBJUyBQUk9WSURFRCBcIkFTIElTXCIsIFdJVEhPVVQgV0FSUkFOVFkgT0YgQU5ZIEtJTkQsIEVYUFJFU1MgT1JcclxuICogSU1QTElFRCwgSU5DTFVESU5HIEJVVCBOT1QgTElNSVRFRCBUTyBUSEUgV0FSUkFOVElFUyBPRiBNRVJDSEFOVEFCSUxJVFksXHJcbiAqIEZJVE5FU1MgRk9SIEEgUEFSVElDVUxBUiBQVVJQT1NFIEFORCBOT05JTkZSSU5HRU1FTlQuIElOIE5PIEVWRU5UIFNIQUxMIFRIRVxyXG4gKiBBVVRIT1JTIE9SIENPUFlSSUdIVCBIT0xERVJTIEJFIExJQUJMRSBGT1IgQU5ZIENMQUlNLCBEQU1BR0VTIE9SIE9USEVSXHJcbiAqIExJQUJJTElUWSwgV0hFVEhFUiBJTiBBTiBBQ1RJT04gT0YgQ09OVFJBQ1QsIFRPUlQgT1IgT1RIRVJXSVNFLCBBUklTSU5HIEZST00sXHJcbiAqIE9VVCBPRiBPUiBJTiBDT05ORUNUSU9OIFdJVEggVEhFIFNPRlRXQVJFIE9SIFRIRSBVU0UgT1IgT1RIRVIgREVBTElOR1MgSU5cclxuICogVEhFIFNPRlRXQVJFLlxyXG4gKlxyXG4gKi9cclxuXHJcbihmdW5jdGlvbigpIHtcclxuICB2YXIgRHJvcHpvbmUsIEVtaXR0ZXIsIGNhbWVsaXplLCBjb250ZW50TG9hZGVkLCBkZXRlY3RWZXJ0aWNhbFNxdWFzaCwgZHJhd0ltYWdlSU9TRml4LCBub29wLCB3aXRob3V0LFxyXG4gICAgX19zbGljZSA9IFtdLnNsaWNlLFxyXG4gICAgX19oYXNQcm9wID0ge30uaGFzT3duUHJvcGVydHksXHJcbiAgICBfX2V4dGVuZHMgPSBmdW5jdGlvbihjaGlsZCwgcGFyZW50KSB7IGZvciAodmFyIGtleSBpbiBwYXJlbnQpIHsgaWYgKF9faGFzUHJvcC5jYWxsKHBhcmVudCwga2V5KSkgY2hpbGRba2V5XSA9IHBhcmVudFtrZXldOyB9IGZ1bmN0aW9uIGN0b3IoKSB7IHRoaXMuY29uc3RydWN0b3IgPSBjaGlsZDsgfSBjdG9yLnByb3RvdHlwZSA9IHBhcmVudC5wcm90b3R5cGU7IGNoaWxkLnByb3RvdHlwZSA9IG5ldyBjdG9yKCk7IGNoaWxkLl9fc3VwZXJfXyA9IHBhcmVudC5wcm90b3R5cGU7IHJldHVybiBjaGlsZDsgfTtcclxuXHJcbiAgbm9vcCA9IGZ1bmN0aW9uKCkge307XHJcblxyXG4gIEVtaXR0ZXIgPSAoZnVuY3Rpb24oKSB7XHJcbiAgICBmdW5jdGlvbiBFbWl0dGVyKCkge31cclxuXHJcbiAgICBFbWl0dGVyLnByb3RvdHlwZS5hZGRFdmVudExpc3RlbmVyID0gRW1pdHRlci5wcm90b3R5cGUub247XHJcblxyXG4gICAgRW1pdHRlci5wcm90b3R5cGUub24gPSBmdW5jdGlvbihldmVudCwgZm4pIHtcclxuICAgICAgdGhpcy5fY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzIHx8IHt9O1xyXG4gICAgICBpZiAoIXRoaXMuX2NhbGxiYWNrc1tldmVudF0pIHtcclxuICAgICAgICB0aGlzLl9jYWxsYmFja3NbZXZlbnRdID0gW107XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5fY2FsbGJhY2tzW2V2ZW50XS5wdXNoKGZuKTtcclxuICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9O1xyXG5cclxuICAgIEVtaXR0ZXIucHJvdG90eXBlLmVtaXQgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIGFyZ3MsIGNhbGxiYWNrLCBjYWxsYmFja3MsIGV2ZW50LCBfaSwgX2xlbjtcclxuICAgICAgZXZlbnQgPSBhcmd1bWVudHNbMF0sIGFyZ3MgPSAyIDw9IGFyZ3VtZW50cy5sZW5ndGggPyBfX3NsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSA6IFtdO1xyXG4gICAgICB0aGlzLl9jYWxsYmFja3MgPSB0aGlzLl9jYWxsYmFja3MgfHwge307XHJcbiAgICAgIGNhbGxiYWNrcyA9IHRoaXMuX2NhbGxiYWNrc1tldmVudF07XHJcbiAgICAgIGlmIChjYWxsYmFja3MpIHtcclxuICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGNhbGxiYWNrcy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgICAgY2FsbGJhY2sgPSBjYWxsYmFja3NbX2ldO1xyXG4gICAgICAgICAgY2FsbGJhY2suYXBwbHkodGhpcywgYXJncyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfTtcclxuXHJcbiAgICBFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lciA9IEVtaXR0ZXIucHJvdG90eXBlLm9mZjtcclxuXHJcbiAgICBFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVBbGxMaXN0ZW5lcnMgPSBFbWl0dGVyLnByb3RvdHlwZS5vZmY7XHJcblxyXG4gICAgRW1pdHRlci5wcm90b3R5cGUucmVtb3ZlRXZlbnRMaXN0ZW5lciA9IEVtaXR0ZXIucHJvdG90eXBlLm9mZjtcclxuXHJcbiAgICBFbWl0dGVyLnByb3RvdHlwZS5vZmYgPSBmdW5jdGlvbihldmVudCwgZm4pIHtcclxuICAgICAgdmFyIGNhbGxiYWNrLCBjYWxsYmFja3MsIGksIF9pLCBfbGVuO1xyXG4gICAgICBpZiAoIXRoaXMuX2NhbGxiYWNrcyB8fCBhcmd1bWVudHMubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgdGhpcy5fY2FsbGJhY2tzID0ge307XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICAgIH1cclxuICAgICAgY2FsbGJhY2tzID0gdGhpcy5fY2FsbGJhY2tzW2V2ZW50XTtcclxuICAgICAgaWYgKCFjYWxsYmFja3MpIHtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgICAgfVxyXG4gICAgICBpZiAoYXJndW1lbnRzLmxlbmd0aCA9PT0gMSkge1xyXG4gICAgICAgIGRlbGV0ZSB0aGlzLl9jYWxsYmFja3NbZXZlbnRdO1xyXG4gICAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgICB9XHJcbiAgICAgIGZvciAoaSA9IF9pID0gMCwgX2xlbiA9IGNhbGxiYWNrcy5sZW5ndGg7IF9pIDwgX2xlbjsgaSA9ICsrX2kpIHtcclxuICAgICAgICBjYWxsYmFjayA9IGNhbGxiYWNrc1tpXTtcclxuICAgICAgICBpZiAoY2FsbGJhY2sgPT09IGZuKSB7XHJcbiAgICAgICAgICBjYWxsYmFja3Muc3BsaWNlKGksIDEpO1xyXG4gICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzO1xyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4gRW1pdHRlcjtcclxuXHJcbiAgfSkoKTtcclxuXHJcbiAgRHJvcHpvbmUgPSAoZnVuY3Rpb24oX3N1cGVyKSB7XHJcbiAgICB2YXIgZXh0ZW5kLCByZXNvbHZlT3B0aW9uO1xyXG5cclxuICAgIF9fZXh0ZW5kcyhEcm9wem9uZSwgX3N1cGVyKTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuRW1pdHRlciA9IEVtaXR0ZXI7XHJcblxyXG5cclxuICAgIC8qXHJcbiAgICBUaGlzIGlzIGEgbGlzdCBvZiBhbGwgYXZhaWxhYmxlIGV2ZW50cyB5b3UgY2FuIHJlZ2lzdGVyIG9uIGEgZHJvcHpvbmUgb2JqZWN0LlxyXG4gICAgXHJcbiAgICBZb3UgY2FuIHJlZ2lzdGVyIGFuIGV2ZW50IGhhbmRsZXIgbGlrZSB0aGlzOlxyXG4gICAgXHJcbiAgICAgICAgZHJvcHpvbmUub24oXCJkcmFnRW50ZXJcIiwgZnVuY3Rpb24oKSB7IH0pO1xyXG4gICAgICovXHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmV2ZW50cyA9IFtcImRyb3BcIiwgXCJkcmFnc3RhcnRcIiwgXCJkcmFnZW5kXCIsIFwiZHJhZ2VudGVyXCIsIFwiZHJhZ292ZXJcIiwgXCJkcmFnbGVhdmVcIiwgXCJhZGRlZGZpbGVcIiwgXCJhZGRlZGZpbGVzXCIsIFwicmVtb3ZlZGZpbGVcIiwgXCJ0aHVtYm5haWxcIiwgXCJlcnJvclwiLCBcImVycm9ybXVsdGlwbGVcIiwgXCJwcm9jZXNzaW5nXCIsIFwicHJvY2Vzc2luZ211bHRpcGxlXCIsIFwidXBsb2FkcHJvZ3Jlc3NcIiwgXCJ0b3RhbHVwbG9hZHByb2dyZXNzXCIsIFwic2VuZGluZ1wiLCBcInNlbmRpbmdtdWx0aXBsZVwiLCBcInN1Y2Nlc3NcIiwgXCJzdWNjZXNzbXVsdGlwbGVcIiwgXCJjYW5jZWxlZFwiLCBcImNhbmNlbGVkbXVsdGlwbGVcIiwgXCJjb21wbGV0ZVwiLCBcImNvbXBsZXRlbXVsdGlwbGVcIiwgXCJyZXNldFwiLCBcIm1heGZpbGVzZXhjZWVkZWRcIiwgXCJtYXhmaWxlc3JlYWNoZWRcIiwgXCJxdWV1ZWNvbXBsZXRlXCJdO1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5kZWZhdWx0T3B0aW9ucyA9IHtcclxuICAgICAgdXJsOiBudWxsLFxyXG4gICAgICBtZXRob2Q6IFwicG9zdFwiLFxyXG4gICAgICB3aXRoQ3JlZGVudGlhbHM6IGZhbHNlLFxyXG4gICAgICBwYXJhbGxlbFVwbG9hZHM6IDIsXHJcbiAgICAgIHVwbG9hZE11bHRpcGxlOiBmYWxzZSxcclxuICAgICAgbWF4RmlsZXNpemU6IDI1NixcclxuICAgICAgcGFyYW1OYW1lOiBcImZpbGVcIixcclxuICAgICAgY3JlYXRlSW1hZ2VUaHVtYm5haWxzOiB0cnVlLFxyXG4gICAgICBtYXhUaHVtYm5haWxGaWxlc2l6ZTogMTAsXHJcbiAgICAgIHRodW1ibmFpbFdpZHRoOiAxMjAsXHJcbiAgICAgIHRodW1ibmFpbEhlaWdodDogMTIwLFxyXG4gICAgICBmaWxlc2l6ZUJhc2U6IDEwMDAsXHJcbiAgICAgIG1heEZpbGVzOiBudWxsLFxyXG4gICAgICBwYXJhbXM6IHt9LFxyXG4gICAgICBjbGlja2FibGU6IHRydWUsXHJcbiAgICAgIGlnbm9yZUhpZGRlbkZpbGVzOiB0cnVlLFxyXG4gICAgICBhY2NlcHRlZEZpbGVzOiBudWxsLFxyXG4gICAgICBhY2NlcHRlZE1pbWVUeXBlczogbnVsbCxcclxuICAgICAgYXV0b1Byb2Nlc3NRdWV1ZTogdHJ1ZSxcclxuICAgICAgYXV0b1F1ZXVlOiB0cnVlLFxyXG4gICAgICBhZGRSZW1vdmVMaW5rczogZmFsc2UsXHJcbiAgICAgIHByZXZpZXdzQ29udGFpbmVyOiBudWxsLFxyXG4gICAgICBoaWRkZW5JbnB1dENvbnRhaW5lcjogXCJib2R5XCIsXHJcbiAgICAgIGNhcHR1cmU6IG51bGwsXHJcbiAgICAgIGRpY3REZWZhdWx0TWVzc2FnZTogXCJEcm9wIGZpbGVzIGhlcmUgdG8gdXBsb2FkXCIsXHJcbiAgICAgIGRpY3RGYWxsYmFja01lc3NhZ2U6IFwiWW91ciBicm93c2VyIGRvZXMgbm90IHN1cHBvcnQgZHJhZyduJ2Ryb3AgZmlsZSB1cGxvYWRzLlwiLFxyXG4gICAgICBkaWN0RmFsbGJhY2tUZXh0OiBcIlBsZWFzZSB1c2UgdGhlIGZhbGxiYWNrIGZvcm0gYmVsb3cgdG8gdXBsb2FkIHlvdXIgZmlsZXMgbGlrZSBpbiB0aGUgb2xkZW4gZGF5cy5cIixcclxuICAgICAgZGljdEZpbGVUb29CaWc6IFwiRmlsZSBpcyB0b28gYmlnICh7e2ZpbGVzaXplfX1NaUIpLiBNYXggZmlsZXNpemU6IHt7bWF4RmlsZXNpemV9fU1pQi5cIixcclxuICAgICAgZGljdEludmFsaWRGaWxlVHlwZTogXCJZb3UgY2FuJ3QgdXBsb2FkIGZpbGVzIG9mIHRoaXMgdHlwZS5cIixcclxuICAgICAgZGljdFJlc3BvbnNlRXJyb3I6IFwiU2VydmVyIHJlc3BvbmRlZCB3aXRoIHt7c3RhdHVzQ29kZX19IGNvZGUuXCIsXHJcbiAgICAgIGRpY3RDYW5jZWxVcGxvYWQ6IFwiQ2FuY2VsIHVwbG9hZFwiLFxyXG4gICAgICBkaWN0Q2FuY2VsVXBsb2FkQ29uZmlybWF0aW9uOiBcIkFyZSB5b3Ugc3VyZSB5b3Ugd2FudCB0byBjYW5jZWwgdGhpcyB1cGxvYWQ/XCIsXHJcbiAgICAgIGRpY3RSZW1vdmVGaWxlOiBcIlJlbW92ZSBmaWxlXCIsXHJcbiAgICAgIGRpY3RSZW1vdmVGaWxlQ29uZmlybWF0aW9uOiBudWxsLFxyXG4gICAgICBkaWN0TWF4RmlsZXNFeGNlZWRlZDogXCJZb3UgY2FuIG5vdCB1cGxvYWQgYW55IG1vcmUgZmlsZXMuXCIsXHJcbiAgICAgIGFjY2VwdDogZnVuY3Rpb24oZmlsZSwgZG9uZSkge1xyXG4gICAgICAgIHJldHVybiBkb25lKCk7XHJcbiAgICAgIH0sXHJcbiAgICAgIGluaXQ6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBub29wO1xyXG4gICAgICB9LFxyXG4gICAgICBmb3JjZUZhbGxiYWNrOiBmYWxzZSxcclxuICAgICAgZmFsbGJhY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBjaGlsZCwgbWVzc2FnZUVsZW1lbnQsIHNwYW4sIF9pLCBfbGVuLCBfcmVmO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc05hbWUgPSBcIlwiICsgdGhpcy5lbGVtZW50LmNsYXNzTmFtZSArIFwiIGR6LWJyb3dzZXItbm90LXN1cHBvcnRlZFwiO1xyXG4gICAgICAgIF9yZWYgPSB0aGlzLmVsZW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJkaXZcIik7XHJcbiAgICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBfcmVmLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgICBjaGlsZCA9IF9yZWZbX2ldO1xyXG4gICAgICAgICAgaWYgKC8oXnwgKWR6LW1lc3NhZ2UoJHwgKS8udGVzdChjaGlsZC5jbGFzc05hbWUpKSB7XHJcbiAgICAgICAgICAgIG1lc3NhZ2VFbGVtZW50ID0gY2hpbGQ7XHJcbiAgICAgICAgICAgIGNoaWxkLmNsYXNzTmFtZSA9IFwiZHotbWVzc2FnZVwiO1xyXG4gICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFtZXNzYWdlRWxlbWVudCkge1xyXG4gICAgICAgICAgbWVzc2FnZUVsZW1lbnQgPSBEcm9wem9uZS5jcmVhdGVFbGVtZW50KFwiPGRpdiBjbGFzcz1cXFwiZHotbWVzc2FnZVxcXCI+PHNwYW4+PC9zcGFuPjwvZGl2PlwiKTtcclxuICAgICAgICAgIHRoaXMuZWxlbWVudC5hcHBlbmRDaGlsZChtZXNzYWdlRWxlbWVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNwYW4gPSBtZXNzYWdlRWxlbWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcInNwYW5cIilbMF07XHJcbiAgICAgICAgaWYgKHNwYW4pIHtcclxuICAgICAgICAgIGlmIChzcGFuLnRleHRDb250ZW50ICE9IG51bGwpIHtcclxuICAgICAgICAgICAgc3Bhbi50ZXh0Q29udGVudCA9IHRoaXMub3B0aW9ucy5kaWN0RmFsbGJhY2tNZXNzYWdlO1xyXG4gICAgICAgICAgfSBlbHNlIGlmIChzcGFuLmlubmVyVGV4dCAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgIHNwYW4uaW5uZXJUZXh0ID0gdGhpcy5vcHRpb25zLmRpY3RGYWxsYmFja01lc3NhZ2U7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuYXBwZW5kQ2hpbGQodGhpcy5nZXRGYWxsYmFja0Zvcm0oKSk7XHJcbiAgICAgIH0sXHJcbiAgICAgIHJlc2l6ZTogZnVuY3Rpb24oZmlsZSkge1xyXG4gICAgICAgIHZhciBpbmZvLCBzcmNSYXRpbywgdHJnUmF0aW87XHJcbiAgICAgICAgaW5mbyA9IHtcclxuICAgICAgICAgIHNyY1g6IDAsXHJcbiAgICAgICAgICBzcmNZOiAwLFxyXG4gICAgICAgICAgc3JjV2lkdGg6IGZpbGUud2lkdGgsXHJcbiAgICAgICAgICBzcmNIZWlnaHQ6IGZpbGUuaGVpZ2h0XHJcbiAgICAgICAgfTtcclxuICAgICAgICBzcmNSYXRpbyA9IGZpbGUud2lkdGggLyBmaWxlLmhlaWdodDtcclxuICAgICAgICBpbmZvLm9wdFdpZHRoID0gdGhpcy5vcHRpb25zLnRodW1ibmFpbFdpZHRoO1xyXG4gICAgICAgIGluZm8ub3B0SGVpZ2h0ID0gdGhpcy5vcHRpb25zLnRodW1ibmFpbEhlaWdodDtcclxuICAgICAgICBpZiAoKGluZm8ub3B0V2lkdGggPT0gbnVsbCkgJiYgKGluZm8ub3B0SGVpZ2h0ID09IG51bGwpKSB7XHJcbiAgICAgICAgICBpbmZvLm9wdFdpZHRoID0gaW5mby5zcmNXaWR0aDtcclxuICAgICAgICAgIGluZm8ub3B0SGVpZ2h0ID0gaW5mby5zcmNIZWlnaHQ7XHJcbiAgICAgICAgfSBlbHNlIGlmIChpbmZvLm9wdFdpZHRoID09IG51bGwpIHtcclxuICAgICAgICAgIGluZm8ub3B0V2lkdGggPSBzcmNSYXRpbyAqIGluZm8ub3B0SGVpZ2h0O1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaW5mby5vcHRIZWlnaHQgPT0gbnVsbCkge1xyXG4gICAgICAgICAgaW5mby5vcHRIZWlnaHQgPSAoMSAvIHNyY1JhdGlvKSAqIGluZm8ub3B0V2lkdGg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRyZ1JhdGlvID0gaW5mby5vcHRXaWR0aCAvIGluZm8ub3B0SGVpZ2h0O1xyXG4gICAgICAgIGlmIChmaWxlLmhlaWdodCA8IGluZm8ub3B0SGVpZ2h0IHx8IGZpbGUud2lkdGggPCBpbmZvLm9wdFdpZHRoKSB7XHJcbiAgICAgICAgICBpbmZvLnRyZ0hlaWdodCA9IGluZm8uc3JjSGVpZ2h0O1xyXG4gICAgICAgICAgaW5mby50cmdXaWR0aCA9IGluZm8uc3JjV2lkdGg7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIGlmIChzcmNSYXRpbyA+IHRyZ1JhdGlvKSB7XHJcbiAgICAgICAgICAgIGluZm8uc3JjSGVpZ2h0ID0gZmlsZS5oZWlnaHQ7XHJcbiAgICAgICAgICAgIGluZm8uc3JjV2lkdGggPSBpbmZvLnNyY0hlaWdodCAqIHRyZ1JhdGlvO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaW5mby5zcmNXaWR0aCA9IGZpbGUud2lkdGg7XHJcbiAgICAgICAgICAgIGluZm8uc3JjSGVpZ2h0ID0gaW5mby5zcmNXaWR0aCAvIHRyZ1JhdGlvO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpbmZvLnNyY1ggPSAoZmlsZS53aWR0aCAtIGluZm8uc3JjV2lkdGgpIC8gMjtcclxuICAgICAgICBpbmZvLnNyY1kgPSAoZmlsZS5oZWlnaHQgLSBpbmZvLnNyY0hlaWdodCkgLyAyO1xyXG4gICAgICAgIHJldHVybiBpbmZvO1xyXG4gICAgICB9LFxyXG5cclxuICAgICAgLypcclxuICAgICAgVGhvc2UgZnVuY3Rpb25zIHJlZ2lzdGVyIHRoZW1zZWx2ZXMgdG8gdGhlIGV2ZW50cyBvbiBpbml0IGFuZCBoYW5kbGUgYWxsXHJcbiAgICAgIHRoZSB1c2VyIGludGVyZmFjZSBzcGVjaWZpYyBzdHVmZi4gT3ZlcndyaXRpbmcgdGhlbSB3b24ndCBicmVhayB0aGUgdXBsb2FkXHJcbiAgICAgIGJ1dCBjYW4gYnJlYWsgdGhlIHdheSBpdCdzIGRpc3BsYXllZC5cclxuICAgICAgWW91IGNhbiBvdmVyd3JpdGUgdGhlbSBpZiB5b3UgZG9uJ3QgbGlrZSB0aGUgZGVmYXVsdCBiZWhhdmlvci4gSWYgeW91IGp1c3RcclxuICAgICAgd2FudCB0byBhZGQgYW4gYWRkaXRpb25hbCBldmVudCBoYW5kbGVyLCByZWdpc3RlciBpdCBvbiB0aGUgZHJvcHpvbmUgb2JqZWN0XHJcbiAgICAgIGFuZCBkb24ndCBvdmVyd3JpdGUgdGhvc2Ugb3B0aW9ucy5cclxuICAgICAgICovXHJcbiAgICAgIGRyb3A6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJkei1kcmFnLWhvdmVyXCIpO1xyXG4gICAgICB9LFxyXG4gICAgICBkcmFnc3RhcnQ6IG5vb3AsXHJcbiAgICAgIGRyYWdlbmQ6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJkei1kcmFnLWhvdmVyXCIpO1xyXG4gICAgICB9LFxyXG4gICAgICBkcmFnZW50ZXI6IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJkei1kcmFnLWhvdmVyXCIpO1xyXG4gICAgICB9LFxyXG4gICAgICBkcmFnb3ZlcjogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmFkZChcImR6LWRyYWctaG92ZXJcIik7XHJcbiAgICAgIH0sXHJcbiAgICAgIGRyYWdsZWF2ZTogZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImR6LWRyYWctaG92ZXJcIik7XHJcbiAgICAgIH0sXHJcbiAgICAgIHBhc3RlOiBub29wLFxyXG4gICAgICByZXNldDogZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwiZHotc3RhcnRlZFwiKTtcclxuICAgICAgfSxcclxuICAgICAgYWRkZWRmaWxlOiBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgICAgdmFyIG5vZGUsIHJlbW92ZUZpbGVFdmVudCwgcmVtb3ZlTGluaywgX2ksIF9qLCBfaywgX2xlbiwgX2xlbjEsIF9sZW4yLCBfcmVmLCBfcmVmMSwgX3JlZjIsIF9yZXN1bHRzO1xyXG4gICAgICAgIGlmICh0aGlzLmVsZW1lbnQgPT09IHRoaXMucHJldmlld3NDb250YWluZXIpIHtcclxuICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotc3RhcnRlZFwiKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMucHJldmlld3NDb250YWluZXIpIHtcclxuICAgICAgICAgIGZpbGUucHJldmlld0VsZW1lbnQgPSBEcm9wem9uZS5jcmVhdGVFbGVtZW50KHRoaXMub3B0aW9ucy5wcmV2aWV3VGVtcGxhdGUudHJpbSgpKTtcclxuICAgICAgICAgIGZpbGUucHJldmlld1RlbXBsYXRlID0gZmlsZS5wcmV2aWV3RWxlbWVudDtcclxuICAgICAgICAgIHRoaXMucHJldmlld3NDb250YWluZXIuYXBwZW5kQ2hpbGQoZmlsZS5wcmV2aWV3RWxlbWVudCk7XHJcbiAgICAgICAgICBfcmVmID0gZmlsZS5wcmV2aWV3RWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtZHotbmFtZV1cIik7XHJcbiAgICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICAgICAgbm9kZSA9IF9yZWZbX2ldO1xyXG4gICAgICAgICAgICBub2RlLnRleHRDb250ZW50ID0gZmlsZS5uYW1lO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgX3JlZjEgPSBmaWxlLnByZXZpZXdFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS1kei1zaXplXVwiKTtcclxuICAgICAgICAgIGZvciAoX2ogPSAwLCBfbGVuMSA9IF9yZWYxLmxlbmd0aDsgX2ogPCBfbGVuMTsgX2orKykge1xyXG4gICAgICAgICAgICBub2RlID0gX3JlZjFbX2pdO1xyXG4gICAgICAgICAgICBub2RlLmlubmVySFRNTCA9IHRoaXMuZmlsZXNpemUoZmlsZS5zaXplKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmICh0aGlzLm9wdGlvbnMuYWRkUmVtb3ZlTGlua3MpIHtcclxuICAgICAgICAgICAgZmlsZS5fcmVtb3ZlTGluayA9IERyb3B6b25lLmNyZWF0ZUVsZW1lbnQoXCI8YSBjbGFzcz1cXFwiZHotcmVtb3ZlXFxcIiBocmVmPVxcXCJqYXZhc2NyaXB0OnVuZGVmaW5lZDtcXFwiIGRhdGEtZHotcmVtb3ZlPlwiICsgdGhpcy5vcHRpb25zLmRpY3RSZW1vdmVGaWxlICsgXCI8L2E+XCIpO1xyXG4gICAgICAgICAgICBmaWxlLnByZXZpZXdFbGVtZW50LmFwcGVuZENoaWxkKGZpbGUuX3JlbW92ZUxpbmspO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmVtb3ZlRmlsZUV2ZW50ID0gKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgaWYgKGZpbGUuc3RhdHVzID09PSBEcm9wem9uZS5VUExPQURJTkcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBEcm9wem9uZS5jb25maXJtKF90aGlzLm9wdGlvbnMuZGljdENhbmNlbFVwbG9hZENvbmZpcm1hdGlvbiwgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5yZW1vdmVGaWxlKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlmIChfdGhpcy5vcHRpb25zLmRpY3RSZW1vdmVGaWxlQ29uZmlybWF0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiBEcm9wem9uZS5jb25maXJtKF90aGlzLm9wdGlvbnMuZGljdFJlbW92ZUZpbGVDb25maXJtYXRpb24sIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5yZW1vdmVGaWxlKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5yZW1vdmVGaWxlKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgIH0pKHRoaXMpO1xyXG4gICAgICAgICAgX3JlZjIgPSBmaWxlLnByZXZpZXdFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS1kei1yZW1vdmVdXCIpO1xyXG4gICAgICAgICAgX3Jlc3VsdHMgPSBbXTtcclxuICAgICAgICAgIGZvciAoX2sgPSAwLCBfbGVuMiA9IF9yZWYyLmxlbmd0aDsgX2sgPCBfbGVuMjsgX2srKykge1xyXG4gICAgICAgICAgICByZW1vdmVMaW5rID0gX3JlZjJbX2tdO1xyXG4gICAgICAgICAgICBfcmVzdWx0cy5wdXNoKHJlbW92ZUxpbmsuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIHJlbW92ZUZpbGVFdmVudCkpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIF9yZXN1bHRzO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgcmVtb3ZlZGZpbGU6IGZ1bmN0aW9uKGZpbGUpIHtcclxuICAgICAgICB2YXIgX3JlZjtcclxuICAgICAgICBpZiAoZmlsZS5wcmV2aWV3RWxlbWVudCkge1xyXG4gICAgICAgICAgaWYgKChfcmVmID0gZmlsZS5wcmV2aWV3RWxlbWVudCkgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICBfcmVmLnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQoZmlsZS5wcmV2aWV3RWxlbWVudCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLl91cGRhdGVNYXhGaWxlc1JlYWNoZWRDbGFzcygpO1xyXG4gICAgICB9LFxyXG4gICAgICB0aHVtYm5haWw6IGZ1bmN0aW9uKGZpbGUsIGRhdGFVcmwpIHtcclxuICAgICAgICB2YXIgdGh1bWJuYWlsRWxlbWVudCwgX2ksIF9sZW4sIF9yZWY7XHJcbiAgICAgICAgaWYgKGZpbGUucHJldmlld0VsZW1lbnQpIHtcclxuICAgICAgICAgIGZpbGUucHJldmlld0VsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImR6LWZpbGUtcHJldmlld1wiKTtcclxuICAgICAgICAgIF9yZWYgPSBmaWxlLnByZXZpZXdFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS1kei10aHVtYm5haWxdXCIpO1xyXG4gICAgICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBfcmVmLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgICAgIHRodW1ibmFpbEVsZW1lbnQgPSBfcmVmW19pXTtcclxuICAgICAgICAgICAgdGh1bWJuYWlsRWxlbWVudC5hbHQgPSBmaWxlLm5hbWU7XHJcbiAgICAgICAgICAgIHRodW1ibmFpbEVsZW1lbnQuc3JjID0gZGF0YVVybDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBzZXRUaW1lb3V0KCgoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgIHJldHVybiBmaWxlLnByZXZpZXdFbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJkei1pbWFnZS1wcmV2aWV3XCIpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgfSkodGhpcykpLCAxKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIGVycm9yOiBmdW5jdGlvbihmaWxlLCBtZXNzYWdlKSB7XHJcbiAgICAgICAgdmFyIG5vZGUsIF9pLCBfbGVuLCBfcmVmLCBfcmVzdWx0cztcclxuICAgICAgICBpZiAoZmlsZS5wcmV2aWV3RWxlbWVudCkge1xyXG4gICAgICAgICAgZmlsZS5wcmV2aWV3RWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotZXJyb3JcIik7XHJcbiAgICAgICAgICBpZiAodHlwZW9mIG1lc3NhZ2UgIT09IFwiU3RyaW5nXCIgJiYgbWVzc2FnZS5lcnJvcikge1xyXG4gICAgICAgICAgICBtZXNzYWdlID0gbWVzc2FnZS5lcnJvcjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIF9yZWYgPSBmaWxlLnByZXZpZXdFbGVtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCJbZGF0YS1kei1lcnJvcm1lc3NhZ2VdXCIpO1xyXG4gICAgICAgICAgX3Jlc3VsdHMgPSBbXTtcclxuICAgICAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgICAgICBub2RlID0gX3JlZltfaV07XHJcbiAgICAgICAgICAgIF9yZXN1bHRzLnB1c2gobm9kZS50ZXh0Q29udGVudCA9IG1lc3NhZ2UpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIF9yZXN1bHRzO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgZXJyb3JtdWx0aXBsZTogbm9vcCxcclxuICAgICAgcHJvY2Vzc2luZzogZnVuY3Rpb24oZmlsZSkge1xyXG4gICAgICAgIGlmIChmaWxlLnByZXZpZXdFbGVtZW50KSB7XHJcbiAgICAgICAgICBmaWxlLnByZXZpZXdFbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJkei1wcm9jZXNzaW5nXCIpO1xyXG4gICAgICAgICAgaWYgKGZpbGUuX3JlbW92ZUxpbmspIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZpbGUuX3JlbW92ZUxpbmsudGV4dENvbnRlbnQgPSB0aGlzLm9wdGlvbnMuZGljdENhbmNlbFVwbG9hZDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH0sXHJcbiAgICAgIHByb2Nlc3NpbmdtdWx0aXBsZTogbm9vcCxcclxuICAgICAgdXBsb2FkcHJvZ3Jlc3M6IGZ1bmN0aW9uKGZpbGUsIHByb2dyZXNzLCBieXRlc1NlbnQpIHtcclxuICAgICAgICB2YXIgbm9kZSwgX2ksIF9sZW4sIF9yZWYsIF9yZXN1bHRzO1xyXG4gICAgICAgIGlmIChmaWxlLnByZXZpZXdFbGVtZW50KSB7XHJcbiAgICAgICAgICBfcmVmID0gZmlsZS5wcmV2aWV3RWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiW2RhdGEtZHotdXBsb2FkcHJvZ3Jlc3NdXCIpO1xyXG4gICAgICAgICAgX3Jlc3VsdHMgPSBbXTtcclxuICAgICAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgICAgICBub2RlID0gX3JlZltfaV07XHJcbiAgICAgICAgICAgIGlmIChub2RlLm5vZGVOYW1lID09PSAnUFJPR1JFU1MnKSB7XHJcbiAgICAgICAgICAgICAgX3Jlc3VsdHMucHVzaChub2RlLnZhbHVlID0gcHJvZ3Jlc3MpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgIF9yZXN1bHRzLnB1c2gobm9kZS5zdHlsZS53aWR0aCA9IFwiXCIgKyBwcm9ncmVzcyArIFwiJVwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIF9yZXN1bHRzO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgdG90YWx1cGxvYWRwcm9ncmVzczogbm9vcCxcclxuICAgICAgc2VuZGluZzogbm9vcCxcclxuICAgICAgc2VuZGluZ211bHRpcGxlOiBub29wLFxyXG4gICAgICBzdWNjZXNzOiBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgICAgaWYgKGZpbGUucHJldmlld0VsZW1lbnQpIHtcclxuICAgICAgICAgIHJldHVybiBmaWxlLnByZXZpZXdFbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJkei1zdWNjZXNzXCIpO1xyXG4gICAgICAgIH1cclxuICAgICAgfSxcclxuICAgICAgc3VjY2Vzc211bHRpcGxlOiBub29wLFxyXG4gICAgICBjYW5jZWxlZDogZnVuY3Rpb24oZmlsZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVtaXQoXCJlcnJvclwiLCBmaWxlLCBcIlVwbG9hZCBjYW5jZWxlZC5cIik7XHJcbiAgICAgIH0sXHJcbiAgICAgIGNhbmNlbGVkbXVsdGlwbGU6IG5vb3AsXHJcbiAgICAgIGNvbXBsZXRlOiBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgICAgaWYgKGZpbGUuX3JlbW92ZUxpbmspIHtcclxuICAgICAgICAgIGZpbGUuX3JlbW92ZUxpbmsudGV4dENvbnRlbnQgPSB0aGlzLm9wdGlvbnMuZGljdFJlbW92ZUZpbGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChmaWxlLnByZXZpZXdFbGVtZW50KSB7XHJcbiAgICAgICAgICByZXR1cm4gZmlsZS5wcmV2aWV3RWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotY29tcGxldGVcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9LFxyXG4gICAgICBjb21wbGV0ZW11bHRpcGxlOiBub29wLFxyXG4gICAgICBtYXhmaWxlc2V4Y2VlZGVkOiBub29wLFxyXG4gICAgICBtYXhmaWxlc3JlYWNoZWQ6IG5vb3AsXHJcbiAgICAgIHF1ZXVlY29tcGxldGU6IG5vb3AsXHJcbiAgICAgIGFkZGVkZmlsZXM6IG5vb3AsXHJcbiAgICAgIHByZXZpZXdUZW1wbGF0ZTogXCI8ZGl2IGNsYXNzPVxcXCJkei1wcmV2aWV3IGR6LWZpbGUtcHJldmlld1xcXCI+XFxuICA8ZGl2IGNsYXNzPVxcXCJkei1pbWFnZVxcXCI+PGltZyBkYXRhLWR6LXRodW1ibmFpbCAvPjwvZGl2PlxcbiAgPGRpdiBjbGFzcz1cXFwiZHotZGV0YWlsc1xcXCI+XFxuICAgIDxkaXYgY2xhc3M9XFxcImR6LXNpemVcXFwiPjxzcGFuIGRhdGEtZHotc2l6ZT48L3NwYW4+PC9kaXY+XFxuICAgIDxkaXYgY2xhc3M9XFxcImR6LWZpbGVuYW1lXFxcIj48c3BhbiBkYXRhLWR6LW5hbWU+PC9zcGFuPjwvZGl2PlxcbiAgPC9kaXY+XFxuICA8ZGl2IGNsYXNzPVxcXCJkei1wcm9ncmVzc1xcXCI+PHNwYW4gY2xhc3M9XFxcImR6LXVwbG9hZFxcXCIgZGF0YS1kei11cGxvYWRwcm9ncmVzcz48L3NwYW4+PC9kaXY+XFxuICA8ZGl2IGNsYXNzPVxcXCJkei1lcnJvci1tZXNzYWdlXFxcIj48c3BhbiBkYXRhLWR6LWVycm9ybWVzc2FnZT48L3NwYW4+PC9kaXY+XFxuICA8ZGl2IGNsYXNzPVxcXCJkei1zdWNjZXNzLW1hcmtcXFwiPlxcbiAgICA8c3ZnIHdpZHRoPVxcXCI1NHB4XFxcIiBoZWlnaHQ9XFxcIjU0cHhcXFwiIHZpZXdCb3g9XFxcIjAgMCA1NCA1NFxcXCIgdmVyc2lvbj1cXFwiMS4xXFxcIiB4bWxucz1cXFwiaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmdcXFwiIHhtbG5zOnhsaW5rPVxcXCJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rXFxcIiB4bWxuczpza2V0Y2g9XFxcImh0dHA6Ly93d3cuYm9oZW1pYW5jb2RpbmcuY29tL3NrZXRjaC9uc1xcXCI+XFxuICAgICAgPHRpdGxlPkNoZWNrPC90aXRsZT5cXG4gICAgICA8ZGVmcz48L2RlZnM+XFxuICAgICAgPGcgaWQ9XFxcIlBhZ2UtMVxcXCIgc3Ryb2tlPVxcXCJub25lXFxcIiBzdHJva2Utd2lkdGg9XFxcIjFcXFwiIGZpbGw9XFxcIm5vbmVcXFwiIGZpbGwtcnVsZT1cXFwiZXZlbm9kZFxcXCIgc2tldGNoOnR5cGU9XFxcIk1TUGFnZVxcXCI+XFxuICAgICAgICA8cGF0aCBkPVxcXCJNMjMuNSwzMS44NDMxNDU4IEwxNy41ODUyNDE5LDI1LjkyODM4NzcgQzE2LjAyNDgyNTMsMjQuMzY3OTcxMSAxMy40OTEwMjk0LDI0LjM2NjgzNSAxMS45Mjg5MzIyLDI1LjkyODkzMjIgQzEwLjM3MDAxMzYsMjcuNDg3ODUwOCAxMC4zNjY1OTEyLDMwLjAyMzQ0NTUgMTEuOTI4Mzg3NywzMS41ODUyNDE5IEwyMC40MTQ3NTgxLDQwLjA3MTYxMjMgQzIwLjUxMzM5OTksNDAuMTcwMjU0MSAyMC42MTU5MzE1LDQwLjI2MjY2NDkgMjAuNzIxODYxNSw0MC4zNDg4NDM1IEMyMi4yODM1NjY5LDQxLjg3MjU2NTEgMjQuNzk0MjM0LDQxLjg2MjYyMDIgMjYuMzQ2MTU2NCw0MC4zMTA2OTc4IEw0My4zMTA2OTc4LDIzLjM0NjE1NjQgQzQ0Ljg3NzEwMjEsMjEuNzc5NzUyMSA0NC44NzU4MDU3LDE5LjI0ODM4ODcgNDMuMzEzNzA4NSwxNy42ODYyOTE1IEM0MS43NTQ3ODk5LDE2LjEyNzM3MjkgMzkuMjE3NjAzNSwxNi4xMjU1NDIyIDM3LjY1Mzg0MzYsMTcuNjg5MzAyMiBMMjMuNSwzMS44NDMxNDU4IFogTTI3LDUzIEM0MS4zNTk0MDM1LDUzIDUzLDQxLjM1OTQwMzUgNTMsMjcgQzUzLDEyLjY0MDU5NjUgNDEuMzU5NDAzNSwxIDI3LDEgQzEyLjY0MDU5NjUsMSAxLDEyLjY0MDU5NjUgMSwyNyBDMSw0MS4zNTk0MDM1IDEyLjY0MDU5NjUsNTMgMjcsNTMgWlxcXCIgaWQ9XFxcIk92YWwtMlxcXCIgc3Ryb2tlLW9wYWNpdHk9XFxcIjAuMTk4Nzk0MTU4XFxcIiBzdHJva2U9XFxcIiM3NDc0NzRcXFwiIGZpbGwtb3BhY2l0eT1cXFwiMC44MTY1MTk0NzVcXFwiIGZpbGw9XFxcIiNGRkZGRkZcXFwiIHNrZXRjaDp0eXBlPVxcXCJNU1NoYXBlR3JvdXBcXFwiPjwvcGF0aD5cXG4gICAgICA8L2c+XFxuICAgIDwvc3ZnPlxcbiAgPC9kaXY+XFxuICA8ZGl2IGNsYXNzPVxcXCJkei1lcnJvci1tYXJrXFxcIj5cXG4gICAgPHN2ZyB3aWR0aD1cXFwiNTRweFxcXCIgaGVpZ2h0PVxcXCI1NHB4XFxcIiB2aWV3Qm94PVxcXCIwIDAgNTQgNTRcXFwiIHZlcnNpb249XFxcIjEuMVxcXCIgeG1sbnM9XFxcImh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnXFxcIiB4bWxuczp4bGluaz1cXFwiaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGlua1xcXCIgeG1sbnM6c2tldGNoPVxcXCJodHRwOi8vd3d3LmJvaGVtaWFuY29kaW5nLmNvbS9za2V0Y2gvbnNcXFwiPlxcbiAgICAgIDx0aXRsZT5FcnJvcjwvdGl0bGU+XFxuICAgICAgPGRlZnM+PC9kZWZzPlxcbiAgICAgIDxnIGlkPVxcXCJQYWdlLTFcXFwiIHN0cm9rZT1cXFwibm9uZVxcXCIgc3Ryb2tlLXdpZHRoPVxcXCIxXFxcIiBmaWxsPVxcXCJub25lXFxcIiBmaWxsLXJ1bGU9XFxcImV2ZW5vZGRcXFwiIHNrZXRjaDp0eXBlPVxcXCJNU1BhZ2VcXFwiPlxcbiAgICAgICAgPGcgaWQ9XFxcIkNoZWNrLSstT3ZhbC0yXFxcIiBza2V0Y2g6dHlwZT1cXFwiTVNMYXllckdyb3VwXFxcIiBzdHJva2U9XFxcIiM3NDc0NzRcXFwiIHN0cm9rZS1vcGFjaXR5PVxcXCIwLjE5ODc5NDE1OFxcXCIgZmlsbD1cXFwiI0ZGRkZGRlxcXCIgZmlsbC1vcGFjaXR5PVxcXCIwLjgxNjUxOTQ3NVxcXCI+XFxuICAgICAgICAgIDxwYXRoIGQ9XFxcIk0zMi42NTY4NTQyLDI5IEwzOC4zMTA2OTc4LDIzLjM0NjE1NjQgQzM5Ljg3NzEwMjEsMjEuNzc5NzUyMSAzOS44NzU4MDU3LDE5LjI0ODM4ODcgMzguMzEzNzA4NSwxNy42ODYyOTE1IEMzNi43NTQ3ODk5LDE2LjEyNzM3MjkgMzQuMjE3NjAzNSwxNi4xMjU1NDIyIDMyLjY1Mzg0MzYsMTcuNjg5MzAyMiBMMjcsMjMuMzQzMTQ1OCBMMjEuMzQ2MTU2NCwxNy42ODkzMDIyIEMxOS43ODIzOTY1LDE2LjEyNTU0MjIgMTcuMjQ1MjEwMSwxNi4xMjczNzI5IDE1LjY4NjI5MTUsMTcuNjg2MjkxNSBDMTQuMTI0MTk0MywxOS4yNDgzODg3IDE0LjEyMjg5NzksMjEuNzc5NzUyMSAxNS42ODkzMDIyLDIzLjM0NjE1NjQgTDIxLjM0MzE0NTgsMjkgTDE1LjY4OTMwMjIsMzQuNjUzODQzNiBDMTQuMTIyODk3OSwzNi4yMjAyNDc5IDE0LjEyNDE5NDMsMzguNzUxNjExMyAxNS42ODYyOTE1LDQwLjMxMzcwODUgQzE3LjI0NTIxMDEsNDEuODcyNjI3MSAxOS43ODIzOTY1LDQxLjg3NDQ1NzggMjEuMzQ2MTU2NCw0MC4zMTA2OTc4IEwyNywzNC42NTY4NTQyIEwzMi42NTM4NDM2LDQwLjMxMDY5NzggQzM0LjIxNzYwMzUsNDEuODc0NDU3OCAzNi43NTQ3ODk5LDQxLjg3MjYyNzEgMzguMzEzNzA4NSw0MC4zMTM3MDg1IEMzOS44NzU4MDU3LDM4Ljc1MTYxMTMgMzkuODc3MTAyMSwzNi4yMjAyNDc5IDM4LjMxMDY5NzgsMzQuNjUzODQzNiBMMzIuNjU2ODU0MiwyOSBaIE0yNyw1MyBDNDEuMzU5NDAzNSw1MyA1Myw0MS4zNTk0MDM1IDUzLDI3IEM1MywxMi42NDA1OTY1IDQxLjM1OTQwMzUsMSAyNywxIEMxMi42NDA1OTY1LDEgMSwxMi42NDA1OTY1IDEsMjcgQzEsNDEuMzU5NDAzNSAxMi42NDA1OTY1LDUzIDI3LDUzIFpcXFwiIGlkPVxcXCJPdmFsLTJcXFwiIHNrZXRjaDp0eXBlPVxcXCJNU1NoYXBlR3JvdXBcXFwiPjwvcGF0aD5cXG4gICAgICAgIDwvZz5cXG4gICAgICA8L2c+XFxuICAgIDwvc3ZnPlxcbiAgPC9kaXY+XFxuPC9kaXY+XCJcclxuICAgIH07XHJcblxyXG4gICAgZXh0ZW5kID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBrZXksIG9iamVjdCwgb2JqZWN0cywgdGFyZ2V0LCB2YWwsIF9pLCBfbGVuO1xyXG4gICAgICB0YXJnZXQgPSBhcmd1bWVudHNbMF0sIG9iamVjdHMgPSAyIDw9IGFyZ3VtZW50cy5sZW5ndGggPyBfX3NsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSA6IFtdO1xyXG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IG9iamVjdHMubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICBvYmplY3QgPSBvYmplY3RzW19pXTtcclxuICAgICAgICBmb3IgKGtleSBpbiBvYmplY3QpIHtcclxuICAgICAgICAgIHZhbCA9IG9iamVjdFtrZXldO1xyXG4gICAgICAgICAgdGFyZ2V0W2tleV0gPSB2YWw7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0YXJnZXQ7XHJcbiAgICB9O1xyXG5cclxuICAgIGZ1bmN0aW9uIERyb3B6b25lKGVsZW1lbnQsIG9wdGlvbnMpIHtcclxuICAgICAgdmFyIGVsZW1lbnRPcHRpb25zLCBmYWxsYmFjaywgX3JlZjtcclxuICAgICAgdGhpcy5lbGVtZW50ID0gZWxlbWVudDtcclxuICAgICAgdGhpcy52ZXJzaW9uID0gRHJvcHpvbmUudmVyc2lvbjtcclxuICAgICAgdGhpcy5kZWZhdWx0T3B0aW9ucy5wcmV2aWV3VGVtcGxhdGUgPSB0aGlzLmRlZmF1bHRPcHRpb25zLnByZXZpZXdUZW1wbGF0ZS5yZXBsYWNlKC9cXG4qL2csIFwiXCIpO1xyXG4gICAgICB0aGlzLmNsaWNrYWJsZUVsZW1lbnRzID0gW107XHJcbiAgICAgIHRoaXMubGlzdGVuZXJzID0gW107XHJcbiAgICAgIHRoaXMuZmlsZXMgPSBbXTtcclxuICAgICAgaWYgKHR5cGVvZiB0aGlzLmVsZW1lbnQgPT09IFwic3RyaW5nXCIpIHtcclxuICAgICAgICB0aGlzLmVsZW1lbnQgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKHRoaXMuZWxlbWVudCk7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKCEodGhpcy5lbGVtZW50ICYmICh0aGlzLmVsZW1lbnQubm9kZVR5cGUgIT0gbnVsbCkpKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBkcm9wem9uZSBlbGVtZW50LlwiKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5lbGVtZW50LmRyb3B6b25lKSB7XHJcbiAgICAgICAgdGhyb3cgbmV3IEVycm9yKFwiRHJvcHpvbmUgYWxyZWFkeSBhdHRhY2hlZC5cIik7XHJcbiAgICAgIH1cclxuICAgICAgRHJvcHpvbmUuaW5zdGFuY2VzLnB1c2godGhpcyk7XHJcbiAgICAgIHRoaXMuZWxlbWVudC5kcm9wem9uZSA9IHRoaXM7XHJcbiAgICAgIGVsZW1lbnRPcHRpb25zID0gKF9yZWYgPSBEcm9wem9uZS5vcHRpb25zRm9yRWxlbWVudCh0aGlzLmVsZW1lbnQpKSAhPSBudWxsID8gX3JlZiA6IHt9O1xyXG4gICAgICB0aGlzLm9wdGlvbnMgPSBleHRlbmQoe30sIHRoaXMuZGVmYXVsdE9wdGlvbnMsIGVsZW1lbnRPcHRpb25zLCBvcHRpb25zICE9IG51bGwgPyBvcHRpb25zIDoge30pO1xyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmZvcmNlRmFsbGJhY2sgfHwgIURyb3B6b25lLmlzQnJvd3NlclN1cHBvcnRlZCgpKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucy5mYWxsYmFjay5jYWxsKHRoaXMpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMudXJsID09IG51bGwpIHtcclxuICAgICAgICB0aGlzLm9wdGlvbnMudXJsID0gdGhpcy5lbGVtZW50LmdldEF0dHJpYnV0ZShcImFjdGlvblwiKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAoIXRoaXMub3B0aW9ucy51cmwpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBVUkwgcHJvdmlkZWQuXCIpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuYWNjZXB0ZWRGaWxlcyAmJiB0aGlzLm9wdGlvbnMuYWNjZXB0ZWRNaW1lVHlwZXMpIHtcclxuICAgICAgICB0aHJvdyBuZXcgRXJyb3IoXCJZb3UgY2FuJ3QgcHJvdmlkZSBib3RoICdhY2NlcHRlZEZpbGVzJyBhbmQgJ2FjY2VwdGVkTWltZVR5cGVzJy4gJ2FjY2VwdGVkTWltZVR5cGVzJyBpcyBkZXByZWNhdGVkLlwiKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmFjY2VwdGVkTWltZVR5cGVzKSB7XHJcbiAgICAgICAgdGhpcy5vcHRpb25zLmFjY2VwdGVkRmlsZXMgPSB0aGlzLm9wdGlvbnMuYWNjZXB0ZWRNaW1lVHlwZXM7XHJcbiAgICAgICAgZGVsZXRlIHRoaXMub3B0aW9ucy5hY2NlcHRlZE1pbWVUeXBlcztcclxuICAgICAgfVxyXG4gICAgICB0aGlzLm9wdGlvbnMubWV0aG9kID0gdGhpcy5vcHRpb25zLm1ldGhvZC50b1VwcGVyQ2FzZSgpO1xyXG4gICAgICBpZiAoKGZhbGxiYWNrID0gdGhpcy5nZXRFeGlzdGluZ0ZhbGxiYWNrKCkpICYmIGZhbGxiYWNrLnBhcmVudE5vZGUpIHtcclxuICAgICAgICBmYWxsYmFjay5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGZhbGxiYWNrKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLnByZXZpZXdzQ29udGFpbmVyICE9PSBmYWxzZSkge1xyXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMucHJldmlld3NDb250YWluZXIpIHtcclxuICAgICAgICAgIHRoaXMucHJldmlld3NDb250YWluZXIgPSBEcm9wem9uZS5nZXRFbGVtZW50KHRoaXMub3B0aW9ucy5wcmV2aWV3c0NvbnRhaW5lciwgXCJwcmV2aWV3c0NvbnRhaW5lclwiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgdGhpcy5wcmV2aWV3c0NvbnRhaW5lciA9IHRoaXMuZWxlbWVudDtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5jbGlja2FibGUpIHtcclxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmNsaWNrYWJsZSA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgdGhpcy5jbGlja2FibGVFbGVtZW50cyA9IFt0aGlzLmVsZW1lbnRdO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICB0aGlzLmNsaWNrYWJsZUVsZW1lbnRzID0gRHJvcHpvbmUuZ2V0RWxlbWVudHModGhpcy5vcHRpb25zLmNsaWNrYWJsZSwgXCJjbGlja2FibGVcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuaW5pdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5nZXRBY2NlcHRlZEZpbGVzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBmaWxlLCBfaSwgX2xlbiwgX3JlZiwgX3Jlc3VsdHM7XHJcbiAgICAgIF9yZWYgPSB0aGlzLmZpbGVzO1xyXG4gICAgICBfcmVzdWx0cyA9IFtdO1xyXG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICBmaWxlID0gX3JlZltfaV07XHJcbiAgICAgICAgaWYgKGZpbGUuYWNjZXB0ZWQpIHtcclxuICAgICAgICAgIF9yZXN1bHRzLnB1c2goZmlsZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBfcmVzdWx0cztcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmdldFJlamVjdGVkRmlsZXMgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIGZpbGUsIF9pLCBfbGVuLCBfcmVmLCBfcmVzdWx0cztcclxuICAgICAgX3JlZiA9IHRoaXMuZmlsZXM7XHJcbiAgICAgIF9yZXN1bHRzID0gW107XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgIGZpbGUgPSBfcmVmW19pXTtcclxuICAgICAgICBpZiAoIWZpbGUuYWNjZXB0ZWQpIHtcclxuICAgICAgICAgIF9yZXN1bHRzLnB1c2goZmlsZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBfcmVzdWx0cztcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmdldEZpbGVzV2l0aFN0YXR1cyA9IGZ1bmN0aW9uKHN0YXR1cykge1xyXG4gICAgICB2YXIgZmlsZSwgX2ksIF9sZW4sIF9yZWYsIF9yZXN1bHRzO1xyXG4gICAgICBfcmVmID0gdGhpcy5maWxlcztcclxuICAgICAgX3Jlc3VsdHMgPSBbXTtcclxuICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBfcmVmLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgZmlsZSA9IF9yZWZbX2ldO1xyXG4gICAgICAgIGlmIChmaWxlLnN0YXR1cyA9PT0gc3RhdHVzKSB7XHJcbiAgICAgICAgICBfcmVzdWx0cy5wdXNoKGZpbGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5nZXRRdWV1ZWRGaWxlcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5nZXRGaWxlc1dpdGhTdGF0dXMoRHJvcHpvbmUuUVVFVUVEKTtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmdldFVwbG9hZGluZ0ZpbGVzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLmdldEZpbGVzV2l0aFN0YXR1cyhEcm9wem9uZS5VUExPQURJTkcpO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuZ2V0QWRkZWRGaWxlcyA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICByZXR1cm4gdGhpcy5nZXRGaWxlc1dpdGhTdGF0dXMoRHJvcHpvbmUuQURERUQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuZ2V0QWN0aXZlRmlsZXMgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIGZpbGUsIF9pLCBfbGVuLCBfcmVmLCBfcmVzdWx0cztcclxuICAgICAgX3JlZiA9IHRoaXMuZmlsZXM7XHJcbiAgICAgIF9yZXN1bHRzID0gW107XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgIGZpbGUgPSBfcmVmW19pXTtcclxuICAgICAgICBpZiAoZmlsZS5zdGF0dXMgPT09IERyb3B6b25lLlVQTE9BRElORyB8fCBmaWxlLnN0YXR1cyA9PT0gRHJvcHpvbmUuUVVFVUVEKSB7XHJcbiAgICAgICAgICBfcmVzdWx0cy5wdXNoKGZpbGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5pbml0ID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBldmVudE5hbWUsIG5vUHJvcGFnYXRpb24sIHNldHVwSGlkZGVuRmlsZUlucHV0LCBfaSwgX2xlbiwgX3JlZiwgX3JlZjE7XHJcbiAgICAgIGlmICh0aGlzLmVsZW1lbnQudGFnTmFtZSA9PT0gXCJmb3JtXCIpIHtcclxuICAgICAgICB0aGlzLmVsZW1lbnQuc2V0QXR0cmlidXRlKFwiZW5jdHlwZVwiLCBcIm11bHRpcGFydC9mb3JtLWRhdGFcIik7XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuY29udGFpbnMoXCJkcm9wem9uZVwiKSAmJiAhdGhpcy5lbGVtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuZHotbWVzc2FnZVwiKSkge1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5hcHBlbmRDaGlsZChEcm9wem9uZS5jcmVhdGVFbGVtZW50KFwiPGRpdiBjbGFzcz1cXFwiZHotZGVmYXVsdCBkei1tZXNzYWdlXFxcIj48c3Bhbj5cIiArIHRoaXMub3B0aW9ucy5kaWN0RGVmYXVsdE1lc3NhZ2UgKyBcIjwvc3Bhbj48L2Rpdj5cIikpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmNsaWNrYWJsZUVsZW1lbnRzLmxlbmd0aCkge1xyXG4gICAgICAgIHNldHVwSGlkZGVuRmlsZUlucHV0ID0gKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGlmIChfdGhpcy5oaWRkZW5GaWxlSW5wdXQpIHtcclxuICAgICAgICAgICAgICBfdGhpcy5oaWRkZW5GaWxlSW5wdXQucGFyZW50Tm9kZS5yZW1vdmVDaGlsZChfdGhpcy5oaWRkZW5GaWxlSW5wdXQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF90aGlzLmhpZGRlbkZpbGVJbnB1dCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJpbnB1dFwiKTtcclxuICAgICAgICAgICAgX3RoaXMuaGlkZGVuRmlsZUlucHV0LnNldEF0dHJpYnV0ZShcInR5cGVcIiwgXCJmaWxlXCIpO1xyXG4gICAgICAgICAgICBpZiAoKF90aGlzLm9wdGlvbnMubWF4RmlsZXMgPT0gbnVsbCkgfHwgX3RoaXMub3B0aW9ucy5tYXhGaWxlcyA+IDEpIHtcclxuICAgICAgICAgICAgICBfdGhpcy5oaWRkZW5GaWxlSW5wdXQuc2V0QXR0cmlidXRlKFwibXVsdGlwbGVcIiwgXCJtdWx0aXBsZVwiKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBfdGhpcy5oaWRkZW5GaWxlSW5wdXQuY2xhc3NOYW1lID0gXCJkei1oaWRkZW4taW5wdXRcIjtcclxuICAgICAgICAgICAgaWYgKF90aGlzLm9wdGlvbnMuYWNjZXB0ZWRGaWxlcyAhPSBudWxsKSB7XHJcbiAgICAgICAgICAgICAgX3RoaXMuaGlkZGVuRmlsZUlucHV0LnNldEF0dHJpYnV0ZShcImFjY2VwdFwiLCBfdGhpcy5vcHRpb25zLmFjY2VwdGVkRmlsZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChfdGhpcy5vcHRpb25zLmNhcHR1cmUgIT0gbnVsbCkge1xyXG4gICAgICAgICAgICAgIF90aGlzLmhpZGRlbkZpbGVJbnB1dC5zZXRBdHRyaWJ1dGUoXCJjYXB0dXJlXCIsIF90aGlzLm9wdGlvbnMuY2FwdHVyZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgX3RoaXMuaGlkZGVuRmlsZUlucHV0LnN0eWxlLnZpc2liaWxpdHkgPSBcImhpZGRlblwiO1xyXG4gICAgICAgICAgICBfdGhpcy5oaWRkZW5GaWxlSW5wdXQuc3R5bGUucG9zaXRpb24gPSBcImFic29sdXRlXCI7XHJcbiAgICAgICAgICAgIF90aGlzLmhpZGRlbkZpbGVJbnB1dC5zdHlsZS50b3AgPSBcIjBcIjtcclxuICAgICAgICAgICAgICBfdGhpcy5oaWRkZW5GaWxlSW5wdXQuaWQgPSBcInVwbG9hZFwiO1xyXG4gICAgICAgICAgICAgIF90aGlzLmhpZGRlbkZpbGVJbnB1dC5zdHlsZS5sZWZ0ID0gXCIwXCI7XHJcbiAgICAgICAgICAgIF90aGlzLmhpZGRlbkZpbGVJbnB1dC5zdHlsZS5oZWlnaHQgPSBcIjBcIjtcclxuICAgICAgICAgICAgX3RoaXMuaGlkZGVuRmlsZUlucHV0LnN0eWxlLndpZHRoID0gXCIwXCI7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoX3RoaXMub3B0aW9ucy5oaWRkZW5JbnB1dENvbnRhaW5lcikuYXBwZW5kQ2hpbGQoX3RoaXMuaGlkZGVuRmlsZUlucHV0KTtcclxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLmhpZGRlbkZpbGVJbnB1dC5hZGRFdmVudExpc3RlbmVyKFwiY2hhbmdlXCIsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgIHZhciBmaWxlLCBmaWxlcywgX2ksIF9sZW47XHJcbiAgICAgICAgICAgICAgZmlsZXMgPSBfdGhpcy5oaWRkZW5GaWxlSW5wdXQuZmlsZXM7XHJcbiAgICAgICAgICAgICAgaWYgKGZpbGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBmaWxlcy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgICAgICAgICAgICBmaWxlID0gZmlsZXNbX2ldO1xyXG4gICAgICAgICAgICAgICAgICBfdGhpcy5hZGRGaWxlKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBfdGhpcy5lbWl0KFwiYWRkZWRmaWxlc1wiLCBmaWxlcyk7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIHNldHVwSGlkZGVuRmlsZUlucHV0KCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgfTtcclxuICAgICAgICB9KSh0aGlzKTtcclxuICAgICAgICBzZXR1cEhpZGRlbkZpbGVJbnB1dCgpO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuVVJMID0gKF9yZWYgPSB3aW5kb3cuVVJMKSAhPSBudWxsID8gX3JlZiA6IHdpbmRvdy53ZWJraXRVUkw7XHJcbiAgICAgIF9yZWYxID0gdGhpcy5ldmVudHM7XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZjEubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICBldmVudE5hbWUgPSBfcmVmMVtfaV07XHJcbiAgICAgICAgdGhpcy5vbihldmVudE5hbWUsIHRoaXMub3B0aW9uc1tldmVudE5hbWVdKTtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLm9uKFwidXBsb2FkcHJvZ3Jlc3NcIiwgKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgcmV0dXJuIF90aGlzLnVwZGF0ZVRvdGFsVXBsb2FkUHJvZ3Jlc3MoKTtcclxuICAgICAgICB9O1xyXG4gICAgICB9KSh0aGlzKSk7XHJcbiAgICAgIHRoaXMub24oXCJyZW1vdmVkZmlsZVwiLCAoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICByZXR1cm4gX3RoaXMudXBkYXRlVG90YWxVcGxvYWRQcm9ncmVzcygpO1xyXG4gICAgICAgIH07XHJcbiAgICAgIH0pKHRoaXMpKTtcclxuICAgICAgdGhpcy5vbihcImNhbmNlbGVkXCIsIChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgICAgICByZXR1cm4gX3RoaXMuZW1pdChcImNvbXBsZXRlXCIsIGZpbGUpO1xyXG4gICAgICAgIH07XHJcbiAgICAgIH0pKHRoaXMpKTtcclxuICAgICAgdGhpcy5vbihcImNvbXBsZXRlXCIsIChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgICAgICBpZiAoX3RoaXMuZ2V0QWRkZWRGaWxlcygpLmxlbmd0aCA9PT0gMCAmJiBfdGhpcy5nZXRVcGxvYWRpbmdGaWxlcygpLmxlbmd0aCA9PT0gMCAmJiBfdGhpcy5nZXRRdWV1ZWRGaWxlcygpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gc2V0VGltZW91dCgoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmVtaXQoXCJxdWV1ZWNvbXBsZXRlXCIpO1xyXG4gICAgICAgICAgICB9KSwgMCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgfSkodGhpcykpO1xyXG4gICAgICBub1Byb3BhZ2F0aW9uID0gZnVuY3Rpb24oZSkge1xyXG4gICAgICAgIGUuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgaWYgKGUucHJldmVudERlZmF1bHQpIHtcclxuICAgICAgICAgIHJldHVybiBlLnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHJldHVybiBlLnJldHVyblZhbHVlID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmxpc3RlbmVycyA9IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICBlbGVtZW50OiB0aGlzLmVsZW1lbnQsXHJcbiAgICAgICAgICBldmVudHM6IHtcclxuICAgICAgICAgICAgXCJkcmFnc3RhcnRcIjogKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5lbWl0KFwiZHJhZ3N0YXJ0XCIsIGUpO1xyXG4gICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pKHRoaXMpLFxyXG4gICAgICAgICAgICBcImRyYWdlbnRlclwiOiAoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICAgICAgbm9Qcm9wYWdhdGlvbihlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5lbWl0KFwiZHJhZ2VudGVyXCIsIGUpO1xyXG4gICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pKHRoaXMpLFxyXG4gICAgICAgICAgICBcImRyYWdvdmVyXCI6IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZWZjdDtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgIGVmY3QgPSBlLmRhdGFUcmFuc2Zlci5lZmZlY3RBbGxvd2VkO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoX2Vycm9yKSB7fVxyXG4gICAgICAgICAgICAgICAgZS5kYXRhVHJhbnNmZXIuZHJvcEVmZmVjdCA9ICdtb3ZlJyA9PT0gZWZjdCB8fCAnbGlua01vdmUnID09PSBlZmN0ID8gJ21vdmUnIDogJ2NvcHknO1xyXG4gICAgICAgICAgICAgICAgbm9Qcm9wYWdhdGlvbihlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5lbWl0KFwiZHJhZ292ZXJcIiwgZSk7XHJcbiAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSkodGhpcyksXHJcbiAgICAgICAgICAgIFwiZHJhZ2xlYXZlXCI6IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gX3RoaXMuZW1pdChcImRyYWdsZWF2ZVwiLCBlKTtcclxuICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9KSh0aGlzKSxcclxuICAgICAgICAgICAgXCJkcm9wXCI6IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICAgICAgICBub1Byb3BhZ2F0aW9uKGUpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmRyb3AoZSk7XHJcbiAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfSkodGhpcyksXHJcbiAgICAgICAgICAgIFwiZHJhZ2VuZFwiOiAoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIF90aGlzLmVtaXQoXCJkcmFnZW5kXCIsIGUpO1xyXG4gICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH0pKHRoaXMpXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICBdO1xyXG4gICAgICB0aGlzLmNsaWNrYWJsZUVsZW1lbnRzLmZvckVhY2goKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKGNsaWNrYWJsZUVsZW1lbnQpIHtcclxuICAgICAgICAgIHJldHVybiBfdGhpcy5saXN0ZW5lcnMucHVzaCh7XHJcbiAgICAgICAgICAgIGVsZW1lbnQ6IGNsaWNrYWJsZUVsZW1lbnQsXHJcbiAgICAgICAgICAgIGV2ZW50czoge1xyXG4gICAgICAgICAgICAgIFwiY2xpY2tcIjogZnVuY3Rpb24oZXZ0KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoKGNsaWNrYWJsZUVsZW1lbnQgIT09IF90aGlzLmVsZW1lbnQpIHx8IChldnQudGFyZ2V0ID09PSBfdGhpcy5lbGVtZW50IHx8IERyb3B6b25lLmVsZW1lbnRJbnNpZGUoZXZ0LnRhcmdldCwgX3RoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yKFwiLmR6LW1lc3NhZ2VcIikpKSkge1xyXG4gICAgICAgICAgICAgICAgICBfdGhpcy5oaWRkZW5GaWxlSW5wdXQuY2xpY2soKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfSkodGhpcykpO1xyXG4gICAgICB0aGlzLmVuYWJsZSgpO1xyXG4gICAgICByZXR1cm4gdGhpcy5vcHRpb25zLmluaXQuY2FsbCh0aGlzKTtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmRlc3Ryb3kgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIF9yZWY7XHJcbiAgICAgIHRoaXMuZGlzYWJsZSgpO1xyXG4gICAgICB0aGlzLnJlbW92ZUFsbEZpbGVzKHRydWUpO1xyXG4gICAgICBpZiAoKF9yZWYgPSB0aGlzLmhpZGRlbkZpbGVJbnB1dCkgIT0gbnVsbCA/IF9yZWYucGFyZW50Tm9kZSA6IHZvaWQgMCkge1xyXG4gICAgICAgIHRoaXMuaGlkZGVuRmlsZUlucHV0LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5oaWRkZW5GaWxlSW5wdXQpO1xyXG4gICAgICAgIHRoaXMuaGlkZGVuRmlsZUlucHV0ID0gbnVsbDtcclxuICAgICAgfVxyXG4gICAgICBkZWxldGUgdGhpcy5lbGVtZW50LmRyb3B6b25lO1xyXG4gICAgICByZXR1cm4gRHJvcHpvbmUuaW5zdGFuY2VzLnNwbGljZShEcm9wem9uZS5pbnN0YW5jZXMuaW5kZXhPZih0aGlzKSwgMSk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS51cGRhdGVUb3RhbFVwbG9hZFByb2dyZXNzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBhY3RpdmVGaWxlcywgZmlsZSwgdG90YWxCeXRlcywgdG90YWxCeXRlc1NlbnQsIHRvdGFsVXBsb2FkUHJvZ3Jlc3MsIF9pLCBfbGVuLCBfcmVmO1xyXG4gICAgICB0b3RhbEJ5dGVzU2VudCA9IDA7XHJcbiAgICAgIHRvdGFsQnl0ZXMgPSAwO1xyXG4gICAgICBhY3RpdmVGaWxlcyA9IHRoaXMuZ2V0QWN0aXZlRmlsZXMoKTtcclxuICAgICAgaWYgKGFjdGl2ZUZpbGVzLmxlbmd0aCkge1xyXG4gICAgICAgIF9yZWYgPSB0aGlzLmdldEFjdGl2ZUZpbGVzKCk7XHJcbiAgICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBfcmVmLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgICBmaWxlID0gX3JlZltfaV07XHJcbiAgICAgICAgICB0b3RhbEJ5dGVzU2VudCArPSBmaWxlLnVwbG9hZC5ieXRlc1NlbnQ7XHJcbiAgICAgICAgICB0b3RhbEJ5dGVzICs9IGZpbGUudXBsb2FkLnRvdGFsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0b3RhbFVwbG9hZFByb2dyZXNzID0gMTAwICogdG90YWxCeXRlc1NlbnQgLyB0b3RhbEJ5dGVzO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRvdGFsVXBsb2FkUHJvZ3Jlc3MgPSAxMDA7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIHRoaXMuZW1pdChcInRvdGFsdXBsb2FkcHJvZ3Jlc3NcIiwgdG90YWxVcGxvYWRQcm9ncmVzcywgdG90YWxCeXRlcywgdG90YWxCeXRlc1NlbnQpO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuX2dldFBhcmFtTmFtZSA9IGZ1bmN0aW9uKG4pIHtcclxuICAgICAgaWYgKHR5cGVvZiB0aGlzLm9wdGlvbnMucGFyYW1OYW1lID09PSBcImZ1bmN0aW9uXCIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zLnBhcmFtTmFtZShuKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gXCJcIiArIHRoaXMub3B0aW9ucy5wYXJhbU5hbWUgKyAodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlID8gXCJbXCIgKyBuICsgXCJdXCIgOiBcIlwiKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuZ2V0RmFsbGJhY2tGb3JtID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBleGlzdGluZ0ZhbGxiYWNrLCBmaWVsZHMsIGZpZWxkc1N0cmluZywgZm9ybTtcclxuICAgICAgaWYgKGV4aXN0aW5nRmFsbGJhY2sgPSB0aGlzLmdldEV4aXN0aW5nRmFsbGJhY2soKSkge1xyXG4gICAgICAgIHJldHVybiBleGlzdGluZ0ZhbGxiYWNrO1xyXG4gICAgICB9XHJcbiAgICAgIGZpZWxkc1N0cmluZyA9IFwiPGRpdiBjbGFzcz1cXFwiZHotZmFsbGJhY2tcXFwiPlwiO1xyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmRpY3RGYWxsYmFja1RleHQpIHtcclxuICAgICAgICBmaWVsZHNTdHJpbmcgKz0gXCI8cD5cIiArIHRoaXMub3B0aW9ucy5kaWN0RmFsbGJhY2tUZXh0ICsgXCI8L3A+XCI7XHJcbiAgICAgIH1cclxuICAgICAgZmllbGRzU3RyaW5nICs9IFwiPGlucHV0IHR5cGU9XFxcImZpbGVcXFwiIG5hbWU9XFxcIlwiICsgKHRoaXMuX2dldFBhcmFtTmFtZSgwKSkgKyBcIlxcXCIgXCIgKyAodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlID8gJ211bHRpcGxlPVwibXVsdGlwbGVcIicgOiB2b2lkIDApICsgXCIgLz48aW5wdXQgdHlwZT1cXFwic3VibWl0XFxcIiB2YWx1ZT1cXFwiVXBsb2FkIVxcXCI+PC9kaXY+XCI7XHJcbiAgICAgIGZpZWxkcyA9IERyb3B6b25lLmNyZWF0ZUVsZW1lbnQoZmllbGRzU3RyaW5nKTtcclxuICAgICAgaWYgKHRoaXMuZWxlbWVudC50YWdOYW1lICE9PSBcIkZPUk1cIikge1xyXG4gICAgICAgIGZvcm0gPSBEcm9wem9uZS5jcmVhdGVFbGVtZW50KFwiPGZvcm0gYWN0aW9uPVxcXCJcIiArIHRoaXMub3B0aW9ucy51cmwgKyBcIlxcXCIgZW5jdHlwZT1cXFwibXVsdGlwYXJ0L2Zvcm0tZGF0YVxcXCIgbWV0aG9kPVxcXCJcIiArIHRoaXMub3B0aW9ucy5tZXRob2QgKyBcIlxcXCI+PC9mb3JtPlwiKTtcclxuICAgICAgICBmb3JtLmFwcGVuZENoaWxkKGZpZWxkcyk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LnNldEF0dHJpYnV0ZShcImVuY3R5cGVcIiwgXCJtdWx0aXBhcnQvZm9ybS1kYXRhXCIpO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5zZXRBdHRyaWJ1dGUoXCJtZXRob2RcIiwgdGhpcy5vcHRpb25zLm1ldGhvZCk7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIGZvcm0gIT0gbnVsbCA/IGZvcm0gOiBmaWVsZHM7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5nZXRFeGlzdGluZ0ZhbGxiYWNrID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBmYWxsYmFjaywgZ2V0RmFsbGJhY2ssIHRhZ05hbWUsIF9pLCBfbGVuLCBfcmVmO1xyXG4gICAgICBnZXRGYWxsYmFjayA9IGZ1bmN0aW9uKGVsZW1lbnRzKSB7XHJcbiAgICAgICAgdmFyIGVsLCBfaSwgX2xlbjtcclxuICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGVsZW1lbnRzLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgICBlbCA9IGVsZW1lbnRzW19pXTtcclxuICAgICAgICAgIGlmICgvKF58IClmYWxsYmFjaygkfCApLy50ZXN0KGVsLmNsYXNzTmFtZSkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGVsO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfTtcclxuICAgICAgX3JlZiA9IFtcImRpdlwiLCBcImZvcm1cIl07XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgIHRhZ05hbWUgPSBfcmVmW19pXTtcclxuICAgICAgICBpZiAoZmFsbGJhY2sgPSBnZXRGYWxsYmFjayh0aGlzLmVsZW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUodGFnTmFtZSkpKSB7XHJcbiAgICAgICAgICByZXR1cm4gZmFsbGJhY2s7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5zZXR1cEV2ZW50TGlzdGVuZXJzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBlbGVtZW50TGlzdGVuZXJzLCBldmVudCwgbGlzdGVuZXIsIF9pLCBfbGVuLCBfcmVmLCBfcmVzdWx0cztcclxuICAgICAgX3JlZiA9IHRoaXMubGlzdGVuZXJzO1xyXG4gICAgICBfcmVzdWx0cyA9IFtdO1xyXG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICBlbGVtZW50TGlzdGVuZXJzID0gX3JlZltfaV07XHJcbiAgICAgICAgX3Jlc3VsdHMucHVzaCgoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICB2YXIgX3JlZjEsIF9yZXN1bHRzMTtcclxuICAgICAgICAgIF9yZWYxID0gZWxlbWVudExpc3RlbmVycy5ldmVudHM7XHJcbiAgICAgICAgICBfcmVzdWx0czEgPSBbXTtcclxuICAgICAgICAgIGZvciAoZXZlbnQgaW4gX3JlZjEpIHtcclxuICAgICAgICAgICAgbGlzdGVuZXIgPSBfcmVmMVtldmVudF07XHJcbiAgICAgICAgICAgIF9yZXN1bHRzMS5wdXNoKGVsZW1lbnRMaXN0ZW5lcnMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKGV2ZW50LCBsaXN0ZW5lciwgZmFsc2UpKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBfcmVzdWx0czE7XHJcbiAgICAgICAgfSkoKSk7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIF9yZXN1bHRzO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUucmVtb3ZlRXZlbnRMaXN0ZW5lcnMgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIGVsZW1lbnRMaXN0ZW5lcnMsIGV2ZW50LCBsaXN0ZW5lciwgX2ksIF9sZW4sIF9yZWYsIF9yZXN1bHRzO1xyXG4gICAgICBfcmVmID0gdGhpcy5saXN0ZW5lcnM7XHJcbiAgICAgIF9yZXN1bHRzID0gW107XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgIGVsZW1lbnRMaXN0ZW5lcnMgPSBfcmVmW19pXTtcclxuICAgICAgICBfcmVzdWx0cy5wdXNoKChmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciBfcmVmMSwgX3Jlc3VsdHMxO1xyXG4gICAgICAgICAgX3JlZjEgPSBlbGVtZW50TGlzdGVuZXJzLmV2ZW50cztcclxuICAgICAgICAgIF9yZXN1bHRzMSA9IFtdO1xyXG4gICAgICAgICAgZm9yIChldmVudCBpbiBfcmVmMSkge1xyXG4gICAgICAgICAgICBsaXN0ZW5lciA9IF9yZWYxW2V2ZW50XTtcclxuICAgICAgICAgICAgX3Jlc3VsdHMxLnB1c2goZWxlbWVudExpc3RlbmVycy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyLCBmYWxzZSkpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIF9yZXN1bHRzMTtcclxuICAgICAgICB9KSgpKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5kaXNhYmxlID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBmaWxlLCBfaSwgX2xlbiwgX3JlZiwgX3Jlc3VsdHM7XHJcbiAgICAgIHRoaXMuY2xpY2thYmxlRWxlbWVudHMuZm9yRWFjaChmdW5jdGlvbihlbGVtZW50KSB7XHJcbiAgICAgICAgcmV0dXJuIGVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImR6LWNsaWNrYWJsZVwiKTtcclxuICAgICAgfSk7XHJcbiAgICAgIHRoaXMucmVtb3ZlRXZlbnRMaXN0ZW5lcnMoKTtcclxuICAgICAgX3JlZiA9IHRoaXMuZmlsZXM7XHJcbiAgICAgIF9yZXN1bHRzID0gW107XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gX3JlZi5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgIGZpbGUgPSBfcmVmW19pXTtcclxuICAgICAgICBfcmVzdWx0cy5wdXNoKHRoaXMuY2FuY2VsVXBsb2FkKGZpbGUpKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5lbmFibGUgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdGhpcy5jbGlja2FibGVFbGVtZW50cy5mb3JFYWNoKGZ1bmN0aW9uKGVsZW1lbnQpIHtcclxuICAgICAgICByZXR1cm4gZWxlbWVudC5jbGFzc0xpc3QuYWRkKFwiZHotY2xpY2thYmxlXCIpO1xyXG4gICAgICB9KTtcclxuICAgICAgcmV0dXJuIHRoaXMuc2V0dXBFdmVudExpc3RlbmVycygpO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuZmlsZXNpemUgPSBmdW5jdGlvbihzaXplKSB7XHJcbiAgICAgIHZhciBjdXRvZmYsIGksIHNlbGVjdGVkU2l6ZSwgc2VsZWN0ZWRVbml0LCB1bml0LCB1bml0cywgX2ksIF9sZW47XHJcbiAgICAgIHNlbGVjdGVkU2l6ZSA9IDA7XHJcbiAgICAgIHNlbGVjdGVkVW5pdCA9IFwiYlwiO1xyXG4gICAgICBpZiAoc2l6ZSA+IDApIHtcclxuICAgICAgICB1bml0cyA9IFsnVEInLCAnR0InLCAnTUInLCAnS0InLCAnYiddO1xyXG4gICAgICAgIGZvciAoaSA9IF9pID0gMCwgX2xlbiA9IHVuaXRzLmxlbmd0aDsgX2kgPCBfbGVuOyBpID0gKytfaSkge1xyXG4gICAgICAgICAgdW5pdCA9IHVuaXRzW2ldO1xyXG4gICAgICAgICAgY3V0b2ZmID0gTWF0aC5wb3codGhpcy5vcHRpb25zLmZpbGVzaXplQmFzZSwgNCAtIGkpIC8gMTA7XHJcbiAgICAgICAgICBpZiAoc2l6ZSA+PSBjdXRvZmYpIHtcclxuICAgICAgICAgICAgc2VsZWN0ZWRTaXplID0gc2l6ZSAvIE1hdGgucG93KHRoaXMub3B0aW9ucy5maWxlc2l6ZUJhc2UsIDQgLSBpKTtcclxuICAgICAgICAgICAgc2VsZWN0ZWRVbml0ID0gdW5pdDtcclxuICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHNlbGVjdGVkU2l6ZSA9IE1hdGgucm91bmQoMTAgKiBzZWxlY3RlZFNpemUpIC8gMTA7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIFwiPHN0cm9uZz5cIiArIHNlbGVjdGVkU2l6ZSArIFwiPC9zdHJvbmc+IFwiICsgc2VsZWN0ZWRVbml0O1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuX3VwZGF0ZU1heEZpbGVzUmVhY2hlZENsYXNzID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIGlmICgodGhpcy5vcHRpb25zLm1heEZpbGVzICE9IG51bGwpICYmIHRoaXMuZ2V0QWNjZXB0ZWRGaWxlcygpLmxlbmd0aCA+PSB0aGlzLm9wdGlvbnMubWF4RmlsZXMpIHtcclxuICAgICAgICBpZiAodGhpcy5nZXRBY2NlcHRlZEZpbGVzKCkubGVuZ3RoID09PSB0aGlzLm9wdGlvbnMubWF4RmlsZXMpIHtcclxuICAgICAgICAgIHRoaXMuZW1pdCgnbWF4ZmlsZXNyZWFjaGVkJywgdGhpcy5maWxlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmFkZChcImR6LW1heC1maWxlcy1yZWFjaGVkXCIpO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImR6LW1heC1maWxlcy1yZWFjaGVkXCIpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5kcm9wID0gZnVuY3Rpb24oZSkge1xyXG4gICAgICB2YXIgZmlsZXMsIGl0ZW1zO1xyXG4gICAgICBpZiAoIWUuZGF0YVRyYW5zZmVyKSB7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHRoaXMuZW1pdChcImRyb3BcIiwgZSk7XHJcbiAgICAgIGZpbGVzID0gZS5kYXRhVHJhbnNmZXIuZmlsZXM7XHJcbiAgICAgIHRoaXMuZW1pdChcImFkZGVkZmlsZXNcIiwgZmlsZXMpO1xyXG4gICAgICBpZiAoZmlsZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgaXRlbXMgPSBlLmRhdGFUcmFuc2Zlci5pdGVtcztcclxuICAgICAgICBpZiAoaXRlbXMgJiYgaXRlbXMubGVuZ3RoICYmIChpdGVtc1swXS53ZWJraXRHZXRBc0VudHJ5ICE9IG51bGwpKSB7XHJcbiAgICAgICAgICB0aGlzLl9hZGRGaWxlc0Zyb21JdGVtcyhpdGVtcyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgIHRoaXMuaGFuZGxlRmlsZXMoZmlsZXMpO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUucGFzdGUgPSBmdW5jdGlvbihlKSB7XHJcbiAgICAgIHZhciBpdGVtcywgX3JlZjtcclxuICAgICAgaWYgKChlICE9IG51bGwgPyAoX3JlZiA9IGUuY2xpcGJvYXJkRGF0YSkgIT0gbnVsbCA/IF9yZWYuaXRlbXMgOiB2b2lkIDAgOiB2b2lkIDApID09IG51bGwpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5lbWl0KFwicGFzdGVcIiwgZSk7XHJcbiAgICAgIGl0ZW1zID0gZS5jbGlwYm9hcmREYXRhLml0ZW1zO1xyXG4gICAgICBpZiAoaXRlbXMubGVuZ3RoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2FkZEZpbGVzRnJvbUl0ZW1zKGl0ZW1zKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuaGFuZGxlRmlsZXMgPSBmdW5jdGlvbihmaWxlcykge1xyXG4gICAgICB2YXIgZmlsZSwgX2ksIF9sZW4sIF9yZXN1bHRzO1xyXG4gICAgICBfcmVzdWx0cyA9IFtdO1xyXG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGZpbGVzLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgZmlsZSA9IGZpbGVzW19pXTtcclxuICAgICAgICBfcmVzdWx0cy5wdXNoKHRoaXMuYWRkRmlsZShmaWxlKSk7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIF9yZXN1bHRzO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuX2FkZEZpbGVzRnJvbUl0ZW1zID0gZnVuY3Rpb24oaXRlbXMpIHtcclxuICAgICAgdmFyIGVudHJ5LCBpdGVtLCBfaSwgX2xlbiwgX3Jlc3VsdHM7XHJcbiAgICAgIF9yZXN1bHRzID0gW107XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gaXRlbXMubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICBpdGVtID0gaXRlbXNbX2ldO1xyXG4gICAgICAgIGlmICgoaXRlbS53ZWJraXRHZXRBc0VudHJ5ICE9IG51bGwpICYmIChlbnRyeSA9IGl0ZW0ud2Via2l0R2V0QXNFbnRyeSgpKSkge1xyXG4gICAgICAgICAgaWYgKGVudHJ5LmlzRmlsZSkge1xyXG4gICAgICAgICAgICBfcmVzdWx0cy5wdXNoKHRoaXMuYWRkRmlsZShpdGVtLmdldEFzRmlsZSgpKSk7XHJcbiAgICAgICAgICB9IGVsc2UgaWYgKGVudHJ5LmlzRGlyZWN0b3J5KSB7XHJcbiAgICAgICAgICAgIF9yZXN1bHRzLnB1c2godGhpcy5fYWRkRmlsZXNGcm9tRGlyZWN0b3J5KGVudHJ5LCBlbnRyeS5uYW1lKSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBfcmVzdWx0cy5wdXNoKHZvaWQgMCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmIChpdGVtLmdldEFzRmlsZSAhPSBudWxsKSB7XHJcbiAgICAgICAgICBpZiAoKGl0ZW0ua2luZCA9PSBudWxsKSB8fCBpdGVtLmtpbmQgPT09IFwiZmlsZVwiKSB7XHJcbiAgICAgICAgICAgIF9yZXN1bHRzLnB1c2godGhpcy5hZGRGaWxlKGl0ZW0uZ2V0QXNGaWxlKCkpKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIF9yZXN1bHRzLnB1c2godm9pZCAwKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgX3Jlc3VsdHMucHVzaCh2b2lkIDApO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gX3Jlc3VsdHM7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5fYWRkRmlsZXNGcm9tRGlyZWN0b3J5ID0gZnVuY3Rpb24oZGlyZWN0b3J5LCBwYXRoKSB7XHJcbiAgICAgIHZhciBkaXJSZWFkZXIsIGVudHJpZXNSZWFkZXI7XHJcbiAgICAgIGRpclJlYWRlciA9IGRpcmVjdG9yeS5jcmVhdGVSZWFkZXIoKTtcclxuICAgICAgZW50cmllc1JlYWRlciA9IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbihlbnRyaWVzKSB7XHJcbiAgICAgICAgICB2YXIgZW50cnksIF9pLCBfbGVuO1xyXG4gICAgICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBlbnRyaWVzLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgICAgIGVudHJ5ID0gZW50cmllc1tfaV07XHJcbiAgICAgICAgICAgIGlmIChlbnRyeS5pc0ZpbGUpIHtcclxuICAgICAgICAgICAgICBlbnRyeS5maWxlKGZ1bmN0aW9uKGZpbGUpIHtcclxuICAgICAgICAgICAgICAgIGlmIChfdGhpcy5vcHRpb25zLmlnbm9yZUhpZGRlbkZpbGVzICYmIGZpbGUubmFtZS5zdWJzdHJpbmcoMCwgMSkgPT09ICcuJykge1xyXG4gICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBmaWxlLmZ1bGxQYXRoID0gXCJcIiArIHBhdGggKyBcIi9cIiArIGZpbGUubmFtZTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBfdGhpcy5hZGRGaWxlKGZpbGUpO1xyXG4gICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGVudHJ5LmlzRGlyZWN0b3J5KSB7XHJcbiAgICAgICAgICAgICAgX3RoaXMuX2FkZEZpbGVzRnJvbURpcmVjdG9yeShlbnRyeSwgXCJcIiArIHBhdGggKyBcIi9cIiArIGVudHJ5Lm5hbWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgfSkodGhpcyk7XHJcbiAgICAgIHJldHVybiBkaXJSZWFkZXIucmVhZEVudHJpZXMoZW50cmllc1JlYWRlciwgZnVuY3Rpb24oZXJyb3IpIHtcclxuICAgICAgICByZXR1cm4gdHlwZW9mIGNvbnNvbGUgIT09IFwidW5kZWZpbmVkXCIgJiYgY29uc29sZSAhPT0gbnVsbCA/IHR5cGVvZiBjb25zb2xlLmxvZyA9PT0gXCJmdW5jdGlvblwiID8gY29uc29sZS5sb2coZXJyb3IpIDogdm9pZCAwIDogdm9pZCAwO1xyXG4gICAgICB9KTtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmFjY2VwdCA9IGZ1bmN0aW9uKGZpbGUsIGRvbmUpIHtcclxuICAgICAgaWYgKGZpbGUuc2l6ZSA+IHRoaXMub3B0aW9ucy5tYXhGaWxlc2l6ZSAqIDEwMjQgKiAxMDI0KSB7XHJcbiAgICAgICAgcmV0dXJuIGRvbmUodGhpcy5vcHRpb25zLmRpY3RGaWxlVG9vQmlnLnJlcGxhY2UoXCJ7e2ZpbGVzaXplfX1cIiwgTWF0aC5yb3VuZChmaWxlLnNpemUgLyAxMDI0IC8gMTAuMjQpIC8gMTAwKS5yZXBsYWNlKFwie3ttYXhGaWxlc2l6ZX19XCIsIHRoaXMub3B0aW9ucy5tYXhGaWxlc2l6ZSkpO1xyXG4gICAgICB9IGVsc2UgaWYgKCFEcm9wem9uZS5pc1ZhbGlkRmlsZShmaWxlLCB0aGlzLm9wdGlvbnMuYWNjZXB0ZWRGaWxlcykpIHtcclxuICAgICAgICByZXR1cm4gZG9uZSh0aGlzLm9wdGlvbnMuZGljdEludmFsaWRGaWxlVHlwZSk7XHJcbiAgICAgIH0gZWxzZSBpZiAoKHRoaXMub3B0aW9ucy5tYXhGaWxlcyAhPSBudWxsKSAmJiB0aGlzLmdldEFjY2VwdGVkRmlsZXMoKS5sZW5ndGggPj0gdGhpcy5vcHRpb25zLm1heEZpbGVzKSB7XHJcbiAgICAgICAgZG9uZSh0aGlzLm9wdGlvbnMuZGljdE1heEZpbGVzRXhjZWVkZWQucmVwbGFjZShcInt7bWF4RmlsZXN9fVwiLCB0aGlzLm9wdGlvbnMubWF4RmlsZXMpKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbWl0KFwibWF4ZmlsZXNleGNlZWRlZFwiLCBmaWxlKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zLmFjY2VwdC5jYWxsKHRoaXMsIGZpbGUsIGRvbmUpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5hZGRGaWxlID0gZnVuY3Rpb24oZmlsZSkge1xyXG4gICAgICBmaWxlLnVwbG9hZCA9IHtcclxuICAgICAgICBwcm9ncmVzczogMCxcclxuICAgICAgICB0b3RhbDogZmlsZS5zaXplLFxyXG4gICAgICAgIGJ5dGVzU2VudDogMFxyXG4gICAgICB9O1xyXG4gICAgICB0aGlzLmZpbGVzLnB1c2goZmlsZSk7XHJcbiAgICAgIGZpbGUuc3RhdHVzID0gRHJvcHpvbmUuQURERUQ7XHJcbiAgICAgIHRoaXMuZW1pdChcImFkZGVkZmlsZVwiLCBmaWxlKTtcclxuICAgICAgdGhpcy5fZW5xdWV1ZVRodW1ibmFpbChmaWxlKTtcclxuICAgICAgcmV0dXJuIHRoaXMuYWNjZXB0KGZpbGUsIChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbihlcnJvcikge1xyXG4gICAgICAgICAgaWYgKGVycm9yKSB7XHJcbiAgICAgICAgICAgIGZpbGUuYWNjZXB0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgX3RoaXMuX2Vycm9yUHJvY2Vzc2luZyhbZmlsZV0sIGVycm9yKTtcclxuICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGZpbGUuYWNjZXB0ZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICBpZiAoX3RoaXMub3B0aW9ucy5hdXRvUXVldWUpIHtcclxuICAgICAgICAgICAgICBfdGhpcy5lbnF1ZXVlRmlsZShmaWxlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgICAgcmV0dXJuIF90aGlzLl91cGRhdGVNYXhGaWxlc1JlYWNoZWRDbGFzcygpO1xyXG4gICAgICAgIH07XHJcbiAgICAgIH0pKHRoaXMpKTtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmVucXVldWVGaWxlcyA9IGZ1bmN0aW9uKGZpbGVzKSB7XHJcbiAgICAgIHZhciBmaWxlLCBfaSwgX2xlbjtcclxuICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBmaWxlcy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgIGZpbGUgPSBmaWxlc1tfaV07XHJcbiAgICAgICAgdGhpcy5lbnF1ZXVlRmlsZShmaWxlKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gbnVsbDtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmVucXVldWVGaWxlID0gZnVuY3Rpb24oZmlsZSkge1xyXG4gICAgICBpZiAoZmlsZS5zdGF0dXMgPT09IERyb3B6b25lLkFEREVEICYmIGZpbGUuYWNjZXB0ZWQgPT09IHRydWUpIHtcclxuICAgICAgICBmaWxlLnN0YXR1cyA9IERyb3B6b25lLlFVRVVFRDtcclxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLmF1dG9Qcm9jZXNzUXVldWUpIHtcclxuICAgICAgICAgIHJldHVybiBzZXRUaW1lb3V0KCgoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgIHJldHVybiBfdGhpcy5wcm9jZXNzUXVldWUoKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgIH0pKHRoaXMpKSwgMCk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIHRocm93IG5ldyBFcnJvcihcIlRoaXMgZmlsZSBjYW4ndCBiZSBxdWV1ZWQgYmVjYXVzZSBpdCBoYXMgYWxyZWFkeSBiZWVuIHByb2Nlc3NlZCBvciB3YXMgcmVqZWN0ZWQuXCIpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5fdGh1bWJuYWlsUXVldWUgPSBbXTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuX3Byb2Nlc3NpbmdUaHVtYm5haWwgPSBmYWxzZTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuX2VucXVldWVUaHVtYm5haWwgPSBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuY3JlYXRlSW1hZ2VUaHVtYm5haWxzICYmIGZpbGUudHlwZS5tYXRjaCgvaW1hZ2UuKi8pICYmIGZpbGUuc2l6ZSA8PSB0aGlzLm9wdGlvbnMubWF4VGh1bWJuYWlsRmlsZXNpemUgKiAxMDI0ICogMTAyNCkge1xyXG4gICAgICAgIHRoaXMuX3RodW1ibmFpbFF1ZXVlLnB1c2goZmlsZSk7XHJcbiAgICAgICAgcmV0dXJuIHNldFRpbWVvdXQoKChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICByZXR1cm4gX3RoaXMuX3Byb2Nlc3NUaHVtYm5haWxRdWV1ZSgpO1xyXG4gICAgICAgICAgfTtcclxuICAgICAgICB9KSh0aGlzKSksIDApO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5fcHJvY2Vzc1RodW1ibmFpbFF1ZXVlID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIGlmICh0aGlzLl9wcm9jZXNzaW5nVGh1bWJuYWlsIHx8IHRoaXMuX3RodW1ibmFpbFF1ZXVlLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICB0aGlzLl9wcm9jZXNzaW5nVGh1bWJuYWlsID0gdHJ1ZTtcclxuICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlVGh1bWJuYWlsKHRoaXMuX3RodW1ibmFpbFF1ZXVlLnNoaWZ0KCksIChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIF90aGlzLl9wcm9jZXNzaW5nVGh1bWJuYWlsID0gZmFsc2U7XHJcbiAgICAgICAgICByZXR1cm4gX3RoaXMuX3Byb2Nlc3NUaHVtYm5haWxRdWV1ZSgpO1xyXG4gICAgICAgIH07XHJcbiAgICAgIH0pKHRoaXMpKTtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLnJlbW92ZUZpbGUgPSBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgIGlmIChmaWxlLnN0YXR1cyA9PT0gRHJvcHpvbmUuVVBMT0FESU5HKSB7XHJcbiAgICAgICAgdGhpcy5jYW5jZWxVcGxvYWQoZmlsZSk7XHJcbiAgICAgIH1cclxuICAgICAgdGhpcy5maWxlcyA9IHdpdGhvdXQodGhpcy5maWxlcywgZmlsZSk7XHJcbiAgICAgIHRoaXMuZW1pdChcInJlbW92ZWRmaWxlXCIsIGZpbGUpO1xyXG4gICAgICBpZiAodGhpcy5maWxlcy5sZW5ndGggPT09IDApIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lbWl0KFwicmVzZXRcIik7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLnJlbW92ZUFsbEZpbGVzID0gZnVuY3Rpb24oY2FuY2VsSWZOZWNlc3NhcnkpIHtcclxuICAgICAgdmFyIGZpbGUsIF9pLCBfbGVuLCBfcmVmO1xyXG4gICAgICBpZiAoY2FuY2VsSWZOZWNlc3NhcnkgPT0gbnVsbCkge1xyXG4gICAgICAgIGNhbmNlbElmTmVjZXNzYXJ5ID0gZmFsc2U7XHJcbiAgICAgIH1cclxuICAgICAgX3JlZiA9IHRoaXMuZmlsZXMuc2xpY2UoKTtcclxuICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBfcmVmLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgZmlsZSA9IF9yZWZbX2ldO1xyXG4gICAgICAgIGlmIChmaWxlLnN0YXR1cyAhPT0gRHJvcHpvbmUuVVBMT0FESU5HIHx8IGNhbmNlbElmTmVjZXNzYXJ5KSB7XHJcbiAgICAgICAgICB0aGlzLnJlbW92ZUZpbGUoZmlsZSk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuY3JlYXRlVGh1bWJuYWlsID0gZnVuY3Rpb24oZmlsZSwgY2FsbGJhY2spIHtcclxuICAgICAgdmFyIGZpbGVSZWFkZXI7XHJcbiAgICAgIGZpbGVSZWFkZXIgPSBuZXcgRmlsZVJlYWRlcjtcclxuICAgICAgZmlsZVJlYWRlci5vbmxvYWQgPSAoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICByZXR1cm4gZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICBpZiAoZmlsZS50eXBlID09PSBcImltYWdlL3N2Zyt4bWxcIikge1xyXG4gICAgICAgICAgICBfdGhpcy5lbWl0KFwidGh1bWJuYWlsXCIsIGZpbGUsIGZpbGVSZWFkZXIucmVzdWx0KTtcclxuICAgICAgICAgICAgaWYgKGNhbGxiYWNrICE9IG51bGwpIHtcclxuICAgICAgICAgICAgICBjYWxsYmFjaygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBfdGhpcy5jcmVhdGVUaHVtYm5haWxGcm9tVXJsKGZpbGUsIGZpbGVSZWFkZXIucmVzdWx0LCBjYWxsYmFjayk7XHJcbiAgICAgICAgfTtcclxuICAgICAgfSkodGhpcyk7XHJcbiAgICAgIHJldHVybiBmaWxlUmVhZGVyLnJlYWRBc0RhdGFVUkwoZmlsZSk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5jcmVhdGVUaHVtYm5haWxGcm9tVXJsID0gZnVuY3Rpb24oZmlsZSwgaW1hZ2VVcmwsIGNhbGxiYWNrLCBjcm9zc09yaWdpbikge1xyXG4gICAgICB2YXIgaW1nO1xyXG4gICAgICBpbWcgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW1nXCIpO1xyXG4gICAgICBpZiAoY3Jvc3NPcmlnaW4pIHtcclxuICAgICAgICBpbWcuY3Jvc3NPcmlnaW4gPSBjcm9zc09yaWdpbjtcclxuICAgICAgfVxyXG4gICAgICBpbWcub25sb2FkID0gKGZ1bmN0aW9uKF90aGlzKSB7XHJcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgdmFyIGNhbnZhcywgY3R4LCByZXNpemVJbmZvLCB0aHVtYm5haWwsIF9yZWYsIF9yZWYxLCBfcmVmMiwgX3JlZjM7XHJcbiAgICAgICAgICBmaWxlLndpZHRoID0gaW1nLndpZHRoO1xyXG4gICAgICAgICAgZmlsZS5oZWlnaHQgPSBpbWcuaGVpZ2h0O1xyXG4gICAgICAgICAgcmVzaXplSW5mbyA9IF90aGlzLm9wdGlvbnMucmVzaXplLmNhbGwoX3RoaXMsIGZpbGUpO1xyXG4gICAgICAgICAgaWYgKHJlc2l6ZUluZm8udHJnV2lkdGggPT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXNpemVJbmZvLnRyZ1dpZHRoID0gcmVzaXplSW5mby5vcHRXaWR0aDtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmIChyZXNpemVJbmZvLnRyZ0hlaWdodCA9PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJlc2l6ZUluZm8udHJnSGVpZ2h0ID0gcmVzaXplSW5mby5vcHRIZWlnaHQ7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiY2FudmFzXCIpO1xyXG4gICAgICAgICAgY3R4ID0gY2FudmFzLmdldENvbnRleHQoXCIyZFwiKTtcclxuICAgICAgICAgIGNhbnZhcy53aWR0aCA9IHJlc2l6ZUluZm8udHJnV2lkdGg7XHJcbiAgICAgICAgICBjYW52YXMuaGVpZ2h0ID0gcmVzaXplSW5mby50cmdIZWlnaHQ7XHJcbiAgICAgICAgICBkcmF3SW1hZ2VJT1NGaXgoY3R4LCBpbWcsIChfcmVmID0gcmVzaXplSW5mby5zcmNYKSAhPSBudWxsID8gX3JlZiA6IDAsIChfcmVmMSA9IHJlc2l6ZUluZm8uc3JjWSkgIT0gbnVsbCA/IF9yZWYxIDogMCwgcmVzaXplSW5mby5zcmNXaWR0aCwgcmVzaXplSW5mby5zcmNIZWlnaHQsIChfcmVmMiA9IHJlc2l6ZUluZm8udHJnWCkgIT0gbnVsbCA/IF9yZWYyIDogMCwgKF9yZWYzID0gcmVzaXplSW5mby50cmdZKSAhPSBudWxsID8gX3JlZjMgOiAwLCByZXNpemVJbmZvLnRyZ1dpZHRoLCByZXNpemVJbmZvLnRyZ0hlaWdodCk7XHJcbiAgICAgICAgICB0aHVtYm5haWwgPSBjYW52YXMudG9EYXRhVVJMKFwiaW1hZ2UvcG5nXCIpO1xyXG4gICAgICAgICAgX3RoaXMuZW1pdChcInRodW1ibmFpbFwiLCBmaWxlLCB0aHVtYm5haWwpO1xyXG4gICAgICAgICAgaWYgKGNhbGxiYWNrICE9IG51bGwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGNhbGxiYWNrKCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgICAgfSkodGhpcyk7XHJcbiAgICAgIGlmIChjYWxsYmFjayAhPSBudWxsKSB7XHJcbiAgICAgICAgaW1nLm9uZXJyb3IgPSBjYWxsYmFjaztcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gaW1nLnNyYyA9IGltYWdlVXJsO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUucHJvY2Vzc1F1ZXVlID0gZnVuY3Rpb24oKSB7XHJcbiAgICAgIHZhciBpLCBwYXJhbGxlbFVwbG9hZHMsIHByb2Nlc3NpbmdMZW5ndGgsIHF1ZXVlZEZpbGVzO1xyXG4gICAgICBwYXJhbGxlbFVwbG9hZHMgPSB0aGlzLm9wdGlvbnMucGFyYWxsZWxVcGxvYWRzO1xyXG4gICAgICBwcm9jZXNzaW5nTGVuZ3RoID0gdGhpcy5nZXRVcGxvYWRpbmdGaWxlcygpLmxlbmd0aDtcclxuICAgICAgaSA9IHByb2Nlc3NpbmdMZW5ndGg7XHJcbiAgICAgIGlmIChwcm9jZXNzaW5nTGVuZ3RoID49IHBhcmFsbGVsVXBsb2Fkcykge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICBxdWV1ZWRGaWxlcyA9IHRoaXMuZ2V0UXVldWVkRmlsZXMoKTtcclxuICAgICAgaWYgKCEocXVldWVkRmlsZXMubGVuZ3RoID4gMCkpIHtcclxuICAgICAgICByZXR1cm47XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMub3B0aW9ucy51cGxvYWRNdWx0aXBsZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb2Nlc3NGaWxlcyhxdWV1ZWRGaWxlcy5zbGljZSgwLCBwYXJhbGxlbFVwbG9hZHMgLSBwcm9jZXNzaW5nTGVuZ3RoKSk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgd2hpbGUgKGkgPCBwYXJhbGxlbFVwbG9hZHMpIHtcclxuICAgICAgICAgIGlmICghcXVldWVkRmlsZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHRoaXMucHJvY2Vzc0ZpbGUocXVldWVkRmlsZXMuc2hpZnQoKSk7XHJcbiAgICAgICAgICBpKys7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5wcm9jZXNzRmlsZSA9IGZ1bmN0aW9uKGZpbGUpIHtcclxuICAgICAgcmV0dXJuIHRoaXMucHJvY2Vzc0ZpbGVzKFtmaWxlXSk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5wcm9jZXNzRmlsZXMgPSBmdW5jdGlvbihmaWxlcykge1xyXG4gICAgICB2YXIgZmlsZSwgX2ksIF9sZW47XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gZmlsZXMubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICBmaWxlID0gZmlsZXNbX2ldO1xyXG4gICAgICAgIGZpbGUucHJvY2Vzc2luZyA9IHRydWU7XHJcbiAgICAgICAgZmlsZS5zdGF0dXMgPSBEcm9wem9uZS5VUExPQURJTkc7XHJcbiAgICAgICAgdGhpcy5lbWl0KFwicHJvY2Vzc2luZ1wiLCBmaWxlKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlKSB7XHJcbiAgICAgICAgdGhpcy5lbWl0KFwicHJvY2Vzc2luZ211bHRpcGxlXCIsIGZpbGVzKTtcclxuICAgICAgfVxyXG4gICAgICByZXR1cm4gdGhpcy51cGxvYWRGaWxlcyhmaWxlcyk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5fZ2V0RmlsZXNXaXRoWGhyID0gZnVuY3Rpb24oeGhyKSB7XHJcbiAgICAgIHZhciBmaWxlLCBmaWxlcztcclxuICAgICAgcmV0dXJuIGZpbGVzID0gKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHZhciBfaSwgX2xlbiwgX3JlZiwgX3Jlc3VsdHM7XHJcbiAgICAgICAgX3JlZiA9IHRoaXMuZmlsZXM7XHJcbiAgICAgICAgX3Jlc3VsdHMgPSBbXTtcclxuICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICAgIGZpbGUgPSBfcmVmW19pXTtcclxuICAgICAgICAgIGlmIChmaWxlLnhociA9PT0geGhyKSB7XHJcbiAgICAgICAgICAgIF9yZXN1bHRzLnB1c2goZmlsZSk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBfcmVzdWx0cztcclxuICAgICAgfSkuY2FsbCh0aGlzKTtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLmNhbmNlbFVwbG9hZCA9IGZ1bmN0aW9uKGZpbGUpIHtcclxuICAgICAgdmFyIGdyb3VwZWRGaWxlLCBncm91cGVkRmlsZXMsIF9pLCBfaiwgX2xlbiwgX2xlbjEsIF9yZWY7XHJcbiAgICAgIGlmIChmaWxlLnN0YXR1cyA9PT0gRHJvcHpvbmUuVVBMT0FESU5HKSB7XHJcbiAgICAgICAgZ3JvdXBlZEZpbGVzID0gdGhpcy5fZ2V0RmlsZXNXaXRoWGhyKGZpbGUueGhyKTtcclxuICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGdyb3VwZWRGaWxlcy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgICAgZ3JvdXBlZEZpbGUgPSBncm91cGVkRmlsZXNbX2ldO1xyXG4gICAgICAgICAgZ3JvdXBlZEZpbGUuc3RhdHVzID0gRHJvcHpvbmUuQ0FOQ0VMRUQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZpbGUueGhyLmFib3J0KCk7XHJcbiAgICAgICAgZm9yIChfaiA9IDAsIF9sZW4xID0gZ3JvdXBlZEZpbGVzLmxlbmd0aDsgX2ogPCBfbGVuMTsgX2orKykge1xyXG4gICAgICAgICAgZ3JvdXBlZEZpbGUgPSBncm91cGVkRmlsZXNbX2pdO1xyXG4gICAgICAgICAgdGhpcy5lbWl0KFwiY2FuY2VsZWRcIiwgZ3JvdXBlZEZpbGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlKSB7XHJcbiAgICAgICAgICB0aGlzLmVtaXQoXCJjYW5jZWxlZG11bHRpcGxlXCIsIGdyb3VwZWRGaWxlcyk7XHJcbiAgICAgICAgfVxyXG4gICAgICB9IGVsc2UgaWYgKChfcmVmID0gZmlsZS5zdGF0dXMpID09PSBEcm9wem9uZS5BRERFRCB8fCBfcmVmID09PSBEcm9wem9uZS5RVUVVRUQpIHtcclxuICAgICAgICBmaWxlLnN0YXR1cyA9IERyb3B6b25lLkNBTkNFTEVEO1xyXG4gICAgICAgIHRoaXMuZW1pdChcImNhbmNlbGVkXCIsIGZpbGUpO1xyXG4gICAgICAgIGlmICh0aGlzLm9wdGlvbnMudXBsb2FkTXVsdGlwbGUpIHtcclxuICAgICAgICAgIHRoaXMuZW1pdChcImNhbmNlbGVkbXVsdGlwbGVcIiwgW2ZpbGVdKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5hdXRvUHJvY2Vzc1F1ZXVlKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvY2Vzc1F1ZXVlKCk7XHJcbiAgICAgIH1cclxuICAgIH07XHJcblxyXG4gICAgcmVzb2x2ZU9wdGlvbiA9IGZ1bmN0aW9uKCkge1xyXG4gICAgICB2YXIgYXJncywgb3B0aW9uO1xyXG4gICAgICBvcHRpb24gPSBhcmd1bWVudHNbMF0sIGFyZ3MgPSAyIDw9IGFyZ3VtZW50cy5sZW5ndGggPyBfX3NsaWNlLmNhbGwoYXJndW1lbnRzLCAxKSA6IFtdO1xyXG4gICAgICBpZiAodHlwZW9mIG9wdGlvbiA9PT0gJ2Z1bmN0aW9uJykge1xyXG4gICAgICAgIHJldHVybiBvcHRpb24uYXBwbHkodGhpcywgYXJncyk7XHJcbiAgICAgIH1cclxuICAgICAgcmV0dXJuIG9wdGlvbjtcclxuICAgIH07XHJcblxyXG4gICAgRHJvcHpvbmUucHJvdG90eXBlLnVwbG9hZEZpbGUgPSBmdW5jdGlvbihmaWxlKSB7XHJcbiAgICAgIHJldHVybiB0aGlzLnVwbG9hZEZpbGVzKFtmaWxlXSk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS51cGxvYWRGaWxlcyA9IGZ1bmN0aW9uKGZpbGVzKSB7XHJcbiAgICAgIHZhciBmaWxlLCBmb3JtRGF0YSwgaGFuZGxlRXJyb3IsIGhlYWRlck5hbWUsIGhlYWRlclZhbHVlLCBoZWFkZXJzLCBpLCBpbnB1dCwgaW5wdXROYW1lLCBpbnB1dFR5cGUsIGtleSwgbWV0aG9kLCBvcHRpb24sIHByb2dyZXNzT2JqLCByZXNwb25zZSwgdXBkYXRlUHJvZ3Jlc3MsIHVybCwgdmFsdWUsIHhociwgX2ksIF9qLCBfaywgX2wsIF9sZW4sIF9sZW4xLCBfbGVuMiwgX2xlbjMsIF9tLCBfcmVmLCBfcmVmMSwgX3JlZjIsIF9yZWYzLCBfcmVmNCwgX3JlZjU7XHJcbiAgICAgIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xyXG4gICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGZpbGVzLmxlbmd0aDsgX2kgPCBfbGVuOyBfaSsrKSB7XHJcbiAgICAgICAgZmlsZSA9IGZpbGVzW19pXTtcclxuICAgICAgICBmaWxlLnhociA9IHhocjtcclxuICAgICAgfVxyXG4gICAgICBtZXRob2QgPSByZXNvbHZlT3B0aW9uKHRoaXMub3B0aW9ucy5tZXRob2QsIGZpbGVzKTtcclxuICAgICAgdXJsID0gcmVzb2x2ZU9wdGlvbih0aGlzLm9wdGlvbnMudXJsLCBmaWxlcyk7XHJcbiAgICAgIHhoci5vcGVuKG1ldGhvZCwgdXJsLCB0cnVlKTtcclxuICAgICAgeGhyLndpdGhDcmVkZW50aWFscyA9ICEhdGhpcy5vcHRpb25zLndpdGhDcmVkZW50aWFscztcclxuICAgICAgcmVzcG9uc2UgPSBudWxsO1xyXG4gICAgICBoYW5kbGVFcnJvciA9IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIHZhciBfaiwgX2xlbjEsIF9yZXN1bHRzO1xyXG4gICAgICAgICAgX3Jlc3VsdHMgPSBbXTtcclxuICAgICAgICAgIGZvciAoX2ogPSAwLCBfbGVuMSA9IGZpbGVzLmxlbmd0aDsgX2ogPCBfbGVuMTsgX2orKykge1xyXG4gICAgICAgICAgICBmaWxlID0gZmlsZXNbX2pdO1xyXG4gICAgICAgICAgICBfcmVzdWx0cy5wdXNoKF90aGlzLl9lcnJvclByb2Nlc3NpbmcoZmlsZXMsIHJlc3BvbnNlIHx8IF90aGlzLm9wdGlvbnMuZGljdFJlc3BvbnNlRXJyb3IucmVwbGFjZShcInt7c3RhdHVzQ29kZX19XCIsIHhoci5zdGF0dXMpLCB4aHIpKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBfcmVzdWx0cztcclxuICAgICAgICB9O1xyXG4gICAgICB9KSh0aGlzKTtcclxuICAgICAgdXBkYXRlUHJvZ3Jlc3MgPSAoZnVuY3Rpb24oX3RoaXMpIHtcclxuICAgICAgICByZXR1cm4gZnVuY3Rpb24oZSkge1xyXG4gICAgICAgICAgdmFyIGFsbEZpbGVzRmluaXNoZWQsIHByb2dyZXNzLCBfaiwgX2ssIF9sLCBfbGVuMSwgX2xlbjIsIF9sZW4zLCBfcmVzdWx0cztcclxuICAgICAgICAgIGlmIChlICE9IG51bGwpIHtcclxuICAgICAgICAgICAgcHJvZ3Jlc3MgPSAxMDAgKiBlLmxvYWRlZCAvIGUudG90YWw7XHJcbiAgICAgICAgICAgIGZvciAoX2ogPSAwLCBfbGVuMSA9IGZpbGVzLmxlbmd0aDsgX2ogPCBfbGVuMTsgX2orKykge1xyXG4gICAgICAgICAgICAgIGZpbGUgPSBmaWxlc1tfal07XHJcbiAgICAgICAgICAgICAgZmlsZS51cGxvYWQgPSB7XHJcbiAgICAgICAgICAgICAgICBwcm9ncmVzczogcHJvZ3Jlc3MsXHJcbiAgICAgICAgICAgICAgICB0b3RhbDogZS50b3RhbCxcclxuICAgICAgICAgICAgICAgIGJ5dGVzU2VudDogZS5sb2FkZWRcclxuICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBhbGxGaWxlc0ZpbmlzaGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgcHJvZ3Jlc3MgPSAxMDA7XHJcbiAgICAgICAgICAgIGZvciAoX2sgPSAwLCBfbGVuMiA9IGZpbGVzLmxlbmd0aDsgX2sgPCBfbGVuMjsgX2srKykge1xyXG4gICAgICAgICAgICAgIGZpbGUgPSBmaWxlc1tfa107XHJcbiAgICAgICAgICAgICAgaWYgKCEoZmlsZS51cGxvYWQucHJvZ3Jlc3MgPT09IDEwMCAmJiBmaWxlLnVwbG9hZC5ieXRlc1NlbnQgPT09IGZpbGUudXBsb2FkLnRvdGFsKSkge1xyXG4gICAgICAgICAgICAgICAgYWxsRmlsZXNGaW5pc2hlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICBmaWxlLnVwbG9hZC5wcm9ncmVzcyA9IHByb2dyZXNzO1xyXG4gICAgICAgICAgICAgIGZpbGUudXBsb2FkLmJ5dGVzU2VudCA9IGZpbGUudXBsb2FkLnRvdGFsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChhbGxGaWxlc0ZpbmlzaGVkKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICBfcmVzdWx0cyA9IFtdO1xyXG4gICAgICAgICAgZm9yIChfbCA9IDAsIF9sZW4zID0gZmlsZXMubGVuZ3RoOyBfbCA8IF9sZW4zOyBfbCsrKSB7XHJcbiAgICAgICAgICAgIGZpbGUgPSBmaWxlc1tfbF07XHJcbiAgICAgICAgICAgIF9yZXN1bHRzLnB1c2goX3RoaXMuZW1pdChcInVwbG9hZHByb2dyZXNzXCIsIGZpbGUsIHByb2dyZXNzLCBmaWxlLnVwbG9hZC5ieXRlc1NlbnQpKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBfcmVzdWx0cztcclxuICAgICAgICB9O1xyXG4gICAgICB9KSh0aGlzKTtcclxuICAgICAgeGhyLm9ubG9hZCA9IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbihlKSB7XHJcbiAgICAgICAgICB2YXIgX3JlZjtcclxuICAgICAgICAgIGlmIChmaWxlc1swXS5zdGF0dXMgPT09IERyb3B6b25lLkNBTkNFTEVEKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIGlmICh4aHIucmVhZHlTdGF0ZSAhPT0gNCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgICByZXNwb25zZSA9IHhoci5yZXNwb25zZVRleHQ7XHJcbiAgICAgICAgICBpZiAoeGhyLmdldFJlc3BvbnNlSGVhZGVyKFwiY29udGVudC10eXBlXCIpICYmIH54aHIuZ2V0UmVzcG9uc2VIZWFkZXIoXCJjb250ZW50LXR5cGVcIikuaW5kZXhPZihcImFwcGxpY2F0aW9uL2pzb25cIikpIHtcclxuICAgICAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgICByZXNwb25zZSA9IEpTT04ucGFyc2UocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChfZXJyb3IpIHtcclxuICAgICAgICAgICAgICBlID0gX2Vycm9yO1xyXG4gICAgICAgICAgICAgIHJlc3BvbnNlID0gXCJJbnZhbGlkIEpTT04gcmVzcG9uc2UgZnJvbSBzZXJ2ZXIuXCI7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHVwZGF0ZVByb2dyZXNzKCk7XHJcbiAgICAgICAgICBpZiAoISgoMjAwIDw9IChfcmVmID0geGhyLnN0YXR1cykgJiYgX3JlZiA8IDMwMCkpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBoYW5kbGVFcnJvcigpO1xyXG4gICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLl9maW5pc2hlZChmaWxlcywgcmVzcG9uc2UsIGUpO1xyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICAgIH0pKHRoaXMpO1xyXG4gICAgICB4aHIub25lcnJvciA9IChmdW5jdGlvbihfdGhpcykge1xyXG4gICAgICAgIHJldHVybiBmdW5jdGlvbigpIHtcclxuICAgICAgICAgIGlmIChmaWxlc1swXS5zdGF0dXMgPT09IERyb3B6b25lLkNBTkNFTEVEKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgIH1cclxuICAgICAgICAgIHJldHVybiBoYW5kbGVFcnJvcigpO1xyXG4gICAgICAgIH07XHJcbiAgICAgIH0pKHRoaXMpO1xyXG4gICAgICBwcm9ncmVzc09iaiA9IChfcmVmID0geGhyLnVwbG9hZCkgIT0gbnVsbCA/IF9yZWYgOiB4aHI7XHJcbiAgICAgIHByb2dyZXNzT2JqLm9ucHJvZ3Jlc3MgPSB1cGRhdGVQcm9ncmVzcztcclxuICAgICAgaGVhZGVycyA9IHtcclxuICAgICAgICBcIkFjY2VwdFwiOiBcImFwcGxpY2F0aW9uL2pzb25cIixcclxuICAgICAgICBcIkNhY2hlLUNvbnRyb2xcIjogXCJuby1jYWNoZVwiLFxyXG4gICAgICAgIFwiWC1SZXF1ZXN0ZWQtV2l0aFwiOiBcIlhNTEh0dHBSZXF1ZXN0XCJcclxuICAgICAgfTtcclxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5oZWFkZXJzKSB7XHJcbiAgICAgICAgZXh0ZW5kKGhlYWRlcnMsIHRoaXMub3B0aW9ucy5oZWFkZXJzKTtcclxuICAgICAgfVxyXG4gICAgICBmb3IgKGhlYWRlck5hbWUgaW4gaGVhZGVycykge1xyXG4gICAgICAgIGhlYWRlclZhbHVlID0gaGVhZGVyc1toZWFkZXJOYW1lXTtcclxuICAgICAgICBpZiAoaGVhZGVyVmFsdWUpIHtcclxuICAgICAgICAgIHhoci5zZXRSZXF1ZXN0SGVhZGVyKGhlYWRlck5hbWUsIGhlYWRlclZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgZm9ybURhdGEgPSBuZXcgRm9ybURhdGEoKTtcclxuICAgICAgaWYgKHRoaXMub3B0aW9ucy5wYXJhbXMpIHtcclxuICAgICAgICBfcmVmMSA9IHRoaXMub3B0aW9ucy5wYXJhbXM7XHJcbiAgICAgICAgZm9yIChrZXkgaW4gX3JlZjEpIHtcclxuICAgICAgICAgIHZhbHVlID0gX3JlZjFba2V5XTtcclxuICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZChrZXksIHZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgZm9yIChfaiA9IDAsIF9sZW4xID0gZmlsZXMubGVuZ3RoOyBfaiA8IF9sZW4xOyBfaisrKSB7XHJcbiAgICAgICAgZmlsZSA9IGZpbGVzW19qXTtcclxuICAgICAgICB0aGlzLmVtaXQoXCJzZW5kaW5nXCIsIGZpbGUsIHhociwgZm9ybURhdGEpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMudXBsb2FkTXVsdGlwbGUpIHtcclxuICAgICAgICB0aGlzLmVtaXQoXCJzZW5kaW5nbXVsdGlwbGVcIiwgZmlsZXMsIHhociwgZm9ybURhdGEpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLmVsZW1lbnQudGFnTmFtZSA9PT0gXCJGT1JNXCIpIHtcclxuICAgICAgICBfcmVmMiA9IHRoaXMuZWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiaW5wdXQsIHRleHRhcmVhLCBzZWxlY3QsIGJ1dHRvblwiKTtcclxuICAgICAgICBmb3IgKF9rID0gMCwgX2xlbjIgPSBfcmVmMi5sZW5ndGg7IF9rIDwgX2xlbjI7IF9rKyspIHtcclxuICAgICAgICAgIGlucHV0ID0gX3JlZjJbX2tdO1xyXG4gICAgICAgICAgaW5wdXROYW1lID0gaW5wdXQuZ2V0QXR0cmlidXRlKFwibmFtZVwiKTtcclxuICAgICAgICAgIGlucHV0VHlwZSA9IGlucHV0LmdldEF0dHJpYnV0ZShcInR5cGVcIik7XHJcbiAgICAgICAgICBpZiAoaW5wdXQudGFnTmFtZSA9PT0gXCJTRUxFQ1RcIiAmJiBpbnB1dC5oYXNBdHRyaWJ1dGUoXCJtdWx0aXBsZVwiKSkge1xyXG4gICAgICAgICAgICBfcmVmMyA9IGlucHV0Lm9wdGlvbnM7XHJcbiAgICAgICAgICAgIGZvciAoX2wgPSAwLCBfbGVuMyA9IF9yZWYzLmxlbmd0aDsgX2wgPCBfbGVuMzsgX2wrKykge1xyXG4gICAgICAgICAgICAgIG9wdGlvbiA9IF9yZWYzW19sXTtcclxuICAgICAgICAgICAgICBpZiAob3B0aW9uLnNlbGVjdGVkKSB7XHJcbiAgICAgICAgICAgICAgICBmb3JtRGF0YS5hcHBlbmQoaW5wdXROYW1lLCBvcHRpb24udmFsdWUpO1xyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgfSBlbHNlIGlmICghaW5wdXRUeXBlIHx8ICgoX3JlZjQgPSBpbnB1dFR5cGUudG9Mb3dlckNhc2UoKSkgIT09IFwiY2hlY2tib3hcIiAmJiBfcmVmNCAhPT0gXCJyYWRpb1wiKSB8fCBpbnB1dC5jaGVja2VkKSB7XHJcbiAgICAgICAgICAgIGZvcm1EYXRhLmFwcGVuZChpbnB1dE5hbWUsIGlucHV0LnZhbHVlKTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgZm9yIChpID0gX20gPSAwLCBfcmVmNSA9IGZpbGVzLmxlbmd0aCAtIDE7IDAgPD0gX3JlZjUgPyBfbSA8PSBfcmVmNSA6IF9tID49IF9yZWY1OyBpID0gMCA8PSBfcmVmNSA/ICsrX20gOiAtLV9tKSB7XHJcbiAgICAgICAgZm9ybURhdGEuYXBwZW5kKHRoaXMuX2dldFBhcmFtTmFtZShpKSwgZmlsZXNbaV0sIGZpbGVzW2ldLm5hbWUpO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiB0aGlzLnN1Ym1pdFJlcXVlc3QoeGhyLCBmb3JtRGF0YSwgZmlsZXMpO1xyXG4gICAgfTtcclxuXHJcbiAgICBEcm9wem9uZS5wcm90b3R5cGUuc3VibWl0UmVxdWVzdCA9IGZ1bmN0aW9uKHhociwgZm9ybURhdGEsIGZpbGVzKSB7XHJcbiAgICAgIHJldHVybiB4aHIuc2VuZChmb3JtRGF0YSk7XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5fZmluaXNoZWQgPSBmdW5jdGlvbihmaWxlcywgcmVzcG9uc2VUZXh0LCBlKSB7XHJcbiAgICAgIHZhciBmaWxlLCBfaSwgX2xlbjtcclxuICAgICAgZm9yIChfaSA9IDAsIF9sZW4gPSBmaWxlcy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgIGZpbGUgPSBmaWxlc1tfaV07XHJcbiAgICAgICAgZmlsZS5zdGF0dXMgPSBEcm9wem9uZS5TVUNDRVNTO1xyXG4gICAgICAgIHRoaXMuZW1pdChcInN1Y2Nlc3NcIiwgZmlsZSwgcmVzcG9uc2VUZXh0LCBlKTtcclxuICAgICAgICB0aGlzLmVtaXQoXCJjb21wbGV0ZVwiLCBmaWxlKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlKSB7XHJcbiAgICAgICAgdGhpcy5lbWl0KFwic3VjY2Vzc211bHRpcGxlXCIsIGZpbGVzLCByZXNwb25zZVRleHQsIGUpO1xyXG4gICAgICAgIHRoaXMuZW1pdChcImNvbXBsZXRlbXVsdGlwbGVcIiwgZmlsZXMpO1xyXG4gICAgICB9XHJcbiAgICAgIGlmICh0aGlzLm9wdGlvbnMuYXV0b1Byb2Nlc3NRdWV1ZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb2Nlc3NRdWV1ZSgpO1xyXG4gICAgICB9XHJcbiAgICB9O1xyXG5cclxuICAgIERyb3B6b25lLnByb3RvdHlwZS5fZXJyb3JQcm9jZXNzaW5nID0gZnVuY3Rpb24oZmlsZXMsIG1lc3NhZ2UsIHhocikge1xyXG4gICAgICB2YXIgZmlsZSwgX2ksIF9sZW47XHJcbiAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gZmlsZXMubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICBmaWxlID0gZmlsZXNbX2ldO1xyXG4gICAgICAgIGZpbGUuc3RhdHVzID0gRHJvcHpvbmUuRVJST1I7XHJcbiAgICAgICAgdGhpcy5lbWl0KFwiZXJyb3JcIiwgZmlsZSwgbWVzc2FnZSwgeGhyKTtcclxuICAgICAgICB0aGlzLmVtaXQoXCJjb21wbGV0ZVwiLCBmaWxlKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLnVwbG9hZE11bHRpcGxlKSB7XHJcbiAgICAgICAgdGhpcy5lbWl0KFwiZXJyb3JtdWx0aXBsZVwiLCBmaWxlcywgbWVzc2FnZSwgeGhyKTtcclxuICAgICAgICB0aGlzLmVtaXQoXCJjb21wbGV0ZW11bHRpcGxlXCIsIGZpbGVzKTtcclxuICAgICAgfVxyXG4gICAgICBpZiAodGhpcy5vcHRpb25zLmF1dG9Qcm9jZXNzUXVldWUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9jZXNzUXVldWUoKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuXHJcbiAgICByZXR1cm4gRHJvcHpvbmU7XHJcblxyXG4gIH0pKEVtaXR0ZXIpO1xyXG5cclxuICBEcm9wem9uZS52ZXJzaW9uID0gXCI0LjIuMFwiO1xyXG5cclxuICBEcm9wem9uZS5vcHRpb25zID0ge307XHJcblxyXG4gIERyb3B6b25lLm9wdGlvbnNGb3JFbGVtZW50ID0gZnVuY3Rpb24oZWxlbWVudCkge1xyXG4gICAgaWYgKGVsZW1lbnQuZ2V0QXR0cmlidXRlKFwiaWRcIikpIHtcclxuICAgICAgcmV0dXJuIERyb3B6b25lLm9wdGlvbnNbY2FtZWxpemUoZWxlbWVudC5nZXRBdHRyaWJ1dGUoXCJpZFwiKSldO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHZvaWQgMDtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBEcm9wem9uZS5pbnN0YW5jZXMgPSBbXTtcclxuXHJcbiAgRHJvcHpvbmUuZm9yRWxlbWVudCA9IGZ1bmN0aW9uKGVsZW1lbnQpIHtcclxuICAgIGlmICh0eXBlb2YgZWxlbWVudCA9PT0gXCJzdHJpbmdcIikge1xyXG4gICAgICBlbGVtZW50ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbGVtZW50KTtcclxuICAgIH1cclxuICAgIGlmICgoZWxlbWVudCAhPSBudWxsID8gZWxlbWVudC5kcm9wem9uZSA6IHZvaWQgMCkgPT0gbnVsbCkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJObyBEcm9wem9uZSBmb3VuZCBmb3IgZ2l2ZW4gZWxlbWVudC4gVGhpcyBpcyBwcm9iYWJseSBiZWNhdXNlIHlvdSdyZSB0cnlpbmcgdG8gYWNjZXNzIGl0IGJlZm9yZSBEcm9wem9uZSBoYWQgdGhlIHRpbWUgdG8gaW5pdGlhbGl6ZS4gVXNlIHRoZSBgaW5pdGAgb3B0aW9uIHRvIHNldHVwIGFueSBhZGRpdGlvbmFsIG9ic2VydmVycyBvbiB5b3VyIERyb3B6b25lLlwiKTtcclxuICAgIH1cclxuICAgIHJldHVybiBlbGVtZW50LmRyb3B6b25lO1xyXG4gIH07XHJcblxyXG4gIERyb3B6b25lLmF1dG9EaXNjb3ZlciA9IHRydWU7XHJcblxyXG4gIERyb3B6b25lLmRpc2NvdmVyID0gZnVuY3Rpb24oKSB7XHJcbiAgICB2YXIgY2hlY2tFbGVtZW50cywgZHJvcHpvbmUsIGRyb3B6b25lcywgX2ksIF9sZW4sIF9yZXN1bHRzO1xyXG4gICAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwpIHtcclxuICAgICAgZHJvcHpvbmVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5kcm9wem9uZVwiKTtcclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGRyb3B6b25lcyA9IFtdO1xyXG4gICAgICBjaGVja0VsZW1lbnRzID0gZnVuY3Rpb24oZWxlbWVudHMpIHtcclxuICAgICAgICB2YXIgZWwsIF9pLCBfbGVuLCBfcmVzdWx0cztcclxuICAgICAgICBfcmVzdWx0cyA9IFtdO1xyXG4gICAgICAgIGZvciAoX2kgPSAwLCBfbGVuID0gZWxlbWVudHMubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICAgIGVsID0gZWxlbWVudHNbX2ldO1xyXG4gICAgICAgICAgaWYgKC8oXnwgKWRyb3B6b25lKCR8ICkvLnRlc3QoZWwuY2xhc3NOYW1lKSkge1xyXG4gICAgICAgICAgICBfcmVzdWx0cy5wdXNoKGRyb3B6b25lcy5wdXNoKGVsKSk7XHJcbiAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBfcmVzdWx0cy5wdXNoKHZvaWQgMCk7XHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBfcmVzdWx0cztcclxuICAgICAgfTtcclxuICAgICAgY2hlY2tFbGVtZW50cyhkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZShcImRpdlwiKSk7XHJcbiAgICAgIGNoZWNrRWxlbWVudHMoZG9jdW1lbnQuZ2V0RWxlbWVudHNCeVRhZ05hbWUoXCJmb3JtXCIpKTtcclxuICAgIH1cclxuICAgIF9yZXN1bHRzID0gW107XHJcbiAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGRyb3B6b25lcy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICBkcm9wem9uZSA9IGRyb3B6b25lc1tfaV07XHJcbiAgICAgIGlmIChEcm9wem9uZS5vcHRpb25zRm9yRWxlbWVudChkcm9wem9uZSkgIT09IGZhbHNlKSB7XHJcbiAgICAgICAgX3Jlc3VsdHMucHVzaChuZXcgRHJvcHpvbmUoZHJvcHpvbmUpKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBfcmVzdWx0cy5wdXNoKHZvaWQgMCk7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICAgIHJldHVybiBfcmVzdWx0cztcclxuICB9O1xyXG5cclxuICBEcm9wem9uZS5ibGFja2xpc3RlZEJyb3dzZXJzID0gWy9vcGVyYS4qTWFjaW50b3NoLip2ZXJzaW9uXFwvMTIvaV07XHJcblxyXG4gIERyb3B6b25lLmlzQnJvd3NlclN1cHBvcnRlZCA9IGZ1bmN0aW9uKCkge1xyXG4gICAgdmFyIGNhcGFibGVCcm93c2VyLCByZWdleCwgX2ksIF9sZW4sIF9yZWY7XHJcbiAgICBjYXBhYmxlQnJvd3NlciA9IHRydWU7XHJcbiAgICBpZiAod2luZG93LkZpbGUgJiYgd2luZG93LkZpbGVSZWFkZXIgJiYgd2luZG93LkZpbGVMaXN0ICYmIHdpbmRvdy5CbG9iICYmIHdpbmRvdy5Gb3JtRGF0YSAmJiBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKSB7XHJcbiAgICAgIGlmICghKFwiY2xhc3NMaXN0XCIgaW4gZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImFcIikpKSB7XHJcbiAgICAgICAgY2FwYWJsZUJyb3dzZXIgPSBmYWxzZTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBfcmVmID0gRHJvcHpvbmUuYmxhY2tsaXN0ZWRCcm93c2VycztcclxuICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IF9yZWYubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgICAgIHJlZ2V4ID0gX3JlZltfaV07XHJcbiAgICAgICAgICBpZiAocmVnZXgudGVzdChuYXZpZ2F0b3IudXNlckFnZW50KSkge1xyXG4gICAgICAgICAgICBjYXBhYmxlQnJvd3NlciA9IGZhbHNlO1xyXG4gICAgICAgICAgICBjb250aW51ZTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH0gZWxzZSB7XHJcbiAgICAgIGNhcGFibGVCcm93c2VyID0gZmFsc2U7XHJcbiAgICB9XHJcbiAgICByZXR1cm4gY2FwYWJsZUJyb3dzZXI7XHJcbiAgfTtcclxuXHJcbiAgd2l0aG91dCA9IGZ1bmN0aW9uKGxpc3QsIHJlamVjdGVkSXRlbSkge1xyXG4gICAgdmFyIGl0ZW0sIF9pLCBfbGVuLCBfcmVzdWx0cztcclxuICAgIF9yZXN1bHRzID0gW107XHJcbiAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGxpc3QubGVuZ3RoOyBfaSA8IF9sZW47IF9pKyspIHtcclxuICAgICAgaXRlbSA9IGxpc3RbX2ldO1xyXG4gICAgICBpZiAoaXRlbSAhPT0gcmVqZWN0ZWRJdGVtKSB7XHJcbiAgICAgICAgX3Jlc3VsdHMucHVzaChpdGVtKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIF9yZXN1bHRzO1xyXG4gIH07XHJcblxyXG4gIGNhbWVsaXplID0gZnVuY3Rpb24oc3RyKSB7XHJcbiAgICByZXR1cm4gc3RyLnJlcGxhY2UoL1tcXC1fXShcXHcpL2csIGZ1bmN0aW9uKG1hdGNoKSB7XHJcbiAgICAgIHJldHVybiBtYXRjaC5jaGFyQXQoMSkudG9VcHBlckNhc2UoKTtcclxuICAgIH0pO1xyXG4gIH07XHJcblxyXG4gIERyb3B6b25lLmNyZWF0ZUVsZW1lbnQgPSBmdW5jdGlvbihzdHJpbmcpIHtcclxuICAgIHZhciBkaXY7XHJcbiAgICBkaXYgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiZGl2XCIpO1xyXG4gICAgZGl2LmlubmVySFRNTCA9IHN0cmluZztcclxuICAgIHJldHVybiBkaXYuY2hpbGROb2Rlc1swXTtcclxuICB9O1xyXG5cclxuICBEcm9wem9uZS5lbGVtZW50SW5zaWRlID0gZnVuY3Rpb24oZWxlbWVudCwgY29udGFpbmVyKSB7XHJcbiAgICBpZiAoZWxlbWVudCA9PT0gY29udGFpbmVyKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgd2hpbGUgKGVsZW1lbnQgPSBlbGVtZW50LnBhcmVudE5vZGUpIHtcclxuICAgICAgaWYgKGVsZW1lbnQgPT09IGNvbnRhaW5lcikge1xyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICByZXR1cm4gZmFsc2U7XHJcbiAgfTtcclxuXHJcbiAgRHJvcHpvbmUuZ2V0RWxlbWVudCA9IGZ1bmN0aW9uKGVsLCBuYW1lKSB7XHJcbiAgICB2YXIgZWxlbWVudDtcclxuICAgIGlmICh0eXBlb2YgZWwgPT09IFwic3RyaW5nXCIpIHtcclxuICAgICAgZWxlbWVudCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpO1xyXG4gICAgfSBlbHNlIGlmIChlbC5ub2RlVHlwZSAhPSBudWxsKSB7XHJcbiAgICAgIGVsZW1lbnQgPSBlbDtcclxuICAgIH1cclxuICAgIGlmIChlbGVtZW50ID09IG51bGwpIHtcclxuICAgICAgdGhyb3cgbmV3IEVycm9yKFwiSW52YWxpZCBgXCIgKyBuYW1lICsgXCJgIG9wdGlvbiBwcm92aWRlZC4gUGxlYXNlIHByb3ZpZGUgYSBDU1Mgc2VsZWN0b3Igb3IgYSBwbGFpbiBIVE1MIGVsZW1lbnQuXCIpO1xyXG4gICAgfVxyXG4gICAgcmV0dXJuIGVsZW1lbnQ7XHJcbiAgfTtcclxuXHJcbiAgRHJvcHpvbmUuZ2V0RWxlbWVudHMgPSBmdW5jdGlvbihlbHMsIG5hbWUpIHtcclxuICAgIHZhciBlLCBlbCwgZWxlbWVudHMsIF9pLCBfaiwgX2xlbiwgX2xlbjEsIF9yZWY7XHJcbiAgICBpZiAoZWxzIGluc3RhbmNlb2YgQXJyYXkpIHtcclxuICAgICAgZWxlbWVudHMgPSBbXTtcclxuICAgICAgdHJ5IHtcclxuICAgICAgICBmb3IgKF9pID0gMCwgX2xlbiA9IGVscy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICAgICAgZWwgPSBlbHNbX2ldO1xyXG4gICAgICAgICAgZWxlbWVudHMucHVzaCh0aGlzLmdldEVsZW1lbnQoZWwsIG5hbWUpKTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gY2F0Y2ggKF9lcnJvcikge1xyXG4gICAgICAgIGUgPSBfZXJyb3I7XHJcbiAgICAgICAgZWxlbWVudHMgPSBudWxsO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKHR5cGVvZiBlbHMgPT09IFwic3RyaW5nXCIpIHtcclxuICAgICAgZWxlbWVudHMgPSBbXTtcclxuICAgICAgX3JlZiA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoZWxzKTtcclxuICAgICAgZm9yIChfaiA9IDAsIF9sZW4xID0gX3JlZi5sZW5ndGg7IF9qIDwgX2xlbjE7IF9qKyspIHtcclxuICAgICAgICBlbCA9IF9yZWZbX2pdO1xyXG4gICAgICAgIGVsZW1lbnRzLnB1c2goZWwpO1xyXG4gICAgICB9XHJcbiAgICB9IGVsc2UgaWYgKGVscy5ub2RlVHlwZSAhPSBudWxsKSB7XHJcbiAgICAgIGVsZW1lbnRzID0gW2Vsc107XHJcbiAgICB9XHJcbiAgICBpZiAoISgoZWxlbWVudHMgIT0gbnVsbCkgJiYgZWxlbWVudHMubGVuZ3RoKSkge1xyXG4gICAgICB0aHJvdyBuZXcgRXJyb3IoXCJJbnZhbGlkIGBcIiArIG5hbWUgKyBcImAgb3B0aW9uIHByb3ZpZGVkLiBQbGVhc2UgcHJvdmlkZSBhIENTUyBzZWxlY3RvciwgYSBwbGFpbiBIVE1MIGVsZW1lbnQgb3IgYSBsaXN0IG9mIHRob3NlLlwiKTtcclxuICAgIH1cclxuICAgIHJldHVybiBlbGVtZW50cztcclxuICB9O1xyXG5cclxuICBEcm9wem9uZS5jb25maXJtID0gZnVuY3Rpb24ocXVlc3Rpb24sIGFjY2VwdGVkLCByZWplY3RlZCkge1xyXG4gICAgaWYgKHdpbmRvdy5jb25maXJtKHF1ZXN0aW9uKSkge1xyXG4gICAgICByZXR1cm4gYWNjZXB0ZWQoKTtcclxuICAgIH0gZWxzZSBpZiAocmVqZWN0ZWQgIT0gbnVsbCkge1xyXG4gICAgICByZXR1cm4gcmVqZWN0ZWQoKTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBEcm9wem9uZS5pc1ZhbGlkRmlsZSA9IGZ1bmN0aW9uKGZpbGUsIGFjY2VwdGVkRmlsZXMpIHtcclxuICAgIHZhciBiYXNlTWltZVR5cGUsIG1pbWVUeXBlLCB2YWxpZFR5cGUsIF9pLCBfbGVuO1xyXG4gICAgaWYgKCFhY2NlcHRlZEZpbGVzKSB7XHJcbiAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG4gICAgYWNjZXB0ZWRGaWxlcyA9IGFjY2VwdGVkRmlsZXMuc3BsaXQoXCIsXCIpO1xyXG4gICAgbWltZVR5cGUgPSBmaWxlLnR5cGU7XHJcbiAgICBiYXNlTWltZVR5cGUgPSBtaW1lVHlwZS5yZXBsYWNlKC9cXC8uKiQvLCBcIlwiKTtcclxuICAgIGZvciAoX2kgPSAwLCBfbGVuID0gYWNjZXB0ZWRGaWxlcy5sZW5ndGg7IF9pIDwgX2xlbjsgX2krKykge1xyXG4gICAgICB2YWxpZFR5cGUgPSBhY2NlcHRlZEZpbGVzW19pXTtcclxuICAgICAgdmFsaWRUeXBlID0gdmFsaWRUeXBlLnRyaW0oKTtcclxuICAgICAgaWYgKHZhbGlkVHlwZS5jaGFyQXQoMCkgPT09IFwiLlwiKSB7XHJcbiAgICAgICAgaWYgKGZpbGUubmFtZS50b0xvd2VyQ2FzZSgpLmluZGV4T2YodmFsaWRUeXBlLnRvTG93ZXJDYXNlKCksIGZpbGUubmFtZS5sZW5ndGggLSB2YWxpZFR5cGUubGVuZ3RoKSAhPT0gLTEpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfSBlbHNlIGlmICgvXFwvXFwqJC8udGVzdCh2YWxpZFR5cGUpKSB7XHJcbiAgICAgICAgaWYgKGJhc2VNaW1lVHlwZSA9PT0gdmFsaWRUeXBlLnJlcGxhY2UoL1xcLy4qJC8sIFwiXCIpKSB7XHJcbiAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgaWYgKG1pbWVUeXBlID09PSB2YWxpZFR5cGUpIHtcclxuICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgcmV0dXJuIGZhbHNlO1xyXG4gIH07XHJcblxyXG4gIGlmICh0eXBlb2YgalF1ZXJ5ICE9PSBcInVuZGVmaW5lZFwiICYmIGpRdWVyeSAhPT0gbnVsbCkge1xyXG4gICAgalF1ZXJ5LmZuLmRyb3B6b25lID0gZnVuY3Rpb24ob3B0aW9ucykge1xyXG4gICAgICByZXR1cm4gdGhpcy5lYWNoKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiBuZXcgRHJvcHpvbmUodGhpcywgb3B0aW9ucyk7XHJcbiAgICAgIH0pO1xyXG4gICAgfTtcclxuICB9XHJcblxyXG4gIGlmICh0eXBlb2YgbW9kdWxlICE9PSBcInVuZGVmaW5lZFwiICYmIG1vZHVsZSAhPT0gbnVsbCkge1xyXG4gICAgbW9kdWxlLmV4cG9ydHMgPSBEcm9wem9uZTtcclxuICB9IGVsc2Uge1xyXG4gICAgd2luZG93LkRyb3B6b25lID0gRHJvcHpvbmU7XHJcbiAgfVxyXG5cclxuICBEcm9wem9uZS5BRERFRCA9IFwiYWRkZWRcIjtcclxuXHJcbiAgRHJvcHpvbmUuUVVFVUVEID0gXCJxdWV1ZWRcIjtcclxuXHJcbiAgRHJvcHpvbmUuQUNDRVBURUQgPSBEcm9wem9uZS5RVUVVRUQ7XHJcblxyXG4gIERyb3B6b25lLlVQTE9BRElORyA9IFwidXBsb2FkaW5nXCI7XHJcblxyXG4gIERyb3B6b25lLlBST0NFU1NJTkcgPSBEcm9wem9uZS5VUExPQURJTkc7XHJcblxyXG4gIERyb3B6b25lLkNBTkNFTEVEID0gXCJjYW5jZWxlZFwiO1xyXG5cclxuICBEcm9wem9uZS5FUlJPUiA9IFwiZXJyb3JcIjtcclxuXHJcbiAgRHJvcHpvbmUuU1VDQ0VTUyA9IFwic3VjY2Vzc1wiO1xyXG5cclxuXHJcbiAgLypcclxuICBcclxuICBCdWdmaXggZm9yIGlPUyA2IGFuZCA3XHJcbiAgU291cmNlOiBodHRwOi8vc3RhY2tvdmVyZmxvdy5jb20vcXVlc3Rpb25zLzExOTI5MDk5L2h0bWw1LWNhbnZhcy1kcmF3aW1hZ2UtcmF0aW8tYnVnLWlvc1xyXG4gIGJhc2VkIG9uIHRoZSB3b3JrIG9mIGh0dHBzOi8vZ2l0aHViLmNvbS9zdG9taXRhL2lvcy1pbWFnZWZpbGUtbWVnYXBpeGVsXHJcbiAgICovXHJcblxyXG4gIGRldGVjdFZlcnRpY2FsU3F1YXNoID0gZnVuY3Rpb24oaW1nKSB7XHJcbiAgICB2YXIgYWxwaGEsIGNhbnZhcywgY3R4LCBkYXRhLCBleSwgaWgsIGl3LCBweSwgcmF0aW8sIHN5O1xyXG4gICAgaXcgPSBpbWcubmF0dXJhbFdpZHRoO1xyXG4gICAgaWggPSBpbWcubmF0dXJhbEhlaWdodDtcclxuICAgIGNhbnZhcyA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJjYW52YXNcIik7XHJcbiAgICBjYW52YXMud2lkdGggPSAxO1xyXG4gICAgY2FudmFzLmhlaWdodCA9IGloO1xyXG4gICAgY3R4ID0gY2FudmFzLmdldENvbnRleHQoXCIyZFwiKTtcclxuICAgIGN0eC5kcmF3SW1hZ2UoaW1nLCAwLCAwKTtcclxuICAgIGRhdGEgPSBjdHguZ2V0SW1hZ2VEYXRhKDAsIDAsIDEsIGloKS5kYXRhO1xyXG4gICAgc3kgPSAwO1xyXG4gICAgZXkgPSBpaDtcclxuICAgIHB5ID0gaWg7XHJcbiAgICB3aGlsZSAocHkgPiBzeSkge1xyXG4gICAgICBhbHBoYSA9IGRhdGFbKHB5IC0gMSkgKiA0ICsgM107XHJcbiAgICAgIGlmIChhbHBoYSA9PT0gMCkge1xyXG4gICAgICAgIGV5ID0gcHk7XHJcbiAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgc3kgPSBweTtcclxuICAgICAgfVxyXG4gICAgICBweSA9IChleSArIHN5KSA+PiAxO1xyXG4gICAgfVxyXG4gICAgcmF0aW8gPSBweSAvIGloO1xyXG4gICAgaWYgKHJhdGlvID09PSAwKSB7XHJcbiAgICAgIHJldHVybiAxO1xyXG4gICAgfSBlbHNlIHtcclxuICAgICAgcmV0dXJuIHJhdGlvO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIGRyYXdJbWFnZUlPU0ZpeCA9IGZ1bmN0aW9uKGN0eCwgaW1nLCBzeCwgc3ksIHN3LCBzaCwgZHgsIGR5LCBkdywgZGgpIHtcclxuICAgIHZhciB2ZXJ0U3F1YXNoUmF0aW87XHJcbiAgICB2ZXJ0U3F1YXNoUmF0aW8gPSBkZXRlY3RWZXJ0aWNhbFNxdWFzaChpbWcpO1xyXG4gICAgcmV0dXJuIGN0eC5kcmF3SW1hZ2UoaW1nLCBzeCwgc3ksIHN3LCBzaCwgZHgsIGR5LCBkdywgZGggLyB2ZXJ0U3F1YXNoUmF0aW8pO1xyXG4gIH07XHJcblxyXG5cclxuICAvKlxyXG4gICAqIGNvbnRlbnRsb2FkZWQuanNcclxuICAgKlxyXG4gICAqIEF1dGhvcjogRGllZ28gUGVyaW5pIChkaWVnby5wZXJpbmkgYXQgZ21haWwuY29tKVxyXG4gICAqIFN1bW1hcnk6IGNyb3NzLWJyb3dzZXIgd3JhcHBlciBmb3IgRE9NQ29udGVudExvYWRlZFxyXG4gICAqIFVwZGF0ZWQ6IDIwMTAxMDIwXHJcbiAgICogTGljZW5zZTogTUlUXHJcbiAgICogVmVyc2lvbjogMS4yXHJcbiAgICpcclxuICAgKiBVUkw6XHJcbiAgICogaHR0cDovL2phdmFzY3JpcHQubndib3guY29tL0NvbnRlbnRMb2FkZWQvXHJcbiAgICogaHR0cDovL2phdmFzY3JpcHQubndib3guY29tL0NvbnRlbnRMb2FkZWQvTUlULUxJQ0VOU0VcclxuICAgKi9cclxuXHJcbiAgY29udGVudExvYWRlZCA9IGZ1bmN0aW9uKHdpbiwgZm4pIHtcclxuICAgIHZhciBhZGQsIGRvYywgZG9uZSwgaW5pdCwgcG9sbCwgcHJlLCByZW0sIHJvb3QsIHRvcDtcclxuICAgIGRvbmUgPSBmYWxzZTtcclxuICAgIHRvcCA9IHRydWU7XHJcbiAgICBkb2MgPSB3aW4uZG9jdW1lbnQ7XHJcbiAgICByb290ID0gZG9jLmRvY3VtZW50RWxlbWVudDtcclxuICAgIGFkZCA9IChkb2MuYWRkRXZlbnRMaXN0ZW5lciA/IFwiYWRkRXZlbnRMaXN0ZW5lclwiIDogXCJhdHRhY2hFdmVudFwiKTtcclxuICAgIHJlbSA9IChkb2MuYWRkRXZlbnRMaXN0ZW5lciA/IFwicmVtb3ZlRXZlbnRMaXN0ZW5lclwiIDogXCJkZXRhY2hFdmVudFwiKTtcclxuICAgIHByZSA9IChkb2MuYWRkRXZlbnRMaXN0ZW5lciA/IFwiXCIgOiBcIm9uXCIpO1xyXG4gICAgaW5pdCA9IGZ1bmN0aW9uKGUpIHtcclxuICAgICAgaWYgKGUudHlwZSA9PT0gXCJyZWFkeXN0YXRlY2hhbmdlXCIgJiYgZG9jLnJlYWR5U3RhdGUgIT09IFwiY29tcGxldGVcIikge1xyXG4gICAgICAgIHJldHVybjtcclxuICAgICAgfVxyXG4gICAgICAoZS50eXBlID09PSBcImxvYWRcIiA/IHdpbiA6IGRvYylbcmVtXShwcmUgKyBlLnR5cGUsIGluaXQsIGZhbHNlKTtcclxuICAgICAgaWYgKCFkb25lICYmIChkb25lID0gdHJ1ZSkpIHtcclxuICAgICAgICByZXR1cm4gZm4uY2FsbCh3aW4sIGUudHlwZSB8fCBlKTtcclxuICAgICAgfVxyXG4gICAgfTtcclxuICAgIHBvbGwgPSBmdW5jdGlvbigpIHtcclxuICAgICAgdmFyIGU7XHJcbiAgICAgIHRyeSB7XHJcbiAgICAgICAgcm9vdC5kb1Njcm9sbChcImxlZnRcIik7XHJcbiAgICAgIH0gY2F0Y2ggKF9lcnJvcikge1xyXG4gICAgICAgIGUgPSBfZXJyb3I7XHJcbiAgICAgICAgc2V0VGltZW91dChwb2xsLCA1MCk7XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgICB9XHJcbiAgICAgIHJldHVybiBpbml0KFwicG9sbFwiKTtcclxuICAgIH07XHJcbiAgICBpZiAoZG9jLnJlYWR5U3RhdGUgIT09IFwiY29tcGxldGVcIikge1xyXG4gICAgICBpZiAoZG9jLmNyZWF0ZUV2ZW50T2JqZWN0ICYmIHJvb3QuZG9TY3JvbGwpIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgdG9wID0gIXdpbi5mcmFtZUVsZW1lbnQ7XHJcbiAgICAgICAgfSBjYXRjaCAoX2Vycm9yKSB7fVxyXG4gICAgICAgIGlmICh0b3ApIHtcclxuICAgICAgICAgIHBvbGwoKTtcclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgICAgZG9jW2FkZF0ocHJlICsgXCJET01Db250ZW50TG9hZGVkXCIsIGluaXQsIGZhbHNlKTtcclxuICAgICAgZG9jW2FkZF0ocHJlICsgXCJyZWFkeXN0YXRlY2hhbmdlXCIsIGluaXQsIGZhbHNlKTtcclxuICAgICAgcmV0dXJuIHdpblthZGRdKHByZSArIFwibG9hZFwiLCBpbml0LCBmYWxzZSk7XHJcbiAgICB9XHJcbiAgfTtcclxuXHJcbiAgRHJvcHpvbmUuX2F1dG9EaXNjb3ZlckZ1bmN0aW9uID0gZnVuY3Rpb24oKSB7XHJcbiAgICBpZiAoRHJvcHpvbmUuYXV0b0Rpc2NvdmVyKSB7XHJcbiAgICAgIHJldHVybiBEcm9wem9uZS5kaXNjb3ZlcigpO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIGNvbnRlbnRMb2FkZWQod2luZG93LCBEcm9wem9uZS5fYXV0b0Rpc2NvdmVyRnVuY3Rpb24pO1xyXG5cclxufSkuY2FsbCh0aGlzKTtcclxuIiwiZGVmaW5lKGZ1bmN0aW9uKCkge1xyXG4gICAgY29uc3QgT3JkZXJfTGlzdF9EZXRhaWwgPSAoeyB1dWlkLCBiYXNrZXRGaWVsZDEsIGN1c3RvbWVySW5mbywgc3BlY2lhbFByb2R1Y3RUeXBlT2JqZWN0LCBiYXNrZXRGaWVsZDIsIHBvcywgY291bnQsIGFsbEdyb3NzLCBwcm9kdWN0LCBzdGF0dXMsIGFsbE5ldCB9LCBvcmRlclV1aWQpID0+IGBcclxuICAgICAgICA8dHIgc3R5bGU9XCIke3BzYy5vcmRlci5nZXRfcG9zX2JnX2NvbG9yKHN0YXR1cyl9XCIgY2xhc3M9XCIke3BzYy5vcmRlci5nZXRfcG9zX2JnX2NvbG9yX2NsYXNzKHN0YXR1cyl9XCI+XHJcbiAgICAgICAgPHRkPi08L3RkPlxyXG4gICAgICAgIDx0ZD48L3RkPlxyXG4gICAgICAgIDx0ZD4ke3Bvc308L3RkPlxyXG4gICAgICAgIDx0ZCBjb2xzcGFuPVwiNFwiPjxhIGhyZWY9XCIvYXBwcy9iYWNrZW5kL3Byb2R1Y3QvZWRpdC9pbmRleC8ke3Byb2R1Y3QudXVpZH1cIj4ke3Byb2R1Y3QudGl0bGV9PC9hPjxici8+QXJ0TnIgaW50ZXJuOiAke3Byb2R1Y3QubnJJbnRlcm59PGJyLz5cclxuICAgICAgICAke3BzYy5vcmRlci5nZXRfc3BlY2lhbF9wcm9kdWN0X29wdGlvbnMoc3BlY2lhbFByb2R1Y3RUeXBlT2JqZWN0KX1cclxuICAgICAgICA8L3RkPlxyXG4gICAgICAgIDx0ZD5BdWZsYWdlOiAke2NvdW50fTwvdGQ+XHJcbiAgICAgICAgPHRkPkt1bmRlbiBJbmZvOiAke2N1c3RvbWVySW5mb308L3RkPlxyXG4gICAgICAgIDx0ZD4ke2Jhc2tldEZpZWxkMX08L3RkPlxyXG4gICAgICAgIDx0ZD4ke2Jhc2tldEZpZWxkMn08L3RkPlxyXG4gICAgICAgIDx0ZD48L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzcz1cInRleHQtZW5kXCI+JHtuZXcgSW50bC5OdW1iZXJGb3JtYXQoJ2RlLURFJywgeyBzdHlsZTogJ2N1cnJlbmN5JywgY3VycmVuY3k6ICdFVVInIH0pLmZvcm1hdChhbGxOZXQpfSA8c3Ryb25nPigke25ldyBJbnRsLk51bWJlckZvcm1hdCgnZGUtREUnLCB7IHN0eWxlOiAnY3VycmVuY3knLCBjdXJyZW5jeTogJ0VVUicgfSkuZm9ybWF0KGFsbEdyb3NzKX0pPC9zdHJvbmc+PC90ZD5cclxuICAgICAgICA8dGQ+PGRpdiBjbGFzcz1cImJ0bi1ncm91cCBidG4tZ3JvdXAtc21cIj5cclxuICAgICAgICAgICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiYnRuIGJ0bi1wcmltYXJ5IGRyb3Bkb3duLXRvZ2dsZVwiIGRhdGEtYnMtdG9nZ2xlPVwiZHJvcGRvd25cIiBhcmlhLWhhc3BvcHVwPVwidHJ1ZVwiIGFyaWEtZXhwYW5kZWQ9XCJmYWxzZVwiPlxyXG4gICAgICAgICAgICAgICAgJHtwc2Mub3JkZXIuZ2V0X3Bvc19zdGF0dXMoc3RhdHVzKS5pbnRlcm5hbE5hbWV9XHJcbiAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDxkaXYgY2xhc3M9XCJkcm9wZG93bi1tZW51XCI+XHJcbiAgICAgICAgICAgICAgICAgICAgJHtwc2Mub3JkZXIuZ2V0X3Bvc19zdGF0dXNfbG9vcChvcmRlclV1aWQsIHV1aWQpfVxyXG4gICAgICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgICAgIDwvZGl2PjwvdGQ+XHJcbiAgICAgICAgPHRkPjwvdGQ+XHJcbiAgICA8L3RyPlxyXG4gICAgYDtcclxuXHJcbiAgICBjb25zdCBPcmRlcl9MaXN0X0RldGFpbF9TaW1wbGUgPSAoeyB1dWlkLCBiYXNrZXRGaWVsZDEsIGN1c3RvbWVySW5mbywgc3BlY2lhbFByb2R1Y3RUeXBlT2JqZWN0LCBiYXNrZXRGaWVsZDIsIHBvcywgY291bnQsIGFsbEdyb3NzLCBwcm9kdWN0LCBzdGF0dXMsIGFsbE5ldCB9LCBvcmRlclV1aWQpID0+IGBcclxuICAgICAgICA8dHIgc3R5bGU9XCIke3BzYy5vcmRlci5nZXRfcG9zX2JnX2NvbG9yKHN0YXR1cyl9XCIgY2xhc3M9XCIke3BzYy5vcmRlci5nZXRfcG9zX2JnX2NvbG9yX2NsYXNzKHN0YXR1cyl9XCI+XHJcbiAgICAgICAgPHRkPi08L3RkPlxyXG4gICAgICAgIDx0ZD48L3RkPlxyXG4gICAgICAgIDx0ZD4ke3Bvc308L3RkPlxyXG4gICAgICAgIDx0ZCBjb2xzcGFuPVwiNFwiPjxhIGhyZWY9XCIvYXBwcy9iYWNrZW5kL3Byb2R1Y3QvZWRpdC9pbmRleC8ke3Byb2R1Y3QudXVpZH1cIj4ke3Byb2R1Y3QudGl0bGV9PC9hPjxici8+QXJ0TnIgaW50ZXJuOiAke3Byb2R1Y3QubnJJbnRlcm59PGJyLz5cclxuICAgICAgICAke3BzYy5vcmRlci5nZXRfc3BlY2lhbF9wcm9kdWN0X29wdGlvbnMoc3BlY2lhbFByb2R1Y3RUeXBlT2JqZWN0KX08L3RkPlxyXG4gICAgICAgIDx0ZD5BdWZsYWdlOiAke2NvdW50fTwvdGQ+XHJcbiAgICAgICAgPHRkPkt1bmRlbiBJbmZvOiAke2N1c3RvbWVySW5mb308L3RkPlxyXG4gICAgICAgIDx0ZD4ke2Jhc2tldEZpZWxkMX08L3RkPlxyXG4gICAgICAgIDx0ZD4ke2Jhc2tldEZpZWxkMn08L3RkPlxyXG4gICAgICAgIDx0ZD48L3RkPlxyXG4gICAgICAgIDx0ZCBjbGFzcz1cInRleHQtZW5kXCI+JHtuZXcgSW50bC5OdW1iZXJGb3JtYXQoJ2RlLURFJywgeyBzdHlsZTogJ2N1cnJlbmN5JywgY3VycmVuY3k6ICdFVVInIH0pLmZvcm1hdChhbGxOZXQpfSA8c3Ryb25nPigke25ldyBJbnRsLk51bWJlckZvcm1hdCgnZGUtREUnLCB7IHN0eWxlOiAnY3VycmVuY3knLCBjdXJyZW5jeTogJ0VVUicgfSkuZm9ybWF0KGFsbEdyb3NzKX0pPC9zdHJvbmc+PC90ZD5cclxuICAgICAgICA8dGQ+JHtwc2Mub3JkZXIuZ2V0X3Bvc19zdGF0dXMoc3RhdHVzKS5pbnRlcm5hbE5hbWV9PC90ZD5cclxuICAgICAgICA8dGQ+PC90ZD5cclxuICAgIDwvdHI+XHJcbiAgICBgO1xyXG5cclxuICAgIGNvbnN0IE9yZGVyX0xpc3RfUG9zX1N0YXR1cyA9IChzdGF0dXMsIG9yZGVyVXVpZCwgcG9zVXVpZCkgPT4gYFxyXG4gICAgICAgIDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwiZHJvcGRvd24taXRlbSBzd2l0Y2hfcG9zXCIgZGF0YS1vcmRlci11dWlkPVwiJHtvcmRlclV1aWR9XCIgZGF0YS1wb3MtdXVpZD1cIiR7cG9zVXVpZH1cIiBkYXRhLXN0YXR1cz1cIiR7c3RhdHVzLmNvZGV9XCI+JHtzdGF0dXMuaW50ZXJuYWxOYW1lfTwvYnV0dG9uPlxyXG4gICAgYDtcclxuXHJcbiAgICBjb25zdCBPcmRlcl9MaXN0X1Bvc19DYWxjX09wdGlvbiA9IChlbG0pID0+IGBcclxuICAgICAgICA8dHI+PHRkPiR7ZWxtLm5hbWV9PC90ZD48dGQ+JHtlbG0udmFsdWV9PC90ZD48L3RyPlxyXG4gICAgYDtcclxuICAgIFxyXG4gICAgcmV0dXJuIHtcclxuICAgICAgICBcclxuICAgICAgICBpbml0KCkge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5sb2FkX3N0YXR1cygpO1xyXG4gICAgICAgICAgICB0aGlzLmJpbmRfYnV0dG9ucygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldF9zcGVjaWFsX3Byb2R1Y3Rfb3B0aW9ucyhzcGVjaWFsUHJvZHVjdFR5cGVPYmplY3QpIHtcclxuICAgICAgICAgICAgaWYoc3BlY2lhbFByb2R1Y3RUeXBlT2JqZWN0LnR5cCAhPSA2KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJyc7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiAnPHRhYmxlPicgKyBzcGVjaWFsUHJvZHVjdFR5cGVPYmplY3Qub3B0aW9ucy5tYXAoZWxtID0+IHtcclxuICAgICAgICAgICAgICAgIGlmKGVsbS50eXBlICE9ICdoaWRkZW4nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIE9yZGVyX0xpc3RfUG9zX0NhbGNfT3B0aW9uKGVsbSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pLmpvaW4oJycpICsgJzwvdGFibGU+JztcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBsb2FkX3N0YXR1cygpIHtcclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIHVybDogXCIvYXBwcy9hcGkvc3lzdGVtL3N0YXR1c1wiLFxyXG4gICAgICAgICAgICAgICAgY29udGVudFR5cGU6IFwiYXBwbGljYXRpb24vanNvblwiLFxyXG4gICAgICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgICAgICAgIFwiQXV0aG9yaXphdGlvblwiOiBcIkJlYXJlciBcIiArIGp3dF90b2tlblxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIG1ldGhvZDogJ0dFVCcsXHJcbiAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbihyZXN1bHQpIHtcclxuICAgICAgICAgICAgICAgICAgICBwc2Mub3JkZXIuc3RhdHVzX29yZGVyID0gcmVzdWx0Lm9yZGVyO1xyXG4gICAgICAgICAgICAgICAgICAgIHBzYy5vcmRlci5zdGF0dXNfcG9zID0gcmVzdWx0LnBvc2l0aW9uO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICBiaW5kX2J1dHRvbnMoKSB7XHJcbiAgICAgICAgICAgICQoJy50b29nbGVfbGlzdF9kZXRhaWxfdmlldycpLm9mZigpO1xyXG4gICAgICAgICAgICAkKCcudG9vZ2xlX2xpc3RfZGV0YWlsX3ZpZXcnKS5vbignY2xpY2snLCBmdW5jdGlvbigpIHtcclxuICAgICAgICAgICAgICAgIHZhciB1dWlkID0gJCh0aGlzKS5kYXRhKCkudXVpZDsgXHJcbiAgICAgICAgICAgICAgICBpZigkKCcjcm93cy0nICsgdXVpZCArICcgPiB0cicpLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAkKCcjcm93cy0nICsgdXVpZCkuaHRtbCgnJyk7XHJcbiAgICAgICAgICAgICAgICB9ZWxzZXtcclxuICAgICAgICAgICAgICAgICAgICBwc2Mub3JkZXIubG9hZF9kZXRhaWwodXVpZCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgJCgnLnRvb2dsZV9saXN0X3NpbXBsZV9kZXRhaWxfdmlldycpLm9mZigpO1xyXG4gICAgICAgICAgICAkKCcudG9vZ2xlX2xpc3Rfc2ltcGxlX2RldGFpbF92aWV3Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgdXVpZCA9ICQodGhpcykuZGF0YSgpLnV1aWQ7IFxyXG4gICAgICAgICAgICAgICAgaWYoJCgnI3Jvd3MtJyArIHV1aWQgKyAnID4gdHInKS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI3Jvd3MtJyArIHV1aWQpLmh0bWwoJycpO1xyXG4gICAgICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAgICAgcHNjLm9yZGVyLmxvYWRfZGV0YWlsKHV1aWQsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIFxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGxvYWRfZGV0YWlsKG9yZGVyVXVpZCwgc2ltcGxlID0gZmFsc2UpIHtcclxuICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgIHVybDogXCIvYXBwcy9hcGkvb3JkZXIvZ2V0b25lYnl1dWlkXCIsXHJcbiAgICAgICAgICAgICAgICBjb250ZW50VHlwZTogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgICAgICAgICBoZWFkZXJzOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgXCJBdXRob3JpemF0aW9uXCI6IFwiQmVhcmVyIFwiICsgand0X3Rva2VuXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgbWV0aG9kOiAnUE9TVCcsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiBKU09OLnN0cmluZ2lmeSh7XHJcbiAgICAgICAgICAgICAgICAgICAgdXVpZDogb3JkZXJVdWlkXHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHBzYy5vcmRlci5idWlsZF9kZXRhaWxzKG9yZGVyVXVpZCwgcmVzdWx0LCBzaW1wbGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGJ1aWxkX2RldGFpbHMob3JkZXJVdWlkLCByZXN1bHQsIHNpbXBsZSA9IGZhbHNlKSB7XHJcbiAgICAgICAgICAgIGlmKHNpbXBsZSkge1xyXG4gICAgICAgICAgICAgICAgJCgnI3Jvd3MtJyArIHJlc3VsdC51dWlkKS5odG1sKHJlc3VsdC5wb3NpdGlvbnMubWFwKHggPT4gT3JkZXJfTGlzdF9EZXRhaWxfU2ltcGxlKHgsIG9yZGVyVXVpZCkpLmpvaW4oJycpKTtcclxuICAgICAgICAgICAgfWVsc2V7XHJcbiAgICAgICAgICAgICAgICAkKCcjcm93cy0nICsgcmVzdWx0LnV1aWQpLmh0bWwocmVzdWx0LnBvc2l0aW9ucy5tYXAoeCA9PiBPcmRlcl9MaXN0X0RldGFpbCh4LCBvcmRlclV1aWQpKS5qb2luKCcnKSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgICAgIHRoaXMuYmluZF9zdGF0dXNfYnV0dG9ucygpO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGJpbmRfc3RhdHVzX2J1dHRvbnMoKSB7XHJcbiAgICAgICAgICAgICQoJy5zd2l0Y2hfcG9zJykub2ZmKCk7XHJcbiAgICAgICAgICAgICQoJy5zd2l0Y2hfcG9zJykub24oJ2NsaWNrJywgZnVuY3Rpb24oKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIG9yZGVyVXVpZCA9ICQodGhpcykuZGF0YSgpLm9yZGVyVXVpZDtcclxuICAgICAgICAgICAgICAgIHZhciBwb3NVdWlkID0gJCh0aGlzKS5kYXRhKCkucG9zVXVpZDtcclxuICAgICAgICAgICAgICAgIHZhciBzdGF0dXMgPSAkKHRoaXMpLmRhdGEoKS5zdGF0dXM7XHJcblxyXG4gICAgICAgICAgICAgICAgJC5hamF4KHtcclxuICAgICAgICAgICAgICAgICAgICB1cmw6IFwiL2FwcHMvYXBpL3Bvc2l0aW9uL3N0YXR1cy9jaGFuZ2VcIixcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50VHlwZTogXCJhcHBsaWNhdGlvbi9qc29uXCIsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcIkF1dGhvcml6YXRpb25cIjogXCJCZWFyZXIgXCIgKyBqd3RfdG9rZW5cclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IEpTT04uc3RyaW5naWZ5KHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcG9zaXRpb246IHBvc1V1aWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN0YXR1czogc3RhdHVzLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIG1ldGhvZDogJ1BPU1QnLFxyXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKHJlc3VsdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjcm93cy0nICsgb3JkZXJVdWlkKS5odG1sKCcnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHNjLm9yZGVyLmxvYWRfZGV0YWlsKG9yZGVyVXVpZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldF9wb3NfYmdfY29sb3Ioc3RhdHVzKSB7XHJcblxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICB7JSBpZiBvcmRlclN0YXR1c2UuZ2V0UG9zU3RhdHVzQ29sb3IocG9zLnN0YXR1cykgIT0gJycgJX1iYWNrZ3JvdW5kLWNvbG9yOiB7eyBvcmRlclN0YXR1c2UuZ2V0UG9zU3RhdHVzQ29sb3IocG9zLnN0YXR1cykgfX17JSBlbmRpZiAlfVwiXHJcbiAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIGNvbnN0IGZvdW5kID0gcHNjLm9yZGVyLnN0YXR1c19wb3MuZmluZChzdGF0ID0+IHN0YXQuY29kZSA9PSBzdGF0dXMpO1xyXG4gICAgICAgICAgICBcclxuICAgICAgICAgICAgaWYoZm91bmQgJiYgZm91bmQuY29sb3IgIT0gXCJcIikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICdiYWNrZ3JvdW5kLWNvbG9yOiAnICsgZm91bmQuY29sb3I7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0X3Bvc19zdGF0dXMoc3RhdHVzKSB7XHJcblxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICB7JSBpZiBvcmRlclN0YXR1c2UuZ2V0UG9zU3RhdHVzQ29sb3IocG9zLnN0YXR1cykgIT0gJycgJX1iYWNrZ3JvdW5kLWNvbG9yOiB7eyBvcmRlclN0YXR1c2UuZ2V0UG9zU3RhdHVzQ29sb3IocG9zLnN0YXR1cykgfX17JSBlbmRpZiAlfVwiXHJcbiAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgIGNvbnN0IGZvdW5kID0gcHNjLm9yZGVyLnN0YXR1c19wb3MuZmluZChzdGF0ID0+IHN0YXQuY29kZSA9PSBzdGF0dXMpO1xyXG4gICAgICAgICAgICBpZihmb3VuZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZvdW5kO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4ge2ludGVybmFsTmFtZTogJ25vdEZvdW5kJ307XHJcblxyXG4gICAgICAgIH0sXHJcblxyXG4gICAgICAgIGdldF9wb3NfYmdfY29sb3JfY2xhc3Moc3RhdHVzKSB7XHJcblxyXG4gICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICBjbGFzcz1cInslIGlmIHBvcy5zdGF0dXMgPT0gMTcwICV9YmctbGlnaHRkYXJreyUgZWxzZWlmIHBvcy5zdGF0dXMgPT0gMjAwIG9yIHBvcy5zdGF0dXMgPT0gMjEwICV9YmctbGlnaHRzdWNjZXNzeyUgZWxzZSAlfWJnLWxpZ2h0ZGFuZ2VyeyUgZW5kaWYgJX1cclxuICAgICAgICAgICAgKi9cclxuXHJcbiAgICAgICAgICAgIGlmKHN0YXR1cyA9PSAxNzApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnYmctbGlnaHRkYXJrJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZihzdGF0dXMgPT0gMjAwIHx8IHN0YXR1cyA9PSAyMTApIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnYmctbGlnaHRzdWNjZXNzJztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuICdiZy1saWdodGRhbmdlcic7XHJcbiAgICAgICAgfSxcclxuXHJcbiAgICAgICAgZ2V0X3Bvc19zdGF0dXNfbG9vcChvcmRlclV1aWQsIHBvc1V1aWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHBzYy5vcmRlci5zdGF0dXNfcG9zLm1hcCh4ID0+IE9yZGVyX0xpc3RfUG9zX1N0YXR1cyh4LCBvcmRlclV1aWQsIHBvc1V1aWQpKS5qb2luKCcnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgXHJcbiAgICB9XHJcbn0pOyIsIihmdW5jdGlvbiAoZmFjdG9yeSkge1xyXG4gICAgLyogZ2xvYmFsIGRlZmluZSAqL1xyXG4gICAgaWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xyXG4gICAgICAgIC8vIEFNRC4gUmVnaXN0ZXIgYXMgYW4gYW5vbnltb3VzIG1vZHVsZS5cclxuICAgICAgICBkZWZpbmUoWydqcXVlcnknXSwgZmFjdG9yeSk7XHJcbiAgICB9IGVsc2Uge1xyXG4gICAgICAgIC8vIEJyb3dzZXIgZ2xvYmFsczogalF1ZXJ5XHJcbiAgICAgICAgZmFjdG9yeSh3aW5kb3cualF1ZXJ5KTtcclxuICAgIH1cclxufShmdW5jdGlvbiAoJCl7XHJcbiAgICAkLmV4dGVuZCgkLnN1bW1lcm5vdGUucGx1Z2lucywge1xyXG4gICAgICAgICdtZWRpYSc6IGZ1bmN0aW9uKGNvbnRleHQpIHtcclxuICAgICAgICAgICAgdmFyIHNlbGYgPSB0aGlzO1xyXG5cclxuICAgICAgICAgICAgLy8gdWkgaGFzIHJlbmRlcnMgdG8gYnVpbGQgdWkgZWxlbWVudHMuXHJcbiAgICAgICAgICAgIC8vICAtIHlvdSBjYW4gY3JlYXRlIGEgYnV0dG9uIHdpdGggYHVpLmJ1dHRvbmBcclxuICAgICAgICAgICAgdmFyIHVpID0gJC5zdW1tZXJub3RlLnVpO1xyXG5cclxuICAgICAgICAgICAgLy8gYWRkIGhlbGxvIGJ1dHRvblxyXG4gICAgICAgICAgICBjb250ZXh0Lm1lbW8oJ2J1dHRvbi5tZWRpYScsIGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgLy8gY3JlYXRlIGJ1dHRvblxyXG4gICAgICAgICAgICAgICAgdmFyIGJ1dHRvbiA9IHVpLmJ1dHRvbih7XHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudHM6ICc8aSBjbGFzcz1cImZhIGZhLWltYWdlc1wiLz4gTWVkaWEnLFxyXG4gICAgICAgICAgICAgICAgICAgIHRvb2x0aXA6ICdNZWRpYScsXHJcbiAgICAgICAgICAgICAgICAgICAgY2xpY2s6IGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZWRpYUJ1bmRsZUJyb3dzZXIoY29udGV4dC4kbm90ZS5hdHRyKCdpZCcpKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gY3JlYXRlIGpRdWVyeSBvYmplY3QgZnJvbSBidXR0b24gaW5zdGFuY2UuXHJcbiAgICAgICAgICAgICAgICB2YXIgJG1lZGlhID0gYnV0dG9uLnJlbmRlcigpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICRtZWRpYTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG59KSk7IiwiIWZ1bmN0aW9uKHQsZSl7XCJmdW5jdGlvblwiPT10eXBlb2YgZGVmaW5lJiZkZWZpbmUuYW1kP2RlZmluZShlKTpcIm9iamVjdFwiPT10eXBlb2YgZXhwb3J0cz9tb2R1bGUuZXhwb3J0cz1lKHJlcXVpcmUsZXhwb3J0cyxtb2R1bGUpOnQuVGV0aGVyPWUoKX0odGhpcyxmdW5jdGlvbih0LGUsbyl7XCJ1c2Ugc3RyaWN0XCI7ZnVuY3Rpb24gbih0LGUpe2lmKCEodCBpbnN0YW5jZW9mIGUpKXRocm93IG5ldyBUeXBlRXJyb3IoXCJDYW5ub3QgY2FsbCBhIGNsYXNzIGFzIGEgZnVuY3Rpb25cIil9ZnVuY3Rpb24gaSh0KXt2YXIgZT10LmdldEJvdW5kaW5nQ2xpZW50UmVjdCgpLG89e307Zm9yKHZhciBuIGluIGUpb1tuXT1lW25dO2lmKHQub3duZXJEb2N1bWVudCE9PWRvY3VtZW50KXt2YXIgcj10Lm93bmVyRG9jdW1lbnQuZGVmYXVsdFZpZXcuZnJhbWVFbGVtZW50O2lmKHIpe3ZhciBzPWkocik7by50b3ArPXMudG9wLG8uYm90dG9tKz1zLnRvcCxvLmxlZnQrPXMubGVmdCxvLnJpZ2h0Kz1zLmxlZnR9fXJldHVybiBvfWZ1bmN0aW9uIHIodCl7dmFyIGU9Z2V0Q29tcHV0ZWRTdHlsZSh0KXx8e30sbz1lLnBvc2l0aW9uLG49W107aWYoXCJmaXhlZFwiPT09bylyZXR1cm5bdF07Zm9yKHZhciBpPXQ7KGk9aS5wYXJlbnROb2RlKSYmaSYmMT09PWkubm9kZVR5cGU7KXt2YXIgcj12b2lkIDA7dHJ5e3I9Z2V0Q29tcHV0ZWRTdHlsZShpKX1jYXRjaChzKXt9aWYoXCJ1bmRlZmluZWRcIj09dHlwZW9mIHJ8fG51bGw9PT1yKXJldHVybiBuLnB1c2goaSksbjt2YXIgYT1yLGY9YS5vdmVyZmxvdyxsPWEub3ZlcmZsb3dYLGg9YS5vdmVyZmxvd1k7LyhhdXRvfHNjcm9sbCkvLnRlc3QoZitoK2wpJiYoXCJhYnNvbHV0ZVwiIT09b3x8W1wicmVsYXRpdmVcIixcImFic29sdXRlXCIsXCJmaXhlZFwiXS5pbmRleE9mKHIucG9zaXRpb24pPj0wKSYmbi5wdXNoKGkpfXJldHVybiBuLnB1c2godC5vd25lckRvY3VtZW50LmJvZHkpLHQub3duZXJEb2N1bWVudCE9PWRvY3VtZW50JiZuLnB1c2godC5vd25lckRvY3VtZW50LmRlZmF1bHRWaWV3KSxufWZ1bmN0aW9uIHMoKXtBJiZkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKEEpLEE9bnVsbH1mdW5jdGlvbiBhKHQpe3ZhciBlPXZvaWQgMDt0PT09ZG9jdW1lbnQ/KGU9ZG9jdW1lbnQsdD1kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQpOmU9dC5vd25lckRvY3VtZW50O3ZhciBvPWUuZG9jdW1lbnRFbGVtZW50LG49aSh0KSxyPVAoKTtyZXR1cm4gbi50b3AtPXIudG9wLG4ubGVmdC09ci5sZWZ0LFwidW5kZWZpbmVkXCI9PXR5cGVvZiBuLndpZHRoJiYobi53aWR0aD1kb2N1bWVudC5ib2R5LnNjcm9sbFdpZHRoLW4ubGVmdC1uLnJpZ2h0KSxcInVuZGVmaW5lZFwiPT10eXBlb2Ygbi5oZWlnaHQmJihuLmhlaWdodD1kb2N1bWVudC5ib2R5LnNjcm9sbEhlaWdodC1uLnRvcC1uLmJvdHRvbSksbi50b3A9bi50b3Atby5jbGllbnRUb3Asbi5sZWZ0PW4ubGVmdC1vLmNsaWVudExlZnQsbi5yaWdodD1lLmJvZHkuY2xpZW50V2lkdGgtbi53aWR0aC1uLmxlZnQsbi5ib3R0b209ZS5ib2R5LmNsaWVudEhlaWdodC1uLmhlaWdodC1uLnRvcCxufWZ1bmN0aW9uIGYodCl7cmV0dXJuIHQub2Zmc2V0UGFyZW50fHxkb2N1bWVudC5kb2N1bWVudEVsZW1lbnR9ZnVuY3Rpb24gbCgpe3ZhciB0PWRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoXCJkaXZcIik7dC5zdHlsZS53aWR0aD1cIjEwMCVcIix0LnN0eWxlLmhlaWdodD1cIjIwMHB4XCI7dmFyIGU9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKTtoKGUuc3R5bGUse3Bvc2l0aW9uOlwiYWJzb2x1dGVcIix0b3A6MCxsZWZ0OjAscG9pbnRlckV2ZW50czpcIm5vbmVcIix2aXNpYmlsaXR5OlwiaGlkZGVuXCIsd2lkdGg6XCIyMDBweFwiLGhlaWdodDpcIjE1MHB4XCIsb3ZlcmZsb3c6XCJoaWRkZW5cIn0pLGUuYXBwZW5kQ2hpbGQodCksZG9jdW1lbnQuYm9keS5hcHBlbmRDaGlsZChlKTt2YXIgbz10Lm9mZnNldFdpZHRoO2Uuc3R5bGUub3ZlcmZsb3c9XCJzY3JvbGxcIjt2YXIgbj10Lm9mZnNldFdpZHRoO289PT1uJiYobj1lLmNsaWVudFdpZHRoKSxkb2N1bWVudC5ib2R5LnJlbW92ZUNoaWxkKGUpO3ZhciBpPW8tbjtyZXR1cm57d2lkdGg6aSxoZWlnaHQ6aX19ZnVuY3Rpb24gaCgpe3ZhciB0PWFyZ3VtZW50cy5sZW5ndGg8PTB8fHZvaWQgMD09PWFyZ3VtZW50c1swXT97fTphcmd1bWVudHNbMF0sZT1bXTtyZXR1cm4gQXJyYXkucHJvdG90eXBlLnB1c2guYXBwbHkoZSxhcmd1bWVudHMpLGUuc2xpY2UoMSkuZm9yRWFjaChmdW5jdGlvbihlKXtpZihlKWZvcih2YXIgbyBpbiBlKSh7fSkuaGFzT3duUHJvcGVydHkuY2FsbChlLG8pJiYodFtvXT1lW29dKX0pLHR9ZnVuY3Rpb24gdSh0LGUpe2lmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiB0LmNsYXNzTGlzdCllLnNwbGl0KFwiIFwiKS5mb3JFYWNoKGZ1bmN0aW9uKGUpe2UudHJpbSgpJiZ0LmNsYXNzTGlzdC5yZW1vdmUoZSl9KTtlbHNle3ZhciBvPW5ldyBSZWdFeHAoXCIoXnwgKVwiK2Uuc3BsaXQoXCIgXCIpLmpvaW4oXCJ8XCIpK1wiKCB8JClcIixcImdpXCIpLG49Yyh0KS5yZXBsYWNlKG8sXCIgXCIpO2codCxuKX19ZnVuY3Rpb24gZCh0LGUpe2lmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiB0LmNsYXNzTGlzdCllLnNwbGl0KFwiIFwiKS5mb3JFYWNoKGZ1bmN0aW9uKGUpe2UudHJpbSgpJiZ0LmNsYXNzTGlzdC5hZGQoZSl9KTtlbHNle3UodCxlKTt2YXIgbz1jKHQpKyhcIiBcIitlKTtnKHQsbyl9fWZ1bmN0aW9uIHAodCxlKXtpZihcInVuZGVmaW5lZFwiIT10eXBlb2YgdC5jbGFzc0xpc3QpcmV0dXJuIHQuY2xhc3NMaXN0LmNvbnRhaW5zKGUpO3ZhciBvPWModCk7cmV0dXJuIG5ldyBSZWdFeHAoXCIoXnwgKVwiK2UrXCIoIHwkKVwiLFwiZ2lcIikudGVzdChvKX1mdW5jdGlvbiBjKHQpe3JldHVybiB0LmNsYXNzTmFtZSBpbnN0YW5jZW9mIHQub3duZXJEb2N1bWVudC5kZWZhdWx0Vmlldy5TVkdBbmltYXRlZFN0cmluZz90LmNsYXNzTmFtZS5iYXNlVmFsOnQuY2xhc3NOYW1lfWZ1bmN0aW9uIGcodCxlKXt0LnNldEF0dHJpYnV0ZShcImNsYXNzXCIsZSl9ZnVuY3Rpb24gbSh0LGUsbyl7by5mb3JFYWNoKGZ1bmN0aW9uKG8pey0xPT09ZS5pbmRleE9mKG8pJiZwKHQsbykmJnUodCxvKX0pLGUuZm9yRWFjaChmdW5jdGlvbihlKXtwKHQsZSl8fGQodCxlKX0pfWZ1bmN0aW9uIG4odCxlKXtpZighKHQgaW5zdGFuY2VvZiBlKSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiQ2Fubm90IGNhbGwgYSBjbGFzcyBhcyBhIGZ1bmN0aW9uXCIpfWZ1bmN0aW9uIHYodCxlKXtpZihcImZ1bmN0aW9uXCIhPXR5cGVvZiBlJiZudWxsIT09ZSl0aHJvdyBuZXcgVHlwZUVycm9yKFwiU3VwZXIgZXhwcmVzc2lvbiBtdXN0IGVpdGhlciBiZSBudWxsIG9yIGEgZnVuY3Rpb24sIG5vdCBcIit0eXBlb2YgZSk7dC5wcm90b3R5cGU9T2JqZWN0LmNyZWF0ZShlJiZlLnByb3RvdHlwZSx7Y29uc3RydWN0b3I6e3ZhbHVlOnQsZW51bWVyYWJsZTohMSx3cml0YWJsZTohMCxjb25maWd1cmFibGU6ITB9fSksZSYmKE9iamVjdC5zZXRQcm90b3R5cGVPZj9PYmplY3Quc2V0UHJvdG90eXBlT2YodCxlKTp0Ll9fcHJvdG9fXz1lKX1mdW5jdGlvbiB5KHQsZSl7dmFyIG89YXJndW1lbnRzLmxlbmd0aDw9Mnx8dm9pZCAwPT09YXJndW1lbnRzWzJdPzE6YXJndW1lbnRzWzJdO3JldHVybiB0K28+PWUmJmU+PXQtb31mdW5jdGlvbiBiKCl7cmV0dXJuXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHBlcmZvcm1hbmNlJiZcInVuZGVmaW5lZFwiIT10eXBlb2YgcGVyZm9ybWFuY2Uubm93P3BlcmZvcm1hbmNlLm5vdygpOituZXcgRGF0ZX1mdW5jdGlvbiB3KCl7Zm9yKHZhciB0PXt0b3A6MCxsZWZ0OjB9LGU9YXJndW1lbnRzLmxlbmd0aCxvPUFycmF5KGUpLG49MDtlPm47bisrKW9bbl09YXJndW1lbnRzW25dO3JldHVybiBvLmZvckVhY2goZnVuY3Rpb24oZSl7dmFyIG89ZS50b3Asbj1lLmxlZnQ7XCJzdHJpbmdcIj09dHlwZW9mIG8mJihvPXBhcnNlRmxvYXQobywxMCkpLFwic3RyaW5nXCI9PXR5cGVvZiBuJiYobj1wYXJzZUZsb2F0KG4sMTApKSx0LnRvcCs9byx0LmxlZnQrPW59KSx0fWZ1bmN0aW9uIEModCxlKXtyZXR1cm5cInN0cmluZ1wiPT10eXBlb2YgdC5sZWZ0JiYtMSE9PXQubGVmdC5pbmRleE9mKFwiJVwiKSYmKHQubGVmdD1wYXJzZUZsb2F0KHQubGVmdCwxMCkvMTAwKmUud2lkdGgpLFwic3RyaW5nXCI9PXR5cGVvZiB0LnRvcCYmLTEhPT10LnRvcC5pbmRleE9mKFwiJVwiKSYmKHQudG9wPXBhcnNlRmxvYXQodC50b3AsMTApLzEwMCplLmhlaWdodCksdH1mdW5jdGlvbiBPKHQsZSl7cmV0dXJuXCJzY3JvbGxQYXJlbnRcIj09PWU/ZT10LnNjcm9sbFBhcmVudHNbMF06XCJ3aW5kb3dcIj09PWUmJihlPVtwYWdlWE9mZnNldCxwYWdlWU9mZnNldCxpbm5lcldpZHRoK3BhZ2VYT2Zmc2V0LGlubmVySGVpZ2h0K3BhZ2VZT2Zmc2V0XSksZT09PWRvY3VtZW50JiYoZT1lLmRvY3VtZW50RWxlbWVudCksXCJ1bmRlZmluZWRcIiE9dHlwZW9mIGUubm9kZVR5cGUmJiFmdW5jdGlvbigpe3ZhciB0PWUsbz1hKGUpLG49byxpPWdldENvbXB1dGVkU3R5bGUoZSk7aWYoZT1bbi5sZWZ0LG4udG9wLG8ud2lkdGgrbi5sZWZ0LG8uaGVpZ2h0K24udG9wXSx0Lm93bmVyRG9jdW1lbnQhPT1kb2N1bWVudCl7dmFyIHI9dC5vd25lckRvY3VtZW50LmRlZmF1bHRWaWV3O2VbMF0rPXIucGFnZVhPZmZzZXQsZVsxXSs9ci5wYWdlWU9mZnNldCxlWzJdKz1yLnBhZ2VYT2Zmc2V0LGVbM10rPXIucGFnZVlPZmZzZXR9JC5mb3JFYWNoKGZ1bmN0aW9uKHQsbyl7dD10WzBdLnRvVXBwZXJDYXNlKCkrdC5zdWJzdHIoMSksXCJUb3BcIj09PXR8fFwiTGVmdFwiPT09dD9lW29dKz1wYXJzZUZsb2F0KGlbXCJib3JkZXJcIit0K1wiV2lkdGhcIl0pOmVbb10tPXBhcnNlRmxvYXQoaVtcImJvcmRlclwiK3QrXCJXaWR0aFwiXSl9KX0oKSxlfXZhciBFPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gdCh0LGUpe2Zvcih2YXIgbz0wO288ZS5sZW5ndGg7bysrKXt2YXIgbj1lW29dO24uZW51bWVyYWJsZT1uLmVudW1lcmFibGV8fCExLG4uY29uZmlndXJhYmxlPSEwLFwidmFsdWVcImluIG4mJihuLndyaXRhYmxlPSEwKSxPYmplY3QuZGVmaW5lUHJvcGVydHkodCxuLmtleSxuKX19cmV0dXJuIGZ1bmN0aW9uKGUsbyxuKXtyZXR1cm4gbyYmdChlLnByb3RvdHlwZSxvKSxuJiZ0KGUsbiksZX19KCkseD12b2lkIDA7XCJ1bmRlZmluZWRcIj09dHlwZW9mIHgmJih4PXttb2R1bGVzOltdfSk7dmFyIEE9bnVsbCxUPWZ1bmN0aW9uKCl7dmFyIHQ9MDtyZXR1cm4gZnVuY3Rpb24oKXtyZXR1cm4rK3R9fSgpLFM9e30sUD1mdW5jdGlvbigpe3ZhciB0PUE7dHx8KHQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKSx0LnNldEF0dHJpYnV0ZShcImRhdGEtdGV0aGVyLWlkXCIsVCgpKSxoKHQuc3R5bGUse3RvcDowLGxlZnQ6MCxwb3NpdGlvbjpcImFic29sdXRlXCJ9KSxkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHQpLEE9dCk7dmFyIGU9dC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRldGhlci1pZFwiKTtyZXR1cm5cInVuZGVmaW5lZFwiPT10eXBlb2YgU1tlXSYmKFNbZV09aSh0KSxNKGZ1bmN0aW9uKCl7ZGVsZXRlIFNbZV19KSksU1tlXX0sVz1bXSxNPWZ1bmN0aW9uKHQpe1cucHVzaCh0KX0sXz1mdW5jdGlvbigpe2Zvcih2YXIgdD12b2lkIDA7dD1XLnBvcCgpOyl0KCl9LGs9ZnVuY3Rpb24oKXtmdW5jdGlvbiB0KCl7bih0aGlzLHQpfXJldHVybiBFKHQsW3trZXk6XCJvblwiLHZhbHVlOmZ1bmN0aW9uKHQsZSxvKXt2YXIgbj1hcmd1bWVudHMubGVuZ3RoPD0zfHx2b2lkIDA9PT1hcmd1bWVudHNbM10/ITE6YXJndW1lbnRzWzNdO1widW5kZWZpbmVkXCI9PXR5cGVvZiB0aGlzLmJpbmRpbmdzJiYodGhpcy5iaW5kaW5ncz17fSksXCJ1bmRlZmluZWRcIj09dHlwZW9mIHRoaXMuYmluZGluZ3NbdF0mJih0aGlzLmJpbmRpbmdzW3RdPVtdKSx0aGlzLmJpbmRpbmdzW3RdLnB1c2goe2hhbmRsZXI6ZSxjdHg6byxvbmNlOm59KX19LHtrZXk6XCJvbmNlXCIsdmFsdWU6ZnVuY3Rpb24odCxlLG8pe3RoaXMub24odCxlLG8sITApfX0se2tleTpcIm9mZlwiLHZhbHVlOmZ1bmN0aW9uKHQsZSl7aWYoXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHRoaXMuYmluZGluZ3MmJlwidW5kZWZpbmVkXCIhPXR5cGVvZiB0aGlzLmJpbmRpbmdzW3RdKWlmKFwidW5kZWZpbmVkXCI9PXR5cGVvZiBlKWRlbGV0ZSB0aGlzLmJpbmRpbmdzW3RdO2Vsc2UgZm9yKHZhciBvPTA7bzx0aGlzLmJpbmRpbmdzW3RdLmxlbmd0aDspdGhpcy5iaW5kaW5nc1t0XVtvXS5oYW5kbGVyPT09ZT90aGlzLmJpbmRpbmdzW3RdLnNwbGljZShvLDEpOisrb319LHtrZXk6XCJ0cmlnZ2VyXCIsdmFsdWU6ZnVuY3Rpb24odCl7aWYoXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHRoaXMuYmluZGluZ3MmJnRoaXMuYmluZGluZ3NbdF0pe2Zvcih2YXIgZT0wLG89YXJndW1lbnRzLmxlbmd0aCxuPUFycmF5KG8+MT9vLTE6MCksaT0xO28+aTtpKyspbltpLTFdPWFyZ3VtZW50c1tpXTtmb3IoO2U8dGhpcy5iaW5kaW5nc1t0XS5sZW5ndGg7KXt2YXIgcj10aGlzLmJpbmRpbmdzW3RdW2VdLHM9ci5oYW5kbGVyLGE9ci5jdHgsZj1yLm9uY2UsbD1hO1widW5kZWZpbmVkXCI9PXR5cGVvZiBsJiYobD10aGlzKSxzLmFwcGx5KGwsbiksZj90aGlzLmJpbmRpbmdzW3RdLnNwbGljZShlLDEpOisrZX19fX1dKSx0fSgpO3guVXRpbHM9e2dldEFjdHVhbEJvdW5kaW5nQ2xpZW50UmVjdDppLGdldFNjcm9sbFBhcmVudHM6cixnZXRCb3VuZHM6YSxnZXRPZmZzZXRQYXJlbnQ6ZixleHRlbmQ6aCxhZGRDbGFzczpkLHJlbW92ZUNsYXNzOnUsaGFzQ2xhc3M6cCx1cGRhdGVDbGFzc2VzOm0sZGVmZXI6TSxmbHVzaDpfLHVuaXF1ZUlkOlQsRXZlbnRlZDprLGdldFNjcm9sbEJhclNpemU6bCxyZW1vdmVVdGlsRWxlbWVudHM6c307dmFyIEI9ZnVuY3Rpb24oKXtmdW5jdGlvbiB0KHQsZSl7dmFyIG89W10sbj0hMCxpPSExLHI9dm9pZCAwO3RyeXtmb3IodmFyIHMsYT10W1N5bWJvbC5pdGVyYXRvcl0oKTshKG49KHM9YS5uZXh0KCkpLmRvbmUpJiYoby5wdXNoKHMudmFsdWUpLCFlfHxvLmxlbmd0aCE9PWUpO249ITApO31jYXRjaChmKXtpPSEwLHI9Zn1maW5hbGx5e3RyeXshbiYmYVtcInJldHVyblwiXSYmYVtcInJldHVyblwiXSgpfWZpbmFsbHl7aWYoaSl0aHJvdyByfX1yZXR1cm4gb31yZXR1cm4gZnVuY3Rpb24oZSxvKXtpZihBcnJheS5pc0FycmF5KGUpKXJldHVybiBlO2lmKFN5bWJvbC5pdGVyYXRvciBpbiBPYmplY3QoZSkpcmV0dXJuIHQoZSxvKTt0aHJvdyBuZXcgVHlwZUVycm9yKFwiSW52YWxpZCBhdHRlbXB0IHRvIGRlc3RydWN0dXJlIG5vbi1pdGVyYWJsZSBpbnN0YW5jZVwiKX19KCksRT1mdW5jdGlvbigpe2Z1bmN0aW9uIHQodCxlKXtmb3IodmFyIG89MDtvPGUubGVuZ3RoO28rKyl7dmFyIG49ZVtvXTtuLmVudW1lcmFibGU9bi5lbnVtZXJhYmxlfHwhMSxuLmNvbmZpZ3VyYWJsZT0hMCxcInZhbHVlXCJpbiBuJiYobi53cml0YWJsZT0hMCksT2JqZWN0LmRlZmluZVByb3BlcnR5KHQsbi5rZXksbil9fXJldHVybiBmdW5jdGlvbihlLG8sbil7cmV0dXJuIG8mJnQoZS5wcm90b3R5cGUsbyksbiYmdChlLG4pLGV9fSgpLHo9ZnVuY3Rpb24odCxlLG8pe2Zvcih2YXIgbj0hMDtuOyl7dmFyIGk9dCxyPWUscz1vO249ITEsbnVsbD09PWkmJihpPUZ1bmN0aW9uLnByb3RvdHlwZSk7dmFyIGE9T2JqZWN0LmdldE93blByb3BlcnR5RGVzY3JpcHRvcihpLHIpO2lmKHZvaWQgMCE9PWEpe2lmKFwidmFsdWVcImluIGEpcmV0dXJuIGEudmFsdWU7dmFyIGY9YS5nZXQ7aWYodm9pZCAwPT09ZilyZXR1cm47cmV0dXJuIGYuY2FsbChzKX12YXIgbD1PYmplY3QuZ2V0UHJvdG90eXBlT2YoaSk7aWYobnVsbD09PWwpcmV0dXJuO3Q9bCxlPXIsbz1zLG49ITAsYT1sPXZvaWQgMH19O2lmKFwidW5kZWZpbmVkXCI9PXR5cGVvZiB4KXRocm93IG5ldyBFcnJvcihcIllvdSBtdXN0IGluY2x1ZGUgdGhlIHV0aWxzLmpzIGZpbGUgYmVmb3JlIHRldGhlci5qc1wiKTt2YXIgaj14LlV0aWxzLHI9ai5nZXRTY3JvbGxQYXJlbnRzLGE9ai5nZXRCb3VuZHMsZj1qLmdldE9mZnNldFBhcmVudCxoPWouZXh0ZW5kLGQ9ai5hZGRDbGFzcyx1PWoucmVtb3ZlQ2xhc3MsbT1qLnVwZGF0ZUNsYXNzZXMsTT1qLmRlZmVyLF89ai5mbHVzaCxsPWouZ2V0U2Nyb2xsQmFyU2l6ZSxzPWoucmVtb3ZlVXRpbEVsZW1lbnRzLFk9ZnVuY3Rpb24oKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgZG9jdW1lbnQpcmV0dXJuXCJcIjtmb3IodmFyIHQ9ZG9jdW1lbnQuY3JlYXRlRWxlbWVudChcImRpdlwiKSxlPVtcInRyYW5zZm9ybVwiLFwiV2Via2l0VHJhbnNmb3JtXCIsXCJPVHJhbnNmb3JtXCIsXCJNb3pUcmFuc2Zvcm1cIixcIm1zVHJhbnNmb3JtXCJdLG89MDtvPGUubGVuZ3RoOysrbyl7dmFyIG49ZVtvXTtpZih2b2lkIDAhPT10LnN0eWxlW25dKXJldHVybiBufX0oKSxMPVtdLEQ9ZnVuY3Rpb24oKXtMLmZvckVhY2goZnVuY3Rpb24odCl7dC5wb3NpdGlvbighMSl9KSxfKCl9OyFmdW5jdGlvbigpe3ZhciB0PW51bGwsZT1udWxsLG89bnVsbCxuPWZ1bmN0aW9uIGkoKXtyZXR1cm5cInVuZGVmaW5lZFwiIT10eXBlb2YgZSYmZT4xNj8oZT1NYXRoLm1pbihlLTE2LDI1MCksdm9pZChvPXNldFRpbWVvdXQoaSwyNTApKSk6dm9pZChcInVuZGVmaW5lZFwiIT10eXBlb2YgdCYmYigpLXQ8MTB8fChudWxsIT1vJiYoY2xlYXJUaW1lb3V0KG8pLG89bnVsbCksdD1iKCksRCgpLGU9YigpLXQpKX07XCJ1bmRlZmluZWRcIiE9dHlwZW9mIHdpbmRvdyYmXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyJiZbXCJyZXNpemVcIixcInNjcm9sbFwiLFwidG91Y2htb3ZlXCJdLmZvckVhY2goZnVuY3Rpb24odCl7d2luZG93LmFkZEV2ZW50TGlzdGVuZXIodCxuKX0pfSgpO3ZhciBYPXtjZW50ZXI6XCJjZW50ZXJcIixsZWZ0OlwicmlnaHRcIixyaWdodDpcImxlZnRcIn0sRj17bWlkZGxlOlwibWlkZGxlXCIsdG9wOlwiYm90dG9tXCIsYm90dG9tOlwidG9wXCJ9LEg9e3RvcDowLGxlZnQ6MCxtaWRkbGU6XCI1MCVcIixjZW50ZXI6XCI1MCVcIixib3R0b206XCIxMDAlXCIscmlnaHQ6XCIxMDAlXCJ9LE49ZnVuY3Rpb24odCxlKXt2YXIgbz10LmxlZnQsbj10LnRvcDtyZXR1cm5cImF1dG9cIj09PW8mJihvPVhbZS5sZWZ0XSksXCJhdXRvXCI9PT1uJiYobj1GW2UudG9wXSkse2xlZnQ6byx0b3A6bn19LFU9ZnVuY3Rpb24odCl7dmFyIGU9dC5sZWZ0LG89dC50b3A7cmV0dXJuXCJ1bmRlZmluZWRcIiE9dHlwZW9mIEhbdC5sZWZ0XSYmKGU9SFt0LmxlZnRdKSxcInVuZGVmaW5lZFwiIT10eXBlb2YgSFt0LnRvcF0mJihvPUhbdC50b3BdKSx7bGVmdDplLHRvcDpvfX0sVj1mdW5jdGlvbih0KXt2YXIgZT10LnNwbGl0KFwiIFwiKSxvPUIoZSwyKSxuPW9bMF0saT1vWzFdO3JldHVybnt0b3A6bixsZWZ0Oml9fSxSPVYscT1mdW5jdGlvbih0KXtmdW5jdGlvbiBlKHQpe3ZhciBvPXRoaXM7bih0aGlzLGUpLHooT2JqZWN0LmdldFByb3RvdHlwZU9mKGUucHJvdG90eXBlKSxcImNvbnN0cnVjdG9yXCIsdGhpcykuY2FsbCh0aGlzKSx0aGlzLnBvc2l0aW9uPXRoaXMucG9zaXRpb24uYmluZCh0aGlzKSxMLnB1c2godGhpcyksdGhpcy5oaXN0b3J5PVtdLHRoaXMuc2V0T3B0aW9ucyh0LCExKSx4Lm1vZHVsZXMuZm9yRWFjaChmdW5jdGlvbih0KXtcInVuZGVmaW5lZFwiIT10eXBlb2YgdC5pbml0aWFsaXplJiZ0LmluaXRpYWxpemUuY2FsbChvKX0pLHRoaXMucG9zaXRpb24oKX1yZXR1cm4gdihlLHQpLEUoZSxbe2tleTpcImdldENsYXNzXCIsdmFsdWU6ZnVuY3Rpb24oKXt2YXIgdD1hcmd1bWVudHMubGVuZ3RoPD0wfHx2b2lkIDA9PT1hcmd1bWVudHNbMF0/XCJcIjphcmd1bWVudHNbMF0sZT10aGlzLm9wdGlvbnMuY2xhc3NlcztyZXR1cm5cInVuZGVmaW5lZFwiIT10eXBlb2YgZSYmZVt0XT90aGlzLm9wdGlvbnMuY2xhc3Nlc1t0XTp0aGlzLm9wdGlvbnMuY2xhc3NQcmVmaXg/dGhpcy5vcHRpb25zLmNsYXNzUHJlZml4K1wiLVwiK3Q6dH19LHtrZXk6XCJzZXRPcHRpb25zXCIsdmFsdWU6ZnVuY3Rpb24odCl7dmFyIGU9dGhpcyxvPWFyZ3VtZW50cy5sZW5ndGg8PTF8fHZvaWQgMD09PWFyZ3VtZW50c1sxXT8hMDphcmd1bWVudHNbMV0sbj17b2Zmc2V0OlwiMCAwXCIsdGFyZ2V0T2Zmc2V0OlwiMCAwXCIsdGFyZ2V0QXR0YWNobWVudDpcImF1dG8gYXV0b1wiLGNsYXNzUHJlZml4OlwidGV0aGVyXCJ9O3RoaXMub3B0aW9ucz1oKG4sdCk7dmFyIGk9dGhpcy5vcHRpb25zLHM9aS5lbGVtZW50LGE9aS50YXJnZXQsZj1pLnRhcmdldE1vZGlmaWVyO2lmKHRoaXMuZWxlbWVudD1zLHRoaXMudGFyZ2V0PWEsdGhpcy50YXJnZXRNb2RpZmllcj1mLFwidmlld3BvcnRcIj09PXRoaXMudGFyZ2V0Pyh0aGlzLnRhcmdldD1kb2N1bWVudC5ib2R5LHRoaXMudGFyZ2V0TW9kaWZpZXI9XCJ2aXNpYmxlXCIpOlwic2Nyb2xsLWhhbmRsZVwiPT09dGhpcy50YXJnZXQmJih0aGlzLnRhcmdldD1kb2N1bWVudC5ib2R5LHRoaXMudGFyZ2V0TW9kaWZpZXI9XCJzY3JvbGwtaGFuZGxlXCIpLFtcImVsZW1lbnRcIixcInRhcmdldFwiXS5mb3JFYWNoKGZ1bmN0aW9uKHQpe2lmKFwidW5kZWZpbmVkXCI9PXR5cGVvZiBlW3RdKXRocm93IG5ldyBFcnJvcihcIlRldGhlciBFcnJvcjogQm90aCBlbGVtZW50IGFuZCB0YXJnZXQgbXVzdCBiZSBkZWZpbmVkXCIpO1widW5kZWZpbmVkXCIhPXR5cGVvZiBlW3RdLmpxdWVyeT9lW3RdPWVbdF1bMF06XCJzdHJpbmdcIj09dHlwZW9mIGVbdF0mJihlW3RdPWRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZVt0XSkpfSksZCh0aGlzLmVsZW1lbnQsdGhpcy5nZXRDbGFzcyhcImVsZW1lbnRcIikpLHRoaXMub3B0aW9ucy5hZGRUYXJnZXRDbGFzc2VzIT09ITEmJmQodGhpcy50YXJnZXQsdGhpcy5nZXRDbGFzcyhcInRhcmdldFwiKSksIXRoaXMub3B0aW9ucy5hdHRhY2htZW50KXRocm93IG5ldyBFcnJvcihcIlRldGhlciBFcnJvcjogWW91IG11c3QgcHJvdmlkZSBhbiBhdHRhY2htZW50XCIpO3RoaXMudGFyZ2V0QXR0YWNobWVudD1SKHRoaXMub3B0aW9ucy50YXJnZXRBdHRhY2htZW50KSx0aGlzLmF0dGFjaG1lbnQ9Uih0aGlzLm9wdGlvbnMuYXR0YWNobWVudCksdGhpcy5vZmZzZXQ9Vih0aGlzLm9wdGlvbnMub2Zmc2V0KSx0aGlzLnRhcmdldE9mZnNldD1WKHRoaXMub3B0aW9ucy50YXJnZXRPZmZzZXQpLFwidW5kZWZpbmVkXCIhPXR5cGVvZiB0aGlzLnNjcm9sbFBhcmVudHMmJnRoaXMuZGlzYWJsZSgpLFwic2Nyb2xsLWhhbmRsZVwiPT09dGhpcy50YXJnZXRNb2RpZmllcj90aGlzLnNjcm9sbFBhcmVudHM9W3RoaXMudGFyZ2V0XTp0aGlzLnNjcm9sbFBhcmVudHM9cih0aGlzLnRhcmdldCksdGhpcy5vcHRpb25zLmVuYWJsZWQhPT0hMSYmdGhpcy5lbmFibGUobyl9fSx7a2V5OlwiZ2V0VGFyZ2V0Qm91bmRzXCIsdmFsdWU6ZnVuY3Rpb24oKXtpZihcInVuZGVmaW5lZFwiPT10eXBlb2YgdGhpcy50YXJnZXRNb2RpZmllcilyZXR1cm4gYSh0aGlzLnRhcmdldCk7aWYoXCJ2aXNpYmxlXCI9PT10aGlzLnRhcmdldE1vZGlmaWVyKXtpZih0aGlzLnRhcmdldD09PWRvY3VtZW50LmJvZHkpcmV0dXJue3RvcDpwYWdlWU9mZnNldCxsZWZ0OnBhZ2VYT2Zmc2V0LGhlaWdodDppbm5lckhlaWdodCx3aWR0aDppbm5lcldpZHRofTt2YXIgdD1hKHRoaXMudGFyZ2V0KSxlPXtoZWlnaHQ6dC5oZWlnaHQsd2lkdGg6dC53aWR0aCx0b3A6dC50b3AsbGVmdDp0LmxlZnR9O3JldHVybiBlLmhlaWdodD1NYXRoLm1pbihlLmhlaWdodCx0LmhlaWdodC0ocGFnZVlPZmZzZXQtdC50b3ApKSxlLmhlaWdodD1NYXRoLm1pbihlLmhlaWdodCx0LmhlaWdodC0odC50b3ArdC5oZWlnaHQtKHBhZ2VZT2Zmc2V0K2lubmVySGVpZ2h0KSkpLGUuaGVpZ2h0PU1hdGgubWluKGlubmVySGVpZ2h0LGUuaGVpZ2h0KSxlLmhlaWdodC09MixlLndpZHRoPU1hdGgubWluKGUud2lkdGgsdC53aWR0aC0ocGFnZVhPZmZzZXQtdC5sZWZ0KSksZS53aWR0aD1NYXRoLm1pbihlLndpZHRoLHQud2lkdGgtKHQubGVmdCt0LndpZHRoLShwYWdlWE9mZnNldCtpbm5lcldpZHRoKSkpLGUud2lkdGg9TWF0aC5taW4oaW5uZXJXaWR0aCxlLndpZHRoKSxlLndpZHRoLT0yLGUudG9wPHBhZ2VZT2Zmc2V0JiYoZS50b3A9cGFnZVlPZmZzZXQpLGUubGVmdDxwYWdlWE9mZnNldCYmKGUubGVmdD1wYWdlWE9mZnNldCksZX1pZihcInNjcm9sbC1oYW5kbGVcIj09PXRoaXMudGFyZ2V0TW9kaWZpZXIpe3ZhciB0PXZvaWQgMCxvPXRoaXMudGFyZ2V0O289PT1kb2N1bWVudC5ib2R5PyhvPWRvY3VtZW50LmRvY3VtZW50RWxlbWVudCx0PXtsZWZ0OnBhZ2VYT2Zmc2V0LHRvcDpwYWdlWU9mZnNldCxoZWlnaHQ6aW5uZXJIZWlnaHQsd2lkdGg6aW5uZXJXaWR0aH0pOnQ9YShvKTt2YXIgbj1nZXRDb21wdXRlZFN0eWxlKG8pLGk9by5zY3JvbGxXaWR0aD5vLmNsaWVudFdpZHRofHxbbi5vdmVyZmxvdyxuLm92ZXJmbG93WF0uaW5kZXhPZihcInNjcm9sbFwiKT49MHx8dGhpcy50YXJnZXQhPT1kb2N1bWVudC5ib2R5LHI9MDtpJiYocj0xNSk7dmFyIHM9dC5oZWlnaHQtcGFyc2VGbG9hdChuLmJvcmRlclRvcFdpZHRoKS1wYXJzZUZsb2F0KG4uYm9yZGVyQm90dG9tV2lkdGgpLXIsZT17d2lkdGg6MTUsaGVpZ2h0Oi45NzUqcyoocy9vLnNjcm9sbEhlaWdodCksbGVmdDp0LmxlZnQrdC53aWR0aC1wYXJzZUZsb2F0KG4uYm9yZGVyTGVmdFdpZHRoKS0xNX0sZj0wOzQwOD5zJiZ0aGlzLnRhcmdldD09PWRvY3VtZW50LmJvZHkmJihmPS0xMWUtNSpNYXRoLnBvdyhzLDIpLS4wMDcyNypzKzIyLjU4KSx0aGlzLnRhcmdldCE9PWRvY3VtZW50LmJvZHkmJihlLmhlaWdodD1NYXRoLm1heChlLmhlaWdodCwyNCkpO3ZhciBsPXRoaXMudGFyZ2V0LnNjcm9sbFRvcC8oby5zY3JvbGxIZWlnaHQtcyk7cmV0dXJuIGUudG9wPWwqKHMtZS5oZWlnaHQtZikrdC50b3ArcGFyc2VGbG9hdChuLmJvcmRlclRvcFdpZHRoKSx0aGlzLnRhcmdldD09PWRvY3VtZW50LmJvZHkmJihlLmhlaWdodD1NYXRoLm1heChlLmhlaWdodCwyNCkpLGV9fX0se2tleTpcImNsZWFyQ2FjaGVcIix2YWx1ZTpmdW5jdGlvbigpe3RoaXMuX2NhY2hlPXt9fX0se2tleTpcImNhY2hlXCIsdmFsdWU6ZnVuY3Rpb24odCxlKXtyZXR1cm5cInVuZGVmaW5lZFwiPT10eXBlb2YgdGhpcy5fY2FjaGUmJih0aGlzLl9jYWNoZT17fSksXCJ1bmRlZmluZWRcIj09dHlwZW9mIHRoaXMuX2NhY2hlW3RdJiYodGhpcy5fY2FjaGVbdF09ZS5jYWxsKHRoaXMpKSx0aGlzLl9jYWNoZVt0XX19LHtrZXk6XCJlbmFibGVcIix2YWx1ZTpmdW5jdGlvbigpe3ZhciB0PXRoaXMsZT1hcmd1bWVudHMubGVuZ3RoPD0wfHx2b2lkIDA9PT1hcmd1bWVudHNbMF0/ITA6YXJndW1lbnRzWzBdO3RoaXMub3B0aW9ucy5hZGRUYXJnZXRDbGFzc2VzIT09ITEmJmQodGhpcy50YXJnZXQsdGhpcy5nZXRDbGFzcyhcImVuYWJsZWRcIikpLGQodGhpcy5lbGVtZW50LHRoaXMuZ2V0Q2xhc3MoXCJlbmFibGVkXCIpKSx0aGlzLmVuYWJsZWQ9ITAsdGhpcy5zY3JvbGxQYXJlbnRzLmZvckVhY2goZnVuY3Rpb24oZSl7ZSE9PXQudGFyZ2V0Lm93bmVyRG9jdW1lbnQmJmUuYWRkRXZlbnRMaXN0ZW5lcihcInNjcm9sbFwiLHQucG9zaXRpb24pfSksZSYmdGhpcy5wb3NpdGlvbigpfX0se2tleTpcImRpc2FibGVcIix2YWx1ZTpmdW5jdGlvbigpe3ZhciB0PXRoaXM7dSh0aGlzLnRhcmdldCx0aGlzLmdldENsYXNzKFwiZW5hYmxlZFwiKSksdSh0aGlzLmVsZW1lbnQsdGhpcy5nZXRDbGFzcyhcImVuYWJsZWRcIikpLHRoaXMuZW5hYmxlZD0hMSxcInVuZGVmaW5lZFwiIT10eXBlb2YgdGhpcy5zY3JvbGxQYXJlbnRzJiZ0aGlzLnNjcm9sbFBhcmVudHMuZm9yRWFjaChmdW5jdGlvbihlKXtlLnJlbW92ZUV2ZW50TGlzdGVuZXIoXCJzY3JvbGxcIix0LnBvc2l0aW9uKX0pfX0se2tleTpcImRlc3Ryb3lcIix2YWx1ZTpmdW5jdGlvbigpe3ZhciB0PXRoaXM7dGhpcy5kaXNhYmxlKCksTC5mb3JFYWNoKGZ1bmN0aW9uKGUsbyl7ZT09PXQmJkwuc3BsaWNlKG8sMSl9KSwwPT09TC5sZW5ndGgmJnMoKX19LHtrZXk6XCJ1cGRhdGVBdHRhY2hDbGFzc2VzXCIsdmFsdWU6ZnVuY3Rpb24odCxlKXt2YXIgbz10aGlzO3Q9dHx8dGhpcy5hdHRhY2htZW50LGU9ZXx8dGhpcy50YXJnZXRBdHRhY2htZW50O3ZhciBuPVtcImxlZnRcIixcInRvcFwiLFwiYm90dG9tXCIsXCJyaWdodFwiLFwibWlkZGxlXCIsXCJjZW50ZXJcIl07XCJ1bmRlZmluZWRcIiE9dHlwZW9mIHRoaXMuX2FkZEF0dGFjaENsYXNzZXMmJnRoaXMuX2FkZEF0dGFjaENsYXNzZXMubGVuZ3RoJiZ0aGlzLl9hZGRBdHRhY2hDbGFzc2VzLnNwbGljZSgwLHRoaXMuX2FkZEF0dGFjaENsYXNzZXMubGVuZ3RoKSxcInVuZGVmaW5lZFwiPT10eXBlb2YgdGhpcy5fYWRkQXR0YWNoQ2xhc3NlcyYmKHRoaXMuX2FkZEF0dGFjaENsYXNzZXM9W10pO3ZhciBpPXRoaXMuX2FkZEF0dGFjaENsYXNzZXM7dC50b3AmJmkucHVzaCh0aGlzLmdldENsYXNzKFwiZWxlbWVudC1hdHRhY2hlZFwiKStcIi1cIit0LnRvcCksdC5sZWZ0JiZpLnB1c2godGhpcy5nZXRDbGFzcyhcImVsZW1lbnQtYXR0YWNoZWRcIikrXCItXCIrdC5sZWZ0KSxlLnRvcCYmaS5wdXNoKHRoaXMuZ2V0Q2xhc3MoXCJ0YXJnZXQtYXR0YWNoZWRcIikrXCItXCIrZS50b3ApLGUubGVmdCYmaS5wdXNoKHRoaXMuZ2V0Q2xhc3MoXCJ0YXJnZXQtYXR0YWNoZWRcIikrXCItXCIrZS5sZWZ0KTt2YXIgcj1bXTtuLmZvckVhY2goZnVuY3Rpb24odCl7ci5wdXNoKG8uZ2V0Q2xhc3MoXCJlbGVtZW50LWF0dGFjaGVkXCIpK1wiLVwiK3QpLHIucHVzaChvLmdldENsYXNzKFwidGFyZ2V0LWF0dGFjaGVkXCIpK1wiLVwiK3QpfSksTShmdW5jdGlvbigpe1widW5kZWZpbmVkXCIhPXR5cGVvZiBvLl9hZGRBdHRhY2hDbGFzc2VzJiYobShvLmVsZW1lbnQsby5fYWRkQXR0YWNoQ2xhc3NlcyxyKSxvLm9wdGlvbnMuYWRkVGFyZ2V0Q2xhc3NlcyE9PSExJiZtKG8udGFyZ2V0LG8uX2FkZEF0dGFjaENsYXNzZXMsciksZGVsZXRlIG8uX2FkZEF0dGFjaENsYXNzZXMpfSl9fSx7a2V5OlwicG9zaXRpb25cIix2YWx1ZTpmdW5jdGlvbigpe3ZhciB0PXRoaXMsZT1hcmd1bWVudHMubGVuZ3RoPD0wfHx2b2lkIDA9PT1hcmd1bWVudHNbMF0/ITA6YXJndW1lbnRzWzBdO2lmKHRoaXMuZW5hYmxlZCl7dGhpcy5jbGVhckNhY2hlKCk7dmFyIG89Tih0aGlzLnRhcmdldEF0dGFjaG1lbnQsdGhpcy5hdHRhY2htZW50KTt0aGlzLnVwZGF0ZUF0dGFjaENsYXNzZXModGhpcy5hdHRhY2htZW50LG8pO3ZhciBuPXRoaXMuY2FjaGUoXCJlbGVtZW50LWJvdW5kc1wiLGZ1bmN0aW9uKCl7cmV0dXJuIGEodC5lbGVtZW50KX0pLGk9bi53aWR0aCxyPW4uaGVpZ2h0O2lmKDA9PT1pJiYwPT09ciYmXCJ1bmRlZmluZWRcIiE9dHlwZW9mIHRoaXMubGFzdFNpemUpe3ZhciBzPXRoaXMubGFzdFNpemU7aT1zLndpZHRoLHI9cy5oZWlnaHR9ZWxzZSB0aGlzLmxhc3RTaXplPXt3aWR0aDppLGhlaWdodDpyfTt2YXIgaD10aGlzLmNhY2hlKFwidGFyZ2V0LWJvdW5kc1wiLGZ1bmN0aW9uKCl7cmV0dXJuIHQuZ2V0VGFyZ2V0Qm91bmRzKCl9KSx1PWgsZD1DKFUodGhpcy5hdHRhY2htZW50KSx7d2lkdGg6aSxoZWlnaHQ6cn0pLHA9QyhVKG8pLHUpLGM9Qyh0aGlzLm9mZnNldCx7d2lkdGg6aSxoZWlnaHQ6cn0pLGc9Qyh0aGlzLnRhcmdldE9mZnNldCx1KTtkPXcoZCxjKSxwPXcocCxnKTtmb3IodmFyIG09aC5sZWZ0K3AubGVmdC1kLmxlZnQsdj1oLnRvcCtwLnRvcC1kLnRvcCx5PTA7eTx4Lm1vZHVsZXMubGVuZ3RoOysreSl7dmFyIGI9eC5tb2R1bGVzW3ldLE89Yi5wb3NpdGlvbi5jYWxsKHRoaXMse2xlZnQ6bSx0b3A6dix0YXJnZXRBdHRhY2htZW50Om8sdGFyZ2V0UG9zOmgsZWxlbWVudFBvczpuLG9mZnNldDpkLHRhcmdldE9mZnNldDpwLG1hbnVhbE9mZnNldDpjLG1hbnVhbFRhcmdldE9mZnNldDpnLHNjcm9sbGJhclNpemU6UyxhdHRhY2htZW50OnRoaXMuYXR0YWNobWVudH0pO2lmKE89PT0hMSlyZXR1cm4hMTtcInVuZGVmaW5lZFwiIT10eXBlb2YgTyYmXCJvYmplY3RcIj09dHlwZW9mIE8mJih2PU8udG9wLG09Ty5sZWZ0KX12YXIgRT17cGFnZTp7dG9wOnYsbGVmdDptfSx2aWV3cG9ydDp7dG9wOnYtcGFnZVlPZmZzZXQsYm90dG9tOnBhZ2VZT2Zmc2V0LXYtcitpbm5lckhlaWdodCxsZWZ0Om0tcGFnZVhPZmZzZXQscmlnaHQ6cGFnZVhPZmZzZXQtbS1pK2lubmVyV2lkdGh9fSxBPXRoaXMudGFyZ2V0Lm93bmVyRG9jdW1lbnQsVD1BLmRlZmF1bHRWaWV3LFM9dm9pZCAwO3JldHVybiBBLmJvZHkuc2Nyb2xsV2lkdGg+VC5pbm5lcldpZHRoJiYoUz10aGlzLmNhY2hlKFwic2Nyb2xsYmFyLXNpemVcIixsKSxFLnZpZXdwb3J0LmJvdHRvbS09Uy5oZWlnaHQpLEEuYm9keS5zY3JvbGxIZWlnaHQ+VC5pbm5lckhlaWdodCYmKFM9dGhpcy5jYWNoZShcInNjcm9sbGJhci1zaXplXCIsbCksRS52aWV3cG9ydC5yaWdodC09Uy53aWR0aCksKC0xPT09W1wiXCIsXCJzdGF0aWNcIl0uaW5kZXhPZihBLmJvZHkuc3R5bGUucG9zaXRpb24pfHwtMT09PVtcIlwiLFwic3RhdGljXCJdLmluZGV4T2YoQS5ib2R5LnBhcmVudEVsZW1lbnQuc3R5bGUucG9zaXRpb24pKSYmKEUucGFnZS5ib3R0b209QS5ib2R5LnNjcm9sbEhlaWdodC12LXIsRS5wYWdlLnJpZ2h0PUEuYm9keS5zY3JvbGxXaWR0aC1tLWkpLFwidW5kZWZpbmVkXCIhPXR5cGVvZiB0aGlzLm9wdGlvbnMub3B0aW1pemF0aW9ucyYmdGhpcy5vcHRpb25zLm9wdGltaXphdGlvbnMubW92ZUVsZW1lbnQhPT0hMSYmXCJ1bmRlZmluZWRcIj09dHlwZW9mIHRoaXMudGFyZ2V0TW9kaWZpZXImJiFmdW5jdGlvbigpe3ZhciBlPXQuY2FjaGUoXCJ0YXJnZXQtb2Zmc2V0cGFyZW50XCIsZnVuY3Rpb24oKXtyZXR1cm4gZih0LnRhcmdldCl9KSxvPXQuY2FjaGUoXCJ0YXJnZXQtb2Zmc2V0cGFyZW50LWJvdW5kc1wiLGZ1bmN0aW9uKCl7cmV0dXJuIGEoZSl9KSxuPWdldENvbXB1dGVkU3R5bGUoZSksaT1vLHI9e307aWYoW1wiVG9wXCIsXCJMZWZ0XCIsXCJCb3R0b21cIixcIlJpZ2h0XCJdLmZvckVhY2goZnVuY3Rpb24odCl7clt0LnRvTG93ZXJDYXNlKCldPXBhcnNlRmxvYXQobltcImJvcmRlclwiK3QrXCJXaWR0aFwiXSl9KSxvLnJpZ2h0PUEuYm9keS5zY3JvbGxXaWR0aC1vLmxlZnQtaS53aWR0aCtyLnJpZ2h0LG8uYm90dG9tPUEuYm9keS5zY3JvbGxIZWlnaHQtby50b3AtaS5oZWlnaHQrci5ib3R0b20sRS5wYWdlLnRvcD49by50b3Arci50b3AmJkUucGFnZS5ib3R0b20+PW8uYm90dG9tJiZFLnBhZ2UubGVmdD49by5sZWZ0K3IubGVmdCYmRS5wYWdlLnJpZ2h0Pj1vLnJpZ2h0KXt2YXIgcz1lLnNjcm9sbFRvcCxsPWUuc2Nyb2xsTGVmdDtFLm9mZnNldD17dG9wOkUucGFnZS50b3Atby50b3Arcy1yLnRvcCxsZWZ0OkUucGFnZS5sZWZ0LW8ubGVmdCtsLXIubGVmdH19fSgpLHRoaXMubW92ZShFKSx0aGlzLmhpc3RvcnkudW5zaGlmdChFKSx0aGlzLmhpc3RvcnkubGVuZ3RoPjMmJnRoaXMuaGlzdG9yeS5wb3AoKSxlJiZfKCksITB9fX0se2tleTpcIm1vdmVcIix2YWx1ZTpmdW5jdGlvbih0KXt2YXIgZT10aGlzO2lmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiB0aGlzLmVsZW1lbnQucGFyZW50Tm9kZSl7dmFyIG89e307Zm9yKHZhciBuIGluIHQpe29bbl09e307Zm9yKHZhciBpIGluIHRbbl0pe2Zvcih2YXIgcj0hMSxzPTA7czx0aGlzLmhpc3RvcnkubGVuZ3RoOysrcyl7dmFyIGE9dGhpcy5oaXN0b3J5W3NdO2lmKFwidW5kZWZpbmVkXCIhPXR5cGVvZiBhW25dJiYheShhW25dW2ldLHRbbl1baV0pKXtyPSEwO2JyZWFrfX1yfHwob1tuXVtpXT0hMCl9fXZhciBsPXt0b3A6XCJcIixsZWZ0OlwiXCIscmlnaHQ6XCJcIixib3R0b206XCJcIn0sdT1mdW5jdGlvbih0LG8pe3ZhciBuPVwidW5kZWZpbmVkXCIhPXR5cGVvZiBlLm9wdGlvbnMub3B0aW1pemF0aW9ucyxpPW4/ZS5vcHRpb25zLm9wdGltaXphdGlvbnMuZ3B1Om51bGw7aWYoaSE9PSExKXt2YXIgcj12b2lkIDAscz12b2lkIDA7dC50b3A/KGwudG9wPTAscj1vLnRvcCk6KGwuYm90dG9tPTAscj0tby5ib3R0b20pLHQubGVmdD8obC5sZWZ0PTAscz1vLmxlZnQpOihsLnJpZ2h0PTAscz0tby5yaWdodCksbFtZXT1cInRyYW5zbGF0ZVgoXCIrTWF0aC5yb3VuZChzKStcInB4KSB0cmFuc2xhdGVZKFwiK01hdGgucm91bmQocikrXCJweClcIixcIm1zVHJhbnNmb3JtXCIhPT1ZJiYobFtZXSs9XCIgdHJhbnNsYXRlWigwKVwiKX1lbHNlIHQudG9wP2wudG9wPW8udG9wK1wicHhcIjpsLmJvdHRvbT1vLmJvdHRvbStcInB4XCIsdC5sZWZ0P2wubGVmdD1vLmxlZnQrXCJweFwiOmwucmlnaHQ9by5yaWdodCtcInB4XCJ9LGQ9ITE7aWYoKG8ucGFnZS50b3B8fG8ucGFnZS5ib3R0b20pJiYoby5wYWdlLmxlZnR8fG8ucGFnZS5yaWdodCk/KGwucG9zaXRpb249XCJhYnNvbHV0ZVwiLHUoby5wYWdlLHQucGFnZSkpOihvLnZpZXdwb3J0LnRvcHx8by52aWV3cG9ydC5ib3R0b20pJiYoby52aWV3cG9ydC5sZWZ0fHxvLnZpZXdwb3J0LnJpZ2h0KT8obC5wb3NpdGlvbj1cImZpeGVkXCIsdShvLnZpZXdwb3J0LHQudmlld3BvcnQpKTpcInVuZGVmaW5lZFwiIT10eXBlb2Ygby5vZmZzZXQmJm8ub2Zmc2V0LnRvcCYmby5vZmZzZXQubGVmdD8hZnVuY3Rpb24oKXtsLnBvc2l0aW9uPVwiYWJzb2x1dGVcIjt2YXIgbj1lLmNhY2hlKFwidGFyZ2V0LW9mZnNldHBhcmVudFwiLGZ1bmN0aW9uKCl7cmV0dXJuIGYoZS50YXJnZXQpfSk7ZihlLmVsZW1lbnQpIT09biYmTShmdW5jdGlvbigpe2UuZWxlbWVudC5wYXJlbnROb2RlLnJlbW92ZUNoaWxkKGUuZWxlbWVudCksbi5hcHBlbmRDaGlsZChlLmVsZW1lbnQpfSksdShvLm9mZnNldCx0Lm9mZnNldCksZD0hMH0oKToobC5wb3NpdGlvbj1cImFic29sdXRlXCIsdSh7dG9wOiEwLGxlZnQ6ITB9LHQucGFnZSkpLCFkKXtmb3IodmFyIHA9ITAsYz10aGlzLmVsZW1lbnQucGFyZW50Tm9kZTtjJiYxPT09Yy5ub2RlVHlwZSYmXCJCT0RZXCIhPT1jLnRhZ05hbWU7KXtpZihcInN0YXRpY1wiIT09Z2V0Q29tcHV0ZWRTdHlsZShjKS5wb3NpdGlvbil7cD0hMTticmVha31jPWMucGFyZW50Tm9kZX1wfHwodGhpcy5lbGVtZW50LnBhcmVudE5vZGUucmVtb3ZlQ2hpbGQodGhpcy5lbGVtZW50KSx0aGlzLmVsZW1lbnQub3duZXJEb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKHRoaXMuZWxlbWVudCkpfXZhciBnPXt9LG09ITE7Zm9yKHZhciBpIGluIGwpe3ZhciB2PWxbaV0sYj10aGlzLmVsZW1lbnQuc3R5bGVbaV07YiE9PXYmJihtPSEwLGdbaV09dil9bSYmTShmdW5jdGlvbigpe2goZS5lbGVtZW50LnN0eWxlLGcpfSl9fX1dKSxlfShrKTtxLm1vZHVsZXM9W10seC5wb3NpdGlvbj1EO3ZhciBJPWgocSx4KSxCPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gdCh0LGUpe3ZhciBvPVtdLG49ITAsaT0hMSxyPXZvaWQgMDt0cnl7Zm9yKHZhciBzLGE9dFtTeW1ib2wuaXRlcmF0b3JdKCk7IShuPShzPWEubmV4dCgpKS5kb25lKSYmKG8ucHVzaChzLnZhbHVlKSwhZXx8by5sZW5ndGghPT1lKTtuPSEwKTt9Y2F0Y2goZil7aT0hMCxyPWZ9ZmluYWxseXt0cnl7IW4mJmFbXCJyZXR1cm5cIl0mJmFbXCJyZXR1cm5cIl0oKX1maW5hbGx5e2lmKGkpdGhyb3cgcn19cmV0dXJuIG99cmV0dXJuIGZ1bmN0aW9uKGUsbyl7aWYoQXJyYXkuaXNBcnJheShlKSlyZXR1cm4gZTtpZihTeW1ib2wuaXRlcmF0b3IgaW4gT2JqZWN0KGUpKXJldHVybiB0KGUsbyk7dGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBkZXN0cnVjdHVyZSBub24taXRlcmFibGUgaW5zdGFuY2VcIil9fSgpLGo9eC5VdGlscyxhPWouZ2V0Qm91bmRzLGg9ai5leHRlbmQsbT1qLnVwZGF0ZUNsYXNzZXMsTT1qLmRlZmVyLCQ9W1wibGVmdFwiLFwidG9wXCIsXCJyaWdodFwiLFwiYm90dG9tXCJdO3gubW9kdWxlcy5wdXNoKHtwb3NpdGlvbjpmdW5jdGlvbih0KXt2YXIgZT10aGlzLG89dC50b3Asbj10LmxlZnQsaT10LnRhcmdldEF0dGFjaG1lbnQ7aWYoIXRoaXMub3B0aW9ucy5jb25zdHJhaW50cylyZXR1cm4hMDt2YXIgcj10aGlzLmNhY2hlKFwiZWxlbWVudC1ib3VuZHNcIixmdW5jdGlvbigpe3JldHVybiBhKGUuZWxlbWVudCl9KSxzPXIuaGVpZ2h0LGY9ci53aWR0aDtpZigwPT09ZiYmMD09PXMmJlwidW5kZWZpbmVkXCIhPXR5cGVvZiB0aGlzLmxhc3RTaXplKXt2YXIgbD10aGlzLmxhc3RTaXplO2Y9bC53aWR0aCxzPWwuaGVpZ2h0fXZhciB1PXRoaXMuY2FjaGUoXCJ0YXJnZXQtYm91bmRzXCIsZnVuY3Rpb24oKXtyZXR1cm4gZS5nZXRUYXJnZXRCb3VuZHMoKX0pLGQ9dS5oZWlnaHQscD11LndpZHRoLGM9W3RoaXMuZ2V0Q2xhc3MoXCJwaW5uZWRcIiksdGhpcy5nZXRDbGFzcyhcIm91dC1vZi1ib3VuZHNcIildO3RoaXMub3B0aW9ucy5jb25zdHJhaW50cy5mb3JFYWNoKGZ1bmN0aW9uKHQpe3ZhciBlPXQub3V0T2ZCb3VuZHNDbGFzcyxvPXQucGlubmVkQ2xhc3M7ZSYmYy5wdXNoKGUpLG8mJmMucHVzaChvKX0pLGMuZm9yRWFjaChmdW5jdGlvbih0KXtbXCJsZWZ0XCIsXCJ0b3BcIixcInJpZ2h0XCIsXCJib3R0b21cIl0uZm9yRWFjaChmdW5jdGlvbihlKXtjLnB1c2godCtcIi1cIitlKX0pfSk7dmFyIGc9W10sdj1oKHt9LGkpLHk9aCh7fSx0aGlzLmF0dGFjaG1lbnQpO3JldHVybiB0aGlzLm9wdGlvbnMuY29uc3RyYWludHMuZm9yRWFjaChmdW5jdGlvbih0KXt2YXIgcj10LnRvLGE9dC5hdHRhY2htZW50LGw9dC5waW47XCJ1bmRlZmluZWRcIj09dHlwZW9mIGEmJihhPVwiXCIpO3ZhciBoPXZvaWQgMCx1PXZvaWQgMDtpZihhLmluZGV4T2YoXCIgXCIpPj0wKXt2YXIgYz1hLnNwbGl0KFwiIFwiKSxtPUIoYywyKTt1PW1bMF0saD1tWzFdfWVsc2UgaD11PWE7dmFyIGI9TyhlLHIpOyhcInRhcmdldFwiPT09dXx8XCJib3RoXCI9PT11KSYmKG88YlsxXSYmXCJ0b3BcIj09PXYudG9wJiYobys9ZCx2LnRvcD1cImJvdHRvbVwiKSxvK3M+YlszXSYmXCJib3R0b21cIj09PXYudG9wJiYoby09ZCx2LnRvcD1cInRvcFwiKSksXCJ0b2dldGhlclwiPT09dSYmKFwidG9wXCI9PT12LnRvcCYmKFwiYm90dG9tXCI9PT15LnRvcCYmbzxiWzFdPyhvKz1kLHYudG9wPVwiYm90dG9tXCIsbys9cyx5LnRvcD1cInRvcFwiKTpcInRvcFwiPT09eS50b3AmJm8rcz5iWzNdJiZvLShzLWQpPj1iWzFdJiYoby09cy1kLHYudG9wPVwiYm90dG9tXCIseS50b3A9XCJib3R0b21cIikpLFwiYm90dG9tXCI9PT12LnRvcCYmKFwidG9wXCI9PT15LnRvcCYmbytzPmJbM10/KG8tPWQsdi50b3A9XCJ0b3BcIixvLT1zLHkudG9wPVwiYm90dG9tXCIpOlwiYm90dG9tXCI9PT15LnRvcCYmbzxiWzFdJiZvKygyKnMtZCk8PWJbM10mJihvKz1zLWQsdi50b3A9XCJ0b3BcIix5LnRvcD1cInRvcFwiKSksXCJtaWRkbGVcIj09PXYudG9wJiYobytzPmJbM10mJlwidG9wXCI9PT15LnRvcD8oby09cyx5LnRvcD1cImJvdHRvbVwiKTpvPGJbMV0mJlwiYm90dG9tXCI9PT15LnRvcCYmKG8rPXMseS50b3A9XCJ0b3BcIikpKSwoXCJ0YXJnZXRcIj09PWh8fFwiYm90aFwiPT09aCkmJihuPGJbMF0mJlwibGVmdFwiPT09di5sZWZ0JiYobis9cCx2LmxlZnQ9XCJyaWdodFwiKSxuK2Y+YlsyXSYmXCJyaWdodFwiPT09di5sZWZ0JiYobi09cCx2LmxlZnQ9XCJsZWZ0XCIpKSxcInRvZ2V0aGVyXCI9PT1oJiYobjxiWzBdJiZcImxlZnRcIj09PXYubGVmdD9cInJpZ2h0XCI9PT15LmxlZnQ/KG4rPXAsdi5sZWZ0PVwicmlnaHRcIixuKz1mLHkubGVmdD1cImxlZnRcIik6XCJsZWZ0XCI9PT15LmxlZnQmJihuKz1wLHYubGVmdD1cInJpZ2h0XCIsbi09Zix5LmxlZnQ9XCJyaWdodFwiKTpuK2Y+YlsyXSYmXCJyaWdodFwiPT09di5sZWZ0P1wibGVmdFwiPT09eS5sZWZ0PyhuLT1wLHYubGVmdD1cImxlZnRcIixuLT1mLHkubGVmdD1cInJpZ2h0XCIpOlwicmlnaHRcIj09PXkubGVmdCYmKG4tPXAsdi5sZWZ0PVwibGVmdFwiLG4rPWYseS5sZWZ0PVwibGVmdFwiKTpcImNlbnRlclwiPT09di5sZWZ0JiYobitmPmJbMl0mJlwibGVmdFwiPT09eS5sZWZ0PyhuLT1mLHkubGVmdD1cInJpZ2h0XCIpOm48YlswXSYmXCJyaWdodFwiPT09eS5sZWZ0JiYobis9Zix5LmxlZnQ9XCJsZWZ0XCIpKSksKFwiZWxlbWVudFwiPT09dXx8XCJib3RoXCI9PT11KSYmKG88YlsxXSYmXCJib3R0b21cIj09PXkudG9wJiYobys9cyx5LnRvcD1cInRvcFwiKSxvK3M+YlszXSYmXCJ0b3BcIj09PXkudG9wJiYoby09cyx5LnRvcD1cImJvdHRvbVwiKSksKFwiZWxlbWVudFwiPT09aHx8XCJib3RoXCI9PT1oKSYmKG48YlswXSYmKFwicmlnaHRcIj09PXkubGVmdD8obis9Zix5LmxlZnQ9XCJsZWZ0XCIpOlwiY2VudGVyXCI9PT15LmxlZnQmJihuKz1mLzIseS5sZWZ0PVwibGVmdFwiKSksbitmPmJbMl0mJihcImxlZnRcIj09PXkubGVmdD8obi09Zix5LmxlZnQ9XCJyaWdodFwiKTpcImNlbnRlclwiPT09eS5sZWZ0JiYobi09Zi8yLHkubGVmdD1cInJpZ2h0XCIpKSksXCJzdHJpbmdcIj09dHlwZW9mIGw/bD1sLnNwbGl0KFwiLFwiKS5tYXAoZnVuY3Rpb24odCl7cmV0dXJuIHQudHJpbSgpfSk6bD09PSEwJiYobD1bXCJ0b3BcIixcImxlZnRcIixcInJpZ2h0XCIsXCJib3R0b21cIl0pLGw9bHx8W107dmFyIHc9W10sQz1bXTtvPGJbMV0mJihsLmluZGV4T2YoXCJ0b3BcIik+PTA/KG89YlsxXSx3LnB1c2goXCJ0b3BcIikpOkMucHVzaChcInRvcFwiKSksbytzPmJbM10mJihsLmluZGV4T2YoXCJib3R0b21cIik+PTA/KG89YlszXS1zLHcucHVzaChcImJvdHRvbVwiKSk6Qy5wdXNoKFwiYm90dG9tXCIpKSxuPGJbMF0mJihsLmluZGV4T2YoXCJsZWZ0XCIpPj0wPyhuPWJbMF0sdy5wdXNoKFwibGVmdFwiKSk6Qy5wdXNoKFwibGVmdFwiKSksbitmPmJbMl0mJihsLmluZGV4T2YoXCJyaWdodFwiKT49MD8obj1iWzJdLWYsdy5wdXNoKFwicmlnaHRcIikpOkMucHVzaChcInJpZ2h0XCIpKSx3Lmxlbmd0aCYmIWZ1bmN0aW9uKCl7dmFyIHQ9dm9pZCAwO3Q9XCJ1bmRlZmluZWRcIiE9dHlwZW9mIGUub3B0aW9ucy5waW5uZWRDbGFzcz9lLm9wdGlvbnMucGlubmVkQ2xhc3M6ZS5nZXRDbGFzcyhcInBpbm5lZFwiKSxnLnB1c2godCksdy5mb3JFYWNoKGZ1bmN0aW9uKGUpe2cucHVzaCh0K1wiLVwiK2UpfSl9KCksQy5sZW5ndGgmJiFmdW5jdGlvbigpe3ZhciB0PXZvaWQgMDt0PVwidW5kZWZpbmVkXCIhPXR5cGVvZiBlLm9wdGlvbnMub3V0T2ZCb3VuZHNDbGFzcz9lLm9wdGlvbnMub3V0T2ZCb3VuZHNDbGFzczplLmdldENsYXNzKFwib3V0LW9mLWJvdW5kc1wiKSxnLnB1c2godCksQy5mb3JFYWNoKGZ1bmN0aW9uKGUpe2cucHVzaCh0K1wiLVwiK2UpfSl9KCksKHcuaW5kZXhPZihcImxlZnRcIik+PTB8fHcuaW5kZXhPZihcInJpZ2h0XCIpPj0wKSYmKHkubGVmdD12LmxlZnQ9ITEpLCh3LmluZGV4T2YoXCJ0b3BcIik+PTB8fHcuaW5kZXhPZihcImJvdHRvbVwiKT49MCkmJih5LnRvcD12LnRvcD0hMSksKHYudG9wIT09aS50b3B8fHYubGVmdCE9PWkubGVmdHx8eS50b3AhPT1lLmF0dGFjaG1lbnQudG9wfHx5LmxlZnQhPT1lLmF0dGFjaG1lbnQubGVmdCkmJihlLnVwZGF0ZUF0dGFjaENsYXNzZXMoeSx2KSxlLnRyaWdnZXIoXCJ1cGRhdGVcIix7YXR0YWNobWVudDp5LHRhcmdldEF0dGFjaG1lbnQ6dn0pKX0pLE0oZnVuY3Rpb24oKXtlLm9wdGlvbnMuYWRkVGFyZ2V0Q2xhc3NlcyE9PSExJiZtKGUudGFyZ2V0LGcsYyksbShlLmVsZW1lbnQsZyxjKX0pLHt0b3A6byxsZWZ0Om59fX0pO3ZhciBqPXguVXRpbHMsYT1qLmdldEJvdW5kcyxtPWoudXBkYXRlQ2xhc3NlcyxNPWouZGVmZXI7eC5tb2R1bGVzLnB1c2goe3Bvc2l0aW9uOmZ1bmN0aW9uKHQpe3ZhciBlPXRoaXMsbz10LnRvcCxuPXQubGVmdCxpPXRoaXMuY2FjaGUoXCJlbGVtZW50LWJvdW5kc1wiLGZ1bmN0aW9uKCl7cmV0dXJuIGEoZS5lbGVtZW50KX0pLHI9aS5oZWlnaHQscz1pLndpZHRoLGY9dGhpcy5nZXRUYXJnZXRCb3VuZHMoKSxsPW8rcixoPW4rcyx1PVtdO288PWYuYm90dG9tJiZsPj1mLnRvcCYmW1wibGVmdFwiLFwicmlnaHRcIl0uZm9yRWFjaChmdW5jdGlvbih0KXt2YXIgZT1mW3RdOyhlPT09bnx8ZT09PWgpJiZ1LnB1c2godCl9KSxuPD1mLnJpZ2h0JiZoPj1mLmxlZnQmJltcInRvcFwiLFwiYm90dG9tXCJdLmZvckVhY2goZnVuY3Rpb24odCl7dmFyIGU9Zlt0XTsoZT09PW98fGU9PT1sKSYmdS5wdXNoKHQpfSk7dmFyIGQ9W10scD1bXSxjPVtcImxlZnRcIixcInRvcFwiLFwicmlnaHRcIixcImJvdHRvbVwiXTtyZXR1cm4gZC5wdXNoKHRoaXMuZ2V0Q2xhc3MoXCJhYnV0dGVkXCIpKSxjLmZvckVhY2goZnVuY3Rpb24odCl7ZC5wdXNoKGUuZ2V0Q2xhc3MoXCJhYnV0dGVkXCIpK1wiLVwiK3QpfSksdS5sZW5ndGgmJnAucHVzaCh0aGlzLmdldENsYXNzKFwiYWJ1dHRlZFwiKSksdS5mb3JFYWNoKGZ1bmN0aW9uKHQpe3AucHVzaChlLmdldENsYXNzKFwiYWJ1dHRlZFwiKStcIi1cIit0KX0pLE0oZnVuY3Rpb24oKXtlLm9wdGlvbnMuYWRkVGFyZ2V0Q2xhc3NlcyE9PSExJiZtKGUudGFyZ2V0LHAsZCksbShlLmVsZW1lbnQscCxkKX0pLCEwfX0pO3ZhciBCPWZ1bmN0aW9uKCl7ZnVuY3Rpb24gdCh0LGUpe3ZhciBvPVtdLG49ITAsaT0hMSxyPXZvaWQgMDt0cnl7Zm9yKHZhciBzLGE9dFtTeW1ib2wuaXRlcmF0b3JdKCk7IShuPShzPWEubmV4dCgpKS5kb25lKSYmKG8ucHVzaChzLnZhbHVlKSwhZXx8by5sZW5ndGghPT1lKTtuPSEwKTt9Y2F0Y2goZil7aT0hMCxyPWZ9ZmluYWxseXt0cnl7IW4mJmFbXCJyZXR1cm5cIl0mJmFbXCJyZXR1cm5cIl0oKX1maW5hbGx5e2lmKGkpdGhyb3cgcn19cmV0dXJuIG99cmV0dXJuIGZ1bmN0aW9uKGUsbyl7aWYoQXJyYXkuaXNBcnJheShlKSlyZXR1cm4gZTtpZihTeW1ib2wuaXRlcmF0b3IgaW4gT2JqZWN0KGUpKXJldHVybiB0KGUsbyk7dGhyb3cgbmV3IFR5cGVFcnJvcihcIkludmFsaWQgYXR0ZW1wdCB0byBkZXN0cnVjdHVyZSBub24taXRlcmFibGUgaW5zdGFuY2VcIil9fSgpO3JldHVybiB4Lm1vZHVsZXMucHVzaCh7cG9zaXRpb246ZnVuY3Rpb24odCl7dmFyIGU9dC50b3Asbz10LmxlZnQ7aWYodGhpcy5vcHRpb25zLnNoaWZ0KXt2YXIgbj10aGlzLm9wdGlvbnMuc2hpZnQ7XCJmdW5jdGlvblwiPT10eXBlb2YgdGhpcy5vcHRpb25zLnNoaWZ0JiYobj10aGlzLm9wdGlvbnMuc2hpZnQuY2FsbCh0aGlzLHt0b3A6ZSxsZWZ0Om99KSk7dmFyIGk9dm9pZCAwLHI9dm9pZCAwO2lmKFwic3RyaW5nXCI9PXR5cGVvZiBuKXtuPW4uc3BsaXQoXCIgXCIpLG5bMV09blsxXXx8blswXTt2YXIgcz1uLGE9QihzLDIpO2k9YVswXSxyPWFbMV0saT1wYXJzZUZsb2F0KGksMTApLHI9cGFyc2VGbG9hdChyLDEwKX1lbHNlIGk9bi50b3Ascj1uLmxlZnQ7cmV0dXJuIGUrPWksbys9cix7dG9wOmUsbGVmdDpvfX19fSksSX0pOyIsImRlZmluZShmdW5jdGlvbigpIHtcclxuXHJcbiAgICByZXR1cm4ge1xyXG5cclxuICAgICAgICBpbml0KCkge1xyXG4gICAgICAgICAgICAkKGRvY3VtZW50KS5vbignc2hvdy5icy5tb2RhbCcsICcuanMtYWpheC1tb2RhbCcsIHRoaXMuaW5pdE1vZGFscyk7XHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIGluaXRNb2RhbHMoZSkge1xyXG4gICAgICAgICAgICB2YXIgJG1vZGFsID0gJCh0aGlzKTtcclxuXHJcblxyXG4gICAgICAgICAgICBpZiAoISRtb2RhbC5kYXRhKCdsb2FkZWQnKSkge1xyXG4gICAgICAgICAgICAgICAgdmFyICRidG4gPSAkKGUucmVsYXRlZFRhcmdldCksXHJcbiAgICAgICAgICAgICAgICAgICAgbGluayA9ICRidG4uZGF0YSgnbGluaycpO1xyXG5cclxuICAgICAgICAgICAgICAgICRtb2RhbC5kYXRhKCdsb2FkZWQnLCB0cnVlKTtcclxuICAgICAgICAgICAgICAgICRtb2RhbC5maW5kKCcuanMtYWpheC1tb2RhbC1ib2R5JykuYXBwZW5kKCc8aWZyYW1lIGNsYXNzPVwiYWpheC1tb2RhbF9fYm9keV9faWZyYW1lXCIgZnJhbWVib3JkZXI9XCIwXCIgc3JjPVwiJyArIGxpbmsgKyAnXCIgd2lkdGg9XCIxMDAlXCIgaGVpZ2h0PVwiMTAwJVwiIHNjcm9sbGluZz1cImF1dG9cIj48L2lmcmFtZT4nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcblxyXG5cclxuICAgICAgICByZXNldEFqYXhNb2RhbHMoKSB7XHJcbiAgICAgICAgICAgICQoJy5qcy1hamF4LW1vZGFsJykub2ZmKCdzaG93LmJzLm1vZGFsJywgdGhpcy5pbml0TW9kYWxzKTtcclxuICAgICAgICAgICAgJCgnLmpzLWFqYXgtbW9kYWwnKS5vbignc2hvdy5icy5tb2RhbCcsIHRoaXMuaW5pdE1vZGFscyk7XHJcbiAgICAgICAgfSxcclxuICAgIH1cclxufSk7XHJcbiIsImRlZmluZShmdW5jdGlvbiAoKSB7XHJcbiAgICByZXR1cm4gZnVuY3Rpb24gKG4pIHtcclxuICAgICAgICB2YXIgYSA9IHRoaXMuZ2VuZXJhbDtcclxuICAgICAgICBhLmRlYm91bmNlID0gZnVuY3Rpb24gKG4sIGEsIGUpIHtcclxuICAgICAgICAgICAgdmFyIHQ7XHJcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgciA9IHRoaXMsIHUgPSBhcmd1bWVudHMsIGkgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdCA9IG51bGwsIGUgfHwgbi5hcHBseShyLCB1KVxyXG4gICAgICAgICAgICAgICAgfSwgbyA9IGUgJiYgIXQ7XHJcbiAgICAgICAgICAgICAgICBjbGVhclRpbWVvdXQodCksIHQgPSBzZXRUaW1lb3V0KGksIGEpLCBvICYmIG4uYXBwbHkociwgdSlcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sIGEuZWFzZUluT3V0UXVhZCA9IGZ1bmN0aW9uIChuLCBhLCBlLCB0KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBuIC89IHQgLyAyLCAxID4gbiA/IGUgLyAyICogbiAqIG4gKyBhIDogKG4tLSwgLWUgLyAyICogKG4gKiAobiAtIDIpIC0gMSkgKyBhKVxyXG4gICAgICAgIH0sIGEuYW5pbWF0ZSA9IGZ1bmN0aW9uIChuLCBhLCBlLCB0LCByLCB1KSB7XHJcbiAgICAgICAgICAgIG4gJiYgKHdpbmRvdy5yZXF1ZXN0QW5pbWF0aW9uRnJhbWUoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgbi5zdHlsZVthXSA9IHJcclxuICAgICAgICAgICAgfSwgdSksIG4uc3R5bGVbYV0gPSB0KVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7IiwiZGVmaW5lKGZ1bmN0aW9uICgpIHtcclxuICAgIHJldHVybiBmdW5jdGlvbiAodCwgZSkge1xyXG4gICAgICAgIHZhciBhLCBvLCBuLCBjLCBpLCBsLCByID0gMTUwLCB1ID0gMCwgcyA9IFwianMtdG9nZ2xlLWJ0blwiLCBnID0gXCJ0b2dnbGUtYnRuLS1hY3RpdmVcIiwgZCA9IFwidG9nZ2xlLWl0ZW0tLWFjdGl2ZVwiLFxyXG4gICAgICAgICAgICBmID0gXCJ0b2dnbGUtaXRlbV9fY29udGVudFwiO1xyXG4gICAgICAgIG8gPSBmdW5jdGlvbiAodCwgZSwgYSkge1xyXG4gICAgICAgICAgICBmb3IgKHZhciBvID0gZS5zcGxpdChcIiBcIiksIG4gPSAwLCBjID0gby5sZW5ndGg7IGMgPiBuOyBuKyspIHQuYWRkRXZlbnRMaXN0ZW5lcihvW25dLCBhLCAhMSlcclxuICAgICAgICB9LCBuID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBbXS5mb3JFYWNoLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5cIiArIHMpLCBmdW5jdGlvbiAodCkge1xyXG4gICAgICAgICAgICAgICAgbyh0LCBcImNsaWNrIHRvdWNoc3RhcnQgbW91c2Vkb3duXCIsIGZ1bmN0aW9uICh0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdC5wcmV2ZW50RGVmYXVsdCgpXHJcbiAgICAgICAgICAgICAgICB9KSwgbyh0LCBcInRvdWNoZW5kIG1vdXNldXBcIiwgZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgYSA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwodC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKSlbMF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG8gPSBhLnF1ZXJ5U2VsZWN0b3JBbGwoXCIuXCIgKyBmKVswXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbiA9IG8ub2Zmc2V0SGVpZ2h0LCB2ID0gYS5jbGFzc0xpc3QuY29udGFpbnMoZCksIGIgPSB0LmdldEF0dHJpYnV0ZShcImRhdGEtaGlkZS1vdGhlcnNcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHUgPSBudWxsICE9PSB0LmdldEF0dHJpYnV0ZShcImRhdGEtZHVyYXRpb25cIikgPyB0LmdldEF0dHJpYnV0ZShcImRhdGEtZHVyYXRpb25cIikgOiByLCB2KSBpKHQsIGEpOyBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBoID0gdC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKSwgbSA9IHQuZ2V0QXR0cmlidXRlKFwiZGF0YS1sZXZlbFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtdLmZvckVhY2guY2FsbChkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLlwiICsgcyArICdbZGF0YS1sZXZlbD1cIicgKyBtICsgJ1wiXScpLCBmdW5jdGlvbiAodCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBlID0gdC5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBoICE9PSBlICYmIHQuY2xhc3NMaXN0LmNvbnRhaW5zKGcpICYmIGwodCwgZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChlKVswXSlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgYyh0LCBhLCBvLCBuKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfSwgYyA9IGZ1bmN0aW9uICh0LCBlLCBhLCBvKSB7XHJcbiAgICAgICAgICAgIGNhcmdvYmF5LmdlbmVyYWwuYW5pbWF0ZShlLCBcImhlaWdodFwiLCBcInB4XCIsIG8sIFwiYXV0b1wiLCB1KSwgdC5jbGFzc0xpc3QuYWRkKGcpLCBlLmNsYXNzTGlzdC5hZGQoZClcclxuICAgICAgICB9LCBpID0gZnVuY3Rpb24gKHQsIGUsIGEsIG8pIHtcclxuICAgICAgICAgICAgY2FyZ29iYXkuZ2VuZXJhbC5hbmltYXRlKGUsIFwiaGVpZ2h0XCIsIFwicHhcIiwgMCwgbywgdSksIHQuY2xhc3NMaXN0LnJlbW92ZShnKSwgZS5jbGFzc0xpc3QucmVtb3ZlKGQpXHJcbiAgICAgICAgfSwgbCA9IGZ1bmN0aW9uICh0LCBlKSB7XHJcbiAgICAgICAgICAgIHQuY2xhc3NMaXN0LnJlbW92ZShnKSwgZS5zdHlsZS5oZWlnaHQgPSAwLCBlLmNsYXNzTGlzdC5yZW1vdmUoZClcclxuICAgICAgICB9LCBhID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICBuKClcclxuICAgICAgICB9KClcclxuICAgIH1cclxufSk7IiwiZGVmaW5lKGZ1bmN0aW9uKCkge1xyXG5cclxuICAgIHJldHVybiB7XHJcblxyXG4gICAgICAgIGluaXQoKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXJsQ2hvb3NlcigpO1xyXG4gICAgICAgICAgICB0aGlzLmluaXREZWxCdG4oKTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvLyBEZWwgYnRuXHJcbiAgICAgICAgaW5pdERlbEJ0bigpIHtcclxuICAgICAgICAgICAgJCgnYm9keScpLm9uKCdjbGljaycsICcuanMtbWVkaWEtY2hvb3Nlci1kZWwtcHJldmlldy1idG4nLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICAgICAgdmFyICR0aGlzID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgICAgICAgICBsaW5rZWRJRCA9ICR0aGlzLmRhdGEoJ2xpbmtlZC1pZCcpLFxyXG4gICAgICAgICAgICAgICAgICAgICR3aWRnZXQgPSAkKCcjJyArIGxpbmtlZElEICsgJy13aWRnZXQnKSxcclxuICAgICAgICAgICAgICAgICAgICAkaW5wdXQgPSAkKCcjJyArIGxpbmtlZElEKTtcclxuXHJcbiAgICAgICAgICAgICAgICAkdGhpcy5wYXJlbnQoKS5maW5kKCcubWVkaWEtY2hvb3Nlcl9fcHJldmlldycpLnJlbW92ZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICQoXCIubWVkaWEtdGh1bWJuYWlsX19pY29uXCIpLnJlbW92ZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICR3aWRnZXQucmVtb3ZlQ2xhc3MoJ21lZGlhLWNob29zZXItLWNob29zZW4nKTtcclxuICAgICAgICAgICAgICAgICRpbnB1dC52YWwoJycpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvLyBVUkwtQ2hvb3NlclxyXG4gICAgICAgIHVybENob29zZXIoKSB7XHJcblxyXG4gICAgICAgICAgICAvLyBMaW5rIENob29zZXIgc2VsZWN0XHJcbiAgICAgICAgICAgICQoJ2JvZHknKS5vbignY2xpY2snLCAnLmpzLXVybC1jaG9vc2VyLWxpbmstc2VsZWN0JywgZnVuY3Rpb24gKGUpIHtcclxuICAgICAgICAgICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgJHRoaXMgPSAkKHRoaXMpLFxyXG4gICAgICAgICAgICAgICAgICAgIHNsdWcgPSAkdGhpcy5kYXRhKCdzbHVnJyksXHJcbiAgICAgICAgICAgICAgICAgICAgaWQgPSAkdGhpcy5kYXRhKCdpZCcpLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlcGxhY2VVcmwgPSAkdGhpcy5jbG9zZXN0KCduYXYnKS5kYXRhKCdyZXBsYWNlLXVybCcpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFN0b3JlIHZhbHVlc1xyXG4gICAgICAgICAgICAgICAgaXRlbVVybCA9IChzbHVnID8gc2x1ZyA6ICcnKTtcclxuICAgICAgICAgICAgICAgIGl0ZW1JZCA9IGlkO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFJlcGxhY2UgVVJMXHJcbiAgICAgICAgICAgICAgICAkLmFqYXgoe1xyXG4gICAgICAgICAgICAgICAgICAgIHVybDogcmVwbGFjZVVybCxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnR0VUJyxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB7J3RleHQnOiBpdGVtVXJsfSxcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAocmVzcG9uc2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVwbGFjZWRVcmwgPSByZXNwb25zZS50ZXh0O1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgLy8gVXBkYXRlIHByZXZpZXdcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnI3VybC1jaG9vc2VyX19zZWxlY3Rpb24tcHJldmlldycpLnRleHQoJ1NlbGVjdGlvbjogJyArIHJlcGxhY2VkVXJsKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBNZWRpYSBDaG9vc2VyIHNlbGVjdFxyXG4gICAgICAgICAgICAkKCdib2R5Jykub24oJ2NsaWNrJywgJy5qcy11cmwtY2hvb3Nlci1tZWRpYS1zZWxlY3QnLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgICAgIHZhciAkdGhpcyA9ICQodGhpcyksXHJcbiAgICAgICAgICAgICAgICAgICAgcGF0aCA9ICR0aGlzLmRhdGEoJ3BhdGgnKSxcclxuICAgICAgICAgICAgICAgICAgICB0aHVtYlBhdGggPSAkdGhpcy5kYXRhKCd0aHVtYi1wYXRoJyksXHJcbiAgICAgICAgICAgICAgICAgICAgaWQgPSAkdGhpcy5kYXRhKCdpZCcpLFxyXG4gICAgICAgICAgICAgICAgICAgIHRpdGxlID0gJHRoaXMuZGF0YSgndGl0bGUnKSxcclxuICAgICAgICAgICAgICAgICAgICBja2UgPSAkdGhpcy5kYXRhKCdja2UnKSxcclxuICAgICAgICAgICAgICAgICAgICByZXBsYWNlVXJsID0gJHRoaXMuY2xvc2VzdCgnLnRodW1ibmFpbC13cmFwcGVyJykuZGF0YSgncmVwbGFjZS11cmwnKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBTdG9yZSB2YWx1ZXNcclxuICAgICAgICAgICAgICAgIGl0ZW1VcmwgPSBwYXRoO1xyXG4gICAgICAgICAgICAgICAgaXRlbUlkID0gaWQ7XHJcbiAgICAgICAgICAgICAgICBpdGVtVGl0bGUgPSB0aXRsZTtcclxuICAgICAgICAgICAgICAgIGl0ZW1UaHVtYlBhdGggPSB0aHVtYlBhdGg7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gU2F2ZVxyXG4gICAgICAgICAgICAgICAgaWYgKCFja2UpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgaXNNZWRpYUNob29zZXIgPSAkKHdpbmRvdy5mcmFtZUVsZW1lbnQpLmNsb3Nlc3QoJy5qcy1hamF4LW1vZGFsJykuZGF0YSgnbWVkaWEtY2hvb3NlcicpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAoaXNNZWRpYUNob29zZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHNjLm1lZGlhQ2hvb3Nlci5zYXZlTWVkaWFDaG9vc2VyTW9kYWwoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFJlcGxhY2UgVVJMXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQuYWpheCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6IHJlcGxhY2VVcmwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnR0VUJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRhdGE6IHsndGV4dCc6IGl0ZW1Vcmx9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKHJlc3BvbnNlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVwbGFjZWRVcmwgPSByZXNwb25zZS50ZXh0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KS5kb25lKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBzYy5tZWRpYUNob29zZXIuc2F2ZVVybENob29zZXJNb2RhbChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHBzYy5tZWRpYUNob29zZXIuc2F2ZU1lZGlhQ2hvb3Nlck1vZGFsKHRydWUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcblxyXG4gICAgICAgICAgICAvLyBDYW5jZWxcclxuICAgICAgICAgICAgJCgnI2NhbmNlbC11cmwtY2hvb3Nlci1tb2RhbCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHZhciBja2UgPSAkKHRoaXMpLmRhdGEoJ2NrZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghY2tlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyICRwYXJlbnRNb2RhbCA9ICQod2luZG93LmZyYW1lRWxlbWVudCkuY2xvc2VzdCgnLmpzLWFqYXgtbW9kYWwnKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyZW50TW9kYWxJZCA9ICRwYXJlbnRNb2RhbC5hdHRyKCdpZCcpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBwYXJlbnQuJCgnIycgKyBwYXJlbnRNb2RhbElkKS5tb2RhbCgnaGlkZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuXHJcbiAgICAgICAgICAgIC8vIE9LXHJcbiAgICAgICAgICAgICQoZG9jdW1lbnQpLm9uKCdjbGljaycsICcjc2F2ZS11cmwtY2hvb3Nlci1tb2RhbCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHZhciBja2UgPSAkKHRoaXMpLmRhdGEoJ2NrZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIHBzYy5tZWRpYUNob29zZXIuc2F2ZVVybENob29zZXJNb2RhbChja2UpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9LFxyXG5cclxuICAgICAgICAvLyBTYXZlIGZvciBVUkwtY2hvb3NlclxyXG4gICAgICAgIHNhdmVVcmxDaG9vc2VyTW9kYWwoY2tlKSB7XHJcbiAgICAgICAgICAgIGlmICghY2tlKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgJHBhcmVudE1vZGFsID0gJCh3aW5kb3cuZnJhbWVFbGVtZW50KS5jbG9zZXN0KCcuanMtYWpheC1tb2RhbCcpLFxyXG4gICAgICAgICAgICAgICAgICAgIGxpbmtlZElucHV0SWQgPSAkcGFyZW50TW9kYWwuZGF0YSgnbGlua2VkLWlucHV0LWlkJyksXHJcbiAgICAgICAgICAgICAgICAgICAgcGFyZW50TW9kYWxJZCA9ICRwYXJlbnRNb2RhbC5hdHRyKCdpZCcpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFNldCB2YWxcclxuICAgICAgICAgICAgICAgIHBhcmVudC4kKCcjJyArIGxpbmtlZElucHV0SWQpLnZhbChpdGVtVXJsKS5jaGFuZ2UoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBTZXQgcHJvcGVyIFVSTFxyXG4gICAgICAgICAgICAgICAgcGFyZW50LiQoJyMnICsgbGlua2VkSW5wdXRJZCkucGFyZW50KCkuZmluZCgnLmpzLXVybGNob29zZXItdmFsdWUnKS52YWwocmVwbGFjZWRVcmwpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIENsb3NlIG1vZGFsXHJcbiAgICAgICAgICAgICAgICBwYXJlbnQuJCgnIycgKyBwYXJlbnRNb2RhbElkKS5tb2RhbCgnaGlkZScpO1xyXG5cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZhciBmdW5jTnVtID0gZ2V0VXJsUGFyYW0oJ0NLRWRpdG9yRnVuY051bScpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFNldCB2YWxcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5vcGVuZXIuQ0tFRElUT1IudG9vbHMuY2FsbEZ1bmN0aW9uKGZ1bmNOdW0sIGl0ZW1VcmwpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIENsb3NlIHdpbmRvd1xyXG4gICAgICAgICAgICAgICAgd2luZG93LmNsb3NlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG5cclxuXHJcbiAgICAgICAgLy8gU2F2ZSBmb3IgTWVkaWEtY2hvb3NlclxyXG4gICAgICAgIHNhdmVNZWRpYUNob29zZXJNb2RhbChja2UpIHtcclxuICAgICAgICAgICAgaWYgKCFja2UpIHtcclxuICAgICAgICAgICAgICAgIHZhciAkcGFyZW50TW9kYWwgPSAkKHdpbmRvdy5mcmFtZUVsZW1lbnQpLmNsb3Nlc3QoJy5qcy1hamF4LW1vZGFsJyksXHJcbiAgICAgICAgICAgICAgICAgICAgbGlua2VkSW5wdXRJZCA9ICRwYXJlbnRNb2RhbC5kYXRhKCdsaW5rZWQtaW5wdXQtaWQnKSxcclxuICAgICAgICAgICAgICAgICAgICBwYXJlbnRNb2RhbElkID0gJHBhcmVudE1vZGFsLmF0dHIoJ2lkJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gU2V0IHZhbFxyXG4gICAgICAgICAgICAgICAgcGFyZW50LiQoJyMnICsgbGlua2VkSW5wdXRJZCkudmFsKGl0ZW1JZCkuY2hhbmdlKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gVXBkYXRlIHByZXZpZXdcclxuICAgICAgICAgICAgICAgIHZhciAkbWVkaWFDaG9vc2VyID0gcGFyZW50LiQoJyMnICsgbGlua2VkSW5wdXRJZCArICctd2lkZ2V0JyksXHJcbiAgICAgICAgICAgICAgICAgICAgJHByZXZpZXdJbWcgPSBwYXJlbnQuJCgnIycgKyBsaW5rZWRJbnB1dElkICsgJ19fcHJldmlld19faW1nJyksXHJcbiAgICAgICAgICAgICAgICAgICAgJHByZXZpZXdUaXRsZSA9IHBhcmVudC4kKCcjJyArIGxpbmtlZElucHV0SWQgKyAnX19wcmV2aWV3X190aXRsZScpO1xyXG5cclxuICAgICAgICAgICAgICAgICRtZWRpYUNob29zZXIuYWRkQ2xhc3MoJ21lZGlhLWNob29zZXItLWNob29zZW4nKTtcclxuICAgICAgICAgICAgICAgICRwcmV2aWV3VGl0bGUuaHRtbChpdGVtVGl0bGUpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChpdGVtVGh1bWJQYXRoID09PSBcIlwiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyICRwYXJlbnQgPSAkcHJldmlld1RpdGxlLnBhcmVudCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICRwYXJlbnQucHJlcGVuZCgnPGkgY2xhc3M9XCJmYSBmYS1maWxlLW8gbWVkaWEtdGh1bWJuYWlsX19pY29uXCI+PC9pPicpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAkcHJldmlld0ltZy5hdHRyKCdzcmMnLCBpdGVtVGh1bWJQYXRoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAvLyBDbG9zZSBtb2RhbFxyXG4gICAgICAgICAgICAgICAgcGFyZW50LiQoJyMnICsgcGFyZW50TW9kYWxJZCkubW9kYWwoJ2hpZGUnKTtcclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZnVuY051bSA9IGdldFVybFBhcmFtKCdDS0VkaXRvckZ1bmNOdW0nKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBTZXQgdmFsXHJcbiAgICAgICAgICAgICAgICB3aW5kb3cub3BlbmVyLkNLRURJVE9SLnRvb2xzLmNhbGxGdW5jdGlvbihmdW5jTnVtLCBpdGVtVXJsKTtcclxuXHJcbiAgICAgICAgICAgICAgICAvLyBDbG9zZSB3aW5kb3dcclxuICAgICAgICAgICAgICAgIHdpbmRvdy5jbG9zZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuXHJcblxyXG4gICAgICAgIC8vIEdldCBVcmwgUGFyYW1ldGVyc1xyXG4gICAgICAgIGdldFVybFBhcmFtKHBhcmFtTmFtZSkge1xyXG4gICAgICAgICAgICB2YXIgcmVQYXJhbSA9IG5ldyBSZWdFeHAoJyg/OltcXD8mXXwmYW1wOyknICsgcGFyYW1OYW1lICsgJz0oW14mXSspJywgJ2knKSxcclxuICAgICAgICAgICAgICAgIG1hdGNoID0gd2luZG93LmxvY2F0aW9uLnNlYXJjaC5tYXRjaChyZVBhcmFtKTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybiAobWF0Y2ggJiYgbWF0Y2gubGVuZ3RoID4gMSkgPyBtYXRjaFsxXSA6ICcnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcbn0pO1xyXG4iLCIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iXSwic291cmNlUm9vdCI6IiJ9