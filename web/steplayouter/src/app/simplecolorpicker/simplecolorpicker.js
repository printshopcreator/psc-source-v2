var app = angular.module('simplecolorpicker', []);

//This directive is for the individual buttons.
//I thought it might be easier to break things up.
app.directive("scpButton", function() {
    var fakeText = '&nbsp;&nbsp;&nbsp;&nbsp;';
    return {
        restrict: 'E',
        template: '<div ' +
            ' title="{{titleText || color.name}}"' +
            ' ng-style="{\'background-color\':color.rgb}" data-color="{{color.value}}" ng-class="getClass()"' +
            ' ng-class="getClass()"' +
            ' ng-click="clicked()"' +
            ' role="button" tabindex="0">' + fakeText +
            '</div>',
        replace: true,
        scope: {
            'titleText': '=',
            'color' : '=scpColor',
            'class' : '@class',
            'currentColor' : '=scpCurrentColor'
        },
        link: function(scope, element, attrs) {
            scope.clicked = function() {
                if(element.hasClass('button')) {
                    scope.$parent.selectColor(scope.color);
                }
            };
            scope.getClass = function() {
                if(scope.color && scope.color.value != "0") {
                    if(scope.color.value == "transparent") {
                        scope.color.rgb = "rgb(255,255,255,0)";
                    }else {
                        if(scope.color.value.indexOf('gb') > 0) {
                            scope.color.rgb = scope.color.value;
                        }else{
                            scope.color.rgb = scope.hex2rgb(scope.color.value);
                        }

                    }
                }
                if(scope.$parent.currentColor) {
                    if(scope.$parent.currentColor.value.indexOf('gb') > 0) {
                        scope.$parent.currentColor.value = scope.rgb2hex(scope.$parent.currentColor.value);
                    }
                    return scope.$parent.currentColor.value == scope.color.value && "selected" || "";
                }

                return "";
            };
            scope.hex2rgb = function hex2rgb(hex) {
                if (hex[0]=="#") hex=hex.substr(1);
                if (hex.length==3) {
                    var temp=hex; hex='';
                    temp = /^([a-f0-9])([a-f0-9])([a-f0-9])$/i.exec(temp).slice(1);
                    for (var i=0;i<3;i++) hex+=temp[i]+temp[i];
                }
                var triplets = /^([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})$/i.exec(hex).slice(1);
                return "rgb(" + parseInt(triplets[0],16) + "," + parseInt(triplets[1],16) + ","
                            + parseInt(triplets[2],16) + ")";
            };
            scope.rgb2hex = function (rgb) {
                rgb = Array.apply(null, arguments).join().match(/\d+/g);
                rgb = ((rgb[0] << 16) + (rgb[1] << 8) + (+rgb[2])).toString(16);

                // for (var i = rgb.length; i++ < 6;) rgb = '0' + rgb;

                return rgb;
            };



        }
    };

});

//This directive drives the whole picker.
app.directive('simplecolorpicker', ['painterManager', function(painterManager) {
    return {
        restrict: 'E',
        template: '<span class="simplecolorpicker inline">' +
            '<scp-button class="simplecolorpicker icon" scp-color="currentColor" ng-show="picker"></scp-button>' +
            '</span>' +
            '<span ng-class="getClass()">' +
            '<scp-button class="simplecolorpicker button" ng-repeat="color in colors" scp-current-color="currentColor" scp-color="color"></scp-button>' +
            '</span>',
        replace: false,
        scope: {
            'colors': '=scpColors',
            'currentColor' : '=scpColor',
            'picker': '=scpPicker',
            'index': '@'
        },
        link: function(scope, element, attrs) {

            element = jQuery(element);
            var $painterManager = painterManager;
            var $icon = element.find('.icon');
            var showPicker = function(){
                $('.picker').hide();
                var $picker = element.find('.picker');
                $picker.show();
            };
            var hidePicker = function() {
                var $picker = element.find('.picker');
                if($picker.length > 0 && $picker.is(":visible")) {
                    $picker.hide();
                }
            };
            var mouseDown = function(e) {
                e.stopPropagation();
                e.preventDefault();
            };
            //Event handling.
            $(document).on('mousedown', hidePicker);
            $icon.on('mousedown', showPicker);
            $(element).on('mousedown', '.picker, .icon', mouseDown);

            //This watch is neccessary to make sure the list of buttons gets
            //shown again when the picker option is turned off.
            //Neccessary because of the way hidePicker works currently.
            scope.$watch('picker', function(newVal, oldVal) {
                if(newVal === false) {
                    if(element.find('.colors').length>0) {
                        element.find('.colors').show();
                    }
                }
            });

            //Scope functions, directives can have builtin controllers so that
            //would probably be best but this works for now.
            scope.getClass = function () {
                return [
                    "simplecolorpicker",
                    "colors",
                    scope.picker && 'picker' || ''
                ];
            };
            scope.selectColor = function(color) {
                if(scope.index.indexOf("calendar") >= 0) {
                    $painterManager.changeCalSettingsColors(scope.index, color);
                    return;
                }
                if(!scope.picker) {
                    $painterManager.changeBackgroundColorFrom(scope.index, color);
                }
                scope.currentColor = color;
            };
        }
    };
}]);
