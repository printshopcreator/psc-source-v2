angular.module( 'layouterApp.preview', [

])
.config(function config( ) {
})

.service('previewManager', ['$rootScope', 'sitesManager', 'dataService',function($rootScope, sitesManager, dataService) {
    var Preview = {

        preview: function(site) {

            jQuery("#site_" + site.id).remove();

            var canvasElement = jQuery("#site_li_" + site.id + " .canvas-holder");
            canvasElement.html('<canvas id="site_' + site.id + '"></canvas>');

            canvasElement = document.getElementById('site_' + site.id);
            var currCtx = canvasElement.getContext('2d');
            var viewportWidth = 100;

            canvasElement.setAttribute("width", 100);

            var previewScale = 100/site.width;

            canvasElement.setAttribute("height", site.height*previewScale);

            var siteWidth = 100;
            var siteHeight = site.height*previewScale;

            site.previewWidth = 100;
            site.previewHeight = siteHeight;

            site.previewScale = previewScale;

            paperPreview = new fabric.StaticCanvas(document.getElementById('site_' + site.id));
            paperPreview.on('before:render', function() {
                this._objects.sort(function(a, b) {
                    return (a.pos - b.pos);
                });
            });

            var rect = new fabric.Rect({
                left: 0,
                top: 0,
                fill: site.backgroundColor,
                width: siteWidth,
                height: siteHeight,
                selectable: false,
                hasBorders: false,
                hasControls: false,
                name: "back",
                id: "background-color",
                orgLeft: 0,
                orgTop: 0,
                pos: 0
            });

            // "add" rectangle onto canvas
            paperPreview.add(rect);

            var scope = this;


            angular.forEach(site.elements,function(element, ie){

                element.paintPreview(site, paperPreview, element, element.x, element.y, element.width, element.height, element.selectable);

            });

            site.previewFabric = paperPreview;
            site.previewImage = paperPreview.toDataURL({format: "jpeg", quality: 8});
        },

        previewFromBasket: function(site) {
            jQuery("#site_" + site.id).remove();

            var canvasElement = jQuery("#site_li_" + site.id + " .canvas-holder");
            canvasElement.html('<canvas id="site_' + site.id + '"></canvas>');

            canvasElement = document.getElementById('site_' + site.id);
            var currCtx = canvasElement.getContext('2d');
            var viewportWidth = 100;

            canvasElement.setAttribute("width", 100);

            var previewScale = 100/site.width;

            canvasElement.setAttribute("height", site.height*previewScale);

            var siteWidth = 100;
            var siteHeight = site.height*previewScale;

            site.previewWidth = 100;
            site.previewHeight = siteHeight;

            site.previewScale = previewScale;

            paper = new fabric.StaticCanvas(document.getElementById('site_' + site.id));

            paper.on('after:render', function() {

                var objects = this.getObjects();

                if(objects.length > 0) {
                    this.off('after:render');

                    angular.forEach(objects,function(obj, i){
                        if (obj.name != "left" &&
                            obj.name != "right" &&
                            obj.name != "top" &&
                            obj.name != "bottom" &&
                            obj.name != "back") {
                            obj.left=obj.left*site.previewScale;
                            obj.top=obj.top*site.previewScale;

                            obj.scaleX = (obj.scaleX*site.previewScale);
                            obj.scaleY = (obj.scaleY*site.previewScale);
                            obj.setCoords();
                        }
                    });
                    this.renderAll();
                    site.previewFabric = this;
                    site.previewImage = this.toDataURL({format: "jpeg", quality: 8});
                }
            });
            paper.loadFromJSON(site.fabricJson, paper.renderAll.bind(paper));

        },

        previewLoaded: function(site) {
            site.convertFabricToJsonOnly();

            jQuery("#site_" + site.id).remove();

            var canvasElement = jQuery("#site_li_" + site.id + " .canvas-holder");
            canvasElement.html('<div id="site_' + site.id + '"></div');


            $('#site_' + site.id).css("background", "url('" + site.fabricObj.toDataURL({
                format: 'png',
                multiplier: 100/site.calcWidth,
                left: site.leftOffset+1,
                top: site.topOffset+2,
                width: (site.width-1)*site.scale,
                height: (site.height-1)*site.scale
            }) + "') no-repeat");

            $('#site_' + site.id).css("width", 100);
            $('#site_' + site.id).css("margin", "auto");
            $('#site_' + site.id).css("height", site.height*100/site.width);
            return;


        },

        previewLoadedFromPreview: function(site) {
            site.convertPreviewJson();

            jQuery("#site_" + site.id).remove();

            var canvasElement = jQuery("#site_li_" + site.id + " .canvas-holder");
            canvasElement.html('<canvas id="site_' + site.id + '"></canvas>');

            canvasElement = document.getElementById('site_' + site.id);
            var currCtx = canvasElement.getContext('2d');
            var viewportWidth = 100;

            canvasElement.setAttribute("width", 100);

            var previewScale = 100/site.width;

            canvasElement.setAttribute("height", site.height*previewScale);

            var siteWidth = 100;
            var siteHeight = site.height*previewScale;

            site.previewWidth = 100;
            site.previewHeight = siteHeight;

            site.previewScale = previewScale;

            var paperPrev = new fabric.StaticCanvas(document.getElementById('site_' + site.id));

            paperPrev.on('after:render', function() {

                var objects = this.getObjects();

                if(objects.length > 0) {
                    this.off('after:render');
                    angular.forEach(objects,function(obj, i){
                        if(obj.isLoaded) {
                            return;
                        }
                        if(obj.id == "background-color") {
                            obj.fill = site.backgroundColor;
                        }
                        if(obj.id == "calendar") {
                            fabric.util.loadImage(
                                dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height,
                                    site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1,7),
                                    site.calendarSetting.dayColor.value.substring(1,7), site.calendarSetting.sunColor.value.substring(1,7),
                                    site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal,
                                    site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY,
                                    site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily
                                ), function (img) {
                                obj.setElement(img);
                                    paperPrev.renderAll();
                                site.previewFabric = paperPrev;
                                site.previewJson = paperPrev.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);
                                site.previewImage = paperPrev.toDataURL({format: "jpeg", quality: 8});

                            });

                        }
                    });
                    site.previewFabric = this;
                    site.previewImage = this.toDataURL({format: "jpeg", quality: 8});
                }
            });

            paperPrev.loadFromJSON(site.previewJson, paperPrev.renderAll.bind(paperPrev));
        }
    };

    return Preview;
}])



/**
 * And of course we define a controller for our route.
 */
.controller( 'PreivewCtrl', function PreviewController( $scope, $rootScope, previewManager ) {

})

;
