angular.module( 'layouterApp.elements')

.factory('SiteElementImage', ['dataService' , 'cfpLoadingBar',function(dataService, cfpLoadingBar) {
    var SiteElementImage = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            pos:0,
            type:'image',
            x: 0,
            y: 0,
            id: "",
            motive: [],
            label: "",
            width: 0,
            height: 0,
            rotate: 0,
            selectable: false,
            deletable: true,
            resetable: false,
            limitBoundary: false,
            value : false,
            paint: function($rootScope, site, paper, element, x, y, width, height, selectable, deletable = true, resetable = false) {
                cfpLoadingBar.start();
                if(site.passepartout && element.json) {
                    fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                        var pass;
                        if(objects[0].path) {
                            pass = new fabric.Path(objects[0].d,  {left:0,top:0});
                        }else{
                            pass = fabric.util.groupSVGElements(objects, options);
                        }

                        fabric.Image.fromObject(element.json, function (img) {
                            img.clipTo = null;
                            var angle = img.angle;
                            img.angle = 0;
                            if(site.leftOffset == 0) {
                                //img.left = ((img.left+site.trimLeft)*site.scale);
                                //img.top = ((img.top+site.trimTop)*site.scale);
                            }else{
                                img.left = site.leftOffset+((img.left-site.trimLeft)*site.scale);
                                img.top = site.topOffset+((img.top-site.trimTop)*site.scale);
                            }

                            img.scaleToWidth(element.json.width*element.json.scaleX*site.scale);

                            img.angle = angle;
                            img.clipBox = element.json.clipBox;

                            img.siteScale = site.scale;
                            img.renderPreview = element.json.renderPreview;
                            img.renderPdf = element.json.renderPdf;
                            img.passepartout = pass;
                            img.passepartoutOrg = {objects: objects, options: options, element: {value: element.passepartout, rel: element.passepartoutRel, x: element.passepartoutX, y: element.passepartoutY, width: element.passepartoutWidth, height: element.passepartoutHeight}};

                            img.on('selected', function() {

                                $rootScope.$broadcast('tools.motiv.selected', {
                                    deletable: element.deletable,
                                    resetable: element.resetable,
                                    motivUUID: element.id,
                                    motivWidth: element.width/site.scale,
                                    motivHeight: element.height/site.scale,
                                });
                            });

                            img.clipTo = function (ctx) {
                                this.passepartout.top = 0;
                                this.passepartout.left = 0;
                                ctx.save();
                                ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                ctx.translate(0,0);
                                ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(site.passepartoutX*site.scale))*-1/ this.getScaleX(),
                                    ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(site.passepartoutY*site.scale))*-1/ this.getScaleX());
                                ctx.beginPath();
                                this.passepartout.opacity=0;
                                this.passepartout.scaleX = ((parseFloat(site.passepartoutWidth)*site.scale/ this.getScaleX())/this.passepartout.width);
                                this.passepartout.scaleY = ((parseFloat(site.passepartoutHeight)*site.scale/ this.getScaleY())/this.passepartout.height);
                                this.passepartout.render(ctx);
                                ctx.closePath();
                                ctx.restore();
                            };

                            paper.add(img);
                            cfpLoadingBar.complete();
                        });


                    });
                }else{
                    if(element.json) {
                        if(element.passepartout) {


                            fabric.loadSVGFromURL(dataService.getRawPath(element.passepartout), function(objects, options) {

                                var pass;
                                if (objects[0].path) {
                                    pass = new fabric.Path(objects[0].d, {left: 0, top: 0});
                                } else {
                                    pass = fabric.util.groupSVGElements(objects, options);
                                }

                                fabric.Image.fromObject(element.json, function (img) {
                                    img.clipTo = null;
                                    var angle = img.angle;
                                    img.angle = 0;
                                    if (site.leftOffset == 0) {
                                        //img.left = ((img.left+site.trimLeft)*site.scale);
                                        //img.top = ((img.top+site.trimTop)*site.scale);
                                    } else {
                                        img.left = site.leftOffset + ((img.left - site.trimLeft) * site.scale);
                                        img.top = site.topOffset + ((img.top - site.trimTop) * site.scale);
                                    }

                                    img.scaleToWidth(element.json.width * element.json.scaleX * site.scale);

                                    img.angle = angle;
                                    img.clipBox = element.json.clipBox;

                                    img.siteScale = site.scale;
                                    img.renderPreview = element.json.renderPreview;
                                    img.renderPdf = element.json.renderPdf;
                                    img.passepartout = pass;
                                    img.passepartoutOrg = {objects: objects, options: options, element: {value: element.passepartout, rel: element.passepartoutRel, x: element.passepartoutX, y: element.passepartoutY, width: element.passepartoutWidth, height: element.passepartoutHeight}};


                                    img.on('selected', function () {

                                        $rootScope.$broadcast('tools.motiv.selected', {
                                            deletable: element.deletable,
                                            resetable: element.resetable,
                                            motivUUID: element.id,
                                            motivWidth: element.width/site.scale,
                                            motivHeight: element.height/site.scale,
                                        });
                                    });

                                    img.clipTo = function (ctx) {
                                        this.passepartout.top = 0;
                                        this.passepartout.left = 0;
                                        ctx.save();
                                        ctx.rotate(this.getAngle() * -1 / 180 * Math.PI);
                                        ctx.translate(0, 0);
                                        ctx.translate(((this.getPointByOrigin("center", "center").x - site.leftOffset) - parseFloat(element.passepartoutX * site.scale)) * -1 / this.getScaleX(),
                                            ((this.getPointByOrigin("center", "center").y - site.topOffset) - parseFloat(element.passepartoutY * site.scale)) * -1 / this.getScaleX());
                                        ctx.beginPath();
                                        this.passepartout.opacity = 0;
                                        this.passepartout.scaleX = ((parseFloat(element.passepartoutWidth) * site.scale / this.getScaleX()) / this.passepartout.width);
                                        this.passepartout.scaleY = ((parseFloat(element.passepartoutHeight) * site.scale / this.getScaleY()) / this.passepartout.height);
                                        this.passepartout.render(ctx);
                                        ctx.closePath();
                                        ctx.restore();
                                    };

                                    paper.add(img);
                                    cfpLoadingBar.complete();
                                });

                            });


                        }else {
                            fabric.Image.fromObject(element.json, function (img) {
                                img.clipTo = null;
                                var angle = img.angle;
                                img.angle = 0;
                                if (site.leftOffset == 0) {
                                    //img.left = ((img.left+site.trimLeft)*site.scale);
                                    //img.top = ((img.top+site.trimTop)*site.scale);
                                } else {
                                    img.left = site.leftOffset + ((img.left - site.trimLeft) * site.scale);
                                    img.top = site.topOffset + ((img.top - site.trimTop) * site.scale);
                                }

                                img.scaleToWidth(element.json.width * element.json.scaleX * site.scale);

                                img.angle = angle;
                                img.clipBox = element.json.clipBox;

                                img.siteScale = site.scale;
                                img.renderPreview = element.json.renderPreview;
                                img.renderPdf = element.json.renderPdf;
                                if(element.limitBoundary) {
                                    img.on('moving', (e) => {

                                        img.set({
                                            top: Math.min(Math.max(img.top, site.topOffset + (site.trimTop * site.scale)), site.topOffset + ((site.height - site.trimTop) * site.scale) - (img.height * img.scaleY)),
                                            left: Math.min(Math.max(img.left, site.leftOffset + (site.trimLeft * site.scale)), site.leftOffset + ((site.width - site.trimLeft) * site.scale) - (img.width * img.scaleX)),
                                        })
                                        img.setCoords();

                                    });
                                    img.on('scaling', (e) => {

                                        if(img.width*img.scaleX>site.width*site.scale||img.height*img.scaleY>site.height*site.scale) {
                                            img.set({
                                                top: site.topOffset,
                                                left: site.leftOffset,
                                                scaleX: img.orgScaleX,
                                                scaleY: img.orgScaleY,
                                            });

                                        }else{
                                            img.set({
                                                top: Math.min(Math.max(img.top, site.topOffset ), site.topOffset + (site.height * site.scale) - img.getBoundingRect().height),
                                                left: Math.min(Math.max(img.left, site.leftOffset ), site.leftOffset + (site.width * site.scale) - img.getBoundingRect().width),
                                            });
                                            img.orgScaleX = img.scaleX;
                                            img.orgScaleY = img.scaleY;
                                        }
                                        img.setCoords();

                                    });
                                }
                                img.on('selected', function () {

                                    $rootScope.$broadcast('tools.motiv.selected', {
                                        deletable: element.deletable,
                                        resetable: element.resetable,
                                        motivUUID: element.id,
                                        motivWidth: element.width/site.scale,
                                        motivHeight: element.height/site.scale,
                                    });
                                });

                                if (img.clipBox) {

                                    img.clipTo = function (ctx) {

                                        ctx.save();
                                        ctx.translate(0, 0);
                                        ctx.rotate(this.getAngle() * -1 / 180 * Math.PI);
                                        //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                                        ctx.beginPath();
                                        ctx.rect(
                                            (this.getPointByOrigin("center", "center").x - site.leftOffset - (parseFloat(this.clipBox[0]) * this.siteScale)) * -1 / this.getScaleX(),
                                            (this.getPointByOrigin("center", "center").y - site.topOffset - (parseFloat(this.clipBox[1]) * this.siteScale)) * -1 / this.getScaleX(),
                                            parseFloat(this.clipBox[2]) * this.siteScale / this.getScaleX(),
                                            parseFloat(this.clipBox[3]) * this.siteScale / this.getScaleX()
                                        );
                                        ctx.closePath();
                                        ctx.restore();
                                    };

                                }

                                paper.add(img);
                                cfpLoadingBar.complete();
                            });
                        }
                    }else{
                        cfpLoadingBar.start();
                        fabric.util.loadImage(element.value, function (img) {

                            var motiv = new fabric.Image(img, {
                                originX: 'left',
                                originY: 'top',
                                left: (site.leftOffset+x*site.scale),
                                top: (site.topOffset+y*site.scale),
                                centeredRotating: true,
                                centeredScaling: true,
                                id: element.id,
                                selectable: element.selectable,
                                deletable: element.deletable,
                                resetable: element.resetable,
                                evented: element.selectable,
                                angle: element.rotate,
                                orgLeft: x,
                                orgTop: y,
                                clipBox: element.clipbox,
                                siteScale: site.scale,
                                renderPreview: element.renderPreview,
                                renderPdf: element.renderPdf,
                                pos: parseFloat(element.pos, 10)
                            });

                            motiv.scaleToWidth(width*site.scale);
                            if(element.limitBoundary) {
                                motiv.on('moving', (e) => {

                                    motiv.set({
                                        top: Math.min(Math.max(motiv.top, site.topOffset ), site.topOffset + (site.height * site.scale) - motiv.getBoundingRect().height),
                                        left: Math.min(Math.max(motiv.left, site.leftOffset ), site.leftOffset + (site.width * site.scale) - motiv.getBoundingRect().width),
                                    })
                                    motiv.setCoords();
                                    console.log(site.leftOffset);

                                });
                                motiv.on('scaling', (e) => {

                                    if(motiv.width*motiv.scaleX>site.width*site.scale||motiv.height*motiv.scaleY>site.height*site.scale) {
                                        motiv.set({
                                            top: site.topOffset,
                                            left: site.leftOffset,
                                            scaleX: motiv.orgScaleX,
                                            scaleY: motiv.orgScaleY,
                                        });

                                    }else{
                                        motiv.set({
                                            top: Math.min(Math.max(motiv.top, site.topOffset ), site.topOffset + (site.height * site.scale) - motiv.getBoundingRect().height),
                                            left: Math.min(Math.max(motiv.left, site.leftOffset ), site.leftOffset + (site.width * site.scale) - motiv.getBoundingRect().width),
                                        });
                                        motiv.orgScaleX = motiv.scaleX;
                                        motiv.orgScaleY = motiv.scaleY;
                                    }
                                    motiv.setCoords();

                                });
                            }
                            motiv.on('selected', function() {

                                $rootScope.$broadcast('tools.motiv.selected', {
                                    deletable: element.deletable,
                                    resetable: element.resetable,
                                    motivUUID: element.id,
                                    motivWidth: element.width/site.scale,
                                    motivHeight: element.height/site.scale,
                                });
                            });

                            if(element.clipbox) {
                                motiv.clipBox = element.clipbox;
                                motiv.clipTo = function (ctx) {

                                    ctx.save();
                                    ctx.translate(0,0);
                                    ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                    //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                                    ctx.beginPath();
                                    ctx.rect(
                                        (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*this.siteScale))*-1/ this.getScaleX(),
                                        (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*this.siteScale))*-1/ this.getScaleX(),
                                        parseFloat(this.clipBox[2])*this.siteScale/ this.getScaleX(),
                                        parseFloat(this.clipBox[3])*this.siteScale/ this.getScaleX()
                                    );
                                    ctx.closePath();
                                    ctx.restore();
                                };

                            }
                            if(element.hideOnStart) {
                                motiv.visible = false;
                            }
                            if(element.hideOnUserInput) {
                                motiv.pos = 3.21;
                            }

                            if(element.hideTransform) {
                                motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});
                            }

                            if(element.passepartout) {
                                fabric.loadSVGFromURL(dataService.getRawPath(element.passepartout), function(objects, options) {

                                    var pass;
                                    if (objects[0].path) {
                                        pass = new fabric.Path(objects[0].d, {left: 0, top: 0});
                                    } else {
                                        pass = fabric.util.groupSVGElements(objects, options);
                                    }

                                    motiv.passepartout = pass;
                                    motiv.passepartoutOrg = {objects: objects, options: options, element: {value: element.passepartout, rel: element.passepartoutRel, x: element.passepartoutX, y: element.passepartoutY, width: element.passepartoutWidth, height: element.passepartoutHeight}};
                                    motiv.clipTo = function (ctx) {
                                        this.passepartout.top = 0;
                                        this.passepartout.left = 0;
                                        ctx.save();
                                        ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                        ctx.translate(0,0);
                                        ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(this.passepartoutOrg.element.x*site.scale))*-1/ this.getScaleX(),
                                            ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(this.passepartoutOrg.element.y*site.scale))*-1/ this.getScaleX());
                                        ctx.beginPath();
                                        this.passepartout.opacity=0;
                                        this.passepartout.scaleX = ((parseFloat(this.passepartoutOrg.element.width)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                                        this.passepartout.scaleY = ((parseFloat(this.passepartoutOrg.element.height)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                                        this.passepartout.render(ctx);
                                        ctx.closePath();
                                        ctx.restore();
                                    };

                                    paper.add(motiv);
                                });
                            }else {
                                paper.add(motiv);
                            }

                            cfpLoadingBar.complete();
                        });

                    }
                }

            },

            paintPreview: function(site, paper, element, x, y, width, height, selectable, deletable, resetable) {

                if(site.passepartout && element.json) {
                    fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                        var pass;
                        if(objects[0].path) {
                            pass = new fabric.Path(objects[0].d,  {left:0,top:0});
                        }else{
                            pass = fabric.util.groupSVGElements(objects, options);
                        }

                        fabric.Image.fromObject(element.json, function (img) {
                            img.clipTo = null;
                            var angle = img.angle;
                            img.angle = 0;
                            img.left = 100*img.left/site.width;
                            img.top = 100*img.top/site.width;
                            img.passepartout = pass;
                            img.siteScale = site.scale;
                            img.passepartoutOrg = {objects: objects, options: options};

                            img.scaleToWidth(100*img.width*img.scaleX / site.width);
                            img.angle = angle;

                            img.clipTo = function (ctx) {
                                this.passepartout.top = 0;
                                this.passepartout.left = 0;
                                ctx.save();
                                ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                ctx.translate(0,0);
                                ctx.translate(((this.getPointByOrigin("center", "center").x)-parseFloat(site.passepartoutX*site.scale))*-1/ this.getScaleX(),
                                    ((this.getPointByOrigin("center", "center").y)-parseFloat(site.passepartoutY*site.scale))*-1/ this.getScaleX());
                                ctx.beginPath();
                                this.passepartout.opacity=0;
                                this.passepartout.scaleX = ((parseFloat(site.passepartoutWidth)*site.scale/ this.getScaleX()+2)/this.passepartout.width*site.previewScale);
                                this.passepartout.scaleY = ((parseFloat(site.passepartoutHeight)*site.scale/ this.getScaleY()+2)/this.passepartout.height*site.previewScale);
                                this.passepartout.render(ctx);
                                ctx.closePath();
                                ctx.restore();
                            };

                            paper.add(img);

                            site.previewJson = paper.toJSON(['limitBoundary','templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'deletable', 'resetable', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);

                            site.previewImage = paper.toDataURL({format: "jpeg", quality: 8});
                        })
                    })
                }else{

                    if(element.json) {
                        fabric.Image.fromObject(element.json, function (img) {
                            img.clipTo = null;
                            var angle = img.angle;
                            img.angle = 0;
                            img.left = 100*(img.left-site.trimLeft)/site.width;
                            img.top = 100*(img.top-site.trimTop)/site.width;
                            img.scaleToWidth(element.json.width*site.scale);
                            img.clipBox = element.json.clipBox;
                            img.siteScale = site.scale;

                            img.scaleToWidth(100*img.width*img.scaleX / site.width);
                            img.angle = angle;
                            if(img.clipBox) {

                                img.clipTo = function (ctx) {

                                    ctx.save();
                                    ctx.rotate(this.getAngle()*Math.PI/180*-1);
                                    ctx.translate((this.getPointByOrigin("center", "center").x*-1/ this.getScaleX()),(this.getPointByOrigin("center", "center").y*-1/ this.getScaleY()));

                                    ctx.beginPath();

                                    ctx.rect(
                                        0,
                                        0,
                                        parseFloat(this.clipBox[2])/ this.getScaleX()*site.previewScale,
                                        parseFloat(this.clipBox[3])/ this.getScaleY()*site.previewScale
                                    );

                                    ctx.closePath();
                                    ctx.restore();
                                };

                            }

                            paper.add(img);
                        });
                    }else{
                        fabric.util.loadImage(element.value, function (img) {

                            var motiv = new fabric.Image(img, {
                                left: (100*x / site.width),
                                top: (100*y / site.width),
                                centeredRotating: true,
                                centeredScaling: true,
                                originX: 'left',
                                originY: 'top',
                                angle: element.rotate,
                                id: element.id,
                                orgLeft: x,
                                clipBox: element.clipbox,
                                orgTop: y,
                                siteScale: site.scale,
                                renderPreview: element.renderPreview,
                                renderPdf: element.renderPdf,
                                selectable: element.selectable,
                                deletable: element.deletable,
                                resetable: element.resetable,
                                evented: element.selectable,
                                pos: parseFloat(element.pos, 10)
                            });
                            motiv.scaleToWidth(100*width / site.width);

                            if(element.clipbox) {

                                motiv.clipTo = function (ctx) {
                                    ctx.save();
                                    ctx.translate(0,0);
                                    ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                    //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                                    ctx.beginPath();
                                    ctx.rect(
                                        (this.getPointByOrigin("center", "center").x-site.leftOffset-parseFloat(this.clipBox[0]))*-1/ this.getScaleX(),
                                        (this.getPointByOrigin("center", "center").y-site.topOffset-parseFloat(this.clipBox[1]))*-1/ this.getScaleX(),
                                        parseFloat(this.clipBox[2])*this.siteScale/ this.getScaleX(),
                                        parseFloat(this.clipBox[3])*this.siteScale/ this.getScaleX()
                                    );
                                    ctx.closePath();
                                    ctx.restore();
                                };




                            }
                            if(element.hideOnStart) {
                                motiv.visible = false;
                            }
                            if(element.hideOnUserInput) {
                                motiv.pos = 3.21;
                            }

                            if(element.passepartout) {
                                fabric.loadSVGFromURL(dataService.getRawPath(element.passepartout), function(objects, options) {

                                    var pass;
                                    if (objects[0].path) {
                                        pass = new fabric.Path(objects[0].d, {left: 0, top: 0});
                                    } else {
                                        pass = fabric.util.groupSVGElements(objects, options);
                                    }
                                    motiv.passepartout = pass;

                                    motiv.passepartoutOrg = {objects: objects, options: options, element: {value: element.passepartout, rel: element.passepartoutRel, x: element.passepartoutX, y: element.passepartoutY, width: element.passepartoutWidth, height: element.passepartoutHeight}};
                                    motiv.clipTo = function (ctx) {
                                        this.passepartout.top = 0;
                                        this.passepartout.left = 0;
                                        ctx.save();
                                        ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                        ctx.translate(0,0);
                                        ctx.translate(((this.getPointByOrigin("center", "center").x)-parseFloat(site.passepartoutX*site.scale))*-1/ this.getScaleX(),
                                            ((this.getPointByOrigin("center", "center").y)-parseFloat(site.passepartoutY*site.scale))*-1/ this.getScaleX());
                                        ctx.beginPath();
                                        this.passepartout.opacity=0;
                                        this.passepartout.scaleX = ((parseFloat(site.passepartoutWidth)*site.scale/ this.getScaleX()+2)/this.passepartout.width*site.previewScale);
                                        this.passepartout.scaleY = ((parseFloat(site.passepartoutHeight)*site.scale/ this.getScaleY()+2)/this.passepartout.height*site.previewScale);
                                        this.passepartout.render(ctx);
                                        ctx.closePath();
                                        ctx.restore();
                                    };
                                    paper.add(motiv);
                                    site.previewJson = paper.toJSON(['limitBoundary','templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'deletable', 'resetable', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);

                                    site.previewImage = paper.toDataURL({format: "jpeg", quality: 8});
                                });
                            }else{
                                paper.add(motiv);
                                site.previewJson = paper.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'deletable', 'resetable', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);

                                site.previewImage = paper.toDataURL({format: "jpeg", quality: 8});
                            }
                        });
                    }

                }
            }

        });
        angular.extend(this, data);
    };
    return SiteElementImage;

}])