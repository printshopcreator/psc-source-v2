angular.module('layouterApp.elements', []).factory('SiteElementCal', [
  'dataService',
  function (dataService) {
    var SiteElementCal = function (data) {
      angular.extend(this, {
        pos: 0,
        type: 'cal',
        x: 0,
        y: 0,
        id: '',
        width: 0,
        height: 0,
        label: '',
        month: false,
        limitBoundary: false,
        paint: function ($rootScope, site, paper, element, x, y, width, height, selectable) {
          fabric.util.loadImage(dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height, site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1, 7), site.calendarSetting.dayColor.value.substring(1, 7), site.calendarSetting.sunColor.value.substring(1, 7), site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal, site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY, site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily), function (img) {
            var motiv = new fabric.Image(img, {
                originX: 'left',
                originY: 'top',
                left: site.leftOffset + x * site.scale,
                top: site.topOffset + y * site.scale,
                centeredRotating: true,
                centeredScaling: true,
                id: element.id,
                selectable: false,
                orgLeft: x,
                orgTop: y,
                siteScale: site.scale,
                renderPreview: element.renderPreview,
                renderPdf: element.renderPdf,
                pos: parseFloat(element.pos, 10)
              });
            motiv.scaleToWidth(width * site.scale);
            paper.add(motiv);
          });
        },
        paintPreview: function (site, paper, element, x, y, width, height, selectable) {
          fabric.util.loadImage(dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height, site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1, 7), site.calendarSetting.dayColor.value.substring(1, 7), site.calendarSetting.sunColor.value.substring(1, 7), site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal, site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY, site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily), function (img) {
            var motiv = new fabric.Image(img, {
                left: 100 * x / site.width,
                top: 100 * y / site.width,
                centeredRotating: true,
                centeredScaling: true,
                originX: 'left',
                originY: 'top',
                orgLeft: x,
                id: element.id,
                orgTop: y,
                siteScale: site.scale,
                renderPreview: element.renderPreview,
                renderPdf: element.renderPdf,
                selectable: false,
                pos: parseFloat(element.pos, 10)
              });
            motiv.scaleToWidth(100 * width / site.width);
            paper.add(motiv);
            site.previewJson = paper.toJSON([
              'limitBoundary',
              'templatePrintBinding',
              'passepartoutOrg',
              'id',
              'renderPreview',
              'renderPdf',
              'passepartout',
              'centeredScaling',
              'centeredRotation',
              'fontFamily',
              'originX',
              'originY',
              'siteScale',
              'clipBox',
              'calcWidth',
              'name',
              'transformMatrix',
              'currentWidth',
              'currentHeight',
              'style',
              'height',
              'width',
              'orgLeft',
              'orgTop',
              'pos',
              'scaleX',
              'scaleY',
              'angle',
              'selectable',
              'lockMovementX',
              'lockMovementY',
              'lockRotation',
              'lockScalingX',
              'lockScalingY',
              'lockUniScaling'
            ]);
            site.previewImage = paper.toDataURL({
              format: 'jpeg',
              quality: 8
            });
          });
        }
      });
      angular.extend(this, data);
    };
    return SiteElementCal;
  }
]);