angular.module( 'layouterApp.painter', [

])

.config(function config( ) {
})

.service('painterManager', ['_', '$rootScope', 'sitesManager', 'previewManager', 'dataService', 'cfpLoadingBar','SiteElementSVG',function(_, $rootScope, sitesManager, previewManager, dataService, cfpLoadingBar,SiteElementSVG) {
    var Painter = {
        canvas: null,
        paper: null,
        site: null,
        haveStatic: true,

        create: function(site) {
            this.createCanvas(site);
            $rootScope.$broadcast('sites.selected', site);
            $rootScope.$broadcast('sites.added', site);
        },

        createLoaded: function(site) {
            if(site.id === 0) {
                if(site.fabricJson !== null) {
                    this.createFromBasket(site);
                }else{
                    this.createCanvas(site);
                }
                $rootScope.$broadcast('sites.selected', site);
            }

            $rootScope.$broadcast('sites.added', site);

        },

        changeTextForDataTemplate: function(item) {

            angular.forEach(this.paper.getObjects(),function(obj, i){
                if(obj.templatePrintBinding) {
                    var str = obj.templatePrintBinding;

                    _.mapObject(item, function(val, key) {
                        str = str.replace('{{', '');
                        str = str.replace('}}', '');
                        str = str.replace('row.' + key, val);
                    });

                    obj.setText(str);
                }
            });
            var paper = this.paper;
            var site = this.site;

            paper.renderAll();
            previewManager.previewLoaded(site);

        },

        changeText: function(index, text) {
            var element = false;

            angular.forEach(this.paper.getObjects(),function(obj, i){
                if(obj.id == index) {
                    element = obj;
                }
            });

            if(element === false) {
                return;
            }

            var paper = this.paper;
            var site = this.site;

            element.setText(text);

            paper.renderAll();
            previewManager.previewLoaded(site);
        },

        downloadMotiv: function() {
            var element = this.paper.getActiveObject();
            if(element) {
                if(element.src) {
                    window.open(element.src.replace("mid", "download").replace("raw", "download"));
                }
            }
        },

        changeMotiv: function(motiv, resize) {

            var element = this.paper.getActiveObject();

            var paper = this.paper;
            var site = this.site;


            fabric.util.loadImage(motiv, function (img) {
                element.setElement(img);
                element.setCoords();
                if(resize) {
                    if (element.clipBox) {
                        element.scaleToWidth((element.clipBox[2]) * site.scale);
                    } else {
                        element.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                    }
                }
                paper.renderAll();
                previewManager.previewLoaded(site);
            });


        },

        changeSvgById: function(index, motiv, success) {

            var element;

            angular.forEach(this.paper.getObjects(),function(obj, i){
                if(obj.id == index) {
                    element = obj;
                }
            });

            var paper = this.paper;
            var site = this.site;
            var elementSettings = element.elementSettings;
            element.remove();
            var x = elementSettings.x;
            var y = elementSettings.y;
            var width = elementSettings.width;
            var height = elementSettings.height;
            var selectable = elementSettings.selectable;
            var element = elementSettings.element;
            element.value = motiv;

            ele = new SiteElementSVG();
            ele.paint($rootScope, site, paper, element, x, y, width, height, selectable, success);

            paper.renderAll();
            previewManager.previewLoaded(site);

        },

        changeMotivById: function(index, motiv) {

            var element;

            angular.forEach(this.paper.getObjects(),function(obj, i){
                if(obj.id == index) {
                    element = obj;
                }
            });

            var paper = this.paper;
            var site = this.site;

            fabric.util.loadImage(motiv, function (img) {
                element.setElement(img);
                element.setCoords();
                if(element.clipBox) {
                    element.scaleToWidth((element.clipBox[2]) * site.scale);
                }else{
                    element.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                }
                paper.renderAll();
                previewManager.previewLoaded(site);
            });


        },

        changeMotivWithoutResizeById: function(index, motiv) {

            var element;

            angular.forEach(this.paper.getObjects(),function(obj, i){
                if(obj.id == index) {
                    element = obj;
                }
            });

            var paper = this.paper;
            var site = this.site;

            fabric.util.loadImage(motiv, function (img) {
                var orgWidth = element.width;

                element.setElement(img);
                element.setCoords();
                element.scaleToWidth(orgWidth * site.scale);
                paper.renderAll();
                previewManager.previewLoaded(site);
            });


        },

        showMotivById: function(index) {

            var element;

            angular.forEach(this.paper.getObjects(),function(obj, i){
                if(obj.id == index) {
                    element = obj;
                }
            });

            element.visible = true;
            this.paper.renderAll();

        },

        hideMotivById: function(index) {

            var element;

            angular.forEach(this.paper.getObjects(),function(obj, i){
                if(obj.id == index) {
                    element = obj;
                }
            });

            element.visible = false;

            this.paper.renderAll();
        },

        addText: function() {
            this.removeStatic();
            var paper = this.paper;
            var site = this.site;

            var leftOffset = 0;
            var topOffset = 0;

            if(site.settingsDefaults.createText.offset) {
                leftOffset = parseFloat(site.settingsDefaults.createText.offset[0]);
                topOffset = parseFloat(site.settingsDefaults.createText.offset[1]);
            }

            if(site.passepartout) {
                fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {
                    var pass;
                    pass = fabric.util.groupSVGElements(objects, options);

                    site.settingsDefaults.createText.pos++;

                    var fontSize = 20;
                    if(site.width > 300) {
                        fontSize = 40;
                    }

                    var text = new fabric.IText('Dein Text', {
                        left: (site.leftOffset+(site.width/2+leftOffset)*site.scale),
                        top: (site.topOffset+(site.height/2+topOffset)*site.scale),
                        fontSize: fontSize,
                        originX: 'center',
                        centeredScaling: true,
                        centeredRotation: true,
                        scaleX: site.scale,
                        scaleY: site.scale,
                        renderPreview: true,
                        renderPdf: true,
                        fontFamily: dataService.initalFont.name,
                        lockScalingX: true,
                        lockScalingY: true,
                        selectable: true,
                        color: "#000000",
                        pos: parseFloat(site.settingsDefaults.createText.pos, 10),
                        passepartout: pass,
                        passepartoutOrg: {objects: objects, options: options, element: {value: site.passepartout, x: site.passepartoutX, y: site.passepartoutY, width: site.passepartoutWidth, height: site.passepartoutHeight}}
                    });
                    text.siteScale=site.scale;
                    text.setControlsVisibility({bl:false,br:false,ml:false,mr:false,mb:false,mt:false,tl:false,tr:false});

                    text.on('selected', function() {
                        var fontSize = this.fontSize;
                        var color = this.fill;
                        var fontFamily = this.fontFamily;
                        var align = this.textAlign;
                        var bold = this.fontWeight;
                        var italic = this.fontStyle;
                        var underline = this.textDecoration;
                        var text = this.text;

                        $rootScope.$broadcast('tools.text.selected', {
                            fontSize: fontSize,
                            fontFamily: fontFamily,
                            color: color,
                            align: align,
                            underline: underline,
                            italic: italic,
                            bold: bold,
                            text: text
                        });
                    });

                    text.clipTo = function (ctx) {
                        this.passepartout.top = 0;
                        this.passepartout.left = 0;
                        ctx.save();
                        ctx.rotate(this.getAngle()*-1/180*Math.PI);
                        ctx.translate(0,0);
                        ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(this.passepartoutOrg.element.x*site.scale))*-1/ this.getScaleX(),
                            ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(this.passepartoutOrg.element.y*site.scale))*-1/ this.getScaleX());
                        ctx.beginPath();
                        this.passepartout.opacity=0;
                        this.passepartout.scaleX = ((parseFloat(this.passepartoutOrg.element.width)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                        this.passepartout.scaleY = ((parseFloat(this.passepartoutOrg.element.height)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                        this.passepartout.render(ctx);
                        ctx.closePath();
                        ctx.restore();
                    };


                    paper.add(text);
                    paper.renderAll();
                    previewManager.previewLoaded(site);
                    paper.setActiveObject(text);
                });
            }else{

                var fontSize = 20;
                if(site.width > 300) {
                    fontSize = 40;
                }

                var text = new fabric.IText('Dein Text', {
                    left: (this.site.leftOffset+(site.width/2+leftOffset)*site.scale),
                    top: (this.site.topOffset+(site.height/2+topOffset)*site.scale),
                    fontSize: fontSize,
                    fontFamily: dataService.initalFont.name,
                    centeredScaling: true,
                    centeredRotation: true,
                    renderPreview: true,
                    renderPdf: true,
                    selectable: true,
                    lockScalingX: true,
                    originX: 'center',
                    lockScalingY: true,
                    color: "#000000",
                    pos: parseFloat(site.settingsDefaults.createText.pos, 10)
                });
                text.siteScale=site.scale;
                text.setControlsVisibility({bl:false,br:false,ml:false,mr:false,mb:false,mt:false,tl:false,tr:false});

                if(site.settingsDefaults.createText.clipBox) {
                    text.clipBox = site.settingsDefaults.createText.clipBox;
                    text.clipTo = function (ctx) {

                        ctx.save();
                        ctx.translate(0,0);
                        ctx.rotate(this.getAngle()*-1/180*Math.PI);
                        //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                        ctx.beginPath();
                        ctx.rect(
                            (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                            (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleY(),
                            parseFloat(this.clipBox[2])*site.scale/ this.getScaleX(),
                            parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()
                        );

                        ctx.closePath();
                        ctx.restore();
                    };

                };

                text.on('selected', function() {
                    var fontSize = this.fontSize;
                    var color = this.fill;
                    var fontFamily = this.fontFamily;
                    var align = this.textAlign;
                    var bold = this.fontWeight;
                    var italic = this.fontStyle;
                    var underline = this.textDecoration;
                    var text = this.text;

                    $rootScope.$broadcast('tools.text.selected', {
                        fontSize: fontSize,
                        fontFamily: fontFamily,
                        color: color,
                        align: align,
                        underline: underline,
                        italic: italic,
                        bold: bold,
                        text: text
                    });
                });

                this.paper.add(text);
                this.paper.renderAll();
                previewManager.previewLoaded(this.site);
                this.paper.setActiveObject(text);
            }

            site.settingsDefaults.createText.pos++;
        },

        addMotiv: function(motiv, x = 0, y= 0, width = 0, height = 0) {
            this.removeStatic();
            var paper = this.paper;
            var site = this.site;

            var leftOffset = 0;
            var topOffset = 0;

            if(site.settingsDefaults.createMotiv.offset) {
                leftOffset = parseFloat(site.settingsDefaults.createMotiv.offset[0]);
                topOffset = parseFloat(site.settingsDefaults.createMotiv.offset[1]);
            }

            leftOffset += x;
            topOffset += y;

            if(site.passepartout) {
                fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                    var pass;
                    pass = fabric.util.groupSVGElements(objects, options);

                    fabric.util.loadImage(motiv, function (img) {
                        var motiv = new fabric.Image(img, {
                            left: (site.leftOffset) + leftOffset - (site.trimLeft*site.scale),
                            top: (site.topOffset) + topOffset - (site.trimTop*site.scale),
                            centeredRotating: true,
                            centeredScaling: true,
                            selectable: true,
                            renderPdf: true,
                            renderPreview: true,
                            pos: parseFloat(site.settingsDefaults.createMotiv.pos, 10),
                            angle: 0,
                            orgLeft: 20,
                            orgTop: 20,
                            passepartout: pass,
                            passepartoutOrg: {objects: objects, options: options, element: {value: site.passepartout, x: site.passepartoutX, y: site.passepartoutY, width: site.passepartoutWidth, height: site.passepartoutHeight}}
                        });
                        if(site.settingsDefaults.createMotiv.clipBox) {
                            motiv.scaleToWidth((this.clipBox[2]) * site.scale);
                        }else{
                            motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                        }

                        motiv.on('selected', function() {

                            $rootScope.$broadcast('tools.motiv.selected', {
                            });
                        });
                        motiv.clipTo = function (ctx) {
                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(this.passepartoutOrg.element.x*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(this.passepartoutOrg.element.y*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(this.passepartoutOrg.element.width)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                            this.passepartout.scaleY = ((parseFloat(this.passepartoutOrg.element.height)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };
                        motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});
                        paper.add(motiv);
                        paper.renderAll();
                        previewManager.previewLoaded(site);
                    });
                });
            }else{
                fabric.Image.fromURL(motiv, function (motiv) {
                    motiv.left = site.leftOffset + ((leftOffset - site.trimLeft) * site.scale);
                    motiv.top = site.topOffset + ((topOffset - site.trimTop) * site.scale);
                    motiv.centeredRotating = true;
                    motiv.centeredScaling = true;
                    motiv.selectable = true;
                    motiv.angle = 0;
                    motiv.orgLeft = 20;
                    motiv.orgTop = 20;
                    motiv.renderPreview = true;
                    motiv.renderPdf = true;
                    motiv.pos = parseFloat(site.settingsDefaults.createMotiv.pos, 10);

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.scaleToWidth((site.settingsDefaults.createMotiv.clipBox[2]) * site.scale);
                    }else{
                        if(width != 0) {
                            motiv.scaleToWidth((site.trimLeft + site.trimRight + width) * site.scale);
                        }else{
                            motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                        }

                    }

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.clipBox = site.settingsDefaults.createMotiv.clipBox;

                        motiv.clipTo = function (ctx) {

                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleY(),
                                parseFloat(this.clipBox[2])*site.scale/ this.getScaleX(),
                                parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()
                            );

                            ctx.closePath();
                            ctx.restore();
                        };

                    };

                    motiv.on('selected', function() {

                        $rootScope.$broadcast('tools.motiv.selected', {
                        });
                    });
                    motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});

                    paper.add(motiv);
                    paper.renderAll();
                    previewManager.previewLoaded(site);
                });
            }
            site.settingsDefaults.createMotiv.pos++;
        },

        addClipart: function(motiv) {
            this.removeStatic();
            var paper = this.paper;
            var site = this.site;

            var leftOffset = 0;
            var topOffset = 0;

            if(site.settingsDefaults.createClipart.offset) {
                leftOffset = parseFloat(site.settingsDefaults.createClipart.offset[0]);
                topOffset = parseFloat(site.settingsDefaults.createClipart.offset[1]);
            }

            if(site.passepartout) {
                fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                    var pass;
                    pass = fabric.util.groupSVGElements(objects, options);

                    fabric.util.loadImage(motiv, function (img) {
                        var motiv = new fabric.Image(img, {
                            left: (site.leftOffset) + leftOffset - (site.trimLeft*site.scale),
                            top: (site.topOffset) + topOffset - (site.trimTop*site.scale),
                            centeredRotating: true,
                            centeredScaling: true,
                            selectable: true,
                            renderPdf: true,
                            renderPreview: true,
                            pos: parseFloat(site.settingsDefaults.createClipart.pos, 10),
                            angle: 0,
                            orgLeft: 20,
                            orgTop: 20,
                            passepartout: pass,
                            passepartoutOrg: {objects: objects, options: options, element: {value: site.passepartout, x: site.passepartoutX, y: site.passepartoutY, width: site.passepartoutWidth, height: site.passepartoutHeight}}
                        });
                        if(site.settingsDefaults.createMotiv.clipBox) {
                            motiv.scaleToWidth((this.clipBox[2]) * site.scale);
                        }else{
                            motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                        }

                        motiv.on('selected', function() {

                            $rootScope.$broadcast('tools.motiv.selected', {
                            });
                        });
                        motiv.clipTo = function (ctx) {
                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(this.passepartoutOrg.element.x*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(this.passepartoutOrg.element.y*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(this.passepartoutOrg.element.width)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                            this.passepartout.scaleY = ((parseFloat(this.passepartoutOrg.element.height)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };
                        motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});
                        paper.add(motiv);
                        paper.renderAll();
                        previewManager.previewLoaded(site);
                    });
                });
            }else{
                fabric.Image.fromURL(motiv, function (motiv) {
                    motiv.left = (site.leftOffset) + leftOffset - (site.trimLeft*site.scale);
                    motiv.top = (site.topOffset) + topOffset - (site.trimTop*site.scale);
                    motiv.centeredRotating = true;
                    motiv.centeredScaling = true;
                    motiv.selectable = true;
                    motiv.angle = 0;
                    motiv.orgLeft = 20;
                    motiv.orgTop = 20;
                    motiv.renderPreview = true;
                    motiv.renderPdf = true;
                    motiv.pos = parseFloat(site.settingsDefaults.createClipart.pos, 10);

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.scaleToWidth((site.settingsDefaults.createMotiv.clipBox[2]) * site.scale);
                    }else{
                        motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                    }

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.clipBox = site.settingsDefaults.createMotiv.clipBox;

                        motiv.clipTo = function (ctx) {

                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleY(),
                                parseFloat(this.clipBox[2])*site.scale/ this.getScaleX(),
                                parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()
                            );

                            ctx.closePath();
                            ctx.restore();
                        };

                    };

                    motiv.on('selected', function() {

                        $rootScope.$broadcast('tools.motiv.selected', {
                        });
                    });
                    motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});

                    paper.add(motiv);
                    paper.renderAll();
                    previewManager.previewLoaded(site);
                });
            }

            site.settingsDefaults.createClipart.pos++;
        },

        addClipart: function(motiv) {
            this.removeStatic();
            var paper = this.paper;
            var site = this.site;

            var leftOffset = 0;
            var topOffset = 0;

            if(site.settingsDefaults.createClipart.offset) {
                leftOffset = parseFloat(site.settingsDefaults.createClipart.offset[0]);
                topOffset = parseFloat(site.settingsDefaults.createClipart.offset[1]);
            }

            if(site.passepartout) {
                fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                    var pass;
                    pass = fabric.util.groupSVGElements(objects, options);

                    fabric.util.loadImage(motiv, function (img) {
                        var motiv = new fabric.Image(img, {
                            left: (site.leftOffset) + leftOffset - (site.trimLeft*site.scale),
                            top: (site.topOffset) + topOffset - (site.trimTop*site.scale),
                            centeredRotating: true,
                            centeredScaling: true,
                            selectable: true,
                            renderPdf: true,
                            renderPreview: true,
                            pos: parseFloat(site.settingsDefaults.createClipart.pos, 10),
                            angle: 0,
                            orgLeft: 20,
                            orgTop: 20,
                            passepartout: pass,
                            passepartoutOrg: {objects: objects, options: options, element: {value: site.passepartout, x: site.passepartoutX, y: site.passepartoutY, width: site.passepartoutWidth, height: site.passepartoutHeight}}
                        });
                        if(site.settingsDefaults.createMotiv.clipBox) {
                            motiv.scaleToWidth((this.clipBox[2]) * site.scale);
                        }else{
                            motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                        }

                        motiv.on('selected', function() {

                            $rootScope.$broadcast('tools.motiv.selected', {
                            });
                        });
                        motiv.clipTo = function (ctx) {
                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(this.passepartoutOrg.element.x*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(this.passepartoutOrg.element.y*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(this.passepartoutOrg.element.width)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                            this.passepartout.scaleY = ((parseFloat(this.passepartoutOrg.element.height)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };
                        motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});
                        paper.add(motiv);
                        paper.renderAll();
                        previewManager.previewLoaded(site);
                    });
                });
            }else{
                fabric.Image.fromURL(motiv, function (motiv) {
                    motiv.left = (site.leftOffset) + leftOffset - (site.trimLeft*site.scale);
                    motiv.top = (site.topOffset) + topOffset - (site.trimTop*site.scale);
                    motiv.centeredRotating = true;
                    motiv.centeredScaling = true;
                    motiv.selectable = true;
                    motiv.angle = 0;
                    motiv.orgLeft = 20;
                    motiv.orgTop = 20;
                    motiv.renderPreview = true;
                    motiv.renderPdf = true;
                    motiv.pos = parseFloat(site.settingsDefaults.createClipart.pos, 10);

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.scaleToWidth((site.settingsDefaults.createMotiv.clipBox[2]) * site.scale);
                    }else{
                        motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                    }

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.clipBox = site.settingsDefaults.createMotiv.clipBox;

                        motiv.clipTo = function (ctx) {

                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleY(),
                                parseFloat(this.clipBox[2])*site.scale/ this.getScaleX(),
                                parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()
                            );

                            ctx.closePath();
                            ctx.restore();
                        };

                    };

                    motiv.on('selected', function() {

                        $rootScope.$broadcast('tools.motiv.selected', {
                        });
                    });
                    motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});

                    paper.add(motiv);
                    paper.renderAll();
                    previewManager.previewLoaded(site);
                });

            }

            site.settingsDefaults.createClipart.pos++;
        },

        addSvg: function(motiv) {
            this.removeStatic();
            var paper = this.paper;
            var site = this.site;

            var leftOffset = 0;
            var topOffset = 0;

            if(site.settingsDefaults.createClipart.offset) {
                leftOffset = parseFloat(site.settingsDefaults.createClipart.offset[0]);
                topOffset = parseFloat(site.settingsDefaults.createClipart.offset[1]);
            }

            if(site.passepartout) {
                fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                    var pass;
                    pass = fabric.util.groupSVGElements(objects, options);

                    fabric.util.loadImage(motiv, function (img) {
                        var motiv = new fabric.Image(img, {
                            left: (site.leftOffset) + leftOffset - (site.trimLeft*site.scale),
                            top: (site.topOffset) + topOffset - (site.trimTop*site.scale),
                            centeredRotating: true,
                            centeredScaling: true,
                            selectable: true,
                            renderPdf: true,
                            renderPreview: true,
                            pos: parseFloat(site.settingsDefaults.createClipart.pos, 10),
                            angle: 0,
                            orgLeft: 20,
                            orgTop: 20,
                            passepartout: pass,
                            passepartoutOrg: {objects: objects, options: options, element: {value: site.passepartout, x: site.passepartoutX, y: site.passepartoutY, width: site.passepartoutWidth, height: site.passepartoutHeight}}
                        });
                        if(site.settingsDefaults.createMotiv.clipBox) {
                            motiv.scaleToWidth((this.clipBox[2]) * site.scale);
                        }else{
                            motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                        }

                        motiv.on('selected', function() {

                            $rootScope.$broadcast('tools.motiv.selected', {
                            });
                        });
                        motiv.clipTo = function (ctx) {
                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(this.passepartoutOrg.element.x*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(this.passepartoutOrg.element.y*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(this.passepartoutOrg.element.width)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                            this.passepartout.scaleY = ((parseFloat(this.passepartoutOrg.element.height)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };
                        motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});
                        paper.add(motiv);
                        paper.renderAll();
                        previewManager.previewLoaded(site);
                    });
                });
            }else{
                fabric.loadSVGFromURL(motiv, function(objects, options) {

                    var shape = fabric.util.groupSVGElements(objects, options);
                    shape.left = (site.leftOffset) + leftOffset - (site.trimLeft*site.scale);
                    shape.top = (site.topOffset) + topOffset - (site.trimTop*site.scale);
                    shape.siteScale = site.scale;
                    shape.originX = 'left';
                    shape.originY = 'top';
                    shape.selectable = true;
                    shape.evented = true;
                    if(site.settingsDefaults.createMotiv.clipBox) {
                        shape.scaleToWidth((site.settingsDefaults.createMotiv.clipBox[2]) * site.scale);
                    }else{
                        shape.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                    }
                    shape.pos = parseFloat(site.settingsDefaults.createClipart.pos, 10);
                    shape.renderPreview = true;
                    shape.renderPdf = true;

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        shape.clipBox = site.settingsDefaults.createMotiv.clipBox;

                        shape.clipTo = function (ctx) {

                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleY(),
                                parseFloat(this.clipBox[2])*site.scale/ this.getScaleX(),
                                parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()
                            );

                            ctx.closePath();
                            ctx.restore();
                        };

                    }

                    shape.on('selected', function() {

                        $rootScope.$broadcast('tools.motiv.selected', {
                        });
                    });
                    paper.add(shape);
                    paper.renderAll();
                    previewManager.previewLoaded(site);
                });

                /*
                fabric.Image.fromURL(motiv, function (motiv) {
                    motiv.left = (site.leftOffset) + leftOffset - (site.trimLeft*site.scale);
                    motiv.top = (site.topOffset) + topOffset - (site.trimTop*site.scale);
                    motiv.centeredRotating = true;
                    motiv.centeredScaling = true;
                    motiv.selectable = true;
                    motiv.angle = 0;
                    motiv.orgLeft = 20;
                    motiv.orgTop = 20;
                    motiv.renderPreview = true;
                    motiv.renderPdf = true;
                    motiv.pos = parseFloat(site.settingsDefaults.createClipart.pos, 10);

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.scaleToWidth((site.settingsDefaults.createMotiv.clipBox[2]) * site.scale);
                    }else{
                        motiv.scaleToWidth((site.trimLeft + site.trimRight + site.width) * site.scale);
                    }

                    if(site.settingsDefaults.createMotiv.clipBox) {
                        motiv.clipBox = site.settingsDefaults.createMotiv.clipBox;

                        motiv.clipTo = function (ctx) {

                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleY(),
                                parseFloat(this.clipBox[2])*site.scale/ this.getScaleX(),
                                parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()
                            );

                            ctx.closePath();
                            ctx.restore();
                        };

                    };

                    motiv.on('selected', function() {

                        $rootScope.$broadcast('tools.motiv.selected', {
                        });
                    });
                    motiv.setControlsVisibility({bl:true,br:true,ml:false,mr:false,mb:false,mt:false,tl:true,tr:true});

                    paper.add(motiv);
                    paper.renderAll();
                    previewManager.previewLoaded(site);
                });
                 */
            }

            site.settingsDefaults.createClipart.pos++;
        },

        select: function(site) {
            this.site = site;
            if(site.fabricJson == null) {
                this.createCanvas(site);

                return;
            }

            if(site.fabricObj == null) {
                this.createFromBasket(site);

                return;
            }

            angular.forEach(sitesManager.sites, function(site, key){
                site.convertFabricToJson();
            });

            this.canvas = null;
            this.paper = null;
            var viewportWidth = window.innerWidth*1.4;
            var viewportHeight = window.innerHeight*1.4;

            if(dataService.theme) {
                $("#c").mCustomScrollbar("destroy");
                viewportWidth = jQuery(".desk").width()*2.5;
                viewportHeight = jQuery(".desk").height()*2.5;
            }


            if(site.height*1.4 > window.innerHeight*1.4 && !dataService.theme) {
                viewportWidth = window.innerWidth*1.6;
                viewportHeight = window.innerHeight*1.6;
            }


            if(site.height > ((window.innerHeight)-200) && !dataService.theme) {
                //site.scale = 0.4;
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            if(dataService.theme && site.maxHeight >= site.maxWidth) {
                var calcHight = jQuery(".desk").height()-280;
                //site.scale = window.math.round(calcHight/site.maxHeight, 2);
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            if(dataService.theme && site.maxHeight < site.maxWidth) {
                var calcWidth = jQuery(".desk").width()-280;
                //site.scale = window.math.round(calcWidth/site.maxWidth, 2);

                if((jQuery(".desk").height()-280) < (site.height*site.scale)) {
                    //site.scale = window.math.round((jQuery(".desk").height()-280)/site.maxHeight, 2);
                }
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            jQuery("#canvas").remove();
            canvasElement = document.getElementById("c");
            canvasElement.innerHTML = '<canvas id="canvas"></canvas>';

            this.currCanvas = document.getElementById('canvas');
            var currCtx = this.currCanvas.getContext('2d');

            this.currCanvas.style.position = "fixed";
            jQuery("#canvas").width(viewportWidth);
            jQuery("#canvas").height(viewportHeight);
            this.currCanvas.setAttribute("width", viewportWidth);
            this.currCanvas.setAttribute("height", viewportHeight);
            this.currCanvas.style.top = 0;
            this.currCanvas.style.left = 0;


            var siteWidth = (site.width+site.trimLeft+site.trimRight)*site.scale;
            var siteHeight = (site.height+site.trimTop+site.trimBottom)*site.scale;

            var leftOffset = viewportWidth /2 - siteWidth/2;
            var topOffset = viewportHeight /2 - siteHeight/2;
            site.calcWidth = siteWidth;

            this.paper = new fabric.Canvas(document.getElementById('canvas'), { stateful : true });
            this.paper.preserveObjectStacking = true;

            var paperObj = this.paper;
            if(!dataService.theme) {
                jQuery('#c').scrollLeft((viewportWidth-window.innerWidth)/2);
                jQuery('#c').scrollTop((viewportHeight-window.innerHeight)/2);
            }else{
                jQuery('#c').height(jQuery(".desk").height()-180);
                $("#c").mCustomScrollbar({
                    axis:"yx",
                    alwaysShowScrollbar: true,
                    mouseWheel: false,
                    theme:'inset-3-dark',
                    callbacks:{
                        onScroll:function(){
                            paperObj.calcOffset();
                        }
                    }
                });


                jQuery(".mCSB_container").css('left', ((jQuery(".mCSB_container").width()-jQuery(".mCustomScrollbar").width())/2)*-1);
                jQuery(".mCSB_container").css('top', ((jQuery(".mCSB_container").height()-jQuery(".mCustomScrollbar").height())/2)*-1);

                if(site.width * site.scale > jQuery("._mCS_1").width() || site.height * site.scale > jQuery("._mCS_1").height()) {
                    jQuery('#c').mCustomScrollbar('update');
                }else {
                    jQuery('#c').mCustomScrollbar('disable');
                }
            }
            this.paper.off('object:modified');
            this.paper.on('object:modified', function() {
                previewManager.previewLoaded(site);
            });

            this.paper.off('selection:cleared');
            this.paper.on('selection:cleared', function() {
                $rootScope.$broadcast('tools.hide');
            });

            paper = this.paper;

            if(!dataService.theme) {
                jQuery('#c').scroll(function(){
                    paperObj.calcOffset();
                });
            }


            this.paper.on('after:render', function() {
                cfpLoadingBar.complete();
                var objects = this.getObjects();

                if(objects.length > 0) {
                    this.off('after:render');
                    this.off('object:added');
                    angular.forEach(objects,function(obj, i){
                        if (obj.name != "left" &&
                            obj.name != "right" &&
                            obj.name != "top" &&
                            obj.name != "bottom") {

                            if(obj.id == "background-color") {
                                obj.fill = site.backgroundColor;
                            }

                            if(obj.id == "calendar") {
                                fabric.util.loadImage(
                                    dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height,
                                        site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1,7),
                                        site.calendarSetting.dayColor.value.substring(1,7), site.calendarSetting.sunColor.value.substring(1,7),
                                        site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal,
                                        site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY,
                                        site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily
                                ), function (img) {
                                    obj.setElement(img);
                                });
                            }

                            if(obj.fontFamily) {

                                obj.on('selected', function() {

                                    var fontSize = this.fontSize;
                                    var color = this.fill;
                                    var fontFamily = this.fontFamily;
                                    var align = this.textAlign;
                                    var bold = this.fontWeight;
                                    var italic = this.fontStyle;
                                    var underline = this.textDecoration;
                                    var text = this.text;

                                    $rootScope.$broadcast('tools.text.selected', {
                                        fontSize: fontSize,
                                        fontFamily: fontFamily,
                                        color: color,
                                        align: align,
                                        underline: underline,
                                        italic: italic,
                                        bold: bold,
                                        text: text
                                    });
                                });
                            }else{
                                obj.on('selected', function() {

                                    $rootScope.$broadcast('tools.motiv.selected', {
                                    });
                                });
                            }

                            if(obj.clipBox) {

                                obj.clipTo = function(ctx) {
                                    ctx.save();
                                    ctx.translate(0,0);
                                    ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                    //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                                    ctx.beginPath();
                                    ctx.rect(
                                        (((this.getPointByOrigin("center", "center").x-site.leftOffset)*-1)+(parseFloat(this.clipBox[0])*site.scale))/ this.getScaleX(),
                                        (((this.getPointByOrigin("center", "center").y-site.topOffset)*-1)+(parseFloat(this.clipBox[1])*site.scale))/ this.getScaleX(),
                                        parseFloat(this.clipBox[2])*site.scale/ this.getScaleX()+2,
                                        parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()+2
                                    );

                                    ctx.closePath();
                                    ctx.restore();
                                };

                            }

                            if(obj.passepartoutOrg && obj.passepartoutOrg.element && obj.passepartout && obj.passepartout.paths) {
                                fabric.loadSVGFromURL(dataService.getRawPath(obj.passepartoutOrg.element.value), function(objects, options) {
                                    var pass = fabric.util.groupSVGElements(objects, options);
                                    obj.passepartout = pass;
                                    obj.clipTo = function (ctx) {
                                        this.passepartout.top = 0;
                                        this.passepartout.left = 0;
                                        ctx.save();
                                        ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                        ctx.translate(0,0);
                                        ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(this.passepartoutOrg.element.x*site.scale))*-1/ this.getScaleX(),
                                            ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(this.passepartoutOrg.element.y*site.scale))*-1/ this.getScaleX());
                                        ctx.beginPath();
                                        this.passepartout.opacity=0;
                                        this.passepartout.scaleX = ((parseFloat(this.passepartoutOrg.element.width)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                                        this.passepartout.scaleY = ((parseFloat(this.passepartoutOrg.element.height)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                                        this.passepartout.render(ctx);
                                        ctx.closePath();
                                        ctx.restore();
                                    };
                                    paper.renderAll();
                                });
                            }

                            obj.setCoords();
                        }
                    });
                    this.renderAll();
                    site.fabricObj = this;
                    this.site = site;
                    sitesManager.sites[site.id] = site;
                }
            });

            var elements_rendered = angular.fromJson(site.fabricJson);
            angular.forEach(elements_rendered.objects,function(obj, i){
                obj.clipTo = null;
            });
            this.paper.loadFromJSON(angular.toJson(elements_rendered), this.paper.renderAll.bind(this.paper));
        },

        removeStatic: function() {
            if(!this.haveStatic) return;
            this.paper._objects.forEach(function(obj) {
                if(obj.pos == 3.21) {
                    obj.remove();
                }
            });
            this.haveStatic = false;
        },

        createFromBasket: function(site) {

            if(site.fabricJson == null) {
                this.createCanvas(site);

                return;
            }

            angular.forEach(sitesManager.sites, function(site, key){
                site.convertFabricToJson();
            });



            this.canvas = null;
            this.paper = null;
            var viewportWidth = window.innerWidth*1.4;
            var viewportHeight = window.innerHeight*1.4;

            if(dataService.theme) {
                $("#c").mCustomScrollbar("destroy");
                viewportWidth = jQuery(".desk").width()*2.5;
                viewportHeight = jQuery(".desk").height()*2.5;
            }


            if(site.height*1.4 > window.innerHeight*1.4 && !dataService.theme) {
                viewportWidth = window.innerWidth*1.6;
                viewportHeight = window.innerHeight*1.6;
            }


            if(site.height > ((window.innerHeight)-200) && !dataService.theme) {
                site.scale = 0.4;
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            if(dataService.theme && site.maxHeight >= site.maxWidth) {
                var calcHight = jQuery(".desk").height()-280;
                site.scale = window.math.round(calcHight/site.maxHeight, 2);
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            if(dataService.theme && site.maxHeight < site.maxWidth) {
                var calcWidth = jQuery(".desk").width()-280;
                site.scale = window.math.round(calcWidth/site.maxWidth, 2);

                if((jQuery(".desk").height()-280) < (site.height*site.scale)) {
                    site.scale = window.math.round((jQuery(".desk").height()-280)/site.maxHeight, 2);
                }
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            jQuery("#canvas").remove();
            canvasElement = document.getElementById("c");
            canvasElement.innerHTML = '<canvas id="canvas"></canvas>';

            this.currCanvas = document.getElementById('canvas');
            var currCtx = this.currCanvas.getContext('2d');

            this.currCanvas.style.position = "fixed";
            jQuery("#canvas").width(viewportWidth);
            jQuery("#canvas").height(viewportHeight);
            this.currCanvas.setAttribute("width", viewportWidth);
            this.currCanvas.setAttribute("height", viewportHeight);
            this.currCanvas.style.top = 0;
            this.currCanvas.style.left = 0;


            var siteWidth = (site.width+site.trimLeft+site.trimRight)*site.scale;
            var siteHeight = (site.height+site.trimTop+site.trimBottom)*site.scale;

            var leftOffset = viewportWidth /2 - siteWidth/2;
            var topOffset = viewportHeight /2 - siteHeight/2;
            site.calcWidth = siteWidth;

            var paperObj = new fabric.Canvas(document.getElementById('canvas'), { stateful : true });
            this.paper = paperObj;
            this.paper.preserveObjectStacking = true;

            if(!dataService.theme) {
                jQuery('#c').scrollLeft((viewportWidth-window.innerWidth)/2);
                jQuery('#c').scrollTop((viewportHeight-window.innerHeight)/2);
            }else{
                jQuery('#c').height(jQuery(".desk").height()-180);
                $("#c").mCustomScrollbar({
                    axis:"yx",
                    mouseWheel: false,
                    alwaysShowScrollbar: true,
                    theme:'inset-3-dark',
                    callbacks:{
                        onScroll:function(){
                            paperObj.calcOffset();
                        }
                    }
                });


                jQuery(".mCSB_container").css('left', ((jQuery(".mCSB_container").width()-jQuery(".mCustomScrollbar").width())/2)*-1);
                jQuery(".mCSB_container").css('top', ((jQuery(".mCSB_container").height()-jQuery(".mCustomScrollbar").height())/2)*-1);

                jQuery('#c').mCustomScrollbar('disable');
            }
            this.paper.off('object:modified');
            this.paper.on('object:modified', function() {
                previewManager.previewLoaded(site);
            });

            this.paper.off('selection:cleared');
            this.paper.on('selection:cleared', function() {
                $rootScope.$broadcast('tools.hide');
            });

            paper = this.paper;

            if(!dataService.theme) {
                jQuery('#c').scroll(function(){
                    paperObj.calcOffset();
                });
            }

            this.paper.on('after:render', function() {
                cfpLoadingBar.complete();
                var objects = this.getObjects();

                if(objects.length > 0) {
                    this.off('after:render');
                    this.off("before:render");

                    // CUTS
                    if(site.renderPrintCutting) {
                        var lineT = new fabric.Line([0, 0, viewportWidth, 0], {
                            stroke: 'red',
                            strokeDashArray: [5, 5],
                            selectable: false,
                            name: 'top',
                            id: 'top',
                            left: 0,
                            renderPreview: false,
                            renderPdf: false,
                            top: topOffset + (site.trimTop * site.scale),
                            pos: 9999
                        });

                        this.insertAt(lineT, 9999);

                        var lineB = new fabric.Line([0, 0, viewportWidth, 0], {
                            stroke: 'red',
                            strokeDashArray: [5, 5],
                            selectable: false,
                            name: 'bottom',
                            id: 'bottom',
                            renderPreview: false,
                            renderPdf: false,
                            left: 0,
                            top: topOffset + siteHeight - (site.trimBottom * site.scale),
                            pos: 9999
                        });

                        this.insertAt(lineB, 9999);

                        var lineL = new fabric.Line([0, 0, 0, viewportHeight], {
                            stroke: 'red',
                            strokeDashArray: [5, 5],
                            name: 'left',
                            id: 'left',
                            renderPreview: false,
                            renderPdf: false,
                            selectable: false,
                            left: leftOffset + (site.trimLeft * site.scale),
                            top: 0,
                            pos: 9999
                        });

                        this.insertAt(lineL, 9999);

                        var lineR = new fabric.Line([0, 0, 0, viewportHeight], {
                            stroke: 'red',
                            strokeDashArray: [5, 5],
                            name: 'right',
                            id: 'right',
                            renderPreview: false,
                            renderPdf: false,
                            selectable: false,
                            left: leftOffset + siteWidth - (site.trimRight * site.scale),
                            top: 0,
                            pos: 9999
                        });

                        this.insertAt(lineR,9999);
                    }

                    if(site.renderPrintMedia) {

                        var rectMedia = new fabric.Rect({
                            left: leftOffset,
                            top: topOffset,
                            width: siteWidth,
                            height: siteHeight,
                            fill: 'transparent',
                            strokeWidth: 1,
                            strokeDashArray: [2, 5],
                            stroke: '#666666',
                            id: 'mediaBox',
                            name: 'mediaBox',
                            selectable: false,
                            renderPreview: false,
                            renderPdf: false,
                            pos: 9999

                        });

                        this.insertAt(rectMedia,9999);

                    }

                    angular.forEach(objects,function(obj, i){
                        if (obj.name != "left" &&
                            obj.name != "right" &&
                            obj.name != "top" &&
                            obj.name != "bottom") {
                            obj.left=obj.left*site.scale+leftOffset;
                            obj.top=obj.top*site.scale+topOffset;
                            obj.scaleX = obj.scaleX * site.scale;
                            obj.scaleY = obj.scaleY * site.scale;

                            if(obj.id == "background-color") {
                                obj.fill = site.backgroundColor;
                            }

                            if(obj.id == "calendar") {
                                fabric.util.loadImage(
                                    dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height,
                                        site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1,7),
                                        site.calendarSetting.dayColor.value.substring(1,7), site.calendarSetting.sunColor.value.substring(1,7),
                                        site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal,
                                        site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY,
                                        site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily
                                    ), function (img) {
                                        obj.setElement(img);
                                    });
                            }

                            if(obj.fontFamily) {

                                obj.on('selected', function() {

                                    var fontSize = this.fontSize;
                                    var color = this.fill;
                                    var fontFamily = this.fontFamily;
                                    var align = this.textAlign;
                                    var bold = this.fontWeight;
                                    var italic = this.fontStyle;
                                    var underline = this.textDecoration;
                                    var text = this.text;

                                    $rootScope.$broadcast('tools.text.selected', {
                                        fontSize: fontSize,
                                        fontFamily: fontFamily,
                                        color: color,
                                        align: align,
                                        underline: underline,
                                        italic: italic,
                                        bold: bold,
                                        text: text
                                    });
                                });
                            }else{
                                obj.on('selected', function() {

                                    $rootScope.$broadcast('tools.motiv.selected', {
                                    });
                                });
                            }

                            if(obj.clipBox) {

                                obj.clipTo = function(ctx) {
                                    ctx.save();
                                    ctx.translate(0,0);
                                    ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                    //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                                    ctx.beginPath();
                                    ctx.rect(
                                        (this.getPointByOrigin("center", "center").x-site.leftOffset-parseFloat(this.clipBox[0]))*-1/ this.getScaleX(),
                                        (this.getPointByOrigin("center", "center").y-site.topOffset-parseFloat(this.clipBox[1]))*-1/ this.getScaleX(),
                                        parseFloat(this.clipBox[2])*site.scale/ this.getScaleX()+2,
                                        parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()+2
                                    );

                                    ctx.closePath();
                                    ctx.restore();
                                };

                            }

                            obj.setCoords();
                        }
                    });
                    this.renderAll();
                    site.fabricObj = this;
                    this.site = site;
                    sitesManager.sites[site.id] = site;
                }
            });

            site.leftOffset = leftOffset + (site.trimLeft*site.scale);
            site.topOffset = topOffset +(site.trimTop*site.scale);

            site.fabricObj = this.paper;
            site.calcWidth = siteWidth;

            var scope = this;

            this.site = site;

            var elements_rendered = angular.fromJson(site.fabricJson);
            var temp = [];
            angular.forEach(elements_rendered.objects,function(obj, i){
                if(obj.name == "top" || obj.name == "bottom" || obj.name == "left" || obj.name == "right" || obj.name == "mediaBox") {
                    return;
                }
                obj.clipTo = null;
                temp.push(obj);
            });
            elements_rendered.objects = temp;
            this.paper.loadFromJSON(angular.toJson(elements_rendered), this.paper.renderAll.bind(this.paper));
        },

        createCanvas: function(site) {
            angular.forEach(sitesManager.sites, function(site, key){
                site.convertFabricToJson();
            });

            this.canvas = null;
            this.paper = null;

            var viewportWidth = window.innerWidth*1.4;
            var viewportHeight = window.innerHeight*1.4;

            if(dataService.theme) {
                $("#c").mCustomScrollbar("destroy");
                viewportWidth = jQuery(".desk").width()*2.5;
                viewportHeight = jQuery(".desk").height()*2.5;
            }


            if(site.height*1.4 > window.innerHeight*1.4 && !dataService.theme) {
                viewportWidth = window.innerWidth*1.6;
                viewportHeight = window.innerHeight*1.6;
            }


            if(site.height > ((window.innerHeight)-200) && !dataService.theme) {
                site.scale = 0.4;
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            if(dataService.theme && site.maxHeight >= site.maxWidth) {
                var calcHight = jQuery(".desk").height()-280;
                site.scale = window.math.round(calcHight/site.maxHeight, 1);
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            if(dataService.theme && site.maxHeight < site.maxWidth) {
                var calcWidth = jQuery(".desk").width()-280;
                site.scale = window.math.round(calcWidth/site.maxWidth, 1);

                if((jQuery(".desk").height()-280) < (site.height*site.scale)) {
                    site.scale = window.math.round((jQuery(".desk").height()-280)/site.maxHeight, 2);
                }
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            if(site.maxZoom < site.scale) {
                site.scale = site.maxZoom;
                $rootScope.$broadcast('settings.zoom.set', site.scale);
            }

            jQuery("#canvas").remove();
            canvasElement = document.getElementById("c");
            canvasElement.innerHTML = '<canvas id="canvas"></canvas>';

            this.currCanvas = document.getElementById('canvas');
            var currCtx = this.currCanvas.getContext('2d');

            this.currCanvas.style.position = "fixed";
            jQuery("#canvas").width(viewportWidth);
            jQuery("#canvas").height(viewportHeight);
            this.currCanvas.setAttribute("width", viewportWidth);
            this.currCanvas.setAttribute("height", viewportHeight);
            this.currCanvas.style.top = 0;
            this.currCanvas.style.left = 0;

            var siteWidth = (site.width+site.trimLeft+site.trimRight)*site.scale;
            var siteHeight = (site.height+site.trimTop+site.trimBottom)*site.scale;

            var leftOffset = viewportWidth /2 - siteWidth/2;
            var topOffset = viewportHeight /2 - siteHeight/2;

            this.paper = new fabric.Canvas(document.getElementById('canvas'), { stateful : true });
            this.paper.preserveObjectStacking = true;
            var paperObj = this.paper;
            if(!dataService.theme) {
                jQuery('#c').scrollLeft((viewportWidth-window.innerWidth)/2);
                jQuery('#c').scrollTop((viewportHeight-window.innerHeight)/2);
            }else{
                jQuery('#c').height(jQuery(".desk").height()-90);
                $("#c").mCustomScrollbar({
                    axis:"yx",
                    mouseWheel: false,
                    theme:'inset-3-dark',
                    alwaysShowScrollbar: true,
                    callbacks:{
                        onScroll:function(){
                            paperObj.calcOffset();
                        }
                    }
                });

                jQuery(".mCSB_container").css('left', ((jQuery(".mCSB_container").width()-jQuery(".mCustomScrollbar").width())/2)*-1);
                jQuery(".mCSB_container").css('top', ((jQuery(".mCSB_container").height()-jQuery(".mCustomScrollbar").height())/2)*-1);

                jQuery('#c').mCustomScrollbar('disable');


            }

            this.paper.calcOffset();
            this.paper.leftOffset = leftOffset;
            this.paper.topOffset = topOffset;


            this.paper.on('object:added', function() {
                this._objects.sort(function(a, b) {
                    return (a.pos - b.pos);
                });

            });



            if(!dataService.theme) {
                jQuery('#c').scroll(function(){
                    paperObj.calcOffset();
                });
            }

            this.paper.on('object:modified', function() {
                previewManager.previewLoaded(site);
            });

            this.paper.on('selection:cleared', function() {
                $rootScope.$broadcast('tools.hide');
            });


            var rect = new fabric.Rect({
                left: leftOffset,
                top: topOffset,
                fill: site.backgroundColor,
                width: siteWidth,
                height: siteHeight,
                selectable: false,
                hasBorders: false,
                renderPreview: true,
                renderPdf: true,
                hasControls: false,
                name: "back",
                id: "background-color",
                orgLeft: 0,
                orgTop: 0,
                pos: 0
            });


            this.paper.add(rect);

            // CUTS
            if(site.renderPrintCutting) {
                var lineT = new fabric.Line([0, 0, viewportWidth, 0], {
                    stroke: 'red',
                    strokeDashArray: [5, 5],
                    selectable: false,
                    renderPreview: false,
                    renderPdf: false,
                    name: 'top',
                    id: 'top',
                    left: 0,
                    top: topOffset+(site.trimTop*site.scale),
                    pos: 9999
                });

                this.paper.add(lineT);

                var lineB = new fabric.Line([0, 0, viewportWidth, 0], {
                    stroke: 'red',
                    strokeDashArray: [5, 5],
                    selectable: false,
                    name: 'bottom',
                    renderPreview: false,
                    renderPdf: false,
                    id: 'bottom',
                    left: 0,
                    top: topOffset + siteHeight - (site.trimBottom*site.scale),
                    pos: 9999
                });

                this.paper.add(lineB);

                var lineL = new fabric.Line([0, 0, 0, viewportHeight], {
                    stroke: 'red',
                    strokeDashArray: [5, 5],
                    name: 'left',
                    id: 'left',
                    selectable: false,
                    renderPreview: false,
                    renderPdf: false,
                    left: leftOffset + (site.trimLeft*site.scale),
                    top: 0,
                    pos: 9999
                });

                this.paper.add(lineL);

                var lineR = new fabric.Line([0, 0, 0, viewportHeight], {
                    stroke: 'red',
                    strokeDashArray: [5, 5],
                    name: 'right',
                    id: 'right',
                    selectable: false,
                    renderPreview: false,
                    renderPdf: false,
                    left: leftOffset+siteWidth-(site.trimRight*site.scale),
                    top: 0,
                    pos: 9999
                });

                this.paper.add(lineR);
            }

            // media
            if(site.renderPrintMedia) {

                var rectMedia = new fabric.Rect({
                    left: leftOffset,
                    top: topOffset,
                    width: siteWidth,
                    height: siteHeight,
                    fill: 'transparent',
                    strokeWidth: 1,
                    strokeDashArray: [2, 5],
                    stroke: '#666666',
                    id: 'mediaBox',
                    name: 'mediaBox',
                    selectable: false,
                    renderPreview: false,
                    renderPdf: false,
                    pos: 9999

                });

                this.paper.add(rectMedia);


            }

            site.leftOffset = leftOffset + (site.trimLeft*site.scale);
            site.topOffset = topOffset +(site.trimTop*site.scale);
            site.fabricObj = this.paper;
            site.calcWidth = siteWidth;

            var scope = this;

            this.site = site;


            angular.forEach(site.elements,function(element, ie){

                element.paint($rootScope, scope.site, scope.paper, element, element.x, element.y, element.width, element.height, element.selectable);

            });

            this.site = site;
            cfpLoadingBar.complete();
        },

        setFont: function(font) {
            var activeObject = this.paper.getActiveObject();
            if (activeObject && /text/.test(activeObject.type)) {
                this.setStyle(activeObject, 'fontFamily', font);
            }
            previewManager.previewLoaded(this.site);
        },

        setSize: function(size) {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();

            if (activeObject && /text/.test(activeObject.type)) {
                this.setStyle(activeObject, 'fontSize', parseFloat(size, 10));
                activeObject.setCoords();
                this.paper.renderAll();

            }
        },

        setText: function(text) {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();

            if (activeObject && /text/.test(activeObject.type)) {
                activeObject.setText(text);
                activeObject.setCoords();
                this.paper.renderAll();

            }
        },

        moveBack: function() {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();
            if (activeObject) {
                this.paper.off('before:render');
                var refObject = false;
                var nextObject = true;
                this.paper.forEachObject(function(ob) {
                   if(ob == activeObject) {
                        nextObject = false;
                    }
                      if(nextObject) {
                        refObject = ob;
                    }
              });
                if(refObject && refObject.pos > 100) {
                    activeObject.pos = refObject.pos - 1;
                    this.paper.sendBackwards(activeObject);
                    this.paper.renderAll();
                }
            }
        },

        onBottom: function() {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();
            if (activeObject) {
                this.paper.off('before:render');
                var refObject = false;
                this.paper.forEachObject(function(ob) {
                    if(ob.pos > 100 && ob.pos < 1000) {
                        refObject = ob;
                    }


                });
                if(refObject && refObject.pos > 100) {
                    activeObject.pos = refObject.pos - 1;
                    this.paper.sendToBack(activeObject);
                    this.paper.renderAll();
                }
            }
        },

        moveForward: function() {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();
            if (activeObject) {
                this.paper.off('before:render');

                var refObject = false;
                var nextObject = false;
                this.paper.forEachObject(function(ob) {
                    if(nextObject) {
                        refObject = ob;
                        nextObject = false;
                    }

                    if(ob == activeObject) {
                        nextObject = true;
                    }
                });

                if (refObject && refObject.pos < 1000) {
                    activeObject.pos = refObject.pos + 1;
                    this.paper.bringForward(activeObject);
                    this.paper.renderAll();
                }
            }
        },

        onTop: function() {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();
            if (activeObject) {
                this.paper.off('before:render');
                var refObject = false;
                var nextObject = true;

                if(activeObject.pos < 100 || activeObject > 1000) {
                    return;
                }

                this.paper.forEachObject(function(ob) {



                });

                /**


                this.paper.forEachObject(function(ob) {

                    if(nextObject && ob.pos < 1000) {
                        refObject = ob;
                        nextObject = false;
                    }

                });
                if(refObject && refObject.pos) {
                    activeObject.pos = refObject.pos + 1;
                    this.paper.bringToFront(activeObject);
                    this.paper.renderAll();
                }

                 */
            }
        },

        deleteElement: function() {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();
            if (activeObject) {
                activeObject.remove();
                this.paper.renderAll();
            }
        },

        setColor: function(color) {
            if(this.paper == null) {
                return;
            }
            var activeObject = this.paper.getActiveObject();

            if (activeObject && /text/.test(activeObject.type)) {
                this.setStyle(activeObject, 'fill', color);
            }
        },

        resetBold: function() {
            var activeObject = this.paper.getActiveObject();
            if (activeObject && /text/.test(activeObject.type)) {
                this.setStyle(activeObject, 'fontWeight', '400');
            }
        },

        setBold: function() {
            var activeObject = this.paper.getActiveObject();
            if (activeObject && /text/.test(activeObject.type)) {

                var isBold = this.getStyle(activeObject, 'fontWeight') === '700';
                this.setStyle(activeObject, 'fontWeight', isBold ? '400' : '700');
            }
        },

        resetItalic: function() {
            var activeObject = this.paper.getActiveObject();
            if (activeObject && /text/.test(activeObject.type)) {
                this.setStyle(activeObject, 'fontStyle', '');
            }
        },

        setItalic: function() {
            var activeObject = this.paper.getActiveObject();
            if (activeObject && /text/.test(activeObject.type)) {

                var isItalic = this.getStyle(activeObject, 'fontStyle') === 'italic';
                this.setStyle(activeObject, 'fontStyle', isItalic ? '' : 'italic');
            }
        },

        setUnderline: function() {
            var activeObject = this.paper.getActiveObject();
            if (activeObject && /text/.test(activeObject.type)) {

                var isUnderline = (this.getStyle(activeObject, 'textDecoration') || '').indexOf('underline') > -1;
                this.setStyle(activeObject, 'textDecoration', isUnderline ? '' : 'underline');
            }
        },

        setAlign: function(value) {
            var activeObject = this.paper.getActiveObject();
            if (activeObject && /text/.test(activeObject.type)) {
                activeObject.textAlign = value;
                this.paper.renderAll();
            }
        },

        resize: function() {
            if(this.paper == null) {
                return;
            }

            var newLeftOffset = window.innerWidth*1.4 /2 - this.site.width/2;
            var newTopOffset = window.innerHeight*1.4 /2 - this.site.height/2;

            if(this.site.height*1.4 > window.innerHeight*1.4) {
                newLeftOffset = window.innerWidth*1.6 /2 - this.site.width/2;
                newTopOffset = window.innerHeight*1.6 /2 - this.site.height/2;
            }

            this.paper.setWidth(window.innerWidth*1.4);
            this.paper.setHeight(window.innerHeight*1.4);
            this.paper.calcOffset();

            var objects = this.paper.getObjects();
            var site = this.site;
            angular.forEach(objects,function(obj, i){

                obj.left=obj.left-site.leftOffset+newLeftOffset;
                obj.top=obj.top-site.topOffset+newTopOffset;
                obj.setCoords();
            });
            this.paper.renderAll();
            this.site.leftOffset = newLeftOffset;
            this.site.topOffset = newTopOffset;
            this.site.previewLeftOffset = newLeftOffset-5;
            this.site.previewTopOffset = newTopOffset-5;

        },

        setZoom: function(zoom) {
            if(this.paper == null) {
                return;
            }
            var SCALE_FACTOR=parseFloat(zoom);
            var site = this.site;
            var newLeftOffset = window.innerWidth*1.4 /2 - ((site.width+site.trimLeft+site.trimRight)*SCALE_FACTOR)/2;
            var newTopOffset = window.innerHeight*1.4 /2 - ((site.height+site.trimTop+site.trimBottom)*SCALE_FACTOR)/2;

            if(this.site.height*1.4 > window.innerHeight*1.4) {
                newLeftOffset = window.innerWidth*1.6 /2 - ((site.width+site.trimLeft+site.trimRight)*SCALE_FACTOR)/2;
                newTopOffset = window.innerHeight*1.6 /2 - ((site.height+site.trimTop+site.trimBottom)*SCALE_FACTOR)/2;
            }


            if(dataService.theme) {
                newLeftOffset = jQuery(".desk").width()*2.5 /2 - ((site.width+site.trimLeft+site.trimRight)*SCALE_FACTOR)/2;
                newTopOffset = jQuery(".desk").height()*2.5 /2 - ((site.height+site.trimTop+site.trimBottom)*SCALE_FACTOR)/2;
            }


            this.paper.calcOffset();

            var objects = this.paper.getObjects();




            angular.forEach(objects,function(obj, i){

                if (obj.name == "left") {
                    obj.left=(newLeftOffset)+(site.trimLeft*SCALE_FACTOR);
                }else if (obj.name == "right") {
                    obj.left=(newLeftOffset+((site.width+site.trimLeft)*SCALE_FACTOR))+1;
                }else if (obj.name == "top") {
                    obj.top=(newTopOffset)+(site.trimTop*SCALE_FACTOR);
                }else if (obj.name == "bottom") {
                    obj.top=(newTopOffset+((site.height+site.trimTop)*SCALE_FACTOR))+1;
                }else if (obj.name == "mediaBox") {
                    obj.top=newTopOffset;
                    obj.left=newLeftOffset;
                    obj.width=(site.width*SCALE_FACTOR);
                    obj.height=(site.height*SCALE_FACTOR);
                }else{
                    if(newTopOffset < 1) {
                        newTopOffset = 0;
                    }
                    obj.left   = ((obj.left-site.leftOffset)*((site.width+site.trimLeft+site.trimRight)/site.calcWidth)*(SCALE_FACTOR)+newLeftOffset+(site.trimLeft*SCALE_FACTOR));
                    obj.top    = ((obj.top -site.topOffset )*((site.width+site.trimLeft+site.trimRight)/site.calcWidth)*(SCALE_FACTOR)+newTopOffset+(site.trimTop*SCALE_FACTOR));
                    obj.scaleX=obj.scaleX/site.scale*SCALE_FACTOR;
                    obj.scaleY=obj.scaleY/site.scale*SCALE_FACTOR;
                    obj.siteScale=SCALE_FACTOR;
                }

                obj.setCoords();
            });

            this.site.scale = SCALE_FACTOR;
            this.site.leftOffset = newLeftOffset + (site.trimLeft * SCALE_FACTOR);
            this.site.calcWidth = (site.width+site.trimLeft+site.trimRight) * SCALE_FACTOR;
            this.site.topOffset = newTopOffset + (site.trimTop * SCALE_FACTOR);
            this.site.previewLeftOffset = newLeftOffset;
            this.site.previewTopOffset = newTopOffset;
            this.site.previewWidth = (site.width*SCALE_FACTOR);
            this.site.previewHeight = (site.height*SCALE_FACTOR);
            this.paper.renderAll();
            $rootScope.$broadcast('settings.zoom.set', SCALE_FACTOR);

            if(site.width * SCALE_FACTOR > jQuery("._mCS_1").width() || site.height * SCALE_FACTOR > jQuery("._mCS_1").height()) {
                $("#c").mCustomScrollbar('update');
            }else{
                $("#c").mCustomScrollbar('disable');
            }
            jQuery(".mCSB_container").css('left', ((jQuery(".mCSB_container").width()-jQuery(".mCustomScrollbar").width())/2)*-1);
            jQuery(".mCSB_container").css('top', ((jQuery(".mCSB_container").height()-jQuery(".mCustomScrollbar").height())/2)*-1);

        },

        getStyle: function(object, styleName) {
            return (object.getSelectionStyles && object.isEditing)? object.getSelectionStyles()[styleName] : object[styleName];
        },

        setStyle: function(object, styleName, value) {
            if (object.setSelectionStyles && object.isEditing) {
                var style = { };
                style[styleName] = value;
                object.setSelectionStyles(style);
                object.setCoords();
            }
            else {
                object.set(styleName, value);

            }

            this.paper.renderAll();
        },

        changeBackgroundColorFrom: function(index, color) {
            this.removeStatic();
            var objects = this.paper.getObjects();

            angular.forEach(objects,function(obj, i){
                if(obj.id == index) {
                    if (obj.setSelectionStyles && obj.isEditing) {
                        var style = { };
                        style['fill'] = color.value;
                        style['opacity'] = 1;
                        obj.setSelectionStyles(style);

                    }
                    else {
                        obj.set('fill', color.value);
                    }
                    obj.setCoords();
                    if(obj.paths) {
                        angular.forEach(obj.paths, function(path, i){
                            path.fill = color.value;
                        });
                    }
                }
            });
            this.paper.renderAll();
            if(index == "background-color") {
                angular.forEach(sitesManager.sites,function(obj, i){
                    obj.backgroundColor = color.value;
                    previewManager.previewLoadedFromPreview(obj);
                });
            }
        },

        changeCalSettingsColors: function(index, color) {
            this.removeStatic();
            var objects = this.paper.getObjects();
            var site = this.site;
            var paper = this.paper;
            if(site.calendarSetting) {
                if("calendarSetting.monthColor" == index) {
                    site.calendarSetting.monthColor = color;
                }
                if("calendarSetting.dayColor" == index) {
                    site.calendarSetting.dayColor = color;
                }
                if("calendarSetting.sunColor" == index) {
                    site.calendarSetting.sunColor = color;
                }
                angular.forEach(objects,function(obj, i){
                    if(obj.id == "calendar") {
                        fabric.util.loadImage(
                            dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height,
                                site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1,7),
                                site.calendarSetting.dayColor.value.substring(1,7), site.calendarSetting.sunColor.value.substring(1,7),
                                site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal,
                                site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY,
                                site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily
                            ), function (img) {
                            obj.setElement(img);
                            paper.renderAll();

                        });

                    }
                });
            }

            angular.forEach(sitesManager.sites,function(obj, i){
                if(!obj.calendarSetting) {
                    return;
                }
                if("calendarSetting.monthColor" == index) {
                    obj.calendarSetting.monthColor = color;
                }
                if("calendarSetting.dayColor" == index) {
                    obj.calendarSetting.dayColor = color;
                }
                if("calendarSetting.sunColor" == index) {
                    obj.calendarSetting.sunColor = color;
                }
                previewManager.previewLoadedFromPreview(obj);
            });
        },

        changeCalSettings: function(startMonth, startYear) {
            var objects = this.paper.getObjects();
            var site = this.site;
            var paper = this.paper;

            startYear = parseFloat(startYear, 10);
            startMonth = parseFloat(startMonth, 10);

            if(site.calendarSetting) {

                site.calendarSetting.month = site.calendarSetting.calcMonth + startMonth - 1;
                site.calendarSetting.year = startYear;

                angular.forEach(objects,function(obj, i){
                    if(obj.id == "calendar") {
                        fabric.util.loadImage(
                            dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height,
                                site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1,7),
                                site.calendarSetting.dayColor.value.substring(1,7), site.calendarSetting.sunColor.value.substring(1,7),
                                site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal,
                                site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY,
                                site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily
                            ), function (img) {
                                obj.setElement(img);
                                paper.renderAll();

                            });

                    }
                });
            }

            angular.forEach(sitesManager.sites,function(obj, i){
                if(!obj.calendarSetting) {
                    return;
                }

                obj.calendarSetting.month = obj.calendarSetting.calcMonth + startMonth - 1;
                obj.calendarSetting.year = startYear;

                previewManager.previewLoadedFromPreview(obj);
            });
        }
    };

    return Painter;
}])

.controller( 'PainterCtrl', function PainterController( $scope, $rootScope, painterManager ) {


})

;
