angular.module( 'layouterApp.footer', [
        'angular-loading-bar'
])
.config(function config(  ) {
})

.controller( 'FooterCtrl', function FooterController( $scope, $rootScope, painterManager, previewManager, sitesManager, dataService, cfpLoadingBar, $analytics ) {
    $scope.settings = {
        zoom: 1,
        zoomPercent: 100,
        show_preview: false,
        show_preview_pdf: false,
        show_print_pdf: false,
        show_next: false,
        show_prev: false,
        show_finish: false,
        toPaint: true,
        saveProduct: false,
        showProductSettings: false,
        showZoomSelector: false
    };

    $scope.$on('general.showProductSettingsButton', function() {
        $scope.settings.showProductSettings = true;
    });

    $scope.showProductSettings = function() {
        $rootScope.$broadcast('general.showProductSettings');
    };

    $scope.app_busy = null;

    $scope.finishProduct = function() {
        $analytics.eventTrack('finishProduct');
        cfpLoadingBar.start();
        $scope.renderPreviewPDF(0, 1);

    };

    $scope.saveProduct = function() {
        cfpLoadingBar.start();
        $scope.renderPreviewPDF(0, 2);

    };

    $scope.printPDF = function() {
        $analytics.eventTrack('printPdf');
        cfpLoadingBar.start();
        $scope.renderPreviewPDF(0, 3);

    };

    $scope.changeZoom = function(val) {
        if(val) {
            painterManager.setZoom(val);
        }else{
            painterManager.setZoom($scope.settings.zoom);
        }
        $scope.settings.showZoomSelector = false;

    };

    $scope.previewPDF = function() {
        $analytics.eventTrack('previewPdf');
        cfpLoadingBar.start();
        $scope.renderPreviewPDF(0, 0);
        return false;

    };

    $scope.renderPreviewPDF = function(index, save) {
        if((sitesManager.sites.length-1) < index) {
            if(save === 1) {
                dataService.finishProduct();
                cfpLoadingBar.complete();
            }else if(save === 2) {
                dataService.saveProduct();
                cfpLoadingBar.complete();
            }else if(save === 3) {
                dataService.saveAndPrint();
                cfpLoadingBar.complete();
            }else{
                dataService.openPreviewPdf();
                cfpLoadingBar.complete();
            }


            return false;
        }

        var workInLayouter = false;

        if(index == painterManager.site.id) {
            painterManager.paper.deactivateAll();
            sitesManager.sites[index].fabricJson = painterManager.paper.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);
            workInLayouter= true;
        }

        site = sitesManager.sites[index];
        if(site.previewJson === "") {
            return;
        }

        jQuery("#previewrenderer").remove();
        canvasElement = document.getElementById("previewrenderer_id");
        canvasElement.innerHTML = '<canvas id="previewrenderer"></canvas>';

        currCanvas = document.getElementById('previewrenderer');
        var currCtx = currCanvas.getContext('2d');
        currCanvas.style.position = "fixed";
        currCanvas.setAttribute("width", site.width+site.trimLeft+site.trimTop);
        var actWidth = site.width+site.trimLeft+site.trimTop;
        var actHeight = site.height+site.trimTop+site.trimBottom;
        currCanvas.setAttribute("height", site.height+site.trimTop+site.trimBottom);
        currCanvas.style.top = 0;
        currCanvas.style.left = 0;
        var elements_rendered = false;
        var elements_count = 0;
        var SCALE_FACTOR = false;
        var canFinished = 0;
        if(site.fabricJson != null) {
            elements_rendered = angular.fromJson(site.fabricJson);
            SCALE_FACTOR = (site.width+site.trimLeft+site.trimRight)/site.calcWidth;
        }else{
            elements_rendered = false;
        }

        paper = new fabric.Canvas(document.getElementById('previewrenderer'));



        paper.on('object:added', function(obj) {
            obj.target.clipTo = null;
            if(obj.target) {
                if(obj.target.passepartoutOrg && obj.target.passepartoutOrg.element && obj.target.passepartout && obj.target.passepartoutOrg.element.value) {
                    canFinished++;
                    fabric.loadSVGFromURL(dataService.getRawPath(obj.target.passepartoutOrg.element.value), function(objects, options) {
                        var pass = fabric.util.groupSVGElements(objects, options);
                        if(site.leftOffset > 0) {
                            pass.top = ((obj.target.getPointByOrigin("center", "center").y -site.topOffset) * -1 + (parseFloat(obj.target.passepartoutOrg.element.y)*site.scale)) / obj.target.getScaleY();
                            pass.left = ((obj.target.getPointByOrigin("center", "center").x -site.leftOffset)* -1 + (parseFloat(obj.target.passepartoutOrg.element.x)*site.scale)) / obj.target.getScaleX();
                        }else{
                            pass.top = (obj.target.getPointByOrigin("center", "center").y * -1 + parseFloat(obj.target.passepartoutOrg.element.y)) / obj.target.getScaleY();
                            pass.left = (obj.target.getPointByOrigin("center", "center").x * -1 + parseFloat(obj.target.passepartoutOrg.element.x)) / obj.target.getScaleX();
                        }
                        pass.scaleX = ((parseFloat(obj.target.passepartoutOrg.element.width)*site.scale/ obj.target.getScaleX()+2)/obj.target.passepartout.width);
                        pass.scaleY = ((parseFloat(obj.target.passepartoutOrg.element.height)*site.scale/ obj.target.getScaleY()+2)/obj.target.passepartout.height);
                        obj.target.clipTo = pass;
                        canFinished--;
                        paper.renderAll();
                    });


                }else if(obj.target.passepartout && obj.target.passepartout.paths && obj.target.passepartoutOrg.element) {
                    var pass = new fabric.Circle.fromObject(obj.target.passepartout.paths[0]);
                    pass.top = (obj.target.getPointByOrigin("center", "center").y*-1+parseFloat(site.passepartoutY))/ obj.target.getScaleY();
                    pass.left = (obj.target.getPointByOrigin("center", "center").x*-1+parseFloat(site.passepartoutX))/ obj.target.getScaleX();
                    pass.pathOffset = {x: 0, y: 0};
                    pass.fill="transparent";
                    pass.scaleX = ((parseFloat(obj.target.passepartoutOrg.element.width)*site.scale/ obj.target.getScaleX()+2)/obj.target.passepartout.width)/site.scale;
                    pass.scaleY = ((parseFloat(obj.target.passepartoutOrg.element.height)*site.scale/ obj.target.getScaleY()+2)/obj.target.passepartout.height)/site.scale;
                    pass.originX = 'left';
                    pass.originY = 'top';
                    obj.target.clipTo = pass;

                }
            }
        });

        paper.on('after:render', function() {


            paper.calcOffset();

            var objects = paper.getObjects();

            if(objects.length > 0 && canFinished == 0 && ((elements_rendered.objects && elements_rendered.objects.length == objects.length) || elements_count == objects.length)) {

                paper.off("before:render");
                paper.off("after:render");

                paper.setWidth(actWidth);
                paper.setHeight(actHeight);

                this._objects.sort(function(a, b) {
                    return (a.pos - b.pos);
                });

                angular.forEach(objects,function(obj, i){
                    if(obj.templatePrintBinding) {
                        //obj.text = obj.templatePrintBinding;
                    }
                    if(site.leftOffset > 0) {
                        obj.left=((obj.left-site.leftOffset)*SCALE_FACTOR)+site.trimLeft;
                        obj.top=((obj.top-site.topOffset)*SCALE_FACTOR)+site.trimTop;
                        obj.scaleX = (obj.scaleX*SCALE_FACTOR);
                        obj.scaleY = (obj.scaleY*SCALE_FACTOR);
                    }
                    if(obj.id == "background-color") {
                        obj.fill = site.backgroundColor;
                        obj.top = 0;
                        obj.left = 0;
                    }
                    if(obj.clipBox) {
                        obj.clipTo = new fabric.Rect({
                            top : 0,
                            left :  0,
                            width : parseFloat(obj.clipBox[2])/ obj.getScaleY(),
                            height : parseFloat(obj.clipBox[3])/ obj.getScaleY(),
                            angle: obj.angle*-1,
                            fill : 'transparent',
                            centeredRotation: true,
                            originX : 'center',
                            originY : 'center'
                        });

                        obj.clipTo.setCoords();
                        obj.clipTo.setTransformMatrix([ 1, 0, 0, 1, (obj.getPointByOrigin("center", "center").x*-1)/ obj.getScaleY()+(parseFloat(obj.clipBox[2])/2/ obj.getScaleY())+(parseFloat(obj.clipBox[0])/ obj.getScaleY())+site.trimLeft, (obj.getPointByOrigin("center", "center").y*-1)/ obj.getScaleY()+(parseFloat(obj.clipBox[3])/2/ obj.getScaleY())+(parseFloat(obj.clipBox[1])/ obj.getScaleY())+site.trimTop ]);
                    }


                    if(obj.id == "calendar") {
                        fabric.util.loadImage(
                            dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height,
                                site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1,7),
                                site.calendarSetting.dayColor.value.substring(1,7), site.calendarSetting.sunColor.value.substring(1,7),
                                site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal,
                                site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY,
                                site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily
                            ), function (img) {
                                obj.setElement(img);
                                paper.renderAll();

                            });

                    }

                    obj.setCoords();
                });

                paper.renderAll();
                sitesManager.sites[index].fabricSVG = paper.toSVG();
                if(save === 1 || save === 2 || save === 3) {
                    sitesManager.sites[index].previewImage = "";
                    sitesManager.sites[index].saveJson = paper.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);
                }
                $scope.renderPreviewPDF(index+1, save);
                cfpLoadingBar.inc();
            }

        });
        if(site.fabricJson != null) {
            var elements_rendered = angular.fromJson(site.fabricJson);
            var temp = [];
            angular.forEach(elements_rendered.objects,function(obj, i){
                obj.clipTo = null;
                if(obj.id == "calendar") {
                    obj.src = dataService.getCalImage(site.calendarSetting.type, site.calendarSetting.month, site.calendarSetting.year, site.calendarSetting.width, site.calendarSetting.height,
                        site.calendarSetting.monthSize, site.calendarSetting.daySize, site.calendarSetting.monthColor.value.substring(1,7),
                        site.calendarSetting.dayColor.value.substring(1,7), site.calendarSetting.sunColor.value.substring(1,7),
                        site.calendarSetting.designYear, site.calendarSetting.designHeightMonthCal, site.calendarSetting.designMarginCal,
                        site.calendarSetting.calendarX, site.calendarSetting.calendarY, site.calendarSetting.yearX, site.calendarSetting.yearY,
                        site.calendarSetting.calendarWidth, site.calendarSetting.calendarHeight, site.calendarSetting.monthX, site.calendarSetting.monthY, site.calendarSetting.yearSize, site.calendarSetting.fontFamily
                    );

                }
                if(obj.renderPreview && save == 0 && obj.visible) {
                    temp.push(obj);
                }
                if(obj.renderPdf && obj.visible && (save == 1 || save == 2 || save == 3)) {
                    temp.push(obj);
                }

            });
            elements_rendered.objects = temp;
            paper.loadFromJSON(angular.toJson(elements_rendered), paper.renderAll.bind(paper));
        }else{

            var rect = new fabric.Rect({
                left: 0,
                top: 0,
                fill: site.backgroundColor,
                width: site.width+site.trimLeft+site.trimRight,
                height: site.height+site.trimTop+site.trimBottom,
                selectable: false,
                hasBorders: false,
                hasControls: false,
                renderPdf: true,
                hasControls: false,
                name: "back",
                id: "background-color",
                orgLeft: 0,
                orgTop: 0,
                pos: 0
            });

            // "add" rectangle onto canvas
            paper.add(rect);
            elements_count = 1;
            angular.forEach(site.elements,function(element, ie) {
                if (element.renderPdf && (save == 1 || save == 2 || save == 3)) {
                    elements_count++;
                }else if (element.renderPreview && save != 1 && save != 2 && save != 3) {
                    elements_count++;
                }
            });

            angular.forEach(site.elements,function(element, ie){
                if(element.renderPdf && (save == 1 || save == 2 || save == 3)) {

                    if(element.json) {
                        element.paint($rootScope, site, paper, element, 0, 0, element.width, element.height, element.selectable);
                    }else{
                        element.paint($rootScope, site, paper, element, element.x+site.trimLeft, element.y+site.trimTop, element.width, element.height, element.selectable);
                    }
                }else if(element.renderPreview && save != 1 && save != 2 && save != 3) {

                    if(element.json) {
                        element.paint($rootScope, site, paper, element, 0, 0, element.width, element.height, element.selectable);
                    }else{
                        element.paint($rootScope, site, paper, element, element.x+site.trimLeft, element.y+site.trimTop, element.width, element.height, element.selectable);
                    }
                }


            });

            if(elements_count == 1) {
                paper.renderAll();
            }

        }

    };


    $scope.$on('settings.zoom.set', function(event, v) {
        $scope.settings.zoom = v;
        $scope.settings.zoomPercent = window.math.round(v*100);
    });

    $scope.$on('settings.saveProduct', function(event, v) {
        $scope.settings.saveProduct = v;
    });

    $scope.$on('settings.printPdf', function(event, v) {
        $scope.settings.show_print_pdf = v;
    });

    $scope.$on('settings.load', function($scope, obj) {
        if(obj.general.buttons.pdfpreview) {
            $scope.currentScope.settings.show_preview_pdf = true;
        }else{
            $scope.currentScope.settings.show_preview_pdf = false;
        }
        if(obj.general.buttons.preview) {
            $scope.currentScope.settings.show_preview = true;
        }else{
            $scope.currentScope.settings.show_preview = false;
        }
        if(obj.general.buttons.next) {
            $scope.currentScope.settings.show_next = true;
        }else{
            $scope.currentScope.settings.show_next = false;
        }
        if(obj.general.buttons.prev) {
            $scope.currentScope.settings.show_prev = true;
        }else{
            $scope.currentScope.settings.show_prev = false;
        }
        if(obj.general.buttons.finish && dataService.backendMode != true) {
            $scope.currentScope.settings.show_finish = true;
        }else{
            $scope.currentScope.settings.show_finish = false;
        }

    });
}).directive('showProductSettings', function(){
        return {
            restrict: 'A',
            link: function(scope, elem, attrs){
                return scope.settings.showProductSettings;
            }
        }
    })

;
