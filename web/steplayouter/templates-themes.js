angular.module("templates-themes", ["themes/astov/app/footer/footer.tpl.html", "themes/astov/app/help/help.tpl.html", "themes/astov/app/motiv/motiv.tpl.html", "themes/astov/app/painter/painter.tpl.html", "themes/astov/app/settings/calsettings.tpl.html", "themes/astov/app/settings/settings.tpl.html", "themes/astov/app/sites/sites.tpl.html", "themes/astov/app/tools/tools.tpl.html", "themes/astov/app/top/top.tpl.html", "themes/astov/app/wizard/wizard.tpl.html", "themes/dp/app/footer/footer.tpl.html", "themes/dp/app/motiv/motiv.tpl.html", "themes/dp/app/painter/painter.tpl.html", "themes/dp/app/settings/calsettings.tpl.html", "themes/dp/app/settings/settings.tpl.html", "themes/dp/app/sites/sites.tpl.html", "themes/dp/app/tools/tools.tpl.html", "themes/dp/app/top/top.tpl.html", "themes/dp/app/wizard/wizard.tpl.html", "themes/gz/app/footer/footer.tpl.html", "themes/gz/app/help/help.tpl.html", "themes/gz/app/motiv/motiv.tpl.html", "themes/gz/app/painter/painter.tpl.html", "themes/gz/app/settings/calsettings.tpl.html", "themes/gz/app/settings/settings.tpl.html", "themes/gz/app/sites/sites.tpl.html", "themes/gz/app/tools/tools.tpl.html", "themes/gz/app/top/top.tpl.html", "themes/gz/app/wizard/wizard.tpl.html", "themes/mypetbooks/app/footer/footer.tpl.html", "themes/mypetbooks/app/help/help.tpl.html", "themes/mypetbooks/app/motiv/motiv.tpl.html", "themes/mypetbooks/app/painter/painter.tpl.html", "themes/mypetbooks/app/settings/calsettings.tpl.html", "themes/mypetbooks/app/settings/settings.tpl.html", "themes/mypetbooks/app/sites/sites.tpl.html", "themes/mypetbooks/app/tools/tools.tpl.html", "themes/mypetbooks/app/top/top.tpl.html", "themes/mypetbooks/app/wizard/wizard.tpl.html", "themes/niceart/app/footer/footer.tpl.html", "themes/niceart/app/motiv/motiv.tpl.html", "themes/niceart/app/painter/painter.tpl.html", "themes/niceart/app/settings/calsettings.tpl.html", "themes/niceart/app/settings/settings.tpl.html", "themes/niceart/app/sites/sites.tpl.html", "themes/niceart/app/tools/tools.tpl.html", "themes/niceart/app/top/top.tpl.html", "themes/niceart/app/wizard/wizard.tpl.html", "themes/psc_kdv/app/footer/footer.tpl.html", "themes/psc_kdv/app/help/help.tpl.html", "themes/psc_kdv/app/motiv/motiv.tpl.html", "themes/psc_kdv/app/painter/painter.tpl.html", "themes/psc_kdv/app/settings/calsettings.tpl.html", "themes/psc_kdv/app/settings/settings.tpl.html", "themes/psc_kdv/app/sites/sites.tpl.html", "themes/psc_kdv/app/tools/tools.tpl.html", "themes/psc_kdv/app/top/top.tpl.html", "themes/psc_kdv/app/wizard/wizard.tpl.html", "themes/psc/app/footer/footer.tpl.html", "themes/psc/app/help/help.tpl.html", "themes/psc/app/motiv/motiv.tpl.html", "themes/psc/app/painter/painter.tpl.html", "themes/psc/app/settings/calsettings.tpl.html", "themes/psc/app/settings/settings.tpl.html", "themes/psc/app/sites/sites.tpl.html", "themes/psc/app/tools/tools.tpl.html", "themes/psc/app/top/top.tpl.html", "themes/psc/app/wizard/wizard.tpl.html", "themes/sweet/app/footer/footer.tpl.html", "themes/sweet/app/help/help.tpl.html", "themes/sweet/app/motiv/motiv.tpl.html", "themes/sweet/app/painter/painter.tpl.html", "themes/sweet/app/settings/calsettings.tpl.html", "themes/sweet/app/settings/settings.tpl.html", "themes/sweet/app/sites/sites.tpl.html", "themes/sweet/app/tools/tools.tpl.html", "themes/sweet/app/top/top.tpl.html", "themes/sweet/app/wizard/wizard.tpl.html"]);

angular.module("themes/astov/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/footer/footer.tpl.html",
    "<footer id=\"niceartpagefooter\">\n" +
    "    <nav id=\"navbarniceartpagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div ng-include=\"'themes/astov/app/help/help.tpl.html'\" style=\"float: left;\" ng-controller=\"HelpCtrl\" data-ng-show=\"showHelpButton\"></div>\n" +
    "            <!-- <button class=\"btn btn-danger navbar-btn\" title=\"Alle Personalisierungen aufgeben und von vorne beginnen\" data-toggle=\"modal\" data-target=\"#askBeforeKillModal\"><i class=\"fa fa-ban fa-fw\"></i> Neustart</button> -->\n" +
    "            <!-- <button class=\"btn btn-default navbar-btn\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</button> -->\n" +
    "            <!-- <button type=\"button\" class=\"btn btn-default navbar-btn goInfoTour\"><i class=\"fa fa-info-circle\"></i> Tour starten</button> -->\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <!-- <button class=\"btn btn-default btn-lg navbar-right navbar-btn\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button> -->\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>\n" +
    "");
}]);

angular.module("themes/astov/app/help/help.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/help/help.tpl.html",
    "<button class=\"btn btn-info btn-lg\" style=\"margin-top:5px; width: 100%\" ng-click=\"openHelpModal()\"><i class=\"fa fa-question-circle\"></i> Hilfe</button>\n" +
    "<script type=\"text/ng-template\" id=\"helpModal\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hilfe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <img data-ng-src=\"{{url}}\"/>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/astov/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/astov/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/painter/painter.tpl.html",
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite zurück\"><i class=\"fa fa-angle-left fa-5x\"></i></a>\n" +
    "</div>\n" +
    "<div id=\"niceart-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite vor\"><i class=\"fa fa-angle-right fa-5x\"></i></a>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/astov/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2014\">2014</option>\n" +
    "                    <option value=\"2015\">2015</option>\n" +
    "                    <option value=\"2016\">2016</option>\n" +
    "                    <option value=\"2017\">2017</option>\n" +
    "                    <option value=\"2018\">2018</option>\n" +
    "                    <option value=\"2019\">2019</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/astov/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.altLabel }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/astov/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/sites/sites.tpl.html",
    "<nav id=\"monthNav\" uib-dropdown ng-class=\"{true:'btn-group', false:''}[settings.showSiteBrowser]\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" uib-dropdown-toggle>{{title()}} &nbsp;\n" +
    "        <span class=\"fa fa-angle-down\"></span></button>\n" +
    "    <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "        <li class=\"col-lg-1 col-sm-3 col-xs-6 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"media\">\n" +
    "                <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                <div class=\"media-body\">\n" +
    "                    <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</nav>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/astov/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-md-12\" style=\"margin-top: 5px\">\n" +
    "        <button class=\"btn btn-info btn-block\" data-ng-controller=\"MotivCtrl\" ng-click=\"openClipartGallery(false)\">Stilelemente</button>\n" +
    "    </div>\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/astov/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/astov/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[showTextTools]\"></i>\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"niceartTxtTools\" role=\"form\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font.name}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptTxt\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-success center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptImage\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-lg\" title=\"In den Vordergrund\"><i class=\"icon-bring-to-front\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-lg\" title=\"In den Hintergrund\"><i class=\"icon-send-to-back\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("themes/astov/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/top/top.tpl.html",
    "<header id=\"niceartpageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"navbar-header\">\n" +
    "                        <span class=\"logo\"></span>\n" +
    "                        <a class=\"navbar-brand\" href=\"#\" title=\"HTML-Layouter\">{{title}}</a>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group dropdown\" uib-dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                        <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" uib-dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                        <div class=\"btn-group dropdown\" dropdown>\n" +
    "                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                            <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                                <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"siteBrowser\" style=\"display: inline\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/astov/app/sites/sites.tpl.html'\" ></div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2\">\n" +
    "                    <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                        <li><a class=\"niceart-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>\n" +
    "                        <li><a class=\"niceart-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"orderNr\" id=\"orderNr\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/astov/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/astov/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/dp/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/footer/footer.tpl.html",
    "<footer id=\"dp24pagefooter\">\n" +
    "    <nav id=\"navbarDp24pagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <span class=\"navbar-btn navbar-left text-muted\"><small>dp24 Tmpl v.1.2 | Layouter v.1.0.1</small></span>\n" +
    "            <!-- <button class=\"btn btn-danger navbar-btn\" title=\"Alle Personalisierungen aufgeben und von vorne beginnen\" data-toggle=\"modal\" data-target=\"#askBeforeKillModal\"><i class=\"fa fa-ban fa-fw\"></i> Neustart</button> -->\n" +
    "            <!-- <button class=\"btn btn-default navbar-btn\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</button> -->\n" +
    "            <!-- <button type=\"button\" class=\"btn btn-default navbar-btn goInfoTour\"><i class=\"fa fa-info-circle\"></i> Tour starten</button> -->\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <!-- <button class=\"btn btn-default btn-lg navbar-right navbar-btn\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button> -->\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>");
}]);

angular.module("themes/dp/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <pagination total-items=\"myCount\" ng-model=\"myPage\" ng-change=\"myPageChanged()\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></pagination>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <pagination total-items=\"count\" ng-model=\"page\" ng-change=\"pageChanged()\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></pagination>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"25\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></pagination>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"25\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></pagination>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/dp/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/painter/painter.tpl.html",
    "<header class=\"row siteBrowser\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/dp/app/sites/sites.tpl.html'\" ></header>\n" +
    "\n" +
    "<div id=\"dp24-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/dp/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2014\">2014</option>\n" +
    "                    <option value=\"2015\">2015</option>\n" +
    "                    <option value=\"2016\">2016</option>\n" +
    "                    <option value=\"2017\">2017</option>\n" +
    "                    <option value=\"2018\">2018</option>\n" +
    "                    <option value=\"2019\">2019</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/dp/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.label }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/dp/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/sites/sites.tpl.html",
    "<div class=\"col-lg-12 text-center\">\n" +
    "    <div id=\"pageNav\" role=\"toolbar\">\n" +
    "            <button type=\"button\" class=\"btn btn-link\" ng-click=\"selectSite(settings.showSiteBrowserPrevButton.id)\" data-ng-show=\"settings.showSiteBrowserPrevButton.show\"><i class=\"fa fa-angle-left fa-3x\"></i></button>\n" +
    "            <button type=\"button\" class=\"btn btn-link\" ng-click=\"selectSite(settings.showSiteBrowserPrevItem2.id)\" data-ng-show=\"settings.showSiteBrowserPrevItem2.show\">{{settings.showSiteBrowserPrevItem2.text}}</button>\n" +
    "            <button type=\"button\" class=\"btn btn-link\" ng-click=\"selectSite(settings.showSiteBrowserPrevItem1.id)\" data-ng-show=\"settings.showSiteBrowserPrevItem1.show\">{{settings.showSiteBrowserPrevItem1.text}}</button>\n" +
    "            <nav id=\"monthNav\" dropdown class=\"btn-group\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "                <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" dropdown-toggle>{{title()}} &nbsp;\n" +
    "                    <span class=\"caret\"></span></button>\n" +
    "                <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "                    <li class=\"col-lg-1 col-sm-3 col-xs-6 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "                        <div class=\"media\">\n" +
    "                            <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                            <div class=\"media-body\">\n" +
    "                                <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </li>\n" +
    "\n" +
    "                </ul>\n" +
    "            </nav>\n" +
    "            <button type=\"button\" class=\"btn btn-link\" ng-click=\"selectSite(settings.showSiteBrowserNextItem1.id)\" data-ng-show=\"settings.showSiteBrowserNextItem1.show\">{{settings.showSiteBrowserNextItem1.text}}</button>\n" +
    "            <button type=\"button\" class=\"btn btn-link\" ng-click=\"selectSite(settings.showSiteBrowserNextItem2.id)\" data-ng-show=\"settings.showSiteBrowserNextItem2.show\">{{settings.showSiteBrowserNextItem2.text}}</button>\n" +
    "            <button type=\"button\" class=\"btn btn-link\" ng-click=\"selectSite(settings.showSiteBrowserNextButton.id)\" data-ng-show=\"settings.showSiteBrowserNextButton.show\"><i class=\"fa fa-angle-right fa-3x\"></i></button>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/dp/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }} Auswählen</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }} Auswählen</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/dp/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/dp/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[showTextTools]\"></i>\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"dp24TxtTools\" role=\"form\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"dp24ptTxt\" class=\"btn-group btn-group-justified dp24-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-success center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"dp24ptImage\" class=\"btn-group btn-group-justified dp24-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-lg\" title=\"In den Vordergrund\"><i class=\"icon-bring-to-front\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-lg\" title=\"In den Hintergrund\"><i class=\"icon-send-to-back\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/dp/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/top/top.tpl.html",
    "<header id=\"dp24pageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"navbar-header\">\n" +
    "                <a class=\"navbar-brand\" href=\"#\" title=\"directprint24.de HTML-Layouter\">{{title}}</a>\n" +
    "            </div>\n" +
    "\n" +
    "            <div class=\"collapse navbar-collapse\" id=\"dp24-top-nav-collapse-1\"> <!-- collapse container -->\n" +
    "                <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                    <li><a class=\"dp24-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>\n" +
    "                    <li><a class=\"dp24-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                </ul>\n" +
    "                \n" +
    "                <div class=\"btn-group dropdown\" dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                    <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                        <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                        <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group\">\n" +
    "                    <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                    <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                    <div class=\"btn-group dropdown\" dropdown>\n" +
    "                        <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/dp/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/dp/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/gz/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/footer/footer.tpl.html",
    "<footer id=\"niceartpagefooter\">\n" +
    "    <nav id=\"navbarniceartpagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <a href=\"/cms/hilfe-creativelayouter\" target=\"_blank\" class=\"btn btn-default btn-lg navbar-btn\"><i class=\"fa fa-question-circle fa-fw\"></i> Hilfe</a>\n" +
    "            <div ng-include=\"'themes/gz/app/help/help.tpl.html'\" style=\"float: left;\" ng-controller=\"HelpCtrl\" data-ng-show=\"showHelpButton\"></div>\n" +
    "            <!-- <button class=\"btn btn-danger navbar-btn\" title=\"Alle Personalisierungen aufgeben und von vorne beginnen\" data-toggle=\"modal\" data-target=\"#askBeforeKillModal\"><i class=\"fa fa-ban fa-fw\"></i> Neustart</button> -->\n" +
    "            <!-- <button class=\"btn btn-default navbar-btn\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</button> -->\n" +
    "            <!-- <button type=\"button\" class=\"btn btn-default navbar-btn goInfoTour\"><i class=\"fa fa-info-circle\"></i> Tour starten</button> -->\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <!-- <button class=\"btn btn-default btn-lg navbar-right navbar-btn\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button> -->\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>\n" +
    "");
}]);

angular.module("themes/gz/app/help/help.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/help/help.tpl.html",
    "<button class=\"btn btn-info btn-lg\" style=\"margin-top:5px; width: 100%\" ng-click=\"openHelpModal()\"><i class=\"fa fa-question-circle\"></i> Hilfe</button>\n" +
    "<script type=\"text/ng-template\" id=\"helpModal\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hilfe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <img data-ng-src=\"{{url}}\"/>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/gz/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/gz/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/painter/painter.tpl.html",
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite zurück\"><i class=\"fa fa-angle-left fa-5x\"></i></a>\n" +
    "</div>\n" +
    "<div id=\"niceart-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite vor\"><i class=\"fa fa-angle-right fa-5x\"></i></a>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/gz/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2020\">2020</option>\n" +
    "                    <option value=\"2021\">2021</option>\n" +
    "                    <option value=\"2022\">2022</option>\n" +
    "                    <option value=\"2023\">2023</option>\n" +
    "                    <option value=\"2024\">2024</option>\n" +
    "                    <option value=\"2025\">2025</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/gz/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.altLabel }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/gz/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/sites/sites.tpl.html",
    "<nav id=\"monthNav\" uib-dropdown ng-class=\"{true:'btn-group', false:''}[settings.showSiteBrowser]\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" uib-dropdown-toggle>{{title()}} &nbsp;\n" +
    "        <span class=\"fa fa-angle-down\"></span></button>\n" +
    "    <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "        <li class=\"col-lg-3 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"media\">\n" +
    "                <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                <div class=\"media-body\">\n" +
    "                    <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</nav>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/gz/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-md-12\" style=\"margin-top: 5px\">\n" +
    "        <button class=\"btn btn-info btn-block\" data-ng-controller=\"MotivCtrl\" ng-click=\"openClipartGallery(false)\">Stilelemente</button>\n" +
    "    </div>\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/gz/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/gz/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[showTextTools]\"></i>\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"niceartTxtTools\" role=\"form\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-12\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label for=\"text\">Text</label>\n" +
    "                        <textarea class=\"form-control\" name=\"Text\" ng-model=\"settings.text\" rows=\"3\"></textarea>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font.name}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptTxt\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-primary center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-primary center-block\" ng-show=\"settings.showDownload\"  ng-controller=\"MotivCtrl\" ng-click=\"downloadMotiv()\" title=\"Motiv Download\"><i class=\"fa fa-cloud-download\"></i> Motiv Download</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptImage\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-lg\" title=\"In den Vordergrund\"><i class=\"icon-bring-to-front\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-lg\" title=\"In den Hintergrund\"><i class=\"icon-send-to-back\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <div class=\"helpText\"  ng-bind-html=\"help\" data-html=\"true\"></div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/gz/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/top/top.tpl.html",
    "<header id=\"niceartpageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"navbar-header\">\n" +
    "                        <span class=\"logo\"></span>\n" +
    "                        <a class=\"navbar-brand \" href=\"#\" title=\"HTML-Layouter\">{{title}}</a>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group dropdown\" uib-dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                        <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" uib-dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.2)\">220%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.5)\">250%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.7)\">270%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3)\">300%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3.2)\">320%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3.5)\">350%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3.7)\">370%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(4)\">400%</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                        <div class=\"btn-group dropdown\" dropdown>\n" +
    "                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                            <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                                <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"siteBrowser\" style=\"display: inline; float:left\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/psc/app/sites/sites.tpl.html'\" ></div>\n" +
    "                    <h4 class=\"nagger__title\">Der Shop des Medienzentrum Günzburg</h4>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                        <!--<li><a class=\"niceart-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>-->\n" +
    "                        <li><a class=\"niceart-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"orderNr\" id=\"orderNr\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/gz/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/gz/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/mypetbooks/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/footer/footer.tpl.html",
    "<footer id=\"niceartpagefooter\">\n" +
    "    <nav id=\"navbarniceartpagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div ng-include=\"'themes/mypetbooks/app/help/help.tpl.html'\" style=\"float: left;\" ng-controller=\"HelpCtrl\" data-ng-show=\"showHelpButton\"></div>\n" +
    "            <!-- <button class=\"btn btn-danger navbar-btn\" title=\"Alle Personalisierungen aufgeben und von vorne beginnen\" data-toggle=\"modal\" data-target=\"#askBeforeKillModal\"><i class=\"fa fa-ban fa-fw\"></i> Neustart</button> -->\n" +
    "            <!-- <button class=\"btn btn-default navbar-btn\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</button> -->\n" +
    "            <!-- <button type=\"button\" class=\"btn btn-default navbar-btn goInfoTour\"><i class=\"fa fa-info-circle\"></i> Tour starten</button> -->\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <!-- <button class=\"btn btn-default btn-lg navbar-right navbar-btn\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button> -->\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>\n" +
    "");
}]);

angular.module("themes/mypetbooks/app/help/help.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/help/help.tpl.html",
    "<button class=\"btn btn-info btn-lg\" style=\"margin-top:5px; width: 100%\" ng-click=\"openHelpModal()\"><i class=\"fa fa-question-circle\"></i> Hilfe</button>\n" +
    "<script type=\"text/ng-template\" id=\"helpModal\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hilfe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <img data-ng-src=\"{{url}}\"/>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/mypetbooks/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/mypetbooks/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/painter/painter.tpl.html",
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite zurück\"><i class=\"fa fa-angle-left fa-5x\"></i></a>\n" +
    "</div>\n" +
    "<div id=\"niceart-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite vor\"><i class=\"fa fa-angle-right fa-5x\"></i></a>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/mypetbooks/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2014\">2014</option>\n" +
    "                    <option value=\"2015\">2015</option>\n" +
    "                    <option value=\"2016\">2016</option>\n" +
    "                    <option value=\"2017\">2017</option>\n" +
    "                    <option value=\"2018\">2018</option>\n" +
    "                    <option value=\"2019\">2019</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/mypetbooks/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.altLabel }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/mypetbooks/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/sites/sites.tpl.html",
    "<nav id=\"monthNav\" uib-dropdown ng-class=\"{true:'btn-group', false:''}[settings.showSiteBrowser]\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" uib-dropdown-toggle>{{title()}} &nbsp;\n" +
    "        <span class=\"fa fa-angle-down\"></span></button>\n" +
    "    <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "        <li class=\"col-lg-1 col-sm-3 col-xs-6 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"media\">\n" +
    "                <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                <div class=\"media-body\">\n" +
    "                    <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</nav>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/mypetbooks/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-md-12\" style=\"margin-top: 5px\">\n" +
    "        <button class=\"btn btn-info btn-block\" data-ng-controller=\"MotivCtrl\" ng-click=\"openClipartGallery(false)\">Stilelemente</button>\n" +
    "    </div>\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/mypetbooks/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/mypetbooks/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[showTextTools]\"></i>\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"niceartTxtTools\" role=\"form\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font.name}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptTxt\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-success center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptImage\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-lg\" title=\"In den Vordergrund\"><i class=\"icon-bring-to-front\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-lg\" title=\"In den Hintergrund\"><i class=\"icon-send-to-back\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("themes/mypetbooks/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/top/top.tpl.html",
    "<header id=\"niceartpageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"navbar-header\">\n" +
    "                        <span class=\"logo\"></span>\n" +
    "                        <a class=\"navbar-brand\" href=\"#\" title=\"HTML-Layouter\">{{title}}</a>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group dropdown\" uib-dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                        <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" uib-dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                        <div class=\"btn-group dropdown\" dropdown>\n" +
    "                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                            <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                                <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"siteBrowser\" style=\"display: inline\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/astov/app/sites/sites.tpl.html'\" ></div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2\">\n" +
    "                    <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                        <li><a class=\"niceart-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>\n" +
    "                        <li><a class=\"niceart-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"orderNr\" id=\"orderNr\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/mypetbooks/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/mypetbooks/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/niceart/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/footer/footer.tpl.html",
    "<footer id=\"niceartpagefooter\">\n" +
    "    <nav id=\"navbarniceartpagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <!-- <button class=\"btn btn-danger navbar-btn\" title=\"Alle Personalisierungen aufgeben und von vorne beginnen\" data-toggle=\"modal\" data-target=\"#askBeforeKillModal\"><i class=\"fa fa-ban fa-fw\"></i> Neustart</button> -->\n" +
    "            <!-- <button class=\"btn btn-default navbar-btn\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</button> -->\n" +
    "            <!-- <button type=\"button\" class=\"btn btn-default navbar-btn goInfoTour\"><i class=\"fa fa-info-circle\"></i> Tour starten</button> -->\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <!-- <button class=\"btn btn-default btn-lg navbar-right navbar-btn\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button> -->\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>\n" +
    "");
}]);

angular.module("themes/niceart/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"12\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"12\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/niceart/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/painter/painter.tpl.html",
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite zurück\"><i class=\"fa fa-angle-left fa-5x\"></i></a>\n" +
    "</div>\n" +
    "<div id=\"niceart-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite vor\"><i class=\"fa fa-angle-right fa-5x\"></i></a>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/niceart/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2014\">2014</option>\n" +
    "                    <option value=\"2015\">2015</option>\n" +
    "                    <option value=\"2016\">2016</option>\n" +
    "                    <option value=\"2017\">2017</option>\n" +
    "                    <option value=\"2018\">2018</option>\n" +
    "                    <option value=\"2019\">2019</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/niceart/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.label }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/niceart/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/sites/sites.tpl.html",
    "<nav id=\"monthNav\" uib-dropdown ng-class=\"{true:'btn-group', false:''}[settings.showSiteBrowser]\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" uib-dropdown-toggle>{{title()}} &nbsp;\n" +
    "        <span class=\"fa fa-angle-down\"></span></button>\n" +
    "    <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "        <li class=\"col-lg-1 col-sm-3 col-xs-6 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"media\">\n" +
    "                <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                <div class=\"media-body\">\n" +
    "                    <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</nav>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/niceart/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }} Auswählen</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }} Auswählen</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/dp/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/dp/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[showTextTools]\"></i>\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"niceartTxtTools\" role=\"form\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptTxt\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-success center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptImage\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-lg\" title=\"In den Vordergrund\"><i class=\"icon-bring-to-front\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-lg\" title=\"In den Hintergrund\"><i class=\"icon-send-to-back\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("themes/niceart/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/top/top.tpl.html",
    "<header id=\"niceartpageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"navbar-header\">\n" +
    "                        <a class=\"navbar-brand\" href=\"#\" title=\"directprint24.de HTML-Layouter\">{{title}}</a>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group dropdown\" uib-dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                        <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" uib-dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                        <div class=\"btn-group dropdown\" dropdown>\n" +
    "                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                            <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                                <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"siteBrowser\" style=\"display: inline\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/niceart/app/sites/sites.tpl.html'\" ></div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2\">\n" +
    "                    <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                        <li><a class=\"niceart-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>\n" +
    "                        <li><a class=\"niceart-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"orderNr\" id=\"orderNr\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/niceart/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/niceart/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc_kdv/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/footer/footer.tpl.html",
    "<footer id=\"niceartpagefooter\">\n" +
    "    <nav id=\"navbarniceartpagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <a class=\"btn btn-info btn-lg\" style=\"margin-top:5px;float: left\" href=\"/cms/layouterhilfe\" target=\"_blank\"><i class=\"fa fa-question-circle\"></i> Hilfe / FAQ</a>\n" +
    "            <div ng-include=\"'themes/psc/app/help/help.tpl.html'\" style=\"float: left;\" ng-controller=\"HelpCtrl\" data-ng-show=\"showHelpButton\"></div>\n" +
    "            <div style=\"width: 70%;float: left;padding-left: 10px;padding-top: 8px;\">Hinweis: Die Verwendung von Schießscheiben oder Zielen, die Personen oder in ihrer äußeren Form menschliche Silhouetten darstellen, fällt unter das \"kampfmäßige Schießen\". Dieses ist im Schießsport in Deutschland laut Waffenrecht verboten. Das Angebot solcher Scheiben richtet sich nur an einen Personenkreis, der dieser Einschränkung nicht unterliegt.</div>\n" +
    "            <!-- <button class=\"btn btn-danger navbar-btn\" title=\"Alle Personalisierungen aufgeben und von vorne beginnen\" data-toggle=\"modal\" data-target=\"#askBeforeKillModal\"><i class=\"fa fa-ban fa-fw\"></i> Neustart</button> -->\n" +
    "            <!-- <button class=\"btn btn-default navbar-btn\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</button> -->\n" +
    "            <!-- <button type=\"button\" class=\"btn btn-default navbar-btn goInfoTour\"><i class=\"fa fa-info-circle\"></i> Tour starten</button> -->\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <!-- <button class=\"btn btn-default btn-lg navbar-right navbar-btn\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button> -->\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>\n" +
    "");
}]);

angular.module("themes/psc_kdv/app/help/help.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/help/help.tpl.html",
    "<button class=\"btn btn-info btn-lg\" style=\"margin-top:5px; width: 100%\" ng-click=\"openHelpModal()\"><i class=\"fa fa-question-circle\"></i> Hilfe</button>\n" +
    "<script type=\"text/ng-template\" id=\"helpModal\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hilfe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <img data-ng-src=\"{{url}}\"/>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc_kdv/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/><br/>\n" +
    "                                {{ motiv.title }}\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/><br/>\n" +
    "                                {{ motiv.title }}\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-4\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/><br/>\n" +
    "                                {{ motiv.title }}\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-4\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/><br/>\n" +
    "                                {{ motiv.title }}\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc_kdv/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/painter/painter.tpl.html",
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite zurück\"><i class=\"fa fa-angle-left fa-5x\"></i></a>\n" +
    "</div>\n" +
    "<div id=\"niceart-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite vor\"><i class=\"fa fa-angle-right fa-5x\"></i></a>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/psc_kdv/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2020\">2020</option>\n" +
    "                    <option value=\"2021\">2021</option>\n" +
    "                    <option value=\"2022\">2022</option>\n" +
    "                    <option value=\"2023\">2023</option>\n" +
    "                    <option value=\"2024\">2024</option>\n" +
    "                    <option value=\"2025\">2025</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/psc_kdv/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.altLabel }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-4\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-4\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc_kdv/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/sites/sites.tpl.html",
    "<nav id=\"monthNav\" uib-dropdown ng-class=\"{true:'btn-group', false:''}[settings.showSiteBrowser]\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" uib-dropdown-toggle>{{title()}} &nbsp;\n" +
    "        <span class=\"fa fa-angle-down\"></span></button>\n" +
    "    <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "        <li class=\"col-lg-3 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"media\">\n" +
    "                <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                <div class=\"media-body\">\n" +
    "                    <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</nav>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/psc_kdv/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-md-12\" style=\"margin-top: 20px\">\n" +
    "        <button class=\"btn btn-info btn-block\" data-ng-controller=\"MotivCtrl\" ng-click=\"openClipartGallery(false)\">Stilelemente</button>\n" +
    "    </div>\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 10px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 10px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/psc_kdv/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/psc_kdv/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"niceartTxtTools\" role=\"form\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-12\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label for=\"text\">Text</label>\n" +
    "                        <textarea class=\"form-control\" name=\"Text\" ng-model=\"settings.text\" rows=\"3\"></textarea>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font.name}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptTxt\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-primary center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-primary center-block\" ng-show=\"settings.showDownload\"  ng-controller=\"MotivCtrl\" ng-click=\"downloadMotiv()\" title=\"Motiv Download\"><i class=\"fa fa-cloud-download\"></i> Motiv Download</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\" ng-show=\"settings.showDelete\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptImage\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("themes/psc_kdv/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/top/top.tpl.html",
    "<header id=\"niceartpageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"navbar-header\">\n" +
    "                        <a class=\"navbar-brand\" href=\"#\" title=\"HTML-Layouter\">{{title}}</a>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group dropdown\" uib-dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                        <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" uib-dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                        <div class=\"btn-group dropdown\" dropdown>\n" +
    "                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                            <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                                <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"siteBrowser\" style=\"display: inline\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/psc/app/sites/sites.tpl.html'\" ></div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2\">\n" +
    "                    <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                        <!--<li><a class=\"niceart-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>-->\n" +
    "                        <li><a class=\"niceart-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"orderNr\" id=\"orderNr\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/psc_kdv/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc_kdv/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/footer/footer.tpl.html",
    "<footer id=\"niceartpagefooter\">\n" +
    "    <nav id=\"navbarniceartpagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div ng-include=\"'themes/psc/app/help/help.tpl.html'\" style=\"float: left;\" ng-controller=\"HelpCtrl\" data-ng-show=\"showHelpButton\"></div>\n" +
    "            <!-- <button class=\"btn btn-danger navbar-btn\" title=\"Alle Personalisierungen aufgeben und von vorne beginnen\" data-toggle=\"modal\" data-target=\"#askBeforeKillModal\"><i class=\"fa fa-ban fa-fw\"></i> Neustart</button> -->\n" +
    "            <!-- <button class=\"btn btn-default navbar-btn\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</button> -->\n" +
    "            <!-- <button type=\"button\" class=\"btn btn-default navbar-btn goInfoTour\"><i class=\"fa fa-info-circle\"></i> Tour starten</button> -->\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <!-- <button class=\"btn btn-default btn-lg navbar-right navbar-btn\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button> -->\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>\n" +
    "");
}]);

angular.module("themes/psc/app/help/help.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/help/help.tpl.html",
    "<button class=\"btn btn-info btn-lg\" style=\"margin-top:5px; width: 100%\" ng-click=\"openHelpModal()\"><i class=\"fa fa-question-circle\"></i> Hilfe</button>\n" +
    "<script type=\"text/ng-template\" id=\"helpModal\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hilfe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <img data-ng-src=\"{{url}}\"/>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/painter/painter.tpl.html",
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite zurück\"><i class=\"fa fa-angle-left fa-5x\"></i></a>\n" +
    "</div>\n" +
    "<div id=\"niceart-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite vor\"><i class=\"fa fa-angle-right fa-5x\"></i></a>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/psc/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2020\">2020</option>\n" +
    "                    <option value=\"2021\">2021</option>\n" +
    "                    <option value=\"2022\">2022</option>\n" +
    "                    <option value=\"2023\">2023</option>\n" +
    "                    <option value=\"2024\">2024</option>\n" +
    "                    <option value=\"2025\">2025</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/psc/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.altLabel }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/psc/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/sites/sites.tpl.html",
    "<nav id=\"monthNav\" uib-dropdown ng-class=\"{true:'btn-group', false:''}[settings.showSiteBrowser]\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" uib-dropdown-toggle>{{title()}} &nbsp;\n" +
    "        <span class=\"fa fa-angle-down\"></span></button>\n" +
    "    <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "        <li class=\"col-lg-3 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"media\">\n" +
    "                <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                <div class=\"media-body\">\n" +
    "                    <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</nav>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/psc/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-md-12\" style=\"margin-top: 5px\">\n" +
    "        <button class=\"btn btn-info btn-block\" data-ng-controller=\"MotivCtrl\" ng-click=\"openClipartGallery(false)\">Stilelemente</button>\n" +
    "    </div>\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/psc/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/psc/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[showTextTools]\"></i>\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"niceartTxtTools\" role=\"form\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-12\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label for=\"text\">Text</label>\n" +
    "                        <textarea class=\"form-control\" name=\"Text\" ng-model=\"settings.text\" rows=\"3\"></textarea>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font.name}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptTxt\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-primary center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-primary center-block\" ng-show=\"settings.showDownload\"  ng-controller=\"MotivCtrl\" ng-click=\"downloadMotiv()\" title=\"Motiv Download\"><i class=\"fa fa-cloud-download\"></i> Motiv Download</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptImage\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-lg\" title=\"In den Vordergrund\"><i class=\"icon-bring-to-front\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-lg\" title=\"In den Hintergrund\"><i class=\"icon-send-to-back\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("themes/psc/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/top/top.tpl.html",
    "<header id=\"niceartpageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"navbar-header\">\n" +
    "                        <a class=\"navbar-brand\" href=\"#\" title=\"HTML-Layouter\">{{title}}</a>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group dropdown\" uib-dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                        <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" uib-dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.2)\">220%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.5)\">250%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.7)\">270%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3)\">300%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3.2)\">320%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3.5)\">350%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(3.7)\">370%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(4)\">400%</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                        <div class=\"btn-group dropdown\" dropdown>\n" +
    "                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                            <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                                <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"siteBrowser\" style=\"display: inline\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/psc/app/sites/sites.tpl.html'\" ></div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2\">\n" +
    "                    <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                        <!--<li><a class=\"niceart-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>-->\n" +
    "                        <li><a class=\"niceart-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"orderNr\" id=\"orderNr\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/psc/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/psc/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/sweet/app/footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/footer/footer.tpl.html",
    "<footer id=\"niceartpagefooter\">\n" +
    "    <nav id=\"navbarniceartpagefooter\" class=\"navbar navbar-default navbar-fixed-bottom\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div ng-include=\"'themes/sweet/app/help/help.tpl.html'\" style=\"float: left;\" ng-controller=\"HelpCtrl\" data-ng-show=\"showHelpButton\"></div>\n" +
    "            <button id=\"btnNextStep\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success btn-lg navbar-btn navbar-right\">Speichern & fertigstellen <i class=\"fa fa-arrow-right fa-fw\"></i></button>\n" +
    "            <div class=\"navbar-right\" style=\"margin-right: 10px; width: 40%; margin-top: 5px;\"><p>Bitte mit dem Vorschau-Button überprüfen, ob Bilder und gewählte Stilelemente die richtige Qualität haben und ob diese richtig Positioniert sind.</p></div>\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</footer>\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau Fertig</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>\n" +
    "");
}]);

angular.module("themes/sweet/app/help/help.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/help/help.tpl.html",
    "<button class=\"btn btn-info btn-lg\" style=\"margin-top:5px; width: 100%\" ng-click=\"openHelpModal()\"><i class=\"fa fa-question-circle\"></i> Hilfe</button>\n" +
    "<script type=\"text/ng-template\" id=\"helpModal\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hilfe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <img data-ng-src=\"{{url}}\"/>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/sweet/app/motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Upload\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div class=\"col-lg-12 text-center\">\n" +
    "                            <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ngf-select ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                <p>oder</p>\n" +
    "                                <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                        <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                            <div class=\"progress progress-striped active\">\n" +
    "                                <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                    <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                        <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                            Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                        </div></div>\n" +
    "                    </div>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"bigTotalItems\" page=\"bigCurrentPage\" max-size=\"maxSize\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "\n" +
    "\n" +
    "\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel2\"><i class=\"fa fa-cloud-upload fa-lg\"></i> Bild Upload</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-lg-12 text-center\">\n" +
    "                    <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-drop ngf-change=\"onFileSelect($files, true);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                        <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                        <p>oder</p>\n" +
    "                        <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                    <div class=\"progress progress-striped active\">\n" +
    "                        <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                            <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                    Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                </div></div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\">&times;</button>\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <uib-tabset>\n" +
    "\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"count\" ng-model=\"page\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"pageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <ul uib-pagination total-items=\"myCount\" ng-model=\"myPage\" items-per-page=\"24\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\" ng-change=\"myPageChanged()\"></ul>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-danger pull-left\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/sweet/app/painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/painter/painter.tpl.html",
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite zurück\"><i class=\"fa fa-angle-left fa-5x\"></i></a>\n" +
    "</div>\n" +
    "<div id=\"niceart-canvas-body\" class=\"col-sm-10 col-md-10\" scrollable>\n" +
    "    <div class=\"img-responsive\" id=\"c\" style=\"overflow: hidden\"></div>\n" +
    "</div>\n" +
    "<div class=\"col-sm-1 col-md-1\">\n" +
    "    <a type=\"button\" class=\"btn page-controls\" ng-show=\"showSiteBrowser\" title=\"Seite vor\"><i class=\"fa fa-angle-right fa-5x\"></i></a>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "\n" +
    "");
}]);

angular.module("themes/sweet/app/settings/calsettings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/settings/calsettings.tpl.html",
    "<div class=\"modal-body\">\n" +
    "    <h4>Farben</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Tagfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-md-4\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <h4>Einstellungen</h4>\n" +
    "    <div class=\"row\">\n" +
    "        <!--<div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startmonat</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\"ng-model=\"calendarSetting.startMonth\">\n" +
    "                    <option value=\"1\">Januar</option>\n" +
    "                    <option value=\"2\">Februar</option>\n" +
    "                    <option value=\"3\">März</option>\n" +
    "                    <option value=\"4\">April</option>\n" +
    "                    <option value=\"5\">Mai</option>\n" +
    "                    <option value=\"6\">Juni</option>\n" +
    "                    <option value=\"7\">Juli</option>\n" +
    "                    <option value=\"8\">August</option>\n" +
    "                    <option value=\"9\">September</option>\n" +
    "                    <option value=\"10\">Oktober</option>\n" +
    "                    <option value=\"11\">November</option>\n" +
    "                    <option value=\"12\">Dezember</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>-->\n" +
    "        <div class=\"col-md-6\">\n" +
    "            <div class=\"form-group\">\n" +
    "                <label class=\"control-label\">Startjahr</label>\n" +
    "                <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                    <option value=\"2014\">2014</option>\n" +
    "                    <option value=\"2015\">2015</option>\n" +
    "                    <option value=\"2016\">2016</option>\n" +
    "                    <option value=\"2017\">2017</option>\n" +
    "                    <option value=\"2018\">2018</option>\n" +
    "                    <option value=\"2019\">2019</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("themes/sweet/app/settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/settings/settings.tpl.html",
    "<div class=\"panel-body\">\n" +
    "<div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "    <h4>{{ setting.altLabel }}</h4>\n" +
    "    <simplecolorpicker scp-inline=\"false\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Das Erste mal Laden der Motive kann einen Moment dauern. Wir bitten um etwas Geduld.</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("themes/sweet/app/sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/sites/sites.tpl.html",
    "<nav id=\"monthNav\" uib-dropdown ng-class=\"{true:'btn-group', false:''}[settings.showSiteBrowser]\" data-ng-show=\"settings.showSiteBrowser\">\n" +
    "    <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle btn-long\" uib-dropdown-toggle>{{title()}} &nbsp;\n" +
    "        <span class=\"fa fa-angle-down\"></span></button>\n" +
    "    <ul class=\"dropdown-menu text-left\" role=\"menu\" ng-show=\"settings.showSiteBrowser\">\n" +
    "        <li class=\"col-lg-1 col-sm-3 col-xs-6 text-center\" ng-repeat=\"site in sites\" id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"media\">\n" +
    "                <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "                <div class=\"media-body\">\n" +
    "                    <p class=\"media-heading\">{{site.title}}</p>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </li>\n" +
    "\n" +
    "    </ul>\n" +
    "</nav>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/sweet/app/tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/tools/tools.tpl.html",
    "<div class=\"row indent-small\">\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" title=\"Motiv hinzufügen\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(0)\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-picture-o fa-fw fa-lg\"></i>\n" +
    "            <span class=\"hidden-sm\">Bild einfügen</span>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-lg-6 col-md-12 col-xs-6\">\n" +
    "        <button class=\"btn btn-info btn-block\" ng-click=\"addText()\">\n" +
    "            <i class=\"fa fa-plus fa-fw\"></i><i class=\"fa fa-font fa-fw fa-lg\"></i>\n" +
    "            Text einfügen</button>\n" +
    "    </div>\n" +
    "    <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "    <div class=\"col-md-12\" style=\"margin-top: 5px\">\n" +
    "        <button class=\"btn btn-info btn-block\" data-ng-controller=\"MotivCtrl\" ng-click=\"openClipartGallery(false)\">Stilelemente</button>\n" +
    "    </div>\n" +
    "    <span ng-controller=\"SettingsCtrl\">\n" +
    "        <div ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "            <div ng-class=\"motivSelect.id=='appendMotiv'? 'col-md-12' : 'col-md-6'\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openMotivSelect(motivSelect.id)\">{{ motivSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "        <div ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "            <div class=\"col-lg-6 col-md-12 col-xs-6\" style=\"margin-top: 5px\">\n" +
    "                <button class=\"btn btn-info btn-block\" ng-click=\"openSvgSelect(svgSelect.id)\">{{ svgSelect.label }}</button>\n" +
    "            </div>\n" +
    "            <span class=\"visible-md\"><br class=\"clearfix\"></span>\n" +
    "        </div>\n" +
    "\n" +
    "    </span>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowSettingsToolsDetails]\"  data-ng-show=\"showSettingsTools\">\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowSettingsToolsDetails=!isShowSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-tint fa-lg\"></i> Farben\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/sweet/app/settings/settings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[isShowCalendarSettingsToolsDetails]\" data-ng-show=\"showCalendarSettingsTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\" data-ng-click=\"isShowCalendarSettingsToolsDetails=!isShowCalendarSettingsToolsDetails\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[isShowCalendarSettingsToolsDetails]\"></i>\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-calendar fa-lg\"></i> Kalendar\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div ng-include=\"'themes/sweet/app/settings/calsettings.tpl.html'\" ng-controller=\"SettingsCtrl\" data-ng-show=\"isShowCalendarSettingsToolsDetails\"></div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default textTools\" data-ng-class=\"{true:'active', false:''}[showTextTools]\" data-ng-show=\"showTextTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <i class=\"pull-right fa ng-scope fa-chevron-left\" ng-class=\"{true:'fa-chevron-down', false:'fa-chevron-left'}[showTextTools]\"></i>\n" +
    "\n" +
    "        <h4 class=\"panel-title\">\n" +
    "            <i class=\"fa fa-font\"></i> Schriften\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <form id=\"niceartTxtTools\" role=\"form\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftart</label>\n" +
    "                        <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\">\n" +
    "                            <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font.name}}</ui-select-match>\n" +
    "                            <ui-select-choices repeat=\"font in fonts\">\n" +
    "                                <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                            </ui-select-choices>\n" +
    "                        </ui-select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\" data-ng-if=\"settings.picker\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftfarbe</label>\n" +
    "\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"settings.picker\" index=\"settings.color\" scp-colors=\"settings.colors\" scp-current-color=\"settings.color\" scp-color=\"settings.color\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-8\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Schriftgröße</label>\n" +
    "                        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" style=\"display: none\" id=\"tools_size\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">&nbsp;</label>\n" +
    "                        <input type=\"number\" ng-model=\"settings.size\" class=\"form-control\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "\n" +
    "            <!-- BS3 Button Toolbar -->\n" +
    "            <label class=\"btn-group-label\">Stil, Ausrichtug & Anordnung</label>\n" +
    "            <div class=\"btn-toolbar\" role=\"toolbar\">\n" +
    "                <div class=\"btn-group typeface\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><i class=\"fa fa-bold\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\"class=\"btn btn-default\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><i class=\"fa fa-italic\"></i></button>\n" +
    "                </div>\n" +
    "                <div class=\"btn-group text-align\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" ng-class=\"settings.alignLeft\" class=\"btn btn-default\"><i class=\"fa fa-align-left\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" ng-class=\"settings.alignCenter\" class=\"btn btn-default\"><i class=\"fa fa-align-center\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" ng-class=\"settings.alignRight\" class=\"btn btn-default\"><i class=\"fa fa-align-right\"></i></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignJustify()\" ng-class=\"settings.alignJustify\" class=\"btn btn-default\"><i class=\"fa fa-align-justify\"></i></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </form>\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptTxt\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <!--<div class=\"btn-group\">\n" +
    "                        <button type=\"button\" data-container=\"body\" rel=\"popover\" class=\"btn btn-link btn-lg\" data-placement=\"right\" data-html=\"true\" data-content=\"Hier soll ein Slider rein :)\" title=\"Text Schatten\"><i style=\"color:#f9f9f9!important; text-shadow: 1px 1px 3px #000000;\" class=\"fa fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Outline/Kontur\"><i class=\"fa fa-o fa-font\"></i></button>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" class=\"btn btn-link btn-lg\" title=\"Text Hintergrundfarbe\"><i class=\"icon-text-background\"></i></button>\n" +
    "                    </div>-->\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Textobjekt löschen\"><i class=\"fa fa-trash text-danger\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"panel panel-default\" data-ng-class=\"{true:'active', false:''}[showMotivTools]\" data-ng-show=\"showMotivTools\">\n" +
    "\n" +
    "    <div class=\"panel-heading\">\n" +
    "        <h4 class=\"panel-title\">\n" +
    "\n" +
    "                <i class=\"fa fa-picture-o\"></i> Bilder\n" +
    "\n" +
    "        </h4>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"panel-body\">\n" +
    "        <div class=\"row indent-small\">\n" +
    "            <div class=\"col-md-12\">\n" +
    "                <button class=\"btn btn-default btn-success center-block\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod(1)\" title=\"Motiv austauschen\"><i class=\"fa fa-cloud-upload\"></i> Motiv Austauschen</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"panel-footer\">\n" +
    "        <div class=\"row\">\n" +
    "            <div class=\"col-xs-12\">\n" +
    "                <div id=\"niceartptImage\" class=\"btn-group btn-group-justified niceart-panel-toolbar\">\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-link btn-lg\" title=\"Bild löschen\"><i class=\"fa fa-trash text-error\"></i></button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<div class=\"row indent-small\" ng-show=\"showTools\">\n" +
    "    <div class=\"col-lg-12\">\n" +
    "        <h4 class=\"label\">Objekt Ebene ändern</h4>\n" +
    "        <div class=\"btn-group btn-group-justified\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach vorne\"><i class=\"icon-bring-forward\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-lg\" title=\"In den Vordergrund\"><i class=\"icon-bring-to-front\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-lg\" title=\"In den Hintergrund\"><i class=\"icon-send-to-back\"></i></button>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-lg\" title=\"Schrittweise nach hinten\"><i class=\"icon-send-backwards\"></i></button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "");
}]);

angular.module("themes/sweet/app/top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/top/top.tpl.html",
    "<header id=\"niceartpageheader\">\n" +
    "    <nav id=\"top-nav\" class=\"navbar navbar-default navbar-fixed-top\" role=\"navigation\">\n" +
    "        <div class=\"container-fluid\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"navbar-header\">\n" +
    "                        <a class=\"navbar-brand\" href=\"#\" title=\"directprint24.de HTML-Layouter\">{{title}}</a>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group dropdown\" uib-dropdown is-open=\"settings.showZoomSelector\" ng-controller=\"FooterCtrl\">\n" +
    "                        <button type=\"button\" class=\"btn btn-default navbar-btn dropdown-toggle\" uib-dropdown-toggle>{{settings.zoomPercent}}%&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                        <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                            <li><a ng-click=\"changeZoom(0.1)\">10%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.2)\">20%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.3)\">30%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.4)\">40%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.5)\">50%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.6)\">60%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.7)\">70%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.8)\">80%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(0.9)\">90%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.0)\">100%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.1)\">110%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.2)\">120%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.3)\">130%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.4)\">140%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.5)\">150%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.6)\">160%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.7)\">170%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.8)\">180%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(1.9)\">190%</a></li>\n" +
    "                            <li><a ng-click=\"changeZoom(2.0)\">200%</a></li>\n" +
    "                        </ul>\n" +
    "                    </div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" title=\"Eine PDF Vorschau deines Designs\">Vorschau <i class=\"fa fa-file-pdf-o fa-fw\"></i></button>\n" +
    "                        <button class=\"btn btn-default\" ng-controller=\"FooterCtrl\" ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</button>\n" +
    "                    </div>\n" +
    "\n" +
    "                    <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                        <div class=\"btn-group dropdown\" dropdown>\n" +
    "                            <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                            <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                                <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                            </ul>\n" +
    "                        </div>\n" +
    "                        <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-5\">\n" +
    "                    <div class=\"siteBrowser\" style=\"display: inline\" ng-controller=\"SitesCtrl\" ng-include=\"'themes/dp/app/sites/sites.tpl.html'\" ></div>\n" +
    "                    <div class=\"btn-group\">\n" +
    "                        <h4>Bitte die Stilelemente auf die gewünschte Größe verkleinern</h4>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-2\">\n" +
    "                    <ul class=\"nav navbar-nav navbar-right btn-navbar-icon\">\n" +
    "                        <li><a class=\"niceart-btn-save\" ng-controller=\"FooterCtrl\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" title=\"Speichere jetzt und bearbeite dein Design später weiter\"><i class=\"fa fa-cloud fa-fw\"></i> speichern</a></li>\n" +
    "                        <li><a class=\"niceart-btn-kill\" ng-click=\"closeApplication()\"><i class=\"fa fa-times fa-lg\"></i></a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "    </nav>\n" +
    "</header>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content modal-danger\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <button type=\"button\" class=\"close\" ng-click=\"cancel()\" aria-hidden=\"true\"><i class=\"fa fa-times\"></i></button>\n" +
    "            <h4 class=\"modal-title\" id=\"meinModalLabel3\">NEUSTART</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body text-center\">\n" +
    "            <p><i class=\"fa fa-exclamation fa-5x\"></i>\n" +
    "            </p>\n" +
    "            <p class=\"lead\">Möchtest du wirklich alle Personalisierungen (Anpassungen) aufgeben und von vorne beginnen?</p>\n" +
    "            <p class=\"text-muted\">Beachte bitte das der Stand nicht wieder herstellbar ist - auch nicht von uns.</p>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-click=\"cancel()\"><i class=\"fa fa-times fa-fw\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" class=\"btn btn-success\" ng-click=\"ok()\"><i class=\"fa fa-check fa-fw\"></i> Ok möchte ich</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" name=\"orderNr\" id=\"orderNr\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "");
}]);

angular.module("themes/sweet/app/wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("themes/sweet/app/wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <div class=\"row\">\n" +
    "                            <div class=\"col-lg-12 text-center\">\n" +
    "                                <div id=\"dragdroparea\" class=\"well well-dragdrop\" ngf-drop ng-model-rejected=\"rejFiles\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ngf-select ngf-change=\"changeMotiv($files, element);\" ng-file-drop-available=\"dropSupported=true\">\n" +
    "                                    <p class=\"lead\">Einfach Fotos per Drag & Drop hereinziehen</p>\n" +
    "                                    <p>oder</p>\n" +
    "                                    <p><button type=\"button\" class=\"btn btn-inverse\"><i class=\"fa fa-plus\"></i> Datei auswählen ...</button></p>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"selectedFiles != null\">\n" +
    "                            <div ng-repeat=\"f in selectedFiles\" class=\"col-xs-12\">\n" +
    "                                <div class=\"progress progress-striped active\">\n" +
    "                                    <div class=\"progress-bar\"  role=\"progressbar\" aria-valuenow=\"{{progress[$index]}}\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"width:{{progress[$index]}}%\">\n" +
    "                                        <span class=\"sr-only\">{{progress[$index]}}% Complete</span>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                            <tr>\n" +
    "                                <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                            </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                            <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <h3>{{ element.label }} ({{ selectableMotive.length }} Stück)</h3>\n" +
    "                        <p>\n" +
    "                            <button type=\"button\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\" class=\"btn btn-success\"><i class=\"fa fa-plus\"></i> Foto auswählen ...</button>\n" +
    "                            <button type=\"button\" ng-click=\"clearMotive()\" class=\"btn btn-danger\"><i class=\"fa fa-trash\"></i> Alle Fotos löschen</button>\n" +
    "                        </p>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                        <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                            <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                            </div></div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);
