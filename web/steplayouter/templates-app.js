angular.module("templates-app", ["footer/footer.tpl.html", "help/help.tpl.html", "motiv/motiv.tpl.html", "painter/painter.tpl.html", "settings/settings.tpl.html", "sites/sites.tpl.html", "tools/tools.tpl.html", "top/top.tpl.html", "wizard/wizard.tpl.html"]);

angular.module("footer/footer.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("footer/footer.tpl.html",
    "<div id=\"footer\" class=\"navbar footer\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-6 col-md-4\">\n" +
    "            <div class=\"btn-group\">\n" +
    "                <button type=\"button\" ng-show=\"settings.show_prev\" class=\"btn btn-default\">Zurück</button>\n" +
    "                <button type=\"button\" ng-show=\"settings.show_next\" class=\"btn btn-default\">Weiter</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-6 col-md-4\" style=\"text-align: center\">\n" +
    "            <button type=\"button\" ng-show=\"settings.showProductSettings\" ng-click=\"showProductSettings()\" class=\"btn btn-info\">Einstellungen</button>\n" +
    "            <div class=\"btn-group\">\n" +
    "                <select class=\"form-control\" ng-change=\"changeZoom()\" ng-model=\"settings.zoom\">\n" +
    "                    <option value=\"2.0\">200%</option>\n" +
    "                    <option value=\"1.9\">190%</option>\n" +
    "                    <option value=\"1.8\">180%</option>\n" +
    "                    <option value=\"1.7\">170%</option>\n" +
    "                    <option value=\"1.6\">160%</option>\n" +
    "                    <option value=\"1.5\">150%</option>\n" +
    "                    <option value=\"1.4\">140%</option>\n" +
    "                    <option value=\"1.3\">130%</option>\n" +
    "                    <option value=\"1.2\">120%</option>\n" +
    "                    <option value=\"1.1\">110%</option>\n" +
    "                    <option value=\"1.0\">100%</option>\n" +
    "                    <option value=\"0.9\" >90%</option>\n" +
    "                    <option value=\"0.8\" >80%</option>\n" +
    "                    <option value=\"0.7\" >70%</option>\n" +
    "                    <option value=\"0.6\" >60%</option>\n" +
    "                    <option value=\"0.5\" >50%</option>\n" +
    "                    <option value=\"0.4\" >40%</option>\n" +
    "                    <option value=\"0.3\" >30%</option>\n" +
    "                    <option value=\"0.2\" >20%</option>\n" +
    "                    <option value=\"0.1\" >10%</option>\n" +
    "                </select>\n" +
    "            </div>\n" +
    "            <button type=\"button\" ng-show=\"settings.saveProduct\" ng-click=\"saveProduct()\" class=\"btn btn-warning\">Produkt speichern</button>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"col-xs-6 col-md-4\">\n" +
    "            <div class=\"btn-group pull-right\" id=\"actionBar\">\n" +
    "                <a ng-show=\"settings.show_print_pdf\" ng-click=\"printPDF()\" href=\"\" class=\"btn btn-default\">Print als PDF</a>\n" +
    "                <a ng-show=\"settings.show_preview_pdf\" ng-click=\"previewPDF()\" href=\"\" class=\"btn btn-default\">Vorschau als PDF</a>\n" +
    "                <!--<button type=\"button\" ng-show=\"settings.show_preview\" class=\"btn btn-default\">Vorschau</button>-->\n" +
    "                <button type=\"button\" ng-show=\"settings.show_finish\" ng-click=\"finishProduct()\" class=\"btn btn-success\">Fertigstellen</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPreviewLink\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Vorschau</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "                <a class=\"btn btn-success\" ng-click=\"cancel()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</a>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"showPdfPrint\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Eingabe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <p class=\"text-center\">\n" +
    "            <form>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <label for=\"orderNr\">Auftragsnummer</label>\n" +
    "                    <input type=\"text\" class=\"form-control\" data-ng-model=\"orderNr\" placeholder=\"Auftragsnummer\">\n" +
    "                </div>\n" +
    "                <div class=\"form-group\">\n" +
    "                    <button class=\"btn btn-success\" ng-click=\"print()\" target=\"_blank\" href=\"{{link}}\">Vorschau Anzeigen</button>\n" +
    "                </div>\n" +
    "            </form>\n" +
    "            </p>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<div style=\"position: absolute;\n" +
    "top: -99999999px;\n" +
    "left: -99999999px;\" id=\"previewrenderer_id\"><canvas id=\"previewrenderer\"></canvas></div>");
}]);

angular.module("help/help.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("help/help.tpl.html",
    "<button class=\"btn btn-info\" style=\"margin-top:5px; width: 100%\" ng-click=\"openHelpModal()\"><i class=\"fa fa-question-circle\"></i> Hilfe</button>\n" +
    "<script type=\"text/ng-template\" id=\"helpModal\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hilfe</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <img data-ng-src=\"{{url}}\"/>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("motiv/motiv.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("motiv/motiv.tpl.html",
    "<script type=\"text/ng-template\" id=\"motivMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Wie möchten Sie ein Motiv einfügen?</h4>\n" +
    "        </div>\n" +
    "        <form ng-submit=\"submit()\">\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div class=\"panel-group\" id=\"accordion\">\n" +
    "                    <div class=\"panel panel-default\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title\">\n" +
    "                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">\n" +
    "                                    Vom eigenen Rechner hochladen\n" +
    "                                </a>\n" +
    "                            </h4>\n" +
    "                        </div>\n" +
    "                        <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n" +
    "                            <div class=\"panel-body\">\n" +
    "                                <input type=\"file\" name=\"file\" ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-select ngf-change=\"onFileSelect($files, false)\"/>\n" +
    "                                <div ng-show=\"selectedFiles != null\">\n" +
    "                                    <div class=\"sel-file\" ng-repeat=\"f in selectedFiles\">\n" +
    "                                        <img ng-show=\"dataUrls[$index]\" ng-src=\"{{dataUrls[$index]}}\">\n" +
    "                                        <span class=\"progress\" ng-show=\"progress[$index] >= 0\">\n" +
    "                                            <div style=\"width:{{progress[$index]}}%\">{{progress[$index]}}%</div>\n" +
    "                                        </span><span style=\"float: right\"{{f.name}} - size: {{f.size}}B - type: {{f.type}}</div>\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                                    <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                        Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                                    </div></div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-default\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title\">\n" +
    "                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\">\n" +
    "                                    Aus der Motiv Gallerie\n" +
    "                                </a>\n" +
    "                            </h4>\n" +
    "                        </div>\n" +
    "                        <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n" +
    "                            <div class=\"panel-body\">\n" +
    "                                <button class=\"btn btn-default\" ng-click=\"openMotivGallery(false)\">Motivliste aufrufen</button>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer\">\n" +
    "                <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivEditMethod\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Wie möchten Sie das Motiv ändern?</h4>\n" +
    "        </div>\n" +
    "        <form ng-submit=\"submit()\">\n" +
    "            <div class=\"modal-body\">\n" +
    "                <div class=\"panel-group\" id=\"accordion\">\n" +
    "                    <div class=\"panel panel-default\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title\">\n" +
    "                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseOne\">\n" +
    "                                    Vom eigenen Rechner hochladen\n" +
    "                                </a>\n" +
    "                            </h4>\n" +
    "                        </div>\n" +
    "                        <div id=\"collapseOne\" class=\"panel-collapse collapse in\">\n" +
    "                            <div class=\"panel-body\">\n" +
    "                                <input type=\"file\" ngf-select  ngf-accept=\"'.jpg,.jpeg,.png,.gif'\" ng-model-rejected=\"rejFiles\" ngf-change=\"onFileSelect($files, false)\">\n" +
    "                                <div ng-show=\"selectedFiles != null\">\n" +
    "                                    <div class=\"sel-file\" ng-repeat=\"f in selectedFiles\">\n" +
    "                                        <img ng-show=\"dataUrls[$index]\" ng-src=\"{{dataUrls[$index]}}\">\n" +
    "                                        <span class=\"progress\" ng-show=\"progress[$index] >= 0\">\n" +
    "                                            <div style=\"width:{{progress[$index]}}%\">{{progress[$index]}}%</div>\n" +
    "                                        </span>\n" +
    "                                        {{f.name}} - size: {{f.size}}B - type: {{f.type}}\n" +
    "                                    </div>\n" +
    "                                </div>\n" +
    "                                <div class=\"row\" ng-show=\"rejFiles.length > 0\">\n" +
    "                                    <div class=\"col-lg-12 text-center\"><div class=\"alert alert-danger\">\n" +
    "                                        Dateiformat wird nicht unterstützt. Lade dein Bild bitte als JPEG, PNG oder Gif hoch.\n" +
    "                                    </div></div>\n" +
    "                                </div>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"panel panel-default\">\n" +
    "                        <div class=\"panel-heading\">\n" +
    "                            <h4 class=\"panel-title\">\n" +
    "                                <a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapseTwo\">\n" +
    "                                    Aus der Motiv Gallerie\n" +
    "                                </a>\n" +
    "                            </h4>\n" +
    "                        </div>\n" +
    "                        <div id=\"collapseTwo\" class=\"panel-collapse collapse in\">\n" +
    "                            <div class=\"panel-body\">\n" +
    "                                <button class=\"btn btn-default\" ng-click=\"openMotivGallery(true)\">Motivliste aufrufen</button>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"modal-footer\">\n" +
    "                <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "        <div class=\"modal-body\">\n" +
    "            <h5>Stilelemente können in der Größe verändert, gedreht und positioniert werden.</h5>\n" +
    "\n" +
    "            <uib-tabset>\n" +
    "                <uib-tab heading=\"Gallery\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <pagination total-items=\"count\" ng-model=\"page\" ng-change=\"pageChanged()\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" num-pages=\"numPages\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></pagination>\n" +
    "                </uib-tab>\n" +
    "                <uib-tab heading=\"Meine Motive\">\n" +
    "                    <div class=\"row\">\n" +
    "                        <div ng-repeat=\"motiv in myMotive\" class=\"col-xs-2\">\n" +
    "                            <a ng-click=\"selectMotiv(motiv.uuid, false)\" class=\"thumbnail\">\n" +
    "                                <img ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                            </a>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <pagination total-items=\"myCount\" ng-model=\"myPage\" ng-change=\"myPageChanged()\" class=\"pagination-small\" boundary-links=\"true\" rotate=\"false\" previous-text=\"&lsaquo;\" next-text=\"&rsaquo;\" first-text=\"&laquo;\" last-text=\"&raquo;\"></pagination>\n" +
    "                </uib-tab>\n" +
    "\n" +
    "            </uib-tabset>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("painter/painter.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("painter/painter.tpl.html",
    "<div id=\"c\" style=\"overflow: scroll\"></div>");
}]);

angular.module("settings/settings.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("settings/settings.tpl.html",
    "<div class=\"settings panel panel-default check-element animate-show\" ng-show=\"show\">\n" +
    "\n" +
    "    <div ng-repeat=\"setting in settings\" id=\"setting_{{ setting.id }}\" ng-cloak>\n" +
    "        <h6>{{ setting.altLabel || setting.label }}</h6>\n" +
    "        <simplecolorpicker scp-inline=\"true\" scp-picker=\"picker\" index=\"{{setting.id}}\" scp-colors=\"setting.data\" scp-color=\"selectedColor\"></simplecolorpicker>\n" +
    "    </div>\n" +
    "    <br/>\n" +
    "    <div>\n" +
    "        <h6>Stilelemente</h6>\n" +
    "        <button class=\"btn btn-primary btn-xs\" data-ng-controller=\"MotivCtrl\" ng-click=\"openClipartGallery(false)\">Auswählen</button>\n" +
    "    </div>\n" +
    "    <div style=\"float: left; margin-right: 10px;\" ng-repeat=\"motivSelect in motivSelects\" id=\"motivSelect_{{ motivSelect.id }}\" ng-cloak>\n" +
    "        <h6>{{ motivSelect.label }}</h6>\n" +
    "        <button class=\"btn btn-primary btn-xs\" ng-click=\"openMotivSelect(motivSelect.id)\">Auswählen</button>\n" +
    "    </div>\n" +
    "\n" +
    "    <div style=\"float: left; margin-right: 10px;\" ng-repeat=\"svgSelect in svgSelects\" id=\"svgSelect_{{ svgSelect.id }}\" ng-cloak>\n" +
    "        <h6>{{ svgSelect.label }}</h6>\n" +
    "        <button class=\"btn btn-primary btn-xs\" ng-click=\"openSvgSelect(svgSelect.id)\">Auswählen</button>\n" +
    "    </div>\n" +
    "    <div style=\"clear: both\"></div>\n" +
    "    <div data-ng-show=\"showCalendarSettings\" ng-cloak>\n" +
    "        <h6>Kalender</h6>\n" +
    "        <button class=\"btn btn-primary btn-xs\" ng-click=\"openCalSettings(svgSelect.id)\">Einstellungen</button>\n" +
    "    </div>\n" +
    "    <div ng-include=\"'help/help.tpl.html'\" ng-controller=\"HelpCtrl\" data-ng-show=\"showHelpButton\"></div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"motivSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectMotiv(motiv.uuid, true)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getImageThumbPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"svgSelect\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Motive</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div ng-repeat=\"motiv in motive\" class=\"col-xs-2\">\n" +
    "                    <a ng-click=\"selectSvg(motiv.uuid)\" class=\"thumbnail\">\n" +
    "\n" +
    "                        <img ng-if=\"motiv.uuid\" ng-src=\"{{ ds.getRawPath(motiv.uuid) }}\"/>\n" +
    "                        <img ng-if=\"!motiv.uuid\" data-src=\"holder.js/87x87?text=Keins\" alt=\"No\" holder-fix/>\n" +
    "                        <div class=\"caption\">\n" +
    "                            <h6>{{ motiv.label }}</h6>\n" +
    "                        </div>\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"calSettings\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Kalender Einstellungen</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <h4>Farben</h4>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Monatsfarbe</label>\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.monthColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.monthColor\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Tagfarbe</label>\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.dayColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.dayColor\"></simplecolorpicker>\n" +
    "\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-4\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Sonntagsfarbe</label>\n" +
    "                        <simplecolorpicker scp-inline=\"true\" style=\"display: block;\" scp-picker=\"1\" index=\"calendarSetting.sunColor\" scp-colors=\"calendarSetting.data\" scp-color=\"calendarSetting.sunColor\"></simplecolorpicker>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <h4>Einstellungen</h4>\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-md-6\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Startmonat</label>\n" +
    "                        <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startMonth\">\n" +
    "                            <option value=\"1\" label=\"Januar\"></option>\n" +
    "                            <option value=\"2\" label=\"Februar\"></option>\n" +
    "                            <option value=\"3\" label=\"März\"></option>\n" +
    "                            <option value=\"4\" label=\"April\"></option>\n" +
    "                            <option value=\"5\" label=\"Mai\"></option>\n" +
    "                            <option value=\"6\" label=\"Juni\"></option>\n" +
    "                            <option value=\"7\" label=\"Juli\"></option>\n" +
    "                            <option value=\"8\" label=\"August\"></option>\n" +
    "                            <option value=\"9\" label=\"September\"></option>\n" +
    "                            <option value=\"10\" label=\"Oktober\"></option>\n" +
    "                            <option value=\"11\" label=\"November\"></option>\n" +
    "                            <option value=\"12\" label=\"Dezember\"></option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "                <div class=\"col-md-6\">\n" +
    "                    <div class=\"form-group\">\n" +
    "                        <label class=\"control-label\">Startjahr</label>\n" +
    "                        <select class=\"form-control\" ng-change=\"changeCalendarSetting()\" ng-model=\"calendarSetting.startYear\">\n" +
    "                            <option value=\"2014\" label=\"2014\"></option>\n" +
    "                            <option value=\"2015\" label=\"2015\"></option>\n" +
    "                            <option value=\"2016\" label=\"2016\"></option>\n" +
    "                            <option value=\"2017\" label=\"2017\"></option>\n" +
    "                            <option value=\"2018\" label=\"2018\"></option>\n" +
    "                            <option value=\"2019\" label=\"2019\"></option>\n" +
    "                        </select>\n" +
    "                    </div>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("sites/sites.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("sites/sites.tpl.html",
    "<div class=\"siteBrowser panel panel-default\" ng-show=\"settings.showSiteBrowser\">\n" +
    "<div class=\"btn-group\"><button  ng-show=\"settings.showAddSite\" class=\"btn btn-success\" ng-click=\"createSite()\"><span class=\"fa fa-plus-square\"></span></button> <button  ng-show=\"settings.showRemoveSite\" class=\"btn btn-danger\" ng-click=\"deleteSite()\"><span class=\"fa fa-minus-square\"></span></button></div>\n" +
    "<div class=\"scroller\">\n" +
    "    <style>\n" +
    "        .sites canvas {\n" +
    "            position: relative;\n" +
    "        }\n" +
    "    </style>\n" +
    "    <ul class=\"sites\">\n" +
    "        <li ng-repeat=\"site in sites\"id=\"site_li_{{ site.id }}\" ng-click=\"selectSite(site.id)\" repeat-done=\"layoutDone()\" ng-cloak>\n" +
    "            <div class=\"canvas-holder\"><canvas id=\"site_{{ site.id }}\"></canvas></div>\n" +
    "        </li>\n" +
    "    </ul>\n" +
    "</div>\n" +
    "</div>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"sitesLayoutGallery\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Seitenlayouts</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-xs-3\" ng-repeat=\"layout in layouts\" ng-click=\"addLayout(layout.vorlageSite)\" id=\"layout_li_{{ layout.id }}\">\n" +
    "                    <a class=\"thumbnail\">\n" +
    "                        <img id=\"layout_{{ layout.id }}\" ng-src=\"{{ layout.previewImage }}\" />\n" +
    "                    </a>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-warning\" ng-click=\"cancel()\">Schließen</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"deleteSite\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Löschen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite löschen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"addLayout\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Hinzufügen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich die Seite hinzfügen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("tools/tools.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("tools/tools.tpl.html",
    "<div class=\"tools panel panel-default check-element animate-show\" ng-show=\"show\">\n" +
    "\n" +
    "    <button type=\"button\" id=\"addMotiv\" style=\"margin-bottom: 1px\" class=\"btn btn-default btn-sm\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivMethod()\"><span class=\"fa fa-picture-o\"></span> Foto hinzufügen</button>\n" +
    "    <button type=\"button\" id=\"addText\" class=\"btn btn-default btn-sm\" ng-click=\"addText()\"><span class=\"fa fa-font\"></span> Text hinzufügen</button>\n" +
    "\n" +
    "    <div class=\"editTools editTextTools check-element animate-show\" ng-show=\"showTextTools\">\n" +
    "        <h6>Text</h6>\n" +
    "        <h6>Bitte in das Textfeld klicken</h6>\n" +
    "        <div class=\"btn-group\">\n" +
    "            <ui-select ng-model=\"settings.font\" on-select=\"selectFont($item, $model)\" theme=\"bootstrap\" style=\"width: 150px;\">\n" +
    "                <ui-select-match style=\"font-family: {{ settings.font }}\">{{settings.font}}</ui-select-match>\n" +
    "                <ui-select-choices repeat=\"font in fonts\">\n" +
    "                    <div style=\"font-family: {{ font.name }}\" >{{ font.name }}</div>\n" +
    "                </ui-select-choices>\n" +
    "            </ui-select>\n" +
    "        </div>\n" +
    "        <div class=\"btn-group\">\n" +
    "            <button colorpicker colorpicker-position=\"left\" class=\"btn btn-success btn-sm\" ng-model=\"settings.color\">Farbe</button>\n" +
    "        </div>\n" +
    "\n" +
    "        <input type=\"text\" ng-change=\"selectSize()\" ng-model=\"settings.size\" name=\"tools_size\" value=\"{{settings.size}}\" id=\"tools_size\"/>\n" +
    "        Größe: <input type=\"text\" ng-model=\"settings.size\" value=\"{{settings.size}}\" size=\"3\" name=\"tool_size_raw\" id=\"tool_size_raw\"/>\n" +
    "\n" +
    "        <div class=\"row\" style=\"clear: both;margin-top: 5px\">\n" +
    "            <div class=\"col-xs-6\" style=\"padding-right: 0;\">\n" +
    "                <div class=\"btn-group\">\n" +
    "                    <button type=\"button\" ng-click=\"selectBold()\" class=\"btn btn-default btn-xs\" ng-class=\"settings.bold\" ng-disabled=\"!settings.showBold\"><span class=\"fa fa-bold\"></span></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectItalic()\" class=\"btn btn-default btn-xs\" ng-class=\"settings.italic\" ng-disabled=\"!settings.showItalic\"><span class=\"fa fa-italic\"></span></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectUnderline()\" class=\"btn btn-default btn-xs\" ng-class=\"settings.underline\"><span class=\"fa fa-underline\"></span></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div class=\"col-xs-6\" style=\"padding-left: 0;\">\n" +
    "                <div class=\"btn-group\">\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignLeft()\" class=\"btn btn-default btn-xs\" ng-class=\"settings.alignLeft\"><span class=\"fa fa-align-left\"></span></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignCenter()\" class=\"btn btn-default btn-xs\" ng-class=\"settings.alignCenter\"><span class=\"fa fa-align-center\"></span></button>\n" +
    "                    <button type=\"button\" ng-click=\"selectAlignRight()\" class=\"btn btn-default btn-xs\" ng-class=\"settings.alignRight\"><span class=\"fa fa-align-right\"></span></button>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"editTools editMotivTools check-element animate-show\" ng-show=\"showMotivTools\">\n" +
    "        <h6>Motiv</h6>\n" +
    "        <div class=\"btn-group\">\n" +
    "            <button type=\"button\" class=\"btn btn-default\" ng-controller=\"MotivCtrl\" ng-click=\"selectMotivEditMethod()\"><span class=\"fa fa-picture-o\"></span> Motiv ändern</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "    <div class=\"editTools check-element animate-show\" ng-show=\"showTools\">\n" +
    "        <h6>Ebenen verschieben</h6>\n" +
    "        <div class=\"btn-group\">\n" +
    "            <button type=\"button\" ng-click=\"onTop()\" class=\"btn btn-default btn-sm\"><span class=\"icon-bring-to-front\"></span></button>\n" +
    "            <button type=\"button\" ng-click=\"moveForward()\" class=\"btn btn-default btn-sm\"><span class=\"icon-bring-forward\"></span></button>\n" +
    "            <button type=\"button\" ng-click=\"moveBack()\" class=\"btn btn-default btn-sm\"><span class=\"icon-send-backwards\"></span></button>\n" +
    "            <button type=\"button\" ng-click=\"onBottom()\" class=\"btn btn-default btn-sm\"><span class=\"icon-send-to-back\"></span></button>\n" +
    "        </div>\n" +
    "        <div class=\"btn-group\">\n" +
    "            <button type=\"button\" ng-click=\"deleteElement()\" class=\"btn btn-danger btn-sm\"><span class=\"fa fa-ban\"></span> Löschen</button>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</div>");
}]);

angular.module("top/top.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("top/top.tpl.html",
    "<nav id=\"top\" class=\"navbar navbar-default fixed\">\n" +
    "    <div class=\"row\">\n" +
    "        <div class=\"col-xs-6 col-md-4\">\n" +
    "            <div class=\"navbar-header\">\n" +
    "                <a class=\"navbar-brand\" href=\"#\">{{title}}</a>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-6 col-md-4\">\n" +
    "            <div class=\"navbar-header\" ng-show=\"showSiteCount\">\n" +
    "                <a class=\"navbar-brand\" href=\"#\">Seite {{actSite}}/{{sumSite}}</a>\n" +
    "            </div>\n" +
    "            <!--<button type=\"button\" class=\"btn btn-info navbar-btn\" ng-controller=\"AppCtrl\" ng-click=\"CallMe();\"><span class=\"fa fa-times\"></span> Hilfe</button>-->\n" +
    "            <div class=\"navbar-header\" ng-show=\"showSiteCountError\" style=\"margin-bottom: 0\">\n" +
    "                <div class=\"alert alert-danger\" style=\"margin-bottom: 0\">Produkt muß {{minSiteCount}} oder {{maxSiteCount}} Seiten besitzen</div>\n" +
    "            </div>\n" +
    "            <div class=\"btn-group\" ng-show=\"datatable.length > 0\" role=\"group\">\n" +
    "                <div class=\"btn-group dropdown\" dropdown>\n" +
    "                    <button type=\"button\" class=\"btn btn-default dropdown-toggle\" dropdown-toggle>Datensätze&nbsp;<span class=\"fa fa-angle-down\"></span></button>\n" +
    "                    <ul class=\"dropdown-menu text-left\" role=\"menu\" >\n" +
    "                        <li ng-repeat=\"row in datatable\"><a ng-click=\"mydatatableSelect(row)\">{{row[datatableLabel]}}</a></li>\n" +
    "                    </ul>\n" +
    "                </div>\n" +
    "                <button type=\"button\" class=\"btn btn-default\" ng-click=\"editDatatable()\">Bearbeiten</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "        <div class=\"col-xs-6 col-md-4\">\n" +
    "            <div class=\"btn-group pull-right\" >\n" +
    "                <button type=\"button\" id=\"closeButton\" class=\"btn btn-danger navbar-btn\" ng-click=\"closeApplication()\"><span class=\"fa fa-times\"></span> Abbrechen</button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "</nav>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"closeApplication\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4>Verlassen?</h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            Wollen Sie wirklich den Layouter verlassen?\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button class=\"btn btn-success\" ng-click=\"ok()\">Ja</button>\n" +
    "            <button class=\"btn btn-danger\" ng-click=\"cancel()\">Nein</button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>");
}]);

angular.module("wizard/wizard.tpl.html", []).run(["$templateCache", function($templateCache) {
  $templateCache.put("wizard/wizard.tpl.html",
    "<script type=\"text/ng-template\" id=\"stepSet\">\n" +
    "    <div class=\"modal-content\">\n" +
    "        <div class=\"modal-header\">\n" +
    "            <h4><span ng-repeat=\"step in steps\"> <i class=\"fa fa-circle-o fa-lg\" ng-class=\"{'text-danger': step.isDisplayed, 'text-muted': !step.isDisplayed, 'text-success': step.isFinished }\"></i> {{step.label}}</span></h4>\n" +
    "        </div>\n" +
    "\n" +
    "        <div class=\"modal-body\">\n" +
    "            <div id=\"{{ step.id }}\" ng-repeat=\"step in steps\" ng-show=\"step.isDisplayed\">\n" +
    "                <span ng-repeat=\"element in step.elements\">\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formrte'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"text\" class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"/>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formtextarea'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <textarea class=\"form-control\" name=\"email\" ng-change=\"changeText(element)\" ng-model=\"element.value\"></textarea>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formuploadmotiv'\">\n" +
    "                        <label>{{ element.label }}</label>\n" +
    "                        <input type=\"file\" name=\"file\" ngf-select ngf-change=\"changeMotiv($files, element)\"/>\n" +
    "                        <div ng-show=\"selectedFiles != null\">\n" +
    "                            <div class=\"sel-file\" ng-repeat=\"f in selectedFiles\">\n" +
    "                                <img ng-show=\"dataUrls[$index]\" ng-src=\"{{dataUrls[$index]}}\">\n" +
    "                                <span class=\"progress\" ng-show=\"progress[$index] >= 0\">\n" +
    "                                    <div style=\"width:{{progress[$index]}}%\">{{progress[$index]}}%</div>\n" +
    "                                </span>\n" +
    "                                {{f.name}} - size: {{f.size}}B - type: {{f.type}}\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='formmultiuploadmotiv'\">\n" +
    "                        <label>{{ element.label }} ({{ selectableMotive.length }} Stück)</label>\n" +
    "                        <input type=\"file\" name=\"file\" ngf-select ngf-change=\"changeMotive($files, element)\" ngf-multiple=\"true\"/>\n" +
    "                        <div class=\"row\" ng-show=\"multiUploadFiles != null\">\n" +
    "                            <div ng-repeat=\"f in multiUploadFiles\" class=\"col-xs-4 col-md-2\">\n" +
    "                                <a href=\"#\" class=\"thumbnail\">\n" +
    "                                    <img ng-show=\"f.url\" ng-src=\"{{f.url}}\" class=\"img-rounded\"/>\n" +
    "                                </a>\n" +
    "                            </div>\n" +
    "                        </div>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h1'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='datatable'\">\n" +
    "                        <h1>{{ element.value }}</h1>\n" +
    "                        <table class=\"table\">\n" +
    "                            <thead>\n" +
    "                                <tr>\n" +
    "                                    <th ng-repeat=\"col in element.columns\">{{ col.label }}</th><th><button ng-click=\"addRowFromDatatable(element)\" class=\"btn btn-sm btn-success\"><i class=\"fa fa-plus-square\"></i></button></th>\n" +
    "                                </tr>\n" +
    "                            </thead>\n" +
    "                            <tbody>\n" +
    "                                <tr ng-repeat=\"row in element.rows\"><td ng-repeat=\"col in element.columns\"><input type=\"text\" ng-model=\"row[col.id]\"/></td><td><button ng-click=\"removeRowFromDatatable(row, element)\" class=\"btn btn-sm btn-danger\"><i class=\"fa fa-minus-square\"></i></button></td></tr>\n" +
    "                            </tbody>\n" +
    "\n" +
    "                        </table>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h2'\">\n" +
    "                        <h2>{{ element.value }}</h2>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h3'\">\n" +
    "                        <h3>{{ element.value }}</h3>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h4'\">\n" +
    "                        <h4>{{ element.value }}</h4>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='h5'\">\n" +
    "                        <h5>{{ element.value }}</h5>\n" +
    "                    </div>\n" +
    "                    <div class=\"form-group\" ng-if=\"element.type=='p'\">\n" +
    "                        <p>{{ element.value }}</p>\n" +
    "                    </div>\n" +
    "                </span>\n" +
    "            </div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"modal-footer\">\n" +
    "            <button type=\"button\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"cancel()\"><i class=\"fa fa-times\"></i> abbrechen</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!previousEnabled\" class=\"btn btn-default pull-left\" data-original-title=\"\" title=\"\" ng-click=\"previous()\"><i class=\"fa fa-chevron-left\"></i> zurück</button>\n" +
    "            <button type=\"button\" ng-disabled=\"!nextEnabled\" class=\"btn btn-success assi-weiter-2\" data-original-title=\"\" title=\"\" ng-click=\"next()\">weiter <i class=\"fa fa-chevron-right\"></i></button>\n" +
    "            <button type=\"button\" ng-disabled=\"!submitEnabled\" class=\"btn btn-success\" data-original-title=\"\" title=\"\" ng-click=\"finish()\">Fertigstellen <i class=\"fa fa-check\"></i></button>\n" +
    "        </div>\n" +
    "\n" +
    "    </div>\n" +
    "</script>\n" +
    "\n" +
    "\n" +
    "<script type=\"text/ng-template\" id=\"stepsetOld\">\n" +
    "    <div>\n" +
    "        <form name=\"stepForm\">\n" +
    "            <div id=\"steps\" ng-transclude>\n" +
    "            </div>\n" +
    "\n" +
    "            <div id=\"navBar\">\n" +
    "      <span>\n" +
    "        <button class=\"btn\" ng-click=\"previous()\" ng-disabled=\"!previousEnabled\">{{previousText}}</button>\n" +
    "        <button class=\"btn\" ng-click=\"next()\" ng-disabled=\"!nextEnabled\">{{nextText}}</button>\n" +
    "        <button class=\"btn btn-success\" ng-click=\"submit()\" ng-disabled=\"stepForm.$invalid || !submitEnabled\">{{submitText}}</button>\n" +
    "      </span>\n" +
    "            </div>\n" +
    "        </form>\n" +
    "    </div>\n" +
    "</script>\n" +
    "<script type=\"text/ng-template\" id=\"stepOld\">\n" +
    "    <div ng-show=\"isDisplayed\">\n" +
    "        <h1>{{title}}</h1>\n" +
    "        <p>{{description}}</p>\n" +
    "        <div ng-transclude></div>\n" +
    "    </div>\n" +
    "</script>");
}]);
