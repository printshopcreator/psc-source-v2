Ext.define('MyApp.view.override.ImageUploadField', {
    override: 'MyApp.view.ImageUploadField',
    
    /**
    * @cfg {String} buttonDeleteText Set the delete button caption.
    */
    buttonDeleteText:  'Delete',
    /**
    * @cfg {String} buttonDeleteMargin Set the margin of the delete button.
    */
    buttonDeleteMargin: 3,
    readOnly: false,
    
    componentLayout: 'imagefield',
    /**
    * @cfg {String} buttonPreviewText Set the preview button caption.
    */
    buttonPreviewText:  'Preview',
    /**
    * @cfg {String} buttonPreviewMargin Set the margin of the preview button.
    */
    buttonPreviewMargin: 3,
    /**
    * @cfg {String} imageRootPath Set the root URL to the uploaded picture
    */
    imageRootPath:      'image/getpathbyuid/',
    
    initComponent : function(){
        var me = this;
        me.readOnlyTemp = me.readOnly || false;
        
        me.readOnly = true; // temporarily make it readOnly so that the parent function work properly
        me.addEvents(
            /**
             * @event deletefile
             * Fires when the delete file button is pressed
             * @param {Ext.ux.form.field.UploadFileField} this
             * @param {boolean} pressed
             */
            'deletefile'
        );
        me.callParent(arguments);
        
    },
    
    deletePressed: function(field, pressed){
        var me = this;
        if(me.buttonPreview){
            me.buttonPreview.setDisabled(pressed);
        }        
    },

    onRender: function() {
        var me = this;

        me.callParent(arguments);
        me.createDeleteButton();
        me.onReadOnly(me.readOnlyTemp);
        me.createPreviewButton();
        if(!Ext.isEmpty(me.getValue())){
            me.buttonPreview.enable();
        }
        me.on('deletefile', me.deletePressed, me);
    },
    
    setReadOnly: function(readOnly){
        var me = this;
        
        me.onReadOnly(readOnly);
        me.readOnly = readOnly;
    },
    
    onReadOnly: function(readOnly){
        var me = this;

        if(me.button){
            me.button.setDisabled(readOnly);
        }
        if(me['buttonEl-btnEl']){
            me['buttonEl-btnEl'].dom.disabled = readOnly;
        }
        if(me.fileInputEl){
            me.fileInputEl.dom.disabled = readOnly;
        }
        if(me.buttonDelete){
            me.buttonDelete.setDisabled(readOnly);
        }
    },
    
    onDisable: function(){
        var me = this;
        
        me.callParent(arguments);
        if(me.buttonDelete){
            me.buttonDelete.setDisabled(true);
        }
        if(me.buttonPreview){
            me.buttonPreview.setDisabled(true);
        }
    },
    
    onEnable: function(){
        var me = this;

        me.callParent(arguments);
        if(me.buttonDelete){
            me.buttonDelete.setDisabled(false);
        }
        if(me.buttonPreview){
            me.buttonPreview.setDisabled(!Ext.isEmpty(me.getValue()));
        }
    },
    
    createDeleteButton: function(){
        var me = this;
        var parent = me.browseButtonWrap ? me.browseButtonWrap : me.bodyEl;
        me.buttonDelete = Ext.widget('button', Ext.apply({
            ui:             me.ui,
            renderTo:       parent,
            text:           me.buttonDeleteText,
            cls:            Ext.baseCSSPrefix + 'form-file-btn',
            preventDefault: true,
            pressed:        false,
            style:          'margin-left:' + me.buttonDeleteMargin + 'px',
            toggleHandler:  me.handleDelete,
            enableToggle:   true,
            scope:          me
        }, me.buttonDeleteConfig));

    },
    
    handleDelete: function(btn, pressed, e){
        var me = this;
        
        if(pressed){
            me.originalValue = me.getValue();
            me.setValue('delete');
            me.fireEvent('deletefile', me, true);
        }else{
            me.setValue(me.originalValue);
            me.fireEvent('deletefile', me, false);
        }
    },
    
    isFileUpload: function(){
        return this.getValue() != 'delete';
    },
    
    setValue: function(value){
        var me = this,
            buttonDelete = me.buttonDelete,
            buttonPreview = me.buttonPreview;
        if (buttonPreview && !Ext.isEmpty(value) && value != 'delete') {
            buttonPreview.enable();
        }
        if(buttonDelete && value != 'delete'){
            buttonDelete.toggle(false, true);
        }

        me.setRawValue(me.valueToRaw(value));
        return me.mixins.field.setValue.call(me, value);
    },
    
    onDestroy: function(){
        Ext.destroyMembers(this, 'buttonPreview');
        Ext.destroyMembers(this, 'buttonDelete');
        this.callParent();
    },
    
    createPreviewButton: function(){
        var me = this;
        var parent = me.browseButtonWrap ? me.browseButtonWrap : me.bodyEl;
        me.buttonPreview = Ext.widget('button', Ext.apply({
            ui:             me.ui,
            renderTo:       parent,
            text:           me.buttonPreviewText,
            cls:            Ext.baseCSSPrefix + 'form-file-btn',
            preventDefault: true,
            disabled:       true,
            style:          'margin-left:' + me.buttonPreviewMargin + 'px',
            handler:        me.showPreview,
            scope:          me
        }, me.buttonPreviewConfig));

    },
    
    showPreview: function(btn, e){
        try{
            var img = new Image(),
                style;
            img.src = this.getImageValue();
            if(400 / img.width < 400 / img.height){
                style = 'style="max-width:100%"';
            }else{
                style = 'style="max-height:100%"';
            }
        }catch(err){}
        var win = Ext.create('Ext.window.Window', {
            title:      this.buttonPreviewText,
            height:     400,
            width:      400,
            modal:      true,
            layout:     'fit',
            html: '<img src="' + this.getImageValue() + '" />',
            tools: [{
                type:   'maximize',
                handler: function(event, toolEl, owner, tool){
                    win.toggleMaximize();
                },
                scope:  win
            }]
        }).show();
    },
    
    getImageValue: function(){
        return Routing.generate('backend_portal_image_getpathbyuid', { uid: this.getValue() }) ;
    }
});