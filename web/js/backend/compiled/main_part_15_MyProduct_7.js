/*
 * File: app/view/MyProduct.js
 *
 * This file was generated by Sencha Architect version 2.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.MyProduct', {
    extend: 'Ext.grid.Panel',
    alias: 'widget.MyProduct',

    height: 250,
    width: 543,
    title: 'Produkte',
    forceFit: true,
    store: 'MyProductStore',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'uid',
                    text: 'Id'
                },
                {
                    xtype: 'booleancolumn',
                    dataIndex: 'active',
                    text: 'Aktiv?',
                    falseText: 'Nein',
                    trueText: 'Ja'
                },
                {
                    xtype: 'gridcolumn',
                    filter: {
                        type: 'string'
                    },
                    dataIndex: 'title',
                    text: 'Title'
                }
            ],
            dockedItems: [
                {
                    xtype: 'pagingtoolbar',
                    dock: 'bottom',
                    width: 400,
                    displayInfo: true
                }
            ],
            selModel: Ext.create('Ext.selection.RowModel', {

            })
        });

        me.processMyProduct(me);
        me.callParent(arguments);
    },

    processMyProduct: function(config) {
        console.log(config);

        var filters = {
            ftype: 'filters',
            // encode and local configuration options defined previously for easier reuse
            encode: true, // json encode the filter query
            local: false,   // defaults to false (remote filtering)

            // Filters are most naturally placed in the column definition, but can also be
            // added here.
            filters: [{
                type: 'numeric',
                dataIndex: 'uid'
            }, {
                type: 'string',
                dataIndex: 'title'
            }]
        };

        config.features = [filters];
    }

});