/*
 * File: app/view/LoginWindow.js
 *
 * This file was generated by Sencha Architect version 2.2.0.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.LoginWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.LoginWindow',

    draggable: false,
    height: 223,
    width: 400,
    resizable: false,
    layout: {
        type: 'fit'
    },
    closable: false,
    modal: true,

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            buttons: [
                {
                    text: 'Login',
                    type: 'submit',
                    action: 'login',
                    id: 'login',
                    formBind: true,
                    
                }
            ],
            items: [
                {
                    xtype: 'form',
                    bodyPadding: 10,
                    title: 'Login',
                    api: {
    submit: Actions.PSCBackendPortal_User.login
},
                    items: [
                        {
                            xtype: 'textfield',
                            anchor: '100%',
                            fieldLabel: 'EMail',
                            name: 'username'
                        },
                        {
                            xtype: 'textfield',
                            width: 366,
                            fieldLabel: 'Passwort',
                            name: 'password',
                            inputType: 'password'
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    }

});