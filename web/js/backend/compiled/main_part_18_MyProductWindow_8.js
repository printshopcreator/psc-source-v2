/*
 * File: app/view/MyProductWindow.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('MyApp.view.MyProductWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.MyProductWindow',

    requires: [
        'MyApp.view.ImageUploadField'
    ],

    height: 580,
    id: 'MyProductWindow',
    itemId: 'MyProductWindow',
    width: 944,
    layout: {
        type: 'fit'
    },
    title: 'Produkt',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'container',
                    itemId: 'main',
                    minHeight: 200,
                    layout: {
                        align: 'stretch',
                        type: 'vbox'
                    },
                    items: [
                        {
                            xtype: 'form',
                            itemId: 'MyProductWindowForm',
                            layout: {
                                type: 'fit'
                            },
                            api: {
    load: Actions.PSCBackendPortal_Product.getProductFormData,
    submit: Actions.PSCBackendPortal_Product.saveProductFormData
},
                            paramOrder: 'uid',
                            items: [
                                {
                                    xtype: 'tabpanel',
                                    activeTab: 0,
                                    items: [
                                        {
                                            xtype: 'panel',
                                            layout: {
                                                align: 'stretch',
                                                type: 'vbox'
                                            },
                                            bodyBorder: false,
                                            bodyPadding: 10,
                                            title: 'Basics',
                                            items: [
                                                {
                                                    xtype: 'fieldcontainer',
                                                    width: 400,
                                                    defaults: {
                                                        hideLabel: true
                                                    },
                                                    layout: {
                                                        align: 'stretch',
                                                        type: 'hbox'
                                                    },
                                                    fieldLabel: 'ID/UUID',
                                                    items: [
                                                        {
                                                            xtype: 'textfield',
                                                            width: 79,
                                                            fieldLabel: 'Label',
                                                            name: 'uid',
                                                            readOnly: true
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            flex: 1,
                                                            fieldLabel: 'Label',
                                                            name: 'uuid',
                                                            readOnly: true
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'textfield',
                                                    flex: 1,
                                                    fieldLabel: 'Titel',
                                                    name: 'title'
                                                },
                                                {
                                                    xtype: 'checkboxgroup',
                                                    flex: 1,
                                                    width: 400,
                                                    fieldLabel: '',
                                                    items: [
                                                        {
                                                            xtype: 'checkboxfield',
                                                            name: 'active',
                                                            boxLabel: 'Aktiv',
                                                            inputValue: '1',
                                                            uncheckedValue: '0'
                                                        },
                                                        {
                                                            xtype: 'checkboxfield',
                                                            name: 'privat',
                                                            boxLabel: 'Privatisieren',
                                                            inputValue: '1',
                                                            uncheckedValue: '0'
                                                        },
                                                        {
                                                            xtype: 'checkboxfield',
                                                            fieldLabel: '',
                                                            name: 'notBuy',
                                                            boxLabel: 'Nicht verkaufbar (nur zur Anzeige)',
                                                            inputValue: '1',
                                                            uncheckedValue: '0'
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'fieldset',
                                                    flex: 1,
                                                    height: 52,
                                                    layout: {
                                                        type: 'column'
                                                    },
                                                    collapsible: true,
                                                    title: 'Lager',
                                                    items: [
                                                        {
                                                            xtype: 'checkboxfield',
                                                            margin: '0 10px 0 0',
                                                            fieldLabel: 'Bestand führen?',
                                                            name: 'isStock',
                                                            inputValue: '1',
                                                            uncheckedValue: '0'
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            margin: '0 10px0 0',
                                                            width: 176,
                                                            fieldLabel: 'Mindestbestand',
                                                            name: 'stockMinCount'
                                                        },
                                                        {
                                                            xtype: 'textfield',
                                                            margin: '0 10px 0 0',
                                                            width: 216,
                                                            fieldLabel: 'Akt. Bestand',
                                                            name: 'stockCount',
                                                            readOnly: true
                                                        },
                                                        {
                                                            xtype: 'button',
                                                            width: 76,
                                                            text: 'Buchen',
                                                            listeners: {
                                                                click: {
                                                                    fn: me.onButtonClick1,
                                                                    scope: me
                                                                }
                                                            }
                                                        }
                                                    ]
                                                },
                                                {
                                                    xtype: 'htmleditor',
                                                    flex: 1,
                                                    height: 150,
                                                    style: 'background-color: white;',
                                                    fieldLabel: 'Beschreibung',
                                                    labelAlign: 'top',
                                                    name: 'description'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: {
                                                type: 'fit'
                                            },
                                            bodyPadding: 10,
                                            title: 'Gruppen',
                                            items: [
                                                {
                                                    xtype: 'combobox',
                                                    itemId: 'articleGroups',
                                                    fieldLabel: 'Produktgruppen',
                                                    name: 'articlegroups[]',
                                                    displayField: 'title',
                                                    hiddenName: '',
                                                    multiSelect: true,
                                                    queryMode: 'local',
                                                    store: 'MyProductgroupStore',
                                                    valueField: 'uid'
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            layout: {
                                                align: 'stretch',
                                                type: 'vbox'
                                            },
                                            bodyPadding: 10,
                                            title: 'Bilder',
                                            items: [
                                                {
                                                    xtype: 'imageuploadfield',
                                                    fieldLabel: 'Bild 1',
                                                    name: 'file1',
                                                    flex: 1
                                                },
                                                {
                                                    xtype: 'imageuploadfield',
                                                    fieldLabel: 'Bild 2',
                                                    name: 'file2',
                                                    flex: 1
                                                }
                                            ]
                                        },
                                        {
                                            xtype: 'panel',
                                            height: 459,
                                            layout: {
                                                type: 'fit'
                                            },
                                            title: 'Buchungen',
                                            items: [
                                                {
                                                    xtype: 'gridpanel',
                                                    itemId: 'stockBooking',
                                                    header: false,
                                                    title: 'Buchungen',
                                                    forceFit: true,
                                                    store: 'MyProductStockStore',
                                                    columns: [
                                                        {
                                                            xtype: 'datecolumn',
                                                            dataIndex: 'created',
                                                            text: 'Wann',
                                                            format: 'd.m.Y H:i:s'
                                                        },
                                                        {
                                                            xtype: 'numbercolumn',
                                                            dataIndex: 'amount',
                                                            text: 'Anzahl'
                                                        },
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'contact',
                                                            text: 'Wer'
                                                        },
                                                        {
                                                            xtype: 'gridcolumn',
                                                            sortable: false,
                                                            dataIndex: 'description',
                                                            text: 'Was'
                                                        },
                                                        {
                                                            xtype: 'gridcolumn',
                                                            dataIndex: 'delivery',
                                                            text: 'Lieferschein'
                                                        }
                                                    ],
                                                    dockedItems: [
                                                        {
                                                            xtype: 'pagingtoolbar',
                                                            dock: 'bottom',
                                                            itemId: 'pagination',
                                                            width: 360,
                                                            displayInfo: true,
                                                            store: 'MyProductStockStore'
                                                        }
                                                    ],
                                                    features: [
                                                        me.processMyRowBodyFeature({
                                                            ftype: 'rowbody'
                                                        })
                                                    ]
                                                }
                                            ]
                                        }
                                    ]
                                }
                            ]
                        }
                    ]
                }
            ],
            dockedItems: [
                {
                    xtype: 'toolbar',
                    dock: 'bottom',
                    ui: 'footer',
                    items: [
                        {
                            xtype: 'tbfill'
                        },
                        {
                            xtype: 'button',
                            action: 'save',
                            glyph: 81,
                            text: 'Speichern',
                            listeners: {
                                click: {
                                    fn: me.onButtonClick,
                                    scope: me
                                }
                            }
                        }
                    ]
                }
            ]
        });

        me.callParent(arguments);
    },

    processMyRowBodyFeature: function(config) {
        var filters = {
            ftype: 'filters',
            // encode and local configuration options defined previously for easier reuse
            encode: true, // json encode the filter query
            local: false,   // defaults to false (remote filtering)
            // Filters are most naturally placed in the column definition, but can also be
            // added here.
            filters: [
            {
                type: 'date',
                dataIndex: 'created'
            },{
                type: 'string',
                dataIndex: 'description'
            },{
                type: 'string',
                dataIndex: 'delivery'
            }]
        };

        config.features = [filters];

        return config;
    },

    onButtonClick1: function(button, e, eOpts) {
        var stockAddWindow = Ext.create('MyApp.view.MyStockAddWindow');
        stockAddWindow.show();
        stockAddWindow.items.items[0].getForm().findField("articleId").setValue(this.items.get(0).items.get(0).getForm().uid);
    },

    onButtonClick: function(button, e, eOpts) {
        this.items.get(0).items.get(0).getForm().submit({
            params: {
                uid: this.items.get(0).items.get(0).getForm().uid
            }
        });
    }

});