<?php

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/backend/dashboard');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertEquals('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
}
