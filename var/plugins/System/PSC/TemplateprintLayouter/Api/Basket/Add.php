<?php
namespace Plugin\System\PSC\TemplateprintLayouter\Api\Basket;

use Doctrine\ORM\EntityManagerInterface;
use Plugin\System\PSC\TemplateprintLayouter\Dto\Basket\Add\Input;
use Plugin\System\PSC\TemplateprintLayouter\Dto\Basket\Add\Output;
use PSC\Shop\EntityBundle\Entity\Contact;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Add extends AbstractController
{
    private EntityManagerInterface $entityManager;

    protected static $key = "p489r7nwpefsnfp0";

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * change temp basketitem
     * @deprecated
     *
     * @Route("/legacy/add", methods={"POST"})
     * @ParamConverter("data", class="\Plugin\System\PSC\TemplateprintLayouter\Dto\Basket\Add\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="basket",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\TemplateprintLayouter\Dto\Basket\Add\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\Plugin\System\PSC\TemplateprintLayouter\Dto\Basket\Add\Input::class))
     * )
     * @OA\Tag(name="Plugin/System/psc/TemplateprintLayouter/Basket")
     */
    public function add(Input $data): JsonResponse
    {
        require_once(__DIR__ . '/../../../../../../../src/PSC/Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
        require_once(__DIR__ . '/../../../../../../../src/PSC/Shop/EntityBundle/Lagacy/TP_Basket_Collection.php');
        require_once(__DIR__ . '/../../../../../../../src/PSC/Shop/EntityBundle/Lagacy/TP_Util.php');

        /** @var Contact $contact */
        $contact = $this->entityManager->getRepository(Contact::class)->findOneBy(['uuid' => $data->contact]);

        $articles = $_SESSION['Basket']['Articles'];
        /** @var \TP_Basket_Item $basketItem */
        $basketItem = clone $_SESSION['Basket']['TempProduct'][$data->product];

        $basketItem->setRef($contact->getFirstname() . ' ' . $contact->getLastname());
        $basketItem->setTemplatePrintId(md5(Uuid::uuid4()->toString()));

        $itemId = $articles->addItem($basketItem);

        $basepath = 'http://' . $_SERVER["SERVER_NAME"];

        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            $basepath = 'https://' . $_SERVER["SERVER_NAME"];
        }
        $output = new Output();
        $output->success = true;
        $output->genUrl = 'http://tp:8080/w2p/batch?w2pinframeredirect=true&w2pproductid=' . $data->product . '&w2puserid=' . $data->contact . '&ARTID=' . $this->encryptDocker(array('title' => $contact->getFirstname() . ' ' . $contact->getLastname(), 'userid' => $data->contact, 'basketposid' => $itemId, 'UUID' => $data->product, 'ARTID' => session_id(), 'COPY' => 2, 'load' => 4, 'SERVER' => $basepath, 'LAYOUTERID' => Uuid::uuid4()->toString())) . '&time=' . time();

        return $this->json($output);
        /*$user = Doctrine_Query::create()
            ->from('Contact m')
            ->where('uuid = ?')->fetchOne(array($id));

        $sessart = $basket->getTempProduct($article->uuid);
        $sessart->setRef($user->self_firstname . ' ' . $user->self_lastname);
        $sessart->setTemplatePrintId(md5(TP_Util::uuid()));

        $basket->setTempProductClass($sessart);

        $newTempId = $basket->addProduct(true, $article->uuid);
        $this->_helper->json->sendJson(
            array('success' => true,
                'genUrl' => 'http://tp:8080/w2p/batch?w2pinframeredirect=true&w2pproductid=' . $article->uuid . '&w2puserid=' . $id . '&ARTID=' . TP_Crypt::encryptDocker(array('title' => $user->self_firstname . ' ' . $user->self_lastname, 'userid' => $id, 'basketposid' => $newTempId, 'UUID' => $article->uuid, 'ARTID' => Zend_Session::getId(), 'COPY' => 2, 'load' => 4, 'SERVER' => $this->view->basepath, 'LAYOUTERID' => TP_Util::uuid())) . '&time=' . time()));
*/
    }

    private function encryptDocker( $data, $url = "service/template", $saveHost = "http://web") {
        $crypttext = openssl_encrypt	 (http_build_query($data), "AES-128-ECB", self::$key);
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            return str_replace("+", "PLUS", base64_encode($crypttext)) . "&URL=".urlencode($url) . "&WEBHOST=".urlencode("https://".$_SERVER["SERVER_NAME"]) . "&SAVEHOST=" . urlencode($saveHost );
        }
        return str_replace("+", "PLUS", base64_encode($crypttext)) . "&URL=".urlencode($url) . "&WEBHOST=".urlencode("http://".$_SERVER["SERVER_NAME"]) . "&SAVEHOST=". urlencode($saveHost );
    }
}