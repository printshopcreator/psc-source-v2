<?php

namespace Plugin\System\PSC\TemplateprintLayouter\Form;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use Plugin\System\PSC\FormLayouter\Form\Group\FormLayouter;
use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BackendFields extends \PSC\System\PluginBundle\Form\Field implements Field
{
    protected $tab = "uploads";

    private $statusService;
    /**
     * @var Shop
     */
    private Shop $shopService;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(Status $statusService, Shop $shopService, EntityManagerInterface $entityManager)
    {
        $this->statusService = $statusService;
        $this->shopService = $shopService;
        $this->entityManager = $entityManager;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCTemplateprintLayouter/form/field/backend.html.twig';
    }

    public function getModule()
    {
        return Field::Product;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $results = $this->entityManager->getRepository(Account::class)->createQueryBuilder('a')
            ->join('a.shops', 's')
            ->where('s.uid = :shop')->setParameter('shop', $this->shopService->getSelectedShop()->getId())->getQuery()->execute();

        $accounts = [];
        /** @var Account $result */
        foreach($results as $result) {
            $accounts[$result->getTitle()] = $result->getId();
        }

        /** @var Product $product */
        $product = $options['product'];

        $builder->add('uploadCollectingOrders', CheckboxType::class, ['required' => false, 'label' => 'Aktivieren'])
            ->add('collectingOrdersChangePicture', CheckboxType::class, ['required' => false, 'label' => 'Bild tauschen aktivieren'])
            ->add('collectingOrdersCopy', CheckboxType::class, ['required' => false, 'label' => 'Mitarbeiter kopieren'])
            ->add('collectingOrdersNewContact', CheckboxType::class, ['required' => false, 'label' => 'Neuen Mitarbeiter anlegen'])
            ->add('collectingOrdersInviteContact', CheckboxType::class, ['required' => false, 'label' => 'Mitarbeiter einladen'])
            ->add('collectingOrdersIntegrated', CheckboxType::class, ['required' => false, 'label' => 'nicht als Popup'])
            ->add('collectingOrdersAccountFilter', ChoiceType::class, array(
                'choices' => array(
                    'Alle nicht gesperrten' => 1,
                    'Gleich der angemeldeten Firma' => 2,
                    'Filter nach gewählter Firma' => 3,
                    'Filter nach gewählter Firma + alle Personen aller Firmen die meiner Firma zugordnet sind' => 4,
                    'Gleich der angemeldeten Firma + alle Personen aller Firmen die meiner Firma zugordnet sind' => 5,
                ),
                'label' => 'Firmenfilter'
            ))
            ->add("uploadCollectingOrdersInitalStatus", ChoiceType::class, array(
                'label' => 'Initalstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ))
            ->add('collectingOrdersAccount', ChoiceType::class, array(
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'label' => 'Gewählte Firma',
                'choices' => $accounts
            ))
            ->add('uploadTemplateprint', CheckboxType::class, ['required' => false, 'label' => 'Aktivieren'])
            ->add("uploadTemplateprintInitalStatus", ChoiceType::class, array(
                'label' => 'Status',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ));

        if($product) {
            $builder->get('uploadCollectingOrders')->setData($product->isUploadCollectingOrders());
            $builder->get('collectingOrdersChangePicture')->setData($product->isCollectingOrdersChangePicture());
            $builder->get('collectingOrdersCopy')->setData($product->isCollectingOrdersCopy());
            $builder->get('collectingOrdersNewContact')->setData($product->isCollectingOrdersNewContact());
            $builder->get('collectingOrdersInviteContact')->setData($product->isCollectingOrdersInviteContact());
            $builder->get('collectingOrdersAccountFilter')->setData($product->getCollectingOrdersAccountFilter());
            $builder->get('uploadCollectingOrdersInitalStatus')->setData($product->getUploadCollectingOrdersInitalStatus());
            $builder->get('collectingOrdersAccount')->setData($product->getCollectingOrdersAccount());
            $builder->get('uploadTemplateprint')->setData($product->isUploadTemplateprint());
            $builder->get('uploadTemplateprintInitalStatus')->setData($product->getUploadTemplateprintInitalStatus());
        }

        return $builder;
    }

    public function getGroup()
    {
        return "templateprintlayouter";
    }

    public function formPostSetData(FormEvent $event)
    {
        // TODO: Implement formPostSetData() method.
    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Product $product */
        $product = $event->getData();
        $product->setUploadCollectingOrders($event->getForm()->get('templateprintlayouter')->get('uploadCollectingOrders')->getData());
        $product->setCollectingOrdersChangePicture($event->getForm()->get('templateprintlayouter')->get('collectingOrdersChangePicture')->getData());
        $product->setCollectingOrdersCopy($event->getForm()->get('templateprintlayouter')->get('collectingOrdersCopy')->getData());
        $product->setCollectingOrdersNewContact($event->getForm()->get('templateprintlayouter')->get('collectingOrdersNewContact')->getData());
        $product->setCollectingOrdersInviteContact($event->getForm()->get('templateprintlayouter')->get('collectingOrdersInviteContact')->getData());
        $product->setCollectingOrdersAccountFilter($event->getForm()->get('templateprintlayouter')->get('collectingOrdersAccountFilter')->getData());
        $product->setUploadCollectingOrdersInitalStatus($event->getForm()->get('templateprintlayouter')->get('uploadCollectingOrdersInitalStatus')->getData());
        $product->setCollectingOrdersAccount($event->getForm()->get('templateprintlayouter')->get('collectingOrdersAccount')->getData());
        $product->setUploadTemplateprint($event->getForm()->get('templateprintlayouter')->get('uploadTemplateprint')->getData());
        $product->setUploadTemplateprintInitalStatus($event->getForm()->get('templateprintlayouter')->get('uploadTemplateprintInitalStatus')->getData());
    }

    public function formPreSetData(FormEvent $event)
    {
        // TODO: Implement formPreSetData() method.
    }
}