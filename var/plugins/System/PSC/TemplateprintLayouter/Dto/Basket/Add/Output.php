<?php

namespace Plugin\System\PSC\TemplateprintLayouter\Dto\Basket\Add;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class Output {

    /**
     * @OA\Property(type="string")
     */
    public string $genUrl;

    /**
     * @OA\Property(type="boolean")
     */
    public bool $success;

}