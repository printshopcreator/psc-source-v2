<?php

namespace Plugin\System\PSC\TemplateprintLayouter\Dto\Basket\Add;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class Input {

    /**
     * @OA\Property(type="string")
     */
    public string $product = "";

    /**
     * @OA\Property(type="string")
     */
    public string $contact = "";

}