<?php
namespace Plugin\System\PSC\TemplateprintLayouter;

use PSC\System\PluginBundle\Plugin\Base;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Templateprint Layouter Module';

    public function getType()
    {
        return Plugin::Frontend;
    }

    public function getDescription()
    {
        return 'Templateprint Layouter Modul';
    }

    public function getVersion()
    {
        return 1;
    }


}