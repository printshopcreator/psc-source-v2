<?php
namespace Plugin\System\PSC\TemplateprintLayouter\Controller;


use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use MongoDB\BSON\ObjectId;
use PSC\Shop\MediaBundle\Document\Folder;
use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Service\MediaManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class GalleryController
 *
 * @Route("/gallery")
 */
class GalleryController extends AbstractController
{
    /**
     * @Route("/index", name="psc_plugin_templateprintlayouter_gallery_index")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     */
    public function indexAction(Request $request, DocumentManager $documentManager)
    {
        $query = $request->get('query', false);

        $data = [
            "name" => "Main",
            "description" => "Main",
            "subcategories" => []
            ];

        if($query) {
            $query = explode(",", $query);

            foreach($query as $key) {
                /** @var Folder $selectedFolder */
                $selectedFolder = $documentManager
                    ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
                    ->findOneBy(array('id' => trim($key)));

                $subCat = [
                    'name' => $selectedFolder->getTitle(),
                    'description' => $selectedFolder->getTitle(),
                    'images' => []
                ];
                
                $images = [];

                $medias = $documentManager
                    ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                    ->createQueryBuilder('media')
                    ->field('folder.$id')->equals(new ObjectId($selectedFolder->getId()))
                    ->sort('uid', 'ASC')->getQuery()->execute();

                /** @var Media $media */
                foreach($medias as $media) {

                    $images[] = [
                        'name' => $media->getTitle(),
                        'id' => $media->getId(),
                        'description' => $media->getDescription()
                    ];

                }


                $subCat['images'] = $images;

                $data['subcategories'][] = $subCat;
            }
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/preview", name="psc_plugin_templateprintlayouter_gallery_preview")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     */
    public function previewAction(Request $request, DocumentManager $documentManager, MediaManager $mediaManager)
    {
        $media = $mediaManager->retrieveThumbnailPath($mediaManager->getMedia($request->get('id')));

        return new RedirectResponse($media);
    }

    /**
     * @Route("/image", name="psc_plugin_templateprintlayouter_gallery_image")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     */
    public function imageAction(Request $request, DocumentManager $documentManager, MediaManager $mediaManager)
    {
        $media = $mediaManager->getMedia($request->get('id'));

        return new RedirectResponse('http://web/apps/'.$media->getUrl());
    }
}