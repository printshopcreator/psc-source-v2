<?php
namespace Plugin\System\PSC\CreativeLayouter;

use PSC\System\PluginBundle\Plugin\Base;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Creative Layouter';

    public function getType()
    {
        return Plugin::Frontend;
    }

    public function getDescription()
    {
        return 'Creative Layouter';
    }

    public function getVersion()
    {
        return 1;
    }

}