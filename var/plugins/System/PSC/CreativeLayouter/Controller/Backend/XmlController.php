<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\CreativeLayouter\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Plugin\System\PSC\CreativeLayouter\Form\XML\EditType;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class XmlController extends AbstractController
{

    /**
     * creative
     *
     * @Route("/xml/edit/{uuid}", name="psc_plugin_creativelayouter_backend_xml_edit")
     * @template()
     *
     * @param Request $request Request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param string $uuid Uuid
     *
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uuid)
    {
        $selectedShop = $shopService->getSelectedShop();

        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        $form = $this->createForm(EditType::class, $product);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($product);
            $entityManager->flush();
        }
        return array(
            'form' => $form->createView(),
            'product' => $product
        );
    }
}