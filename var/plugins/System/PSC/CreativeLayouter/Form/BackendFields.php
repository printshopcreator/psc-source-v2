<?php

namespace Plugin\System\PSC\CreativeLayouter\Form;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use Plugin\System\PSC\FormLayouter\Form\Group\FormLayouter;
use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BackendFields extends \PSC\System\PluginBundle\Form\Field implements  Field
{
    protected $tab = 'uploads';

    private $statusService;
    /**
     * @var Shop
     */
    private Shop $shopService;

    public function __construct(Status $statusService, Shop $shopService)
    {
        $this->statusService = $statusService;
        $this->shopService = $shopService;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCCreativeLayouter/form/field/backend.html.twig';
    }

    public function getModule()
    {
        return Field::Product;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Product $product */
        $product = $options['product'];

        $builder->add('uploadSteplayouter2', CheckboxType::class, ['required' => false, 'label' => 'Aktivieren'])
            ->add("uploadSteplayouter2InitalStatus", ChoiceType::class, array(
                'label' => 'Initalstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ));

        if($product) {
            $builder->get('uploadSteplayouter2')->setData($product->isUploadSteplayouter2());
            $builder->get('uploadSteplayouter2InitalStatus')->setData($product->getUploadSteplayouter2InitalStatus());
        }

        return $builder;
    }

    public function getGroup()
    {
        return "creativelayouter";
    }

    public function formPostSetData(FormEvent $event)
    {

    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Product $product */
        $product = $event->getData();
        $product->setUploadSteplayouter2InitalStatus($event->getForm()->get('creativelayouter')->get('uploadSteplayouter2InitalStatus')->getData());
        $product->setUploadSteplayouter2($event->getForm()->get('creativelayouter')->get('uploadSteplayouter2')->getData());
    }

    public function formPreSetData(FormEvent $event)
    {

    }
}