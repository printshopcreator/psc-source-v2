<?php

namespace Plugin\System\PSC\Bootstrap3\Form\Field;

use DirectoryIterator;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Shopsetting;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Startseite implements Field
{

    private $_entityManager;

    /** @var \PSC\System\SettingsBundle\Service\Shop */
    private $_shopService;

    private $options;

    public function getTemplate()
    {
        return '@PluginSystemPSCBootstrap3/form/field/startseite.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $products = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findBy(array("shop" => $this->_shopService->getSelectedShop()->getUid(), "enable" => 1));

        $tmp = [];
        foreach ($products as $product) {
            $tmp[$product->getTitle() . " (" . $product->getUid() . ")"] = $product->getUid();
        }

        $productGroups = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findBy(array("shop" => $this->_shopService->getSelectedShop()->getUid(), "enable" => 1));

        $tmpGroups = [];
        foreach ($productGroups as $productGroup) {
            $tmpGroups[$productGroup->getTitle() . " (" . $productGroup->getUid() . ")"] = $productGroup->getUid();
        }

        $builder->add('mode', ChoiceType::class, [
            'required' => false,
            'label' => 'Startpage',
            'choices' => [
                'Willkommensseite' => "index",
                'Slider + Produktliste o. Produktgruppen ohne Seitennavi' => "sliderproductsnosidenav",
            ]
        ])
            ->add('slider_interval', ChoiceType::class, [
                'required' => false,
                'label' => 'Sliderspeed',
                'choices' => [
                    'slow' => "6000",
                    'normal' => "4000",
                    'fast' => "2000",
                ]
            ])
            ->add('mc_start_products', ChoiceType::class, array(
                'choices' => $tmp,
                'multiple' => true,
                'required' => false,
                'label' => 'Product',
            ))
            ->add('mc_start_productgroups', ChoiceType::class, array(
                'choices' => $tmpGroups,
                'multiple' => true,
                'required' => false,
                'label' => 'Productgroups',
            ))
            ->add('index_slogan', TextareaType::class, array(
                'label' => 'Slogen',
                'required' => false
            ));

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Bootstrap3\Form\Group\Startseite::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);

        if (isset($layoutSettings[$shopEntity->getLayout()])) {
            if(isset($layoutSettings[$shopEntity->getLayout()]['slider_interval'])) {
                $event->getForm()->get('bootstrap3Startseite')->get('slider_interval')->setData($layoutSettings[$shopEntity->getLayout()]['slider_interval']);
            }

            if (strtolower($shopEntity->getDefaultFunc()) == "index") {
                /** @var Shopsetting $setting */
                $setting = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shopsetting')->findOneBy(array("shop" => $this->_shopService->getSelectedShop()->getUid(), "key" => "index_index_layout"));
                if ($setting) {
                    $event->getForm()->get('bootstrap3Startseite')->get('mode')->setData($setting->getValue());
                }
            }
            if(isset($layoutSettings[$shopEntity->getLayout()]['mc_start_products'])) {
                $event->getForm()->get('bootstrap3Startseite')->get('mc_start_products')->setData($layoutSettings[$shopEntity->getLayout()]['mc_start_products']);
            }
            if(isset($layoutSettings[$shopEntity->getLayout()]['mc_start_productgroups'])) {
                $event->getForm()->get('bootstrap3Startseite')->get('mc_start_productgroups')->setData($layoutSettings[$shopEntity->getLayout()]['mc_start_productgroups']);
            }
            if (isset($layoutSettings[$shopEntity->getLayout()]['index_slogan'])) {
                $event->getForm()->get('bootstrap3Startseite')->get('index_slogan')->setData($layoutSettings[$shopEntity->getLayout()]['index_slogan']);
            }
        }
    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);
        $layoutSettings[$shopEntity->getLayout()]['slider_interval'] = $event->getForm()->get('bootstrap3Startseite')->get('slider_interval')->getData();
        /** @var Shopsetting $setting */
        $setting = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shopsetting')->findOneBy(array("shop" => $this->_shopService->getSelectedShop()->getUid(), "key" => "index_index_layout"));
        if ($setting) {
            $setting->setValue($event->getForm()->get('bootstrap3Startseite')->get('mode')->getData());

            $this->_entityManager->persist($setting);
        } else {
            $setting = new Shopsetting();
            $setting->setShop($this->_shopService->getSelectedShop());
            $setting->setKey("index_index_layout");
            $setting->setUid(time());
            $setting->setValue($event->getForm()->get('bootstrap3Startseite')->get('mode')->getData());
            $this->_entityManager->persist($setting);
        }
        $layoutSettings[$shopEntity->getLayout()]['mode'] = $event->getForm()->get('bootstrap3Startseite')->get('mode')->getData();
        $layoutSettings[$shopEntity->getLayout()]['mc_start_products'] = $event->getForm()->get('bootstrap3Startseite')->get('mc_start_products')->getData();
        $layoutSettings[$shopEntity->getLayout()]['mc_start_productgroups'] = $event->getForm()->get('bootstrap3Startseite')->get('mc_start_productgroups')->getData();
        $layoutSettings[$shopEntity->getLayout()]['index_slogan'] = $event->getForm()->get('bootstrap3Startseite')->get('index_slogan')->getData();

        $shopEntity->setLayoutSettings(json_encode($layoutSettings));
    }

    public function formPreSetData(FormEvent $event)
    {
    }

    /**
     * @param mixed $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->_entityManager = $entityManager;
    }

    /**
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     */
    public function setShopService($shopService)
    {
        $this->_shopService = $shopService;
    }
}

