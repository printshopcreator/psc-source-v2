<?php

namespace Plugin\System\PSC\Bootstrap3\Form\Field;

use DirectoryIterator;
use Mpdf\Tag\TextArea;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Formulare extends AbstractController implements Field
{

    private $options;

    public function getTemplate()
    {
        return '@PluginSystemPSCBootstrap3/form/field/formulare.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $txt = "";
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $tempLayouts = [];
        if ($shopEntity->isCustomTemplates() == 1) {
            foreach (new DirectoryIterator('/data/www/old/application/design/vorlagen') as $file) {
                if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                    continue;
                }
                $tempLayouts[$file->getFileName()] = $file->getFileName();
            }
        } else {
            if (file_exists('/data/www/old/application/design/clients/' . $shopEntity->getUID())) {
                foreach (new DirectoryIterator('/data/www/old/application/design/clients/' . $shopEntity->getUID()) as $file) {
                    if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                        continue;
                    }
                    $tempLayouts[$file->getFileName()] = $file->getFileName();
                }
            }
        }
if(isset($_POST["settings"]["bootstrap3Images"]["layout"])) {
        header('location: /apps/backend/theme/settings');
        //die();
} else {
        //copy("/data/www/old/public/styles/vorlagen/" . $shopEntity->getLayout() . "/bootstrap/css/style.css","/data/www/old/public/styles/vorlagen/" . $shopEntity->getLayout() . "/bootstrap/css/style_bakup.css");
        $filenamelogin = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/config/user/login.ini";
        $handlelogin = fopen($filenamelogin, 'r'); 
        $txtlogin = fread($handlelogin, filesize($filenamelogin)); 
        fclose($handlelogin); 
        
        $filenameregister = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/config/user/registercontact.ini";
        $handleregister = fopen($filenameregister, 'r'); 
        $txtregister = fread($handleregister, filesize($filenameregister)); 
        fclose($handleregister); 

        $filenamepass = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/config/user/resetpassword.ini";
        $handlepass = fopen($filenamepass, 'r'); 
        $txtpass = fread($handlepass, filesize($filenamepass)); 
        fclose($handlepass); 

        $filenameaddress = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/config/user/updatecontact.ini";
        $handleaddress = fopen($filenameaddress, 'r'); 
        $txtaddress = fread($handleaddress, filesize($filenameaddress)); 
        fclose($handleaddress); 

        $filenameaddaddress = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/config/user/address.ini";
        $handleaddaddress = fopen($filenameaddaddress, 'r'); 
        $txtaddaddress = fread($handleaddaddress, filesize($filenameaddaddress)); 
        fclose($handleaddaddress);
}

if(!file_exists($filenamelogin)) {
    $handle = fopen($filenamelogin, 'w'); 
    fclose($handle);   
    chmod($filenamelogin, 0777);
}
if(!file_exists($filenameregister)) {
    $handle = fopen($filenameregister, 'w'); 
    fclose($handle);   
    chmod($filenameregister, 0777);
}
if(!file_exists($filenamepass)) {
    $handle = fopen($filenamepass, 'w'); 
    fclose($handle);   
    chmod($filenamepass, 0777);
}
if(!file_exists($filenameaddress)) {
    $handle = fopen($filenameaddress, 'w'); 
    fclose($handle);   
    chmod($filenameaddress, 0777);
}
if(!file_exists($filenameaddaddress)) {
    $handle = fopen($filenameaddaddress, 'w'); 
    fclose($handle);   
    chmod($filenameaddaddress, 0777);
}

        $builder
        ->add('formularelogin', TextareaType::class, array(
            'label' => 'login ini',
            'data' => $txtlogin,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangeformulare('formularelogin');")
        ))
        ->add('formulareloginedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('formulareregister', TextareaType::class, array(
            'label' => 'registercontact ini',
            'data' => $txtregister,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangeformulare('formulareregister');")
        ))
        ->add('formulareregisteredit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('formularepass', TextareaType::class, array(
            'label' => 'resetpassword ini',
            'data' => $txtpass,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangeformulare('formularepass');")
        ))
        ->add('formularepassedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('formulareaddress', TextareaType::class, array(
            'label' => 'updatecontact ini',
            'data' => $txtaddress,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangeformulare('formulareaddress');")
        ))
        ->add('formulareaddressedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('formulareaddaddress', TextareaType::class, array(
            'label' => 'Address ini',
            'data' => $txtaddaddress,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangeformulare('formulareaddaddress');")
        ))
        ->add('formulareaddaddressedit', HiddenType::class, array('required' => false, 'data' => 0))
        ;

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Bootstrap3\Form\Group\Formulare::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);
        $event->getForm()->get('bootstrap3General')->get('displayArticleCount')->setData($shopEntity->getDisplayArticleCount());
        $event->getForm()->get('bootstrap3General')->get('basketField1')->setData($shopEntity->getBasketfield1());
        $event->getForm()->get('bootstrap3General')->get('basketField2')->setData($shopEntity->getBasketfield2());
        $event->getForm()->get('bootstrap3General')->get('basketPosField1')->setData($shopEntity->getBasketposfield1());
        $event->getForm()->get('bootstrap3General')->get('basketPosField2')->setData($shopEntity->getBasketposfield2());
        $event->getForm()->get('bootstrap3General')->get('displaySender')->setData($shopEntity->getDisplaySender());
        $event->getForm()->get('bootstrap3General')->get('displayDelivery')->setData($shopEntity->getDisplayDelivery());
        $event->getForm()->get('bootstrap3General')->get('layout')->setData($shopEntity->getLayout());
        $event->getForm()->get('bootstrap3General')->get('customTemplates')->setData($shopEntity->isCustomTemplates());
        $event->getForm()->get('bootstrap3General')->get('googleanalyticscode')->setData($shopEntity->getGoogleanalyticscode());
        $event->getForm()->get('bootstrap3General')->get('defaultFunc')->setData($shopEntity->getDefaultFunc());
        $event->getForm()->get('bootstrap3General')->get('defaultParam')->setData($shopEntity->getDefaultParam());
        $event->getForm()->get('bootstrap3General')->get('templateDisplayProductsCrossselling')->setData($shopEntity->isTemplateDisplayProductsCrossselling());
        $event->getForm()->get('bootstrap3General')->get('templateDisplayUserApproval')->setData($shopEntity->isTemplateDisplayUserApproval());

    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        if($event->getForm()->get('bootstrap3Style')->get('write')->getData() == "1") {
            if($event->getForm()->get('bootstrap3Formulare')->get('formulareloginedit')->getData() == "1") {
                $filenamelogin = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/config/user/login.ini";
                $handlelogin = fopen($filenamelogin, 'w'); 
                fputs($handlelogin, $event->getForm()->get('bootstrap3Formulare')->get('formularelogin')->getData()); 
                fclose($handlelogin); 
            }
            if($event->getForm()->get('bootstrap3Formulare')->get('formulareregisteredit')->getData() == "1") {
                $filenameregister = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/config/user/registercontact.ini";
                $handleregister = fopen($filenameregister, 'w'); 
                fputs($handleregister, $event->getForm()->get('bootstrap3Formulare')->get('formulareregister')->getData()); 
                fclose($handleregister); 
            }
            if($event->getForm()->get('bootstrap3Formulare')->get('formularepassedit')->getData() == "1") {
                $filenamepass = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/config/user/resetpassword.ini";
                $handlepass = fopen($filenamepass, 'w'); 
                fputs($handlepass, $event->getForm()->get('bootstrap3Formulare')->get('formularepass')->getData()); 
                fclose($handlepass); 
            }
            if($event->getForm()->get('bootstrap3Formulare')->get('formulareaddressedit')->getData() == "1") {
                $filenameaddress = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/config/user/updatecontact.ini";
                $handleaddress = fopen($filenameaddress, 'w'); 
                fputs($handleaddress, $event->getForm()->get('bootstrap3Formulare')->get('formulareaddress')->getData()); 
                fclose($handleaddress); 
            }
            if($event->getForm()->get('bootstrap3Formulare')->get('formulareaddaddressedit')->getData() == "1") {
                $filenameaddaddress = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/config/user/address.ini";
                $handleaddaddress = fopen($filenameaddaddress, 'w'); 
                fputs($handleaddaddress, $event->getForm()->get('bootstrap3Formulare')->get('formulareaddaddress')->getData()); 
                fclose($handleaddaddress); 
            }
        }
        $shopEntity->setDisplayArticleCount($event->getForm()->get('bootstrap3General')->get('displayArticleCount')->getData());
        $shopEntity->setBasketfield1($event->getForm()->get('bootstrap3General')->get('basketField1')->getData());
        $shopEntity->setBasketfield2($event->getForm()->get('bootstrap3General')->get('basketField2')->getData());
        $shopEntity->setBasketposfield1($event->getForm()->get('bootstrap3General')->get('basketPosField1')->getData());
        $shopEntity->setBasketposfield2($event->getForm()->get('bootstrap3General')->get('basketPosField2')->getData());
        $shopEntity->setDisplaySender($event->getForm()->get('bootstrap3General')->get('displaySender')->getData());
        $shopEntity->setDisplayDelivery($event->getForm()->get('bootstrap3General')->get('displayDelivery')->getData());
        $shopEntity->setCustomTemplates($event->getForm()->get('bootstrap3General')->get('customTemplates')->getData());
        $shopEntity->setLayout($event->getForm()->get('bootstrap3General')->get('layout')->getData());
        $shopEntity->setGoogleanalyticscode($event->getForm()->get('bootstrap3General')->get('googleanalyticscode')->getData());
        $shopEntity->setDefaultFunc($event->getForm()->get('bootstrap3General')->get('defaultFunc')->getData());
        $shopEntity->setDefaultParam($event->getForm()->get('bootstrap3General')->get('defaultParam')->getData());
        $shopEntity->setTemplateDisplayProductsCrossselling($event->getForm()->get('bootstrap3General')->get('templateDisplayProductsCrossselling')->getData());
        $shopEntity->setTemplateDisplayUserApproval($event->getForm()->get('bootstrap3General')->get('templateDisplayUserApproval')->getData());
    }

    public function formPreSetData(FormEvent $event)
    {

    }
}