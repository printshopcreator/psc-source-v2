<?php

namespace Plugin\System\PSC\Bootstrap3\Form\Field;

use DirectoryIterator;
use Mpdf\Tag\TextArea;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Images extends AbstractController implements Field
{

    private $options;

    public function getTemplate()
    {
        return '@PluginSystemPSCBootstrap3/form/field/images.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $txt = "";
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $tempLayouts = [];
        if ($shopEntity->isCustomTemplates() == 1) {
            foreach (new DirectoryIterator('/data/www/old/application/design/vorlagen') as $file) {
                if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                    continue;
                }
                $tempLayouts[$file->getFileName()] = $file->getFileName();
            }
        } else {
            if (file_exists('/data/www/old/application/design/clients/' . $shopEntity->getUID())) {
                foreach (new DirectoryIterator('/data/www/old/application/design/clients/' . $shopEntity->getUID()) as $file) {
                    if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                        continue;
                    }
                    $tempLayouts[$file->getFileName()] = $file->getFileName();
                }
            }
        }
if(isset($_POST["settings"]["bootstrap3Images"]["layout"])) {
        header('location: /apps/backend/theme/settings');
        //die();
} else {
        //copy("/data/www/old/public/styles/vorlagen/" . $shopEntity->getLayout() . "/bootstrap/css/style.css","/data/www/old/public/styles/vorlagen/" . $shopEntity->getLayout() . "/bootstrap/css/style_bakup.css");
        $filename = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/config/images.ini";
        $handle = fopen($filename, 'r'); 
        $txt = fread($handle, filesize($filename)); 
        fclose($handle);   
}
if(!file_exists($filename)) {
    $handle = fopen($filename, 'w'); 
    fclose($handle);   
    chmod($filename, 0777);
}
        $builder
        ->add('imagesini', TextareaType::class, array(
            'label' => 'Images ini',
            'data' => $txt,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangeimages('imagesini');")
        ))
        ->add('imagesiniedit', HiddenType::class, array('required' => false, 'data' => 0))
        ;

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Bootstrap3\Form\Group\Images::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);
        $event->getForm()->get('bootstrap3General')->get('displayArticleCount')->setData($shopEntity->getDisplayArticleCount());
        $event->getForm()->get('bootstrap3General')->get('basketField1')->setData($shopEntity->getBasketfield1());
        $event->getForm()->get('bootstrap3General')->get('basketField2')->setData($shopEntity->getBasketfield2());
        $event->getForm()->get('bootstrap3General')->get('basketPosField1')->setData($shopEntity->getBasketposfield1());
        $event->getForm()->get('bootstrap3General')->get('basketPosField2')->setData($shopEntity->getBasketposfield2());
        $event->getForm()->get('bootstrap3General')->get('displaySender')->setData($shopEntity->getDisplaySender());
        $event->getForm()->get('bootstrap3General')->get('displayDelivery')->setData($shopEntity->getDisplayDelivery());
        $event->getForm()->get('bootstrap3General')->get('layout')->setData($shopEntity->getLayout());
        $event->getForm()->get('bootstrap3General')->get('customTemplates')->setData($shopEntity->isCustomTemplates());
        $event->getForm()->get('bootstrap3General')->get('googleanalyticscode')->setData($shopEntity->getGoogleanalyticscode());
        $event->getForm()->get('bootstrap3General')->get('defaultFunc')->setData($shopEntity->getDefaultFunc());
        $event->getForm()->get('bootstrap3General')->get('defaultParam')->setData($shopEntity->getDefaultParam());
        $event->getForm()->get('bootstrap3General')->get('templateDisplayProductsCrossselling')->setData($shopEntity->isTemplateDisplayProductsCrossselling());
        $event->getForm()->get('bootstrap3General')->get('templateDisplayUserApproval')->setData($shopEntity->isTemplateDisplayUserApproval());

    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        if($event->getForm()->get('bootstrap3Style')->get('write')->getData() == "1") {
            if($event->getForm()->get('bootstrap3Images')->get('imagesiniedit')->getData() == "1") {
                $filename = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/config/images.ini";
                $handle = fopen($filename, 'w'); 
                fputs($handle, $event->getForm()->get('bootstrap3Images')->get('imagesini')->getData()); 
                fclose($handle); 
            }
        }
        $shopEntity->setDisplayArticleCount($event->getForm()->get('bootstrap3General')->get('displayArticleCount')->getData());
        $shopEntity->setBasketfield1($event->getForm()->get('bootstrap3General')->get('basketField1')->getData());
        $shopEntity->setBasketfield2($event->getForm()->get('bootstrap3General')->get('basketField2')->getData());
        $shopEntity->setBasketposfield1($event->getForm()->get('bootstrap3General')->get('basketPosField1')->getData());
        $shopEntity->setBasketposfield2($event->getForm()->get('bootstrap3General')->get('basketPosField2')->getData());
        $shopEntity->setDisplaySender($event->getForm()->get('bootstrap3General')->get('displaySender')->getData());
        $shopEntity->setDisplayDelivery($event->getForm()->get('bootstrap3General')->get('displayDelivery')->getData());
        $shopEntity->setCustomTemplates($event->getForm()->get('bootstrap3General')->get('customTemplates')->getData());
        $shopEntity->setLayout($event->getForm()->get('bootstrap3General')->get('layout')->getData());
        $shopEntity->setGoogleanalyticscode($event->getForm()->get('bootstrap3General')->get('googleanalyticscode')->getData());
        $shopEntity->setDefaultFunc($event->getForm()->get('bootstrap3General')->get('defaultFunc')->getData());
        $shopEntity->setDefaultParam($event->getForm()->get('bootstrap3General')->get('defaultParam')->getData());
        $shopEntity->setTemplateDisplayProductsCrossselling($event->getForm()->get('bootstrap3General')->get('templateDisplayProductsCrossselling')->getData());
        $shopEntity->setTemplateDisplayUserApproval($event->getForm()->get('bootstrap3General')->get('templateDisplayUserApproval')->getData());
    }

    public function formPreSetData(FormEvent $event)
    {

    }
}