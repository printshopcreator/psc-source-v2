<?php

namespace Plugin\System\PSC\Bootstrap3\Form\Field;

use DirectoryIterator;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class General implements Field
{

    private $options;

    public function getTemplate()
    {
        return '@PluginSystemPSCBootstrap3/form/field/general.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $tempLayouts = [];
        if ($shopEntity->isCustomTemplates() == 1) {
            foreach (new DirectoryIterator('/data/www/old/application/design/vorlagen') as $file) {
                if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                    continue;
                }
                $tempLayouts[$file->getFileName()] = $file->getFileName();
            }
        } else {
            if (file_exists('/data/www/old/application/design/clients/' . $shopEntity->getUID())) {
                foreach (new DirectoryIterator('/data/www/old/application/design/clients/' . $shopEntity->getUID()) as $file) {
                    if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                        continue;
                    }
                    $tempLayouts[$file->getFileName()] = $file->getFileName();
                }
            }
        }

        $builder->add('displayArticleCount', NumberType::class, ['label' => 'Numberofitemsinlist'])
        ->add('displayMyOrderCount', NumberType::class, ['label' => 'Numberofitemsinmyorders', 'required' => false])
        ->add('basketField1', TextType::class, array('required' => false, 'label' => 'Shoppingcartfield1'))
        ->add('basketField2', TextType::class, array('required' => false, 'label' => 'Shoppingcartfield2'))
        ->add('basketPosField1', TextType::class, array('required' => false, 'label' => 'Shoppingcartpositionfield1'))
        ->add('basketPosField2', TextType::class, array('required' => false, 'label' => 'Shoppingcartpositionfield2'))
        ->add('customTemplates', CheckboxType::class, ['label' => 'Usetemplates', 'required' => false])
        ->add('templateDisplayProductsCrossselling', CheckboxType::class, ['label' => 'ShowCrossSelling', 'required' => false])
        ->add('templateDisplayUserApproval', CheckboxType::class, ['label' => 'Userreleaseneedyshow', 'required' => false])
        ->add('layout', ChoiceType::class, array('label' => 'Layout', 'choices' => $tempLayouts, 'required' => false, 'attr' => array('onchange' => "savestyle();")))

        ->add('googleanalyticscode', TextType::class, ['required' => false, 'label' => 'googleanalyticscode'])
        ->add('defaultFunc', ChoiceType::class, ['required' => false, 'label' => 'Defaultfunc',
            'choices' => [
                "Cms" => "Cms",
                "Overview" => "Overview",
                "Page" => "Page",
                "Article" => "Article",
                "Login" => "Login",
                "Index" => "Index",
                "OverviewWithRedirect" => "OverviewWithRedirect",
                "PersArticle" => "PersArticle",
                "Redirect" => "Redirect"
            ]
        ])
        ->add('defaultParam', TextType::class, ['required' => false, 'label' => 'Defaultparam'])


        ->add('displaySender', CheckboxType::class, ['required' => false, 'label' => 'Usesenderaddress'])
        ->add('displayDelivery', CheckboxType::class, ['required' => false, 'label' => 'Usedeliveryaddress'])

        ;

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Bootstrap3\Form\Group\General::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);

        if(isset($layoutSettings[$shopEntity->getLayout()])) {
            if(isset($layoutSettings[$shopEntity->getLayout()]['displayMyOrderCount'])) {
                $event->getForm()->get('bootstrap3General')->get('displayMyOrderCount')->setData(intval($layoutSettings[$shopEntity->getLayout()]['displayMyOrderCount']));
            }
        }  
        $event->getForm()->get('bootstrap3General')->get('displayArticleCount')->setData($shopEntity->getDisplayArticleCount());
        $event->getForm()->get('bootstrap3General')->get('basketField1')->setData($shopEntity->getBasketfield1());
        $event->getForm()->get('bootstrap3General')->get('basketField2')->setData($shopEntity->getBasketfield2());
        $event->getForm()->get('bootstrap3General')->get('basketPosField1')->setData($shopEntity->getBasketposfield1());
        $event->getForm()->get('bootstrap3General')->get('basketPosField2')->setData($shopEntity->getBasketposfield2());
        $event->getForm()->get('bootstrap3General')->get('displaySender')->setData($shopEntity->getDisplaySender());
        $event->getForm()->get('bootstrap3General')->get('displayDelivery')->setData($shopEntity->getDisplayDelivery());
        $event->getForm()->get('bootstrap3General')->get('layout')->setData($shopEntity->getLayout());
        $event->getForm()->get('bootstrap3General')->get('customTemplates')->setData($shopEntity->isCustomTemplates());
        $event->getForm()->get('bootstrap3General')->get('googleanalyticscode')->setData($shopEntity->getGoogleanalyticscode());
        $event->getForm()->get('bootstrap3General')->get('defaultFunc')->setData($shopEntity->getDefaultFunc());
        $event->getForm()->get('bootstrap3General')->get('defaultParam')->setData($shopEntity->getDefaultParam());
        $event->getForm()->get('bootstrap3General')->get('templateDisplayProductsCrossselling')->setData($shopEntity->isTemplateDisplayProductsCrossselling());
        $event->getForm()->get('bootstrap3General')->get('templateDisplayUserApproval')->setData($shopEntity->isTemplateDisplayUserApproval());

    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $shopEntity->setDisplayArticleCount($event->getForm()->get('bootstrap3General')->get('displayArticleCount')->getData());
        $shopEntity->setBasketfield1($event->getForm()->get('bootstrap3General')->get('basketField1')->getData());
        $shopEntity->setBasketfield2($event->getForm()->get('bootstrap3General')->get('basketField2')->getData());
        $shopEntity->setBasketposfield1($event->getForm()->get('bootstrap3General')->get('basketPosField1')->getData());
        $shopEntity->setBasketposfield2($event->getForm()->get('bootstrap3General')->get('basketPosField2')->getData());
        $shopEntity->setDisplaySender($event->getForm()->get('bootstrap3General')->get('displaySender')->getData());
        $shopEntity->setDisplayDelivery($event->getForm()->get('bootstrap3General')->get('displayDelivery')->getData());
        $shopEntity->setCustomTemplates($event->getForm()->get('bootstrap3General')->get('customTemplates')->getData());
        $shopEntity->setLayout($event->getForm()->get('bootstrap3General')->get('layout')->getData());
        $shopEntity->setGoogleanalyticscode($event->getForm()->get('bootstrap3General')->get('googleanalyticscode')->getData());
        $shopEntity->setDefaultFunc($event->getForm()->get('bootstrap3General')->get('defaultFunc')->getData());
        $shopEntity->setDefaultParam($event->getForm()->get('bootstrap3General')->get('defaultParam')->getData());
        $shopEntity->setTemplateDisplayProductsCrossselling($event->getForm()->get('bootstrap3General')->get('templateDisplayProductsCrossselling')->getData());
        $shopEntity->setTemplateDisplayUserApproval($event->getForm()->get('bootstrap3General')->get('templateDisplayUserApproval')->getData());
        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);
        $layoutSettings[$shopEntity->getLayout()]['displayMyOrderCount'] = $event->getForm()->get('bootstrap3General')->get('displayMyOrderCount')->getData();

        $shopEntity->setLayoutSettings(json_encode($layoutSettings));

    }

    public function formPreSetData(FormEvent $event)
    {

    }
}
