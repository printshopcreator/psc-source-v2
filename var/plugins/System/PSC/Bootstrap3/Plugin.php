<?php
namespace Plugin\System\PSC\Bootstrap3;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Bootstrap 3 Standard Theme';

    protected $defaultInstall = true;

    public function getType()
    {
        return Plugin::Theme;
    }

    public function getDescription()
    {
        return 'Bootstrap 3 Standard Theme';
    }

    public function getVersion()
    {
        return 1;
    }
}