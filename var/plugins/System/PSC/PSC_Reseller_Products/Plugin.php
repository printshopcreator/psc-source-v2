<?php

namespace Plugin\System\PSC\PSC_Reseller_Products;

use PSC\System\PluginBundle\Plugin\Base;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin
{

    protected $name = 'PSC Reseller Products';

    public function getType()
    {
        return Plugin::ProductType;
    }

    public function getDescription()
    {
        return 'PSC Reseller Products';
    }

    public function getVersion()
    {
        return 1;
    }
}

