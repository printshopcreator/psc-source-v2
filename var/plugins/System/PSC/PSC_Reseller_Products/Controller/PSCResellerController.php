<?php

namespace Plugin\System\PSC\PSC_Reseller_Products\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Plugin\System\PSC\PSC_Reseller_Products\Api\GetAvailableOptions;
use Plugin\System\PSC\PSC_Reseller_Products\Api\GetPrices;
use Plugin\System\PSC\PSC_Reseller_Products\Api\GetPricesV3;
use Plugin\System\PSC\PSC_Reseller_Products\Api\GetProducts;
use Plugin\System\PSC\PSC_Reseller_Products\Api\GetDefault;
use FOS\JsRoutingBundle\Controller\Controller;
use Plugin\System\PSC\PSC_Reseller_Products\Form\ResellerSelectType;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class PSCResellerController
 *
 * @package Plugin/Custom/PSC/PSC_Reseller_Products
 */
class PSCResellerController extends AbstractController
{
    /**
     * creative
     *
     * @Route("/product/edit", name="psc_plugin_reseller_backend_design_edit")
     * @template()
     *
     * @return array
     */
    public function editAction(Shop $shopService)
    {
        $form = $this->createForm(ResellerSelectType::class);
        return ['form' => $form->createView(), 'shop' => $shopService->getSelectedShop()];
    }



}

