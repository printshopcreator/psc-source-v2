<?php

namespace Plugin\System\PSC\PSC_Reseller_Products\Service;

use PSC\Shop\ProductBundle\Interfaces\IJsonFormProducer;

class ProductType implements \PSC\System\PluginBundle\Product\Type
{

    public function getId()
    {
        return 106;
    }

    public function getName()
    {
        return 'PSC Reseller Produkt Typ';
    }
    
    public function getPositionProductTransformer()
    {
        // TODO: Implement getPositionProductTransformer() method.
    }

    public function getJsonFormProducer(): IJsonFormProducer
    {
        // TODO: Implement getJsonFormProducer() method.
    }
}
