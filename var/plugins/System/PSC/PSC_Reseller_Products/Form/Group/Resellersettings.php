<?php

namespace Plugin\System\PSC\PSC_Reseller_Products\Form\Group;


use Ddeboer\DataImport\Reader\CsvReader;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\System\PluginBundle\Form\Group;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Resellersettings extends Group
{
    const GROUP_ID = 'reseller';

    public function __construct()
    {
        $this->title = 'External Product URL';
    }

    public function getModule()
    {
        return Field::Shop;
    }

    public function getId()
    {
        return self::GROUP_ID;
    }
}