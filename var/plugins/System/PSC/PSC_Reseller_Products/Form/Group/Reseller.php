<?php

namespace Plugin\System\PSC\PSC_Reseller_Products\Form\Group;

use PSC\System\PluginBundle\Form\Group;
use PSC\System\PluginBundle\Form\Interfaces\Field;

class Reseller extends Group
{
    const GROUP_ID = 'reseller';

    public function __construct()
    {
        $this->title = 'PSC Reseller';
    }

    public function getModule()
    {
        return Field::Product;
    }

    public function getId()
    {
        return self::GROUP_ID;
    }
}

