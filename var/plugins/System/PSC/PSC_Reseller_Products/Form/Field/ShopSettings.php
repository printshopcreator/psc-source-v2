<?php

namespace Plugin\System\PSC\PSC_Reseller_Products\Form\Field;

use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use Plugin\System\PSC\PSC_Reseller_Products\Form\Group\Resellersettings;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ShopSettings implements Field
{

    public function getTemplate()
    {
        return '@PluginSystemPSCPSC_Reseller_Products/form/field/shop_settings.html.twig';
    }

    public function getModule()
    {
        return Field::Shop;
    }

    /**
     * @param array $data
     */
    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $data */
        $data = $event->getData();
        $data->setPluginSettingModule('reseller', 'name1', $event->getForm()->get('reseller')->get('name1')->getData());
        $data->setPluginSettingModule('reseller', 'url1', $event->getForm()->get('reseller')->get('url1')->getData());
        $data->setPluginSettingModule('reseller', 'uuid1', $event->getForm()->get('reseller')->get('uuid1')->getData());
        $data->setPluginSettingModule('reseller', 'aufschlag1', $event->getForm()->get('reseller')->get('aufschlag1')->getData());
        $data->setPluginSettingModule('reseller', 'name2', $event->getForm()->get('reseller')->get('name2')->getData());
        $data->setPluginSettingModule('reseller', 'url2', $event->getForm()->get('reseller')->get('url2')->getData());
        $data->setPluginSettingModule('reseller', 'uuid2', $event->getForm()->get('reseller')->get('uuid1')->getData());
        $data->setPluginSettingModule('reseller', 'aufschlag2', $event->getForm()->get('reseller')->get('aufschlag2')->getData());
        $data->setPluginSettingModule('reseller', 'name3', $event->getForm()->get('reseller')->get('name3')->getData());
        $data->setPluginSettingModule('reseller', 'url3', $event->getForm()->get('reseller')->get('url3')->getData());
        $data->setPluginSettingModule('reseller', 'uuid3', $event->getForm()->get('reseller')->get('uuid1')->getData());
        $data->setPluginSettingModule('reseller', 'aufschlag3', $event->getForm()->get('reseller')->get('aufschlag3')->getData());
        $data->setPluginSettingModule('reseller', 'name4', $event->getForm()->get('reseller')->get('name4')->getData());
        $data->setPluginSettingModule('reseller', 'url4', $event->getForm()->get('reseller')->get('url4')->getData());
        $data->setPluginSettingModule('reseller', 'uuid4', $event->getForm()->get('reseller')->get('uuid1')->getData());
        $data->setPluginSettingModule('reseller', 'aufschlag4', $event->getForm()->get('reseller')->get('aufschlag4')->getData());
        $data->setPluginSettingModule('reseller', 'name5', $event->getForm()->get('reseller')->get('name5')->getData());
        $data->setPluginSettingModule('reseller', 'url5', $event->getForm()->get('reseller')->get('url5')->getData());
        $data->setPluginSettingModule('reseller', 'uuid5', $event->getForm()->get('reseller')->get('uuid1')->getData());
        $data->setPluginSettingModule('reseller', 'aufschlag5', $event->getForm()->get('reseller')->get('aufschlag5')->getData());
        $event->setData($data);
    }

    public function formPostSetData(FormEvent $event)
    {
        /** @var Shop $data */
        $data = $event->getData();
        $event->getForm()->get('reseller')->get('name1')->setData($data->getPluginSettingModule('reseller', 'name1'));
        $event->getForm()->get('reseller')->get('url1')->setData($data->getPluginSettingModule('reseller', 'url1'));
        $event->getForm()->get('reseller')->get('uuid1')->setData($data->getPluginSettingModule('reseller', 'uuid5'));
        $event->getForm()->get('reseller')->get('aufschlag1')->setData($data->getPluginSettingModule('reseller', 'aufschlag1'));
        $event->getForm()->get('reseller')->get('name2')->setData($data->getPluginSettingModule('reseller', 'name2'));
        $event->getForm()->get('reseller')->get('url2')->setData($data->getPluginSettingModule('reseller', 'url2'));
        $event->getForm()->get('reseller')->get('uuid2')->setData($data->getPluginSettingModule('reseller', 'uuid5'));
        $event->getForm()->get('reseller')->get('aufschlag2')->setData($data->getPluginSettingModule('reseller', 'aufschlag2'));
        $event->getForm()->get('reseller')->get('name3')->setData($data->getPluginSettingModule('reseller', 'name3'));
        $event->getForm()->get('reseller')->get('url3')->setData($data->getPluginSettingModule('reseller', 'url3'));
        $event->getForm()->get('reseller')->get('uuid3')->setData($data->getPluginSettingModule('reseller', 'uuid5'));
        $event->getForm()->get('reseller')->get('aufschlag3')->setData($data->getPluginSettingModule('reseller', 'aufschlag3'));
        $event->getForm()->get('reseller')->get('name4')->setData($data->getPluginSettingModule('reseller', 'name4'));
        $event->getForm()->get('reseller')->get('url4')->setData($data->getPluginSettingModule('reseller', 'url4'));
        $event->getForm()->get('reseller')->get('uuid4')->setData($data->getPluginSettingModule('reseller', 'uuid5'));
        $event->getForm()->get('reseller')->get('aufschlag4')->setData($data->getPluginSettingModule('reseller', 'aufschlag4'));
        $event->getForm()->get('reseller')->get('name5')->setData($data->getPluginSettingModule('reseller', 'name5'));
        $event->getForm()->get('reseller')->get('url5')->setData($data->getPluginSettingModule('reseller', 'url5'));
        $event->getForm()->get('reseller')->get('uuid5')->setData($data->getPluginSettingModule('reseller', 'uuid5'));
        $event->getForm()->get('reseller')->get('aufschlag5')->setData($data->getPluginSettingModule('reseller', 'aufschlag5'));
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name1', TextType::class, array(
            'label' => 'Name',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('url1', TextType::class, array(
            'label' => 'URL',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('uuid1', TextType::class, array(
            'label' => 'Shop UUId',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('aufschlag1', TextType::class, array(
		    'label' => 'Aufschlag',
            'required' => false,
            'mapped' => false
        ));

        $builder->add('name2', TextType::class, array(
            'label' => 'Name',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('url2', TextType::class, array(
            'label' => 'URL',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('uuid2', TextType::class, array(
            'label' => 'Shop UUId',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('aufschlag2', TextType::class, array(
            'label' => 'Aufschlag',
            'required' => false,
            'mapped' => false
        ));

        $builder->add('name3', TextType::class, array(
            'label' => 'Name',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('url3', TextType::class, array(
            'label' => 'URL',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('uuid3', TextType::class, array(
            'label' => 'Shop UUId',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('aufschlag3', TextType::class, array(
            'label' => 'Aufschlag',
            'required' => false,
            'mapped' => false
        ));

        $builder->add('name4', TextType::class, array(
            'label' => 'Name',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('url4', TextType::class, array(
            'label' => 'URL',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('uuid4', TextType::class, array(
            'label' => 'Shop UUId',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('aufschlag4', TextType::class, array(
            'label' => 'Aufschlag',
            'required' => false,
            'mapped' => false
        ));

        $builder->add('name5', TextType::class, array(
            'label' => 'Name',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('url5', TextType::class, array(
            'label' => 'URL',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('uuid5', TextType::class, array(
            'label' => 'Shop UUId',
            'required' => false,
            'mapped'=> false
        ));
        $builder->add('aufschlag5', TextType::class, array(
            'label' => 'Aufschlag',
            'required' => false,
            'mapped' => false
        ));

        return $builder;
    }

    public function getGroup()
    {
        return Resellersettings::GROUP_ID;
    }

    public function formPreSubmit(FormEvent $event)
    {
        // TODO: Implement formPreSubmit() method.
    }

    public function formPreSetData(FormEvent $event)
    {
    }
}
