<?php

namespace Plugin\System\PSC\PSC_Reseller_Products\Form\Field;


use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use Plugin\System\PSC\PSC_Reseller_Products\Api\GetProducts;
use Plugin\System\PSC\PSC_Reseller_Products\Form\Group\Reseller;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Tax;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ResellerProduct implements Field
{
    protected $taxService;

    public function __construct(Tax $taxService)
    {
        $this->taxService = $taxService;
    }
    public function getTemplate()
    {
        return '@PluginSystemPSCPSC_Reseller_Products/form/field/reseller_product.html.twig';
    }

    public function getModule()
    {
        return Field::Product;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add("mwert", ChoiceType::class, array('label' => 'MwSt.',
            'choices' => $this->taxService->getTaxesForForm(),
        ))->add('usetextandimages', CheckboxType::class, array(
            'label' => 'Bilder, Texte und Druckdaten Infos von externem Produkt anzeigen',
            'required' => false
        ))->add('auflage', TextType::class, array(
            'label' => 'Auflage Startwert:',
            'required' => false
        ))->add('product', TextType::class, array(
            'label' => 'Produkt UUID'
        ))->add('aufschlag', TextType::class, array(
            'label' => 'Aufschlag in %:',
            'required' => false
        ))->add('uploadXML', AceEditorType::class, array(
            'wrapper_attr' => array(), // aceeditor wrapper html attributes.
            'width' => '90%',
            'height' => '500',
            'font_size' => 14,
            'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
            'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
            'tab_size' => null,
            'read_only' => null,
            'use_soft_tabs' => null,
            'use_wrap_mode' => null,
            'show_print_margin' => null,
            'required' => false,
            'highlight_active_line' => null,
            'label' => 'Upload XML'
        ));
        return $builder;
    }

    public function getGroup()
    {
        return Reseller::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        /** @var Product $data */
        $data = $event->getData();
        $event->getForm()->get('reseller')->get('mwert')->setData($data->getMwert());
        $event->getForm()->get('reseller')->get('uploadXML')->setData((string)$data->getCalcXml());
    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Product $data */
        $data = $event->getData();
        $data->setMwert($event->getForm()->get('reseller')->get('mwert')->getData());
        $data->setCalcXml($event->getForm()->get('reseller')->get('uploadXML')->getData());
        $event->setData($data);
    }

    public function formPreSetData(FormEvent $event)
    {
        // TODO: Implement formPreSetData() method.
    }
}

