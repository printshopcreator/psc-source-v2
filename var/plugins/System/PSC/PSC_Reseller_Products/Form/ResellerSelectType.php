<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace Plugin\System\PSC\PSC_Reseller_Products\Form;

use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class ResellerSelectType extends AbstractType
{
    private Shop $shopService;

    public function __construct(Shop $shopService)
    {
        $this->shopService = $shopService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tmp = [];

        $shop = $this->shopService->getMongoSelectedShop();
        for($i=1; $i < 6; $i++) {
            if($shop->getPluginSettingModule('reseller', 'name' . $i)) {
                $tmp[$shop->getPluginSettingModule('reseller', 'name' . $i)] = $shop->getPluginSettingModule('reseller', 'url' . $i) . '#' . $shop->getPluginSettingModule('reseller', 'uuid' . $i);
            }
        }

        $builder
            ->add('reseller', ChoiceType::class, [
                'label' => 'Reseller',
                'choices' => $tmp
            ]);
    }

    public function getName()
    {
        return 'reseller';
    }

}
