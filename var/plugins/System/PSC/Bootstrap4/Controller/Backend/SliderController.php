<?php
namespace Plugin\System\PSC\Bootstrap4\Controller\Backend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Plugin\System\PSC\Bootstrap4\Form\SlideType;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Shopsetting;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CheckController
 *
 * @Route("/slider")
 */
class SliderController extends AbstractController
{
    /**
     * @Route("/", name="psc_plugin_bootstrap4_backend_slider_index")
     * @template()
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @return array[]
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(\PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager)
    {
        $selectedShop = $shopService->getSelectedShop();

        $settings = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shopsetting')->findBy(array("shop" => $selectedShop->getUid(), "key" => "index_slides"));

        $tmp = [];
        foreach($settings as $setting) {
            $tmp[] = array_merge(['uid' => $setting->getUid()], json_decode($setting->getValue(), true));
        }

        return ['slides' => $tmp];
    }

    /**
     * @Route("/edit/{uid}", defaults={"uid" = "" }, name="psc_plugin_bootstrap4_backend_slider_edit")
     * @template()
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uid)
    {
        $selectedShop = $shopService->getSelectedShop();

        $data = ["title" => ""];
        if($uid) {
            /** @var Shopsetting $setting */
            $setting = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shopsetting')->findOneBy(array("shop" => $selectedShop->getUid(), "uid" => $uid));

            $data = array_merge(['uid' => $setting->getUid()], json_decode($setting->getValue(), true));
        }

        $form = $this->createForm(SlideType::class, $data);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $dataForm = $form->getData();
            if($uid) {
                $setting->setValue(json_encode([
                    'title' => $dataForm['title'],
                    'link' => $dataForm['link'],
                    'text' => $dataForm['text'],
                    'image' => $dataForm['image']
                ]));

            }else{
                $setting = new Shopsetting();
                $setting->setShop($selectedShop);
                $setting->setUid(time());
                $setting->setKey("index_slides");
                $setting->setValue(json_encode([
                    'title' => $dataForm['title'],
                    'link' => $dataForm['link'],
                    'text' => $dataForm['text'],
                    'image' => $dataForm['image']
                ]));
            }

            $entityManager->persist($setting);
            $entityManager->flush();

            return $this->redirectToRoute("psc_plugin_bootstrap4_backend_slider_index");

        }

        return ['form' => $form->createView(), "slide" => $data];
    }

    /**
     * @Route("/delete/{uid}", name="psc_plugin_bootstrap4_backend_slider_delete")
     * @template()
     * @param Request $request
     * @param SessionInterface $session
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAction(Request $request, SessionInterface $session, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uid)
    {
        $selectedShop = $shopService->getSelectedShop();

        if($uid) {
            /** @var Shopsetting $setting */
            $setting = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shopsetting')->findOneBy(array("shop" => $selectedShop->getUid(), "uid" => $uid));

            $data = array_merge(['uid' => $setting->getUid()], json_decode($setting->getValue(), true));
        }

        $form = $this->createForm(\PSC\Shop\NewsBundle\Form\Backend\DeleteType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $title = $data['title'];
                $entityManager->remove($setting);
                $entityManager->flush();
                $session->getFlashBag()->add(
                    'success',
                    'Slide \'' . $title . '\' has been deleted!'
                );
                return $this->redirectToRoute('psc_plugin_bootstrap4_backend_slider_index');
            }
            return $this->redirectToRoute('psc_plugin_bootstrap4_backend_slider_index');
        }

        return ['slide' => $data, 'form' => $form->createView()];
    }
}