<?php
namespace Plugin\System\PSC\Bootstrap4;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Bootstrap 4 Standard Theme';

    protected $defaultInstall = true;

    public function getType()
    {
        return Plugin::Theme;
    }

    public function getDescription()
    {
        return 'Bootstrap 4 Standard Theme';
    }

    public function getVersion()
    {
        return 1;
    }
}