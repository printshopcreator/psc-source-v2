<?php

namespace Plugin\System\PSC\Bootstrap4\Form\Field\Product;

use DirectoryIterator;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Shopsetting;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PostUpload implements Field
{

    private $_entityManager;

    /** @var \PSC\System\SettingsBundle\Service\Shop */
    private $_shopService;

    private $options;

    public function getTemplate()
    {
        return '@PluginSystemPSCBootstrap4/form/field/product/post_upload.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $builder
            ->add('textPost', TextareaType::class, array(
                'label' => 'Text Post',
                'required' => false,
                'attr' => ['rows' => 10]
            ))
            ->add('textMail', TextareaType::class, array(
                'label' => 'Text EMail',
                'required' => false,
                'attr' => ['rows' => 10]
            ));

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Bootstrap4\Form\Group\Product::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);

        if(isset($layoutSettings[$shopEntity->getLayout()]['textPost'])) {
            $event->getForm()->get('bootstrap4Product')->get('textPost')->setData($layoutSettings[$shopEntity->getLayout()]['textPost']);
        }
        if(isset($layoutSettings[$shopEntity->getLayout()]['textMail'])) {
            $event->getForm()->get('bootstrap4Product')->get('textMail')->setData($layoutSettings[$shopEntity->getLayout()]['textMail']);
        }
    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);

        $layoutSettings[$shopEntity->getLayout()]['textPost'] = $event->getForm()->get('bootstrap4Product')->get('textPost')->getData();
        $layoutSettings[$shopEntity->getLayout()]['textMail'] = $event->getForm()->get('bootstrap4Product')->get('textMail')->getData();

        $shopEntity->setLayoutSettings(json_encode($layoutSettings));
    }

    public function formPreSetData(FormEvent $event)
    {
    }

    /**
     * @param mixed $entityManager
     */
    public function setEntityManager($entityManager)
    {
        $this->_entityManager = $entityManager;
    }

    /**
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     */
    public function setShopService($shopService)
    {
        $this->_shopService = $shopService;
    }
}

