<?php

namespace Plugin\System\PSC\Bootstrap4\Form\Field;

use DirectoryIterator;
use Mpdf\Tag\TextArea;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Sites extends AbstractController implements Field
{

    private $options;

    public function getTemplate()
    {
        return '@PluginSystemPSCBootstrap4/form/field/sites.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $txt = "";
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $tempLayouts = [];
        if ($shopEntity->isCustomTemplates() == 1) {
            foreach (new DirectoryIterator('/data/www/old/application/design/vorlagen') as $file) {
                if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                    continue;
                }
                $tempLayouts[$file->getFileName()] = $file->getFileName();
            }
        } else {
            if (file_exists('/data/www/old/application/design/clients/' . $shopEntity->getUID())) {
                foreach (new DirectoryIterator('/data/www/old/application/design/clients/' . $shopEntity->getUID()) as $file) {
                    if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                        continue;
                    }
                    $tempLayouts[$file->getFileName()] = $file->getFileName();
                }
            }
        }
if(isset($_POST["settings"]["bootstrap4Images"]["layout"])) {
        header('location: /apps/backend/theme/settings');
        //die();
} else {
        //copy("/data/www/old/public/styles/vorlagen/" . $shopEntity->getLayout() . "/bootstrap/css/style.css","/data/www/old/public/styles/vorlagen/" . $shopEntity->getLayout() . "/bootstrap/css/style_bakup.css");
        $filenamedefault = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/layout/default.phtml";
        $handledefault = fopen($filenamedefault, 'r'); 
        $txtdefault = fread($handledefault, filesize($filenamedefault)); 
        fclose($handledefault); 

        $filenamehauptmenu = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/layout/_hauptmenu.phtml";
        $handlehauptmenu = fopen($filenamehauptmenu, 'r'); 
        $txthauptmenu = fread($handlehauptmenu, filesize($filenamehauptmenu)); 
        fclose($handlehauptmenu);
        
        $filenamebasketindex = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/basket/index.phtml";
        $handlebasketindex = fopen($filenamebasketindex, 'r'); 
        $txtbasketindex = fread($handlebasketindex, filesize($filenamebasketindex)); 
        fclose($handlebasketindex); 

        $filenamereview = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/basket/review.phtml";
        $handlereview = fopen($filenamereview, 'r'); 
        $txtreview = fread($handlereview, filesize($filenamereview)); 
        fclose($handlereview); 

        $filenamefinish = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/basket/finish.phtml";
        $handlefinish = fopen($filenamefinish, 'r'); 
        $txtfinish = fread($handlefinish, filesize($filenamefinish)); 
        fclose($handlefinish); 

        $filenamedone = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/basket/done.phtml";
        $handledone = fopen($filenamedone, 'r'); 
        $txtdone = fread($handledone, filesize($filenamedone)); 
        fclose($handledone);

        $filenamewarehouse = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/articletemplates/scripts/2_basket_index.phtml";
        $handlewarehouse = fopen($filenamewarehouse, 'r'); 
        $txtwarehouse = fread($handlewarehouse, filesize($filenamewarehouse)); 
        fclose($handlewarehouse);

        $filenamekalkproducts = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/articletemplates/scripts/6_basket_index.phtml";
        $handlekalkproducts = fopen($filenamekalkproducts, 'r'); 
        $txtkalkproducts = fread($handlekalkproducts, filesize($filenamekalkproducts)); 
        fclose($handlekalkproducts);

        $filenamelogin= "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/user/login.phtml";
        $handlelogin = fopen($filenamelogin, 'r'); 
        $txtlogin = fread($handlelogin, filesize($filenamelogin)); 
        fclose($handlelogin);
        
        $filenameregister= "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/user/register.phtml";
        $handleregister = fopen($filenameregister, 'r'); 
        $txtregister = fread($handleregister, filesize($filenameregister)); 
        fclose($handleregister);

        $filenamemyoverview= "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/user/myoverview.phtml";
        $handlemyoverview = fopen($filenamemyoverview, 'r'); 
        $txtmyoverview = fread($handlemyoverview, filesize($filenamemyoverview)); 
        fclose($handlemyoverview);

        $filenamewarehousedetails = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/articletemplates/scripts/2.phtml";
        $handlewarehousedetails = fopen($filenamewarehousedetails, 'r'); 
        $txtwarehousedetails = fread($handlewarehousedetails, filesize($filenamewarehousedetails)); 
        fclose($handlewarehousedetails);

        $filenamekalkproductsdetails = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/articletemplates/scripts/6.phtml";
        $handlekalkproductsdetails = fopen($filenamekalkproductsdetails, 'r'); 
        $txtkalkproductsdetails = fread($handlekalkproductsdetails, filesize($filenamekalkproductsdetails)); 
        fclose($handlekalkproductsdetails);

        $filenamesaxoprintdetails = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/articletemplates/scripts/100.phtml";
        if (file_exists($filenamesaxoprintdetails)) {
        $handlesaxoprintdetails = fopen($filenamesaxoprintdetails, 'r'); 
        $txtsaxoprintdetails = fread($handlesaxoprintdetails, filesize($filenamesaxoprintdetails)); 
        fclose($handlesaxoprintdetails);
        }

        $filenamecmsindex= "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/templates/cms/index.phtml";
        $handlecmsindex = fopen($filenamecmsindex, 'r'); 
        $txtcmsindex = fread($handlecmsindex, filesize($filenamecmsindex)); 
        fclose($handlecmsindex);
}

        $builder
        ->add('default', TextareaType::class, array(
            'label' => 'default.phtml',
            'data' => $txtdefault,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('default');")
        ))
        ->add('defaultedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('hauptmenu', TextareaType::class, array(
            'label' => 'hauptmenu.phtml',
            'data' => $txthauptmenu,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('hauptmenu');")
        ))
        ->add('hauptmenuedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('basketindex', TextareaType::class, array(
            'label' => 'Basket index.phtml',
            'data' => $txtbasketindex,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('basketindex');")
        ))
        ->add('basketindexedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('basketreview', TextareaType::class, array(
            'label' => 'Basket review.phtml',
            'data' => $txtreview,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('basketreview');")
        ))
        ->add('basketreviewedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('basketfinish', TextareaType::class, array(
            'label' => 'Basket finish.phtml',
            'data' => $txtfinish,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('basketfinish');")
        ))
        ->add('basketfinishedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('basketdone', TextareaType::class, array(
            'label' => 'Basket done.phtml',
            'data' => $txtdone,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('basketdone');")
        ))
        ->add('basketdoneedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('warehouse', TextareaType::class, array(
            'label' => 'Basket 2_basket_index.phtml',
            'data' => $txtwarehouse,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('warehouse');")
        ))
        ->add('warehouseedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('kalkproducts', TextareaType::class, array(
            'label' => 'Basket 6_basket_index.phtml',
            'data' => $txtkalkproducts,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('kalkproducts');")
        ))
        ->add('kalkproductsedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('userlogin', TextareaType::class, array(
            'label' => 'User login.phtml',
            'data' => $txtlogin,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('userlogin');")
        ))
        ->add('userloginedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('userregister', TextareaType::class, array(
            'label' => 'User register.phtml',
            'data' => $txtregister,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('userregister');")
        ))
        ->add('userregisteredit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('usermyoverview', TextareaType::class, array(
            'label' => 'User usermyoverview.phtml',
            'data' => $txtmyoverview,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('usermyoverview');")
        ))
        ->add('usermyoverviewedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('warehousedetails', TextareaType::class, array(
            'label' => 'Basket 2.phtml',
            'data' => $txtwarehousedetails,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('warehousedetails');")
        ))
        ->add('warehousedetailsedit', HiddenType::class, array('required' => false, 'data' => 0))
        ->add('kalkproductsdetails', TextareaType::class, array(
            'label' => 'Basket 6.phtml',
            'data' => $txtkalkproductsdetails,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('kalkproductsdetails');")
        ))
        ->add('kalkproductsdetailsedit', HiddenType::class, array('required' => false, 'data' => 0));
        if (file_exists($filenamesaxoprintdetails)) {
        $builder
        ->add('saxoprintdetails', TextareaType::class, array(
            'label' => 'Basket 100.phtml',
            'data' => $txtsaxoprintdetails,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('saxoprintdetails');")
        ))
        ->add('saxoprintdetailsedit', HiddenType::class, array('required' => false, 'data' => 0))
        ;
        }
        $builder
        ->add('cmsindex', TextareaType::class, array(
            'label' => 'CMS index.phtml',
            'data' => $txtcmsindex,
            'required' => false,
            'attr' => array('rows' => 50, 'onclick' => "onchangesites('cmsindex');")
        ))
        ->add('cmsindexedit', HiddenType::class, array('required' => false, 'data' => 0));
        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Bootstrap4\Form\Group\Sites::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);
        $event->getForm()->get('bootstrap4General')->get('displayArticleCount')->setData($shopEntity->getDisplayArticleCount());
        $event->getForm()->get('bootstrap4General')->get('basketField1')->setData($shopEntity->getBasketfield1());
        $event->getForm()->get('bootstrap4General')->get('basketField2')->setData($shopEntity->getBasketfield2());
        $event->getForm()->get('bootstrap4General')->get('basketPosField1')->setData($shopEntity->getBasketposfield1());
        $event->getForm()->get('bootstrap4General')->get('basketPosField2')->setData($shopEntity->getBasketposfield2());
        $event->getForm()->get('bootstrap4General')->get('displaySender')->setData($shopEntity->getDisplaySender());
        $event->getForm()->get('bootstrap4General')->get('displayDelivery')->setData($shopEntity->getDisplayDelivery());
        $event->getForm()->get('bootstrap4General')->get('layout')->setData($shopEntity->getLayout());
        $event->getForm()->get('bootstrap4General')->get('customTemplates')->setData($shopEntity->isCustomTemplates());
        $event->getForm()->get('bootstrap4General')->get('googleanalyticscode')->setData($shopEntity->getGoogleanalyticscode());
        $event->getForm()->get('bootstrap4General')->get('defaultFunc')->setData($shopEntity->getDefaultFunc());
        $event->getForm()->get('bootstrap4General')->get('defaultParam')->setData($shopEntity->getDefaultParam());
        $event->getForm()->get('bootstrap4General')->get('templateDisplayProductsCrossselling')->setData($shopEntity->isTemplateDisplayProductsCrossselling());
        $event->getForm()->get('bootstrap4General')->get('templateDisplayUserApproval')->setData($shopEntity->isTemplateDisplayUserApproval());

    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        if($event->getForm()->get('bootstrap4Style')->get('write')->getData() == "1") {
            if($event->getForm()->get('bootstrap4Sites')->get('defaultedit')->getData() == "1") {
                $filenamedefault = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/layout/default.phtml";
                $handledefault = fopen($filenamedefault, 'w'); 
                fputs($handledefault, $event->getForm()->get('bootstrap4Sites')->get('default')->getData()); 
                fclose($handledefault); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('hauptmenuedit')->getData() == "1") {
                $filenamehauptmenu = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/layout/_hauptmenu.phtml";
                $handlehauptmenu = fopen($filenamehauptmenu, 'w'); 
                fputs($handlehauptmenu, $event->getForm()->get('bootstrap4Sites')->get('hauptmenu')->getData()); 
                fclose($handlehauptmenu); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('basketindexedit')->getData() == "1") {
                $filenamebasketindex = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/basket/index.phtml";
                $handlebasketindex = fopen($filenamebasketindex, 'w'); 
                fputs($handlebasketindex, $event->getForm()->get('bootstrap4Sites')->get('basketindex')->getData()); 
                fclose($handlebasketindex); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('basketreviewedit')->getData() == "1") {
                $filenamereview = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/basket/review.phtml";
                $handlereview = fopen($filenamereview, 'w'); 
                fputs($handlereview, $event->getForm()->get('bootstrap4Sites')->get('basketreview')->getData()); 
                fclose($handlereview); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('basketfinishedit')->getData() == "1") {
                $filenamefinish = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/basket/finish.phtml";
                $handlefinish = fopen($filenamefinish, 'w'); 
                fputs($handlefinish, $event->getForm()->get('bootstrap4Sites')->get('basketfinish')->getData()); 
                fclose($handlefinish); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('basketdoneedit')->getData() == "1") {
                $filenamedone = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/basket/done.phtml";
                $handledone = fopen($filenamedone, 'w'); 
                fputs($handledone, $event->getForm()->get('bootstrap4Sites')->get('basketdone')->getData()); 
                fclose($handledone); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('warehouseedit')->getData() == "1") {
                $filenamedone = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/articletemplates/scripts/2_basket_index.phtml";
                $handledone = fopen($filenamedone, 'w'); 
                fputs($handledone, $event->getForm()->get('bootstrap4Sites')->get('warehouse')->getData()); 
                fclose($handledone); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('kalkproductsedit')->getData() == "1") {
                $filenamedone = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/articletemplates/scripts/6_basket_index.phtml";
                $handledone = fopen($filenamedone, 'w'); 
                fputs($handledone, $event->getForm()->get('bootstrap4Sites')->get('kalkproducts')->getData()); 
                fclose($handledone); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('userloginedit')->getData() == "1") {
                $filenamedone = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/user/login.phtml";
                $handledone = fopen($filenamedone, 'w'); 
                fputs($handledone, $event->getForm()->get('bootstrap4Sites')->get('userlogin')->getData()); 
                fclose($handledone); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('userregisteredit')->getData() == "1") {
                $filenameregister = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/user/register.phtml";
                $handleregister = fopen($filenameregister, 'w'); 
                fputs($handleregister, $event->getForm()->get('bootstrap4Sites')->get('userregister')->getData()); 
                fclose($handleregister); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('usermyoverviewedit')->getData() == "1") {
                $filenamemyoverview = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/user/myoverview.phtml";
                $handlemyoverview = fopen($filenamemyoverview, 'w'); 
                fputs($handlemyoverview, $event->getForm()->get('bootstrap4Sites')->get('usermyoverview')->getData()); 
                fclose($handlemyoverview); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('warehousedetailsedit')->getData() == "1") {
                $filenamedone = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/articletemplates/scripts/2.phtml";
                $handledone = fopen($filenamedone, 'w'); 
                fputs($handledone, $event->getForm()->get('bootstrap4Sites')->get('warehousedetails')->getData()); 
                fclose($handledone); 
            }
            if($event->getForm()->get('bootstrap4Sites')->get('kalkproductsdetailsedit')->getData() == "1") {
                $filenamedone = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/articletemplates/scripts/6.phtml";
                $handledone = fopen($filenamedone, 'w'); 
                fputs($handledone, $event->getForm()->get('bootstrap4Sites')->get('kalkproductsdetails')->getData()); 
                fclose($handledone); 
            }
            $filenamesaxoprintdetails = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/articletemplates/scripts/100.phtml";
            if (file_exists($filenamesaxoprintdetails)) {
                if($event->getForm()->get('bootstrap4Sites')->get('saxoprintdetailsedit')->getData() == "1") {
                    $filenamedone = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/articletemplates/scripts/100.phtml";
                    $handledone = fopen($filenamedone, 'w'); 
                    fputs($handledone, $event->getForm()->get('bootstrap4Sites')->get('saxoprintdetails')->getData()); 
                    fclose($handledone); 
                }
            }
            if($event->getForm()->get('bootstrap4Sites')->get('cmsindexedit')->getData() == "1") {
                $filenamecmsindex = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap4General"]["layout"] . "/templates/cms/index.phtml";
                $handlecmsindex = fopen($filenamecmsindex, 'w'); 
                fputs($handlecmsindex, $event->getForm()->get('bootstrap4Sites')->get('cmsindex')->getData()); 
                fclose($handlecmsindex); 
            }
        }
        $shopEntity->setDisplayArticleCount($event->getForm()->get('bootstrap4General')->get('displayArticleCount')->getData());
        $shopEntity->setBasketfield1($event->getForm()->get('bootstrap4General')->get('basketField1')->getData());
        $shopEntity->setBasketfield2($event->getForm()->get('bootstrap4General')->get('basketField2')->getData());
        $shopEntity->setBasketposfield1($event->getForm()->get('bootstrap4General')->get('basketPosField1')->getData());
        $shopEntity->setBasketposfield2($event->getForm()->get('bootstrap4General')->get('basketPosField2')->getData());
        $shopEntity->setDisplaySender($event->getForm()->get('bootstrap4General')->get('displaySender')->getData());
        $shopEntity->setDisplayDelivery($event->getForm()->get('bootstrap4General')->get('displayDelivery')->getData());
        $shopEntity->setCustomTemplates($event->getForm()->get('bootstrap4General')->get('customTemplates')->getData());
        $shopEntity->setLayout($event->getForm()->get('bootstrap4General')->get('layout')->getData());
        $shopEntity->setGoogleanalyticscode($event->getForm()->get('bootstrap4General')->get('googleanalyticscode')->getData());
        $shopEntity->setDefaultFunc($event->getForm()->get('bootstrap4General')->get('defaultFunc')->getData());
        $shopEntity->setDefaultParam($event->getForm()->get('bootstrap4General')->get('defaultParam')->getData());
        $shopEntity->setTemplateDisplayProductsCrossselling($event->getForm()->get('bootstrap4General')->get('templateDisplayProductsCrossselling')->getData());
        $shopEntity->setTemplateDisplayUserApproval($event->getForm()->get('bootstrap4General')->get('templateDisplayUserApproval')->getData());
    }

    public function formPreSetData(FormEvent $event)
    {

    }
}