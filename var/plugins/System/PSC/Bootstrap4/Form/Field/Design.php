<?php

namespace Plugin\System\PSC\Bootstrap4\Form\Field;

use DirectoryIterator;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Design implements Field
{

    private $options;

    public function getTemplate()
    {
        return '@PluginSystemPSCBootstrap4/form/field/design.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $builder->add('b2bshop', CheckboxType::class, ['required' => false, 'label' => 'B2B Shop'])
        ->add('einbetten', CheckboxType::class, ['required' => false, 'label' => 'Einbetten nicht anzeigen?'])
        ->add('display_no_language', CheckboxType::class, ['required' => false, 'label' => 'Sprache nicht benutzen?']);

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Bootstrap4\Form\Group\Design::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);

        if(isset($layoutSettings[$shopEntity->getLayout()])) {
            $event->getForm()->get('bootstrap4Design')->get('b2bshop')->setData(boolval($layoutSettings[$shopEntity->getLayout()]['b2bshop']));
            $event->getForm()->get('bootstrap4Design')->get('einbetten')->setData(boolval($layoutSettings[$shopEntity->getLayout()]['einbetten']));
            $event->getForm()->get('bootstrap4Design')->get('display_no_language')->setData(boolval($layoutSettings[$shopEntity->getLayout()]['display_no_language']));
        }
    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];

        $layoutSettings = json_decode($shopEntity->getLayoutSettings(), true);
        $layoutSettings[$shopEntity->getLayout()]['b2bshop'] = $event->getForm()->get('bootstrap4Design')->get('b2bshop')->getData();
        $layoutSettings[$shopEntity->getLayout()]['einbetten'] = $event->getForm()->get('bootstrap4Design')->get('einbetten')->getData();
        $layoutSettings[$shopEntity->getLayout()]['display_no_language'] = $event->getForm()->get('bootstrap4Design')->get('display_no_language')->getData();

        $shopEntity->setLayoutSettings(json_encode($layoutSettings));
    }

    public function formPreSetData(FormEvent $event)
    {

    }
}