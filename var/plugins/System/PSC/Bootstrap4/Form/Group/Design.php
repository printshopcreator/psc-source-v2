<?php

namespace Plugin\System\PSC\Bootstrap4\Form\Group;

use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\System\PluginBundle\Form\Group;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Design extends Group
{
    const GROUP_ID = 'bootstrap4Design';

    public function __construct()
    {
        $this->title = 'Design';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    public function getId()
    {
        return self::GROUP_ID;
    }
}