<?php

namespace Plugin\System\PSC\PaypalPlus\Paypal;

use PayPalHttp\HttpRequest;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;

class CreateOrderRequest extends HttpRequest
{

    function __construct()
    {
        parent::__construct("/v2/checkout/orders?", "POST");
        $this->headers["Content-Type"] = "application/json";
    }

    public function prefer($prefer)
    {
        $this->headers["Prefer"] = $prefer;
    }

    public function buildRequestBody(Contact $contact, ContactAddress $invoiceAddress, $brandName, $amount, $return_url, $cancel_url)
    {
        $this->body = array(
            'intent' => 'AUTHORIZE',
            'payer' => [
                'email_address' => $contact->getEmail(),
                'given_name' => $contact->getLastname(),
                'surname' => $contact->getFirstname(),
                'address' => [
                    'postal_code' => $invoiceAddress->getZip(),
                    'address_line_1' => $invoiceAddress->getStreet() . ' ' . $invoiceAddress->getHouseNumber(),
                    'admin_area_2' => $invoiceAddress->getCity(),
                    'country_code' => 'DE'
                ]
            ],
            'application_context' =>
                array(
                    'return_url' => $return_url,
                    'cancel_url' => $cancel_url,
                    'brand_name' => $brandName,
                    'locale' => 'de',
                    'landing_page' => 'BILLING',
                    'shipping_preference' => 'NO_SHIPPING',
                    'user_action' => 'PAY_NOW',
                ),
            'purchase_units' =>
                array(
                    0 =>
                        array(
                            'amount' =>
                                array(
                                    'currency_code' => 'EUR',
                                    'value' => $amount
                                )
                        )
                )
        );
    }

}
