<?php

namespace Plugin\System\PSC\PaypalPlus\Paypal;

use PayPalHttp\HttpRequest;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;

class CreateDoPayRequest extends HttpRequest
{

    function __construct($paymentId)
    {
        parent::__construct("/v1/payments/payment/" . $paymentId . '/execute', "POST");
        $this->headers["Content-Type"] = "application/json";
    }

    public function prefer($prefer)
    {
        $this->headers["Prefer"] = $prefer;
    }

    public function buildRequestBody($payerId)
    {
        $this->body = array(
            'payer_id' => $payerId
        );

    }

}
