<?php

namespace Plugin\System\PSC\PaypalPlus\Paypal;

use PayPalHttp\HttpRequest;

class CreateProfileRequest extends HttpRequest
{

    function __construct()
    {
        parent::__construct("/v1/payment-experience/web-profiles?", "POST");
        $this->headers["Content-Type"] = "application/json";
    }

    public function prefer($prefer)
    {
        $this->headers["Prefer"] = $prefer;
    }

    public function buildRequestBody($brandName, $logo)
    {
        $this->body = [
            'name' => rand(),
            'temporary' => 1,
            'presentation' => [
                'brand_name' => $brandName,
                'logo_image' => $logo,
                'locale_code' => 'DE'
            ],
            'input_fields' => [
                'no_shipping' => 1,
                'address_override' => 0
            ],
            'flow_config' => [
                'landing_page_type' => 'billing',
                'bank_txn_pending_url' => 'https://localhost:8001/test'
            ]
        ];
    }

}
