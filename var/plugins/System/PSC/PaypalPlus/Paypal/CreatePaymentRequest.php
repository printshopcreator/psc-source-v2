<?php

namespace Plugin\System\PSC\PaypalPlus\Paypal;

use PayPalHttp\HttpRequest;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;

class CreatePaymentRequest extends HttpRequest
{

    function __construct()
    {
        parent::__construct("/v1/payments/payment", "POST");
        $this->headers["Content-Type"] = "application/json";
    }

    public function prefer($prefer)
    {
        $this->headers["Prefer"] = $prefer;
    }

    public function buildRequestBody(Contact $contact, ContactAddress $invoiceAddress, $brandName, $amount, $return_url, $cancel_url)
    {
        /** @var \TP_Basket_Collection $basket */
        $basket = $_SESSION['Basket'];

        $this->body = array(
            'intent' => 'sale',
            'payer' => [
                "payment_method" => "paypal",
                "payer_info" => [
                    'payer_id' => $contact->getId(),
                    'email' => $contact->getEmail(),
                    'last_name' => $contact->getLastname(),
                    'first_name' => $contact->getFirstname(),
                    'billing_address' => [
                        'postal_code' => $invoiceAddress->getZip(),
                        'line1' => $invoiceAddress->getStreet() . ' ' . $invoiceAddress->getHouseNumber(),
                        'city' => $invoiceAddress->getCity(),
                        'country_code' => 'DE'
                    ]
                ]
            ],
            "transactions"=> [[
                "amount"=> [
                    "total"=> (string)round($basket['brutto'],2),
                    "currency"=> "EUR"
                ],
                "invoice_number"=> $basket['paymentRef'],
                "item_list"=> [
                        "items"=> [
                  ]
                ]
              ]],
          "note_to_payer"=> "Contact us for any questions on your order.",
          "redirect_urls"=> [
                "return_url"=> $return_url,
                "cancel_url"=> $cancel_url
          ]);

    }
}
