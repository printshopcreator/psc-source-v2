<?php
namespace Plugin\System\PSC\PaypalPlus\Payment;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use PayPalCheckoutSdk\Core\PayPalHttpClient;
use PayPalCheckoutSdk\Core\ProductionEnvironment;
use PayPalCheckoutSdk\Core\SandboxEnvironment;
use Plugin\System\PSC\PaypalPlus\Document\Paypal;
use Plugin\System\PSC\PaypalPlus\Paypal\CreateDoPayRequest;
use Plugin\System\PSC\PaypalPlus\Paypal\CreateOrderRequest;
use Plugin\System\PSC\PaypalPlus\Paypal\CreatePaymentRequest;
use Plugin\System\PSC\PaypalPlus\Paypal\CreateProfileRequest;
use PSC\Shop\EntityBundle\Document\Order;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Provider\PaymentProvider;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;
use PSC\Shop\QueueBundle\Event\Order\Payed;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use Sofort\SofortLib\Notification;
use Sofort\SofortLib\TransactionData;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Provider extends PaymentProvider
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    private \PSC\System\SettingsBundle\Service\Shop $shopService;
    /**
     * @var Manager
     */
    private Manager $eventManager;
    /**
     * @var SessionInterface
     */
    private SessionInterface $session;

    function __construct(SessionInterface $session, Manager $eventManager, FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, \PSC\System\SettingsBundle\Service\Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->shopService = $shopService;
        $this->eventManager = $eventManager;
        $this->session = $session;
    }

    public function getName()
    {
        return 'Paypal Plus';
    }

    public function getType()
    {
        return 'paypalplus';
    }

    public function saveDocument(Gatewaysettings $settings, Form $form)
    {
        $doc = new Paypal();

        $doc->setClientId($form->get('paypalplus')->get('clientId')->getData());
        $doc->setClientSecret($form->get('paypalplus')->get('clientSecret')->getData());
        $doc->setProduction($form->get('paypalplus')->get('production')->getData());
        $doc->setBrandName((string)$form->get('paypalplus')->get('brandName')->getData());

        $settings->setGatewayDocument($doc);

        return $settings;
    }

    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder)
    {

        if(!$settings->getGatewayDocument()) {
            $settings->setGatewayDocument(new Paypal());
        }

        $builder
            ->add('production', CheckboxType::class, array('label' => 'Production?', 'required' => false))
            ->add('clientId', TextType::class, array('label' => 'Client Id', 'required' => false))
            ->add('clientSecret', TextType::class, array('label' => 'Client Secret', 'required' => false))
            ->add('brandName', TextType::class, array('label' => 'Brand  Name', 'required' => false));

        $builder->get('production')->setData($settings->getGatewayDocument()->isProduction());
        $builder->get('clientId')->setData($settings->getGatewayDocument()->getClientId());
        $builder->get('clientSecret')->setData($settings->getGatewayDocument()->getClientSecret());
        $builder->get('brandName')->setData($settings->getGatewayDocument()->getBrandName());

        return $builder;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCPaypalPlus/settings.html.twig';
    }

    public function handlePayment(Request $request)
    {

        if($this->getGatewaySettings()->getGatewayDocument()->isProduction()) {
            $environment = new ProductionEnvironment(
                $this->getGatewaySettings()->getGatewayDocument()->getClientId(),
                $this->getGatewaySettings()->getGatewayDocument()->getClientSecret());
        }else{
            $environment = new SandboxEnvironment(
                $this->getGatewaySettings()->getGatewayDocument()->getClientId(),
                $this->getGatewaySettings()->getGatewayDocument()->getClientSecret());
        }

        $client = new PayPalHttpClient($environment);

        $contact = $request->get('contact');
        $invoice = $request->get('invoiceAddress');
        $delivery = $request->get('deliveryAddress');

        /** @var Contact $contact */
        $contact = $this->_entityManager->getRepository(Contact::class)->findOneBy(['uuid' => $contact]);
        /** @var ContactAddress $invoiceAddress */
        $invoiceAddress = $this->_entityManager->getRepository(ContactAddress::class)->findOneBy(['uuid' => $invoice]);

        $requestPaypal = new CreatePaymentRequest();
        $requestPaypal->buildRequestBody(
            $contact,
            $invoiceAddress,
            $this->getGatewaySettings()->getGatewayDocument()->getBrandName(),
            $request->get('amount'),
            $this->getHost().'/basket/finish?Data=finish&token='.$request->get('hash'),
            $this->getHost().'/basket/finish?error=Fehler'
        );

        try {

            $response = $client->execute($requestPaypal);

            $this->session->set('plugin_system_psc_paypalplus_paymentId', $response->result->id);
            $this->session->set('plugin_system_psc_paypalplus_production', $this->getGatewaySettings()->getGatewayDocument()->isProduction());
            foreach($response->result->links as $link) {
                if($link->rel == 'approval_url') {
                    $this->session->set('plugin_system_psc_paypalplus_link', $link->href);
                }
            }

            return new RedirectResponse('/apps/payment/paypalplus/pay/wall');
        }catch (\Exception $e) {
            var_dump($e->getMessage());
        }
        die();

    }

    public function handleNotify(Request $request)
    {
        $request = $request->getContent();
        $request = json_decode($request, true);

        if(isset($request['resource']) && isset($request['resource']['state']) && $request['resource']['state'] == 'completed')

            /** @var \PSC\Shop\EntityBundle\Document\Order $orderDoc */
            $orderDoc = $this->_doctrine_mongodb
                ->getRepository('PSC\Shop\EntityBundle\Document\Order')
                ->findOneBy(array('paymentRef' => (string)$request['resource']['parent_payment']));

        /** @var \PSC\Shop\EntityBundle\Entity\Order $order */
        $order = $this->_entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('uid' => $orderDoc->getUid()));

        $order->setStatus(145);
        $this->_entityManager->persist($order);
        $this->_entityManager->flush();

        $notify = new Payed();
        $notify->setShop($this->getShopEntity()->getUID());
        $notify->setOrder($order->getUuid());

        $this->eventManager->addJob($notify);

    }

    public function doPayment(Request $request)
    {
        if($this->getGatewaySettings()->getGatewayDocument()->isProduction()) {
            $environment = new ProductionEnvironment(
                $this->getGatewaySettings()->getGatewayDocument()->getClientId(),
                $this->getGatewaySettings()->getGatewayDocument()->getClientSecret());
        }else{
            $environment = new SandboxEnvironment(
                $this->getGatewaySettings()->getGatewayDocument()->getClientId(),
                $this->getGatewaySettings()->getGatewayDocument()->getClientSecret());
        }

        $client = new PayPalHttpClient($environment);

        $requestPaypal = new CreateDoPayRequest($this->session->get('plugin_system_psc_paypalplus_paymentId'));
        $requestPaypal->buildRequestBody($request->get('PayerID'));

        $response = $client->execute($requestPaypal);

        if($response->statusCode == 200) {
            return new RedirectResponse($this->getHost().'/basket/finish?token='.$request->get('hash').'&paymentRef='.$response->result->id);
        }

       return new RedirectResponse($this->getHost().'/basket/finish?error=DoPayment');
    }
}