<?php
namespace Plugin\System\PSC\PaypalPlus\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class ServiceController
 *
 * @Route("/pay")
 */
class PayController extends AbstractController
{
    /**
     * @Route("/wall", name="psc_plugin_system_paypalplus_wall")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @template()
     */
    public function wallAction(SessionInterface $session, Request $request, EntityManagerInterface $entityManager)
    {
        $paymentId = $session->get('plugin_system_psc_paypalplus_paymentId');

        $approvalUrl = $session->get('plugin_system_psc_paypalplus_link');
        $production = $session->get('plugin_system_psc_paypalplus_production');

        return new RedirectResponse($approvalUrl);

    }
}