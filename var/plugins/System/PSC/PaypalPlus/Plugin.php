<?php
namespace Plugin\System\PSC\PaypalPlus;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Paypalplus Payment';

    public function getType()
    {
        return Plugin::Payment;
    }

    public function getDescription()
    {
        return 'Stellt Unterstützung für Paypalplus bereit';
    }

    public function getVersion()
    {
        return 1;
    }
}