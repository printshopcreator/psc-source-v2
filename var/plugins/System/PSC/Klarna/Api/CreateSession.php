<?php

namespace Plugin\System\PSC\Klarna\Api;

use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\System\SettingsBundle\Service\Shop;

class CreateSession extends Base
{

    /** @var ContactAddress */
    protected $invoiceAddress = null;

    public function call()
    {
        return $this->postV3('/payments/v1/sessions', $this->buildData());
    }

    protected function buildData()
    {
        $basket = $_SESSION['Basket'];

        $data = [
            "purchase_country" => $this->invoiceAddress->getCountry(),
            "purchase_currency" => "EUR",
            "billing_address" => [
                "city" => $this->invoiceAddress->getCity(),
                "country" => $this->invoiceAddress->getCountry(),
                "email" => $this->invoiceAddress->getEmail(),
                "family_name" => $this->invoiceAddress->getLastname(),
                "given_name" => $this->invoiceAddress->getFirstname(),
                "postal_code" => $this->invoiceAddress->getZip(),
                "street_address" => $this->invoiceAddress->getStreet() . ' ' . $this->invoiceAddress->getHouseNumber(),
                "title" => $this->invoiceAddress->getAnrede()
            ],
            "locale" => "de-DE",
            "order_amount" => intval(round($basket['brutto']*100),2),
            "order_lines" => []
        ];

        /** @var \TP_Basket_Item $article */
        foreach($basket['Articles'] as $article) {
            $data['order_lines'][] = [
                'name' => $article->getArticle()['title'],
                'quantity' => $article->getCount(),
                'total_amount' => intval(round($article->getBrutto()*$article->getCount()*100,0)),
                'unit_price' => intval(round($article->getBrutto()*100,0))
            ];
        }

        if($basket['VersandkostenBrutto'] > 0) {
            $data['order_lines'][] = [
                'name' => "Versandkosten",
                'quantity' => 1,
                'total_amount' => intval(round($basket['VersandkostenBrutto'],2)*100),
                'unit_price' => intval(round($basket['VersandkostenBrutto'],2)*100)
            ];
        }

        if($basket['Zahlkosten'] > 0) {
            $data['order_lines'][] = [
                'name' => "Zahlkosten",
                'quantity' => 1,
                'total_amount' => intval(round($basket['Zahlkosten'],2)*100),
                'unit_price' => intval(round($basket['Zahlkosten'],2)*100)
            ];
        }

        return $data;
    }

    /**
     * @param ContactAddress $invoiceAddress
     */
    public function setInvoiceAddress(ContactAddress $invoiceAddress): void
    {
        $this->invoiceAddress = $invoiceAddress;
    }
}