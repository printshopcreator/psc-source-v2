<?php

namespace Plugin\System\PSC\Klarna\Api;

use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Base
{

    protected $baseUrlTest = 'https://api.playground.klarna.com';
    protected $baseUrlLive = 'https://api.klarna.com';

    protected \PSC\Shop\EntityBundle\Document\Shop $shop;

    /**
     * @var HttpClientInterface
     */
    protected HttpClientInterface $client;

    protected $isProduction = false;
    protected $username = "";
    protected $password = "";

    public function __construct(HttpClientInterface $client)
    {
        $this->client = $client;
    }

    public function postV3($url, $data)
    {

        $baseUrl = $this->baseUrlTest;
        if($this->isProduction) {
            $baseUrl = $this->baseUrlLive;
        }

        $response = $this->client->request('POST', $baseUrl . $url, [
            'auth_basic' => [$this->username, $this->password],
            'headers' =>
                $this->buildHeaders()
            ,
            'json' => $data,
        ]);

        $content = $response->toArray();

        return $content;
    }

    private function buildHeaders()
    {
        return [
            'Accept' => 'application/json',
        ];
    }

    /**
     * @param \PSC\Shop\EntityBundle\Document\Shop $shop
     */
    public function setShop(\PSC\Shop\EntityBundle\Document\Shop $shop): void
    {
        $this->shop = $shop;
    }

    protected function buildQuery(Array $data): string
    {
        return base64_encode(json_encode($data));
    }

    /**
     * @param bool $isProduction
     */
    public function setIsProduction(bool $isProduction): void
    {
        $this->isProduction = $isProduction;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

}