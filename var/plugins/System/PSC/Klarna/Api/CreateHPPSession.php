<?php

namespace Plugin\System\PSC\Klarna\Api;

use PSC\System\SettingsBundle\Service\Shop;

class CreateHPPSession extends Base
{
    protected $sessionId = "";

    protected $successUrl = "";
    protected $errorUrl = "";

    public function call()
    {
        return $this->postV3('/hpp/v1/sessions', $this->buildData());
    }

    protected function buildData()
    {
        $baseUrl = $this->baseUrlTest;
        if($this->isProduction) {
            $baseUrl = $this->baseUrlLive;
        }
        return [
            "payment_session_url" => $baseUrl . '/payments/v1/sessions/' . $this->sessionId,
            "merchant_urls" => [
                "back" => $this->errorUrl,
                "cancel" => $this->errorUrl,
                "error" => $this->errorUrl,
                "failure" => $this->errorUrl,
                "success" => $this->successUrl,
            ],
            "options" => [
                "place_order_mode" => "PLACE_ORDER"
            ]
        ];
    }

    /**
     * @param string $sessionId
     */
    public function setSessionId(string $sessionId): void
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @param string $successUrl
     */
    public function setSuccessUrl(string $successUrl): void
    {
        $this->successUrl = $successUrl;
    }

    /**
     * @param string $errorUrl
     */
    public function setErrorUrl(string $errorUrl): void
    {
        $this->errorUrl = $errorUrl;
    }

}