<?php
namespace Plugin\System\PSC\Klarna;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Klarna Payment';

    public function getType()
    {
        return Plugin::Payment;
    }

    public function getDescription()
    {
        return 'Stellt Unterstützung für Klarna bereit';
    }

    public function getVersion()
    {
        return 1;
    }
}