<?php
namespace Plugin\System\PSC\Klarna\Payment;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Plugin\System\PSC\Klarna\Api\CreateHPPSession;
use Plugin\System\PSC\Klarna\Api\CreateSession;
use Plugin\System\PSC\Klarna\Document\Klarna;
use PSC\Shop\EntityBundle\Document\Order;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Provider\PaymentProvider;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;
use PSC\Shop\QueueBundle\Event\Order\Payed;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use Sofort\SofortLib\Notification;
use Sofort\SofortLib\TransactionData;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Provider extends PaymentProvider
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    private \PSC\System\SettingsBundle\Service\Shop $shopService;
    /**
     * @var Manager
     */
    private Manager $eventManager;
    /**
     * @var CreateSession
     */
    private CreateSession $createSessionService;
    /**
     * @var CreateHPPSession
     */
    private CreateHPPSession $createHPPSessionService;

    function __construct(CreateHPPSession $createHPPSession, CreateSession $createSession, Manager $eventManager, FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, \PSC\System\SettingsBundle\Service\Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->shopService = $shopService;
        $this->eventManager = $eventManager;
        $this->createSessionService = $createSession;
        $this->createHPPSessionService = $createHPPSession;
    }

    public function getName()
    {
        return 'Klarna';
    }

    public function getType()
    {
        return 'klarna';
    }

    public function saveDocument(Gatewaysettings $settings, Form $form)
    {
        $doc = new Klarna();

        $doc->setUsername($form->get('klarna')->get('username')->getData());
        $doc->setPassword($form->get('klarna')->get('password')->getData());
        $doc->setProduction($form->get('klarna')->get('production')->getData());

        $settings->setGatewayDocument($doc);

        return $settings;
    }

    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder)
    {

        if(!$settings->getGatewayDocument()) {
            $settings->setGatewayDocument(new Klarna());
        }

        $builder
            ->add('production', CheckboxType::class, array('label' => 'Production?', 'required' => false))
            ->add('username', TextType::class, array('label' => 'Username', 'required' => false))
            ->add('password', TextType::class, array('label' => 'Password', 'required' => false));

        $builder->get('production')->setData($settings->getGatewayDocument()->isProduction());
        $builder->get('username')->setData($settings->getGatewayDocument()->getUsername());
        $builder->get('password')->setData($settings->getGatewayDocument()->getPassword());

        return $builder;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCKlarna/settings.html.twig';
    }

    public function handlePayment(Request $request)
    {
        $contact = $request->get('contact');
        $invoice = $request->get('invoiceAddress');
        $delivery = $request->get('deliveryAddress');

        /** @var Contact $contact */
        $contact = $this->_entityManager->getRepository(Contact::class)->findOneBy(['uuid' => $contact]);
        /** @var ContactAddress $invoiceAddress */
        $invoiceAddress = $this->_entityManager->getRepository(ContactAddress::class)->findOneBy(['uuid' => $invoice]);

        $this->createSessionService->setShop($this->shopDoc);
        $this->createSessionService->setIsProduction($this->getGatewaySettings()->getGatewayDocument()->isProduction());
        $this->createSessionService->setUsername($this->getGatewaySettings()->getGatewayDocument()->getUsername());
        $this->createSessionService->setPassword($this->getGatewaySettings()->getGatewayDocument()->getPassword());
        $this->createSessionService->setInvoiceAddress($invoiceAddress);

        $response = $this->createSessionService->call();

        $this->createHPPSessionService->setShop($this->shopDoc);
        $this->createHPPSessionService->setIsProduction($this->getGatewaySettings()->getGatewayDocument()->isProduction());
        $this->createHPPSessionService->setUsername($this->getGatewaySettings()->getGatewayDocument()->getUsername());
        $this->createHPPSessionService->setPassword($this->getGatewaySettings()->getGatewayDocument()->getPassword());
        $this->createHPPSessionService->setSessionId($response['session_id']);
        $this->createHPPSessionService->setSuccessUrl($this->getHost().'/basket/finish?Data=finish&token='.$request->get('hash'));
        $this->createHPPSessionService->setErrorUrl($this->getHost().'/basket/finish?error=Fehler');

        $response = $this->createHPPSessionService->call();

        $response = new RedirectResponse($response['redirect_url']);
        return $response;

    }

    public function handleNotify(Request $request)
    {


    }

    public function doPayment(Request $request)
    {


    }
}