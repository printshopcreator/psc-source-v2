<?php
namespace Plugin\System\PSC\HPLayouter\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Render
{
    /**
     * @var string $urlIdentifier
     *
     * @Field(type="string")
     */
    protected $urlIdentifier;

    /**
     * @return string
     */
    public function getUrlIdentifier()
    {
        return $this->urlIdentifier;
    }

    /**
     * @param string $urlIdentifier
     */
    public function setUrlIdentifier($urlIdentifier)
    {
        $this->urlIdentifier = $urlIdentifier;
    }

}