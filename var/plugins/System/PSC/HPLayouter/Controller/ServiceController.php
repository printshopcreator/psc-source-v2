<?php
namespace Plugin\System\PSC\HPLayouter\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ServiceController
 *
 * @Route("/service")
 */
class ServiceController extends AbstractController
{
    protected static $key = "p489r7nwpefsnfp0";


    /**
     * @Route("/user_info", name="psc_plugin_hplayouter_service_userinfo")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     */
    public function userInfoAction(Request $request, EntityManagerInterface $entityManager)
    {
        $settings = $this->getSettings();

        $user_id = 1;

        if(isset($settings['USERID'])) {
            /** @var Contact $contact */
            $contact = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')->findOneBy(['uid' => $settings['USERID']]);

            $user_info = array('id' => $contact->getId(), 'name' => $contact->getEmail());
        }else{
            $user_info = array('id' => 0, 'name' => "anonymous");
        }

        echo json_encode($user_info);

        exit;
    }

    /**
     * @Route("/projects", name="psc_plugin_hplayouter_service_projects")
     */
    public function projectsAction(Request $request, EntityManagerInterface $entityManager)
    {
        $h = fopen('services.txt', 'a');

        fwrite($h, "\n\n\n\r**************************************************************************************\n");
        fwrite($h, "\r****** Date time: ".(date('Y-m-d H:i:s'))." ******\n");
        fwrite($h, "\r****** METHOD GET ***** REQUEST /projects ***** FILE services.php ******\n");
        fwrite($h, "****** IP: ".$_SERVER['REMOTE_ADDR']." ******\n");
        fwrite($h, "****** HEADERS: ******\n\n");
        fwrite($h, "****** METHOD: ******".$request->getMethod()."\n\n");

        $settings = $this->getSettings();

        fwrite($h, "****** SETTINGS: ******".json_encode($settings)."\n\n");

        $data = [];

        if($request->isMethod('GET')) {
            $repo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata');
            /** @var QueryBuilder $qb */
            $qb = $repo->createQueryBuilder('l');

            $data = [];

            if(isset($settings['ARTID']) && isset($settings['USERID'])) {
                $qb->andWhere($qb->expr()->orX(
                    $qb->expr()->eq('l.uuid', '?1'),
                    $qb->expr()->eq('l.datatable', '?2')
                ));
                $qb->setParameter('1', $settings['ARTID']);
                $qb->setParameter('2', $settings['USERID']);

                $query = $qb->getQuery();
                $data = $query->execute();

            }elseif(isset($settings['USERID'])) {
                $qb->andWhere($qb->expr()->eq('l.datatable', '?1'));
                $qb->setParameter('1', $settings['USERID']);

                $query = $qb->getQuery();
                $data = $query->execute();
            }elseif(isset($settings['ARTID'])) {
                $qb->andWhere($qb->expr()->eq('l.uuid', '?1'));
                $qb->setParameter('1', $settings['ARTID']);

                $query = $qb->getQuery();
                $data = $query->execute();
            }

            $temp = [];

            foreach($data as $row) {
                $t = $row->getDesign();
                $t['id'] = $row->getUid();

                $temp[] = $t;
            }

            fwrite($h, "****** New: ******".json_encode($temp)."\n\n");
            echo json_encode($temp);


        }elseif($request->isMethod('POST')) {


            fwrite($h, "****** Name: ******".$request->get('name')."\n\n");


            $temp = [
                'name' => $request->get('name'),
                'articleId' => $settings['UUID'],
                'path' => $request->get('path'),
                'content_context_token' => $request->get('content_context_token'),
                'state' => 'EDITING'
            ];

            fwrite($h, "****** New: ******\n\n");
            $layoutDesignData = new Layoutdesigndata();

            $layoutDesignData->setArticleUuid($settings['UUID']);

            if(isset($settings['ARTID'])) {
                $layoutDesignData->setUuid($settings['ARTID']);
            }

            if(isset($settings['USERID'])) {
                $layoutDesignData->setDatatable($settings['USERID']);
            }

            $layoutDesignData->setTitle($request->get('name'));

            $layoutDesignData->setDesign($temp);

            $entityManager->persist($layoutDesignData);
            $entityManager->flush();

            $temp['id'] = $layoutDesignData->getUID();

            fwrite($h, "****** New: ******".json_encode($temp)."\n\n");


            echo json_encode($temp);
        }

        exit;
    }

    /**
     * @Route("/projects/{id}", name="psc_plugin_hplayouter_service_projects_id")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $id
     */
    public function projectSaveAction(Request $request, EntityManagerInterface $entityManager, SessionInterface $session, $id)
    {

        $h = fopen('services.txt', 'a');

        fwrite($h, "\n\n\n\r**************************************************************************************\n");
        fwrite($h, "\r****** Date time: ".(date('Y-m-d H:i:s'))." ******\n");
        fwrite($h, "\r****** METHOD GET ***** REQUEST /projectsave ***** FILE services.php ******\n");
        fwrite($h, "****** IP: ".$_SERVER['REMOTE_ADDR']." ******\n");
        fwrite($h, "****** HEADERS: ******\n\n");
        fwrite($h, "****** METHOD: ******".$request->getMethod()."\n\n");

        $settings = $this->getSettings();

        $data = [];

        if($request->isMethod('GET')) {

            /** @var Layoutdesigndata $layoutDesignData */
            $layoutDesignData = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uid' => $id));

            if($layoutDesignData) {
                echo json_encode($layoutDesignData->getDesign());
            }else{
                echo json_encode(array());
            }

        }elseif($request->isMethod('PUT')) {

            /** @var Layoutdesigndata $layoutDesignData */
            $layoutDesignData = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uid' => $id));

            $temp = [
                'id' => $id,
                'name' => $request->get('name'),
                'path' => $request->get('path'),
                'content_context_token' => $request->get('content_context_token')
            ];

            if(isset($settings['ARTID'])) {
                $layoutDesignData->setUuid($settings['ARTID']);
            }

            if(isset($settings['USERID'])) {
                $layoutDesignData->setDatatable($settings['USERID']);
            }

            $layoutDesignData->setDesign($temp);

            $entityManager->persist($layoutDesignData);
            $entityManager->flush();

            echo json_encode($temp);
        }

        exit;
    }

    /**
     * @Route("/projects/{id}/add_to_cart", name="psc_plugin_hplayouter_service_project_addcard")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $id
     */
    public function addCardAction(Request $request, EntityManagerInterface $entityManager, $id)
    {
        $h = fopen('services.txt', 'a');

        fwrite($h, "\n\n\n\r**************************************************************************************\n");
        fwrite($h, "\r****** Date time: ".(date('Y-m-d H:i:s'))." ******\n");
        fwrite($h, "\r****** METHOD GET ***** REQUEST /addcard ***** FILE services.php ******\n");
        fwrite($h, "****** IP: ".$_SERVER['REMOTE_ADDR']." ******\n");
        fwrite($h, "****** HEADERS: ******\n\n");
        fwrite($h, "****** METHOD: ******".$request->getMethod()."\n\n");

        /** @var Layoutdesigndata $layoutDesignData */
        $layoutDesignData = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uid' => $id));


        $json = file_get_contents('https://store.hpwallart.com/huelswitt/projects/'.$id);
        fwrite($h, "****** JSON: ******".$json."\n\n");
        $decode = json_decode($json, true);
        fwrite($h, "****** METHOD: ******".print_r($decode,true)."\n\n");

        $price = 0;
        if(isset($decode['price']['base']['price'])) {
            $price = $price + $decode['price']['base']['price'];
        }
        if(isset($decode['price']['fotolia']['price'])) {
            $price = $price + $decode['price']['fotolia']['price'];
        }
        if(isset($decode['price']['pattern_design']['price'])) {
            $price = $price + $decode['price']['pattern_design']['price'];
        }

        header('location: /service/steplayouter/savenewlayouter/format/json?modus=8&uuid='.$layoutDesignData->getArticleUuid().'&project_id='.$id.'&hplayouter_price='.$price);

        exit;
    }


    protected function getSettings() {
        $headers = $this->getAllHeadersJorge();

        return $this->decrypt($headers['X-Auth-Token']);
    }

    /**
     * Decryption Procedure
     */
    protected function decrypt( $enc ) {

        $str = base64_decode(str_replace("PLUS", "+", $enc));

        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $decrypttext = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, self::$key, $str, MCRYPT_MODE_ECB, $iv);
        parse_str($decrypttext, $output);
        return $output;
    }

    protected function getAllHeadersJorge()
    {
        $retarr = array();

        //JORGE
        foreach ($_SERVER AS $key => $value) {
            $key = preg_replace('/^HTTP_/i', '', $key);
            $key = str_replace('REDIRECT_', '', $key);
            $key = str_replace('Redirect-', '', $key);
            $key = str_replace(
                " ",
                "-",
                ucwords(strtolower(str_replace(array("-", "_"), " ", $key)))
            );
            $retarr[$key] = $value;
        }
        ksort($retarr);

        return $retarr;
    }
}