<?php
namespace Plugin\System\PSC\HPLayouter\Queue;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Render implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'hp_layouter_render';
    }

    public function getDescription()
    {
        return 'Triggert das Rendern auf dem HP Server';
    }

    public function getGroup()
    {
        return 'HP Layouter';
    }

    public function getName()
    {
        return 'HP Layouter Rendering';
    }

    public function getForm(FormBuilderInterface $builder, $form_options, EventInterface $event)
    {
        $builder->add("urlIdentifier", TextType::class, array('label' => 'Url Identifier', 'attr' => array('class' => 'form-element')));
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        $url = new \Plugin\System\PSC\HPLayouter\Document\Render();
        $url->setUrlIdentifier($form->get('urlIdentifier')->getData());
        $objQueue->setQueueDocument($url);
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
        $form->get('urlIdentifier')->setData($queueObj->getQueueDocument()->getUrlIdentifier());
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCHPLayouter/queue/render.html.twig';
    }

    /**
     * @param EventInterface $event
     * @param Queue $queue
     * @return bool
     * @internal param Queue $doc
     */
    public function execute(EventInterface $event, Queue $queue) {

        /** @var \Plugin\System\PSC\HPLayouter\Document\Render $render */
        $render = $queue->getQueueDocument();
        $eventDoc = $queue->getEventDocument();

        $orderRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');
        /** @var \PSC\Shop\EntityBundle\Entity\Order $order */
        $order = $orderRepo->findOneBy(array('uuid' => $event->getOrder()));

        /** @var Orderpos $position */
        foreach($order->getPositions() as $position) {

            if($position->getLayouterMode() == 8) {
                /** @var Position $objDoc */
                $objDoc = $this->_doctrine_mongodb
                    ->getRepository('PSCEntityBundle:Position')
                    ->findOneByUid((string)$position->getId());


                $body = json_encode(array(
                    "job_id" => $position->getUuid(),
                    "barcode_text" => "test",
                    "callback_url" => ""
                ));
                $url = "https://store.hpwallart.com/".$render->getUrlIdentifier()."/projects/".$objDoc->getTemplatePrintId()."/trigger_rendering";

                $client = new Client([
                    'headers' => [ 'Content-Type' => 'application/json' ]
                ]);

                try {
                    $response = $client->post($url,
                        ['body' => $body]
                    );
                }catch (\Exception $e) {
                    $this->_error = $e->getMessage();
                    return false;
                }

            }

        }


        return true;
    }

    public function getError()
    {
        return $this->_error;
    }
}