<?php

namespace Plugin\System\PSC\HPLayouter\Form\Group;

use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\System\PluginBundle\Form\Group;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class HPLayouter extends Group
{
    const GROUP_ID = 'hplayouter';

    public function __construct()
    {
        $this->title = 'HP Layouter';
    }

    public function getModule()
    {
        return Field::Product;
    }

    public function getId()
    {
        return self::GROUP_ID;
    }
}