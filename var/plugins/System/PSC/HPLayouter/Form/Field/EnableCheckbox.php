<?php

namespace Plugin\System\PSC\HPLayouter\Form\Field;

use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EnableCheckbox implements Field
{

    public function getTemplate()
    {
        return '@PluginSystemPSCHPLayouter/form/field/enable_checkbox.html.twig';
    }

    public function getModule()
    {
        return Field::Product;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('hplayouter', CheckboxType::class, array(
            'label' => 'HP Layouter aktivieren',
            'required' => false
        ));

        return $builder;
    }

    public function getGroup()
    {
        return HPLayouter::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        // TODO: Implement formPostSetData() method.
    }

    public function formPostSubmit(FormEvent $event)
    {
        // TODO: Implement formPostSubmit() method.
    }

    public function formPreSetData(FormEvent $event)
    {
        // TODO: Implement formPreSetData() method.
    }
}