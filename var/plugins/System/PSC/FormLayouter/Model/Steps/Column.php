<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Steps;

use Plugin\System\PSC\FormLayouter\Model\Steps\Options\Base;

class Column {

    /** @var Base[] */
    private $options;

    /**
     * @return Base[]
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param Base[] $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    public function addOption($option)
    {
        $this->options[] = $option;
    }

}