<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Steps;

use Plugin\System\PSC\FormLayouter\Model\Steps\Options\Base;

class Step {

    private $id;

    private $name;

    private $headline;

    private $description;

    private $first = false;

    private $last = false;

    /** @var Column[] */
    private $columns = [];

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getHeadline()
    {
        return $this->headline;
    }

    /**
     * @param mixed $headline
     */
    public function setHeadline($headline)
    {
        $this->headline = $headline;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Column[]
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param Column[] $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }

    public function getColumn($pos)
    {
        if(!isset($this->columns[$pos])) {
            $this->columns[$pos] = new Column();
        }

        return $this->columns[$pos];
    }

    public function getOption($id)
    {
        foreach($this->columns as $columns) {
            foreach($columns->getOptions() as $option) {
                if($option->getId() == $id) return $option;
            }
        }

    }

    public function getColumnWidth()
    {
        return 12/count($this->columns);
    }

    /**
     * @return bool
     */
    public function isLast()
    {
        return $this->last;
    }

    /**
     * @param bool $last
     */
    public function setLast($last)
    {
        $this->last = $last;
    }

    /**
     * @return bool
     */
    public function isFirst()
    {
        return $this->first;
    }

    /**
     * @param bool $first
     */
    public function setFirst($first)
    {
        $this->first = $first;
    }

}