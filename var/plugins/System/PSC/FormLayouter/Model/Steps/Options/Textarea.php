<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Steps\Options;

use PSC\Shop\MediaBundle\Service\MediaManager;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilder;

class Textarea extends Base {

    private $text = "";

    protected $inForm = true;

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    public function renderForm(FormBuilder $builder, MediaManager $mediaManager)
    {

        $builder->add($this->getId(), TextareaType::class, [
            'label' => $this->getLabel(),
            'data' => $this->getText(),
            'attr' => [
                'rows' => 6
            ],
            'required' => $this->isRequired()
        ]);
    }

}