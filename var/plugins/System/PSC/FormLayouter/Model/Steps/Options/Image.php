<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Steps\Options;

use PSC\Shop\MediaBundle\Service\MediaManager;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilder;

class Image extends Base {

    private $cropBox = false;

    private $cropBoxAspectRatio = 0;

    protected $inForm = true;

    public function renderForm(FormBuilder $builder, MediaManager $mediaManager)
    {
        $builder->add($this->getId(), FileType::class, [
            'label' => $this->getLabel(),
            'required' => false
        ]);
        $builder->add($this->getId() . '_original', HiddenType::class, [
        ]);
        if($this->cropBox) {
            $builder->add($this->getId() . '_crop', HiddenType::class, [
            ]);
        }
    }

    /**
     * @return bool
     */
    public function isCropBox()
    {
        return $this->cropBox;
    }

    /**
     * @param bool $cropBox
     */
    public function setCropBox($cropBox)
    {
        $this->cropBox = $cropBox;
    }

    /**
     * @return int
     */
    public function getCropBoxAspectRatio()
    {
        return $this->cropBoxAspectRatio;
    }

    /**
     * @param int $cropBoxAspectRatio
     */
    public function setCropBoxAspectRatio($cropBoxAspectRatio)
    {
        $this->cropBoxAspectRatio = $cropBoxAspectRatio;
    }

}