<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Steps\Options\Select;

use Plugin\System\PSC\FormLayouter\Model\Steps\Options\Base;
use PSC\Shop\MediaBundle\Service\MediaManager;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilder;

class Media extends Base {

    protected $inForm = true;

    protected $options = [];

    protected $choiceAttr = [];

    /** @var MediaManager */
    protected $mediaManager;

    public function renderForm(FormBuilder $builder, MediaManager $mediaManager)
    {

        $this->mediaManager = $mediaManager;

        $this->buildChoiceAttr();

        $builder->add($this->getId(), ChoiceType::class, [
            'label' => $this->getLabel(),
            'required' => false,
            'choices' => $this->options,
            'choice_attr' => $this->choiceAttr
        ]);
    }

    public function addOption($id, $label)
    {
        $this->options[$label] = (string)$id;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    private function buildChoiceAttr()
    {
        foreach($this->options as $name => $key) {
           $media = $this->mediaManager->getMedia($key);
           $this->choiceAttr[$name] = ['data-img-src' => $this->mediaManager->retrieveThumbnailPath($media)];
        }
    }
}