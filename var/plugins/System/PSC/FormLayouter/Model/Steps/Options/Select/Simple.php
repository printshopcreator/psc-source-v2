<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Steps\Options\Select;

use Plugin\System\PSC\FormLayouter\Model\Steps\Options\Base;
use PSC\Shop\MediaBundle\Service\MediaManager;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilder;

class Simple extends Base {

    protected $inForm = true;

    protected $options = [];

    public function renderForm(FormBuilder $builder, MediaManager $mediaManager)
    {
        $builder->add($this->getId(), ChoiceType::class, [
            'label' => $this->getLabel(),
            'required' => false,
            'choices' => $this->options
        ]);
    }

    public function addOption($id, $label)
    {
        $this->options[$label] = $id;
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param array $options
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }
}