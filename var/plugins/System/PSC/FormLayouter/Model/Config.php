<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model;


use Plugin\System\PSC\FormLayouter\Model\Config\Pdf;

class Config {

    /** @var Pdf */
    private $pdf;

    private $pdfPreviewButton = false;

    private $pdfPrintButton = false;

    /**
     * @return Pdf
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * @param Pdf $pdf
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;
    }

    /**
     * @return bool
     */
    public function isPdfPreviewButton()
    {
        return $this->pdfPreviewButton;
    }

    /**
     * @param bool $pdfPreviewButton
     */
    public function setPdfPreviewButton($pdfPreviewButton)
    {
        $this->pdfPreviewButton = $pdfPreviewButton;
    }

    /**
     * @return bool
     */
    public function isPdfPrintButton()
    {
        return $this->pdfPrintButton;
    }

    /**
     * @param bool $pdfPrintButton
     */
    public function setPdfPrintButton($pdfPrintButton)
    {
        $this->pdfPrintButton = $pdfPrintButton;
    }


}