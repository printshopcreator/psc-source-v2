<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Pdf;


use Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\Base;

class Site {

    private $id;

    private $label = "";

    private $width = 0;

    private $height = 0;

    private $trimLeft = 0;
    private $trimRight = 0;
    private $trimTop = 0;
    private $trimBottom = 0;

    /** @var Base[] */
    private $elements = [];

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getTrimLeft()
    {
        return $this->trimLeft;
    }

    /**
     * @param int $trimLeft
     */
    public function setTrimLeft($trimLeft)
    {
        $this->trimLeft = $trimLeft;
    }

    /**
     * @return int
     */
    public function getTrimRight()
    {
        return $this->trimRight;
    }

    /**
     * @param int $trimRight
     */
    public function setTrimRight($trimRight)
    {
        $this->trimRight = $trimRight;
    }

    /**
     * @return int
     */
    public function getTrimTop()
    {
        return $this->trimTop;
    }

    /**
     * @param int $trimTop
     */
    public function setTrimTop($trimTop)
    {
        $this->trimTop = $trimTop;
    }

    /**
     * @return int
     */
    public function getTrimBottom()
    {
        return $this->trimBottom;
    }

    /**
     * @param int $trimBottom
     */
    public function setTrimBottom($trimBottom)
    {
        $this->trimBottom = $trimBottom;
    }

    /**
     * @return Base[]
     */
    public function getElements()
    {
        return $this->elements;
    }

    /**
     * @param Base[] $elements
     */
    public function setElements($elements)
    {
        $this->elements = $elements;
    }


}