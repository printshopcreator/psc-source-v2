<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Pdf\Elements;

use MongoDB\BSON\ObjectId;
use Monolog\Handler\Mongo;

class Media extends Base
{

    private $file = "";

    private $width = 0;

    private $height = 0;

    /**
     * @return string
     */
    public function getFile()
    {
        if ($this->hasOption($this->getId())) {
            return $this->getOption($this->getId());
        } else {
            return $this->file;
        }
    }

    public function renderPdf($pdf)
    {

        if( false && $this->getFile() != "") {
            $id = new ObjectId($this->getFile());

            $render = true;
            /** @var \PSC\Shop\MediaBundle\Document\Media $imageObj */
            $imageObj = $this->getMongoDb()
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['_id' => new ObjectId($this->getFile())]);
            # Load the image
            $image = $pdf->load_image("auto", "/data/www/new/web/" . $imageObj->getUrl(), "");
            if ($image == 0) {
                echo("Error: " . $pdf->get_errmsg());
                exit(1);
            }
            $opt = "position={top left} boxsize={" . $this->getWidth()*$this->getScale() . " " . $this->getHeight()*$this->getScale() .
                "} fitmethod=meet";


            if($render) {
                $pdf->fit_image($image, $this->getX() * $this->getScale(),
                    ($this->getHeight() + $this->getY()) * $this->getScale(), $opt);
            }
        }
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @param string $file
     * @return Image
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }


}