<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Pdf\Elements;

use MongoDB\BSON\ObjectId;
use Monolog\Handler\Mongo;

class Image extends Base
{

    private $file = "";

    private $width = 0;

    private $height = 0;


    /**
     * @return string
     */
    public function getFile()
    {
        if($this->hasOption($this->getId().'_original')) {
            return $this->getOption($this->getId().'_original');
        }
        return $this->getId();
    }


    public function renderPdf($pdf)
    {
        if($this->getFile() != "") {
            $id = new ObjectId($this->getFile());

            $render = true;
            /** @var \Plugin\System\PSC\FormLayouter\Document\Image $imageObj */
            $imageObj = $this->getMongoDb()
                ->getRepository('Plugin\System\PSC\FormLayouter\Document\Image')
                ->findOneBy(['_id' => new ObjectId($this->getFile())]);
            # Load the image
            $image = $pdf->load_image("auto", "/data/www/new/web/market/motive/" . $imageObj->getFileName(), "");
            if ($image == 0) {
                echo("Error: " . $pdf->get_errmsg());
                exit(1);
            }
            $opt = "position={top left} boxsize={" . $this->getWidth()*$this->getScale() . " " . $this->getHeight()*$this->getScale() .
                "} fitmethod=meet";


//            $opt = $opt . " matchbox={clipping={1% 1% 50% 50%}}";
            if($imageObj->isCrop()) {
                $opt = $opt . " matchbox={name=" . $this->getId() . " clipping={" .
                    ($imageObj->getCropX()) . "% " .  (100-$imageObj->getCropHeight()-$imageObj->getCropY()) . "% " . ($imageObj->getCropWidth()+$imageObj->getCropX()) . "% " . (100-$imageObj->getCropY()) . "%}}";

                if($imageObj->getCropWidth() == 0 || $imageObj->getCropHeight() == 0) {
                    $render = false;
                }
            }else{
                $opt = $opt . " matchbox={name=" . $this->getId() . "}";
            }
            if($imageObj->getRotate() != 0) {
                //$opt .= " rotate=". $imageObj->getRotate() . "";
            }

            if($render) {
                $pdf->fit_image($image, $this->getX() * $this->getScale(),
                    ($this->getHeight() + $this->getY()) * $this->getScale(), $opt);
            }

            if($this->getBorder() != null) {
                $path = (int)$pdf->info_matchbox($this->getId(), 1, "boundingbox");
                $pdf->draw_path($path, 0, 0, $this->getBorder());
                $pdf->delete_path($path);
            }

            $pdf->close_image($image);
        }
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @param string $file
     * @return Image
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }


}