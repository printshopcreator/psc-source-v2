<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Pdf\Elements;


use Doctrine\ODM\MongoDB\DocumentManager;
use Plugin\System\PSC\FormLayouter\Model\Pdf\Site;

abstract class Base {

    private $id = "";

    private $x = 0;

    private $y = 0;

    private $preview = true;

    private $print = true;

    /** @var string?  */
    private $border = null;

    /** @var DocumentManager */
    private $mongoDb;

    /** @var float  */
    private $scale = 1;

    private $options = [];

    /** @var Site */
    private $site;
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * @param int $x
     */
    public function setX($x)
    {
        $this->x = $x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * @param int $y
     */
    public function setY($y)
    {
        $this->y = $y;
    }

    /**
     * @param \PDFlib $pdf
     */
    abstract function renderPdf($pdf);

    /**
     * @return bool
     */
    public function isPrint()
    {
        return $this->print;
    }

    /**
     * @param bool $print
     */
    public function setPrint($print)
    {
        $this->print = $print;
    }

    /**
     * @return bool
     */
    public function isPreview()
    {
        return $this->preview;
    }

    /**
     * @param bool $preview
     */
    public function setPreview($preview)
    {
        $this->preview = $preview;
    }

    /**
     * @return float
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * @param float $scale
     */
    public function setScale($scale)
    {
        $this->scale = $scale;
    }

    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * @return mixed
     */
    public function getOption($key)
    {
        return $this->options[$key];
    }

    /**
     * @param $key
     * @return boolean
     */
    public function hasOption($key)
    {
        return isset($this->options[$key]);
    }

    /**
     * @return DocumentManager
     */
    public function getMongoDb()
    {
        return $this->mongoDb;
    }

    /**
     * @param DocumentManager $mongoDb
     */
    public function setMongoDb($mongoDb)
    {
        $this->mongoDb = $mongoDb;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param Site $site
     */
    public function setSite($site)
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getBorder()
    {
        return $this->border;
    }

    /**
     * @param string $border
     */
    public function setBorder($border)
    {
        $this->border = $border;
    }

}