<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Pdf\Elements;


use MongoDB\BSON\ObjectId;

class Pdf extends Base
{

    private $file = "";

    private $siteToRender = 1;

    private $width = 0;

    private $height = 0;

    public function renderPdf($pdf)
    {
        /** @var \PSC\Shop\MediaBundle\Document\Media $media */
        $media = $this->getMongoDb()
            ->getRepository('PSC\Shop\MediaBundle\Document\Media')
            ->findOneBy(['_id' => new ObjectId($this->getFile())]);

        $indoc = $pdf->open_pdi_document("/data/www/new/web" . $media->getUrl(), "");

        $page = $pdf->open_pdi_page($indoc, $this->getSiteToRender(), "");
        if ($page == 0) {
            print("Error: " . $pdf->get_errmsg());
            die();
        }

        $optlist = "boxsize {" . $this->getWidth()*$this->getScale() . " " . $this->getHeight()*$this->getScale() . "} fitmethod meet";

        $pdf->fit_pdi_page($page, $this->getX()*$this->getScale(),
            ($this->getSite()->getHeight()-$this->getY())*$this->getScale(), $optlist);

        $pdf->close_pdi_page($page);

        $pdf->close_pdi_document($indoc);

    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return int
     */
    public function getSiteToRender()
    {
        return $this->siteToRender;
    }

    /**
     * @param int $siteToRender
     */
    public function setSiteToRender($siteToRender)
    {
        $this->siteToRender = $siteToRender;
    }

}