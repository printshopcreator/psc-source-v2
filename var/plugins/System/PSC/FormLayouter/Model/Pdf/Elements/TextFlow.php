<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Pdf\Elements;

use MongoDB\BSON\ObjectId;

class TextFlow extends Text
{



    public function renderPdf($pdf)
    {
        if($this->getFont()) {

            /** @var \PSC\Shop\MediaBundle\Document\Media $media */
            $media = $this->getMongoDb()
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['_id' => new ObjectId($this->getFont())]);

            $pdf->set_option("FontOutline={" . $this->getFont() . "=" . "/data/www/new/web" . $media->getUrl()."}");
        }else{
            $this->setFont("Helvetica");
        }

        $optList = "";
        if (strtolower($this->getColorMode()) == "cmyk") {
            $color = explode(" ", $this->getColor());
            $optList .= "fillcolor={cmyk " . $color[0]/100 . " " .  $color[1]/100 . " " .  $color[2]/100 . " " .  $color[3]/100 . "} ";
        }elseif(strtolower($this->getColorMode()) == "rgb") {
            $optList .= "fillcolor={rgb " . $this->getColor() . "} ";
        }else {
            $optList .= "fillcolor=" . $this->getColor() . " ";
        }

        if($this->getRotate() != 0) {
            $optList .= "rotate=". $this->getRotate() . " ";
        }

        $optList .= "leading=". $this->getLeading() . " ";

        $optList .= " alignment=".$this->getHAlign()." encoding=unicode embedding fontname=" . $this->getFont() . " fontsize=" . ($this->getFontSize() * $this->getScale());

        $tf = 0;
        $tf = $pdf->add_textflow($tf, $this->getText(), $optList);


        $optList = " verticalalign=".$this->getVAlign()." fitmethod=" . $this->getFitMethod();
        if($this->isShowBorder()) {
            $optList .= " showborder";
        }

        $pdf->fit_textflow($tf, $this->getX()*$this->getScale(), ($this->getY())*$this->getScale(), ($this->getWidth()+$this->getX())*$this->getScale(), ($this->getY()+$this->getHeight())*$this->getScale(), $optList);
    }

}