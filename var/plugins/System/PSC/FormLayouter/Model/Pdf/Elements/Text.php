<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Pdf\Elements;


use MongoDB\BSON\ObjectId;

class Text extends Base
{

    private $text = "";

    private $fontSize = 14;

    private $color = "black";

    private $colorMode = "hex";

    private $rotate = 0;

    private $font = "";

    private $showBorder = false;

    private $width = 0;

    private $height = 0;

    private $fitMethod = "auto";

    private $vAlign = "top";

    private $hAlign = "left";

    private $leading = "100%";
    /**
     * @return string
     */
    public function getText()
    {
        if ($this->hasOption($this->getId())) {
            return $this->getOption($this->getId());
        } else {
            return $this->text;
        }
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    public function renderPdf($pdf)
    {
        if($this->getFont()) {

            /** @var \PSC\Shop\MediaBundle\Document\Media $media */
            $media = $this->getMongoDb()
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['_id' => new ObjectId($this->getFont())]);

            $pdf->set_option("FontOutline={" . $this->getFont() . "=" . "/data/www/new/web" . $media->getUrl()."}");
            $font = $pdf->load_font($this->getFont(), "unicode", "embedding");
        }else{
            $font = $pdf->load_font("Arial", "unicode", "embedding");
            if ($font == 0) {
                echo("Error: " .  $pdf->get_errmsg());
                exit(1);
            }

        }

        $pdf->setfont($font, $this->getFontSize() * $this->getScale());

        $optList = "";
        if (strtolower($this->getColorMode()) == "cmyk") {
            $color = explode(" ", $this->getColor());
            $optList .= "fillcolor={cmyk " . $color[0]/100 . " " .  $color[1]/100 . " " .  $color[2]/100 . " " .  $color[3]/100 . "} ";
        }elseif(strtolower($this->getColorMode()) == "rgb") {
            $optList .= "fillcolor={rgb " . $this->getColor() . "} ";
        }else {
            $optList .= "fillcolor=" . $this->getColor() . " ";
        }

        if($this->rotate != 0) {
            $optList .= "rotate=". $this->rotate . " ";
        }

        if($this->getWidth() != 0 && $this->getHeight() != 0) {
            $optList .= " boxsize={".($this->getWidth()*$this->getScale())." ".($this->getHeight()*$this->getScale())."} position={".$this->getHAlign()." ".$this->getVAlign()."} fitmethod=" . $this->getFitMethod();

            if($this->isShowBorder()) {
                $optList .= " showborder";
            }
        }

        $pdf->fit_textline($this->getText(), $this->getX()*$this->getScale(), ($this->getY()+$this->getHeight())*$this->getScale(), $optList);
    }

    /**
     * @return int
     */
    public function getFontSize()
    {
        return $this->fontSize;
    }

    /**
     * @param int $fontSize
     */
    public function setFontSize($fontSize)
    {
        $this->fontSize = $fontSize;
    }

    /**
     * @return string
     */
    public function getColorMode()
    {
        return $this->colorMode;
    }

    /**
     * @param string $colorMode
     */
    public function setColorMode($colorMode)
    {
        $this->colorMode = $colorMode;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getRotate()
    {
        return $this->rotate;
    }

    /**
     * @param int $rotate
     */
    public function setRotate($rotate)
    {
        $this->rotate = $rotate;
    }

    /**
     * @return string
     */
    public function getFont()
    {
        return $this->font;
    }

    /**
     * @param string $font
     */
    public function setFont($font)
    {
        $this->font = $font;
    }

    /**
     * @return bool
     */
    public function isShowBorder()
    {
        return $this->showBorder;
    }

    /**
     * @param bool $showBorder
     */
    public function setShowBorder($showBorder)
    {
        $this->showBorder = $showBorder;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getFitMethod()
    {
        return $this->fitMethod;
    }

    /**
     * @param string $fitMethod
     */
    public function setFitMethod($fitMethod)
    {
        $this->fitMethod = $fitMethod;
    }

    /**
     * @return string
     */
    public function getHAlign()
    {
        return $this->hAlign;
    }

    /**
     * @param string $hAlign
     */
    public function setHAlign($hAlign)
    {
        $this->hAlign = $hAlign;
    }

    /**
     * @return string
     */
    public function getVAlign()
    {
        return $this->vAlign;
    }

    /**
     * @param string $vAlign
     */
    public function setVAlign($vAlign)
    {
        $this->vAlign = $vAlign;
    }

    /**
     * @return string
     */
    public function getLeading()
    {
        return $this->leading;
    }

    /**
     * @param string $leading
     */
    public function setLeading($leading)
    {
        $this->leading = $leading;
    }

}