<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Model\Config;


class Pdf {

    private $unit = "pt";

    private $pdfx4 = false;

    private $iccProfile = "";

    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getIccProfile()
    {
        return $this->iccProfile;
    }

    /**
     * @param string $iccProfile
     */
    public function setIccProfile($iccProfile)
    {
        $this->iccProfile = $iccProfile;
    }

    /**
     * @return bool
     */
    public function isPdfx4()
    {
        return $this->pdfx4;
    }

    /**
     * @param bool $pdfx4
     */
    public function setPdfx4($pdfx4)
    {
        $this->pdfx4 = $pdfx4;
    }

}