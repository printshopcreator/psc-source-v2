<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Service;

use Plugin\System\PSC\FormLayouter\Model\Config;
use Plugin\System\PSC\FormLayouter\Model\Pdf\Site;
use Plugin\System\PSC\FormLayouter\Model\Steps\Step;
use Plugin\System\PSC\FormLayouter\Parser\Parser;

class Layouter {

    /** @var Step[] */
    private $steps = [];

    /** @var Site[] */
    private $sites = [];

    /** @var Config */
    private $config;

    public function setXml($xml)
    {

        $parser = new Parser();
        $parser->loadXml($xml);

        $this->steps = $parser->parseStep();
        $this->sites = $parser->parseSites();
        $this->config = $parser->parseConfig();

    }

    public function getStep($stepNum = 0)
    {
        foreach($this->steps as $step) {
            if($stepNum == $step->getId()) return $step;
        }
    }

    public function getSite($site = 1)
    {
        return $this->sites[$site-1];
    }

    /**
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @return Site[]
     */
    public function getSites()
    {
        return $this->sites;
    }
}