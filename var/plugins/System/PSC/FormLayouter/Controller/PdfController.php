<?php
namespace Plugin\System\PSC\FormLayouter\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use DOMXPath;
use Plugin\Custom\CHILIPublish\Designer\Api\Main;
use Plugin\Custom\CHILIPublish\Designer\Api\GenerateApiKey;
use Plugin\Custom\CHILIPublish\Designer\Api\SetWorkspaceAdministration;
use Plugin\Custom\CHILIPublish\Designer\Api\ResourceItemCopy;
use Plugin\Custom\CHILIPublish\Designer\Api\DocumentGetHTMLEditorURL;
use Plugin\System\PSC\FormLayouter\Form\FormDesigner;
use Plugin\System\PSC\FormLayouter\Renderer\Pdf;
use Plugin\System\PSC\FormLayouter\Service\Layouter;
use Plugin\System\PSC\FormLayouter\Session\FormLayouter;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ServiceController
 *
 * @Route("/pdf")
 */
class PdfController extends AbstractController
{

    /**
     * @Route("/pdf/preview/{productId}", name="psc_plugin_formlayouter_pdf_preview")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param Layouter $formLayouter
     * @param Pdf $renderer
     * @param $productId
     * @return Response
     */
    public function previewAction(Request $request, EntityManagerInterface $entityManager, DocumentManager $documentManager, Layouter $formLayouter, Pdf $renderer, $productId)
    {

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
            ->findOneBy(array('uuid' => $productId));

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string)$product->getUid()));

        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $formLayouter->setXml($productDoc->getPluginSetting('formlayouter', 'xml'));

        $config = $formLayouter->getConfig();

        $outfilename = "temp/".uniqid().".pdf";

        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $renderer->setConfig($config);
        $renderer->setOptions($session->getOptions($productId));
        $renderer->renderPreviewPdf($formLayouter->getSites(), $outfilename);

        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($outfilename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($outfilename) . '"');
        $response->headers->set('Content-length', filesize($outfilename));

        $response->sendHeaders();

        $response->setContent(readfile($outfilename));

        unlink($outfilename);

        return $response;

    }

    /**
     * @Route("/pdf/print/{productId}", name="psc_plugin_formlayouter_pdf_print")
     */
    public function printAction(Request $request, EntityManagerInterface $entityManager, DocumentManager $documentManager, Layouter $formLayouter, Pdf $renderer, $productId)
    {

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
            ->findOneBy(array('uuid' => $productId));

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string)$product->getUid()));

        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $formLayouter->setXml($productDoc->getPluginSetting('formlayouter', 'xml'));

        $config = $formLayouter->getConfig();

        $outfilename = "temp/".uniqid().".pdf";

        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $renderer->setConfig($config);
        $renderer->setOptions($session->getOptions($productId));
        $renderer->renderPrintPdf($formLayouter->getSites(), $outfilename);

        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($outfilename));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($outfilename) . '"');

        $response->sendHeaders();

        $response->setContent(readfile($outfilename));

        unlink($outfilename);

        return $response;

    }
}