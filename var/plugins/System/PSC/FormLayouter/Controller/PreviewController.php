<?php
namespace Plugin\System\PSC\FormLayouter\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use DOMXPath;
use Plugin\System\PSC\FormLayouter\Form\FormDesigner;
use Plugin\System\PSC\FormLayouter\Renderer\Pdf;
use Plugin\System\PSC\FormLayouter\Service\Layouter;
use Plugin\System\PSC\FormLayouter\Session\FormLayouter;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ServiceController
 *
 * @Route("/preview")
 */
class PreviewController extends AbstractController
{

    /**
     * @Route("/image/{productId}/{site}/{width}/{height}", name="psc_plugin_formlayouter_preview_image")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param Layouter $formLayouter
     * @param Pdf $renderer
     * @param $productId
     * @param int $site
     * @param int $width
     * @param int $height
     * @return Response
     * @throws \ImagickException
     */
    public function imageAction(Request $request, EntityManagerInterface $entityManager, DocumentManager $documentManager, Layouter $formLayouter, Pdf $renderer, $productId, $site = 1, $width = 400, $height = 900)
    {

        $width = round($width);
        $height = round($height);

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
            ->findOneBy(array('uuid' => $productId));

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string)$product->getUid()));

        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $formLayouter->setXml($productDoc->getPluginSetting('formlayouter', 'xml'));

        $actSite = $formLayouter->getSite($site);
        $config = $formLayouter->getConfig();

        $outfilename = "temp/".uniqid().".pdf";

        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $renderer->setSite($actSite);
        $renderer->setConfig($config);
        $renderer->setOptions($session->getOptions($productId));
        $renderer->renderStep($outfilename);

        $im = new \imagick();

        $im->setCompressionQuality(89);

        $im->setResolution(300,300);
        $im->readImage($outfilename."[0]");
        $im->thumbnailimage($width, $height, true);
        $im->mergeImageLayers(\imagick::LAYERMETHOD_FLATTEN);
       // $im->setImageAlphaChannel(\imagick::ALPHACHANNEL_REMOVE);
        $im->writeimage($outfilename.".jpeg");

        unlink($outfilename);


        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($outfilename.".jpeg"));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($outfilename.".jpeg") . '"');

        $response->sendHeaders();

        $response->setContent(readfile($outfilename.".jpeg"));

        unlink($outfilename.".jpeg");

        return $response;

    }

    /**
     * @Route("/layouter/{layouterId}/{site}/{width}/{height}", name="psc_plugin_formlayouter_preview_layouter")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param Layouter $formLayouter
     * @param Pdf $renderer
     * @param $layouterId
     * @param int $site
     * @param int $width
     * @param int $height
     * @return Response
     * @throws \ImagickException
     */
    public function layouterAction(Request $request, EntityManagerInterface $entityManager, DocumentManager $documentManager, Layouter $formLayouter, Pdf $renderer, $layouterId, $site = 1, $width = 400, $height = 900)
    {
        $width = round($width);
        $height = round($height);

        /** @var Layoutdesigndata $layoutDesignData */
        $layoutDesignData = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $layouterId));


        /** @var Product $product */
        $product = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
            ->findOneBy(array('uuid' => $layoutDesignData->getArticleUuid()));

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string)$product->getUid()));

        $formLayouter->setXml($productDoc->getPluginSetting('formlayouter', 'xml'));

        $actSite = $formLayouter->getSite($site);
        $config = $formLayouter->getConfig();

        $outfilename = "temp/".uniqid().".pdf";

        $renderer->setSite($actSite);
        $renderer->setConfig($config);
        $renderer->setOptions($layoutDesignData->getDesign());
        $renderer->renderStep($outfilename);

        $im = new \imagick($outfilename."[0]");
        if($im->getImageWidth() < $width) {
            $width = $im->getImageWidth();
        }
        $im->setResolution(300,300);
        $im->thumbnailimage($width, $height, true);
        $im->writeimage($outfilename.".png");

        unlink($outfilename);


        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($outfilename.".png"));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($outfilename.".png") . '"');

        $response->sendHeaders();

        $response->setContent(readfile($outfilename.".png"));

        unlink($outfilename.".png");
        return $response;
    }
}