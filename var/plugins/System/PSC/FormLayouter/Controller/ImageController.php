<?php
namespace Plugin\System\PSC\FormLayouter\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use DOMXPath;
use MongoDB\BSON\ObjectId;
use Plugin\Custom\CHILIPublish\Designer\Api\Main;
use Plugin\Custom\CHILIPublish\Designer\Api\GenerateApiKey;
use Plugin\Custom\CHILIPublish\Designer\Api\SetWorkspaceAdministration;
use Plugin\Custom\CHILIPublish\Designer\Api\ResourceItemCopy;
use Plugin\Custom\CHILIPublish\Designer\Api\DocumentGetHTMLEditorURL;
use Plugin\System\PSC\FormLayouter\Form\FormDesigner;
use Plugin\System\PSC\FormLayouter\Renderer\Pdf;
use Plugin\System\PSC\FormLayouter\Session\FormLayouter;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ServiceController
 *
 * @Route("/image")
 */
class ImageController extends AbstractController
{

    /**
     * @Route("/info/{id}", name="psc_plugin_formlayouter_image_info")
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $id
     * @return JsonResponse
     */
    public function infoAction(Request $request, DocumentManager $documentManager, $id)
    {
        /** @var \Plugin\System\PSC\FormLayouter\Document\Image $image */
        $image = $documentManager
            ->getRepository('Plugin\System\PSC\FormLayouter\Document\Image')
            ->findOneBy(['_id' => new ObjectId($id)]);

        return new JsonResponse([
            'title' => $image,
            'rotate' => $image->getRotate(),
            'cropBox' => [
                'width' => $image->getCropWidth(),
                'height' => $image->getCropHeight(),
                'x' => $image->getCropX(),
                'y' => $image->getCropY()
            ]
        ]);
    }

    /**
     * @Route("/original/{id}/{width}/{height}", name="psc_plugin_formlayouter_image_original")
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $id
     * @param int $width
     * @param int $height
     * @return Response
     * @throws \ImagickException
     */
    public function imageAction(Request $request, DocumentManager $documentManager, $id, $width = 400, $height = 400)
    {

        $width = round($width);
        $height = round($height);

        /** @var \Plugin\System\PSC\FormLayouter\Document\Image $image */
        $image = $documentManager
            ->getRepository('Plugin\System\PSC\FormLayouter\Document\Image')
            ->findOneBy(['_id' => new ObjectId($id)]);

        $outfilename = "temp/".md5($id);

        $im = new \imagick("/data/www/new/web/market/motive/" . $image->getFileName());
        if($im->getImageWidth() < $width) {
            $width = $im->getImageWidth();
        }
        $im->setResolution(300,300);
        $im->thumbnailimage($width, $height, true);
        $im->writeimage($outfilename.".png");

        $response = new Response();

        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($outfilename.".png"));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($outfilename.".png") . '"');

        $response->sendHeaders();

        $response->setContent(readfile($outfilename.".png"));

        unlink($outfilename);

        return $response;

    }
}