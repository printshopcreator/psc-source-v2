<?php
namespace Plugin\System\PSC\FormLayouter\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use DOMXPath;
use Gaufrette\Filesystem;
use Gaufrette\FilesystemMapInterface;
use Imagick;
use Knp\Bundle\GaufretteBundle\FilesystemMap;
use MongoDB\BSON\ObjectId;
use Plugin\System\PSC\FormLayouter\Document\Image;
use Plugin\System\PSC\FormLayouter\Form\FormDesigner;
use Plugin\System\PSC\FormLayouter\Renderer\Pdf;
use Plugin\System\PSC\FormLayouter\Session\FormLayouter;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UploadController
 *
 * @Route("/upload")
 */
class UploadController extends AbstractController
{

    /**
     * @Route("/single", name="psc_plugin_formlayouter_upload_single")
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param FilesystemMapInterface $filesystemMap
     * @return JsonResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function singleAction(Request $request, DocumentManager $documentManager, FilesystemMap $filesystemMap)
    {
        if ($request->files->has("file")) {
            $file = $request->files->get("file");

            if($file instanceof UploadedFile) {


                $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());

                /** @var Filesystem $fileContainer */
                $fileContainer = $filesystemMap->get('steplayouter_motiv');

                if(
                    $file->getMimeType() == "image/png" ||
                    $file->getMimeType() == "image/jpg" ||
                    $file->getMimeType() == "image/jpeg"
                ){
                    $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), "jpg");

                    $filenameTemp = sprintf('/tmp/%s.%s', uniqid(), "jpg");
                    exec("convert ".$file->getPathname()." -colorspace cmyk -auto-orient ".$filenameTemp);

                    $fileContainer->write($filename, file_get_contents($filenameTemp));

                    unlink($filenameTemp);

                }else{
                    $fileContainer->write($filename, file_get_contents($file->getPathname()));
                }


                $image = new Image();
                $image->setSessionId($request->getSession()->getId());
                $image->setFileName($fileContainer->get($filename)->getKey());
                $image->setCropWidth(100);
                $image->setCropHeight(100);

                $documentManager->persist($image);
                $documentManager->flush();

                return new JsonResponse(['success' => true, 'id' => $image->getId()]);
            }
        }

        return new JsonResponse(['success' => false]);
    }

    /**
     * @Route("/cropbox/{id}", name="psc_plugin_formlayouter_upload_crop")
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $id
     * @return JsonResponse
     */
    public function cropAction(Request $request, DocumentManager $documentManager, $id)
    {
        /** @var \Plugin\System\PSC\FormLayouter\Document\Image $image */
        $image = $documentManager
            ->getRepository('Plugin\System\PSC\FormLayouter\Document\Image')
            ->findOneBy(['_id' => new ObjectId($id)]);

        $image->setCrop(true);
        $image->setCropX($request->get('x'));
        $image->setCropY($request->get('y'));
        $image->setCropWidth($request->get('width'));
        $image->setCropHeight($request->get('height'));

        $documentManager->persist($image);
        $documentManager->flush();

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/rotate/{id}", name="psc_plugin_formlayouter_upload_rotate")
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $id
     * @return JsonResponse
     */
    public function rotateAction(Request $request, DocumentManager $documentManager, $id)
    {
        /** @var \Plugin\System\PSC\FormLayouter\Document\Image $image */
        $image = $documentManager
            ->getRepository('Plugin\System\PSC\FormLayouter\Document\Image')
            ->findOneBy(['_id' => new ObjectId($id)]);


        exec("convert /data/www/new/web/market/motive/" . $image->getFileName()." -rotate ".($request->get('rotate'))." /data/www/new/web/market/motive/" . $image->getFileName());


        return new JsonResponse(['success' => true]);
    }
}