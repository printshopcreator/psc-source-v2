<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Plugin\System\PSC\FormLayouter\Form\XML\EditType;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use PSC\Shop\ProductBundle\Form\Backend\ProductType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class XmlController extends AbstractController
{

    /**
     * creative
     *
     * @Route("/xml/edit/{uuid}", name="psc_plugin_formlayouter_backend_xml_edit")
     * @template()
     *
     * @param Request $request Request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param string $uuid Uuid
     *
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager, EntityManagerInterface $entityManager, $uuid)
    {
        $selectedShop = $shopService->getSelectedShop();

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string)$product->getUid()));

        $data = ['uuid' => $product->getUUid(), 'xml' => $productDoc->getPluginSettingModule('formlayouter', 'xml')];

        $form = $this->createForm(EditType::class, $data);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            dump($data);
            $productDoc->setPluginSettingModule('formlayouter', 'xml', $data['xml']);
            $documentManager->persist($productDoc);
            $documentManager->flush();
        }
        return array(
            'form' => $form->createView(),
            'product' => $product
        );
    }
}