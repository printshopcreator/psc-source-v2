<?php
namespace Plugin\System\PSC\FormLayouter\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use DOMXPath;
use Gaufrette\Filesystem;
use Plugin\System\PSC\FormLayouter\Document\Image;
use Plugin\System\PSC\FormLayouter\Form\FormDesigner;
use Plugin\System\PSC\FormLayouter\Renderer\Pdf;
use Plugin\System\PSC\FormLayouter\Session\FormLayouter;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class UploadController
 *
 * @Route("/store")
 */
class StoreController extends AbstractController
{

    /**
     * @Route("/save", name="psc_plugin_formlayouter_store_save")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function saveAction(Request $request, EntityManagerInterface $entityManager)
    {

        $uuid = $request->get("layouter");
        $articleUuid = $request->get("article");

        $layoutDesignData = new Layoutdesigndata();
        $layoutDesignData->setUuid($uuid);
        $layoutDesignData->setArticleUuid($articleUuid);
        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }
        $layoutDesignData->setDesign($session->getOptions($articleUuid));

        $entityManager->persist($layoutDesignData);
        $entityManager->flush();

        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true)));

        return $json;
    }

    /**
     * @Route("/update", name="psc_plugin_formlayouter_store_update")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function updateAction(Request $request, EntityManagerInterface $entityManager)
    {

        $uuid = $request->get("layouter");
        $articleUuid = $request->get("article");

        $layoutDesignData = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $uuid));


        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $layoutDesignData->setDesign($session->getOptions($articleUuid));

        $entityManager->persist($layoutDesignData);
        $entityManager->flush();

        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true)));

        return $json;
    }
}