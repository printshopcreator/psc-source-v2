<?php
namespace Plugin\System\PSC\FormLayouter\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use DOMDocument;
use DOMXPath;
use Plugin\Custom\CHILIPublish\Designer\Api\Main;
use Plugin\Custom\CHILIPublish\Designer\Api\GenerateApiKey;
use Plugin\Custom\CHILIPublish\Designer\Api\SetWorkspaceAdministration;
use Plugin\Custom\CHILIPublish\Designer\Api\ResourceItemCopy;
use Plugin\Custom\CHILIPublish\Designer\Api\DocumentGetHTMLEditorURL;
use Plugin\System\PSC\FormLayouter\Form\FormDesigner;
use Plugin\System\PSC\FormLayouter\Service\Layouter;
use Plugin\System\PSC\FormLayouter\Session\FormLayouter;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

/**
 * Class ServiceController
 *
 * @Route("/designer")
 */
class DesignerController extends AbstractController
{

    /**
     * @Route("/load/{productId}/{layouterId}", name="psc_plugin_formlayouter_load")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $productId
     * @param string $layouterId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function loadAction(Request $request, EntityManagerInterface $entityManager, $productId, $layouterId = "")
    {
        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        if($layouterId != "") {
            /** @var Layoutdesigndata $layoutDesignData */
            $layoutDesignData = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $layouterId));

            $session->setOptions($productId, $layoutDesignData->getDesign());
        }

        return $this->redirectToRoute("psc_plugin_formlayouter_start", ['productId' => $productId, 'layouterId' => $layouterId]);
    }

    /**
     * @Route("/start/{productId}/{layouterId}", name="psc_plugin_formlayouter_start")
     *
     * @template
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param Layouter $formLayouter
     * @param $productId
     * @param string $layouterId
     * @return array
     */
    public function startAction(Request $request, EntityManagerInterface $entityManager, DocumentManager $documentManager, Layouter $formLayouter, $productId, $layouterId = "")
    {
        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
            ->findOneBy(array('uuid' => $productId));

/** @var \PSC\Shop\EntityBundle\Entity\Shop $shop */
$shop = $product->getShop();

//\var_dump($shop->getLayout());
        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string)$product->getUid()));



        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $formLayouter->setXml($productDoc->getPluginSetting('formlayouter', 'xml'));

        if($request->get('prev', false) !== false) {
            $actStep = $formLayouter->getStep($request->get('actStep', 1)-1);
        }elseif($request->get('next', false) !== false) {
            $actStep = $formLayouter->getStep($request->get('actStep', 1)+1);
        }else{
            $actStep = $formLayouter->getStep($request->get('actStep', 1));
        }
        $config = $formLayouter->getConfig();

        $data = [];

        $form = $this->createForm(FormDesigner::class, $data, ['step' => $actStep, 'session' => $session, 'productId' => $productId]);

        $request->getSession()->set('formlayouter', $session);

        $loader = new FilesystemLoader('/data/www/old/application/design/vorlagen/' . $shop->getLayout() . '/step_layouter/');
        $twig = new Environment($loader);
//\var_dump($twig);

$header = $twig->render('header.html.twig');
$footer = $twig->render('footer.html.twig');
        return [
            'step' => $actStep,
            'form' => $form->createView(),
            'product' => $product,
            'firstStart' => $layouterId!=""? 1: 0,
            'layouterId' => $layouterId,
            'config' => $config,
            'layout' => $shop->getLayout(),
            'header' => $header,
            'footer' => $footer,
        ];

    }
    
    /**
     * @Route("/update/{productId}", name="psc_plugin_formlayouter_update")
     * @param Request $request
     * @param $productId
     * @return JsonResponse
     */
    public function updateAction(Request $request, $productId)
    {

        $data = json_decode($request->getContent(), true);

        $session = new FormLayouter();
        if($request->getSession()->has('formlayouter')) {
            $session = $request->getSession()->get('formlayouter');
        }

        $session->setOptions($productId, array_merge($session->getOptions($productId), $data));

        $request->getSession()->set('formlayouter', $session);

        return new JsonResponse(['success' => true]);

    }
}