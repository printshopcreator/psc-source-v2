<?php
namespace Plugin\System\PSC\FormLayouter;

use PSC\System\PluginBundle\Plugin\Base;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Step Layouter';

    public function getType()
    {
        return Plugin::Frontend;
    }

    public function getDescription()
    {
        return 'Step Layouter';
    }

    public function getVersion()
    {
        return 1;
    }

}