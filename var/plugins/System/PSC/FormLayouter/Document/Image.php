<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Hash;

/**
 * @Document(repositoryClass="Plugin\System\PSC\FormLayouter\Document\Repository\ImageRepository", collection="plugin_system_psc_formlayouter_image")
 */
class Image
{
    /**
     * @Id
     */
    protected $id;

    /**
     * @Field(type="string")
     */
    protected $sessionId;

    /**
     * @Field(type="string")
     */
    protected $fileName;

    /**
     * @var boolean
     *
     * @Field(type="bool")
     */
    protected $crop = false;

    /**
     * @Field(type="int")
     */
    protected $cropX = 0;

    /**
     * @Field(type="int")
     */
    protected $cropY = 0;

    /**
     * @Field(type="int")
     */
    protected $cropWidth = 0;

    /**
     * @Field(type="int")
     */
    protected $cropHeight = 0;

    /**
     * @Field(type="int")
     */
    protected $rotate = 0;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param mixed $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return bool
     */
    public function isCrop()
    {
        return $this->crop;
    }

    /**
     * @param mixed $crop
     */
    public function setCrop($crop)
    {
        $this->crop = $crop;
    }

    /**
     * @return mixed
     */
    public function getCropX()
    {
        return $this->cropX;
    }

    /**
     * @param mixed $cropX
     */
    public function setCropX($cropX)
    {
        $this->cropX = $cropX;
    }

    /**
     * @return mixed
     */
    public function getCropY()
    {
        return $this->cropY;
    }

    /**
     * @param mixed $cropY
     */
    public function setCropY($cropY)
    {
        $this->cropY = $cropY;
    }

    /**
     * @return mixed
     */
    public function getCropWidth()
    {
        return $this->cropWidth;
    }

    /**
     * @param mixed $cropWidth
     */
    public function setCropWidth($cropWidth)
    {
        $this->cropWidth = $cropWidth;
    }

    /**
     * @return mixed
     */
    public function getCropHeight()
    {
        return $this->cropHeight;
    }

    /**
     * @param mixed $cropHeight
     */
    public function setCropHeight($cropHeight)
    {
        $this->cropHeight = $cropHeight;
    }

    /**
     * @return mixed
     */
    public function getRotate()
    {
        return $this->rotate;
    }

    /**
     * @param mixed $rotate
     */
    public function setRotate($rotate)
    {
        $this->rotate = $rotate;
    }


}
