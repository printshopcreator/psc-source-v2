<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Config;

use Plugin\System\PSC\FormLayouter\Model\Config\Pdf;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Base;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Preview;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Text;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Textarea;

class Config {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Config
     */
    public function parse($xml)
    {
        $config = new \Plugin\System\PSC\FormLayouter\Model\Config();

        $config->setPdf(new Pdf());
        if($xml->unit) {
            $config->getPdf()->setUnit((string)$xml->unit);
        }
        if($xml->pdfPreviewButton) {
            $config->setPdfPreviewButton(boolval(intval($xml->pdfPreviewButton)));
        }
        if($xml->pdfPrintButton) {
            $config->setPdfPrintButton(boolval(intval($xml->pdfPrintButton)));
        }
        if($xml->pdfx4) {
            $config->getPdf()->setPdfx4(boolval(intval($xml->pdfx4)));
            $config->getPdf()->setIccProfile((string)$xml->iccProfile);
        }
        return $config;
    }


}