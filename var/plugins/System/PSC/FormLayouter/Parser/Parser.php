<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser;

use Plugin\System\PSC\FormLayouter\Model\Config;
use Plugin\System\PSC\FormLayouter\Model\Pdf\Site;
use Plugin\System\PSC\FormLayouter\Parser\Form\Step;

class Parser
{

    /** @var \SimpleXMLElement */
    private $xml;

    public function loadXml($xml)
    {
        $this->xml = simplexml_load_string($xml);
    }

    /**
     * @return \Plugin\System\PSC\FormLayouter\Model\Config
     */
    public function parseConfig()
    {
        $configParser = new \Plugin\System\PSC\FormLayouter\Parser\Config\Config();

        $config = $configParser->parse($this->xml->general);

        return $config;
    }

    /**
     * @return \Plugin\System\PSC\FormLayouter\Model\Steps\Step[]
     */
    public function parseStep()
    {

        $steps = [];

        $count = $this->xml->steps->children()->count();

        if($count) {
            $stepParser = new Step();

            $i = 0;

            foreach($this->xml->steps->children() as $stepXml) {
                $step = $stepParser->parse($stepXml);

                if($i == 0) {
                    $step->setFirst(true);
                }
                if($i == $count-1) {
                    $step->setLast(true);
                }

                $steps[] = $step;
                $i++;
            }
        }

        return $steps;

    }

    /**
     * @return \Plugin\System\PSC\FormLayouter\Model\Pdf\Site[]
     */
    public function parseSites()
    {

        $sites = [];

        $count = $this->xml->sites->children()->count();

        if($count) {
            $siteParser = new \Plugin\System\PSC\FormLayouter\Parser\Pdf\Site();


            foreach($this->xml->sites->children() as $siteXml) {
                $site = $siteParser->parse($siteXml);

                $sites[] = $site;
            }
        }

        return $sites;

    }

}