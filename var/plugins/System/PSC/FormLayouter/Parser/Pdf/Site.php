<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Pdf;

use Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements\Image;
use Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements\Media;
use Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements\Motiv;
use Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements\Pdf;
use Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements\Text;
use Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements\TextFlow;

class Site {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Pdf\Site
     */
    public function parse($xml)
    {
        $site = new \Plugin\System\PSC\FormLayouter\Model\Pdf\Site();
        $site->setId(intval($xml['id']));
        $site->setWidth(intval($xml['width']));
        $site->setHeight(intval($xml['height']));

        if(isset($xml['trimLeft'])) {
            $site->setTrimLeft(intval($xml['trimLeft']));
        }
        if(isset($xml['trimRight'])) {
            $site->setTrimRight(intval($xml['trimRight']));
        }
        if(isset($xml['trimTop'])) {
            $site->setTrimTop(intval($xml['trimTop']));
        }
        if(isset($xml['trimBottom'])) {
            $site->setTrimBottom(intval($xml['trimBottom']));
        }

        if(isset($xml['label'])) {
            $site->setLabel((string)$xml['label']);
        }

        if(isset($xml->elements) && $xml->elements->children()->count()) {
            $elements = [];
            foreach($xml->elements->children() as $type => $xmlElement) {
                $parser = new Text();
                if(strtolower($type) == "pdf") {
                    $parser= new Pdf();
                }
                if(strtolower($type) == "image") {
                    $parser= new Image();
                }
                if(strtolower($type) == "media") {
                    $parser= new Media();
                }
                if(strtolower($type) == "textflow") {
                    $parser= new TextFlow();
                }
                $elements[] = $parser->parse($xmlElement);

            }
            $site->setElements($elements);
        }

        return $site;
    }


}