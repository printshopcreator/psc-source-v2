<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements;

class Pdf extends Base {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\Pdf
     */
    public function parse($xml)
    {
        $pdf = new \Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\Pdf();

        parent::parseBase($pdf, $xml);

        $pdf->setFile((string)$xml);
        $pdf->setWidth(intval($xml['width']));
        $pdf->setHeight(intval($xml['height']));
        $pdf->setSiteToRender(intval($xml['site']));


        return $pdf;
    }
}