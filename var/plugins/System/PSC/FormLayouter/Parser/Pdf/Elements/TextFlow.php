<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements;

class TextFlow extends Base {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\TextFlow
     */
    public function parse($xml)
    {
        $text = new \Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\TextFlow();

        parent::parseBase($text, $xml);

        $text->setText((string)$xml);
        if(isset($xml['fontSize'])) {
            $text->setFontSize(intval($xml['fontSize']));
        }

        if(isset($xml['color'])) {
            $text->setColor($xml['color']);
        }

        if(isset($xml['colorMode'])) {
            $text->setColorMode($xml['colorMode']);
        }

        if(isset($xml['rotate'])) {
            $text->setRotate(intval($xml['rotate']));
        }
        if(isset($xml['font'])) {
            $text->setFont((string)$xml['font']);
        }
        if(isset($xml['showBorder'])) {
            $text->setShowBorder(boolval($xml['showBorder']));
        }
        if(isset($xml['width'])) {
            $text->setWidth(intval($xml['width']));
        }
        if(isset($xml['height'])) {
            $text->setHeight(intval($xml['height']));
        }
        if(isset($xml['vAlign'])) {
            $text->setVAlign((string)$xml['vAlign']);
        }
        if(isset($xml['hAlign'])) {
            $text->setHAlign((string)$xml['hAlign']);
        }
        if(isset($xml['fitmethod'])) {
            $text->setFitMethod((string)$xml['fitmethod']);
        }
        if(isset($xml['leading'])) {
            $text->setLeading((string)$xml['leading']);
        }


        return $text;
    }
}