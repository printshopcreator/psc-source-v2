<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements;

abstract class Base {


    /**
     * @param \Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\Base $element
     * @param \SimpleXMLElement $xml
     * @return mixed
     */
    public function parseBase($element, $xml)
    {
        if(!isset($xml['id'])) {
            $element->setId(md5(rand()));
        }else{
            $element->setId((string)$xml['id']);
        }

        if(isset($xml['x'])) {
            $element->setX(intval($xml['x']));
        }
        if(isset($xml['y'])) {
            $element->setY(intval($xml['y']));
        }
        if(isset($xml['preview'])) {
            $element->setPreview(boolval(intval($xml['preview'])));
        }
        if(isset($xml['print'])) {
            $element->setPrint(boolval(intval($xml['print'])));
        }

        return $element;
    }

}