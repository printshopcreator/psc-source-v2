<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Pdf\Elements;

class Image extends Base {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\Image
     */
    public function parse($xml)
    {
        $image = new \Plugin\System\PSC\FormLayouter\Model\Pdf\Elements\Image();

        parent::parseBase($image, $xml);

        $image->setFile((string)$xml);
        $image->setWidth(intval($xml['width']));
        $image->setHeight(intval($xml['height']));

        if(isset($xml['border'])) {
            $image->setBorder($xml['border']);
        }

        return $image;
    }
}