<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Form;

use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Base;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Preview;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Text;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Image;
use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Textarea;

class Step {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Steps\Step
     */
    public function parse($xml)
    {
        $step = new \Plugin\System\PSC\FormLayouter\Model\Steps\Step();
        $step->setId(intval($xml['id']));

        if(isset($xml['label'])) {
            $step->setName((string)$xml['label']);
        }
        if(isset($xml->headline)) {
            $step->setHeadline((string)$xml->headline);
        }
        if(isset($xml->description)) {
            $step->setDescription((string)$xml->description);
        }

        if($xml->elements->count()) {
            foreach($xml->elements->children() as $typeXml => $optionXml) {

                $parser = new Text();
                if($typeXml == "textarea") {
                    $parser = new Textarea();
                }
                if($typeXml == "preview") {
                    $parser = new Preview();
                }
                if($typeXml == "image") {
                    $parser = new Image();
                }
                if($typeXml == "select") {
                    $parser = new \Plugin\System\PSC\FormLayouter\Parser\Form\Options\Select\Simple();
                }
                if($typeXml == "mediaSelect") {
                    $parser = new \Plugin\System\PSC\FormLayouter\Parser\Form\Options\Select\Media();
                }
                $option = $parser->parse($optionXml);

                $step->getColumn($option->getColumn())->addOption($option);
            }
        }

        return $step;
    }


}