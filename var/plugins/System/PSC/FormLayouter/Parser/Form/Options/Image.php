<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Form\Options;

class Image extends Base {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Image
     */
    public function parse($xml)
    {
        $image = new \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Image();

        parent::parseBase($image, $xml);

        if(isset($xml['cropBox'])) {
            $image->setCropBox(boolval($xml['cropBox']));
        }
        if(isset($xml['cropBoxAspectRatio'])) {
            $image->setCropBoxAspectRatio(floatval($xml['cropBoxAspectRatio']));
        }
        return $image;
    }
}