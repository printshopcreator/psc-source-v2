<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Form\Options;

abstract class Base {


    /**
     * @param \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Base $element
     * @param \SimpleXMLElement $xml
     * @return mixed
     */
    public function parseBase($element, $xml)
    {
        if(!isset($xml['id'])) {
            $element->setId(md5(rand()));
        }else{
            $element->setId((string)$xml['id']);
        }

        if(isset($xml['column'])) {
            $element->setColumn(intval($xml['column']));
        }
        if(isset($xml['label'])) {
            $element->setLabel((string)$xml['label']);
        }

        return $element;
    }

}