<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Form\Options\Select;

use Plugin\System\PSC\FormLayouter\Parser\Form\Options\Base;

class Media extends Base {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Select\Media
     */
    public function parse($xml)
    {
        $select = new \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Select\Media();

        parent::parseBase($select, $xml);

        foreach($xml->option as $option) {
            $select->addOption((string)$option['id'], (string)$option);
        }

        return $select;
    }
}