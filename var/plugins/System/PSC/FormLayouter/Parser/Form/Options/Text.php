<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Form\Options;

class Text extends Base {


    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Text
     */
    public function parse($xml)
    {
        $text = new \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Text();

        parent::parseBase($text, $xml);

        $text->setText((string)$xml);
        if(isset($xml['required'])) {
            $text->setRequired(boolval($xml['required']));
        }
        return $text;
    }
}