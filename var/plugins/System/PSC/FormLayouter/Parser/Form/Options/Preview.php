<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Parser\Form\Options;

class Preview extends Base {

    /**
     * @param \SimpleXMLElement $xml
     * @return \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Preview
     */
    public function parse($xml)
    {
        $preview = new \Plugin\System\PSC\FormLayouter\Model\Steps\Options\Preview();

        parent::parseBase($preview, $xml);

        if(isset($xml['site'])) {
            $preview->setSite(intval($xml['site']));
        }

        return $preview;
    }
}