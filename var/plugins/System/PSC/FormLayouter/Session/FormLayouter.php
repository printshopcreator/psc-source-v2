<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Session;

class FormLayouter implements \Serializable
{

    protected $options = array();

    protected $actualStep = 0;

    /**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(
            [
                $this->options,
                $this->actualStep
            ]
        );
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        list(
            $this->options,
            $this->actualStep
            ) = $data;
    }

    /**
     * @param $articleUUID
     * @return array
     */
    public function getOptions($articleUUID)
    {
        if(!isset($this->options[$articleUUID])) {
            return array();
        }
        return $this->options[$articleUUID];
    }

    /**
     * @param array $options
     */
    public function setOptions($articleUUID, $options)
    {
        $this->options[$articleUUID] = $options;
    }

    /**
     * @return int
     */
    public function getActualStep()
    {
        return $this->actualStep;
    }

    /**
     * @param int $actualStep
     */
    public function setActualStep($actualStep)
    {
        $this->actualStep = $actualStep;
    }

    public function hasOption($articleUUID, $option)
    {
        if(!isset($this->options[$articleUUID])) {
            return false;
        }

        return isset($this->options[$articleUUID][$option]);
    }

    public function getOption($articleUUID, $option)
    {
        return $this->options[$articleUUID][$option];
    }

}