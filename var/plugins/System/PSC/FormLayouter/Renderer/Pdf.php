<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Renderer;

use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\ObjectId;
use Plugin\System\PSC\FormLayouter\Model\Config;
use Plugin\System\PSC\FormLayouter\Model\Pdf\Site;
use PSC\Shop\MediaBundle\Document\Media;

class Pdf
{



    /** @var Site */
    private $site;

    /** @var Config */
    private $config;

    private $pdflibLic;

    private $options;

    function __construct(DocumentManager $mongoDb, $pdflib_lic)
    {
        $this->mongoDb = $mongoDb;
        $this->pdflibLic = $pdflib_lic;
    }

    public function setSite($site)
    {
        $this->site = $site;
    }

    public function renderStep($filename)
    {
        $p = new \PDFlib();
        $p->set_option("errorpolicy=return");
        $p->set_option("stringformat=utf8");
        @$p->set_parameter("license", $this->pdflibLic);
        if ($p->begin_document($filename, "") == 0) {
            die("Error: " . $p->get_errmsg());
        }



        $p->set_option("topdown=true");
        $p->set_option("usercoordinates=true");

        $scale = 1;
        if(strtolower($this->config->getPdf()->getUnit()) == "mm") {
            $scale = 2.83465;
        }elseif(strtolower($this->config->getPdf()->getUnit()) == "cm") {
            $scale = 28.3465;
        }

        $p->begin_page_ext(0, 0, "width=" . (($this->site->getWidth()+$this->site->getTrimLeft()+$this->site->getTrimRight())*$scale) . " height="
            . (($this->site->getHeight()+$this->site->getTrimTop()+$this->site->getTrimBottom())*$scale) . " trimbox={" . ($this->site->getTrimLeft()*$scale) . " "
            . ($this->site->getTrimTop()*$scale) . " " . (($this->site->getWidth()+$this->site->getTrimLeft())*$scale) . " " . (($this->site->getHeight()+$this->site->getTrimTop())*$scale) . "}");

        foreach($this->site->getElements() as $element) {
            $element->setScale($scale);
            $element->setOptions($this->options);
            $element->setMongoDb($this->mongoDb);
            $element->setSite($this->site);
            $element->renderPdf($p);
        }

        $p->end_page_ext("");

        $p->end_document("");
    }

    public function renderPreviewPdf($sites, $filename)
    {
        $p = new \PDFlib();
        $p->set_option("errorpolicy=return");
        $p->set_option("stringformat=utf8");
        @$p->set_parameter("license", $this->pdflibLic);
        if ($p->begin_document($filename, "") == 0) {
            die("Error: " . $p->get_errmsg());
        }



        $p->set_option("topdown=true");
        $p->set_option("usercoordinates=true");

        $scale = 1;
        if(strtolower($this->config->getPdf()->getUnit()) == "mm") {
            $scale = 2.83465;
        }elseif(strtolower($this->config->getPdf()->getUnit()) == "cm") {
            $scale = 28.3465;
        }

        foreach($sites as $site) {
            $p->begin_page_ext(0, 0, "width=" . (($site->getWidth()+$site->getTrimLeft()+$site->getTrimRight())*$scale) . " height="
                . (($site->getHeight()+$site->getTrimTop()+$site->getTrimBottom())*$scale) . " trimbox={" . ($site->getTrimLeft()*$scale) . " "
                . ($site->getTrimTop()*$scale) . " " . (($site->getWidth()+$site->getTrimLeft())*$scale) . " " . (($site->getHeight()+$site->getTrimTop())*$scale) . "}");

            foreach ($site->getElements() as $element) {
                if(!$element->isPreview()) continue;
                $element->setScale($scale);
                $element->setOptions($this->options);
                $element->setMongoDb($this->mongoDb);
                $element->setSite($site);
                $element->renderPdf($p);
            }

            $p->end_page_ext("");
        }

        $p->end_document("");
    }

    public function renderPrintPdf($sites, $filename)
    {
        $p = new \PDFlib();
        $p->set_option("errorpolicy=return");
        $p->set_option("stringformat=utf8");
        @$p->set_parameter("license", $this->pdflibLic);
        $p->set_info("Creator", "PSC");
        $p->set_info("Title", "PDFX4");

        if($this->config->getPdf()->isPdfx4()) {

            if ($p->begin_document($filename, "pdfx=PDF/X-4") == 0) {
                die("Error: " . $p->get_errmsg());
            }

            /** @var Media $media */
            $media = $this->mongoDb
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['_id' => new ObjectId($this->config->getPdf()->getIccProfile())]);

            if ($p->load_iccprofile("/data/www/new/web" . $media->getUrl(), "usage=outputintent") == 0) {
                print("Error: " . $p->get_errmsg() . "\n");
                print("See www.pdflib.com for output intent ICC profiles.\n");
                $p->delete();
                return (2);
            }
        }else{
            if ($p->begin_document($filename, "") == 0) {
                die("Error: " . $p->get_errmsg());
            }
        }

        $p->set_option("topdown=true");
        $p->set_option("usercoordinates=true");


        $scale = 1;
        if(strtolower($this->config->getPdf()->getUnit()) == "mm") {
            $scale = 2.83465;
        }elseif(strtolower($this->config->getPdf()->getUnit()) == "cm") {
            $scale = 28.3465;
        }

        foreach($sites as $site) {
            $p->begin_page_ext(0, 0, "width=" . (($site->getWidth()+$site->getTrimLeft()+$site->getTrimRight())*$scale) . " height="
                . (($site->getHeight()+$site->getTrimTop()+$site->getTrimBottom())*$scale) . " trimbox={" . ($site->getTrimLeft()*$scale) . " "
                . ($site->getTrimTop()*$scale) . " " . (($site->getWidth()+$site->getTrimLeft())*$scale) . " " . (($site->getHeight()+$site->getTrimTop())*$scale) . "}");

            foreach ($site->getElements() as $element) {
                if(!$element->isPrint()) continue;
                $element->setScale($scale);
                $element->setOptions($this->options);
                $element->setMongoDb($this->mongoDb);
                $element->setSite($site);
                $element->renderPdf($p);
            }

            $p->end_page_ext("");
        }

        $p->end_document("");
    }

    /**
     * @param Config $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;
    }
}