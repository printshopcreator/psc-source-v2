<?php

namespace Plugin\System\PSC\FormLayouter\Form\Field;

use DirectoryIterator;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use Plugin\System\PSC\FormLayouter\Form\Group\FormLayouterEditor;
use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EnableEditor implements Field
{

    public function getTemplate()
    {
        return '@PluginSystemPSCFormLayouter/form/field/enable_editor.html.twig';
    }

    public function getModule()
    {
        return Field::Theme;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $txtheader = "";
        $txtfooter = "";
        $this->options = $options;

        /** @var Shop $shopEntity */
        $shopEntity = $this->options['shopEntity'];
        $tempLayouts = [];
        if ($shopEntity->isCustomTemplates() == 1) {
            foreach (new DirectoryIterator('/data/www/old/application/design/vorlagen') as $file) {
                if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                    continue;
                }
                $tempLayouts[$file->getFileName()] = $file->getFileName();
            }
        } else {
            if (file_exists('/data/www/old/application/design/clients/' . $shopEntity->getUID())) {
                foreach (new DirectoryIterator('/data/www/old/application/design/clients/' . $shopEntity->getUID()) as $file) {
                    if ($file == '.' || $file == '..' || $file == 'config' || $file == '.svn' || $file == 'datapacks' || $file == '.DS_Store') {
                        continue;
                    }
                    $tempLayouts[$file->getFileName()] = $file->getFileName();
                }
            }
        }
        if(isset($_POST["settings"]["bootstrap3Images"]["layout"])) {
                header('location: /apps/backend/theme/settings');
                //die();
        } else {
            if (!file_exists("/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/step_layouter")) {
                mkdir("/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/step_layouter", 0777);
            }
            $filenameheader = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/step_layouter/header.html.twig";
            $filenamefooter = "/data/www/old/application/design/vorlagen/" . $shopEntity->getLayout() . "/step_layouter/footer.html.twig";
            if (!file_exists($filenameheader)) {
                touch($filenameheader);
                chmod($filenameheader, 0777);
            }
            if (!file_exists($filenamefooter)) {
                touch($filenamefooter);
                chmod($filenamefooter, 0777);
            }

            $handleheader = fopen($filenameheader, 'r');
            if(filesize($filenameheader)>0) {
                $txtheader = fread($handleheader, filesize($filenameheader));
                fclose($handleheader);
            }
            $handlefooter = fopen($filenamefooter, 'r');
            if(filesize($filenamefooter)) {
                $txtfooter = fread($handlefooter, filesize($filenamefooter));
                fclose($handlefooter);
            }

        }


        $builder->add('header', TextareaType::class, array(
            'label' => 'Header',
            'data' => $txtheader,
            'required' => false,
            'attr' => array('rows' => 50)
        ))
        ->add('footer', TextareaType::class, array(
            'label' => 'Footer',
            'data' => $txtfooter,
            'required' => false,
            'attr' => array('rows' => 50)
        ));
        ;

        return $builder;
    }

    public function getGroup()
    {
        return FormLayouterEditor::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        // TODO: Implement formPostSetData() method.
    }

    public function formPostSubmit(FormEvent $event)
    {
            $filename = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/step_layouter/header.html.twig";
            $handle = fopen($filename, 'w'); 
            fputs($handle, $event->getForm()->get('formlayoutereditor')->get('header')->getData()); 
            fclose($handle); 

            $filename = "/data/www/old/application/design/vorlagen/" . $_POST["settings"]["bootstrap3General"]["layout"] . "/step_layouter/footer.html.twig";
            $handle = fopen($filename, 'w'); 
            fputs($handle, $event->getForm()->get('formlayoutereditor')->get('footer')->getData()); 
            fclose($handle); 
        // TODO: Implement formPostSubmit() method.
    }

    public function formPreSetData(FormEvent $event)
    {
        // TODO: Implement formPreSetData() method.
    }
}