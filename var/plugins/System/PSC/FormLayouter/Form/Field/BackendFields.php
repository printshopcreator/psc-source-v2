<?php

namespace Plugin\System\PSC\FormLayouter\Form\Field;

use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use Plugin\System\PSC\FormLayouter\Form\Group\FormLayouter;
use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BackendFields extends \PSC\System\PluginBundle\Form\Field implements  Field
{

    protected $tab = 'uploads';

    private $statusService;

    public function __construct(Status $statusService)
    {
        $this->statusService = $statusService;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCFormLayouter/form/field/backend.html.twig';
    }

    public function getModule()
    {
        return Field::Product;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {

    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('formlayouter', CheckboxType::class, array(
            'label' => 'Step Layouter aktivieren',
            'required' => false
        )) ->add("formlayouter2InitalStatus", ChoiceType::class, array(
            'label' => 'Initalstatus',
            'choices' => $this->statusService->getPositionStatusAsArray(),
            'translation_domain' => 'posstatus'
        ));


        return $builder;
    }

    public function getGroup()
    {
        return "formlayouter";
    }

    public function formPostSetData(FormEvent $event)
    {
        // TODO: Implement formPostSetData() method.
    }

    public function formPostSubmit(FormEvent $event)
    {
        // TODO: Implement formPostSubmit() method.
    }

    public function formPreSetData(FormEvent $event)
    {
        // TODO: Implement formPreSetData() method.
    }
}