<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\FormLayouter\Form\XML;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\Shop\MediaBundle\Form\Type\ImageType;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid', TextType::class, array('disabled' => true))
            ->add('xml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '100%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('save', SubmitType::class, array('label' => 'Speichern'));
    }

    public function getName()
    {
        return 'product';
    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }
}