<?php

namespace Plugin\System\PSC\FormLayouter\Form;

use Plugin\System\PSC\FormLayouter\Model\Steps\Step;
use Plugin\System\PSC\FormLayouter\Session\FormLayouter;
use PSC\Shop\MediaBundle\Service\MediaManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormDesigner extends AbstractType
{
    /**
     * @var MediaManager
     */
    private $mediaManager;

    public function __construct(MediaManager $mediaManager)
    {
        $this->mediaManager = $mediaManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        /** @var FormLayouter $session */
        $session = $options['session'];

        $productId = $options['productId'];

        /** @var Step $step */
        $step = $options['step'];

        foreach($step->getColumns() as $column) {
            foreach($column->getOptions() as $option) {
                if($option->isInForm()) {
                    $option->renderForm($builder, $this->mediaManager);
                }
            }
        }

        foreach($builder->all() as $opt) {
            if($session->hasOption($productId, $opt->getName())) {
                $opt->setData($session->getOption($productId, $opt->getName()));
            }
        }
    }

    public function getName()
    {
        return 'designer';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'step' => null,
            'session' => null,
            'productId' => null,
        ));
    }
}