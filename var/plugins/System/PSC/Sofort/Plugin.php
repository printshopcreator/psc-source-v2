<?php
namespace Plugin\System\PSC\Sofort;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Sofortüberweisung Payment';

    public function getType()
    {
        return Plugin::Payment;
    }

    public function getDescription()
    {
        return 'Stellt Unterstützung für Sofortüberweisung bereit';
    }

    public function getVersion()
    {
        return 1;
    }
}