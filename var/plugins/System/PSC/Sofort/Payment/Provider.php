<?php
namespace Plugin\System\PSC\Sofort\Payment;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Plugin\System\PSC\Sofort\Document\Sofort;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Provider\PaymentProvider;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Order\Payed;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use Sofort\SofortLib\Notification;
use Sofort\SofortLib\Sofortueberweisung;
use Sofort\SofortLib\TransactionData;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Provider extends PaymentProvider
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    private $_eventManager = null;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, Manager $eventManager)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_eventManager = $eventManager;
    }

    public function getName()
    {
        return 'Sofortüberweisung';
    }

    public function getType()
    {
        return 'sofort';
    }

    public function saveDocument(Gatewaysettings $settings, Form $form)
    {
        $doc = new Sofort();
        $doc->setCustomerNr($form->get('sofort')->get('customerNr')->getData());
        $doc->setProjectId($form->get('sofort')->get('projectId')->getData());
        $doc->setApiKey($form->get('sofort')->get('apiKey')->getData());
        $doc->setConfigKey($form->get('sofort')->get('configKey')->getData());

        $settings->setGatewayDocument($doc);

        return $settings;
    }

    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder)
    {

        if(!$settings->getGatewayDocument()) {
            $settings->setGatewayDocument(new Sofort());
        }

        $builder
            ->add('customerNr', TextType::class, array('label' => 'Kundennummer', 'required' => false))
            ->add('projectId', TextType::class, array('label' => 'Projekt-ID', 'required' => false))
            ->add('apiKey', TextType::class, array('label' => 'API-Key', 'required' => false))
            ->add('configKey', TextType::class, array('label' => 'Konfigurationsschlüssel', 'required' => false));

        $builder->get('customerNr')->setData($settings->getGatewayDocument()->getCustomerNr());
        $builder->get('projectId')->setData($settings->getGatewayDocument()->getProjectId());
        $builder->get('apiKey')->setData($settings->getGatewayDocument()->getApiKey());
        $builder->get('configKey')->setData($settings->getGatewayDocument()->getConfigKey());

        return $builder;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCSofort/settings.html.twig';
    }

    public function handlePayment(Request $request)
    {
        $Sofortueberweisung = new Sofortueberweisung($this->getGatewaySettings()->getGatewayDocument()->getConfigKey());
        $Sofortueberweisung->setAmount($request->get('amount'));
        $Sofortueberweisung->setCurrencyCode('EUR');
        $Sofortueberweisung->setReason('Onlinebestellung');
        $Sofortueberweisung->setUserVariable(array(
            'var0',
            'var1',
            $request->get('token')
        ));
        $Sofortueberweisung->setSuccessUrl($this->getHost().'/basket/finish?token='.$request->get('token'), true);
        $Sofortueberweisung->setAbortUrl($this->getHost().'/basket/finish?error=Fehler');
        $Sofortueberweisung->setNotificationUrl($this->getHost().'/apps/payment/notify/'.$this->getShopDoc()->getApiKey().'/sofort', 'url', 'received,credited,untraceable,sofort_bank_account_needed');

        $Sofortueberweisung->sendRequest();
        if($Sofortueberweisung->isError()) {
            //SOFORT-API didn't accept the data
            return new RedirectResponse($this->getHost().'/basket/finish?error='.$Sofortueberweisung->getError());
        } else {
            $transactionId = $Sofortueberweisung->getTransactionId();
            $paymentUrl = $Sofortueberweisung->getPaymentUrl();
            return new RedirectResponse($paymentUrl);
        }
    }

    public function handleNotify(Request $request)
    {

        if($this->getGatewaySettings() == null) {
            return;
        }
        $SofortLibNotification = new Notification();

        $transactionId = $SofortLibNotification->getNotification($request->getContent());

        $SofortLibTransactionData = new TransactionData($this->getGatewaySettings()->getGatewayDocument()->getConfigKey());

        $SofortLibTransactionData->addTransaction($transactionId);

        $SofortLibTransactionData->setApiVersion('2.0');

        $SofortLibTransactionData->sendRequest();

        $token = $SofortLibTransactionData->getUserVariable(0, 2);

        if($SofortLibTransactionData->getStatus() == 'received' ||
            $SofortLibTransactionData->getStatus() == 'credited' ||
            $SofortLibTransactionData->getStatus() == 'untraceable' ||
            $SofortLibTransactionData->getStatus() == 'sofort_bank_account_needed'

        ) {

            /** @var \PSC\Shop\EntityBundle\Document\Order $orderDoc */
            $orderDoc = $this->_doctrine_mongodb
                ->getRepository('PSC\Shop\EntityBundle\Document\Order')
                ->findOneBy(array('paymentToken' => (string)$token));

            /** @var Order $order */
            $order = $this->_entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(array('uid' => $orderDoc->getUid()));

            $order->setStatus(145);
            $this->_entityManager->persist($order);
            $this->_entityManager->flush();

            $notify = new Payed();
            $notify->setShop($this->getShopEntity()->getUID());
            $notify->setOrder($order->getUuid());

            $this->_eventManager->addJob($notify);

        }

    }
}
