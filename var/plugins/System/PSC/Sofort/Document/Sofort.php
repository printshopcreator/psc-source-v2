<?php
namespace Plugin\System\PSC\Sofort\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Sofort
{
    /**
     * @var string $customerNr
     *
     * @Field(type="string")
     */
    protected $customerNr;

    /**
     * @var string $projectId
     *
     * @Field(type="string")
     */
    protected $projectId;

    /**
     * @var string $apiKey
     *
     * @Field(type="string")
     */
    protected $apiKey;

    /**
     * @var string $configKey
     *
     * @Field(type="string")
     */
    protected $configKey;

    /**
     * @return string
     */
    public function getCustomerNr()
    {
        return $this->customerNr;
    }

    /**
     * @param string $customerNr
     */
    public function setCustomerNr($customerNr)
    {
        $this->customerNr = $customerNr;
    }

    /**
     * @return string
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param string $projectId
     */
    public function setProjectId($projectId)
    {
        $this->projectId = $projectId;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getConfigKey()
    {
        return $this->configKey;
    }

    /**
     * @param string $configKey
     */
    public function setConfigKey($configKey)
    {
        $this->configKey = $configKey;
    }
}