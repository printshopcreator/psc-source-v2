<?php
namespace Plugin\System\PSC\Telecash\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Telecash
{
    /**
     * @var string $storeName
     *
     * @Field(type="string")
     */
    protected $storeName;

    /**
     * @var string $secret
     *
     * @Field(type="string")
     */
    protected $secret;

    /**
     * @var boolean $production
     *
     * @Field(type="bool")
     */
    protected $production;

    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->storeName;
    }

    /**
     * @param string $storeName
     */
    public function setStoreName($storeName)
    {
        $this->storeName = $storeName;
    }

    /**
     * @return bool
     */
    public function isProduction()
    {
        return $this->production;
    }

    /**
     * @param bool $production
     */
    public function setProduction($production)
    {
        $this->production = $production;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param string $secret
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
    }


}