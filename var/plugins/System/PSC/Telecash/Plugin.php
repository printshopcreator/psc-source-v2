<?php
namespace Plugin\System\PSC\Telecash;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Telecash Payment';

    public function getType()
    {
        return Plugin::Payment;
    }

    public function getDescription()
    {
        return 'Stellt Unterstützung für Telecash bereit';
    }

    public function getVersion()
    {
        return 1;
    }
}