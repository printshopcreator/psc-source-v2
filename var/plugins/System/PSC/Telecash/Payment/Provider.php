<?php
namespace Plugin\System\PSC\Telecash\Payment;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Plugin\System\PSC\Telecash\Document\Telecash;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Provider\PaymentProvider;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;
use PSC\Shop\QueueBundle\Event\Order\Payed;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use Sofort\SofortLib\Notification;
use Sofort\SofortLib\Sofortueberweisung;
use Sofort\SofortLib\TransactionData;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Provider extends PaymentProvider
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    private $_eventManager = null;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, Manager $eventManager)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_eventManager = $eventManager;
    }

    public function getName()
    {
        return 'Telecash';
    }

    public function getType()
    {
        return 'telecash';
    }

    public function saveDocument(Gatewaysettings $settings, Form $form)
    {
        $doc = new Telecash();
        $doc->setProduction($form->get('telecash')->get('production')->getData());
        $doc->setStoreName($form->get('telecash')->get('storeName')->getData());
        $doc->setSecret($form->get('telecash')->get('secret')->getData());

        $settings->setGatewayDocument($doc);

        return $settings;
    }

    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder)
    {

        if(!$settings->getGatewayDocument()) {
            $settings->setGatewayDocument(new Telecash());
        }

        $builder
            ->add('storeName', TextType::class, array('label' => 'StoreName', 'required' => false))
            ->add('secret', TextType::class, array('label' => 'Secret', 'required' => false))
            ->add('production', CheckboxType::class, array('label' => 'Production?', 'required' => false));

        $builder->get('storeName')->setData($settings->getGatewayDocument()->getStoreName());
        $builder->get('secret')->setData($settings->getGatewayDocument()->getSecret());
        $builder->get('production')->setData($settings->getGatewayDocument()->isProduction());

        return $builder;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCTelecash/settings.html.twig';
    }

    public function handlePayment(Request $request)
    {
        $txndatetime = date("Y:m:d-H:i:s");

        $content = ''
            . '<form action="https://www.ipg-online.com/connect/gateway/processing">'
            . '<input type="hidden" name="chargetotal" value="'.$request->get('amount').'"/>'
            . '<input type="hidden" name="currency" value="978"/>'
            . '<input type="hidden" name="oid" value="'.$request->get('token').'"/>'
            . '<input type="hidden" name="responseSuccessURL" value="'.($this->getHost().'/basket/finish?Data=finish&ARTID=' . $request->getSession()->getId() . '&token='.$request->get('hash')).'"/>'
            . '<input type="hidden" name="responseFailURL" value="'.($this->getHost().'/basket/finish?error=Fehler&ARTID='.$request->getSession()->getId()).'"/>'
            . '<input type="hidden" name="timezone" value="CET"/>'
            . '<input type="hidden" name="storename" value="'.$this->getGatewaySettings()->getGatewayDocument()->getStoreName().'"/>'
            . '<input type="hidden" name="txntype" value="sale"/>'
            . '<input type="hidden" name="txndatetime" value="'.$txndatetime.'"/>'
            . '<input type="hidden" name="hash_algorithm" value="SHA256"/>'
            . '<input type="hidden" name="hash" value="'.$this->createHash($this->getGatewaySettings()->getGatewayDocument()->getStoreName(), $txndatetime, $request->get('amount'), 978, $this->getGatewaySettings()->getGatewayDocument()->getSecret()).'"/>'
            . '</form>'
            . '<script language="javascript">'
            . 'window.document.forms[0].submit()'
            . '</script>';

        $response = new \Symfony\Component\HttpFoundation\Response($content);
        return $response;


    }

    public function handleNotify(Request $request)
    {

        return;

    }

    private function createHash($storeName, $txndatetime, $chargetotal, $currency, $sharedSecret) {
        $stringToHash = $storeName . $txndatetime . $chargetotal . $currency . $sharedSecret;
        $ascii = bin2hex($stringToHash);
        return hash('sha256', $ascii);
	}
}
