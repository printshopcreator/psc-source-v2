<?php
namespace Plugin\System\PSC\Ups\Package;

use PSC\Shop\OrderBundle\Model\Order;
use PSC\Shop\OrderBundle\Model\Order\Position;
use PSC\System\PluginBundle\Package\Type;

class Label implements Type
{

    public function getId()
    {
        return 'plugin_system_psc_up_label';
    }


    public function addFileToPositionArchive(\ZipArchive $archive, Order $order, Position $position, string $path)
    {

    }

    public function addFilesToOrderArchive(\ZipArchive $archive, Order $order)
    {
        if (file_exists('/data/www/old/market/export/' . $order->getShop() . '/' . $order->getAlias() . '_ups.gif')) {
            $archive->addFile('/data/www/old/market/export/' . $order->getShop() . '/' . $order->getAlias() . '_ups.gif', $order->getAlias() . '_ups.gif');
        }
    }
}