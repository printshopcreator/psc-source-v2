<?php
namespace Plugin\System\PSC\Ups\Shipping;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Exception;
use Plugin\System\PSC\Ups\Document\Ups;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use PSC\Shop\ShippingBundle\Provider\ShippingProvider;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Twig\Environment;
use Ups\Entity\Address;
use Ups\Entity\BillShipper;
use Ups\Entity\Dimensions;
use Ups\Entity\Package;
use Ups\Entity\PackagingType;
use Ups\Entity\PaymentInformation;
use Ups\Entity\Prepaid;
use Ups\Entity\ReferenceNumber;
use Ups\Entity\Service;
use Ups\Entity\ShipFrom;
use Ups\Entity\Shipment;
use Ups\Entity\ShipmentRequestLabelSpecification;
use Ups\Entity\UnitOfMeasurement;
use Ups\Response;
use Ups\Shipping;

class Provider extends ShippingProvider
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_template = null;
    private $_error = null;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, Environment $template)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_template = $template;
    }

    public function getName()
    {
        return 'Ups';
    }

    public function getType()
    {
        return 'ups';
    }

    public function saveDocument(ShippingGatewaySettings $settings, Form $form)
    {
        $doc = new Ups();
        $doc->setProduction($form->get('ups')->get('production')->getData());
        $doc->setApiKey($form->get('ups')->get('apiKey')->getData());
        $doc->setUsername($form->get('ups')->get('username')->getData());
        $doc->setPassword($form->get('ups')->get('password')->getData());
        $doc->setKundenNr($form->get('ups')->get('kundenNr')->getData());

        $doc->setShipFromZip($form->get('ups')->get('shipFromZip')->getData());
        $doc->setShipFromCity($form->get('ups')->get('shipFromCity')->getData());
        $doc->setShipFromAddress($form->get('ups')->get('shipFromAddress')->getData());
        $doc->setShipFromCountry($form->get('ups')->get('shipFromCountry')->getData());
        $doc->setShipFromCompany($form->get('ups')->get('shipFromCompany')->getData());
        $doc->setShipFromPhone($form->get('ups')->get('shipFromPhone')->getData());

        $doc->setStandardWeight($form->get('ups')->get('standardWeight')->getData());

        $doc->setUpsStandard($form->get('ups')->get('upsStandard')->getData());
        $doc->setUpsExpress($form->get('ups')->get('upsExpress')->getData());
        $doc->setUpsExpressPlus($form->get('ups')->get('upsExpressPlus')->getData());
        $doc->setUpsExpressSaver($form->get('ups')->get('upsExpressSaver')->getData());

        $settings->setGatewayDocument($doc);

        return $settings;
    }

    public function getSubForm(ShippingGatewaySettings $settings, FormBuilder $builder)
    {

        if(!$settings->getGatewayDocument()) {
            $settings->setGatewayDocument(new Ups());
        }

        $builder
            ->add('production', CheckboxType::class, array('label' => 'Produktions?', 'required' => false))
            ->add('kundenNr', TextType::class, array('label' => 'KundenNr', 'required' => false))
            ->add('username', TextType::class, array('label' => 'Benutzername', 'required' => false))
            ->add('password', TextType::class, array('label' => 'Passwort', 'required' => false))
            ->add('apiKey', TextType::class, array('label' => 'ApiKey', 'required' => false))

            ->add('shipFromZip', TextType::class, array('label' => 'Absender PLZ', 'required' => false))
            ->add('shipFromCity', TextType::class, array('label' => 'Absender Ort', 'required' => false))
            ->add('shipFromAddress', TextType::class, array('label' => 'Absender Adresse', 'required' => false))
            ->add('shipFromCountry', TextType::class, array('label' => 'Absender Land', 'required' => false))
            ->add('shipFromPhone', TextType::class, array('label' => 'Absender Telefon', 'required' => false))
            ->add('shipFromCompany', TextType::class, array('label' => 'Absender Firma', 'required' => false))

            ->add('standardWeight', IntegerType::class, array('label' => 'Standardgewicht (gr.)', 'required' => false))

            ->add('upsStandard', EntityType::class, array(
                'class' => 'PSCEntityBundle:Shipping',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.shop = :shop')
                        ->setParameter('shop', $this->shopEntity)
                        ->orderBy('s.title', 'ASC');
                },
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'label' => 'Ups Standard', 'required' => false))

            ->add('upsExpress', EntityType::class, array(
                'class' => 'PSCEntityBundle:Shipping',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.shop = :shop')
                        ->setParameter('shop', $this->shopEntity)
                        ->orderBy('s.title', 'ASC');
                },
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'label' => 'Ups Express', 'required' => false))

            ->add('upsExpressPlus', EntityType::class, array(
                'class' => 'PSCEntityBundle:Shipping',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.shop = :shop')
                        ->setParameter('shop', $this->shopEntity)
                        ->orderBy('s.title', 'ASC');
                },
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'label' => 'Ups Express Plus', 'required' => false))


            ->add('upsExpressSaver', EntityType::class, array(
                'class' => 'PSCEntityBundle:Shipping',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('s')
                        ->where('s.shop = :shop')
                        ->setParameter('shop', $this->shopEntity)
                        ->orderBy('s.title', 'ASC');
                },
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'label' => 'Ups Express Saver', 'required' => false))
        ;

        $em = $this->_entityManager;

        $builder->get('upsStandard')->addModelTransformer(new CallbackTransformer(
            function ($obj) use ($em) {
                if($obj) {
                    $tempObj = $em->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')->findOneByUid($obj);
                    return $tempObj;
                }
            },
            function ($obj) {
                if($obj) {
                    return $obj->getUid();
                }
            }
        ));
        $builder->get('upsExpress')->addModelTransformer(new CallbackTransformer(
            function ($obj) use ($em) {
                if($obj) {
                    $tempObj = $em->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')->findOneByUid($obj);
                    return $tempObj;
                }
            },
            function ($obj) {
                if($obj) {
                    return $obj->getUid();
                }
            }
        ));
        $builder->get('upsExpressPlus')->addModelTransformer(new CallbackTransformer(
            function ($obj) use ($em) {
                if($obj) {
                    $tempObj = $em->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')->findOneByUid($obj);
                    return $tempObj;
                }
            },
            function ($obj) {
                if($obj) {
                    return $obj->getUid();
                }
            }
        ));
        $builder->get('upsExpressSaver')->addModelTransformer(new CallbackTransformer(
            function ($obj) use ($em) {
                if($obj) {
                    $tempObj = $em->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')->findOneByUid($obj);
                    return $tempObj;
                }
            },
            function ($obj) {
                if($obj) {
                    return $obj->getUid();
                }
            }
        ));


        $builder->get('production')->setData($settings->getGatewayDocument()->isProduction());
        $builder->get('kundenNr')->setData($settings->getGatewayDocument()->getKundenNr());
        $builder->get('username')->setData($settings->getGatewayDocument()->getUsername());
        $builder->get('password')->setData($settings->getGatewayDocument()->getPassword());
        $builder->get('apiKey')->setData($settings->getGatewayDocument()->getApiKey());

        $builder->get('shipFromZip')->setData($settings->getGatewayDocument()->getShipFromZip());
        $builder->get('shipFromCity')->setData($settings->getGatewayDocument()->getShipFromCity());
        $builder->get('shipFromAddress')->setData($settings->getGatewayDocument()->getShipFromAddress());
        $builder->get('shipFromCountry')->setData($settings->getGatewayDocument()->getShipFromCountry());
        $builder->get('shipFromCompany')->setData($settings->getGatewayDocument()->getShipFromCompany());
        $builder->get('shipFromPhone')->setData($settings->getGatewayDocument()->getShipFromPhone());

        $builder->get('standardWeight')->setData($settings->getGatewayDocument()->getStandardWeight());

        $builder->get('upsStandard')->setData($settings->getGatewayDocument()->getUpsStandard());
        $builder->get('upsExpress')->setData($settings->getGatewayDocument()->getUpsExpress());
        $builder->get('upsExpressPlus')->setData($settings->getGatewayDocument()->getUpsExpressPlus());
        $builder->get('upsExpressSaver')->setData($settings->getGatewayDocument()->getUpsExpressSaver());

        return $builder;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCUps/settings.html.twig';
    }

    public function handleShipping(Order $order)
    {

        $apiKey = $this->getShippingGatewaySettings()->getGatewayDocument()->getApiKey();
        $kdNr = $this->getShippingGatewaySettings()->getGatewayDocument()->getKundenNr();

        $twigApiKey = $this->_template->createTemplate($apiKey);
        $twigKdNr = $this->_template->createTemplate($kdNr);

        /** @var Orderpos $pos */
        $pos = $order->getPositions()[0];
        $originalProductId = $pos->getProduct()->getUID();
        if($pos->getProduct()->getOriginalProduct() != "" && $pos->getProduct()->getOriginalProduct() != 0) {
            $originalProductId = $pos->getProduct()->getOriginalProduct();
        }

        $params = array(
            'order' => $order,
            'firstOriginalProductId' => $originalProductId
        );

        $apiKey = $twigApiKey->render(
            $params
        );

        $kdNr = $twigKdNr->render(
            $params
        );


        $shipping = new Shipping(
            $apiKey,
            $this->getShippingGatewaySettings()->getGatewayDocument()->getUsername(),
            $this->getShippingGatewaySettings()->getGatewayDocument()->getPassword(),
            !$this->getShippingGatewaySettings()->getGatewayDocument()->isProduction());

        try {
            $shipment = new Shipment();

            $shipper = $shipment->getShipper();
            $shipper->setShipperNumber($kdNr);
            $shipper->setName($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromCompany());
            $shipper->setPhoneNumber($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromPhone());
            $shipper->setAttentionName($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromCompany());

            $shipperAddress = $shipment->getShipper()->getAddress();
            $shipperAddress->setPostalCode($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromZip());
            $shipperAddress->setAddressLine1($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromAddress());
            $shipperAddress->setCity($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromCity());
            $shipperAddress->setCountryCode($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromCountry());

            $address = new Address();
            $address->setPostalCode($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromZip());
            $address->setAddressLine1($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromAddress());
            $address->setCity($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromCity());
            $address->setCountryCode($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromCountry());
            $shipFrom = new ShipFrom();
            $shipFrom->setAddress($address);
            $shipFrom->setCompanyName($this->getShippingGatewaySettings()->getGatewayDocument()->getShipFromCompany());
            
            $shipment->setShipFrom($shipFrom);

            $shipTo = $shipment->getShipTo();
            $shipTo->setAttentionName($order->getDeliveryAddress()->getFirstname() . ' ' . $order->getDeliveryAddress()->getLastname());
            $shipTo->setReceivingAddressName($order->getDeliveryAddress()->getFirstname() . ' ' . $order->getDeliveryAddress()->getLastname());
            $shipTo->setPhoneNumber($order->getDeliveryAddress()->getPhone());
            $shipTo->setEmailAddress($order->getDeliveryAddress()->getEmail());

            if($order->getDeliveryAddress()->getCompany() != "") {
                $shipTo->setCompanyName($order->getDeliveryAddress()->getCompany());
            }else{
                $shipTo->setCompanyName($order->getDeliveryAddress()->getFirstname() . ' ' . $order->getDeliveryAddress()->getLastname());
            }

            $shipToAddress = $shipTo->getAddress();
            $shipToAddress->setPostalCode($order->getDeliveryAddress()->getZip());
            $shipToAddress->setAddressLine1($order->getDeliveryAddress()->getStreet() . ' ' . $order->getDeliveryAddress()->getHouseNumber());

            $shipToAddress->setCity($order->getDeliveryAddress()->getCity());
            $shipToAddress->setCountryCode($order->getDeliveryAddress()->getCountry());

            $shipment->setShipTo($shipTo);

            $unitofweight = new UnitOfMeasurement;
            $unitofweight->setCode(UnitOfMeasurement::UOM_KGS);

            $package = new Package();
            $package->getPackagingType()->setCode(PackagingType::PT_PACKAGE);

            $weight = $order->getWeight();

            if($weight < 1) {
                $package->getPackageWeight()->setWeight($this->getShippingGatewaySettings()->getGatewayDocument()->getStandardWeight()/1000);
            }else{
                $package->getPackageWeight()->setWeight($order->getWeight()/1000);
            }

            $package->getPackageWeight()->setUnitOfMeasurement($unitofweight);
            
            $shipment->addPackage($package);
            $shipment->setDescription("Shop");

            $referencenumber = new ReferenceNumber;
            $referencenumber->setValue($order->getAlias());
            $shipment->setReferenceNumber($referencenumber);

            $service = new Service();

            if($order->getShippingType()->getUid() == $this->getShippingGatewaySettings()->getGatewayDocument()->getUpsStandard()) {
                $service->setCode(Service::S_STANDARD);
            }elseif($order->getShippingType()->getUid() == $this->getShippingGatewaySettings()->getGatewayDocument()->getUpsExpress()) {
                $service->setCode(Service::S_WW_EXPRESS);
            }elseif($order->getShippingType()->getUid() == $this->getShippingGatewaySettings()->getGatewayDocument()->getUpsExpressPlus()) {
                $service->setCode(Service::S_WW_EXPRESSPLUS);
            }elseif($order->getShippingType()->getUid() == $this->getShippingGatewaySettings()->getGatewayDocument()->getUpsExpressSaver()) {
                $service->setCode(Service::S_SAVER);
            }else{
                $service->setCode(Service::S_STANDARD);
            }
            
            $shipment->setService($service);


            $billshipper= new BillShipper();
            $billshipper->setAccountNumber($kdNr);
            $prepaid=new Prepaid();
            $prepaid->setBillShipper($billshipper);

            $paymentInformation = new PaymentInformation;
            $paymentInformation->setPrepaid($prepaid);

            $shipment->setPaymentInformation($paymentInformation);

            $result = $shipping->confirm('nonvalidate', $shipment);

            $shipment_charges = $result->ShipmentCharges->TotalCharges->MonetaryValue;
            $shipment_weight = $result->BillingWeight->Weight." ".$result->BillingWeight->UnitOfMeasurement->Code;
            $tracking_number = $result->ShipmentIdentificationNumber;
            $shipment_digest = $result->ShipmentDigest;

            $accept = $shipping->accept($shipment_digest);

            //$tracking_number = $accept->PackageResults->TrackingNumber;
            $imageformat = $accept->PackageResults->LabelImage->LabelImageFormat->Code;

            $base64image = $accept->PackageResults->LabelImage->GraphicImage;

            return array(
                'trackingNumber' => $tracking_number,
                'image' => $base64image
            );

        } catch (Exception $e) {
            var_dump($e->getMessage());
        }

    }

    public function handleNotify(Request $request)
    {


    }
}
