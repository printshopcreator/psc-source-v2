<?php
namespace Plugin\System\PSC\Ups\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Ups
{
    /**
     * @var boolean $production
     *
     * @Field(type="bool")
     */
    protected $production;

    /**
     * @var string $apiKey
     *
     * @Field(type="string")
     */
    protected $apiKey;

    /**
     * @var string $username
     *
     * @Field(type="string")
     */
    protected $username;

    /**
     * @var string $username
     *
     * @Field(type="string")
     */
    protected $password;

    /**
     * @var string $kundenNr
     *
     * @Field(type="string")
     */
    protected $kundenNr;

    /**
     * @var string $shipFromZip
     *
     * @Field(type="string")
     */
    protected $shipFromZip;

    /**
     * @var string $shipFromZip
     *
     * @Field(type="string")
     */
    protected $shipFromCity;

    /**
     * @var string $shipFromZip
     *
     * @Field(type="string")
     */
    protected $shipFromAddress;

    /**
     * @var string $shipFromZip
     *
     * @Field(type="string")
     */
    protected $shipFromCountry;

    /**
     * @var string $shipFromZip
     *
     * @Field(type="string")
     */
    protected $shipFromCompany;

    /**
     * @var string $shipFromPhone
     *
     * @Field(type="string")
     */
    protected $shipFromPhone;

    /**
     * @var string $upsStandard
     *
     * @Field(type="int")
     */
    protected $upsStandard;

    /**
     * @var string $upsExpress
     *
     * @Field(type="int")
     */
    protected $upsExpress;

    /**
     * @var string $upsExpressPlus
     *
     * @Field(type="int")
     */
    protected $upsExpressPlus;

    /**
     * @var string $upsExpressSaver
     *
     * @Field(type="int")
     */
    protected $upsExpressSaver;

    /**
     * @var string $standardWeight
     *
     * @Field(type="int")
     */
    protected $standardWeight;

    /**
     * Get the value of Production
     *
     * @return boolean
     */
    public function isProduction()
    {
        return $this->production;
    }

    /**
     * Set the value of Production
     *
     * @param boolean $production production
     *
     * @return self
     */
    public function setProduction($production)
    {
        $this->production = $production;

        return $this;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }

    /**
     * @return string
     */
    public function getShipFromZip()
    {
        return $this->shipFromZip;
    }

    /**
     * @param string $shipFromZip
     */
    public function setShipFromZip($shipFromZip)
    {
        $this->shipFromZip = $shipFromZip;
    }

    /**
     * @return string
     */
    public function getShipFromCity()
    {
        return $this->shipFromCity;
    }

    /**
     * @param string $shipFromCity
     */
    public function setShipFromCity($shipFromCity)
    {
        $this->shipFromCity = $shipFromCity;
    }

    /**
     * @return string
     */
    public function getShipFromAddress()
    {
        return $this->shipFromAddress;
    }

    /**
     * @param string $shipFromAddress
     */
    public function setShipFromAddress($shipFromAddress)
    {
        $this->shipFromAddress = $shipFromAddress;
    }

    /**
     * @return string
     */
    public function getShipFromCountry()
    {
        return $this->shipFromCountry;
    }

    /**
     * @param string $shipFromCountry
     */
    public function setShipFromCountry($shipFromCountry)
    {
        $this->shipFromCountry = $shipFromCountry;
    }

    /**
     * @return string
     */
    public function getShipFromCompany()
    {
        return $this->shipFromCompany;
    }

    /**
     * @param string $shipFromCompany
     */
    public function setShipFromCompany($shipFromCompany)
    {
        $this->shipFromCompany = $shipFromCompany;
    }

    /**
     * @return string
     */
    public function getShipFromPhone()
    {
        return $this->shipFromPhone;
    }

    /**
     * @param string $shipFromPhone
     */
    public function setShipFromPhone($shipFromPhone)
    {
        $this->shipFromPhone = $shipFromPhone;
    }

    /**
     * @return string
     */
    public function getUpsExpress()
    {
        return $this->upsExpress;
    }

    /**
     * @param string $upsExpress
     */
    public function setUpsExpress($upsExpress)
    {
        $this->upsExpress = $upsExpress;
    }

    /**
     * @return string
     */
    public function getUpsStandard()
    {
        return $this->upsStandard;
    }

    /**
     * @param string $upsStandard
     */
    public function setUpsStandard($upsStandard)
    {
        $this->upsStandard = $upsStandard;
    }

    /**
     * @return string
     */
    public function getUpsExpressPlus()
    {
        return $this->upsExpressPlus;
    }

    /**
     * @param string $upsExpressPlus
     */
    public function setUpsExpressPlus($upsExpressPlus)
    {
        $this->upsExpressPlus = $upsExpressPlus;
    }

    /**
     * @return string
     */
    public function getUpsExpressSaver()
    {
        return $this->upsExpressSaver;
    }

    /**
     * @param string $upsExpressSaver
     */
    public function setUpsExpressSaver($upsExpressSaver)
    {
        $this->upsExpressSaver = $upsExpressSaver;
    }

    /**
     * @return string
     */
    public function getStandardWeight()
    {
        return $this->standardWeight;
    }

    /**
     * @param string $standardWeight
     */
    public function setStandardWeight($standardWeight)
    {
        $this->standardWeight = $standardWeight;
    }


}
