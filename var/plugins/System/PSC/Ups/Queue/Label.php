<?php
namespace Plugin\System\PSC\Ups\Queue;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use Plugin\System\PSC\Upload\Helper\UploaderHelper;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Order\Create;
use PSC\Shop\QueueBundle\Event\Package\Created;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use PSC\Shop\ShippingBundle\Provider\ShippingProvider;
use PSC\Shop\ShippingBundle\Service\Registry;
use PSC\System\SettingsBundle\Service\Log;
use PSC\System\SettingsBundle\Service\Printing;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\TemplateVars;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mailer\MailerInterface;

class Label implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private Shop $_shopService;
    private Registry $_shippingProvider;
    private \PSC\Shop\OrderBundle\Service\Order $_orderService;


    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, Shop $shopService, Registry $shippingProvider, \PSC\Shop\OrderBundle\Service\Order $orderService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_shopService = $shopService;
        $this->_shippingProvider = $shippingProvider;
        $this->_orderService = $orderService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'plugin_system_psc_ups_label';
    }

    public function getDescription()
    {
        return 'Generate Label and Packagenumber';
    }

    public function getGroup()
    {
        return 'Allgemeines';
    }

    public function getName()
    {
        return 'UPS Label';
    }

    protected function getQueueDocument()
    {

    }

    public function getForm(FormBuilderInterface $builder, $form_options, EventInterface $event)
    {


    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {

    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {

    }


    public function getTemplate()
    {

    }

    /**
     * @param EventInterface $event
     * @param Queue $doc
     * @return bool
     */
    public function execute(EventInterface $event, Queue $queue)
    {
        if($event instanceof \PSC\Shop\QueueBundle\Event\Order\Status\Change) {
            $order = $this->_orderService->getOrderByUuid($event->getOrder());
            if($order->getStatus() == $event->getStatus()) {
                $this->storeShipping($order);
            }
        }

        if($event instanceof Create) {
            $order = $this->_orderService->getOrderByUuid($event->getOrder());
            $this->storeShipping($order);
        }

        return true;
    }

    protected function storeShipping(\PSC\Shop\OrderBundle\Model\Order $order)
    {
        /** @var ShippingProvider $provider */
        $provider = $this->_shippingProvider->get("ups");

        /** @var Order $order */
        $order = $this->_entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('uuid' => $order->getUuid()));

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $shopEntity */
        $shopEntity = $order->getShop();

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shopDoc */
        $shopDoc = $this->_doctrine_mongodb
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => $shopEntity->getUID()));



        /** @var ShippingGatewaysettings $gwSettings */
        $gwSettings = $this->_doctrine_mongodb
            ->getRepository('PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings')
            ->findOneBy(array('shop' => (string)$shopEntity->getId(), 'gatewayType' => 'ups'));

        $provider->setShopEntity($shopEntity);
        $provider->setShopDoc($shopDoc);
        $provider->setShippingGatewaySettings($gwSettings);
        $data = $provider->handleShipping($order);

        if (isset($data['trackingNumber'])) {
            $order->setPackage($data['trackingNumber']);
            $this->_entityManager->persist($order);
            $this->_entityManager->flush();

        }
        if (isset($data['image'])) {
            $path = '/data/www/old/market/export/';
            if (!file_exists($path)) {
                mkdir($path);
            }
            $path = '/data/www/old/market/export/' . $shopEntity->getUid() . '/';

            if (!file_exists($path)) {
                mkdir($path);
            }

            file_put_contents($path.$order->getAlias().'_ups.gif', base64_decode($data['image']));
        }

    }

    public function getError()
    {
        return $this->_error;
    }
}
