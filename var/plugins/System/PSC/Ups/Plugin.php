<?php
namespace Plugin\System\PSC\Ups;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Ups Shipping Gateway';

    public function getType()
    {
        return Plugin::Shipping;
    }

    public function getDescription()
    {
        return 'Stellt Unterstützung für UPS bereit';
    }

    public function getVersion()
    {
        return 1;
    }
}
