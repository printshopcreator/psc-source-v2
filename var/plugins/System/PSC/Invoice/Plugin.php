<?php
namespace Plugin\System\PSC\Invoice;

use PSC\System\PluginBundle\Plugin\Base;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'invoice / offer module';

    public function getType()
    {
        return Plugin::Backend;
    }

    public function getDescription()
    {
        return 'invoice offer module';
    }

    public function getVersion()
    {
        return 1;
    }


}