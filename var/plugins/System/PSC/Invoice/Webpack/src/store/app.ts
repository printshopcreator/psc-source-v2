import create from 'zustand'
import { devtools, persist } from 'zustand/middleware'

interface OrderState {
  contact: number,
  payment: number,
  shippment: number,
}

const orderStore = create<OrderState>()(
  devtools(
    persist(
      (set) => ({
        contact: null,
        payment: null,
        shippment: null
      }),
      {
        name: 'order-storage',
      }
    )
  )
)

export default orderStore;