import { Order } from '../../state/order';
import {container} from "tsyringe";
import { OrderService } from '../../services/order';
import TypeSelectComponent from '../type/TypeSelectComponent';
import ShopSelectComponent from '../shop/ShopSelectComponent';
import * as React from 'react';

const TopBarComponent = (props) => {
    
    const orderService: OrderService = container.resolve(OrderService);
    const order: Order = container.resolve(Order);

    const handleSave = (e) => {
        e.preventDefault();

        orderService.saveOrder(order);
    };

    return (
        <div className='row'>
            <div className='col-4'>
                <h4>Typ</h4> <TypeSelectComponent />
            </div>
            <div className='col-4'>
                <h4>Shop</h4> <ShopSelectComponent shop={props.shop} change={props.change} />
            </div>
            <div className='col-4'>
                <button className='btn btn-success' onClick={handleSave}>
                    Save Order
                </button>
            </div>
        </div>
    );  
} 

export default TopBarComponent;