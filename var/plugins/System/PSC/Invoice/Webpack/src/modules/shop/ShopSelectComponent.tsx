import * as React from 'react';
import { AsyncPaginate } from "react-select-async-paginate";
import { ShopService } from '../../services/shop';
import { container } from 'tsyringe';
import { Order } from '../../state/order';

const ShopSelectComponent = (props) => {
    
    const order: Order = container.resolve(Order);

    const loadOptions = async (searchQuery, loadedOptions) => {

        const shop_service = new ShopService();
    
        const data = await shop_service.getShops();
    
        return {
          options: data,
          hasMore: false,
        }; 
      };
    

    const onChange = (option) => {
        order.shop = option.id;
        props.change(option);
      };

    return (
        <div>
             <AsyncPaginate
                defaultOptions
                loadOptions={loadOptions}
                getOptionValue={(option) => option.id}
                getOptionLabel={(option) => option.name}
                onChange={onChange}
                isSearchable={false}
            />
        </div>
    );  
} 

export default ShopSelectComponent;