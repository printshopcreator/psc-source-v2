import * as PropTypes from 'prop-types';
import { Shop } from '../../model/shop';
import { container } from 'tsyringe';
import { Order } from '../../state/order';
import {Product} from "../../model/product";
import {ProductGroup} from "../../model/productGroup";
import AsyncSelect from "react-select/async";
import {ProductService} from "../../services/product";

function debounce(inner, ms = 300) {
  let timer = null;
  let resolves = [];

  return function (...args) {
    // Run the function after a certain amount of time
    clearTimeout(timer);
    timer = setTimeout(() => {
      // Get the result of the inner function, then apply it to the resolve function of
      // each promise that has been created since the last time the inner function was run
      let result = inner(...args);
      resolves.forEach(r => r(result));
      resolves = [];
    }, ms);

    return new Promise(r => resolves.push(r));
  };
}

const ProductSelect = (props) => {

  const order: Order = container.resolve(Order);

  const loadOptions = debounce(async (searchQuery) => {
    if(searchQuery == "" && props.productGroup.uuid == "") {
      return [];
    }
    const product_api = new ProductService();
    if(searchQuery != "") {
      return await product_api.getProductsByTerm(searchQuery, props.shop);
    }else{
      return await product_api.getProducts(props.productGroup);
    }
  });

  const onChange = (option) => {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }
  };

  return (
      <AsyncSelect
          defaultOptions
          key={JSON.stringify(props.productGroup.uuid)}
          value={props.value}
          loadOptions={loadOptions}
          getOptionValue={(option) => option.uuid}
          getOptionLabel={(option) => option.title}
          onChange={onChange}
          isSearchable={true}
      />
  );
};

ProductSelect.propTypes = {
  shop: PropTypes.instanceOf(Shop),
  value: PropTypes.instanceOf(Product),
  productGroup: PropTypes.instanceOf(ProductGroup),
  onChange: PropTypes.func,
};

export default ProductSelect;