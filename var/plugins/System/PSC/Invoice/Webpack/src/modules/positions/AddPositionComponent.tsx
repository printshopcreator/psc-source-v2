import * as React from 'react';
import {useEffect, useState} from 'react';
import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import * as PropTypes from "prop-types";
import { Shop } from '../../model/shop';
import {Product} from "../../model/product";
import {ProductGroup} from "../../model/productGroup";
import ProductGroupSelect from "../product/ProductGroupSelect";
import ProductSelect from "../product/ProductSelect";
import ProductForm from "../product/ProductForm";

const AddPositionComponent = (props) => {

    const [shop, setShop] = useState(null);

    useEffect(() => {
        setShop(props.shop);
    }, [props.shop]);

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [product, setProduct] = useState<Product>(new Product());
    const [productGroup, setProductGroup] = useState<ProductGroup>(new ProductGroup());

    return (
        <div>
            <Button variant="primary" onClick={handleShow}>
                add position
            </Button>

            <Modal show={show} fullscreen={true} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>add position</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className='row'>
                        <div className='col-6'>
                            <h4>ProductGroup</h4>
                            <ProductGroupSelect
                                value={productGroup}
                                shop={shop}
                                onChange={setProductGroup}
                            />
                        </div>
                        <div className='col-6'>
                            <h4>Product</h4>
                            <ProductSelect
                                value={product}
                                shop={shop}
                                productGroup={productGroup}
                                onChange={setProduct}
                            />
                        </div>
                    </div>

                    <ProductForm
                        product={product}
                    />
                </Modal.Body>
            </Modal>
        </div>
    );
}

AddPositionComponent.propTypes = {
    shop: PropTypes.instanceOf(Shop),
};


export default AddPositionComponent;