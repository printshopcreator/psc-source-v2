import * as PropTypes from 'prop-types';
import { Shop } from '../../model/shop';
import {Product} from "../../model/product";
import validator from "@rjsf/validator-ajv6";
import { JSONSchema7 } from "json-schema";
import Form from "@rjsf/core";
import {ProductService} from "../../services/product";
import {useEffect, useState} from "react";

const ProductForm = (props) => {

    const [schema, setSchema] = useState<JSONSchema7>({});
    const loadSchema = () => {
        if(props.product.uuid == "") {
            return [];
        }
        const product_api = new ProductService();
        product_api.getAddProductSchema(props.product).then((value) => {
            setSchema(value['jsonForm']);
        })
    };

    useEffect(() => {
        loadSchema();
    }, [props.product]);

    const log = (type) => console.log.bind(console, type);

    return (
        <>
            <div className='row'>
                <div className='col-6'>
                    <Form schema={schema}
                          validator={validator}
                          onChange={log("changed")}
                          onSubmit={log("submitted")}
                          onError={log("errors")} />
                </div>
                <div className='col-6'>
                    <h5>Netto: </h5>
                    <h5>Mwert: </h5>
                    <h4>Brutto</h4>
                </div>
            </div>

        </>
    );
};

ProductForm.propTypes = {
    shop: PropTypes.instanceOf(Shop),
    product: PropTypes.instanceOf(Product),
};

export default ProductForm;