import { AsyncPaginate } from "react-select-async-paginate";
import { AccountService } from '../../services/account';
import { container } from 'tsyringe';
import { Order } from '../../state/order';
import {useEffect, useState} from 'react';
import * as PropTypes from 'prop-types';
import { Shop } from '../../model/shop';

const AccountSelectComponent = (props) => {
      
    const [shopUuid, setShopUuid] = useState(null);

    useEffect(() => {
      setShopUuid(props.shop.uuid);
    }, [props.shop.uuid]);

    const order: Order = container.resolve(Order);

    const loadOptions = async (searchQuery, loadedOptions) => {

        const account_service = new AccountService();
    
        const data = await account_service.getAccounts(props.shop);
    
        return {
          options: data,
          hasMore: false,
        }; 
      };
    

    const onChange = (option) => {
        order.account = option;
    };

    return (
        <div>
             <AsyncPaginate
                defaultOptions
                key={JSON.stringify(shopUuid)}
                loadOptions={loadOptions}
                getOptionValue={(option) => option.uuid}
                getOptionLabel={(option) => option.title}
                onChange={onChange}
                isSearchable={true}
            />
        </div>
    );  
} 

AccountSelectComponent.propTypes = {
  shop: PropTypes.instanceOf(Shop),
};

export default AccountSelectComponent;