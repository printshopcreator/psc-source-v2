import Select from 'react-select';
import {useState} from 'react';
import { container } from 'tsyringe';
import { Order } from '../../state/order';

const TypeSelectComponent = (props) => {
    
    const [type, setType] = useState();
    const order: Order = container.resolve(Order);

    const options = [
        { value: 1, label: 'Auftrag' },
        { value: 2, label: 'Angebot' },
        { value: 3, label: 'Storno' },
        { value: 4, label: 'Gutschrift' },
        { value: 5, label: 'Rechnung' },
        { value: 6, label: 'Teilrechnung' },
        { value: 7, label: 'Teilstorno' },
    ];

    const onChange = (option) => {
        order.type = option.value;
    };

    return (
        <div>
            <Select 
                options={options} 
                onChange={onChange}
            />
        </div>
    );  
} 

export default TypeSelectComponent;