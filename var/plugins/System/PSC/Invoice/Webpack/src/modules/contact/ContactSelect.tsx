import * as PropTypes from 'prop-types';
import { AsyncPaginate } from "react-select-async-paginate";
import { Contact } from '../../model/contact';
import {ContactService} from '../../services/contact';
import { Shop } from '../../model/shop';
import {useEffect, useState} from 'react';
import { container } from 'tsyringe';
import { Order } from '../../state/order';

const ContactSelect = (props) => {

  const [shopUuid, setShopUuid] = useState(null);

  useEffect(() => {
    setShopUuid(props.shop.uuid);
  }, [props.shop.uuid]);

  const order: Order = container.resolve(Order);

  const loadOptions = async (searchQuery, loadedOptions, { page }) => {

    const contact_api = new ContactService();

    let contacts = await contact_api.getContacts(searchQuery, page, props.shop);

    return {
      options: contacts,
      hasMore: false,
      additional: {
        page: page + 1,
      },
    }; 
  };

  const onChange = (option) => {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }
    order.contact = option;
  };

  return (
    <AsyncPaginate
      defaultOptions
      key={JSON.stringify(shopUuid)}
      value={props.value}
      loadOptions={loadOptions}
      getOptionValue={(option) => option.uuid}
      getOptionLabel={(option) => option.username}
      onChange={onChange}
      isSearchable={true}
      additional={{
        page: 1,
      }}
    />
  );
};

ContactSelect.propTypes = {
  value: PropTypes.instanceOf(Contact),
  onChange: PropTypes.func,
  shop: PropTypes.instanceOf(Shop),
};

export default ContactSelect;