import * as React from 'react';
import orderStore from '../../store/app';
import ContactSelect from './ContactSelect';
import ContactAddressSelect from './ContactAddressSelect';
import {Contact} from '../../model/contact';
import {Address} from '../../model/address';
import {useEffect, useState} from 'react';
import { Order } from '../../state/order'
import {container} from "tsyringe";
import AccountSelectComponent from '../account/AccountSelectComponent';

const ContactComponent = (props) => {

    let orderState = container.resolve(Order);

    const [contact, setContact] = useState<Contact>(new Contact());
    const [contactAddressInvoice, setContactAddressInvoice] = useState<Address>(new Address());
    const [contactAddressDelivery, setContactAddressDelivery] = useState<Address>(new Address());
    const [contactAddressSender, setContactAddressSender] = useState<Address>(new Address());
    
    orderState.deliveryAddress = contactAddressDelivery;
    orderState.invoiceAddress = contactAddressInvoice;
    orderState.senderAddress = contactAddressSender;
    return (
        <div>
            <div className='row'>
                <div className='col-6'>
                    <h4>Contact</h4>
                    <ContactSelect
                        value={contact}
                        shop={props.shop}
                        onChange={setContact}
                    />
                </div>
                <div className='col-6'>
                    <h4>Account</h4>
                    <AccountSelectComponent
                        shop={props.shop}
                    />
                </div>
            </div>
            
            <div className='row'>
                <div className='col-4'>
                    <h4>Invoice</h4>
                        <ContactAddressSelect
                            value={contactAddressInvoice}
                            contact={contact}
                            type={1}
                            onChange={setContactAddressInvoice}
                        />
                </div>
                <div className='col-4'>
                    <h4>Delivery</h4>
                        <ContactAddressSelect
                            value={contactAddressDelivery}
                            contact={contact}
                            type={2}
                            onChange={setContactAddressDelivery}
                        />
                </div>
                <div className='col-4'>
                    <h4>Sender</h4>
                        <ContactAddressSelect
                            value={contactAddressSender}
                            contact={contact}
                            type={3}
                            onChange={setContactAddressSender}
                        />
                </div>
            </div>
        </div>
    );  
} 

export default ContactComponent;