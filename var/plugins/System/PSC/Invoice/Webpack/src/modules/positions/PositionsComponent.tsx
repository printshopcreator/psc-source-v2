import * as React from 'react';
import AddPositionComponent from "./AddPositionComponent";
import * as PropTypes from "prop-types";
import { Shop } from '../../model/shop';

const PositionsComponent = (props) => {

    return (
        <>
         <AddPositionComponent shop={props.shop}/>
        </>
    );
}

PositionsComponent.propTypes = {
    shop: PropTypes.instanceOf(Shop),
};

export default PositionsComponent;