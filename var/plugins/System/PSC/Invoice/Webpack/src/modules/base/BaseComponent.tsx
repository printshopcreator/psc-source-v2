import { Shop } from '../../model/shop';
import ContactComponent from '../contact/ContactComponent';
import TopBarComponent from '../topbar/TopBarComponent';
import PositionsComponent from '../positions/PositionsComponent';
import {useEffect, useState} from 'react';

const BaseComponent = (props) => {   

    const [shop, setShop] = useState<Shop>(new Shop());

    return (
        <div> 
            <div><TopBarComponent shop={shop} change={setShop} /></div>
            <div>{ shop.id != 0 && <ContactComponent shop={shop} /> }</div>
            <div>{ shop.id != 0 && <PositionsComponent shop={shop} /> }</div>
        </div>
    );
    
} 

export default BaseComponent;