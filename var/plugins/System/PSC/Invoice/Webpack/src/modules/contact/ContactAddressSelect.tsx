import * as PropTypes from 'prop-types'
import { AsyncPaginate } from "react-select-async-paginate";
import { Contact } from '../../model/contact';
import { Address } from '../../model/address';
import {container} from "tsyringe";
import {ContactService} from '../../services/contact';
import {useEffect,useState} from 'react';

const ContactAddressSelect = (props) => {

  const [contactUuid, setContactUuid] = useState(null);

  useEffect(() => {
    setContactUuid(props.contact.uuid);
  }, [props.contact.uuid]);

  const loadOptions = async (searchQuery, loadedOptions) => {

    const contact_service = new ContactService();

    const data = await contact_service.getContactAddress(contactUuid, props.type);

    return {
      options: data,
      hasMore: false,
    }; 
  };

  const onChange = (option) => {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }
  };

  return (
    <AsyncPaginate
      defaultOptions
      key={JSON.stringify(contactUuid)}
      value={props.value}
      loadOptions={loadOptions}
      getOptionValue={(option) => option.uuid}
      getOptionLabel={(option) => option.firstname + ' ' + option.lastname}
      onChange={onChange}
      isSearchable={false}
    />
  );
};

ContactAddressSelect.propTypes = {
  value: PropTypes.instanceOf(Address),
  contact: PropTypes.instanceOf(Contact),
  type: PropTypes.number,
  onChange: PropTypes.func
};

export default ContactAddressSelect;