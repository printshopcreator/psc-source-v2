import * as PropTypes from 'prop-types';
import { Shop } from '../../model/shop';
import { container } from 'tsyringe';
import { Order } from '../../state/order';
import {ProductGroup} from "../../model/productGroup";
import {ProductGroupService} from "../../services/productgroup";
import AsyncSelect , { useAsync } from 'react-select/async';

const ProductGroupSelect = (props) => {
  const order: Order = container.resolve(Order);

  const loadOptions = async (searchQuery) => {
    const productgroup_api = new ProductGroupService();

    return await productgroup_api.getProductGroups(searchQuery, props.shop);
  };

  const onChange = (option) => {
    if (typeof props.onChange === "function") {
      props.onChange(option);
    }
  };

  return (
      <AsyncSelect
          defaultOptions
          key={JSON.stringify(props.shop.uuid)}
          value={props.value}
          loadOptions={loadOptions}
          getOptionValue={(option) => option.uuid}
          onChange={onChange}
          isSearchable={true}
      />
  );
};

ProductGroupSelect.propTypes = {
  shop: PropTypes.instanceOf(Shop),
  value: PropTypes.instanceOf(ProductGroup),
  onChange: PropTypes.func,
};

export default ProductGroupSelect;