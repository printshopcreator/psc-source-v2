import {v4 as uuidv4} from 'uuid';

export class Contact {
    id: Number;
    username: String;
    password: String;
    uuid: String;

    constructor() {
        this.id = 0;
        this.uuid = uuidv4();
        this.username = "";
        this.password = ""
    }
}