export class ProductGroup {
    id: Number = 0;
    label: String = "";
    uuid: String = "";
    options: ProductGroup[] = [];
}