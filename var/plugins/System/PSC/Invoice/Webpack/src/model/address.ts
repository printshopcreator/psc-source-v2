import {v4 as uuidv4} from 'uuid';

export class Address {
    id: Number;
    uuid: String;
    firstname: String;
    lastname: String;

    constructor() {
        this.id = 0;
        this.uuid = uuidv4();
        this.firstname = "";
        this.lastname = "";
    }
}