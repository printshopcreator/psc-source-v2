import {v4 as uuidv4} from 'uuid';

export class Order {
    
    uuid: String;

    constructor() {

        this.uuid = uuidv4();

    }
}