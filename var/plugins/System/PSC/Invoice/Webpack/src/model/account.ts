import {v4 as uuidv4} from 'uuid';

export class Account {
    id: Number;
    appendix: String;
    title: String;
    uuid: String;

    constructor() {
        this.id = 0;
        this.uuid = uuidv4();
        this.appendix = "";
        this.title = ""
    }
}