import {autoInjectable, singleton} from "tsyringe";
import { Token } from "./token";
import axios from 'axios';
import { Order } from '../state/order';

@singleton()
@autoInjectable()
export class OrderService {

  constructor(private token?: Token) {}

  async saveOrder(order: Order): Promise<Order> {

    return await axios.post('/apps/api/order/create', order ,{
        headers: {
            'Authorization': 'Bearer ' + this.token.currentToken
        }
    }).then(( response ) => {
        console.log(response);
        return response.data;
    });
  }

}