import {autoInjectable, singleton} from "tsyringe";
import { Token } from "./token";
import axios from 'axios';
import { Account } from "../model/account";
import { Shop } from "../model/shop";

@singleton()
@autoInjectable()
export class AccountService {
  constructor(private token?: Token) {}

  async getAccounts(shop: Shop): Promise<Account[]> {

    return await axios.get('/apps/api/account/allbyshop/' + shop.uuid, {
        headers: {
            'Authorization': 'Bearer ' + this.token.currentToken
        }
    }).then(( response ) => {
        return response.data.data.map((item) => {
          let account = new Account();
          account.id = item.id;
          account.title = item.title;
          account.appendix = item.appendix;
          account.uuid = item.uuid;
          return account;
        })
    
    });
  }
}