import {autoInjectable, singleton} from "tsyringe";
import { Token } from "./token";
import axios from 'axios';
import { Shop } from "../model/shop";

@singleton()
@autoInjectable()
export class ShopService {
  constructor(private token?: Token) {}

  async getShops(): Promise<Shop[]> {

    return await axios.get('/apps/api/shops', {
        headers: {
            'Authorization': 'Bearer ' + this.token.currentToken
        }
    }).then(( response ) => {
        return response.data.data.map((item) => {
          let shop = new Shop();
          shop.id = item.id;
          shop.name = item.name;
          shop.uuid = item.uuid;
          return shop;
        })
    
    });
  }
}