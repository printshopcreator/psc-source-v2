import { from, map, Observable, timer } from "rxjs";

import axios from "axios";
import {singleton} from "tsyringe";

@singleton()
export class Token {
    public currentToken:String;

    init() {
        setInterval(() => {
            this.refreshToken();
        }, 2*60*1000);
    }
    refreshToken(): void {
        axios.get('/apps/api/contact/refresh', {
            headers: {
                'Authorization': 'Bearer ' + this.currentToken
            }
        }).then(( response ) => {
            this.currentToken = response.data.token;
        })
    }
}