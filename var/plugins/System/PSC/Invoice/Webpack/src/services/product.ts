import {autoInjectable, singleton} from "tsyringe";
import { Token } from "./token";
import axios from 'axios';
import { Shop } from "../model/shop";
import {ProductGroup} from "../model/productGroup";
import {Product} from "../model/product";
import {JSONSchema7} from "json-schema";

@singleton()
@autoInjectable()
export class ProductService {
  constructor(private token?: Token) {}

    async getProductsByTerm(term: string, shop: Shop): Promise<Product[]> {

        return await axios.get('/apps/api/product/getallbyterm/' + shop.uuid + '/' + term, {
            headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
            }
        }).then(( response ) => {
            return response.data.data.map((item) => {
                let p = new Product();
                p.id = item.id;
                p.title = item.title;
                p.uuid = item.uuid;

                return p;
            })

        })
    }

  async getProducts(productGroup: ProductGroup): Promise<Product[]> {

    return await axios.get('/apps/api/product/getallbyproductgroup/' + productGroup.uuid, {
        headers: {
            'Authorization': 'Bearer ' + this.token.currentToken
        }
    }).then(( response ) => {
        return response.data.data.map((item) => {
          let p = new Product();
          p.id = item.id;
          p.title = item.title;
          p.uuid = item.uuid;

          return p;
        })
    
    })
  }

    async getAddProductSchema(product: Product): Promise<JSONSchema7> {
        return await axios.post('/apps/api/product/get/form/json/' + product.uuid,[], {
            headers: {
                'Authorization': 'Bearer ' + this.token.currentToken
            }
        }).then(( response ) => {
            return response.data;
        })
    }
}