import {autoInjectable, singleton} from "tsyringe";
import { Token } from "./token";
import axios from 'axios';
import { Contact } from "../model/contact";
import { Address } from "../model/address";
import { Shop } from "../model/shop";

@singleton()
@autoInjectable()
export class ContactService {
  constructor(private token?: Token) {}

  async getContacts(searchQuery: String, page: Number, shop: Shop): Promise<Contact[]> {

    return await axios.get('/apps/api/contact/allbyshop/' + shop.uuid + '?term=' + searchQuery + '&page=' + page, {
        headers: {
            'Authorization': 'Bearer ' + this.token.currentToken
        }
    }).then(( response ) => {
        return response.data.data.map((item) => {
          let contact = new Contact;
          contact.id = item.id;
          contact.username = item.username;
          contact.uuid = item.uuid;
          return contact;
        })
    
    });
  }

  async getContactAddress(contact: String, type: Number): Promise<Address[]> {
    return await axios.get('/apps/api/contact/address/getallfortype/' + contact + '/' + type, {
      headers: {
          'Authorization': 'Bearer ' + this.token.currentToken
      }
    }).then(( response ) => {
      return response.data.data.map((item) => {
        let address = new Address;
        address.id = item.id;
        address.firstname = item.firstname;
        address.lastname = item.lastname;
        address.uuid = item.uuid;
        return address;
      })
    })
  }
}