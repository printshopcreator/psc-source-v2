import {autoInjectable, singleton} from "tsyringe";
import { Token } from "./token";
import axios from 'axios';
import { Shop } from "../model/shop";
import {ProductGroup} from "../model/productGroup";

@singleton()
@autoInjectable()
export class ProductGroupService {
  constructor(private token?: Token) {}

  async getProductGroups(searchQuery: String, shop: Shop): Promise<ProductGroup[]> {

    return await axios.get('/apps/api/productgroup/gettree/' + shop.uuid + '?term=' + searchQuery, {
        headers: {
            'Authorization': 'Bearer ' + this.token.currentToken
        }
    }).then(( response ) => {
        return response.data.data.map((item) => {
          let pg = new ProductGroup;
          pg.id = item.id;
          pg.label = item.title;
          pg.uuid = item.uuid;

          item.children.map((sub) => {
              let spg = new ProductGroup;
              spg.id = sub.id;
              spg.label = sub.title;
              spg.uuid = sub.uuid;
              pg.options.push(spg);
          });

          return pg;
        })
    
    })
  }
}