import { Address } from "../model/address";
import { Contact } from "../model/contact";
import { Shipment } from "../model/shipment";
import { Payment } from "../model/payment";
import {singleton} from "tsyringe";
import { Account } from "../model/account";

@singleton()
export class Order {
    contact: Contact;
    account: Account;
    deliveryAddress: Address;
    senderAddress: Address;
    invoiceAddress: Address;

    payment: Payment;
    shipment: Shipment;

    type: Number;
    shop: String;

}