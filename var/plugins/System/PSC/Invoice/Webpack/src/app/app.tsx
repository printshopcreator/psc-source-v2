import * as Eta from 'eta'
import { Token } from '../services/token'
import {container} from "tsyringe";
import * as React from 'react';
import * as ReactDOM from 'react-dom/client';
import BaseComponent from '../modules/base/BaseComponent';


export class App {

    constructor(jwt: String) {
        let token = container.resolve(Token);
        token.currentToken = jwt;
        token.init();
    }

    init(): void {
        this.buildUi();
    }

    buildUi() {
        const root = ReactDOM.createRoot(document.getElementById('root'));
        root.render(React.createElement(BaseComponent));
    }

    run(): void {
        
    
    }
}