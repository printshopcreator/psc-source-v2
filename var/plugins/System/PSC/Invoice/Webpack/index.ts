import "reflect-metadata";
import 'bootstrap/scss/bootstrap.scss';

import * as $ from "jquery";
import { App } from "./src/app/app";

declare var jwt_token: String;

let app = new App(jwt_token);
app.init();
app.run();