<?php
namespace Plugin\System\PSC\Invoice\Controller\Backend;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

/**
 * Class IndexController
 *
 * @package PSC\Component\Invoice
 * @Route("/index")
 */
class IndexController extends AbstractController
{
    /**
     * @Template()
     * @Route("/create", name="psc_backend_invoice_index_create")
     * @Security("is_granted('ROLE_USER')")
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(JWTTokenManagerInterface $jwtManager)
    {
        return array('jwt' => $jwtManager->create($this->getUser()));
    }
}