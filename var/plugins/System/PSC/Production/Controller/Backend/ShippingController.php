<?php
namespace Plugin\System\PSC\Production\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use PSC\Shop\OrderBundle\Form\Backend\OrderStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Tracking;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactDeliveryType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactInvoiceType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactSenderType;
use PSC\Shop\QueueBundle\Event\Order\Updated;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\Shop\ShippingBundle\Service\Registry;
use PSC\System\SettingsBundle\Service\PaperDB;
use PSC\System\SettingsBundle\Service\Status;
use Ramsey\Uuid\Uuid;
use Sauladam\ShipmentTracker\ShipmentTracker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
require_once (__DIR__ .'/../../../../../../../src/PSC/Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
/**
 * Class ShippingController
 *
 * @package PSC\Component\Production
 * @Route("/shipping")
 */
class ShippingController extends AbstractController
{
    /**
     * @Template()
     * @Route("/index", name="psc_backend_production_shipping_list_index")
     * @Security("is_granted('ROLE_PRODUCTION')")
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {

        $selectedShop = $shopService->getSelectedShop();

       $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');
        $qb = $userRepository->createQueryBuilder('orders')
            ->leftJoin('orders.contact', 'contact')
            ->leftJoin('orders.invoiceAddress', 'invoiceAddress')
            ->leftJoin('orders.positions', 'positions')
            ->andWhere('orders.shop = :shop_id')
            ->andWhere('positions.status = :status')
            ->setParameter("shop_id", $selectedShop->getUid())
            ->setParameter("status", 177)
            ->orderBy('orders.uid', 'desc');

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            15
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Template()
     * @Route("/tracking/delete/{uuid}/{pos}/{index}", name="psc_backend_production_shipping_detail_tracking_delete")
     * @Security("is_granted('ROLE_PRODUCTION')")
     */
    public function deleteTrackingAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, DocumentManager $documentManager, $uuid, $pos, $index)
    {
        /** @var Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /**
         * @var Orderpos $pos
         */
        $pos = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $pos));

        /** @var Position $objDoc */
        $objDoc = $documentManager
            ->getRepository('PSCEntityBundle:Position')
            ->findOneByUid((string)$pos->getId());

        $trackings = $objDoc->getTrackings();

        unset($trackings[$index]);

        $objDoc->setTrackings($trackings);

        $documentManager->persist($objDoc);
        $documentManager->flush();

        return $this->redirectToRoute('psc_backend_production_shipping_detail_index', array('uuid' => $uuid));
    }

    /**
     * @Template()
     * @Route("/detail/{uuid}/{enableDelivery}", defaults={"enableDelivery" = 0}, name="psc_backend_production_shipping_detail_index")
     * @Security("is_granted('ROLE_PRODUCTION')")
     */
    public function detailAction(
        Request $request,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        Manager $eventManager,
        PaperDB $paperDB,
        SessionInterface $session,
        Status $statusService,
        Registry $shippingService,
        $uuid, $enableDelivery)
    {
        $selectedShop = $shopService->getSelectedShop();

        /** @var Order $order */
        $order = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));

        /** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
        $orderObj = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('uid' => (string)$order->getUid()));

        $formStatus = $this->createForm(OrderStatusType::class, $order);

        $formStatus->handleRequest($request);

        if ($formStatus->isSubmitted() && $formStatus->isValid()) {
            $entityManager->persist($order);
            $entityManager->flush();

            $session->getFlashBag()->add(
                'success',
                'Status erfolgreich gesetzt'
            );

            $notify = new \PSC\Shop\QueueBundle\Event\Order\Status\Change();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $notify->setStatus($order->getStatus());
            $eventManager->addJob($notify);

        }

        $invoiceObj = $orderObj->getInvoiceAddressSaved();
        $deliveryObj = $orderObj->getDeliveryAddressSaved();
        $senderObj = $orderObj->getSenderAddressSaved();

        $formInvoice = $this->createForm(ContactInvoiceType::class, $invoiceObj);
        $formDelivery = $this->createForm(ContactDeliveryType::class, $deliveryObj);
        $formSender = $this->createForm(ContactSenderType::class, $senderObj);

        $formInvoice->handleRequest($request);
        $formDelivery->handleRequest($request);
        $formSender->handleRequest($request);

        if ($formInvoice->isSubmitted() && $formInvoice->isValid()) {
            $orderObj->setInvoiceAddressSaved($invoiceObj);
            $documentManager->persist($orderObj);
            $documentManager->flush();

            $notify = new Updated();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $eventManager->addJob($notify);
        }
        if ($formDelivery->isSubmitted() && $formDelivery->isValid()) {
            $orderObj->setDeliveryAddressSaved($deliveryObj);
            $documentManager->persist($orderObj);
            $documentManager->flush();

            $notify = new Updated();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $eventManager->addJob($notify);
        }
        if ($formSender->isSubmitted() && $formSender->isValid()) {
            $orderObj->setSenderAddressSaved($senderObj);
            $documentManager->persist($orderObj);
            $documentManager->flush();

            $notify = new Updated();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $eventManager->addJob($notify);
        }

        $positions = array();

        foreach($order->getPositions() as $pos) {

            /** @var Position $objDoc */
            $objDoc = $documentManager
                ->getRepository('PSCEntityBundle:Position')
                ->findOneBy(['uid' => (string)$pos->getId()]);

            if($request->isMethod('post') && $request->get('shippingProvider', false) && strtolower($request->get('shippingProvider', 'keine')) != 'keine') {

                $tracking = new Tracking();
                $tracking->setShippingProvider($request->get('shippingProvider'));
                $tracking->setTrackingNumber($request->get('trackingNumber'));

                $trackingProv = new ShipmentTracker();
                $trackingProv = $trackingProv->get($request->get('shippingProvider'));
                $url = $trackingProv->trackingUrl($request->get('trackingNumber'));
                $tracking->setTrackingUrl($url);

                $objDoc->addTracking($tracking);

                $documentManager->persist($objDoc);
                $documentManager->flush();

            }

            $objPosition = unserialize(($pos->getData()));

            $paperContainer = new PaperContainer();
            $paperContainer->parse(simplexml_load_string($selectedShop->getInstall()->getPaperContainer()));
            $engine = new Engine();
            $engine->setPaperRepository($paperDB);
            $engine->setPaperContainer($paperContainer);
            if($selectedShop->getInstall()->getCalcTemplates()) {
                $engine->setTemplates('<root>'.$selectedShop->getInstall()->getCalcTemplates().'</root>');
            }
            $engine->loadString($pos->getCalcXml());
            $engine->setFormulas($order->getShop()->getFormel());
            $engine->setParameters($order->getShop()->getParameter());
            $engine->setVariables($objPosition->getOptions());
            $engine->setSavedCalcReferences($objDoc->getCalcReferences());
            $engine->setActiveArticle($objPosition->getOptions()['kalk_artikel']);

            $articleCalc = $engine->getArticle();


            $positions[] = array(
                'obj' => $pos,
                'objDoc' => $objDoc,
                'calc' => $articleCalc
            );
        }

        return array(
            'positions' => $positions,
            'formStatus' => $formStatus->createView(),
            'formInvoice' => $formInvoice->createView(),
            'formDelivery' => $formDelivery->createView(),
            'formSender' => $formSender->createView(),
            'order' => $order,
            'shippingProvider' => $shippingService->all(),
            'posStatus' => $statusService->getPositionStatusAsArray()
        );
    }

    /**
     * Delete Seite
     *
     * @Route("/detail/switchposstatus/{order}/{pos}/{status}", name="psc_backend_production_shipping_detail_switchpos")
     * @Template()
     *
     * @param EntityManagerInterface $entityManager
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param Manager $eventManager
     * @param string $order
     * @param string $pos
     * @param int $status
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function switchPosStatusAction(EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, Manager $eventManager, $order = "", $pos = "", $status = 10)
    {
        /** @var Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /**
         * @var Orderpos $pos
         */
        $pos = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $pos));

        $pos->setStatus($status);

        $entityManager->persist($pos);
        $entityManager->flush();

        $notify = new Change();
        $notify->setShop($selectedShop->getUID());
        $notify->setPosition($pos->getUuid());
        $notify->setStatus($pos->getStatus());
        $eventManager->addJob($notify);

        return $this->redirectToRoute('psc_backend_production_shipping_detail_index', array('uuid' => $order));

    }


}
