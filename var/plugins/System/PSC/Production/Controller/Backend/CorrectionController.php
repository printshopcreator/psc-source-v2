<?php
namespace Plugin\System\PSC\Production\Controller\Backend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\PaperDB;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
require_once (__DIR__ .'/../../../../../../../src/PSC/Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
/**
 * Class CorrectionController
 *
 * @package PSC\Component\Production
 * @Route("/correction")
 */
class CorrectionController extends AbstractController
{
    /**
     * @Template()
     * @Route("/index", name="psc_backend_production_correction_list_index")
     * @Security("is_granted('ROLE_PRODUCTION')")
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {

        $selectedShop = $shopService->getSelectedShop();

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
        $qb = $userRepository->createQueryBuilder('orderspos')
            ->leftJoin('orderspos.order', 'ord')
            ->andWhere('orderspos.shop = :shop_id')
            ->andWhere('ord.status = 145')
            ->andWhere('orderspos.status IN (50,155,500,510,520,530)')
            ->setParameter("shop_id", $selectedShop->getUid())
            ->orderBy('orderspos.createdDate', 'desc');

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            15
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Route("/deleteupload/{uuid}/{upload}", name="psc_backend_production_correction_deleteupload")
     * @Security("is_granted('ROLE_PRODUCTION')")
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $uuid
     * @param $upload
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteuploadAction(\PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uuid, $upload)
    {
        /** @var Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        $positionRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
        /** @var Orderpos $position */
        $position = $positionRepo->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));

        if($position) {
            $uploadRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Upload');
            /** @var Upload $upload */
            $upload = $uploadRepo->findOneBy(array('orderPos' => $position, 'uid' => $upload));
            if($upload) {
                $entityManager->remove($upload);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('psc_backend_production_correction_detail_index', array('uuid' => $uuid));
    }

    /**
     * @Route("/changeexport/{uuid}/{upload}", name="psc_backend_production_correction_changeexport")
     * @Security("is_granted('ROLE_PRODUCTION')")
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $uuid
     * @param $upload
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function changeexportAction(\PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uuid, $upload)
    {
        /** @var Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        $positionRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
        /** @var Orderpos $position */
        $position = $positionRepo->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));
        if($position) {

            $uploadRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Upload');
            /** @var Upload $upload */
            $upload = $uploadRepo->findOneBy(array('orderPos' => $position, 'uid' => $upload));

            if($upload) {
                $upload->setExport(!$upload->isExport());
                $entityManager->persist($upload);
                $entityManager->flush();
            }
        }

        return $this->redirectToRoute('psc_backend_production_correction_detail_index', array('uuid' => $uuid));
    }

    /**
     * @Template()
     * @Route("/detail/{uuid}", name="psc_backend_production_correction_detail_index")
     * @Security("is_granted('ROLE_PRODUCTION')")
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param Manager $eventManager
     * @param PaperDB $paperDB
     * @param SessionInterface $session
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function detailAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, Manager $eventManager, PaperDB $paperDB, SessionInterface $session)
    {
        /** @var Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        $positionRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
        /** @var Orderpos $position */
        $position = $positionRepo->findOneBy(array('shop' => $selectedShop, 'uuid' => $request->get('uuid', false)));

        $uploadRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Upload');
        /** @var Upload $uploadObj */
        $uploadObj = $uploadRepo->findOneBy(array('orderPos' => $position));

        $upload = new Upload();
        $upload->setOrderPos($position);
        if($uploadObj) {
            $upload->setTyp($uploadObj->getTyp());
        }
        $formStatus = $this->createForm(StatusType::class, $position);
        $formQuickStatus = $this->createForm(QuickStatusType::class, $position);


        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            /** @var UploadedFile $uploadFile */
            $uploadFile = $form->get('file')->getData();
            if($uploadFile instanceOf UploadedFile) {
                $id = Uuid::uuid4();
                if(!file_exists('uploads/' . $selectedShop->getId() . '/article')) {
                    mkdir('uploads/' . $selectedShop->getId() . '/article', 0777, true);
                }
                $uploadFile->move('uploads/' . $selectedShop->getId() . '/article', $id . '_' . $uploadFile->getClientOriginalName());
                $upload->setPath($id . '_' . $uploadFile->getClientOriginalName());
                $upload->setName($uploadFile->getClientOriginalName());
            }
            $upload->setUuid(Uuid::uuid4());
            $upload->setCreated(new \DateTime());
            $upload->setUpdated(new \DateTime());
            $entityManager->persist($upload);
            $entityManager->flush();
        }

        $formStatus->handleRequest($request);
        $formQuickStatus->handleRequest($request);

        if(!$request->isMethod('post')) {
            $formQuickStatus->get('status')->setData(158);
        }
        if($request->get('status', false)) {
            $position->setStatus($request->get('status', false));
        }


        $uplaodRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Upload');
        $uploads = $uplaodRepo->findBy(array('orderPos' => $position), array('updated' => 'DESC'));

        if (($formStatus->isSubmitted() && $formStatus->isValid()) || ($formQuickStatus->isSubmitted() && $formQuickStatus->isValid())) {
            $inPackage = false;
            /** @var Upload $upload */
            foreach($uploads as $upload) {
                if($upload->isExport()) $inPackage = true;
            }

            if($inPackage || $position->getStatus() != 155) {
                $entityManager->persist($position);
                $entityManager->flush();

                $session->getFlashBag()->add(
                    'success',
                    'Status erfolgreich gesetzt'
                );

                $notify = new Change();
                $notify->setShop($selectedShop->getUID());
                $notify->setPosition($position->getUuid());
                $notify->setStatus($position->getStatus());
                $eventManager->addJob($notify);

                return $this->redirectToRoute('psc_backend_production_correction_list_index');
            }else{
                $session->getFlashBag()->add(
                    'danger',
                    'Mindestens ein Upload muss im Package vorhanden sein'
                );
            }
        }

        $objPosition = unserialize(($position->getData()));

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string($selectedShop->getInstall()->getPaperContainer()));

        $engine = new Engine();
        $engine->setPaperRepository($paperDB);
        $engine->setPaperContainer($paperContainer);
        $engine->loadString($position->getCalcXml());
        $engine->setFormulas($position->getShop()->getFormel());
        $engine->setParameters($position->getShop()->getParameter());
        $engine->setVariables($objPosition->getOptions());
        $engine->setActiveArticle($objPosition->getOptions()['kalk_artikel']);
        $articleCalc = $engine->getArticle();

        return array(
            'position' => $position,
            'uploads' => $uploads,
            'form' => $form->createView(),
            'formStatus' => $formStatus->createView(),
            'quickFormStatus' => $formQuickStatus->createView(),
            'calc' => $articleCalc,
            'invoiceAddress' => $position->getOrder()->getInvoiceAddress(),
            'order' => $position->getOrder(),
            'contact' => $position->getOrder()->getContact(),
        );
    }


}
