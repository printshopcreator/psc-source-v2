<?php
namespace Plugin\System\PSC\Production\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\PaperDB;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
require_once (__DIR__ .'/../../../../../../../src/PSC/Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
/**
 * Class ProductionController
 *
 * @package PSC\Component\Production
 * @Route("/production")
 */
class ProductionController extends AbstractController
{
    /**
     * @Template()
     * @Route("/index", name="psc_backend_production_production_list_index")
     * @Security("is_granted('ROLE_PRODUCTION')")
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {

        $selectedShop = $shopService->getSelectedShop();

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
        $qb = $userRepository->createQueryBuilder('orderspos')
            ->leftJoin('orderspos.order', 'ord')
            ->andWhere('orderspos.shop = :shop_id')
            ->andWhere('ord.status = 145')
            ->andWhere('orderspos.status = 160')
            ->setParameter("shop_id", $selectedShop->getUid())
            ->orderBy('orderspos.createdDate', 'desc');

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            15
        );

        return array(
            'pagination' => $pagination,
        );
    }

    /**
     * @Template()
     * @Route("/detail/{uuid}", name="psc_backend_production_production_detail_index")
     * @Security("is_granted('ROLE_PRODUCTION')")
     */
    public function detailAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, SessionInterface $session, PaperDB $paperDB, EntityManagerInterface $entityManager, DocumentManager $documentManager, Manager $eventManager)
    {
        /** @var Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        $positionRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
        /** @var Orderpos $position */
        $position = $positionRepo->findOneBy(array('shop' => $selectedShop, 'uuid' => $request->get('uuid', false)));

        $uploadRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Upload');
        /** @var Upload $uploadObj */
        $uploadObj = $uploadRepo->findOneBy(array('orderPos' => $position));

        $upload = new Upload();
        $upload->setOrderPos($position);
        if($uploadObj) {
            $upload->setTyp($uploadObj->getTyp());
        }
        $formStatus = $this->createForm(StatusType::class, $position);
        $formQuickStatus = $this->createForm(QuickStatusType::class, $position);

        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $uploadFile */
            $uploadFile = $form->get('file')->getData();

            $id = Uuid::uuid4();
            if(!file_exists('uploads/' . $selectedShop->getId() . '/article')) {
                mkdir('uploads/' . $selectedShop->getId() . '/article', 0777, true);
            }
            $uploadFile->move('uploads/' . $selectedShop->getId() . '/article', $id . '_' . $uploadFile->getClientOriginalName());
            $upload->setPath($id . '_' . $uploadFile->getClientOriginalName());
            $upload->setName($uploadFile->getClientOriginalName());
            $upload->setUuid(Uuid::uuid4());
            $upload->setCreated(new \DateTime());
            $upload->setUpdated(new \DateTime());
            $entityManager->persist($upload);
            $entityManager->flush();
        }

        $formStatus->handleRequest($request);
        $formQuickStatus->handleRequest($request);

        if(!$request->isMethod('post')) {
            $formQuickStatus->get('status')->setData(177);
        }

        if (($formStatus->isSubmitted() && $formStatus->isValid()) || ($formQuickStatus->isSubmitted() && $formQuickStatus->isValid())) {
            $entityManager->persist($position);
            $entityManager->flush();

            $session->getFlashBag()->add(
                'success',
                'Status erfolgreich gesetzt'
            );

            $notify = new Change();
            $notify->setShop($selectedShop->getUID());
            $notify->setPosition($position->getUuid());
            $notify->setStatus($position->getStatus());
            $eventManager->addJob($notify);

            return $this->redirectToRoute('psc_backend_production_production_list_index');
        }

        $uplaodRepo = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Upload');
        $uploads = $uplaodRepo->findBy(array('orderPos' => $position), array('created' => 'DESC'));

        $objPosition = unserialize(($position->getData()));

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string($selectedShop->getInstall()->getPaperContainer()));

        $engine = new Engine();
        $engine->setPaperRepository($paperDB);
        $engine->setPaperContainer($paperContainer);
        $engine->loadString($position->getCalcXml());
        $engine->setFormulas($position->getShop()->getFormel());
        $engine->setParameters($position->getShop()->getParameter());
        $engine->setVariables($objPosition->getOptions());
        $engine->setActiveArticle($objPosition->getOptions()['kalk_artikel']);
        $articleCalc = $engine->getArticle();

        return array(
            'position' => $position,
            'uploads' => $uploads,
            'form' => $form->createView(),
            'formStatus' => $formStatus->createView(),
            'quickFormStatus' => $formQuickStatus->createView(),
            'calc' => $articleCalc,
            'invoiceAddress' => $position->getOrder()->getInvoiceAddress(),
            'order' => $position->getOrder(),
            'contact' => $position->getOrder()->getContact(),
        );
    }


}