<?php

namespace Plugin\System\PSC\Production\Event;

final class MenuEvents
{
    /**
     * @var string
     */
    const CONFIGURE_MAIN = 'psc.backend.production.main';

    /**
     * @var string
     */
    const CONFIGURE_TOP = 'psc.backend.production.top';
}