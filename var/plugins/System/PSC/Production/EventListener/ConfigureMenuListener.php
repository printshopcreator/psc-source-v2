<?php
namespace Plugin\System\PSC\Production\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Plugin\System\PSC\Production\Event\MenuEvents;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    protected $eventDispatcher;
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_PRODUCTION')) {
            $menu = $event->getMenu();
            $factory = $event->getFactory();

            $submenu = $menu->addChild('production', array(
                'uri' => '#',
                'label' => 'Produktion',
                'extras' => array(
                    'icon' => 'fas fa-rocket',
                    'orderNumber' => 12
                )
            ));

            $submenu->addChild('production_graphic',
                array(
                    'route' => 'psc_backend_production_correction_list_index',
                    'label' => 'Grafikabteilung',
                    'extras' => array(
                        'icon' => 'fas fa-wrench'
                    )
                )
            );

            $submenu->addChild('production_correction',
                array(
                    'route' => 'psc_backend_production_prepress_list_index',
                    'label' => 'Druckvorstufe',
                    'extras' => array(
                        'icon' => 'fas fa-wrench'
                    )
                )
            );

            $submenu->addChild('production_prod',
                array(
                    'route' => 'psc_backend_production_production_list_index',
                    'label' => 'Produktionsabteilung',
                    'extras' => array(
                        'icon' => 'fas fa-wrench'
                    )
                )
            );

            $submenu->addChild('production_shipping',
                array(
                    'route' => 'psc_backend_production_shipping_list_index',
                    'label' => 'Versandabteilung',
                    'extras' => array(
                        'icon' => 'fas fa-wrench'
                    )
                )
            );

            $this->eventDispatcher->dispatch(new ConfigureMenuEvent($factory, $submenu), MenuEvents::CONFIGURE_MAIN);
        }
    }
}