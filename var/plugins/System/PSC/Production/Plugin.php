<?php
namespace Plugin\System\PSC\Production;

use PSC\System\PluginBundle\Plugin\Base;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Production Module';

    public function getType()
    {
        return Plugin::Backend;
    }

    public function getDescription()
    {
        return 'Produktionsmodul';
    }

    public function getVersion()
    {
        return 1;
    }


}