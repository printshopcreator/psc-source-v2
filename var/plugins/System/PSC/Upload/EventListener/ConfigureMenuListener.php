<?php
namespace Plugin\System\PSC\Upload\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use PSC\Shop\SettingsBundle\Event\MenuEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();

            $menu->addChild('Upload', array(
                'route' => 'psc_shop_upload_backend_list_index',
                'extras' => array(
                    'icon' => 'fas fa-envelope',
                    'orderNumber' => 33
                )
            ));
        }
    }
}