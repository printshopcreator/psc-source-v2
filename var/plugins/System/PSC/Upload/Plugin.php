<?php
namespace Plugin\System\PSC\Upload;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Upload Plugin';

    public function getType()
    {
        return Plugin::Frontend;
    }

    public function getDescription()
    {
        return 'Stellt Upload Bereich zur Verfügung';
    }

    public function getVersion()
    {
        return 1;
    }
}