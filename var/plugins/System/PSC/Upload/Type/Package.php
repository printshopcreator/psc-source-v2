<?php
namespace Plugin\System\PSC\Upload\Type;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use MongoDB\BSON\ObjectId;
use PhpParser\Comment\Doc;
use Plugin\System\PSC\Upload\Document\Upload;
use Plugin\System\PSC\Upload\Event\Create;
use Plugin\System\PSC\Upload\Helper\UploaderHelper;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use ZipArchive;

class Package implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    private \Twig\Environment $_template;
    /**
     * @var UploaderHelper
     */
    private UploaderHelper $_uploadHelper;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, \Twig\Environment $template, UploaderHelper $uploadHelper)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_template = $template;
        $this->_uploadHelper = $uploadHelper;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'shop_upload_package';
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return 'Shop Upload Modul';
    }

    public function getDescription()
    {
        return 'Generiert eine Zip';
    }

    public function getName()
    {
        return 'UploadPackage';
    }

    public function getForm(FormBuilderInterface $builder, $new, EventInterface $event)
    {
        if ($new) {
            $builder->add("path", TextType::class, array('data' => '/data/www/packages/uploads/{{ upload.id }}.zip', 'label' => 'Pfad', 'attr' => array('class' => 'form-element')));
        } else {
            $builder->add("path", TextType::class, array('label' => 'Pfad', 'attr' => array('class' => 'form-element')));
        }
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        $package = new \Plugin\System\PSC\Upload\Document\Queue\Package();
        $package->setPath($form->get('path')->getData());
        $objQueue->setQueueDocument($package);
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
        $form->get('path')->setData($queueObj->getQueueDocument()->getPath());
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCUpload/queue/package.html.twig';
    }

    /**
     * @param EventInterface $event
     * @param Queue $packageDoc
     */
    public function execute(EventInterface $event, Queue $packageDoc) {

        try{
            if($event instanceof Create) {
                /** @var Upload $upload */
                $upload = $this->_doctrine_mongodb
                    ->getRepository(Upload::class)
                    ->findOneBy(['id' => new ObjectId($event->getUpload())]);

                if($upload) {
                    $archive = new ZipArchive();

                    $path = $this->_template->createTemplate($packageDoc->getQueueDocument()->getPath());
                    $path = $path->render(array('upload' => $upload));
                    $pathInfo = pathinfo($path);
                    if(!file_exists($pathInfo['dirname'])) {
                        mkdir($pathInfo['dirname'], 0777, true);
                    }
                    if(file_exists($path)) {
                        $archive->open($path, ZIPARCHIVE::OVERWRITE);
                    }else{
                        $archive->open($path, ZIPARCHIVE::CREATE);
                    }
                    $archive->addFromString($upload->getFilename(), $this->_uploadHelper->getPath($upload));

                    $archive->close();
                }

            }
        }catch(\Exception $e) {
            $this->_error = $e->getMessage();
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->_error;
    }
}