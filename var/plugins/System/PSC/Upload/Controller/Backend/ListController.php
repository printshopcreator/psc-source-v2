<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\Upload\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\ObjectId;
use Plugin\System\PSC\Upload\Document\Upload;
use Plugin\System\PSC\Upload\Event\Create;
use Plugin\System\PSC\Upload\Form\Backend\DeleteType;
use Plugin\System\PSC\Upload\Helper\UploaderHelper;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * ListController fürs Backend
 *
 * @package    PSC\Shop\Upload
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/list/index", name="psc_shop_upload_backend_list_index")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param DocumentManager $documentManager
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, DocumentManager $documentManager)
    {

        $selectedShop = $shopService->getSelectedShop();

        $uploads = $documentManager
            ->getRepository(\Plugin\System\PSC\Upload\Document\Upload::class)
            ->findBy(['shop' => (string)$selectedShop->getUID()]);

        return array(
            'uploads' => $uploads
        );
    }

    /**
     * Default Seite
     *
     * @Route("/list/download/{uid}", name="psc_shop_upload_backend_list_download")
     * @param DocumentManager $documentManager
     * @param $uid
     * @return BinaryFileResponse
     */
    public function downloadAction(DocumentManager $documentManager, $uid, UploaderHelper $uploaderHelper)
    {
        /** @var Upload $upload */
        $upload = $documentManager
            ->getRepository(\Plugin\System\PSC\Upload\Document\Upload::class)
            ->findOneBy(['id' => new ObjectId($uid)]);

        $response = new BinaryFileResponse($uploaderHelper->getPath($upload));
        $disposition = $response->headers->makeDisposition(
            HeaderUtils::DISPOSITION_ATTACHMENT,
            $upload->getFilename()
        );

        // Set the content disposition
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * Default Seite
     *
     * @Route("/list/delete/{uid}", name="psc_shop_upload_backend_list_delete")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $uid
     * @return array|RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(Request $request, DocumentManager $documentManager, $uid)
    {
         /** @var Upload $upload */
        $upload = $documentManager
            ->getRepository(\Plugin\System\PSC\Upload\Document\Upload::class)
            ->findOneBy(['id' => new ObjectId($uid)]);

        $form = $this->createForm(DeleteType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $documentManager->remove($upload);
                $documentManager->flush();

                return $this->redirectToRoute('psc_shop_upload_backend_list_index');
            }
            return $this->redirectToRoute('psc_shop_upload_backend_list_index');
        }

        return array('upload' => $upload, 'form' => $form->createView());
    }

    /**
     * Default Seite
     *
     * @Route("/list/export/{uid}", name="psc_shop_upload_backend_list_export")
     * @param DocumentManager $documentManager
     * @param Manager $eventManager
     * @param $uid
     * @return RedirectResponse
     */
    public function exportAction(DocumentManager $documentManager, Manager $eventManager, $uid)
    {
        /** @var Upload $upload */
        $upload = $documentManager
            ->getRepository(\Plugin\System\PSC\Upload\Document\Upload::class)
            ->findOneBy(['id' => new \MongoId($uid)]);

        $notify = new Create();
        $notify->setShop($upload->getShop());
        $notify->setUpload($upload->getId());
        $eventManager->addJob($notify);

        return $this->redirectToRoute('psc_shop_upload_backend_list_index');
    }


}
