<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\Upload\Controller\Frontend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Plugin\System\PSC\Upload\Helper\UploaderHelper;
use PSC\Shop\QueueBundle\Event\Package\Created;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use Plugin\System\PSC\Upload\Document\Upload;
use Plugin\System\PSC\Upload\Event\Create;
use Plugin\System\PSC\Upload\Form\Frontend\UploadType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * CreateController fürs Frontend
 *
 * @package    PSC\Shop\Upload
 * @subpackage Controller
 */
class CreateController extends AbstractController
{

    /**
     * @Route("/create", name="psc_shop_upload_frontend_create")
     * @Template()
     * @param Request $request
     *
     * @param Shop $shopService
     * @param DocumentManager $documentManager
     * @param Manager $eventManager
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function createAction(Request $request, Shop $shopService, DocumentManager $documentManager, Manager $eventManager, UploaderHelper $uploaderHelper)
    {
        $selectedShop = $shopService->getShopByDomain();
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Upload $upload */
            $upload = $form->getData();
            /** @var UploadedFile $uploadedFile */
            $uploadedFile = $form['file']->getData();

            if ($uploadedFile) {
                $upload->setFilename($uploadedFile->getClientOriginalName());
                $upload->setMimeType($uploadedFile->getClientMimeType());
                $upload->setFileSize($uploadedFile->getSize());
                $newFilename = $uploaderHelper->uploadFile($uploadedFile);
                $upload->setFile($newFilename);
                $upload->setShop($selectedShop->getUID());

                $documentManager->persist($upload);
                $documentManager->flush();
                $notify = new Create();
                $notify->setShop($selectedShop->getUID());
                $notify->setUpload($upload->getId());
                $eventManager->addJob($notify);

                $created = new Created();
                $created->setShop($selectedShop->getUID());
                $created->setOrder("mongodb;" .$upload->getId());
                $eventManager->addJob($created);

            }





            return array(
                'send' => true
            );
        }


        return array(
            'form' => $form->createView(),
            'send' => false
        );
    }
}