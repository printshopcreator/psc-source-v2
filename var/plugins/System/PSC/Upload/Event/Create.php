<?php
namespace Plugin\System\PSC\Upload\Event;

use PSC\Shop\QueueBundle\Event\Event;

class Create extends Event
{

    /** @var string */
    protected $upload;

    public function getType()
    {
        return 'shop_upload_create';
    }

    public function getDescription()
    {
        return 'Datei Hochgeladen';
    }

    public function getData()
    {
        return array(
            'upload' => $this->upload
        );
    }

    public function setData($data)
    {
        $this->upload = $data['upload'];
    }

    /**
     * @return string
     */
    public function getUpload()
    {
        return $this->upload;
    }

    /**
     * @param string $upload
     */
    public function setUpload($upload)
    {
        $this->upload = $upload;
    }

}