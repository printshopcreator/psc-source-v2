<?php
namespace Plugin\System\PSC\FilterMail;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Produkt-Filter Mail';

    public function getType()
    {
        return Plugin::Queues;
    }

    public function getDescription()
    {
        return 'Mail Queue welche nach Produkten filtert';
    }

    public function getVersion()
    {
        return 1;
    }
}