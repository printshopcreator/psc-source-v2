<?php
namespace Plugin\System\PSC\FilterMail\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Mail extends \PSC\Shop\QueueBundle\Document\Queue\Mail
{
    /**
     *
     * @Field(type="collection")
     */
    protected $products = [];

    /**
     * @return array
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param array $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    public function addProduct($product)
    {
        $this->products[] = $product;
    }
}