<?php
namespace Plugin\System\PSC\Paypal\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Paypal
{
    /**
     * @var string $clientId
     *
     * @Field(type="string")
     */
    protected $clientId;

    /**
     * @var string $clientSecret
     *
     * @Field(type="string")
     */
    protected $clientSecret;

    /**
     * @var boolean $production
     *
     * @Field(type="bool")
     */
    protected $production;

    /**
     * @return boolean
     */
    public function isProduction()
    {
        return $this->production;
    }

    /**
     * @param boolean $production
     */
    public function setProduction($production)
    {
        $this->production = $production;
    }

    /**
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;
    }

    /**
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;
    }


}