<?php
namespace Plugin\System\PSC\Paypal;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Paypal Payment';

    public function getType()
    {
        return Plugin::Payment;
    }

    public function getDescription()
    {
        return 'Stellt Unterstützung für Paypal bereit';
    }

    public function getVersion()
    {
        return 1;
    }
}