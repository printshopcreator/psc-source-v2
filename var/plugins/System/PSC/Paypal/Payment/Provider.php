<?php
namespace Plugin\System\PSC\Paypal\Payment;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPal\Api\PaymentExecution;
use PayPal\Api\RedirectUrls;
use PayPal\Api\Transaction;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Rest\ApiContext;
use Plugin\System\PSC\Paypal\Document\Paypal;
use PSC\Shop\EntityBundle\Document\Order;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Provider\PaymentProvider;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;
use Sofort\SofortLib\Notification;
use Sofort\SofortLib\TransactionData;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Provider extends PaymentProvider
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
    }

    public function getName()
    {
        return 'Paypal';
    }

    public function getType()
    {
        return 'paypal';
    }

    public function saveDocument(Gatewaysettings $settings, Form $form)
    {
        $doc = new Paypal();

        $doc->setClientId($form->get('paypal')->get('clientId')->getData());
        $doc->setClientSecret($form->get('paypal')->get('clientSecret')->getData());
        $doc->setProduction($form->get('paypal')->get('production')->getData());

        $settings->setGatewayDocument($doc);

        return $settings;
    }

    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder)
    {

        if(!$settings->getGatewayDocument()) {
            $settings->setGatewayDocument(new Paypal());
        }

        $builder
            ->add('production', CheckboxType::class, array('label' => 'Production?', 'required' => false))
            ->add('clientId', TextType::class, array('label' => 'Client Id', 'required' => false))
            ->add('clientSecret', TextType::class, array('label' => 'Client Secret', 'required' => false));

        $builder->get('production')->setData($settings->getGatewayDocument()->isProduction());
        $builder->get('clientId')->setData($settings->getGatewayDocument()->getClientId());
        $builder->get('clientSecret')->setData($settings->getGatewayDocument()->getClientSecret());

        return $builder;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCPaypal/settings.html.twig';
    }

    public function handlePayment(Request $request)
    {
        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->getGatewaySettings()->getGatewayDocument()->getClientId(),
                $this->getGatewaySettings()->getGatewayDocument()->getClientSecret()
            )
        );
        
        if($this->getGatewaySettings()->getGatewayDocument()->isProduction()) {
            $apiContext->setConfig(
                array(
                    'mode' => 'live',
                    'log.LogEnabled' => false,
                    'log.FileName' => '/tmp/PayPal.log',
                    'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                    'cache.enabled' => true,
                    'cache.FileName' => '/tmp/auth.cache'
                )
            );
        }else {
            $apiContext->setConfig(
                array(
                    'mode' => 'sandbox',
                    'log.LogEnabled' => false,
                    'log.FileName' => '../PayPal.log',
                    'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                    'cache.enabled' => true,
                    'cache.FileName' => '/tmp/auth.cache'
                )
            );
        }

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        $amount = new Amount();
        $amount->setCurrency("EUR")
            ->setTotal($request->get('amount'));

        $item1 = new Item();
        $item1->setName('Onlinebestellung')
            ->setDescription('Onlinebestellung')
            ->setCurrency('EUR')
            ->setQuantity(1)
            ->setPrice($request->get('amount'));

        $itemList = new ItemList();
        $itemList->setItems(array($item1));

        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Onlinebestellung")
            ->setInvoiceNumber($request->get('token'));

        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($this->getHost().'/basket/finish?Data=finish&hash='.$request->get('token'))
            ->setCancelUrl($this->getHost().'/basket/finish?error=Fehler');
        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions(array($transaction));

        try {
            $payment->create($apiContext);
            $approvalUrl = $payment->getApprovalLink();
            return new RedirectResponse($approvalUrl);
        } catch (\Exception $ex) {
            return new RedirectResponse($this->getHost().'/basket/finish?error='.$ex->getMessage());
        }

    }

    public function handleNotify(Request $request)
    {

        $request = $request->getContent();
        $request = json_decode($request, true);

        if(isset($request['resource']) && isset($request['resource']['state']) && $request['resource']['state'] == 'completed')

        /** @var \PSC\Shop\EntityBundle\Document\Order $orderDoc */
        $orderDoc = $this->_doctrine_mongodb
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('paymentRef' => (string)$request['resource']['parent_payment']));

        /** @var Order $order */
        $order = $this->_entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('uid' => $orderDoc->getUid()));

        $order->setStatus(145);
        $this->_entityManager->persist($order);
        $this->_entityManager->flush();

        $notify = new Payed();
        $notify->setShop($this->getShopEntity()->getUID());
        $notify->setOrder($order->getUuid());

        $this->_eventManager->addJob($notify);
    }

    public function doPayment(Request $request)
    {

        $apiContext = new ApiContext(
            new OAuthTokenCredential(
                $this->getGatewaySettings()->getGatewayDocument()->getClientId(),
                $this->getGatewaySettings()->getGatewayDocument()->getClientSecret()
            )
        );

        if($this->getGatewaySettings()->getGatewayDocument()->isProduction()) {
            $apiContext->setConfig(
                array(
                    'mode' => 'live',
                    'log.LogEnabled' => false,
                    'log.FileName' => '../PayPal.log',
                    'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                    'cache.enabled' => true,
                    'cache.FileName' => '/tmp/auth.cache'
                )
            );
        }else {
            $apiContext->setConfig(
                array(
                    'mode' => 'sandbox',
                    'log.LogEnabled' => false,
                    'log.FileName' => '../PayPal.log',
                    'log.LogLevel' => 'DEBUG', // PLEASE USE `FINE` LEVEL FOR LOGGING IN LIVE ENVIRONMENTS
                    'cache.enabled' => true,
                    'cache.FileName' => '/tmp/auth.cache'
                )
            );
        }

        $paymentId = $request->get('paymentId');
        $payment = Payment::get($paymentId, $apiContext);
        $execution = new PaymentExecution();

        $execution->setPayerId($request->get('PayerID'));
        $transaction = new Transaction();
        $amount = new Amount();
        $amount->setCurrency("EUR")
            ->setTotal($request->get('amount'));
        $transaction->setAmount($amount);
        $execution->addTransaction($transaction);
        try {
            $result = $payment->execute($execution, $apiContext);
            try {
                $payment = Payment::get($paymentId, $apiContext);
            } catch (\Exception $ex) {
                return new RedirectResponse($this->getHost().'/basket/finish?error='.$ex->getMessage());
            }
        } catch (\Exception $ex) {
            return new RedirectResponse($this->getHost().'/basket/finish?error='.$ex->getMessage());
        }

        return new RedirectResponse($this->getHost().'/basket/finish?token='.$request->get('hash').'&paymentRef='.$result->getId());

    }
}