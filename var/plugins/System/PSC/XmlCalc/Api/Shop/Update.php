<?php

namespace Plugin\System\PSC\XmlCalc\Api\Shop;

use PSC\Shop\EntityBundle\Entity\Shop as PSCShop;
use Plugin\System\PSC\XmlCalc\Model\Shop;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class Update extends AbstractController {

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * change shop
     *
     * @Route("/shop/{uuid}", methods={"PUT"})
     * @ParamConverter("shopObj", class="\Plugin\System\PSC\XmlCalc\Model\Shop", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="update shop",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Model\Shop::class))
     * )
     * @OA\RequestBody(
     *     description="This is a request body",
     *     @Model(type=\Plugin\System\PSC\XmlCalc\Model\Shop::class)
     * )
     * @OA\Tag(name="Plugin/System/psc/Xmlcalc/Shop")
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     */
    public function update(string $uuid, Shop $shopObj): JsonResponse
    {
        $shop = $this->entityManager->getRepository(PSCShop::class)->findOneBy(['uid' => $uuid]);

        if(!$shop) {
            return $this->json(new NotFound('shop not found'));
        }
        
        if($shopObj->formel !== null) {
            $shop->setFormel($shopObj->formel);
        }
        if($shopObj->formelTest !== null) {
            $shop->setTestFormel($shopObj->formelTest);
        }
        if($shopObj->parameter !== null) {
            $shop->setParameter($shopObj->parameter);
        }
        if($shopObj->parameterTest !== null) {
            $shop->setTestParameter($shopObj->parameterTest);
        }


        $this->entityManager->persist($shop);
        $this->entityManager->flush();

        return $this->json($shopObj);
    }

}
