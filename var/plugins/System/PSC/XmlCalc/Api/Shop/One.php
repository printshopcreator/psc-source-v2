<?php

namespace Plugin\System\PSC\XmlCalc\Api\Shop;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Shop as PSCShop;
use PSC\System\SettingsBundle\Service\Shop;
use Plugin\System\PSC\XmlCalc\Model\Shop as PluginShop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class One extends AbstractController {
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;  
    }
    /**
     * get shop calc settings
     *
     * @Route("/shop/{uuid}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get special shop settings",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Model\Shop::class))
     * )
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     * @OA\Tag(name="Plugin/System/psc/Xmlcalc/Shop")
     * */ 
    public function one(string $uuid): JsonResponse
    {
        $shop = $this->entityManager->getRepository(PSCShop::class)->findOneBy(['uid' => $uuid]);
        if($shop) {
            $output = new PluginShop();
            $output->formel = (string)$shop->getFormel();
            $output->formelTest = (string)$shop->getTestFormel();
            $output->parameter = (string)$shop->getParameter();
            $output->parameterTest = (string)$shop->getTestParameter();
        }else{
            $output = new NotFound("shop not found");
        }
        return $this->json($output);
    }
}
