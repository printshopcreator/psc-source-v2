<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace Plugin\System\PSC\XmlCalc\Api;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Library\Calc\Error\Validation\Input\Max;
use PSC\Library\Calc\Error\Validation\Input\Min as PSCMin;
use Plugin\System\PSC\XmlCalc\Dto\Input\PriceInput;
use Plugin\System\PSC\XmlCalc\Dto\Output\Display\Group as DisplayGroup;
use Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc\Group;
use Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc\Value;
use Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc\Variant;
use Plugin\System\PSC\XmlCalc\Dto\Output\Price\Element;
use Plugin\System\PSC\XmlCalc\Dto\Output\Price\Option;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\Option\Type\Select\Opt;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\PaperDB;
use Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation\Input\Max as PluginMax;
use Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation\Input\Min;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class GetPrice extends AbstractController
{
    private \PSC\System\SettingsBundle\Service\Shop $shopService;
    /**
     * @var DocumentManager
     */
    private DocumentManager $documentManager;
    /**
     * @var PaperDB
     */
    private PaperDB $paperDB;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    private TokenStorageInterface $tokenStorage;

    public function __construct(\PSC\System\SettingsBundle\Service\Shop $shopService,
                                DocumentManager $documentManager,
                                EntityManagerInterface $entityManager,
                                PaperDB $paperDB,
                                TokenStorageInterface $tokenStorage)
    {
        $this->shopService = $shopService;
        $this->documentManager = $documentManager;
        $this->entityManager = $entityManager;
        $this->tokenStorage = $tokenStorage;
        $this->paperDB = $paperDB;
    }

    /**
     * get price
     *
     * @Route("/price", methods={"POST"})
     * @ParamConverter("data", class="\Plugin\System\PSC\XmlCalc\Dto\Input\PriceInput", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="orders",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Dto\Output\PriceOutput::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\Plugin\System\PSC\XmlCalc\Dto\Input\PriceInput::class))
     * )
     * @OA\Tag(name="Plugin/System/psc/Xmlcalc/Price")
     */
    public function getprice(PriceInput $data)
    {

        $output = new \Plugin\System\PSC\XmlCalc\Dto\Output\PriceOutput();
        $output->product = $data->product;

        /** @var Product $product */
        $product = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(['uuid' => $data->product]);

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string($product->getShop()->getInstall()->getPaperContainer()));
        $engine = new Engine();
        $engine->setPaperRepository($this->paperDB);
        $engine->setPaperContainer($paperContainer);
        if($product->getShop()->getInstall()->getCalcTemplates() && !$data->test) {
            $engine->setTemplates('<root>'.$product->getShop()->getInstall()->getCalcTemplates().'</root>');
        }
        if($product->getShop()->getInstall()->getCalcTemplatesTest() && $data->test) {
            $engine->setTemplates('<root>'.$product->getShop()->getInstall()->getCalcTemplatesTest().'</root>');
        }
        $engine->loadString($product->getCalcXml());
        if(!$data->test) {
            $engine->setFormulas($product->getShop()->getFormel());
            $engine->setParameters($product->getShop()->getParameter());
        }
        if($data->test) {
            $engine->setFormulas($product->getShop()->getTestFormel());
            $engine->setParameters($product->getShop()->getTestParameter());
        }
        $engine->setVariables($data->values);
        $engine->setTax($product->getMwert());

        foreach($engine->getArticle()->getDisplayGroups() as $group) {
            $groupObj = new DisplayGroup();
            $groupObj->id = $group->getId();
            $groupObj->name = $group->getName();
            $output->displayGroups[] = $groupObj;
        }

        $output->netto = $engine->getPrice()*100;
        $output->steuer = $engine->getTaxPrice()*100;
        $output->brutto = $engine->getCompletePrice()*100;
        $output->xmlProduct = $engine->getArticle()->getName();
        /** @var Base $option */
        foreach($engine->getArticle()->getOptions() as $option) {
            $tmp = new Element();
            $tmp->name = $option->getName();
            $tmp->rawValue = $option->getRawValue();
            if($option->getDefault()) {
                $tmp->defaultValue = $option->getDefault();
            }
            $tmp->value = $option->getValue();
            $tmp->help = $option->getHelp();
            $tmp->helpLink = $option->getHelpLink();
            $tmp->id = $option->getId();
            $tmp->valid = $option->isValid();
            $tmp->htmlType = $option->type;
            $tmp->displayGroup = $option->getDisplayGroup();
            if($option->type ==  'select') {
                /** @var Opt $option */
                foreach($option->getOptions() as $opt) {
                    $tmpOpt = new Option();
                    $tmpOpt->id = $opt->getId();
                    $tmpOpt->name = $opt->getLabel();
                    $tmpOpt->valid = $opt->isValid();
                    $tmpOpt->selected = $opt->isSelected();

                    $tmp->options[] = $tmpOpt;
                }

            }
            if($option->type == 'input') {
                $tmp->minValue = $option->getMinValue();
                $tmp->maxValue = $option->getMaxValue(); 
                foreach($option->getValidationErrors() as $error) {
                    if($error instanceof  PSCMin) {
                        $tmp->validationErrors[] = new Min($tmp->value, $option->getMinValue());
                    }
                    if($error instanceof  Max) {
                        $tmp->validationErrors[] = new PluginMax($tmp->value, $option->getMaxValue());
                    }

                }
            }

            $output->elements[] = $tmp;
        }
        
        foreach($engine->getArticle()->getPreCalc()->getGroups() as $group) {
            $groupObj = new Group();
            $groupObj->name = $group->getName(); 
            foreach($group->getVariants() as $variant) {
                $variantObj = new Variant();
                $variantObj->name = $variant->getName();
                foreach($variant->getValues() as $value) {
                    $valueObj = new Value();
                    $valueObj->key = $value->getKey();
                    $valueObj->value = $value->getValue();
                    $variantObj->values[] = $valueObj;
                }
                $groupObj->variants[] = $variant;
            }
            $output->preCalc[] = $groupObj;
        }

        $output->displayValues = $engine->getDisplayVariables();
        $output->exportValues = $engine->getAjaxVariables();

        if($this->isGranted("ROLE_SHOP")) {
            $output->debug['formels'] = $engine->getDebugCalcFormel();
            $output->debug['flatPrice'] = $engine->getDebugFlatPrice();
            $output->debug['price'] = $engine->getDebugPrice();
            $output->debug['calcValues'] = $engine->getDebugCalcVariables();
        }
        return $this->json($output);
    }
}
