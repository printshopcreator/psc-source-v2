<?php

namespace Plugin\System\PSC\XmlCalc\Api\System;

use PSC\Shop\EntityBundle\Entity\Install;
use Plugin\System\PSC\XmlCalc\Model\System;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class Update extends AbstractController {

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * change system
     *
     * @Route("/system", methods={"PUT"})
     * @ParamConverter("systemObj", class="\Plugin\System\PSC\XmlCalc\Model\System", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="update product",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Model\System::class))
     * )
     * @OA\RequestBody(
     *     description="This is a request body",
     *     @Model(type=\Plugin\System\PSC\XmlCalc\Model\System::class)
     * )
     * @OA\Tag(name="Plugin/System/psc/Xmlcalc/System")
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     */
    public function update(System $systemObj): JsonResponse
    {
        $system = $this->entityManager->getRepository(Install::class)->findOneBy(['uid' => 1]);

        if(!$system) {
            return $this->json(new NotFound('system not found'));
        }
        
        if($systemObj->calcTemplates !== null) {
            $system->setCalcTemplates($systemObj->calcTemplates);
        }
        if($systemObj->calcTemplatesTest !== null) {
            $system->setCalcTemplatesTest($systemObj->calcTemplatesTest);
        }

        $this->entityManager->persist($system);
        $this->entityManager->flush();

        return $this->json($systemObj);
    }

}
