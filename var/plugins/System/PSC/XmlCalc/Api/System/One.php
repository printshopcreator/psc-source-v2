<?php

namespace Plugin\System\PSC\XmlCalc\Api\System;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Install;
use PSC\System\SettingsBundle\Service\Shop;
use Plugin\System\PSC\XmlCalc\Model\System;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class One extends AbstractController {
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;  
    }
    /**
     * get system calc settings
     *
     * @Route("/system", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get system calc settings",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Model\System::class))
     * )
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     * @OA\Tag(name="Plugin/System/psc/Xmlcalc/System")
     * */ 
    public function one(): JsonResponse
    {
        $system = $this->entityManager->getRepository(Install::class)->findOneBy(['uid' => 1]);
        if($system) {
            $output = new System();
            $output->calcTemplates = $system->getCalcTemplates();
            $output->calcTemplatesTest = $system->getCalcTemplatesTest();
        }else{
            $output = new NotFound("settings not found");
        }
        return $this->json($output);
    }
}
