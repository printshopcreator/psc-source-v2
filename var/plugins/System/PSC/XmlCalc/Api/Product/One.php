<?php

namespace Plugin\System\PSC\XmlCalc\Api\Product;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Shop;
use Plugin\System\PSC\XmlCalc\Model\Product as PluginProduct;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class One extends AbstractController {
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;  
    }
    /**
     * get special product
     *
     * @Route("/product/{uuid}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get special product",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Model\Product::class))
     * )
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     * @OA\Tag(name="Plugin/System/psc/Xmlcalc/Product")
     * */ 
    public function one(string $uuid): JsonResponse
    {
        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['uuid' => $uuid]);
        if($product) {
            $output = new PluginProduct();
            $output->calcXml = $product->getCalcXml();
        }else{
            $output = new NotFound("product not found");
        }
        return $this->json($output);
    }
}
