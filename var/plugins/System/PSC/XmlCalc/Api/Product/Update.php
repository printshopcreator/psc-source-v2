<?php

namespace Plugin\System\PSC\XmlCalc\Api\Product;

use PSC\Shop\EntityBundle\Entity\Product as PSCProduct;
use Plugin\System\PSC\XmlCalc\Model\Product;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class Update extends AbstractController {

    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * change product
     *
     * @Route("/product/{uuid}", methods={"PUT"})
     * @ParamConverter("productObj", class="\Plugin\System\PSC\XmlCalc\Model\Product", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="update product",
     *     @OA\JsonContent(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Model\Product::class))
     * )
     * @OA\RequestBody(
     *     description="This is a request body",
     *     @Model(type=\Plugin\System\PSC\XmlCalc\Model\Product::class)
     * )
     * @OA\Tag(name="Plugin/System/psc/Xmlcalc/Product")
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     */
    public function update(string $uuid, Product $productObj): JsonResponse
    {
        $product = $this->entityManager->getRepository(PSCProduct::class)->findOneBy(['uuid' => $uuid]);

        if(!$product) {
            return $this->json(new NotFound('paper not found'));
        }
        
        if($productObj->calcXml !== null) {
            $product->setCalcXml($productObj->calcXml);
        }

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $this->json($productObj);
    }

}
