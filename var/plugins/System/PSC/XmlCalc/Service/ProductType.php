<?php
namespace Plugin\System\PSC\XmlCalc\Service;


use Plugin\System\PSC\XmlCalc\Producer\JsonForm;
use Plugin\System\PSC\XmlCalc\Transformer\Position;
use PSC\Shop\ProductBundle\Interfaces\IJsonFormProducer;
use PSC\Shop\ProductBundle\Model\Price;
use PSC\Shop\ProductBundle\Model\Product;

class ProductType implements \PSC\System\PluginBundle\Product\Type {


    private Position $productTransformer;
    private JsonForm $jsonFormProducer;

    public function __construct(Position $positionTransformer, JsonForm $jsonFormProducer)
    {
        $this->productTransformer = $positionTransformer;
        $this->jsonFormProducer = $jsonFormProducer;
    }

    public function getId()
    {
        return 6;
    }

    public function getName()
    {
        return 'XML Kalkulationsprodukt';
    }

    public function getPositionProductTransformer()
    {
        return $this->productTransformer;
    }

    public function getJsonFormProducer(): IJsonFormProducer
    {
        return $this->jsonFormProducer;
    }
}