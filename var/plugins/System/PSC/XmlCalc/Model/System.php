<?php

namespace Plugin\System\PSC\XmlCalc\Model;

use OpenApi\Annotations as OA;

class System {

    /**
     * @OA\Property(type="string")
     */
    public ?string $calcTemplates = null;
    
    /**
     * @OA\Property(type="string")
     */
    public ?string $calcTemplatesTest = null;

}
