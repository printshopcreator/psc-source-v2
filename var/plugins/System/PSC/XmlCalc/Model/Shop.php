<?php

namespace Plugin\System\PSC\XmlCalc\Model;

use OpenApi\Annotations as OA;

class Shop {

    /**
     * @OA\Property(type="string")
     */
    public ?string $formel = null;
    
    /**
     * @OA\Property(type="string")
     */
    public ?string $parameter = null;

    /**
     * @OA\Property(type="string")
     */
    public ?string $formelTest = null;
    
    /**
     * @OA\Property(type="string")
     */
    public ?string $parameterTest = null;


}
