<?php

namespace Plugin\System\PSC\XmlCalc\Model;

use OpenApi\Annotations as OA;

class Product {

    /**
     * @OA\Property(type="string")
     */
    public string $calcXml;

}
