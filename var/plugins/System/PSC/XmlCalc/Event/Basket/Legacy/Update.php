<?php

namespace Plugin\System\PSC\XmlCalc\Event\Basket\Legacy;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\BasketBundle\Event\Legacy\Update as PSCUpdate;
use PSC\System\SettingsBundle\Service\PaperDB;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class Update implements EventSubscriberInterface
{
    private EntityManagerInterface $entityManager;
    private PaperDB $paperDB;

    public function __construct(EntityManagerInterface $entityManager,
                                PaperDB $paperDB)
    {
        $this->entityManager = $entityManager;
        $this->paperDB = $paperDB;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            PSCUpdate::NAME => 'updateProduct',
        ];
    }

    public function updateProduct(PSCUpdate $event): void
    {
        $basketItem = $_SESSION['Basket']['TempProduct'][$event->getData()->productUUId];
        $basketItem->setCount($event->getData()->count);
        $basketItem->setUploadMode($event->getData()->uploadMode); 
        $basketItem->setOptions($event->getData()->values);

        /** @var Product $product */
        $product = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(['uuid' => $event->getData()->productUUId]);

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string($product->getShop()->getInstall()->getPaperContainer()));
        $engine = new Engine();
        $engine->setPaperRepository($this->paperDB);
        $engine->setPaperContainer($paperContainer);
        if($product->getShop()->getInstall()->getCalcTemplates()) {
            $engine->setTemplates('<root>'.$product->getShop()->getInstall()->getCalcTemplates().'</root>');
        }
        $engine->loadString($product->getCalcXml());
        $engine->setFormulas($product->getShop()->getFormel());
        $engine->setParameters($product->getShop()->getParameter());
        $engine->setVariables($event->getData()->values);
        $engine->setTax($product->getMwert());
        $basketItem->setNetto($engine->getPrice());
        $basketItem->setBrutto($engine->getCompletePrice());
        $basketItem->setSteuer($engine->getTaxPrice());
        $basketItem->setWeight($engine->getWeight());
        $basketItem->setWeightSingle($engine->getWeightSingle());
        $basketItem->setCalcValues($engine->getCalcVariables());
         
    }
}
