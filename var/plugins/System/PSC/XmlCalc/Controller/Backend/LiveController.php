<?php
namespace Plugin\System\PSC\XmlCalc\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManager;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Plugin\System\PSC\XmlCalc\Form\Calc;
use Plugin\System\PSC\XmlCalc\Form\Edit\ShopPFType;
use Plugin\System\PSC\XmlCalc\Form\Edit\XmlType;
use PSC\Library\Calc\Option\Type\Select\Opt;
use PSC\Shop\EntityBundle\Entity\Install;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\OrderBundle\Form\Backend\OrderStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Tracking;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactDeliveryType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactInvoiceType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactSenderType;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\System\SettingsBundle\Service\PaperDB;
use Ramsey\Uuid\Uuid;
use Sauladam\ShipmentTracker\ShipmentTracker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/**
 * Class LiveController
 *
 * @package PSC\Component\Production
 * @Route("/live")
 */
class LiveController extends AbstractController
{
    /**
     * @Template()
     * @Route("/index/{uuid}/{test}", name="psc_plugin_system_xmlcalc_backend_live_index")
     * @Security("is_granted('ROLE_ADMIN')")
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @param EntityManagerInterface $entityManager
     * @param PaperDB $paperDB
     * @param $uuid
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(
        Request $request,
        JWTTokenManagerInterface $jwtManager,
        $uuid, $test = false)
    {

        return array(
            'test' => $test,
            'product' => $uuid,
            'jwt' => $jwtManager->create($this->getUser())
        );
    }



}
