<?php
namespace Plugin\System\PSC\XmlCalc\Transformer\Model;

use PSC\Library\Calc\Engine;
use PSC\Shop\OrderBundle\Model\Order\Position\IProductTypeObject;

class Product implements IProductTypeObject {

    private Engine $engine;

    private \ArrayIterator $options;

    public function getName()
    {
        return 'XML Kalkulationsprodukt';
    }

    public function getTyp()
    {
        return 6;
    }

    /**
     * @return Engine
     */
    public function getEngine(): Engine
    {
        return $this->engine;
    }

    /**
     * @param Engine $engine
     */
    public function setEngine(Engine $engine): void
    {
        $this->engine = $engine;
    }

    /**
     * @param \ArrayIterator $options
     */
    public function setOptions(\ArrayIterator $options): void
    {
        $this->options = $options;
    }

    /**
     * @return \ArrayIterator
     */
    public function getOptions(): \ArrayIterator
    {
        return $this->options;
    }
}