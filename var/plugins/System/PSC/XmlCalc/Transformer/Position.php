<?php
namespace Plugin\System\PSC\XmlCalc\Transformer;

use Doctrine\ODM\MongoDB\DocumentManager;
use Plugin\System\PSC\XmlCalc\Transformer\Model\Product;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Embed\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\OrderBundle\Model\Order\Position\IProductTypeObject;
use PSC\Shop\OrderBundle\Transformer\Order\Position\IProductTransformer;
use PSC\System\SettingsBundle\Service\PaperDB;
use PSC\System\SettingsBundle\Service\Shop;

class Position implements IProductTransformer {

    private Shop $shopService;
    private PaperDB $paperService;
    private DocumentManager $mongoService;

    public function __construct(Shop $shopService, PaperDB $paperService, DocumentManager $documentManager)
    {
        $this->shopService = $shopService;
        $this->paperService = $paperService;
        $this->mongoService = $documentManager;
    }

    public function toDocument()
    {
        // TODO: Implement toDocument() method.
    }

    public function fromDocument()
    {
        // TODO: Implement fromDocument() method.
    }

    public function toEntity()
    {
        // TODO: Implement toEntity() method.
    }

    public function fromEntity(\PSC\Shop\OrderBundle\Model\Order\Position $position, Orderpos $pos)
    {
        $obj = new Product();

        $objPosition = unserialize(($position->getData()));

        $selectedShop = $pos->getShop();

        if($pos->hasCalcXml()) {

            /** @var \PSC\Shop\EntityBundle\Document\Position $objDoc */
            $objDoc = $this->mongoService
                ->getRepository('PSCEntityBundle:Position')
                ->findOneBy(['uid' => (string)$pos->getId()]);

            $paperContainer = new PaperContainer();
            $paperContainer->parse(simplexml_load_string($selectedShop->getInstall()->getPaperContainer()));
            $engine = new Engine();
            $engine->setPaperRepository($this->paperService);
            $engine->setPaperContainer($paperContainer);
            if($selectedShop->getInstall()->getCalcTemplates()) {
                $engine->setTemplates('<root>'.$selectedShop->getInstall()->getCalcTemplates().'</root>');
            }
            $engine->loadString($pos->getCalcXml());
            $engine->setFormulas($selectedShop->getFormel());
            $engine->setParameters($selectedShop->getParameter());
            $engine->setVariables($objPosition->getOptions());
            if($objDoc) {
                $engine->setSavedCalcReferences($objDoc->getCalcReferences());
            }
            if(isset($objPosition->getOptions()['kalk_artikel'])) {
                $engine->setActiveArticle($objPosition->getOptions()['kalk_artikel']);
            }
            $articleCalc = $engine->getArticle();

            if($articleCalc) {
                $obj->setOptions($articleCalc->getOptions());
            }else{
                $obj->setOptions(new \ArrayIterator(['error' => true]));
            }
            if($articleCalc->getOptionById('auflage')) {
                $position->setCount($articleCalc->getOptionById('auflage')->getRawValue());
            }
        }else{
            $obj->setOptions(new \ArrayIterator());
        }
        $position->setSpecialProductTypeObject($obj);
    }
}