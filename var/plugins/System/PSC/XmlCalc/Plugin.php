<?php
namespace Plugin\System\PSC\XmlCalc;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'XML Kalkulations Produkt';

    public function getType()
    {
        return Plugin::ProductType;
    }

    public function getDescription()
    {
        return 'XML basiertes Kalkulations Produkt';
    }

    public function getVersion()
    {
        return 1;
    }
}