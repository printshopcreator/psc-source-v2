<?php
namespace Plugin\System\PSC\XmlCalc\Producer;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Select;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\ProductBundle\Interfaces\IJsonFormProducer;
use PSC\Shop\ProductBundle\Model\Price;
use PSC\Shop\ProductBundle\Model\Product;
use PSC\System\SettingsBundle\Service\PaperDB;
use PSC\System\SettingsBundle\Service\Shop;

class JsonForm implements IJsonFormProducer {

    private Shop $shopService;
    private PaperDB $paperService;
    private DocumentManager $mongoService;
    private Product $product;
    private Engine $engine;
    private EntityManagerInterface $entityManager;

    public function __construct(Shop $shopService, PaperDB $paperService, DocumentManager $documentManager, EntityManagerInterface $entityManager)
    {
        $this->shopService = $shopService;
        $this->paperService = $paperService;
        $this->mongoService = $documentManager;
        $this->entityManager = $entityManager;
    }

    public function getPrice(): Price
    {
        $price = new Price();
        $price->net = $this->engine->getPrice();
        $price->vat = $this->engine->getTaxPrice();
        $price->gross = $this->engine->getCompletePrice();
        $price->allNet = 100;
        $price->allVat = 19;
        $price->allGross = 119;

        return $price;
    }

    public function getJsonForm(): array
    {

        $temp = [
            'title' => $this->product->title,
            'type' => 'object',
            'properties' => [],
            'required' => []
        ];


        foreach($this->engine->getArticle()->getOptions() as $option) {

            if($option->isRequire()) {
                $temp['required'][] = $option->getId();
            }

            if($option instanceof Select) {
                $opts = [];
                foreach($option->getValidOptions() as $opt) {
                    $opts[] = [
                        'const' => $opt->getId(),
                        'title' => $opt->getLabel()
                    ];
                }

                $temp['properties'][$option->getId()] = [
                    'type' => 'string',
                    'title' => $option->getName(),
                    'oneOf' => $opts,
                    'default' => $option->getSelectedOption()->getId()
                ];
            }else{
                $temp['properties'][$option->getId()] = [
                    'type' => 'string',
                    'title' => $option->getName(),
                    'default' => $option->getDefault()
                ];
            }
        }

        return $temp;
    }

    public function setProduct(Product $product): void
    {
        $this->product = $product;
        $this->process();
    }

    private function process(): void
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Product $product */
        $product = $this->entityManager->getRepository(\PSC\Shop\EntityBundle\Entity\Product::class)->find($this->product->id);

        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string($product->getShop()->getInstall()->getPaperContainer()));
        $engine = new Engine();
        $engine->setPaperRepository($this->paperService);
        $engine->setPaperContainer($paperContainer);
        if($product->getShop()->getInstall()->getCalcTemplates()) {
            $engine->setTemplates('<root>'.$product->getShop()->getInstall()->getCalcTemplates().'</root>');
        }
        $engine->loadString($product->getCalcXml());
        $engine->setFormulas($product->getShop()->getFormel());
        $engine->setParameters($product->getShop()->getParameter());

        $this->engine = $engine;
    }
}