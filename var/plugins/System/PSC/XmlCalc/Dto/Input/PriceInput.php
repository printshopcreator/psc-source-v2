<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Input;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class PriceInput {

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $product;

    /**
     * @var bool
     *
     * @OA\Property(type="boolean")
     */
    public bool $test = false;

    /**
     * @var array
     *
     * @OA\Property(type="array", @OA\Items(type="string"))
     */
    public array $values = ['auflage' => 100];

}