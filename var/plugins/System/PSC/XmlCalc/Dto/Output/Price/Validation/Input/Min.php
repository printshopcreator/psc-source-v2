<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation\Input;

use Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation\Error;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Min extends Error {

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $minValue = 0;

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $currentValue = 0;

    public function __construct(int $currentValue, int $minValue)
    {
        $this->currentValue = $currentValue;
        $this->minValue = $minValue;
        $this->message = "value must be greater then " . $this->minValue;
        $this->type = 'input::validation::min';
    } 

}
