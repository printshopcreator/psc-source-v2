<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

abstract class Error {
    
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $type;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $message;

}
