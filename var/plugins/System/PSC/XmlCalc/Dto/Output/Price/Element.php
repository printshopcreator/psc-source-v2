<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\Price;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Element
{

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $id;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $name;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $displayGroup = "";

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $defaultValue = "";

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $value;

    /**
     * @OA\Property(type="integer")
     */
    public ?int $minValue;

    /**
     * @OA\Property(type="integer")
     */
    public ?int $maxValue;

    /**
     * @OA\Property(type="string")
     */
    public ?string $help;

    /**
     * @OA\Property(type="string")
     */
    public ?string $helpLink;


    /**
     * @var bool
     *
     * @OA\Property(type="boolean")
     */
    public bool $valid = true;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $rawValue;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $type;

    /**
     * @var Option[] Optionen
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Dto\Output\Price\Option::class)))
     */
    public array $options = [];

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $htmlType = 'input';
    
    /** @var Error[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation\Error::class)))
     */
    public array $validationErrors = [];

}
