<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\Price;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class Option
{

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $id;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $name;

    /**
     * @var bool
     *
     * @OA\Property(type="boolean")
     */
    public bool $valid = true;

    /**
     * @var bool
     *
     * @OA\Property(type="boolean")
     */
    public bool $selected = false;

}
