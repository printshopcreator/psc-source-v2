<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation\Input;

use Plugin\System\PSC\XmlCalc\Dto\Output\Price\Validation\Error;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Max extends Error {

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $maxValue = 0;

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $currentValue = 0;

    public function __construct(int $currentValue, int $maxValue)
    {
        $this->currentValue = $currentValue;
        $this->maxValue = $maxValue;
        $this->message = "value must be lower then " . $this->maxValue;
        $this->type = 'input::validation::max';
    } 

}
