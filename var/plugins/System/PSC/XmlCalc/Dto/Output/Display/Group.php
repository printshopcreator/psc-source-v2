<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\Display;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Group
{

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $name;

   /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $id;

}
