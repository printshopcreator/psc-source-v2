<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Value
{

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $key;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $value;


}
