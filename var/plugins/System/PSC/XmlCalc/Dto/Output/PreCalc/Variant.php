<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Variant
{

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $name;

    /**
     * @var Variant[] options
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc\Value::class)))
     */
    public array $values = [];

}
