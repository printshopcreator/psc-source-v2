<?php

namespace Plugin\System\PSC\XmlCalc\Dto\Output;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Plugin\System\PSC\XmlCalc\Dto\Output\Price\Element;
use Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc\Group;

final class PriceOutput {

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $uuid;

    /**
     * @var array
     *
     * @OA\Property(type="array", @OA\Items(type="array", @OA\Items()))
     */
    public array $debug;
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $product;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $xmlProduct;

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $netto;

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $steuer;

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $brutto;

    /** @var Element[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Dto\Output\Price\Element::class)))
     */
    public array $elements = [];

/** @var Group[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Dto\Output\Display\Group::class)))
     */
    public array $displayGroups = [];


     /** @var Group[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\Plugin\System\PSC\XmlCalc\Dto\Output\PreCalc\Group::class)))
     */
    public array $preCalc = [];

    /**
     * @var array
     *
     * @OA\Property(type="array", @OA\Items(type="array", @OA\Items()))
     */
    public array $displayValues = [];

    /**
     * @var array
     *
     * @OA\Property(type="array", @OA\Items(type="array", @OA\Items()))
     */
    public array $exportValues = [];

}
