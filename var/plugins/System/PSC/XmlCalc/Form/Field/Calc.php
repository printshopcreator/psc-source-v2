<?php

namespace Plugin\System\PSC\XmlCalc\Form\Field;

use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use PSC\System\SettingsBundle\Service\General;
use PSC\System\SettingsBundle\Service\Tax;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Calc implements Field
{

    protected $generalService;
    protected $taxService;

    public function __construct(General $generalService, Tax $taxService)
    {
        $this->generalService = $generalService;
        $this->taxService = $taxService;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCXmlCalc/form/field/calc.html.twig';
    }

    public function getModule()
    {
        return Field::Product;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {


    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add("mwert", ChoiceType::class, array('label' => 'MwSt.',
            'choices' => $this->taxService->getTaxesForForm(),
            ))
            ->add('fromPrice', TextType::class, ['required' => false, 'label' => 'Ab Preis'])
            ->add('calcXml', AceEditorType::class, array(
            'wrapper_attr' => array(), // aceeditor wrapper html attributes.
            'width' => '90%',
            'height' => '500',
            'font_size' => 14,
            'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
            'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
            'tab_size' => null,
            'read_only' => null,
            'use_soft_tabs' => null,
            'use_wrap_mode' => null,
            'show_print_margin' => null,
            'required' => false,
            'highlight_active_line' => null
        ));

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\XmlCalc\Form\Group\Calc::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        /** @var Product $data */
        $data = $event->getData();
        $event->getForm()->get('xmlCalc')->get('calcXml')->setData($data->getCalcXml());
        $event->getForm()->get('xmlCalc')->get('mwert')->setData($data->getMwert());
        $event->getForm()->get('xmlCalc')->get('fromPrice')->setData($data->getFromPrice());
    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Product $data */
        $data = $event->getData();
        $data->setCalcXml($event->getForm()->get('xmlCalc')->get('calcXml')->getData());
        $data->setMwert($event->getForm()->get('xmlCalc')->get('mwert')->getData());
        $data->setFromPrice($event->getForm()->get('xmlCalc')->get('fromPrice')->getData());
        $event->setData($data);
    }

    public function formPreSetData(FormEvent $event)
    {

    }
}