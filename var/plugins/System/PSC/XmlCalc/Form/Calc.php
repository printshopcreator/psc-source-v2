<?php

namespace Plugin\System\PSC\XmlCalc\Form;

use PSC\Library\Calc\Engine;
use PSC\Library\Calc\Option\Type\Base;
use PSC\Library\Calc\Option\Type\Hidden;
use PSC\Library\Calc\Option\Type\Input;
use PSC\Library\Calc\Option\Type\Select;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Calc extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Engine $engine */
        $engine = $options['engine'];

        if(count($engine->getArticles()) > 1) {
            $builder->add('kalk_artikel', ChoiceType::class,[
                'label' => 'Artikelauswahl',
                'choices' => $engine->getArticles(),
                'choice_label' => 'name',
                'choice_value' => 'name',

            ]);

        }else{
            $builder->add('kalk_artikel', HiddenType::class,[
                'data' => $engine->getArticle()->getName()
            ]);
        }

        $article = $engine->getArticle();

        /** @var Base $option */
        foreach($article->getOptions() as $option) {
            if ($option->isValid()) {

                if ($option instanceof Input) {
                    $builder->add($option->getId(), TextType::class, [
                        'required' => $option->isRequire(),
                        'label' => $option->getName(),
                        'data' => $option->getValue()
                    ]);
                }
                if ($option instanceof Hidden) {
                    $builder->add($option->getId(), HiddenType::class, [
                        'data' => $option->getValue()
                    ]);
                }
                if ($option instanceof Select) {
                    $builder->add($option->getId(), ChoiceType::class, [
                        'label' => $option->getName(),
                        'choices' => $option->getValidOptions(),
                        'choice_label' => 'label',
                        'choice_value' => 'id',
                        'data' => $option->getValue(),

                    ]);
                }
            }
        }

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($engine) {
            $data = $event->getData();
            $form = $event->getForm();

            $engine->setVariables($data);
            if ($data) {
                $engine->setActiveArticle($data['kalk_artikel']);
            }

            $article = $engine->getArticle();

            foreach($form->all() as $element) {
                if($element->getName() != 'kalk_artikel') {
                    $form->remove($element->getName());
                }
            }

            /** @var Base $option */
            foreach($article->getOptions() as $option) {
                if($option->isValid()) {

                    if($option instanceof Input) {
                        $form->add($option->getId(), TextType::class, [
                            'required' => $option->isRequire(),
                            'label' => $option->getName(),
                            'data' => $option->getValue()
                        ]);
                    }
                    if($option instanceof Hidden) {
                        $form->add($option->getId(), HiddenType::class, [
                            'data' => $option->getValue()
                        ]);
                    }
                    if($option instanceof Select) {
                        $form->add($option->getId(), ChoiceType::class,[
                            'label' => $option->getName(),
                            'choices' => $option->getOptions(),
                            'choice_label' => 'label',
                            'choice_value' => 'id',
                            'data' => $option->getValue(),

                        ]);
                    }
                }
            }
        });
    }



    public function getName()
    {
        return 'psc_plugin_form_calc';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'engine' => null,
            'allow_extra_fields' => true
        ));
    }

}