<?php

namespace Plugin\System\PSC\XmlCalc\Form\Group;

use PSC\System\PluginBundle\Form\Group;
use PSC\System\PluginBundle\Form\Interfaces\Field;

class Calc extends Group
{
    const GROUP_ID = 'xmlCalc';

    public function __construct()
    {
        $this->title = 'Kalkulation';
    }

    public function getModule()
    {
        return Field::Product;
    }

    public function getId()
    {
        return self::GROUP_ID;
    }
}