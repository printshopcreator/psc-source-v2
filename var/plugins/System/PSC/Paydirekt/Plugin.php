<?php
namespace Plugin\System\PSC\Paydirekt;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Paydirekt Payment';

    public function getType()
    {
        return Plugin::Payment;
    }

    public function getDescription()
    {
        return 'Stellt Unterstützung für Paydirekt bereit';
    }

    public function getVersion()
    {
        return 1;
    }
}