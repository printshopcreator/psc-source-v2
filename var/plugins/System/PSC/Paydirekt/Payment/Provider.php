<?php
namespace Plugin\System\PSC\Paydirekt\Payment;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Paydirekt\Client\Checkout\CheckoutClient;
use Paydirekt\Client\Rest\EndpointConfiguration;
use Paydirekt\Client\Rest\RequestBuilderFactory;
use Paydirekt\Client\Rest\RequestExecutor;
use Paydirekt\Client\Security\SecurityClient;
use Paydirekt\Client\TestUtil\RequestMocks;
use Plugin\System\PSC\Paydirekt\Document\Paydirekt;
use PSC\Shop\EntityBundle\Document\Order;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Provider\PaymentProvider;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;
use Sofort\SofortLib\Notification;
use Sofort\SofortLib\TransactionData;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Provider extends PaymentProvider
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
    }

    public function getName()
    {
        return 'Paydirekt';
    }

    public function getType()
    {
        return 'paydirekt';
    }

    public function saveDocument(Gatewaysettings $settings, Form $form)
    {
        $doc = new Paydirekt();

        $doc->setApiKey($form->get('paydirekt')->get('apiKey')->getData());
        $doc->setApiSecret($form->get('paydirekt')->get('apiSecret')->getData());
        $doc->setProduction($form->get('paydirekt')->get('production')->getData());
        $doc->setStoreName($form->get('paydirekt')->get('storeName')->getData());

        $settings->setGatewayDocument($doc);

        return $settings;
    }

    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder)
    {

        if(!$settings->getGatewayDocument()) {
            $settings->setGatewayDocument(new Paydirekt());
        }

        $builder
            ->add('production', CheckboxType::class, array('label' => 'Production?', 'required' => false))
            ->add('apiKey', TextType::class, array('label' => 'Api Key', 'required' => false))
            ->add('storeName', TextType::class, array('label' => 'Store Name', 'required' => false))
            ->add('apiSecret', TextType::class, array('label' => 'Api Secret', 'required' => false));

        $builder->get('production')->setData($settings->getGatewayDocument()->isProduction());
        $builder->get('apiKey')->setData($settings->getGatewayDocument()->getApiKey());
        $builder->get('storeName')->setData($settings->getGatewayDocument()->getStoreName());
        $builder->get('apiSecret')->setData($settings->getGatewayDocument()->getApiSecret());

        return $builder;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCPaydirekt/settings.html.twig';
    }

    public function handlePayment(Request $request)
    {
        /** @var Paydirekt $doc */
        $doc = $this->getGatewaySettings()->getGatewayDocument();

        try {

            $securityClient = new SecurityClient(EndpointConfiguration::getTokenObtainEndpoint(),
                EndpointConfiguration::API_KEY,
                EndpointConfiguration::API_SECRET,
                EndpointConfiguration::getCaFile());

            if($doc->isProduction()) {
                $securityClient = new SecurityClient(EndpointConfiguration::PRODUCTION_TOKEN_OBTAIN_ENDPOINT,
                    $doc->getApiKey(),
                    $doc->getApiSecret(),
                    EndpointConfiguration::getCaFile());
            }



            $accessToken = $securityClient->getAccessToken();
            $accessToken = $accessToken['access_token'];

            $checkoutClient = new CheckoutClient(EndpointConfiguration::SANDBOX_CHECKOUT_ENDPOINT);

            if($doc->isProduction()) {
                $checkoutClient = new CheckoutClient(EndpointConfiguration::PRODUCTION_CHECKOUT_ENDPOINT);
            }

            $checkoutRequest = json_encode(array(
                'express' => true,
                'callbackUrlCheckDestinations' =>  $this->getHost(),
                'webUrlShippingTerms' =>  $this->getHost(),
                'type' => 'DIRECT_SALE',
                'totalAmount' => $request->get('amount'),
                'shippingAmount' => $request->get('amountShipping'),
                'orderAmount' => $request->get('amountOrder'),
                'currency' => 'EUR',
                'merchantInvoiceReferenceNumber' => $request->get('hash'),
                'note' => $this->getGatewaySettings()->getGatewayDocument()->getStoreName(),
                'minimumAge' => 18,
                'redirectUrlAfterSuccess' => $this->getHost().'/basket/finish?Data=finish&token='.$request->get('hash'),
                'redirectUrlAfterCancellation' => $this->getHost().'/basket/finish',
                'redirectUrlAfterAgeVerificationFailure' => $this->getHost().'/basket/finish?error=Alter',
                'redirectUrlAfterRejection' => $this->getHost().'/basket/finish?error=Abgelehnt'
            ));
            $checkout = $checkoutClient->createCheckout($checkoutRequest, $accessToken);

            $session = $request->getSession();

            $session->set('paydirektCheckoutId', $checkout['checkoutId']);

            return new RedirectResponse($checkout['_links']['approve']['href']);
        } catch (\Exception $ex) {
            return new RedirectResponse($this->getHost().'/basket/finish?error='.$ex->getMessage());
        }

    }

    public function handleNotify(Request $request)
    {


    }
    public function doPayment(Request $request)
    {

        /** @var Paydirekt $doc */
        $doc = $this->getGatewaySettings()->getGatewayDocument();

        try {

            $session = $request->getSession();

            $securityClient = new SecurityClient(EndpointConfiguration::getTokenObtainEndpoint(),
                EndpointConfiguration::API_KEY,
                EndpointConfiguration::API_SECRET,
                EndpointConfiguration::getCaFile());

            if($doc->isProduction()) {
                $securityClient = new SecurityClient(EndpointConfiguration::PRODUCTION_TOKEN_OBTAIN_ENDPOINT,
                    $doc->getApiKey(),
                    $doc->getApiSecret(),
                    EndpointConfiguration::getCaFile());
            }

            $accessToken = $securityClient->getAccessToken();
            $accessToken = $accessToken['access_token'];

            $checkoutClient = new CheckoutClient(EndpointConfiguration::SANDBOX_CHECKOUT_ENDPOINT);
            $request = RequestBuilderFactory::newGetRequestBuilder(EndpointConfiguration::SANDBOX_CHECKOUT_ENDPOINT.'/'.$session->get('paydirektCheckoutId') . '/execute');

            if($doc->isProduction()) {
                $checkoutClient = new CheckoutClient(EndpointConfiguration::PRODUCTION_CHECKOUT_ENDPOINT);
                $request = RequestBuilderFactory::newGetRequestBuilder(EndpointConfiguration::PRODUCTION_CHECKOUT_ENDPOINT.'/'.$session->get('paydirektCheckoutId') . '/execute');
            }

            $checkout = $checkoutClient->getCheckout($session->get('paydirektCheckoutId'),  $accessToken);

            $executeLink = $checkout['_links']['execute']['href'];

            $request = $request
                ->withStandardHeaders($accessToken)
                ->build();

            RequestExecutor::executeRequest($request, true);

        } catch (\Exception $ex) {
            return new RedirectResponse($this->getHost().'/basket/finish?error='.$ex->getMessage());
        }

        return new RedirectResponse($this->getHost().'/basket/finish?payed=1&token='.$request->get('token'));

    }
}