<?php
namespace Plugin\System\PSC\Paydirekt\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Paydirekt
{
    /**
     * @var string $apiKey
     *
     * @Field(type="string")
     */
    protected $apiKey;

    /**
     * @var string $apiSecret
     *
     * @Field(type="string")
     */
    protected $apiSecret;

    /**
     * @var string $storeName
     *
     * @Field(type="string")
     */
    protected $storeName;

    /**
     * @var boolean $production
     *
     * @Field(type="bool")
     */
    protected $production;

    /**
     * @return boolean
     */
    public function isProduction()
    {
        return $this->production;
    }

    /**
     * @param boolean $production
     */
    public function setProduction($production)
    {
        $this->production = $production;
    }

    /**
     * @return string
     */
    public function getApiSecret()
    {
        return $this->apiSecret;
    }

    /**
     * @param string $apiSecret
     */
    public function setApiSecret($apiSecret)
    {
        $this->apiSecret = $apiSecret;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return string
     */
    public function getStoreName()
    {
        return $this->storeName;
    }

    /**
     * @param string $storeName
     */
    public function setStoreName($storeName)
    {
        $this->storeName = $storeName;
    }
}