<?php
namespace Plugin\System\PSC\Ftp\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Ftp
{
    /**
     * @var string $host
     *
     * @Field(type="string")
     */
    protected $host;

    /**
     * @var string $port
     *
     * @Field(type="string")
     */
    protected $port;

    /**
     * @var boolean $ssl
     *
     * @Field(type="bool")
     */
    protected $ssl;

    /**
     * @var boolean $passiv
     *
     * @Field(type="bool")
     */
    protected $passiv;


    /**
     * @var string $username
     *
     * @Field(type="string")
     */
    protected $username;

    /**
     * @var string $password
     *
     * @Field(type="string")
     */
    protected $password;

    /**
     * @var string $path
     *
     * @Field(type="string")
     */
    protected $path;

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return bool
     */
    public function isSsl()
    {
        return $this->ssl;
    }

    /**
     * @param bool $ssl
     */
    public function setSsl($ssl)
    {
        $this->ssl = $ssl;
    }

    /**
     * @return string
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @param string $port
     */
    public function setPort($port)
    {
        $this->port = $port;
    }

    /**
     * @return bool
     */
    public function isPassiv()
    {
        return $this->passiv;
    }

    /**
     * @param bool $passiv
     */
    public function setPassiv($passiv)
    {
        $this->passiv = $passiv;
    }


}