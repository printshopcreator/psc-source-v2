<?php
namespace Plugin\System\PSC\Ftp\Event\Package;

use PSC\Shop\QueueBundle\Event\Event;

class Uploaded extends Event
{
    /** @var string */
    protected $order;

    public function getType()
    {
        return 'package_uploaded';
    }

    public function getDescription()
    {
        return 'Paket hochgeladen';
    }

    public function getData()
    {
        return array(
            'order' => $this->order
        );
    }

    public function setData($data)
    {
        $this->order = $data['order'];
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

}