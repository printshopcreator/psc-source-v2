<?php
namespace Plugin\System\PSC\Ftp\Queue;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use Plugin\System\PSC\Ftp\Event\Package\Uploaded;
use Plugin\System\PSC\Upload\Document\Upload;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Package\Created;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Ftp implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    /** @var Manager $_eventManager */
    private $_eventManager;

    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, Manager $eventManager)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_eventManager = $eventManager;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'ftp';
    }

    public function getDescription()
    {
        return 'Verschiebt Package auf einen FTP Server';
    }

    public function getGroup()
    {
        return 'Allgemeines';
    }

    public function getName()
    {
        return 'Ftp';
    }

    public function getForm(FormBuilderInterface $builder, $form_options, EventInterface $event)
    {
        $builder->add("host", TextType::class, array('label' => 'Server'));
        $builder->add("username", TextType::class, array('label' => 'Benutzer'));
        $builder->add("password", TextType::class, array('label' => 'Passwort'));
        $builder->add("port", TextType::class, array('label' => 'Port', 'required' => false));
        $builder->add("ssl", CheckboxType::class, array('label' => 'SSL', 'required' => false));
        $builder->add("passiv", CheckboxType::class, array('label' => 'Passiv', 'required' => false));
        $builder->add("path", TextType::class, array('label' => 'Pfad', 'required' => false));
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        $url = new \Plugin\System\PSC\Ftp\Document\Ftp();
        $url->setHost($form->get('host')->getData());
        $url->setUsername($form->get('username')->getData());
        $url->setPassword($form->get('password')->getData());
        $url->setPort($form->get('port')->getData());
        $url->setSsl($form->get('ssl')->getData());
        $url->setPassiv($form->get('passiv')->getData());
        $url->setPath($form->get('path')->getData());
        $objQueue->setQueueDocument($url);
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
        $form->get('host')->setData($queueObj->getQueueDocument()->getHost());
        $form->get('username')->setData($queueObj->getQueueDocument()->getUsername());
        $form->get('password')->setData($queueObj->getQueueDocument()->getPassword());
        $form->get('path')->setData($queueObj->getQueueDocument()->getPath());
        $form->get('ssl')->setData($queueObj->getQueueDocument()->isSsl());
        $form->get('passiv')->setData($queueObj->getQueueDocument()->isPassiv());
        $form->get('port')->setData($queueObj->getQueueDocument()->getPort());
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCFtp/queue/ftp.html.twig';
    }

    /**
     * @param EventInterface $event
     * @param Queue $doc
     * @return bool
     */
    public function execute(EventInterface $event, Queue $queue)
    {
        /** @var \Plugin\System\PSC\Ftp\Document\Ftp $ftp */
        $ftpDoc = $queue->getQueueDocument();
        /** @var Created $eventDoc */
        $eventDoc = $event;

        if($eventDoc instanceof Created) {

            if(strpos($eventDoc->getOrder(), "mongodb") !== false) {

                $order = explode(";", $eventDoc->getOrder());

                /** @var Upload $upload */
                $upload = $this->_doctrine_mongodb
                    ->getRepository(Upload::class)
                    ->findOneBy(['id' => new \MongoId($order[1])]);

                try {
                    $ftp = new \FtpClient\FtpClient();

                    if ($ftpDoc->getPort() == "") {
                        $ftp->connect($ftpDoc->getHost(), $ftpDoc->isSsl(), 21);
                    } else {
                        $ftp->connect($ftpDoc->getHost(), $ftpDoc->isSsl(), $ftpDoc->getPort());
                    }

                    $ftp->login($ftpDoc->getUsername(), $ftpDoc->getPassword());

                    $ftp->pasv($ftpDoc->isPassiv());

                    $ftp->chdir($ftpDoc->getPath());

                    $ftp->putFromString($upload->getFilename(), $upload->getFile()->getMongoGridFSFile()->getBytes());

                } catch (\Exception $e) {
                    $this->_error = $e->getMessage();
                    return false;
                }
            }else {

                try {
                    $ftp = new \FtpClient\FtpClient();

                    if ($ftpDoc->getPort() == "") {
                        $ftp->connect($ftpDoc->getHost(), $ftpDoc->isSsl(), 21);
                    } else {
                        $ftp->connect($ftpDoc->getHost(), $ftpDoc->isSsl(), $ftpDoc->getPort());
                    }

                    $ftp->login($ftpDoc->getUsername(), $ftpDoc->getPassword());

                    $ftp->pasv($ftpDoc->isPassiv());

                    $ftp->chdir($ftpDoc->getPath());
                    $ftp->putFromPath($eventDoc->getPath());

                    $pc = new Uploaded();
                    $pc->setOrder($eventDoc->getOrder());
                    $pc->setShop($eventDoc->getShop());

                    $this->_eventManager->addJob($pc);

                } catch (\Exception $e) {
                    $this->_error = $e->getMessage();
                    return false;
                }
            }
        }
        return true;
    }

    public function getError()
    {
        return $this->_error;
    }
}