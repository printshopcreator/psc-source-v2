<?php
namespace Plugin\System\PSC\Ftp;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'FTP Queue';

    public function getType()
    {
        return Plugin::Queues;
    }

    public function getDescription()
    {
        return 'Überträgt Pakete per FTP auf einen entfernten Rechner';
    }

    public function getVersion()
    {
        return 1;
    }
}