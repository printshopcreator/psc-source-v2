<?php
namespace Plugin\System\PSC\Ftp\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Plugin\System\PSC\Production\Form\Backend\QuickStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CheckController
 *
 * @Route("/check")
 */
class CheckController extends AbstractController
{
    /**
     * @Route("/", name="psc_plugin_ftp_check")
     * @param Request $request
     * @return JsonResponse
     */
    public function indexAction(Request $request)
    {
        $ftp = new \FtpClient\FtpClient();

        if($request->get('ftpSsl', 'false') == 'false') {
            $ftp->connect($request->get('ftpHost'), false, $request->get('ftpPort', 21));
        }else{
            $ftp->connect($request->get('ftpHost'), true, $request->get('ftpPort', 21));
        }

        $ftp->login($request->get('ftpUsername'), $request->get('ftpPassword'));

        $ftp->pasv($request->get('ftpPassiv', 0));
        if($request->get('ftpPath') == '' ) {
            $count = $ftp->count('.', null, false);
            $data = $ftp->scanDir('.', false);
        }else{
            $count = $ftp->count($request->get('ftpPath', '.'), null,false);
            $data = $ftp->scanDir($request->get('ftpPath', '.', false));
        }

        $temp = [];
        foreach($data as $key => $row) {
            $temp[] = $row;
        }

        return new JsonResponse(['success' => true, 'data' => $temp, 'count' => $count]);
    }
}