<?php
namespace Plugin\System\PSC\Warehouse\Transformer;

use PSC\Shop\EntityBundle\Document\Embed\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\OrderBundle\Model\Order\Position\IProductTypeObject;
use PSC\Shop\OrderBundle\Transformer\Order\Position\IProductTransformer;
use Plugin\System\PSC\Warehouse\Transformer\Model\Product;

class Position implements IProductTransformer {

    public function toDocument()
    {
        // TODO: Implement toDocument() method.
    }

    public function fromDocument()
    {
        // TODO: Implement fromDocument() method.
    }

    public function toEntity()
    {
        // TODO: Implement toEntity() method.
    }

    public function fromEntity(\PSC\Shop\OrderBundle\Model\Order\Position $position, Orderpos $pos)
    {
        $position->setSpecialProductTypeObject(new Product());
    }
}
