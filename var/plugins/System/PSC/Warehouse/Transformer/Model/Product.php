<?php
namespace Plugin\System\PSC\Warehouse\Transformer\Model;

use PSC\Shop\OrderBundle\Model\Order\Position\IProductTypeObject;

class Product implements IProductTypeObject {

    public function getName()
    {
        return 'Warenhausprodukt';
    }

    public function getTyp()
    {
        return 2;
    }

}