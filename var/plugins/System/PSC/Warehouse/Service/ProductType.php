<?php
namespace Plugin\System\PSC\Warehouse\Service;

use Plugin\System\PSC\Warehouse\Producer\JsonForm;
use Plugin\System\PSC\Warehouse\Transformer\Position;
use PSC\Shop\ProductBundle\Interfaces\IJsonFormProducer;

class ProductType implements \PSC\System\PluginBundle\Product\Type {

    private Position $productTransformer;
    private JsonForm $jsonFormProducer;


    public function __construct(Position $positionTransformer, JsonForm $jsonFormProducer)
    {
        $this->productTransformer = $positionTransformer;
        $this->jsonFormProducer = $jsonFormProducer;
    }

    public function getId()
    {
        return 2;
    }

    public function getName()
    {
        return 'Warenhausprodukt';
    }

    public function getPositionProductTransformer(): Position
    {
        return new Position();
    }

    public function getJsonFormProducer(): IJsonFormProducer
    {
        return $this->jsonFormProducer;
    }
}