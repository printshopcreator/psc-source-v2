<?php
namespace Plugin\System\PSC\Warehouse\Producer;

use PSC\Shop\ProductBundle\Interfaces\IJsonFormProducer;
use PSC\Shop\ProductBundle\Model\Price;
use PSC\Shop\ProductBundle\Model\Product;

class JsonForm implements IJsonFormProducer {

    public function getPrice(): Price
    {
        $price = new Price();
        $price->net = 100;
        $price->vat = 19;
        $price->gross = 119;
        $price->allNet = 100;
        $price->allVat = 19;
        $price->allGross = 119;

        return $price;
    }

    public function getJsonForm(): array
    {
        return [
            'count' => [
                'type' => 'string',
                'title' => 'Anzahl',
            ]
        ];
    }

    public function setProduct(Product $product): void
    {
        // TODO: Implement setProduct() method.
    }
}