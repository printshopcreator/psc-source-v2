<?php
namespace Plugin\System\PSC\Warehouse;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Warenhaus Produkt';

    public function getType()
    {
        return Plugin::ProductType;
    }

    public function getDescription()
    {
        return 'Warenhausprodukte';
    }

    public function getVersion()
    {
        return 1;
    }
}