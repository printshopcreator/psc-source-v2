<?php

namespace Plugin\System\PSC\Warehouse\Form\Group;

use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\System\PluginBundle\Form\Group;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Calc extends Group
{
    const GROUP_ID = 'warehouse';

    public function __construct()
    {
        $this->title = 'Preise';
    }

    public function getModule()
    {
        return Field::Product;
    }

    public function getId()
    {
        return self::GROUP_ID;
    }
}