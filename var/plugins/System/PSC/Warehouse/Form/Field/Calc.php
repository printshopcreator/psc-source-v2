<?php

namespace Plugin\System\PSC\Warehouse\Form\Field;

use Plugin\System\PSC\HPLayouter\Form\Group\HPLayouter;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\PluginBundle\Form\Interfaces\Field;
use PSC\System\SettingsBundle\Service\General;
use PSC\System\SettingsBundle\Service\Tax;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Calc implements Field
{

    protected $generalService;
    protected $taxService;

    public function __construct(General $generalService, Tax $taxService)
    {
        $this->generalService = $generalService;
        $this->taxService = $taxService;
    }

    public function getTemplate()
    {
        return '@PluginSystemPSCWarehouse/form/field/calc.html.twig';
    }

    public function getModule()
    {
        return Field::Product;
    }

    /**
     * @param array $data
     */
    public function formPreSubmit(FormEvent $event)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add("mwert", ChoiceType::class, array(
                'label' => 'MwSt.',
                'choices' => $this->taxService->getTaxesForForm(),
            ))
            ->add('fromPrice', TextType::class, ['required' => false, 'label' => 'Ab Preis'])
            ->add('weight', NumberType::class, ['required' => false, 'label' => 'Gewicht'])
            ->add('price', MoneyType::class, ['required' => false, 'label' => 'Preis'])
            ->add('scaledPrice', TextareaType::class, ['required' => false, 'label' => 'Staffel Preis', 'attr' => ['rows' => 5]]);

        return $builder;
    }

    public function getGroup()
    {
        return \Plugin\System\PSC\Warehouse\Form\Group\Calc::GROUP_ID;
    }

    public function formPostSetData(FormEvent $event)
    {
        /** @var Product $data */
        $data = $event->getData();
        $event->getForm()->get('warehouse')->get('mwert')->setData($data->getMwert());
        $event->getForm()->get('warehouse')->get('fromPrice')->setData($data->getFromPrice());
        $event->getForm()->get('warehouse')->get('price')->setData($data->getPrice());
        $event->getForm()->get('warehouse')->get('scaledPrice')->setData($data->getScaledPrice());
        $event->getForm()->get('warehouse')->get('weight')->setData($data->getWeight());
    }

    public function formPostSubmit(FormEvent $event)
    {
        /** @var Product $data */
        $data = $event->getData();
        $data->setMwert($event->getForm()->get('warehouse')->get('mwert')->getData());
        $data->setFromPrice($event->getForm()->get('warehouse')->get('fromPrice')->getData());
        $data->setPrice($event->getForm()->get('warehouse')->get('price')->getData());
        $data->setScaledPrice($event->getForm()->get('warehouse')->get('scaledPrice')->getData());
        $data->setWeight($event->getForm()->get('warehouse')->get('weight')->getData());
        $event->setData($data);
    }

    public function formPreSetData(FormEvent $event)
    {
    }
}

