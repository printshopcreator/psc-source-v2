<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\Preflightcheck\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="preflight")
 * @ORM\Entity(repositoryClass="Plugin\System\PSC\Preflightcheck\Repository\PreflightcheckRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Preflightcheck
{
    /**
     * Id des Shops
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $uid;

    /**
     * title
     *
     * @var string
     *
     * @ORM\Column(name="nr", type="string")
     */
    protected $nr;

    /**
     * description
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     */
    protected $description;

    /**
     * Created
     *
     * @var Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $createdAt;

    /**
     * Updated
     *
     * @var Datetime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updatedAt;




    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_orientation_quer", type="boolean")
     */
    protected $checkFormatQuer;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_orientation_hoch", type="boolean")
     */
    protected $checkFormatHoch;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="check_format_rel", type="string")
     */
    protected $checkFormatSizeRelation;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="check_format_orientation_rel", type="string")
     */
    protected $checkFormatOrientationRelation;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a0", type="boolean")
     */
    protected $checkFormatA0;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a1", type="boolean")
     */
    protected $checkFormatA1;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a2", type="boolean")
     */
    protected $checkFormatA2;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a3", type="boolean")
     */
    protected $checkFormatA3;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a4", type="boolean")
     */
    protected $checkFormatA4;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a5", type="boolean")
     */
    protected $checkFormatA5;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a6", type="boolean")
     */
    protected $checkFormatA6;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a7", type="boolean")
     */
    protected $checkFormatA7;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a8", type="boolean")
     */
    protected $checkFormatA8;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a9", type="boolean")
     */
    protected $checkFormatA9;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_a10", type="boolean")
     */
    protected $checkFormatA10;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="check_format_custom_width_from", type="integer")
     */
    protected $checkFormatCustomWidthFrom;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="check_format_custom_width_to", type="integer")
     */
    protected $checkFormatCustomWidthTo;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="check_format_custom_height_from", type="integer")
     */
    protected $checkFormatCustomHeightFrom;

    /**
     *
     * @var integer
     *
     * @ORM\Column(name="check_format_custom_height_to", type="integer")
     */
    protected $checkFormatCustomHeightTo;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="check_format_error_message", type="string")
     */
    protected $checkFormatErrorMessage;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_box_media", type="boolean")
     */
    protected $checkFormatBoxMedia;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_box_trim", type="boolean")
     */
    protected $checkFormatBoxTrim;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_box_bleed", type="boolean")
     */
    protected $checkFormatBoxBleed;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_box_crop", type="boolean")
     */
    protected $checkFormatBoxCrop;

    /**
     *
     * @var boolean
     *
     * @ORM\Column(name="check_format_box_art", type="boolean")
     */
    protected $checkFormatBoxArt;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="check_format_box_error_message", type="string")
     */
    protected $checkFormatBoxErrorMessage;


    /**
     * checkFileTypeImage
     *
     * @var boolean
     *
     * @ORM\Column(name="check_filetype_image", type="boolean")
     */
    protected $checkFileTypeImage;

    /**
     * checkFileTypePdf
     *
     * @var boolean
     *
     * @ORM\Column(name="check_filetype_pdf", type="boolean")
     */
    protected $checkFileTypePdf;

    /**
     * checkFileTypeErrorMessage
     *
     * @var string
     *
     * @ORM\Column(name="check_filetype_error_message", type="string")
     */
    protected $checkFileTypeErrorMessage;

    /**
     * checkSiteCountRelation
     *
     * @var string
     *
     * @ORM\Column(name="check_site_count_rel", type="string")
     */
    protected $checkSiteCountRelation;

    /**
     * checkSiteCount
     *
     * @var string
     *
     * @ORM\Column(name="check_site_count", type="integer")
     */
    protected $checkSiteCount;

    /**
     * checkSiteCountColor
     *
     * @var string
     *
     * @ORM\Column(name="check_site_count_color", type="integer")
     */
    protected $checkSiteCountColor;

    /**
     * checkSiteCountBW
     *
     * @var string
     *
     * @ORM\Column(name="check_site_count_bw", type="integer")
     */
    protected $checkSiteCountBlackWhite;

    /**
     * checkSiteCountErrorMessage
     *
     * @var string
     *
     * @ORM\Column(name="check_site_count_error_message", type="string")
     */
    protected $checkSiteCountErrorMessage;

    /**
     * checkPdfFontsEmbedded
     *
     * @var boolean
     *
     * @ORM\Column(name="check_fonts_embedded", type="boolean")
     */
    protected $checkPdfFontsEmbedded;

    /**
     * checkPdfFontsEmbeddedErrorMessage
     *
     * @var string
     *
     * @ORM\Column(name="check_fonts_embedded_error_message", type="string")
     */
    protected $checkPdfFontsEmbeddedErrorMessage;

    /**
     * Voucher constructor.
     */
    public function __construct()
    {
        $this->nr = "Neue Regel";
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }



    /**
     * @return Datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param Datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param Datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return string
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @param string $nr
     */
    public function setNr($nr)
    {
        $this->nr = $nr;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return bool
     */
    public function isCheckPdfFontsEmbedded()
    {
        return $this->checkPdfFontsEmbedded;
    }

    /**
     * @param bool $checkPdfFontsEmbedded
     */
    public function setCheckPdfFontsEmbedded($checkPdfFontsEmbedded)
    {
        $this->checkPdfFontsEmbedded = $checkPdfFontsEmbedded;
    }

    /**
     * @return string
     */
    public function getCheckPdfFontsEmbeddedErrorMessage()
    {
        return $this->checkPdfFontsEmbeddedErrorMessage;
    }

    /**
     * @param string $checkPdfFontsEmbeddedErrorMessage
     */
    public function setCheckPdfFontsEmbeddedErrorMessage($checkPdfFontsEmbeddedErrorMessage)
    {
        $this->checkPdfFontsEmbeddedErrorMessage = $checkPdfFontsEmbeddedErrorMessage;
    }

    /**
     * @return string
     */
    public function getCheckSiteCountRelation()
    {
        return $this->checkSiteCountRelation;
    }

    /**
     * @param string $checkSiteCountRelation
     */
    public function setCheckSiteCountRelation($checkSiteCountRelation)
    {
        $this->checkSiteCountRelation = $checkSiteCountRelation;
    }

    /**
     * @return string
     */
    public function getCheckSiteCount()
    {
        return $this->checkSiteCount;
    }

    /**
     * @param string $checkSiteCount
     */
    public function setCheckSiteCount($checkSiteCount)
    {
        $this->checkSiteCount = $checkSiteCount;
    }

    /**
     * @return string
     */
    public function getCheckSiteCountColor()
    {
        return $this->checkSiteCountColor;
    }

    /**
     * @param string $checkSiteCountColor
     */
    public function setCheckSiteCountColor($checkSiteCountColor)
    {
        $this->checkSiteCountColor = $checkSiteCountColor;
    }

    /**
     * @return string
     */
    public function getCheckSiteCountBlackWhite()
    {
        return $this->checkSiteCountBlackWhite;
    }

    /**
     * @param string $checkSiteCountBlackWhite
     */
    public function setCheckSiteCountBlackWhite($checkSiteCountBlackWhite)
    {
        $this->checkSiteCountBlackWhite = $checkSiteCountBlackWhite;
    }

    /**
     * @return string
     */
    public function getCheckSiteCountErrorMessage()
    {
        return $this->checkSiteCountErrorMessage;
    }

    /**
     * @param string $checkSiteCountErrorMessage
     */
    public function setCheckSiteCountErrorMessage($checkSiteCountErrorMessage)
    {
        $this->checkSiteCountErrorMessage = $checkSiteCountErrorMessage;
    }

    /**
     * @return bool
     */
    public function isCheckFileTypeImage()
    {
        return $this->checkFileTypeImage;
    }

    /**
     * @param bool $checkFileTypeImage
     */
    public function setCheckFileTypeImage($checkFileTypeImage)
    {
        $this->checkFileTypeImage = $checkFileTypeImage;
    }

    /**
     * @return bool
     */
    public function isCheckFileTypePdf()
    {
        return $this->checkFileTypePdf;
    }

    /**
     * @param bool $checkFileTypePdf
     */
    public function setCheckFileTypePdf($checkFileTypePdf)
    {
        $this->checkFileTypePdf = $checkFileTypePdf;
    }

    /**
     * @return string
     */
    public function getCheckFileTypeErrorMessage()
    {
        return $this->checkFileTypeErrorMessage;
    }

    /**
     * @param string $checkFileTypeErrorMessage
     */
    public function setCheckFileTypeErrorMessage($checkFileTypeErrorMessage)
    {
        $this->checkFileTypeErrorMessage = $checkFileTypeErrorMessage;
    }

    /**
     * @return bool
     */
    public function isCheckFormatQuer()
    {
        return $this->checkFormatQuer;
    }

    /**
     * @param bool $checkFormatQuer
     */
    public function setCheckFormatQuer($checkFormatQuer)
    {
        $this->checkFormatQuer = $checkFormatQuer;
    }

    /**
     * @return bool
     */
    public function isCheckFormatHoch()
    {
        return $this->checkFormatHoch;
    }

    /**
     * @param bool $checkFormatHoch
     */
    public function setCheckFormatHoch($checkFormatHoch)
    {
        $this->checkFormatHoch = $checkFormatHoch;
    }

    /**
     * @return string
     */
    public function getCheckFormatSizeRelation()
    {
        return $this->checkFormatSizeRelation;
    }

    /**
     * @param string $checkFormatSizeRelation
     */
    public function setCheckFormatSizeRelation($checkFormatSizeRelation)
    {
        $this->checkFormatSizeRelation = $checkFormatSizeRelation;
    }

    /**
     * @return string
     */
    public function getCheckFormatOrientationRelation()
    {
        return $this->checkFormatOrientationRelation;
    }

    /**
     * @param string $checkFormatOrientationRelation
     */
    public function setCheckFormatOrientationRelation($checkFormatOrientationRelation)
    {
        $this->checkFormatOrientationRelation = $checkFormatOrientationRelation;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA0()
    {
        return $this->checkFormatA0;
    }

    /**
     * @param bool $checkFormatA0
     */
    public function setCheckFormatA0($checkFormatA0)
    {
        $this->checkFormatA0 = $checkFormatA0;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA1()
    {
        return $this->checkFormatA1;
    }

    /**
     * @param bool $checkFormatA1
     */
    public function setCheckFormatA1($checkFormatA1)
    {
        $this->checkFormatA1 = $checkFormatA1;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA2()
    {
        return $this->checkFormatA2;
    }

    /**
     * @param bool $checkFormatA2
     */
    public function setCheckFormatA2($checkFormatA2)
    {
        $this->checkFormatA2 = $checkFormatA2;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA3()
    {
        return $this->checkFormatA3;
    }

    /**
     * @param bool $checkFormatA3
     */
    public function setCheckFormatA3($checkFormatA3)
    {
        $this->checkFormatA3 = $checkFormatA3;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA4()
    {
        return $this->checkFormatA4;
    }

    /**
     * @param bool $checkFormatA4
     */
    public function setCheckFormatA4($checkFormatA4)
    {
        $this->checkFormatA4 = $checkFormatA4;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA5()
    {
        return $this->checkFormatA5;
    }

    /**
     * @param bool $checkFormatA5
     */
    public function setCheckFormatA5($checkFormatA5)
    {
        $this->checkFormatA5 = $checkFormatA5;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA6()
    {
        return $this->checkFormatA6;
    }

    /**
     * @param bool $checkFormatA6
     */
    public function setCheckFormatA6($checkFormatA6)
    {
        $this->checkFormatA6 = $checkFormatA6;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA7()
    {
        return $this->checkFormatA7;
    }

    /**
     * @param bool $checkFormatA7
     */
    public function setCheckFormatA7($checkFormatA7)
    {
        $this->checkFormatA7 = $checkFormatA7;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA8()
    {
        return $this->checkFormatA8;
    }

    /**
     * @param bool $checkFormatA8
     */
    public function setCheckFormatA8($checkFormatA8)
    {
        $this->checkFormatA8 = $checkFormatA8;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA9()
    {
        return $this->checkFormatA9;
    }

    /**
     * @param bool $checkFormatA9
     */
    public function setCheckFormatA9($checkFormatA9)
    {
        $this->checkFormatA9 = $checkFormatA9;
    }

    /**
     * @return bool
     */
    public function isCheckFormatA10()
    {
        return $this->checkFormatA10;
    }

    /**
     * @param bool $checkFormatA10
     */
    public function setCheckFormatA10($checkFormatA10)
    {
        $this->checkFormatA10 = $checkFormatA10;
    }

    /**
     * @return int
     */
    public function getCheckFormatCustomWidthFrom()
    {
        return $this->checkFormatCustomWidthFrom;
    }

    /**
     * @param int $checkFormatCustomWidthFrom
     */
    public function setCheckFormatCustomWidthFrom($checkFormatCustomWidthFrom)
    {
        $this->checkFormatCustomWidthFrom = $checkFormatCustomWidthFrom;
    }

    /**
     * @return int
     */
    public function getCheckFormatCustomWidthTo()
    {
        return $this->checkFormatCustomWidthTo;
    }

    /**
     * @param int $checkFormatCustomWidthTo
     */
    public function setCheckFormatCustomWidthTo($checkFormatCustomWidthTo)
    {
        $this->checkFormatCustomWidthTo = $checkFormatCustomWidthTo;
    }

    /**
     * @return int
     */
    public function getCheckFormatCustomHeightFrom()
    {
        return $this->checkFormatCustomHeightFrom;
    }

    /**
     * @param int $checkFormatCustomHeightFrom
     */
    public function setCheckFormatCustomHeightFrom($checkFormatCustomHeightFrom)
    {
        $this->checkFormatCustomHeightFrom = $checkFormatCustomHeightFrom;
    }

    /**
     * @return int
     */
    public function getCheckFormatCustomHeightTo()
    {
        return $this->checkFormatCustomHeightTo;
    }

    /**
     * @param int $checkFormatCustomHeightTo
     */
    public function setCheckFormatCustomHeightTo($checkFormatCustomHeightTo)
    {
        $this->checkFormatCustomHeightTo = $checkFormatCustomHeightTo;
    }

    /**
     * @return string
     */
    public function getCheckFormatErrorMessage()
    {
        return $this->checkFormatErrorMessage;
    }

    /**
     * @param string $checkFormatErrorMessage
     */
    public function setCheckFormatErrorMessage($checkFormatErrorMessage)
    {
        $this->checkFormatErrorMessage = $checkFormatErrorMessage;
    }

    /**
     * @return bool
     */
    public function isCheckFormatBoxMedia()
    {
        return $this->checkFormatBoxMedia;
    }

    /**
     * @param bool $checkFormatBoxMedia
     */
    public function setCheckFormatBoxMedia($checkFormatBoxMedia)
    {
        $this->checkFormatBoxMedia = $checkFormatBoxMedia;
    }

    /**
     * @return bool
     */
    public function isCheckFormatBoxTrim()
    {
        return $this->checkFormatBoxTrim;
    }

    /**
     * @param bool $checkFormatBoxTrim
     */
    public function setCheckFormatBoxTrim($checkFormatBoxTrim)
    {
        $this->checkFormatBoxTrim = $checkFormatBoxTrim;
    }

    /**
     * @return bool
     */
    public function isCheckFormatBoxBleed()
    {
        return $this->checkFormatBoxBleed;
    }

    /**
     * @param bool $checkFormatBoxBleed
     */
    public function setCheckFormatBoxBleed($checkFormatBoxBleed)
    {
        $this->checkFormatBoxBleed = $checkFormatBoxBleed;
    }

    /**
     * @return bool
     */
    public function isCheckFormatBoxCrop()
    {
        return $this->checkFormatBoxCrop;
    }

    /**
     * @param bool $checkFormatBoxCrop
     */
    public function setCheckFormatBoxCrop($checkFormatBoxCrop)
    {
        $this->checkFormatBoxCrop = $checkFormatBoxCrop;
    }

    /**
     * @return bool
     */
    public function isCheckFormatBoxArt()
    {
        return $this->checkFormatBoxArt;
    }

    /**
     * @param bool $checkFormatBoxArt
     */
    public function setCheckFormatBoxArt($checkFormatBoxArt)
    {
        $this->checkFormatBoxArt = $checkFormatBoxArt;
    }

    /**
     * @return string
     */
    public function getCheckFormatBoxErrorMessage()
    {
        return $this->checkFormatBoxErrorMessage;
    }

    /**
     * @param string $checkFormatBoxErrorMessage
     */
    public function setCheckFormatBoxErrorMessage($checkFormatBoxErrorMessage)
    {
        $this->checkFormatBoxErrorMessage = $checkFormatBoxErrorMessage;
    }


}
