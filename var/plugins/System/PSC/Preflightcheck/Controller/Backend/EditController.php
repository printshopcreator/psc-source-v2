<?php
namespace Plugin\System\PSC\Preflightcheck\Controller\Backend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Plugin\System\PSC\Preflightcheck\Entity\Preflightcheck;
use Plugin\System\PSC\Preflightcheck\Form\Backend\PreflightCheckType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;

use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\NewsBundle\Form\Backend\DeleteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class ListController
 *
 * @package PSC\Plugin\Preflightcheck
 * @Route("/edit")
 */
class EditController extends AbstractController
{
    /**
     * Create
     *
     * @Route("/create", name="psc_plugin_system_preflightcheck_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction(Request $request, SessionInterface $session, EntityManagerInterface $entityManager)
    {

        $preflightCheck = new Preflightcheck();

        $form = $this->createForm(PreflightCheckType::class, $preflightCheck);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($preflightCheck);
            $entityManager->flush();
            $session->getFlashBag()->add(
                'success',
                'Preflightcheck \'' . $preflightCheck->getNr() . '\' has been created!'
            );
            return $this->redirectToRoute('psc_plugin_system_preflightcheck_backend_list');
        }

        return array(
            'preflightCheck' => $preflightCheck,
            'form' => $form->createView()
        );
    }

    /**
     * edit
     *
     * @Route("/edit/{uid}", name="psc_plugin_system_preflightcheck_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, SessionInterface $session, EntityManagerInterface $entityManager, $uid)
    {

        /** @var Preflightcheck $preflightCheck */
        $preflightCheck = $entityManager
            ->getRepository('Plugin\System\PSC\Preflightcheck\Entity\Preflightcheck')->findOneBy(array('uid' => $uid));

        $form = $this->createForm(PreflightCheckType::class, $preflightCheck);

        $preflightCheck->setUpdatedAt(new \DateTime());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($preflightCheck);
            $entityManager->flush();
            $session->getFlashBag()->add(
                'success',
                'Preflightcheck \'' . $preflightCheck->getNr() . '\' has been updated!'
            );
            return $this->redirectToRoute('psc_plugin_system_preflightcheck_backend_list');
        }

        return array(
            'preflightCheck' => $preflightCheck,
            'form' => $form->createView()
        );
    }


    /**
     * remove
     *
     * @Route("/delete/{uid}", name="psc_plugin_system_preflightcheck_backend_delete")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, SessionInterface $session, EntityManagerInterface $entityManager, $uid)
    {

        $preflightCheck = $entityManager->getRepository('Plugin\System\PSC\Preflightcheck\Entity\Preflightcheck')->findOneByUid($uid);

        $form = $this->createForm(DeleteType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $orgTitle = $preflightCheck->getNr();

                $entityManager->remove($preflightCheck);
                $entityManager->flush();
                $session->getFlashBag()->add(
                    'success',
                    'Preflightcheck \'' . $orgTitle . '\' has been deleted!'
                );
                return $this->redirectToRoute('psc_plugin_system_preflightcheck_backend_list');
            }
            return $this->redirectToRoute('psc_plugin_system_preflightcheck_backend_list');
        }


        return array(
            'preflightCheck' => $preflightCheck,
            'form' => $form->createView()
        );
    }
}
