<?php
namespace Plugin\System\PSC\Preflightcheck\Controller\Backend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Plugin\System\PSC\XmlCalc\Form\Calc;
use Plugin\System\PSC\XmlCalc\Form\Edit\XmlType;
use PSC\Library\Calc\Option\Type\Select\Opt;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\OrderBundle\Form\Backend\OrderStatusType;
use Plugin\System\PSC\Production\Form\Backend\StatusType;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Tracking;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Upload;
use Plugin\System\PSC\Production\Form\Backend\UploadType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactDeliveryType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactInvoiceType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactSenderType;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use Ramsey\Uuid\Uuid;
use Sauladam\ShipmentTracker\ShipmentTracker;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
/**
 * Class ListController
 *
 * @package PSC\Plugin\Preflightcheck
 * @Route("/list")
 */
class ListController extends AbstractController
{
    /**
     * @Template()
     * @Route("/list", name="psc_plugin_system_preflightcheck_backend_list")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function indexAction(Request $request, PaginatorInterface $paginator, EntityManagerInterface $entityManager)
    {
        $userRepository = $entityManager->getRepository('Plugin\System\PSC\Preflightcheck\Entity\Preflightcheck');
        $qb = $userRepository->createQueryBuilder('check');

        $pagination = $paginator->paginate(
            $query = $qb->getQuery(),
            $request->query->getInt('page', 1),
            15
        );

        return array(
            'pagination' => $pagination
        );
    }



}
