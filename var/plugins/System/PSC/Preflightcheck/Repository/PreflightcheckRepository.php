<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\Preflightcheck\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Voucher;

/**
 * VoucherItemRepository
 *
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class PreflightcheckRepository extends EntityRepository
{

}
