<?php
namespace Plugin\System\PSC\Preflightcheck;

use PSC\System\PluginBundle\Plugin\Base;

class Plugin extends Base implements \PSC\System\PluginBundle\Interfaces\Plugin {

    protected $name = 'Upload & Preflight';

    public function getType()
    {
        return Plugin::Backend;
    }

    public function getDescription()
    {
        return 'Upload & Preflight';
    }

    public function getVersion()
    {
        return 1;
    }
}