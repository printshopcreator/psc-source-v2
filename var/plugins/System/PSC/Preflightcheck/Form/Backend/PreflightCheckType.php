<?php
/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */
namespace Plugin\System\PSC\Preflightcheck\Form\Backend;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PreflightCheckType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nr', TextType::class, ['label' => 'Nr', 'required' => false])
            ->add('description', TextType::class, ['label' => 'Beschreibung', 'required' => false])

            ->add('checkFileTypePdf', CheckboxType::class, ['label' => 'Pdf', 'required' => false])
            ->add('checkFileTypeImage', CheckboxType::class, ['label' => 'Image', 'required' => false])
            ->add('checkFileTypeErrorMessage', TextType::class, ['label' => 'Fehlermeldung', 'required' => false])


            ->add('checkFormatQuer', CheckboxType::class, ['label' => 'Querformat', 'required' => false])
            ->add('checkFormatHoch', CheckboxType::class, ['label' => 'Hochformat', 'required' => false])
            ->add('checkFormatOrientationRelation', TextType::class, ['label' => 'Verknüpfung Kalkulations Id', 'required' => false])

            ->add('checkFormatA0', CheckboxType::class, ['label' => 'A0', 'required' => false])
            ->add('checkFormatA1', CheckboxType::class, ['label' => 'A1', 'required' => false])
            ->add('checkFormatA2', CheckboxType::class, ['label' => 'A2', 'required' => false])
            ->add('checkFormatA3', CheckboxType::class, ['label' => 'A3', 'required' => false])
            ->add('checkFormatA4', CheckboxType::class, ['label' => 'A4', 'required' => false])
            ->add('checkFormatA5', CheckboxType::class, ['label' => 'A5', 'required' => false])
            ->add('checkFormatA6', CheckboxType::class, ['label' => 'A6', 'required' => false])
            ->add('checkFormatA7', CheckboxType::class, ['label' => 'A7', 'required' => false])
            ->add('checkFormatA8', CheckboxType::class, ['label' => 'A8', 'required' => false])
            ->add('checkFormatA9', CheckboxType::class, ['label' => 'A9', 'required' => false])
            ->add('checkFormatA10', CheckboxType::class, ['label' => 'A10', 'required' => false])

            ->add('checkFormatCustomWidthFrom', NumberType::class, ['label' => 'Breite von', 'required' => false])
            ->add('checkFormatCustomWidthTo', NumberType::class, ['label' => 'Breite bis', 'required' => false])
            ->add('checkFormatCustomHeightFrom', NumberType::class, ['label' => 'Höhe von', 'required' => false])
            ->add('checkFormatCustomHeightTo', NumberType::class, ['label' => 'Höhe bis', 'required' => false])

            ->add('checkFormatSizeRelation', TextType::class, ['label' => 'Verknüpfung Kalkulations Id', 'required' => false])
            ->add('checkFormatErrorMessage', TextType::class, ['label' => 'Fehlermeldung', 'required' => false])


            ->add('checkFormatBoxMedia', CheckboxType::class, ['label' => 'Media', 'required' => false])
            ->add('checkFormatBoxTrim', CheckboxType::class, ['label' => 'Trim', 'required' => false])
            ->add('checkFormatBoxBleed', CheckboxType::class, ['label' => 'Bleed', 'required' => false])
            ->add('checkFormatBoxCrop', CheckboxType::class, ['label' => 'Crop', 'required' => false])
            ->add('checkFormatBoxArt', CheckboxType::class, ['label' => 'Art', 'required' => false])
            ->add('checkFormatBoxErrorMessage', TextType::class, ['label' => 'Fehlermeldung', 'required' => false])



            ->add('checkSiteCount', NumberType::class, ['label' => 'Gesamtanzahl', 'required' => false])
            ->add('checkSiteCountColor', NumberType::class, ['label' => 'Gesamtanzahl Farbig', 'required' => false])
            ->add('checkSiteCountBlackWhite', NumberType::class, ['label' => 'Gesamtanzahl S/W', 'required' => false])
            ->add('checkSiteCountErrorMessage', TextType::class, ['label' => 'Fehlermeldung', 'required' => false])
            ->add('checkSiteCountRelation', TextType::class, ['label' => 'Verknüpfung Kalkulations Id', 'required' => false])


            ->add('checkPdfFontsEmbedded', CheckboxType::class, ['label' => 'Fonts embedded checken', 'required' => false])
            ->add('checkPdfFontsEmbeddedErrorMessage', TextType::class, ['label' => 'Fehlermeldung', 'required' => false])

            ;

        $builder->add('save', SubmitType::class, array('label' => 'Speichern'));

    }

    public function getName()
    {
        return 'preflight';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Plugin\System\PSC\Preflightcheck\Entity\Preflightcheck',
        ));
    }
}