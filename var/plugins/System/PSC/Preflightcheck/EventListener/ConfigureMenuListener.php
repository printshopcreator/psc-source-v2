<?php
namespace Plugin\System\PSC\Preflightcheck\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationCheckerInterface  */
    public $authorizationChecker = null;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();

            $submenu = $menu->addChild('Preflightcheck Regeln', array(
                'route' => 'psc_plugin_system_preflightcheck_backend_list',
                'extras' => array(
                    'icon' => 'fas fa-check-double',
                    'orderNumber' => 26,
                    'routes' => array('psc_plugin_system_preflightcheck_backend_create', 'psc_plugin_system_preflightcheck_backend_edit')
                )
            ));
        }
    }
}