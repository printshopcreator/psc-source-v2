<?php

namespace PSC\Backend\ToolsBundle\Controller\Backend;

use PSC\Backend\ToolsBundle\Interfaces\ConfigurableElementInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class ExporterController
 *
 * @package PSC\Backend\Tools
 * @Route("/exporters")
 */
class ExporterController extends AbstractController
{
    /**
     * @Template()
     * @Route("/list", name="psc_backend_tools_exporter_list_index")
     * @Security("is_granted('ROLE_SHOP')")
     * @param ExporterRegistry $registry
     * @return array
     */
    public function listAction(ExporterRegistry $registry)
    {
        return array('exporters' => $registry->all());
    }

    /**
     * @Route("/{type}/export", name="psc_backend_tools_exporter_form")
     * @Security("is_granted('ROLE_SHOP')")
     * @Template()
     * @param Request $request
     * @param ExporterRegistry $registry
     * @param         $type
     *
     * @return array
     */
    public function formAction(Request $request, ExporterRegistry $registry, $type)
    {
        if (!$registry->has($type)) {
            throw new NotFoundHttpException('Exporter not found');
        }

        $exporter = $registry->get($type);
        $builder  = $this->createFormBuilder(null, array('action' => $this->generateUrl('psc_backend_tools_exporter_form', array('type' => $exporter->getType()))));
        if ($exporter instanceof ConfigurableElementInterface) {
            $exporter->getForm($builder, array());
        }

        //$builder->add('submit', SubmitType::class, array('label' => 'Export!!!'));

        $form = $builder->getForm();
        if ($exporter instanceof ConfigurableElementInterface) {
            $exporter->setForm($form);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            return $exporter->export();
        }


        return array('exporter' => $exporter, 'form' => $form->createView());
    }
}
