<?php

namespace PSC\Backend\ToolsBundle\Event;

final class MenuEvents
{
    /**
     * @var string
     */
    const CONFIGURE_MAIN = 'psc.backend.tools.main';

    /**
     * @var string
     */
    const CONFIGURE_TOP = 'psc.backend.tools.top';
}
