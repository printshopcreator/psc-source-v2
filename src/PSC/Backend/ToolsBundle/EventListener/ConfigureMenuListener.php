<?php

namespace PSC\Backend\ToolsBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use PSC\Backend\ToolsBundle\Event\MenuEvents;
use Psr\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    protected $eventDispatcher;
/** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
/**
     * @param FactoryInterface @factory
     */
    public function __construct(EventDispatcherInterface $eventDispatcher, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $factory = $event->getFactory();
            $submenu = $menu->addChild('tools', array(
                'uri' => '#',
                'label' => 'Tools',
                'extras' => array(
                    'icon' => 'fas fa-wrench',
                    'orderNumber' => 900
                )
            ));

            $submenu->addChild(
                'general',
                array(
                    'route' => 'psc_backend_tools_exporter_list_index',
                    'label' => 'Export Daten',
                    'extras' => array(
                        'icon' => 'fas fa-download'
                    )
                )
            );
            $this->eventDispatcher->dispatch(new ConfigureMenuEvent($factory, $submenu), MenuEvents::CONFIGURE_MAIN);
        }
    }
}
