<?php

namespace PSC\Backend\ToolsBundle\Interfaces;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

interface ConfigurableElementInterface
{
    /**
     * @param Form $form
     * @return mixed
     */
    public function setForm(Form $form);
/**
     * @param FormBuilderInterface $builder
     * @param $form_options
     * @return FormBuilderInterface
     */
    public function getForm(FormBuilderInterface $builder, $form_options);
}
