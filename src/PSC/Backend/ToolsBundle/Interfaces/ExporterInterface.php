<?php

namespace PSC\Backend\ToolsBundle\Interfaces;

use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use Symfony\Component\HttpFoundation\Response;

interface ExporterInterface extends RegistryElementInterface
{
    /**
     * @return Response
     */
    public function export();
/**
     * @return Response
     */
    public function getGroup();
}
