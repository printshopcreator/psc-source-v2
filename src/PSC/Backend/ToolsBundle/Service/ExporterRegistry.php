<?php

namespace PSC\Backend\ToolsBundle\Service;

use LogicException;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryInterface;

class ExporterRegistry implements RegistryInterface
{
    public static $ARTICLE = 1;
    public static $ORDER = 2;
    public static $CONTACT = 3;
    public static $ACCOUNT = 4;
    public static $PAPER = 5;
/** @var  ExporterInterface[] */
    private $exporters = array();
/** @return ExporterInterface[] */
    public function all()
    {
        $tmp = array();
        foreach ($this->exporters as $exporter) {
            $tmp[$exporter->getGroup()][] = $exporter;
        }

        return $tmp;
    }

    /**
     * @param $key string
     *
     * @return ExporterInterface
     */
    public function get($key)
    {
        if (!$this->has($key)) {
            throw new LogicException('Exporter ' . $key . ' not registered');
        }

        return $this->exporters[$key];
    }

    public function has($type)
    {
        return array_key_exists($type, $this->exporters);
    }

    /**
     * @param RegistryElementInterface $element
     */
    public function add(RegistryElementInterface $element)
    {
        if ($this->has($element->getType())) {
            throw new LogicException('Exporter ' . $element->getType() . ' already registered');
        }

        $this->exporters[$element->getType()] = $element;
    }

    /**
     * @param RegistryElementInterface $element
     */
    public function remove(RegistryElementInterface $element)
    {
        if (!$this->has($element->getType())) {
            throw new LogicException('Exporter ' . $element->getType() . ' not registered');
        }

        unset($this->exporters[$element->getType()]);
    }
}
