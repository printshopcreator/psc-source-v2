<?php

namespace PSC\Backend\ToolsBundle\Exporter\Csv;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Port\Csv\CsvWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Export\Writer\ExcelWriter;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Backend\ToolsBundle\Interfaces\ConfigurableElementInterface;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class OrderExporter implements ExporterInterface, ConfigurableElementInterface
{
    private $_formFactory = null;
    private $_entityManager = null;
    private $_shopService = null;
/** @var Form */
    private $_form = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'csv_order_all';
    }

    public function getDescription()
    {
        return 'Auftragsexport (CSV)';
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
        $builder->add("from", DateType::class, array('label' => 'Von', 'attr' => array('class' => 'form-element')));
        $builder->add("to", DateType::class, array('label' => 'Bis', 'attr' => array('class' => 'form-element')));
    }

    public function getGroup()
    {
        return ExporterRegistry::$ORDER;
    }

    public function setForm(Form $form)
    {
        $this->_form = $form;
    }

    /**
     * @return array[]
     */
    public function export()
    {
        $formData = $this->_form->getData();
        $orderRepository = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');
        $qb = $orderRepository->createQueryBuilder('o')
            ->leftJoin('o.contact', 'c')
            ->where("o.created >= :from and o.created <= :to and o.shop = :shop")
            ->setParameter('from', $formData['from'])
            ->setParameter('to', $formData['to'])
            ->setParameter('shop', $this->_shopService->getSelectedShop()->getId())
            ->getQuery();
        $temp = array();
        $rows = $qb->getResult();
        foreach ($rows as $row) {
            $temp[] = array(
                $row->getUID(),
                $row->getAlias(),
                $row->getCreated()->format('Y-m-d H:i:s'),
                $row->getStatus(),
                $row->getBrutto(),
                $row->getPackage(),
                $row->getContact()->getUsername()
            );
        }

        $reader = new ArrayReader($temp);
        $writer = new CsvWriter();
        $writer->setStream(fopen('php://output', 'w'));
        $response = new StreamedResponse(function () use ($reader, $writer) {

            foreach ($reader as $row) {
                $writer->writeItem($row);
            }
        });
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, 'export.csv');
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }
}
