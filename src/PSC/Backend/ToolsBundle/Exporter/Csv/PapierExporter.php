<?php

namespace PSC\Backend\ToolsBundle\Exporter\Csv;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Port\Csv\CsvWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class PapierExporter implements ExporterInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_shopService = null;
    private $_doctrine_mongodb = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Shop $shopService, DocumentManager $doctrine_mongodb)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
        $this->_doctrine_mongodb = $doctrine_mongodb;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'csv_paper';
    }

    public function getDescription()
    {
        return 'einfacher Papierexport (CSV)';
    }

    public function getGroup()
    {
        return ExporterRegistry::$PAPER;
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
    }

    public function setForm(Form $form)
    {
    }

    /**
     * @return array[]
     */
    public function export()
    {
        /** @var Shop $shop */
        $shop = $this->_doctrine_mongodb
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$this->_shopService->getSelectedShop()->getUid()));
        $rows = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Paper')->findBy(array());
        $temp = array();
        $temp[] = array(
            'id' => 'id',
            'install_id' => 'install_id',
            'art_nr' => 'art_nr',
            'auslauf' => 'auslauf',
            'description_1' => 'description_1',
            'description_2' => 'description_2',
            'grammatur' => 'grammatur',
            'staerke' => 'staerke',
            'breite' => 'breite',
            'hoehe' => 'hoehe',
            'laenge' => 'laenge',
            'rollenlaenge' => 'rollenlaenge',
            'gewicht' => 'gewicht',
            'papierausruestung' => 'papierausruestung',
            'farbnummer' => 'farbnummer',
            'farbbezeichnung' => 'farbbezeichnung',
            'huelsendurchmesser' => 'huelsendurchmesser',
            'kleber' => 'kleber',
            'abdeckpapier' => 'abdeckpapier',
            'laufrichtung' => 'laufrichtung',
            'mengenangabe' => 'mengenangabe',
            'mengenangabe_palette' => 'mengenangabe_palette',
            'inhalt' => 'inhalt',
            'etiketten_je_blatt' => 'etiketten_je_blatt',
            'mengeneinheit' => 'mengeneinheit',
            'staffelmenge_1' => 'staffelmenge_1',
            'staffelpreis_1' => 'staffelpreis_1',
            'staffelmenge_2' => 'staffelmenge_2',
            'staffelpreis_2' => 'staffelpreis_2',
            'staffelmenge_3' => 'staffelmenge_3',
            'staffelpreis_3' => 'staffelpreis_3',
            'staffelmenge_4' => 'staffelmenge_4',
            'staffelpreis_4' => 'staffelpreis_4',
            'staffelmenge_5' => 'staffelmenge_5',
            'staffelpreis_5' => 'staffelpreis_5',
            'lagerort' => 'lagerort',
            'verkaufshinweise' => 'verkaufshinweise',
            'abnahmeinfo' => 'abnahmeinfo',
            'produkt_beschreibung' => 'produkt_beschreibung',
            'produkt_eigenschaften' => 'produkt_eigenschaften',
            'produkt_vorteile' => 'produkt_vorteile',
            'produkt_nutzen' => 'produkt_nutzen',
            'produkt_anwendungen' => 'produkt_anwendungen',
            'produkt_besonderheit' => 'produkt_besonderheit',
            'musterbuch' => 'musterbuch',
            'zur_aufnahme_von' => 'eigenschaften',
            'preis' => 'preis',
            'uuid' => 'uuid',
            'offset_fix' => 'offset_fix',
            'offset_var' => 'offset_var',
            'digital_fix' => 'digital_fix',
            'digital_var' => 'digital_var',
            'papierTyp1' => 'papierTyp1',
            'papierTyp2' => 'papierTyp2',
            'papierTyp3' => 'papierTyp3',
            'papierTyp4' => 'papierTyp4',
            'papierTyp5' => 'papierTyp5',
            'papierTyp6' => 'papierTyp6',
            'papierTyp7' => 'papierTyp7',
            'papierTyp8' => 'papierTyp8',
            'papierTyp9' => 'papierTyp9',
            'papierTyp10' => 'papierTyp10',
            'papierTyp11' => 'papierTyp11',
            'papierTyp12' => 'papierTyp12',
            'papierTyp13' => 'papierTyp13',
            'papierTyp14' => 'papierTyp14',
            'umschlagen' => 'umschlagen',
            'happy' => 'happy',
            'eq' => 'eq',
            'sense' => 'sense',
            'sky' => 'sky',
            'glam' => 'glam',
            'post' => 'post',
            'volume' => 'volume',
        );
//var_dump($rows);
        /** @var Paper $row */
        foreach ($rows as $row) {
/** @var \PSC\Shop\EntityBundle\Document\Paper $paperDoc */
            $paperDoc = $this->_doctrine_mongodb
                ->getRepository('PSC\Shop\EntityBundle\Document\Paper')
                ->findOneBy(array('uid' => (string)$row->getId()));
            if ($paperDoc) {
                if ($paperDoc->isPapierTyp1()) {
                    $papiertyp1 = "true";
                } else {
                    $papiertyp1 = "false";
                }
                if ($paperDoc->isPapierTyp2()) {
                    $papiertyp2 = "true";
                } else {
                    $papiertyp2 = "false";
                }
                if ($paperDoc->isPapierTyp3()) {
                    $papiertyp3 = "true";
                } else {
                    $papiertyp3 = "false";
                }
                if ($paperDoc->isPapierTyp4()) {
                    $papiertyp4 = "true";
                } else {
                    $papiertyp4 = "false";
                }
                if ($paperDoc->isPapierTyp5()) {
                    $papiertyp5 = "true";
                } else {
                    $papiertyp5 = "false";
                }
                if ($paperDoc->isPapierTyp6()) {
                    $papiertyp6 = "true";
                } else {
                    $papiertyp6 = "false";
                }
                if ($paperDoc->isPapierTyp7()) {
                    $papiertyp7 = "true";
                } else {
                    $papiertyp7 = "false";
                }
                if ($paperDoc->isPapierTyp8()) {
                    $papiertyp8 = "true";
                } else {
                    $papiertyp8 = "false";
                }
                if ($paperDoc->isPapierTyp9()) {
                    $papiertyp9 = "true";
                } else {
                    $papiertyp9 = "false";
                }
                if ($paperDoc->isPapierTyp10()) {
                    $papiertyp10 = "true";
                } else {
                    $papiertyp10 = "false";
                }
                if ($paperDoc->isPapierTyp11()) {
                    $papiertyp11 = "true";
                } else {
                    $papiertyp11 = "false";
                }
                if ($paperDoc->isPapierTyp12()) {
                    $papiertyp12 = "true";
                } else {
                    $papiertyp12 = "false";
                }
                if ($paperDoc->isPapierTyp13()) {
                    $papiertyp13 = "true";
                } else {
                    $papiertyp13 = "false";
                }
                if ($paperDoc->isPapierTyp14()) {
                    $papiertyp14 = "true";
                } else {
                    $papiertyp14 = "false";
                }
                $row->setPapierTyp1($papiertyp1);
                $row->setPapierTyp2($papiertyp2);
                $row->setPapierTyp3($papiertyp3);
                $row->setPapierTyp4($papiertyp4);
                $row->setPapierTyp5($papiertyp5);
                $row->setPapierTyp6($papiertyp6);
                $row->setPapierTyp7($papiertyp7);
                $row->setPapierTyp8($papiertyp8);
                $row->setPapierTyp9($papiertyp9);
                $row->setPapierTyp10($papiertyp10);
                $row->setPapierTyp11($papiertyp11);
                $row->setPapierTyp12($papiertyp12);
                $row->setPapierTyp13($papiertyp13);
                $row->setPapierTyp14($papiertyp14);
                if ($paperDoc->isUmschlagen()) {
                    $isUmschlagen = "true";
                } else {
                    $isUmschlagen = "false";
                }
                $row->setUmschlagen($isUmschlagen);
                if ($paperDoc->isHappy()) {
                    $isHappy = "true";
                } else {
                    $isHappy = "false";
                }
                $row->setHappy($isHappy);
                if ($paperDoc->isEq()) {
                    $isEq = "true";
                } else {
                    $isEq = "false";
                }
                $row->setEq($isEq);
                if ($paperDoc->isSense()) {
                    $isSense = "true";
                } else {
                    $isSense = "false";
                }
                $row->setSense($isSense);
                if ($paperDoc->isSky()) {
                    $isSky = "true";
                } else {
                    $isSky = "false";
                }
                $row->setSky($isSky);
                if ($paperDoc->isGlam()) {
                    $isGlam = "true";
                } else {
                    $isGlam = "false";
                }
                $row->setGlam($isGlam);
                if ($paperDoc->isPost()) {
                    $isPost = "true";
                } else {
                    $isPost = "false";
                }
                $row->setPost($isPost);
                $row->setVolume($paperDoc->getVolume());
            }
                $temp[] = array(
                    'id' => $row->getId(),
                    'install_id' => $row->getInstall(),
                    'art_nr' => $row->getArtNr(),
                    'auslauf' => $row->getAuslauf(),
                    'description_1' => $row->getDescription1(),
                    'description_2' => $row->getDescription2(),
                    'grammatur' => $row->getGrammatur(),
                    'staerke' => $row->getStaerke(),
                    'breite' => $row->getBreite(),
                    'hoehe' => $row->getHoehe(),
                    'laenge' => $row->getLaenge(),
                    'rollenlaenge' => $row->getRollenlaenge(),
                    'gewicht' => $row->getGewicht(),
                    'papierausruestung' => $row->getPapierausruestung(),
                    'farbnummer' => $row->getFarbnummer(),
                    'farbbezeichnung' => $row->getFarbbezeichnung(),
                    'huelsendurchmesser' => $row->getHuelsendurchmesser(),
                    'kleber' => $row->getKleber(),
                    'abdeckpapier' => $row->getAbdeckpapier(),
                    'laufrichtung' => $row->getLaufrichtung(),
                    'mengenangabe' => $row->getMengenangabe(),
                    'mengenangabe_palette' => $row->getMengenangabePalette(),
                    'inhalt' => $row->getInhalt(),
                    'etiketten_je_blatt' => $row->getEtikettenJeBlatt(),
                    'mengeneinheit' => $row->getMengeneinheit(),
                    'staffelmenge_1' => $row->getStaffelmenge1(),
                    'staffelpreis_1' => $row->getStaffelpreis1(),
                    'staffelmenge_2' => $row->getStaffelmenge2(),
                    'staffelpreis_2' => $row->getStaffelpreis2(),
                    'staffelmenge_3' => $row->getStaffelmenge3(),
                    'staffelpreis_3' => $row->getStaffelpreis3(),
                    'staffelmenge_4' => $row->getStaffelmenge4(),
                    'staffelpreis_4' => $row->getStaffelpreis4(),
                    'staffelmenge_5' => $row->getStaffelmenge5(),
                    'staffelpreis_5' => $row->getStaffelpreis5(),
                    'lagerort' => $row->getLagerort(),
                    'verkaufshinweise' => $row->getVerkaufshinweise(),
                    'abnahmeinfo' => $row->getAbnahmeinfo(),
                    'produkt_beschreibung' => $row->getProduktBeschreibung(),
                    'produkt_eigenschaften' => $row->getProduktEigenschaften(),
                    'produkt_vorteile' => $row->getProduktVorteile(),
                    'produkt_nutzen' => $row->getProduktNutzen(),
                    'produkt_anwendungen' => $row->getProduktAnwendungen(),
                    'produkt_besonderheit' => $row->getProduktBesonderheit(),
                    'musterbuch' => $row->getMusterbuch(),
                    'zur_aufnahme_von' => $row->getEigenschaften(),
                    'preis' => $row->getPreis(),
                    'uuid' => $row->getUuid(),
                    'offset_fix' => $row->getOffsetFix(),
                    'offset_var' => $row->getOffsetVar(),
                    'digital_fix' => $row->getDigitalFix(),
                    'digital_var' => $row->getDigitalVar(),
                    'papierTyp1' => $row->getPapierTyp1(),
                    'papierTyp2' => $row->getPapierTyp2(),
                    'papierTyp3' => $row->getPapierTyp3(),
                    'papierTyp4' => $row->getPapierTyp4(),
                    'papierTyp5' => $row->getPapierTyp5(),
                    'papierTyp6' => $row->getPapierTyp6(),
                    'papierTyp7' => $row->getPapierTyp7(),
                    'papierTyp8' => $row->getPapierTyp8(),
                    'papierTyp9' => $row->getPapierTyp9(),
                    'papierTyp10' => $row->getPapierTyp10(),
                    'papierTyp11' => $row->getPapierTyp11(),
                    'papierTyp12' => $row->getPapierTyp12(),
                    'papierTyp13' => $row->getPapierTyp13(),
                    'papierTyp14' => $row->getPapierTyp14(),
                    'umschlagen' => $row->getUmschlagen(),
                    'happy' => $row->getHappy(),
                    'eq' => $row->getEq(),
                    'sense' => $row->getSense(),
                    'sky' => $row->getSky(),
                    'glam' => $row->getGlam(),
                    'post' => $row->getPost(),
                    'volume' => $row->getVolume(),
                );
//$temp[] = $row->getExportArray($paperDoc);
        }

        $reader = new ArrayReader($temp);
        $writer = new CsvWriter();
        $writer->setStream(fopen('php://output', 'w'));
        $response = new StreamedResponse(function () use ($reader, $writer) {

            foreach ($reader as $row) {
                $writer->writeItem($row);
            }
        });
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, 'export.csv');
        $response->headers->set('Content-Disposition', $disposition);
        return $response;
    }
}
