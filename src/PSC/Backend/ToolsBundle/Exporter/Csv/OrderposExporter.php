<?php

namespace PSC\Backend\ToolsBundle\Exporter\Csv;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use PSC\Backend\ToolsBundle\Export\Writer\ExcelWriter;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Backend\ToolsBundle\Interfaces\ConfigurableElementInterface;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class OrderposExporter implements ExporterInterface, ConfigurableElementInterface
{
    private $_formFactory = null;
    private $_entityManager = null;
    private $_shopService = null;
/** @var Form */
    private $_form = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'csv_orderpos_all';
    }

    public function getDescription()
    {
        return 'Auftragspositionenexport (CSV)';
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
        $builder->add("from", DateType::class, array('label' => 'Von', 'attr' => array('class' => 'form-element')));
        $builder->add("to", DateType::class, array('label' => 'Bis', 'attr' => array('class' => 'form-element')));
    }

    public function getGroup()
    {
        return ExporterRegistry::$ORDER;
    }

    public function setForm(Form $form)
    {
        $this->_form = $form;
    }

    /**
     * @return array[]
     */
    public function export()
    {
        $em = $this->_entityManager;
        $form = $this->_form;
        $response = new StreamedResponse(function () use ($em, $form) {


            $formData = $form->getData();
            $orderRepository = $em->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
            $results = $orderRepository->createQueryBuilder('o')
                ->where("o.createdDate >= :from and o.createdDate <= :to and o.shop = :shop")
                ->setParameter('from', $formData['from'])
                ->setParameter('to', $formData['to'])
                ->setParameter('shop', $this->_shopService->getSelectedShop()->getId())
                ->getQuery()->iterate();
            $handle = fopen('php://output', 'r+');
            while (false !== ($row = $results->next())) {
                fputcsv($handle, array(
                    $row[0]->getOrder()->getUID(),
                    $row[0]->getOrder()->getAlias(),
                    $row[0]->getOrder()->getCreated()->format('Y-m-d H:i:s'),
                    $row[0]->getOrder()->getStatus(),
                    $row[0]->getOrder()->getBrutto(),
                    $row[0]->getOrder()->getPackage(),
                    $row[0]->getOrder()->getContact()->getUsername(),
                    $row[0]->getProduct()->getUid(),
                    $row[0]->getProduct()->getTitle()
                ));
                $em->detach($row[0]);
            }

            fclose($handle);
        });
        $response->headers->set('Content-Type', 'application/force-download');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');
        return $response;
    }
}
