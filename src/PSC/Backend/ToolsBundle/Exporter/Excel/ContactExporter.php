<?php

namespace PSC\Backend\ToolsBundle\Exporter\Excel;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Port\Excel\ExcelWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class ContactExporter implements ExporterInterface
{
    private $_formFactory = null;
    private $_entityManager = null;
    private $_shopService = null;
/** @var Form */
    private $_form = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'excel_contact_sum';
    }

    public function getDescription()
    {
        return 'einfacher Kundenexport (EXCEL)';
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
    }

    public function getGroup()
    {
        return ExporterRegistry::$CONTACT;
    }

    public function setForm(Form $form)
    {
        $this->_form = $form;
    }

    /**
     * @return array[]
     */
    public function export()
    {
        $rows = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact')->getContactsByShop($this->_shopService->getSelectedShop());
        $temp = array();
        $temp[] = array(
            'id' => 'id',
            'uid' => 'uid',
            'account_id' => 'account_id',
            'username' => 'email',
            'firstname' => 'firstname',
            'lastname' => 'lastname',
            'street' => 'street',
            'housenumber' => 'housenumber',
            'zip' => 'zip',
            'city' => 'city',
            'abteilung' => 'abteilung',
            'company' => 'company',
            'company2' => 'company2',
            'email' => 'email',
            'title' => 'title',
            'function' => 'function',
            'position' => 'position',
            'phoneAreaCode' => 'phoneAreaCode',
            'phoneAppendix' => 'phoneAppendix',
            'phone' => 'phone',
            'phonePrefix' => 'phonePrefix',
            'mobileAreaCode' => 'mobileAreaCode',
            'mobileAppendix' => 'mobileAppendix',
            'mobile' => 'mobile',
            'mobilePrefix' => 'mobilePrefix',
            'faxAreaCode' => 'faxAreaCode',
            'faxAppendix' => 'faxAppendix',
            'fax' => 'fax',
            'faxPrefix' => 'faxPrefix',
            'alternativAreaCode' => 'alternativAreaCode',
            'alternativAppendix' => 'alternativAppendix',
            'alternativ' => 'alternativ',
            'alternativType' => 'alternativType'
        );
        foreach ($rows as $row) {
        /** @var Contact $contact */
            $contact = $row->getContact();
            $temp[] = array(
                'id' => (string)$contact->getId(),
                'uid' => (string)$contact->getId(),
                'account_id' => (string)$contact->getAccount()->getId(),
                'username' => $contact->getUsername(),
                'firstname' => $contact->getFirstname(),
                'lastname' => $contact->getLastname(),
                'street' => $contact->getStreet(),
                'housenumber' => $contact->getHouseNumber(),
                'zip' => $contact->getZip(),
                'city' => $contact->getCity(),
                'abteilung' => $contact->getAbteilung(),
                'company' => $contact->getCompany(),
                'company2' => $contact->getCompany2(),
                'email' => $contact->getEmail(),
                'title' => $contact->getTitle(),
                'function' => $contact->getFunction(),
                'position' => $contact->getPosition(),
                'phoneAreaCode' => $contact->getPhoneAreaCode(),
                'phoneAppendix' => $contact->getPhoneAppendix(),
                'phone' => $contact->getPhone(),
                'phonePrefix' => $contact->getPhonePrefix(),
                'mobileAreaCode' => $contact->getMobileAreaCode(),
                'mobileAppendix' => $contact->getMobileAppendix(),
                'mobile' => $contact->getMobile(),
                'mobilePrefix' => $contact->getMobilePrefix(),
                'faxAreaCode' => $contact->getFaxAreaCode(),
                'faxAppendix' => $contact->getFaxAppendix(),
                'fax' => $contact->getFax(),
                'faxPrefix' => $contact->getFaxPrefix(),
                'alternativAreaCode' => $contact->getAlternativAreaCode(),
                'alternativAppendix' => $contact->getAlternativAppendix(),
                'alternativ' => $contact->getAlternativ(),
                'alternativType' => $contact->getAlternativType()
            );
        }

        $reader = new ArrayReader($temp);
        $file = new \SplFileObject('/tmp/test.xls', 'w');
        $writer = new ExcelWriter($file);
        $writer->prepare();
        foreach ($reader as $row) {
            $writer->writeItem($row);
        }
        $writer->finish();
        $response = new BinaryFileResponse($file);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');
        $response->send();
        return $response;
    }
}
