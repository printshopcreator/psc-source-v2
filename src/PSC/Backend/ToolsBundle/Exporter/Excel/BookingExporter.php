<?php

namespace PSC\Backend\ToolsBundle\Exporter\Excel;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Port\Excel\ExcelWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Backend\ToolsBundle\Interfaces\ConfigurableElementInterface;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class BookingExporter implements ExporterInterface, ConfigurableElementInterface
{
    private $_formFactory = null;
    private $_entityManager = null;
    private $_shopService = null;
/** @var Form */
    private $_form = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'excel_article_booking';
    }

    public function getDescription()
    {
        return 'Artikelbuchungen Exportieren (EXCEL)';
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
        $builder->add("from", DateType::class, array('label' => 'Von', 'attr' => array('class' => 'form-element')));
        $builder->add("to", DateType::class, array('label' => 'Bis', 'attr' => array('class' => 'form-element')));
    }

    public function getGroup()
    {
        return ExporterRegistry::$ARTICLE;
    }

    public function setForm(Form $form)
    {
        $this->_form = $form;
    }

    /**
     * @return array[]
     */
    public function export()
    {
        $formData = $this->_form->getData();
        $stockRepository = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Stockbooking');
        $qb = $stockRepository->createQueryBuilder('stock')
            ->leftJoin('stock.product', 'p')
            ->leftJoin('stock.contact', 'c')
            ->where("stock.created >= :from and stock.created <= :to and p.shop = :shop")
            ->setParameter('from', $formData['from'])
            ->setParameter('to', $formData['to'])
            ->setParameter('shop', $this->_shopService->getSelectedShop()->getId())
            ->getQuery();
        $temp = array();
        $rows = $qb->getResult();
        $temp[] = array(
                'articleId' => 'articleId',
                'articleTitle' => 'articleTitle',
                'contactEMail' => 'contactEMail',
                'bookingAmount' => 'bookingAmount',
                'bookingDelivery' => 'bookingDelivery',
                'bookingCreated' => 'bookingCreated',
                'bookingDescription' => 'bookingDescription'
            );
        foreach ($rows as $row) {
            $temp[] = array(
                'articleId' => (string)$row->getProduct()->getId(),
                'articleTitle' => (string)$row->getProduct()->getTitle(),
                'contactEMail' => (string)$row->getContact()->getUsername(),
                'bookingAmount' => (string)$row->getAmount(),
                'bookingDelivery' => (string)$row->getDelivery(),
                'bookingCreated' => (string)$row->getCreated()->format('d.m.Y H:i:s'),
                'bookingDescription' => (string)$row->getDescription()
                );
        }

            $reader = new ArrayReader($temp);
        $file = new \SplFileObject('/tmp/test.xls', 'w');
        $writer = new ExcelWriter($file);
        $writer->prepare();
        foreach ($reader as $row) {
            $writer->writeItem($row);
        }
            $writer->finish();
        $response = new BinaryFileResponse($file);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');
        $response->send();
        return $response;
    }
}
