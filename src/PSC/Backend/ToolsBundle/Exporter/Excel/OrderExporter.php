<?php

namespace PSC\Backend\ToolsBundle\Exporter\Excel;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use PSC\Shop\OrderBundle\Service\Order as PSCOrder;
use Port\Excel\ExcelWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Backend\ToolsBundle\Interfaces\ConfigurableElementInterface;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class OrderExporter implements ExporterInterface, ConfigurableElementInterface
{
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private Shop $_shopService;
    private Status $_statusService;
    private PSCOrder $_orderService;
/** @var Form */
    private $_form = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Status $statusService, DocumentManager $doctrine_mongodb, Shop $shopService, PSCOrder $orderService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
        $this->_statusService = $statusService;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_orderService = $orderService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'excel_order_all';
    }

    public function getDescription()
    {
        return 'Auftragsexport (EXCEL)';
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
        $builder->add("from", DateType::class, array('label' => 'Von', 'attr' => array('class' => 'form-element')));
        $builder->add("to", DateType::class, array('label' => 'Bis', 'attr' => array('class' => 'form-element')));
    }

    public function getGroup()
    {
        return ExporterRegistry::$ORDER;
    }

    public function setForm(Form $form)
    {
        $this->_form = $form;
    }

    /**
     * @return array[]
     */
    public function export()
    {
        $formData = $this->_form->getData();
        $orderRepository = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');
        $qb = $orderRepository->createQueryBuilder('o')
            ->leftJoin('o.contact', 'c')
            ->where("o.created >= :from and o.created <= :to and o.shop = :shop")
            ->setParameter('from', $formData['from'])
            ->setParameter('to', $formData['to'])
            ->setParameter('shop', $this->_shopService->getSelectedShop()->getId())
            ->getQuery();
        $temp = array();
        $rows = $qb->getResult();
        $temp[] = array(
            'orderId' => 'orderId',
            'orderAlias' => 'orderAlias',
            'orderCreated' => 'orderCreated',
            'orderStatus' => 'orderStatus',
            'orderStatusText' => 'orderStatusText',
            'orderNetto' => 'orderNetto',
            'orderBrutto' => 'orderBrutto',
            'orderVat' => 'orderVat',
            'orderPackage' => 'orderPackage',
            'contactEmail' => 'contactEmail',
            'contactCompany' => 'contactCompany',
            'paymentTypeText' => 'paymentTypeText',
            'shippingTypeText' => 'shippingTypeText',
            'invoiceCompany' => 'invoiceCompany',
            'invoiceFirstname' => 'invoiceFirstname',
            'invoiceLastname' => 'invoiceLastname',
            'invoiceStreet' => 'invoiceStreet',
            'invoiceHouseNumber' => 'invoiceHouseNumber',
            'invoiceZip' => 'invoiceZip',
            'invoiceCity' => 'invoiceCity',
            'deliveryCompany' => 'deliveryCompany',
            'deliveryFirstname' => 'deliveryFirstname',
            'deliveryLastname' => 'deliveryLastname',
            'deliveryStreet' => 'deliveryStreet',
            'deliveryHouseNumber' => 'deliveryHouseNumber',
            'deliveryZip' => 'deliveryZip',
            'deliveryCity' => 'deliveryCity',
            'Anzahl' => 'Anzahl',
            'posPriceNet' => 'posPriceNet',
            'posPriceGross' => 'posPriceGross',
            'posPriceVat' => 'posPriceVat',
            'Position' => 'Position',
            'WKFeld1' => 'WKFeld1',
            'WKFeld2' => 'WKFeld2'
        );
/** @var Order $row */
        foreach ($rows as $row) {
            $orderObj = $this->_orderService->getOrderByUid($row->getUID());
            $posstring = "";
            $posanz = "";
            $poscount = count($orderObj->getPositions());
            $counterpos = 1;
            $posprice = "";
            $pospricegross = "";
            $pospricesteuer = "";
            foreach ($orderObj->getPositions() as $position) {
                $posstring .= "Position " . $position->getPos() . ": " . $position->getProduct()->getTitle() . " ";
                if ($position->getSpecialProductTypeObject()->getTyp() === 6) {
                    foreach ($position->getSpecialProductTypeObject()->getOptions() as $option) {
                        if ($option->getId() != "upload_mode" && $option->getId() != "auflage") {
                            $posstring .= $option->getId() . ": " . $option->getValue() . " ";
                        }
                        if ($option->getId() == "auflage") {
                            $posanz .= "" . $option->getValue() . "";
                        }
                    }
                }
                $counterpos++;
                $posprice .= round($position->getAllNet(), 2);
                $pospricegross .= round($position->getAllGross(), 2);
                $pospricesteuer .= round($position->getAllVat(), 2);
                if ($counterpos - 1 != $poscount) {
                    $posanz .= "\n";
                    $posstring .= "\n";
                    $posprice .= "\n";
                    $pospricegross .= "\n";
                    $pospricesteuer .= "\n";
                }
            }
            $temp[] = array(
                'orderId' => (string)$orderObj->getUid(),
                'orderAlias' => (string)$orderObj->getAlias(),
                'orderCreated' => (string)$orderObj->getCreated()->format('d.m.Y H:i:s'),
                'orderStatus' => (string)$orderObj->getStatus(),
                'orderStatusText' => (string)$this->_statusService->getStatusText($orderObj->getStatus()),
                'orderNetto' => (string)$orderObj->getNet(),
                'orderBrutto' => (string)$orderObj->getGross(),
                'orderVat' => (string)$orderObj->getVat(),
                'orderPackage' => (string)$orderObj->getExternalOrderNumber(),
                'contactEmail' => (string)$orderObj->getContact()->getEMail(),
                'contactCompany' => (string)$orderObj->getAccount()->getTitle(),
                'paymentTypeText' => (string)$orderObj->getPayment()->getTitle(),
                'shippingTypeText' => (string)$orderObj->getShipping()->getTitle(),
                'invoiceCompany' => (string)$orderObj->getInvoiceAddress()->getCompany(),
                'invoiceFirstname' => (string)$orderObj->getInvoiceAddress()->getFirstname(),
                'invoiceLastname' => (string)$orderObj->getInvoiceAddress()->getLastname(),
                'invoiceStreet' => (string)$orderObj->getInvoiceAddress()->getStreet(),
                'invoiceHouseNumber' => (string)$orderObj->getInvoiceAddress()->getHouseNumber(),
                'invoiceZip' => (string)$orderObj->getInvoiceAddress()->getZip(),
                'invoiceCity' => (string)$orderObj->getInvoiceAddress()->getCity(),
                'deliveryCompany' => (string)$orderObj->getDeliveryAddress()->getCompany(),
                'deliveryFirstname' => (string)$orderObj->getDeliveryAddress()->getFirstname(),
                'deliveryLastname' => (string)$orderObj->getDeliveryAddress()->getLastname(),
                'deliveryStreet' => (string)$orderObj->getDeliveryAddress()->getStreet(),
                'deliveryHouseNumber' => (string)$orderObj->getDeliveryAddress()->getHouseNumber(),
                'deliveryZip' => (string)$orderObj->getDeliveryAddress()->getZip(),
                'deliveryCity' => (string)$orderObj->getDeliveryAddress()->getCity(),
                'Anzahl' => $posanz,
                'posPriceNet' => $posprice,
                'posPriceGross' => $pospricegross,
                'posPriceVat' => $pospricesteuer,
                'Position' => $posstring,
                'WKFeld1' => $orderObj->getBasketField1(),
                'WKFeld2' => $orderObj->getBasketField2(),

            );
        }

        $reader = new ArrayReader($temp);
        $file = new \SplFileObject('/tmp/test.xls', 'w');
        $writer = new ExcelWriter($file);
        $writer->prepare();
        foreach ($reader as $row) {
            $writer->writeItem($row);
        }
        $writer->finish();
        $response = new BinaryFileResponse($file);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');
        $response->send();
        return $response;
    }
}
