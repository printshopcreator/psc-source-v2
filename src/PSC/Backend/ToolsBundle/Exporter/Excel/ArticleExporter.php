<?php

namespace PSC\Backend\ToolsBundle\Exporter\Excel;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Port\Csv\CsvWriter;
use Port\Excel\ExcelWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ArticleExporter implements ExporterInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_shopService = null;
    private $_doctrine_mongodb = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, \PSC\System\SettingsBundle\Service\Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
        $this->_doctrine_mongodb = $doctrine_mongodb;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'excel_article';
    }

    public function getDescription()
    {
        return 'einfacher Artikelexport (EXCEL)';
    }

    public function getGroup()
    {
        return ExporterRegistry::$ARTICLE;
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
    }

    public function setForm(Form $form)
    {
    }

    /**
     * @return array[]
     */
    public function export()
    {
        /** @var \PSC\System\SettingsBundle\Service\Shop $shop */
        $shop = $this->_doctrine_mongodb
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$this->_shopService->getSelectedShop()->getUid()));
        $rows = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findByShop($this->_shopService->getSelectedShop()->getUid());
        $temp = array();
        $temp[] = array(
            'id' => 'id',
            'uid' => 'uid',
            'uuid' => 'uuid',
            'title' => 'title',
            'textArt' => 'textArt',
            'textFormat' => 'textFormat',
            'nrExtern' => 'nrExtern',
            'nrIntern' => 'nrIntern',
            'ablaufDatum' => 'ablaufDatum',
            'ansprechPartner' => 'ansprechPartner',
            'preis' => 'preis',
            'enable' => 'enable',
            'shopId' => 'shopId',
            'productType' => 'productType',
            'isStock' => 'isStock',
            'stockCount' => 'stockCount',
            'stockMinCount' => 'stockMinCount',
            'stockPlace' => 'stockPlace',
            'stockMaxBuy' => 'stockMaxBuy',
            'buyed' => 'buyed',
            'printPartnerEmail' => 'printPartnerEmail',
            'zusatzAbmessung' => 'zusatzAbmessung',
            'zusatzDesigner' => 'zusatzDesigner',
            'zusatzShipping' => 'zusatzShipping',
            'custom1' => $shop->getProductFieldName1(),
            'custom2' => $shop->getProductFieldName2(),
            'custom3' => $shop->getProductFieldName3(),
            'custom4' => $shop->getProductFieldName4(),
            'custom5' => $shop->getProductFieldName5(),
            'custom6' => $shop->getProductFieldName6(),
            'custom7' => $shop->getProductFieldName7(),
            'custom8' => $shop->getProductFieldName8(),
            'custom9' => $shop->getProductFieldName9(),
            'custom10' => $shop->getProductFieldName10(),
            'custom11' => $shop->getProductFieldName11(),
            'custom12' => $shop->getProductFieldName12(),
        );
/** @var Product $row */
        foreach ($rows as $row) {
/** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
            $productDoc = $this->_doctrine_mongodb
                ->getRepository('PSC\Shop\EntityBundle\Document\Product')
                ->findOneBy(array('uid' => (string)$row->getUid()));
            if ($productDoc) {
                $row->setAblaufDatum($productDoc->getAblaufDatum());
                $row->setAnsprechPartner($productDoc->getAnsprechPartner());
                $row->setZusatzDesigner($productDoc->getZusatzDesigner());
                $row->setZusatzAbmessung($productDoc->getZusatzAbmessung());
                $row->setZusatzShipping($productDoc->getZusatzShipping());
                $row->setCustom1($productDoc->getCustom1());
                $row->setCustom2($productDoc->getCustom2());
                $row->setCustom3($productDoc->getCustom3());
                $row->setCustom4($productDoc->getCustom4());
                $row->setCustom5($productDoc->getCustom5());
                $row->setCustom6($productDoc->getCustom6());
                $row->setCustom7($productDoc->getCustom7());
                $row->setCustom8($productDoc->getCustom8());
                $row->setCustom9($productDoc->getCustom9());
                $row->setCustom10($productDoc->getCustom10());
                $row->setCustom11($productDoc->getCustom11());
                $row->setCustom12($productDoc->getCustom12());
            }

            $temp[] = $row->getExportArray($productDoc);
        }

        $reader = new ArrayReader($temp);
        $file = new \SplFileObject('/tmp/test.xls', 'w');
        $writer = new ExcelWriter($file);
        $writer->prepare();
        foreach ($reader as $row) {
            $writer->writeItem($row);
        }
        $writer->finish();
        $response = new BinaryFileResponse($file);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');
        $response->send();
        return $response;
    }
}
