<?php

namespace PSC\Backend\ToolsBundle\Exporter\Excel;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use LogicException;
use Port\Excel\ExcelWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;

class AccountExporter implements ExporterInterface
{
    private $_formFactory = null;
    private $_entityManager = null;
    private $_shopService = null;
/** @var Form */
    private $_form = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Shop $shopService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'excel_account_sum';
    }

    public function getDescription()
    {
        return 'einfacher Firmenexport (EXCEL)';
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
    }

    public function getGroup()
    {
        return ExporterRegistry::$ACCOUNT;
    }

    public function setForm(Form $form)
    {
        $this->_form = $form;
    }

    /**
     * @return array[]
     */
    public function export()
    {
        $rows = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Account')->getAccountsByShop($this->_shopService->getSelectedShop());
        $temp = array();
        $temp[] = array(
            'id' => 'id',
            'uid' => 'uid',
            'parent_id' => 'parent_id',
            'name' => 'name',
            'email' => 'email',
        );
        foreach ($rows as $row) {
        /** @var Account $account */
            $account = $row->getAccount();
            $temp[] = array(
                'id' => (string)$account->getId(),
                'uid' => (string)$account->getId(),
                'parent_id' => (string)$account->getParentId(),
                'name' => (string)$account->getTitle(),
                'email' => (string)$account->getEmail(),
            );
        }

        $reader = new ArrayReader($temp);
        $file = new \SplFileObject('/tmp/test.xls', 'w');
        $writer = new ExcelWriter($file);
        $writer->prepare();
        foreach ($reader as $row) {
            $writer->writeItem($row);
        }
        $writer->finish();
        $response = new BinaryFileResponse($file);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');
        $response->send();
        return $response;
    }
}
