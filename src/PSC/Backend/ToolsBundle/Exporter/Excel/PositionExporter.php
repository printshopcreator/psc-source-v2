<?php

namespace PSC\Backend\ToolsBundle\Exporter\Excel;

require_once(__DIR__ . '/../../../../Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use LogicException;
use Port\Excel\ExcelWriter;
use Port\Reader\ArrayReader;
use PSC\Backend\ToolsBundle\Interfaces\ExporterInterface;
use PSC\Backend\ToolsBundle\Service\ExporterRegistry;
use PSC\Backend\ToolsBundle\Interfaces\ConfigurableElementInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\System\SettingsBundle\Service\PaperDB;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
class PositionExporter implements ExporterInterface, ConfigurableElementInterface
{
    private $_formFactory = null;
    private $_entityManager = null;
    private $_securityContext = null;
    private $_shopService = null;
/** @var Form */
    private $_form = null;
/**
     * @var PaperDB
     */
    private PaperDB $_paperDbService;
/**
     * @var DocumentManager
     */
    private DocumentManager $_mongoDb;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, Shop $shopService, PaperDB $paperDbService, DocumentManager $mongodb)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_shopService = $shopService;
        $this->_paperDbService = $paperDbService;
        $this->_mongoDb = $mongodb;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'excel_orderspos_buying';
    }

    public function getDescription()
    {
        return 'Auftragspositionenexport (EXCEL)';
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
        $builder->add('product', EntityType::class, array(
            'class' => 'PSCEntityBundle:Product',
            'query_builder' => function (EntityRepository $er) {

                return $er->createQueryBuilder('g')
                    ->andWhere('g.shop = :shop and g.originalProduct = 0')
                    ->setParameter('shop', $this->_shopService->getSelectedShop()->getUid())
                    ->orderBy('g.title', 'ASC');
            },
            'choice_label' => 'title',
            'required' => false,
            'placeholder' => 'Alle',
            'label' => 'Vorgänger'
        ));
        $builder->add("from", DateType::class, array('label' => 'Von', 'attr' => array('class' => 'form-element')));
        $builder->add("to", DateType::class, array('label' => 'Bis', 'attr' => array('class' => 'form-element')));
        $builder->add("calc_options", TextType::class, array('label' => 'Kalkulationsoptionen (auflage,papier)', 'required' => false, 'attr' => array('class' => 'form-element')));
    }

    public function getGroup()
    {
        return ExporterRegistry::$ORDER;
    }

    public function setForm(Form $form)
    {
        $this->_form = $form;
    }

    /**
     * @return array[]
     */
    public function export()
    {
        $formData = $this->_form->getData();
        $orderRepository = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
        if ($formData['product'] === null) {
            $qb = $orderRepository->createQueryBuilder('o')
                ->where("o.createdDate >= :from and o.createdDate <= :to and o.shop = :shop")
                ->setParameter('from', $formData['from'])
                ->setParameter('to', $formData['to'])
                ->setParameter('shop', $this->_shopService->getSelectedShop()->getId())
                ->getQuery();
        } else {
            $qb = $orderRepository->createQueryBuilder('o')
                ->where("o.createdDate >= :from and o.createdDate <= :to and o.shop = :shop AND o.product = :product")
                ->setParameter('from', $formData['from'])
                ->setParameter('to', $formData['to'])
                ->setParameter('shop', $this->_shopService->getSelectedShop()->getId())
                ->setParameter('product', $formData['product'])
                ->getQuery();
        }

        $rows = $qb->getResult();
        $calcOptions = explode(",", $formData['calc_options']);
        $data[] = array();
        $temp = array(
            'order' => 'order',
            'created' => 'created',
            'brutto' => 'brutto',
            'contact' => 'contact',
            'productId' => 'articleId',
            'productTitle' => 'articleName',
            'weight' => 'weight',
            'articleName' => 'articleName'
        );
        foreach ($calcOptions as $key) {
            $temp[$key] = $key;
        }

        $data[] = $temp;
        $paperContainer = new PaperContainer();
        $paperContainer->parse(simplexml_load_string($this->_shopService->getSelectedShop()->getInstall()->getPaperContainer()));
        $engine = new Engine();
        $engine->setPaperRepository($this->_paperDbService);
        $engine->setPaperContainer($paperContainer);
        if ($this->_shopService->getSelectedShop()->getInstall()->getCalcTemplates()) {
            $engine->setTemplates('<root>' . $this->_shopService->getSelectedShop()->getInstall()->getCalcTemplates() . '</root>');
        }

        /** @var Orderpos $row */
        foreach ($rows as $row) {
/** @var Position $objDoc */
            $objDoc = $this->_mongoDb
                ->getRepository('PSCEntityBundle:Position')
                ->findOneBy(['uid' => (string)$row->getId()]);
            $temp = array(
                'order' => (string)$row->getOrder()->getAlias(),
                'created' =>  (string)$row->getOrder()->getCreated()->format('d.m.Y H:i:s'),
                'brutto' => (string)$row->getOrder()->getBrutto(),
                'contact' => (string)$row->getOrder()->getContact()->getEmail(),
                'productId' => (string)$row->getProduct()->getId(),
                'productTitle' => (string)$row->getProduct()->getTitle(),
                'weight' => (string)$row->getWeight(),
            );
            $objPosition = unserialize(($row->getData()));
            if ($row->hasCalcXml()) {
                $engine->loadString($row->getCalcXml());
                $engine->setVariables($objPosition->getOptions());
                $engine->setFormulas($row->getShop()->getFormel());
                $engine->setParameters($row->getShop()->getParameter());
                $engine->setSavedCalcReferences($objDoc->getCalcReferences());
                if (isset($objPosition->getOptions()['kalk_artikel'])) {
                    $engine->setActiveArticle($objPosition->getOptions()['kalk_artikel']);
                }
                $articleCalc = $engine->getArticle();
                $temp['articleName'] = $articleCalc->getName();
            } else {
                if (isset($objPosition->getOptions()['kalk_artikel'])) {
                    $articleCalc = new \PSC\Library\Calc\Article($objPosition->getOptions()['kalk_artikel']);
                    $temp['articleName'] = $objPosition->getOptions()['kalk_artikel'];
                } else {
                    $articleCalc = new \PSC\Library\Calc\Article("test");
                    $temp['articleName'] = '';
                }
            }

            foreach ($calcOptions as $key) {
                if ($articleCalc->getOptionById($key)) {
                    $temp[$key] = (string)$articleCalc->getOptionById($key)->getValue();
                } else {
                    $temp[$key] = "";
                }
            }

            $data[] = $temp;
        }

        $reader = new ArrayReader($data);
        $file = new \SplFileObject('/tmp/test.xls', 'w');
        $writer = new ExcelWriter($file);
        $writer->prepare();
        foreach ($reader as $row) {
            $writer->writeItem($row);
        }
        $writer->finish();
        $response = new BinaryFileResponse($file);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/vnd.ms-excel');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.xls"');
        $response->send();
        return $response;
    }
}
