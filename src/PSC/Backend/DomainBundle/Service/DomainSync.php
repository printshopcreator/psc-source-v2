<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Backend\DomainBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping as ORM;
use PSC\Shop\EntityBundle\Entity\Domain;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Yaml\Yaml;

class DomainSync
{
    private $projectDir;
    private $domains = [];
/**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    public function __construct($projectDir, EntityManagerInterface $entityManager)
    {
        $this->projectDir = $projectDir;
        $this->entityManager = $entityManager;
        $this->loadDomains();
    }

    public function loadDomains()
    {
        if (file_exists($this->projectDir . '/compose.yml')) {
            $yamlArray = Yaml::parseFile($this->projectDir . '/compose.yml');
            if (isset($yamlArray['services']['web']['environment'])) {
                foreach ($yamlArray['services']['web']['environment'] as $entry) {
                    if (strpos($entry, 'LETSENCRYPT_HOST') !== false) {
                        $entry = preg_replace("/\s+/", "", $entry);
                        $entry = substr($entry, 17);
                        $this->domains = explode(",", $entry);
                    }
                }
            }
        }
    }

    public function syncDomains()
    {

        $domains = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findAll();
        $this->domains = [];
/** @var Domain $domain */
        foreach ($domains as $domain) {
            if ($this->isDnsOk($domain->getIp())) {
                $this->domains[] = $domain->getHost();
            }
        }
        $mail = '';
        $yamlArray = Yaml::parseFile($this->projectDir . '/compose.yml');
        if (isset($yamlArray['services']['web']['environment'])) {
            foreach ($yamlArray['services']['web']['environment'] as $entry) {
                if (strpos($entry, 'LETSENCRYPT_EMAIL') !== false) {
                    $mail = substr($entry, 18);
                }
            }
        }
        $yamlArray['services']['web']['environment'] = [
            'LETSENCRYPT_EMAIL=' . $mail,
            'VIRTUAL_HOST=' . implode(',', $this->domains),
            'VIRTUAL_PORT=8000',
            'VIRTUAL_NETWORK=nginx-proxy',
            'LETSENCRYPT_HOST=' . implode(',', $this->domains)
        ];
        file_put_contents($this->projectDir . '/compose.yml', Yaml::dump($yamlArray, 5));
        file_put_contents($this->projectDir . '/watch/update', "1");
    }

    public function upgrade()
    {
        file_put_contents($this->projectDir . '/watch/upgrade', "1");
    }

    public function isInSync($name): bool
    {
        return in_array($name, $this->domains);
    }

    public function isDnsOk($ip): bool
    {
        return $ip == getenv('ftpIp');
    }
}
