<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Backend\DomainBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\Backend\DomainBundle\Form\Backend\DomainType;
use PSC\Backend\DomainBundle\PSCBackendDomainBundle;
use PSC\Backend\DomainBundle\Service\DomainSync;
use PSC\Shop\EntityBundle\Document\Instance;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\MotivBundle\Form\Backend\MotivType;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\Log;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Yaml\Yaml;

/**
 * DomainController fürs Backend
 *
 * @package    PSC\Backend\Domain\Backend
 * @subpackage Controller
 */
class ListController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    public function __construct(Log $logService)
    {
        $this->logService = $logService;
    }

    /**
     * Default Seite
     *
     * @Route("/list/index", name="psc_backend_domain_list_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param Shop $shopService
     * @param DocumentManager $mongoService
     * @param EntityManagerInterface $em
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, DocumentManager $mongoService, EntityManagerInterface $em, DomainSync $sync)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $domain = new Domain($selectedShop);
        $form = $this->createForm(DomainType::class, $domain);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $domainCheck = $em
                ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findBy(array('host' => $domain->getHost()));
            if (count($domainCheck) > 0) {
                $form->get('host')->addError(new FormError("Domain schon vorhanden"));
            } else {
                $em->persist($domain);
                $em->flush();
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCBackendDomainBundle::class, $domain->getHost(), "Domain added");
            }
        }

        $domains = $em
        ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->getAllByShopId($selectedShop);
        return array(
            'domains' => $domains,
            'domainSync' => $sync,
            'form' => $form->createView()
        );
    }
    /**
     * dataAction
     *
     * @Route("/list/sync", name="psc_backend_domain_list_sync")
     * @Security("is_granted('ROLE_SHOP')")
     *
     */
    public function syncAction(DomainSync $sync)
    {
        $sync->syncDomains();
        return new JsonResponse(['success' => true]);
    }

    /**
     * dataAction
     *
     * @Route("/list/delete/{uid}", name="psc_backend_domain_list_delete")
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $mongoManager
     * @param string $uid Uid
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAction(Shop $shopService, EntityManagerInterface $entityManager, DocumentManager $mongoManager, $uid)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Domain $domain */
        $domain = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findOneBy(array('uid' => $uid, 'shop' => $selectedShop));
        $domainText = $domain->getHost();
        $entityManager->remove($domain);
        $entityManager->flush();
        $this->sync($entityManager, $mongoManager);
        $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCBackendDomainBundle::class, $domainText, "Domain deleted");
        return $this->redirect($this->generateUrl("psc_backend_domain_list_index"));
    }

    protected function sync(EntityManagerInterface $entityManager, DocumentManager $mongoManager)
    {
        /** @var Instance $instance */
        $instance = $mongoManager
            ->getRepository('PSCEntityBundle:Instance')
            ->findOneBy(['appId' => "1"]);
        if ($instance && $instance->getSyncServer() != null) {
            $client = new Client();
            $domains = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findAll();
            $temp = array();
        /** @var Domain $domain */
            foreach ($domains as $domain) {
                $temp[] = array('name' => strtolower($domain->getHost()), 'letsEncrypt' => $domain->isLetsEncrypt());
            }

            $response = $client->put($instance->getSyncServer() . '/index.php/json/push/domains', array(
                    'json' => array(
                        'uuid' => $instance->getAppId(),
                        'data' => $temp
                    )
                ));
        }
    }
}
