<?php

namespace PSC\Backend\DashboardBundle\Menu;

use Knp\Menu\FactoryInterface;
use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use PSC\Backend\DashboardBundle\Event\MenuEvents;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class MenuBuilder
{
    private $factory;
    private $container;
/**
     * @param FactoryInterface $factory
     * @param Container $container
     */
    public function __construct(FactoryInterface $factory, Container $container)
    {
        $this->factory = $factory;
        $this->container = $container;
    }

    public function createBackendMainMenu(RequestStack $request)
    {
        $menu = $this->factory->createItem('adminMainMenu');
        $this->container->get('event_dispatcher')->dispatch(new ConfigureMenuEvent($this->factory, $menu), MenuEvents::CONFIGURE_LEFT);
        $this->reorderMenuItems($menu);
        return $menu;
    }

    public function createBackendTopMenu(RequestStack $request)
    {
        $menu = $this->factory->createItem('adminTopMenu');
        $this->container->get('event_dispatcher')->dispatch(new ConfigureMenuEvent($this->factory, $menu), MenuEvents::CONFIGURE_TOP);
        $menu->addChild('Home', array('route' => 'homepage'));
        return $menu;
    }

    public function createBackendSystemMenu(RequestStack $request)
    {
        $menu = $this->factory->createItem('adminSystemMenu');
        $this->container->get('event_dispatcher')->dispatch(new ConfigureMenuEvent($this->factory, $menu), MenuEvents::CONFIGURE_SYSTEM);
        return $menu;
    }

    public function reorderMenuItems($menu)
    {

        $menuOrderArray = array();
        $addLast = array();
        $alreadyTaken = array();
        foreach ($menu->getChildren() as $key => $menuItem) {
            if ($menuItem->hasChildren()) {
                $this->reorderMenuItems($menuItem);
            }

            $orderNumber = $menuItem->getExtra('orderNumber');
            if ($orderNumber != null) {
                if (!isset($menuOrderArray[$orderNumber])) {
                    $menuOrderArray[$orderNumber] = $menuItem->getName();
                } else {
                    $addLast[] = $menuItem->getName();
                // $alreadyTaken[] = array('orderNumber' => $orderNumber, 'name' => $menuItem->getName());
                }
            } else {
                $addLast[] = $menuItem->getName();
            }
        }

        // sort them after first pass
        ksort($menuOrderArray);
// handle position duplicates
        if (count($alreadyTaken)) {
            foreach ($alreadyTaken as $key => $value) {
            // the ever shifting target
                $keysArray = array_keys($menuOrderArray);
                $position = array_search($key, $keysArray);
                if ($position === false) {
                    continue;
                }

                $menuOrderArray = array_merge(array_slice($menuOrderArray, 0, $position), array($value), array_slice($menuOrderArray, $position));
            }
        }

        // sort them after second pass
        ksort($menuOrderArray);
// add items without ordernumber to the end
        if (count($addLast)) {
            foreach ($addLast as $key => $value) {
                $menuOrderArray[] = $value;
            }
        }

        if (count($menuOrderArray)) {
            $menu->reorderChildren($menuOrderArray);
        }
    }
}
