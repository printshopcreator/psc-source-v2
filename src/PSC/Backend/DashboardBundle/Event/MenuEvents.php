<?php

namespace PSC\Backend\DashboardBundle\Event;

final class MenuEvents
{
    /**
     * @var string
     */
    const CONFIGURE_LEFT = 'psc.backend.main';

    /**
     * @var string
     */
    const CONFIGURE_SYSTEM = 'psc.backend.system';

    /**
     * @var string
     */
    const CONFIGURE_TOP = 'psc.backend.sidebar';
}
