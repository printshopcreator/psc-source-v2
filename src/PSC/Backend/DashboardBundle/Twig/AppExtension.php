<?php

namespace PSC\Backend\DashboardBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class AppExtension extends AbstractExtension
{
    public function getFunctions()
    {
        return array(
            new TwigFunction('file_exists', 'file_exists'),
        );
    }

    public function getName()
    {
        return 'file_exists';
    }
}
