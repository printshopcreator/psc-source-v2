<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Backend\DashboardBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * ShopController fürs ProductionBundle
 *
 * @package    PSC\Backend\Production
 * @subpackage Controller
 */
class ShopController extends AbstractController
{
    /**
     * Meine Shops
     *
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @Template()
     *
     * @param Request $request
     * @param Shop $shop
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function myEditableShopsAction(Request $request, Shop $shop, JWTTokenManagerInterface $jwtManager)
    {

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shop->getSelectedShop();
        $shops = $shop->getMyEditableShops();
        return array('shops' => $shops, 'jwt' => $jwtManager->create($this->getUser()),  'selectedShop' => $selectedShop, 'displayDeletedShop' => $request->getSession()->get('displayDeletedShop', false));
    }

    /**
     * Change Shop
     *
     * @Security("is_granted('ROLE_SHOP')")
     * @route("/shop/change/{shop_uuid}", name="psc_backend_dashboard_shop_change")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param \Symfony\Component\Security\Core\Security $security
     * @param $shop_uuid
     * @return RedirectResponse
     */
    public function changeShopAction(Request $request, EntityManagerInterface $em, \Symfony\Component\Security\Core\Security $security, $shop_uuid)
    {
        $em
            ->getRepository('PSC\Shop\EntityBundle\Entity\ShopContact')
            ->changeSelectedShop($security->getUser(), $shop_uuid);
        $request->getSession()->set('selectedShop', $shop_uuid);
        return $this->redirect($this->generateUrl('psc_backend_dashboard_index'));
    }

    /**
     * Change Shop
     *
     * @Security("is_granted('ROLE_SHOP')")
     * @route("/shop/deleted/toogle", name="psc_backend_dashboard_toogle_deleted_shop")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @param \Symfony\Component\Security\Core\Security $security
     * @param Shop $shopService
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function toogleDisplayDeletedShopAction(Request $request, EntityManagerInterface $em, \Symfony\Component\Security\Core\Security $security, Shop $shopService)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        if (!!$request->getSession()->get('displayDeletedShop', false) && $selectedShop->isDeleted()) {
            $shops = $em
                ->getRepository('PSC\Shop\EntityBundle\Entity\ShopContact')->myEditableShopsWithNoDeleted($security->getUser());
            $request->getSession()->set('selectedShop', $shops[0]->getUid());
        }
        $request->getSession()->set('displayDeletedShop', !$request->getSession()->get('displayDeletedShop', false));
        return $this->redirect($this->generateUrl('psc_backend_dashboard_index'));
    }
}
