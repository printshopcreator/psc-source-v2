<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Backend\DashboardBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\QueueBundle\Service\Queue\Manager;
use PSC\System\SettingsBundle\Service\Instance;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\UpdateBundle\Service\Migration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Intl\NumberFormatter\NumberFormatter;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * DashboardController fürs ProductionBundle
 *
 * @package    PSC\Backend\Production
 * @subpackage Controller
 */
class DashboardController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @Route("/dashboard", name="psc_backend_dashboard_index")
     * @Template()
     *
     * @param Migration $migration
     * @param Shop $shop
     * @param EntityManagerInterface $entityManager
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Migration $migration, Shop $shop, EntityManagerInterface $entityManager, Manager $queueService, Instance $instanceService)
    {

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shop->getSelectedShop();
        $tempSales = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order')->getIncomeByShop($selectedShop->getUid());
        $fmt = new \NumberFormatter(locale_get_default(), \NumberFormatter::CURRENCY);
        $sales = array();
        foreach ($tempSales as $sale) {
            $sales[] = array('month' => $sale['month'], 'brutto' => round($sale['brutto']));
        }

        if ($migration->checkIfMigrationMustRun()) {
            return $this->redirectToRoute('psc_system_update_migration_do');
        }

        return array(
            'user' => $this->getUser(),
            'shop' => $selectedShop,
            'queueErrorCount' => $queueService->getErrorJobCount(),
            'instance' => $instanceService->getInstance(),
            'sales' => json_encode($sales)
        );
    }
}
