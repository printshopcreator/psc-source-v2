<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Backend\DashboardBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * DefaultController fürs ProductionBundle
 *
 * @package    PSC\Backend\Production
 * @subpackage Controller
 */
class DefaultController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/", name="psc_backend_default_index")
     * @Template()
     *
     * @return View
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl("psc_backend_dashboard_index"));
    }
}
