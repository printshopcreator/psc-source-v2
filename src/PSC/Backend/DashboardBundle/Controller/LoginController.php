<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Backend\DashboardBundle\Controller;

use Http\Message\Authentication;
use PSC\System\UpdateBundle\Service\Migration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * LoginController fürs ProductionBundle
 *
 * @package    PSC\Backend\Production
 * @subpackage Controller
 */
class LoginController extends AbstractController
{
    /**
     * Login Seite
     *
     * @Route("/login", name="psc_backend_login")
     * @Template()
     *
     * @param Migration $migration
     * @param AuthenticationUtils $authenticationUtils
     * @return array
     */
    public function indexAction(Migration $migration, AuthenticationUtils $authenticationUtils)
    {
        if ($migration->checkIfMigrationMustRun()) {
            $migration->executeMigrations();
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
// last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();
        return array(
            // last username entered by the user
            'last_username' => $lastUsername,
            'error'         => $error,
        );
    }

    /**
     * @Route("/logout", name="psc_backend_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }
}
