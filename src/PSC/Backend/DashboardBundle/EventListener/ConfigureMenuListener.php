<?php

namespace PSC\Backend\DashboardBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;

class ConfigureMenuListener
{
    protected $eventDispatcher;
/**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param  \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();
        $menu->addChild('Dashboard', array(
            'route' => 'psc_backend_dashboard_index',
            'extras' => array(
                'icon' => 'fas fa-chalkboard',
                'orderNumber' => 1
        )));
    }

    /**
     * @param  \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureTop(ConfigureMenuEvent $event)
    {
    }

    /**
     * @param  \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureSystem(ConfigureMenuEvent $event)
    {
    }
}
