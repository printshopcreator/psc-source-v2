<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\ConfigurationlayouterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Gaufrette\Util\Path;
use PSC\Component\SteplayouterBundle\Config\Converter\ToArray;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * StoreController für ConfigurationlayouterBundle
 *
 * @package    PSC\Component\Configuration
 * @subpackage Controller
 */
class StoreController extends AbstractController
{
    /**
     * Save the Steplayouter Json
     *
     * @Route("/configurationlayouter/store/save", name="psc_component_configurationlayouter_store_save")
     *
     * @return void
     */
    public function saveAction(Request $request, EntityManagerInterface $entityManager)
    {

        $data = json_decode($request->getContent(), true);
        $layoutDesignData = new Layoutdesigndata();
        $layoutDesignData->setUuid($data['layouter']);
        $layoutDesignData->setArticleUuid($data['article']);
        $layoutDesignData->setDesign($data['payload']);
        $entityManager->persist($layoutDesignData);
        $entityManager->flush();
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'uuid' => $uuid, 'articleUuid' => $articleUuid)));
        return $json;
    }

    /**
     * Load the Steplayouter Json
     *
     * @Route("/configurationlayouter/store/load/{uuid}", name="psc_component_configurationlayouter_store_load")
     *
     * @param EntityManagerInterface $entityManager
     * @param $uuid
     * @return JsonResponse
     */
    public function loadAction(EntityManagerInterface $entityManager, $uuid)
    {

        $dataDesign = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $uuid));
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'data' => $dataDesign->getDesign())));
        return $json;
    }

    /**
     * Save the Steplayouter Json
     *
     * @Route("/configurationlayouter/store/update", name="psc_component_configurationlayouter_store_update")
     *
     * @return void
     */
    public function updateAction(Request $request, EntityManagerInterface $entityManager)
    {

        $data = json_decode($request->getContent(), true);
        $layoutDesignData = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $data['uuid']));
        $layoutDesignData->setDesign($data['payload']);
        $entityManager->persist($layoutDesignData);
        $entityManager->flush();
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'article' => $layoutDesignData->getArticleUuid())));
        return $json;
    }

    /**
     * Pdf Preview
     *
     * @Route("/configurationlayouter/store/check/{layouter_uuid}", name="psc_component_configurationlayouter_store_check")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $layouter_uuid
     * @return JsonResponse
     */
    public function checkAction(Request $request, EntityManagerInterface $entityManager, $layouter_uuid)
    {
        $layoutData = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $layouter_uuid));
        $data = array('success' => true, 'found' => false);
        if ($layoutData) {
            $data = array('success' => true, 'found' => true, 'data' => $layoutData->getDesign());
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setContent(json_encode($data));
        return $jsonResponse;
    }
}
