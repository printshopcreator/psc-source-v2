<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\ApiBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Stockbooking
{
    /**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $id;
/**
     * @OA\Property(type="object")
     */
    public Contact $contact;
/**
     * @OA\Property(type="object")
     */
    public Product $product;
/**
     * @OA\Property(type="integer")
     */
    public int $amount = 0;
/**
     * @OA\Property(type="date")
     */
    public \DateTime $created;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $deliveryNumber;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $description;
}
