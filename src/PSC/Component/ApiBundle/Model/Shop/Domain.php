<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\ApiBundle\Model\Shop;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Domain
{
    public function __construct(int $id, string $name, bool $ssl)
    {
        $this->id = $id;
        $this->name = $name;
        $this->ssl = $ssl;
    }

    /**
     * @OA\Property(type="integer")
     */
    public int $id;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $name = "";
/**
     * @OA\Property(type="boolean")
     */
    public bool $ssl = false;
}
