<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\ApiBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Shop
{
    /**
     * @OA\Property(type="integer")
     */
    public int $id = 0;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $uuid = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $name = "";
/**
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Component\ApiBundle\Model\Shop\Domain::class)))
     */
    public array $domains = [];
/**
     * @OA\Property(type="boolean")
     */
    public bool $deleted = false;
/**
     * @OA\Property(type="boolean")
     */
    public bool $private = false;
}
