<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\ApiBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Plugin
{
    public function __construct(string $uuid, string $title)
    {
        $this->uuid = $uuid;
        $this->title = $title;
    }

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $uuid = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $title = "";
/**
     * @OA\Property(type="boolean")
     */
    public bool $installed = false;
}
