<?php

namespace PSC\Component\ApiBundle\Hydrate;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Model\Shop\Domain;

class Shop
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function hydrateToModel(\PSC\Shop\EntityBundle\Entity\Shop $cmsEntity)
    {

        $tmp = new \PSC\Component\ApiBundle\Model\Shop();
        $tmp->id = $cmsEntity->getUID();
        $tmp->name = $cmsEntity->getTitle();
        $tmp->uuid = $cmsEntity->getUuid();
        $tmp->deleted = (bool)$cmsEntity->isDeleted();
        $tmp->private = (bool)$cmsEntity->isPrivate();
        $domains = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->getAllByShopId($cmsEntity);
        $domainsTemp = [];
/** @var \PSC\Shop\EntityBundle\Entity\Domain $domain */
        foreach ($domains as $domain) {
            $domainsTemp[] = new Domain($domain->getUid(), $domain->getHost(), $domain->isLetsEncrypt());
        }

        $tmp->domains = $domainsTemp;
        return $tmp;
    }
}
