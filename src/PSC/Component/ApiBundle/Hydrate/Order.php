<?php

namespace PSC\Component\ApiBundle\Hydrate;

use Doctrine\ORM\EntityManagerInterface;

class Order
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function hydrateToModel(\PSC\Shop\EntityBundle\Entity\Cms $cmsEntity)
    {

        $cmsObj = new \PSC\Component\ApiBundle\Model\Cms();
        $cmsObj->id = $cmsEntity->getUID();
        $cmsObj->title = $cmsEntity->getTitle();
        $cmsObj->text = $cmsEntity->getText();
        return $cmsObj;
    }
}
