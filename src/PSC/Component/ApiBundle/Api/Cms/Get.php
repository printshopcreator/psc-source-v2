<?php

namespace PSC\Component\ApiBundle\Api\Cms;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Cms\Cms\Output;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Hydrate\Cms;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class Get extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Cms $hydrateCms;
    private \PSC\System\SettingsBundle\Service\Shop $shopService;

    public function __construct(EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, Cms $hydrateCms)
    {
        $this->entityManager = $entityManager;
        $this->hydrateCms = $hydrateCms;
        $this->shopService = $shopService;
    }

    /**
     * Cms
     *
     * @Route("/cms", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="cms",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Cms\Cms\Output::class))
     * )
     * @OA\Tag(name="Cms")
     */
    public function allAction(): JsonResponse
    {
        $cmss = $this->entityManager->getRepository(\PSC\Shop\EntityBundle\Entity\Cms::class)->findAllByShop($this->shopService->getShopByDomain());

        $data = [];
        /** @var \PSC\Shop\EntityBundle\Entity\Cms $cms */
        foreach ($cmss as $cms) {
            $data[] = $this->hydrateCms->hydrateToModel($cms);
        }

        return $this->json(new Output($data));
    }

    /**
     * Get One Cms
     *
     * @Route("/cms/{id}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="found cms",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Model\Cms::class))
     * )
     * @OA\Response(
     *     response=404,
     *     description="not found",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Error\NotFound::class))
     * )
     * @OA\Tag(name="Cms")
     */
    public function one($id): JsonResponse
    {
        $cms = $this->entityManager->getRepository(\PSC\Shop\EntityBundle\Entity\Cms::class)->findOneBy(['uid' => $id]);

        if ($cms) {
            return $this->json($this->hydrateCms->hydrateToModel($cms));
        }

        return $this->json(new NotFound());
    }
}
