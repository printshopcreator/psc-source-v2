<?php

namespace PSC\Component\ApiBundle\Api\System;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Model\Instance;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class Info extends AbstractController
{
    /**
     * Get system info.
     *
     *
     * @Route("/system/info", methods={"GET"})
     * @Security(name="ApiKeyAuth")
     * @OA\Response(
     *     response=200,
     *     description="Returns system info",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Model\Instance::class))
     * )
     * @OA\Tag(name="System")
     * @IsGranted("ROLE_API")
     */
    public function infoAction(): JsonResponse
    {
        $data = new Instance();
        $data->sftpHost = getenv('ftpIp');
        $data->ip = getenv('ftpIp');
        $data->sftpUserName = getenv('ftpUsername');
        $data->sftpPassword = getenv('ftpPassword');
        $data->sftpPort = getenv('ftpPort');

        return $this->json($data);
    }
}
