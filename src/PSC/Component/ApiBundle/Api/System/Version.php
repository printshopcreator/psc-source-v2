<?php

namespace PSC\Component\ApiBundle\Api\System;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;

class Version extends AbstractController
{
    private KernelInterface $kernel;

    public function __construct(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    /**
     * Get Release version.
     *
     *
     * @Route("/system/version", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="release version",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Model\Version::class))
     * )
     * @OA\Tag(name="System")
     */
    public function versionAction(): JsonResponse
    {
        $version = Yaml::parse(file_get_contents($this->kernel->getProjectDir() . '/version.yaml'));

        $data = new \PSC\Component\ApiBundle\Model\Version();
        $data->release = $version['info']['release'];
        $data->datum = $version['info']['datum'];

        return $this->json($data);
    }
}
