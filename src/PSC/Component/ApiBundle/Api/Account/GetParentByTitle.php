<?php

namespace PSC\Component\ApiBundle\Api\Account;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Account\GetParentByTitle\Input;
use PSC\Component\ApiBundle\Dto\Account\GetParentByTitle\Output;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class GetParentByTitle extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private \PSC\System\SettingsBundle\Service\Shop $shopService;

    public function __construct(EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService)
    {
        $this->entityManager = $entityManager;
        $this->shopService = $shopService;
    }

    /**
     * Check account parents by title
     *
     * @Route("/account/getparentbytitle", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Component\ApiBundle\Dto\Account\GetParentByTitle\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="account",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Account\GetParentByTitle\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Component\ApiBundle\Dto\Account\GetParentByTitle\Input::class))
     * )
     * @OA\Tag(name="Account")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function existsAction(Input $data): JsonResponse
    {
        $output = new Output();
        $account = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Account')
            ->getAccountByTitle(
                $data->title
            );



        if (!$account) {
            $output->parent = "";
            $output->parent_id = 0;
            return $this->json($output);
        }

        if ($account->getParent()) {
            $account = $this->recursiveParent($account);
        }

        $output->parent = $account->getTitle();
        $output->parent_id = $account->getId();

        return $this->json($output);
    }

    /**
     * Gets the Parent Account
     *
     * @param \PSC\Shop\EntityBundle\Entity\Account $account
     *
     * @return \PSC\Shop\EntityBundle\Entity\Account
     */
    protected function recursiveParent(\PSC\Shop\EntityBundle\Entity\Account $account)
    {
        if ($account->getParent()) {
            $account = $this->recursiveParent($account->getParent());
        }

        return $account;
    }
}
