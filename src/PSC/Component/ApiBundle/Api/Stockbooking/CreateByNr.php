<?php

namespace PSC\Component\ApiBundle\Api\Stockbooking;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Error\PersistFailure;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Dto\Stockbooking\CreateByNr\Input;
use PSC\Component\ApiBundle\Dto\Stockbooking\CreateByNr\Output;
use PSC\Component\ApiBundle\Model\Contact;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use PSC\Component\ApiBundle\Model\Stockbooking;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class CreateByNr extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private \PSC\Component\ApiBundle\Hydrate\Shop $hydrateShop;

    public function __construct(EntityManagerInterface $entityManager, \PSC\Component\ApiBundle\Hydrate\Shop $hydrateShop)
    {
        $this->entityManager = $entityManager;
        $this->hydrateShop = $hydrateShop;
    }

    /**
     * create by nr
     *
     * @Route("/stockbooking/createbynr", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Component\ApiBundle\Dto\Stockbooking\CreateByNr\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="stockbooking",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Stockbooking\CreateByNr\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Component\ApiBundle\Dto\Stockbooking\CreateByNr\Input::class))
     * )
     * @OA\Tag(name="Stockbooking")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function create(Input $data): JsonResponse
    {
        $product = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->createQueryBuilder('product')
            ->andWhere('product.nrIntern = :nr or product.nrExtern = :nr')
            ->setParameter('nr', $data->product_nr)
            ->getQuery()->getOneOrNullResult();

        if ($product) {
            $contact = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')->findOneBy(['uid' => $data->contact_id]);

            if (!$contact) {
                return $this->json(new NotFound("Contact not found"));
            }

            $stBooking = new \PSC\Shop\EntityBundle\Entity\Stockbooking($product);
            $stBooking->setProduct($product);
            $stBooking->setDescription($data->description);
            $stBooking->setAmount($data->amount);
            $stBooking->setDelivery($data->delivery_nr);
            $stBooking->setContact($contact);

            $product->setStockCount($product->getStockCount() + $data->amount);

            try {
                $this->entityManager->persist($stBooking);
                $this->entityManager->persist($product);

                $this->entityManager->flush();
            } catch (\Exception $e) {
                return $this->json(new PersistFailure());
            }

            return $this->json(new Output());
        }

        return $this->json(new NotFound("Product not found"));
    }
}
