<?php

namespace PSC\Component\ApiBundle\Api\Stockbooking;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Dto\Stockbooking\GetByNr\Input;
use PSC\Component\ApiBundle\Dto\Stockbooking\GetByNr\Output;
use PSC\Component\ApiBundle\Model\Contact;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use PSC\Component\ApiBundle\Model\Stockbooking;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class GetByNr extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private \PSC\Component\ApiBundle\Hydrate\Shop $hydrateShop;

    public function __construct(EntityManagerInterface $entityManager, \PSC\Component\ApiBundle\Hydrate\Shop $hydrateShop)
    {
        $this->entityManager = $entityManager;
        $this->hydrateShop = $hydrateShop;
    }

    /**
     * get all by nr
     *
     * @Route("/stockbooking/getallbynr", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Component\ApiBundle\Dto\Stockbooking\GetByNr\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="account",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Stockbooking\GetByNr\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Component\ApiBundle\Dto\Stockbooking\GetByNr\Input::class))
     * )
     * @OA\Tag(name="Stockbooking")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function allAction(Input $data): JsonResponse
    {
        $product = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->createQueryBuilder('product')
            ->andWhere('product.nrIntern = :nr or product.nrExtern = :nr')
            ->setParameter('nr', $data->nr)
            ->getQuery()->getOneOrNullResult();

        if ($product) {
            $bookings = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Stockbooking')
                ->findByProduct($product);

            $bookingsTemp = array();
            /** @var \PSC\Shop\EntityBundle\Entity\Stockbooking $booking */
            foreach ($bookings as $booking) {
                $contact = new Contact();
                $contact->id = $booking->getContact()->getUid();
                $contact->username = $booking->getContact()->getUsername();

                $book = new Stockbooking();
                $book->id = $booking->getUID();
                $book->amount = $booking->getAmount();
                $book->contact = $contact;
                $book->created = $booking->getCreated();
                $book->description = $booking->getDescription();
                $book->deliveryNumber = $booking->getDelivery();

                $bookingsTemp[] = $book;
            }

            return $this->json(new Output($bookingsTemp));
        }

        return $this->json(new NotFound("Product not found"));
    }
}
