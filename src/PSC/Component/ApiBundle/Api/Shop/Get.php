<?php

namespace PSC\Component\ApiBundle\Api\Shop;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class Get extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private \PSC\Component\ApiBundle\Hydrate\Shop $hydrateShop;

    public function __construct(EntityManagerInterface $entityManager, \PSC\Component\ApiBundle\Hydrate\Shop $hydrateShop)
    {
        $this->entityManager = $entityManager;
        $this->hydrateShop = $hydrateShop;
    }

    /**
     * Shops
     *
     * @Route("/shops", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="shops",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Shop\Shops\Output::class))
     * )
     * @OA\Tag(name="Shops")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="ApiKeyAuth")
     * @Security(name="Bearer")
     */
    public function allAction(): JsonResponse
    {
        $shops = $this->entityManager->getRepository(\PSC\Shop\EntityBundle\Entity\Shop::class)->findAll();

        $data = [];
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $shop */
        foreach ($shops as $shop) {
            $data[] = $this->hydrateShop->hydrateToModel($shop);
        }

        return $this->json(new Shops\Output($data));
    }

    /**
     * Get One Shop
     *
     * @Route("/shops/{id}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="found shop",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Model\Shop::class))
     * )
     * @OA\Response(
     *     response=404,
     *     description="not found",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Error\NotFound::class))
     * )
     * @OA\Tag(name="Shops")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function one($id): JsonResponse
    {
        $shop = $this->entityManager->getRepository(\PSC\Shop\EntityBundle\Entity\Shop::class)->findOneBy(['uid' => $id]);

        if ($shop) {
            return $this->json($this->hydrateShop->hydrateToModel($shop));
        }

        return $this->json(new NotFound());
    }
}
