<?php

namespace PSC\Component\ApiBundle\Api\Plugin;

use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\ObjectId;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use PSC\System\PluginBundle\Interfaces\Plugin;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class Get extends AbstractController
{
    private DocumentManager $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }

     /**
     * Layouter Plugins
     *
     * @Route("/plugins/layouter", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="shops",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Plugin\Plugins\Output::class))
     * )
     * @OA\Tag(name="Plugin")
     */
    public function layouterAction(): JsonResponse
    {
        $plugins = $this->documentManager->getRepository(\PSC\System\PluginBundle\Document\Plugin::class)->findAll();

        $data = [];
        /** @var \PSC\System\PluginBundle\Document\Plugin $plugin */
        foreach ($plugins as $plugin) {
            $name = $plugin->getNamespace();
            $plugObj = new $name();
            if ($plugObj->getType() === Plugin::Layouter) {
                $tmp = new \PSC\Component\ApiBundle\Model\Plugin($plugin->getId(), $plugin->getName());
                $tmp->installed = $plugin->getInstalled();
                if ($tmp->installed) {
                    $data[] = $tmp;
                }
            }
        }

        return $this->json(new Shops\Output($data));
    }

   /**
     * Plugins
     *
     * @Route("/plugins", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="shops",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Plugin\Plugins\Output::class))
     * )
     * @OA\Tag(name="Plugin")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function allAction(): JsonResponse
    {
        $plugins = $this->documentManager->getRepository(\PSC\System\PluginBundle\Document\Plugin::class)->findAll();

        $data = [];
        /** @var \PSC\System\PluginBundle\Document\Plugin $plugin */
        foreach ($plugins as $plugin) {
            $tmp = new \PSC\Component\ApiBundle\Model\Plugin($plugin->getId(), $plugin->getName());
            $tmp->installed = $plugin->getInstalled();

            $data[] = $tmp;
        }

        return $this->json(new Shops\Output($data));
    }

    /**
     * Get one plugin
     *
     * @Route("/plugins/{uuid}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="found plugin",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Model\Plugin::class))
     * )
     * @OA\Response(
     *     response=404,
     *     description="not found",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Component\ApiBundle\Dto\Error\NotFound::class))
     * )
     * @OA\Tag(name="Plugin")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function one($uuid): JsonResponse
    {
        $plugin = $this->documentManager->getRepository(\PSC\System\PluginBundle\Document\Plugin::class)->findOneBy(['id' => new ObjectId($uuid)]);
        if (!$plugin) {
            return $this->json(new NotFound());
        }
        $tmp = new \PSC\Component\ApiBundle\Model\Plugin($plugin->getId(), $plugin->getName());
        $tmp->installed = $plugin->getInstalled();

        return $this->json($tmp);
    }
}
