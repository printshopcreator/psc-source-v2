<?php

namespace PSC\Component\ApiBundle\Dto\Plugin\Plugins;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Model\Plugin;

class Output
{
    public function __construct(array $plugins)
    {
        $this->data = $plugins;
        $this->count = count($plugins);
    }

    /**
     * @var Plugin[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Component\ApiBundle\Model\Plugin::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
