<?php

namespace PSC\Component\ApiBundle\Dto\Error;

use PSC\Component\ApiBundle\Model\Shop;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class NotFound
{
    public function __construct(string $message = "Object not found")
    {
        $this->message = $message;
    }

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $message = "";
}
