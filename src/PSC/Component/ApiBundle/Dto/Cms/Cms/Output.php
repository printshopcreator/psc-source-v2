<?php

namespace PSC\Component\ApiBundle\Dto\Cms\Cms;

use PSC\Component\ApiBundle\Model\Cms;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    public function __construct(array $cms)
    {
        $this->data = $cms;
        $this->count = count($cms);
    }

    /**
     * @var Cms[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Component\ApiBundle\Model\Cms::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
