<?php

namespace PSC\Component\ApiBundle\Dto\Account\GetParentByTitle;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class Output
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $parent;

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $parent_id;
}
