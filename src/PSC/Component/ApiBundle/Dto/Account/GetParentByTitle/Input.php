<?php

namespace PSC\Component\ApiBundle\Dto\Account\GetParentByTitle;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

final class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $title;
}
