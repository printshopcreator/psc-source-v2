<?php

namespace PSC\Component\ApiBundle\Dto\Stockbooking\GetByNr;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $nr = "";
}
