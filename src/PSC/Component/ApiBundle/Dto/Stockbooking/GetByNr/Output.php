<?php

namespace PSC\Component\ApiBundle\Dto\Stockbooking\GetByNr;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Model\Stockbooking;

class Output
{
    public function __construct(array $shops)
    {
        $this->data = $shops;
        $this->count = count($shops);
    }

    /**
     * @var Stockbooking[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Component\ApiBundle\Model\Stockbooking::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
