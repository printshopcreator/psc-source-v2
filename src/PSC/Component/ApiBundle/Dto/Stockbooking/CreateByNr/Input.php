<?php

namespace PSC\Component\ApiBundle\Dto\Stockbooking\CreateByNr;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $product_nr = "";

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $description = "";

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $amount = 0;

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $delivery_nr = "";

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $contact_id = 0;
}
