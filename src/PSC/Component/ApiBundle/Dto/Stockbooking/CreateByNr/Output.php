<?php

namespace PSC\Component\ApiBundle\Dto\Stockbooking\CreateByNr;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Model\Stockbooking;

class Output
{
    /**
     * @var bool
     *
     * @OA\Property(type="boolean")
     */
    public bool $success = true;
}
