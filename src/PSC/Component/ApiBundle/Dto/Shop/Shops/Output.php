<?php

namespace PSC\Component\ApiBundle\Dto\Shop\Shops;

use PSC\Component\ApiBundle\Model\Shop;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    public function __construct(array $shops)
    {
        $this->data = $shops;
        $this->count = count($shops);
    }

    /**
     * @var Shop[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Component\ApiBundle\Model\Shop::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
