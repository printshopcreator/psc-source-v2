<?php

namespace PSC\Component\SteplayouterBundle\Command;

use GuzzleHttp\Client;
use PSC\Component\SteplayouterBundle\Service\Pdf;
use PSC\Shop\EntityBundle\Document\Job;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Registry;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use PSC\System\PluginBundle\Event\ClearCache;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;

class PrintPdfCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('component:steplayouter:pdf:print')
            ->setDescription('Create Print PDF')
            ->addArgument(
                'outputDir',
                InputArgument::REQUIRED,
                'Layouter Id'
            )
            ->addArgument(
                'layouterUid',
                InputArgument::REQUIRED,
                'Layouter Id'
            )
            ->addArgument(
                'aliasUid',
                InputArgument::REQUIRED,
                'Alias Id'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $layouter_uuid = $input->getArgument('layouterUid');
        $outfilename = $input->getArgument('outputDir');
        $aliasUid = $input->getArgument('aliasUid');

        /** @var Pdf $pdfService */
        $pdfService = $this->getContainer()->get('psc.component.steplayouter.pdf');

        $pdfService->create($outfilename, $layouter_uuid, $aliasUid);
    }
}
