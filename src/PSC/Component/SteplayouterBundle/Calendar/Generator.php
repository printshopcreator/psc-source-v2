<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Calendar;

/**
 * CalendarController fürs SteplayouterBundle
 *
 * @package    PSC\Component\Steplayouter
 * @subpackage Controller
 */
class Generator
{
    protected $_type = null;
    public function __construct()
    {
    }

    public function setType(Type $type)
    {
        $this->_type = $type;
    }

    public function render()
    {
        return $this->_type->render();
    }
}
