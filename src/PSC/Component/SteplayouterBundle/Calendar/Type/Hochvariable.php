<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Calendar\Type;

use PSC\Component\SteplayouterBundle\Calendar\Type;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class Hochvariable extends Type
{
    protected $_id = 7;

    protected function _render()
    {

        $font = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");
        $fontBold = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");
        $startDate = new \DateTime($this->getDesign()->getStartYear() . '-' . $this->getDesign()->getStartMonth() . '-01 00:00:10', new \DateTimeZone('Europe/Berlin'));
        $this->getDesign()->setRenderMonth(($this->getDesign()->getRenderMonth() - 1));
        if ($this->getDesign()->getRenderMonth() > 0) {
            $startDate->modify("+" . $this->getDesign()->getRenderMonth() . " month");
        }
        $maxdays = \cal_days_in_month(CAL_GREGORIAN, $startDate->format("m"), $startDate->format("Y"));
        $tf = 0;
        $tbl = 0;
        $capheight = 15;
        $colWidth = round($this->getDesign()->getCalendarWidth() / 2 / (round($maxdays / 7)));
        setlocale(LC_ALL, 'de_DE.UTF8', 'de_DE@euro', 'de_DE', 'de', 'ge');
        $opts_month = "font=" . $fontBold . " fillcolor={" . $this->getDesign()->getMonthColor() . "} position={top left} fontsize=" . ($this->getDesign()->getMonthSize() ? $this->getDesign()->getMonthSize() : 60);
        $this->_pdf->fit_textline(strftime("%B", strtotime($startDate->format("Y-m-d"))), $this->getDesign()->getMonthX(), $this->getDesign()->getMonthY(), $opts_month);
        $row = 1;
        $color = " fillcolor={" . $this->getDesign()->getDayColor() . "}";
        $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 28) . " position={center center}} " .
            " colwidth=" . $colWidth . " margin=2";
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "Mo", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "Di", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "Mi", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "Do", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "Fr", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "Sa", $tlcell_opts);
        $color = " fillcolor={" . $this->getDesign()->getSunColor() . "}";
        $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 28) . " position={center center}} " .
            " colwidth=" . $colWidth . " margin=2";
        $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "So", $tlcell_opts);
        $row++;
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $row++;
        $column = 1;
        $dayoftheweek = $startDate->format("w");
        if ($dayoftheweek == 0) {
            $dayoftheweek = 7;
        }
        $lastrow = 1;
        $so = array();
        for ($i = 1; $i <= ($maxdays + $dayoftheweek); $i++) {
            if ($row == 3 && $i == 1) {
                $column = $dayoftheweek;
            }


            $color = " fillcolor={" . $this->getDesign()->getDayColor() . "}";
            if ($startDate->format("D") == "Sun") {
                $so[] = $i;
                $color = " fillcolor={" . $this->getDesign()->getSunColor() . "}";
            }

            $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 28) . " position={center top}} " .
                " colwidth=" . $colWidth . " rowheight=" . ($this->getDesign()->getDaySize() ? ($this->getDesign()->getDaySize()) : 28) . "" .
                " margin=" . $this->getDesign()->getDisplayMarginCal();
/* Add the text line cell */

            if ($maxdays >= $i) {
                $tbl = $this->_pdf->add_table_cell($tbl, $column, $row, $i, $tlcell_opts);
                $lastrow = $row;
            }
            $column++;
            $startDate->modify("+1day");
            if ($column >= 8) {
                $column = 1;
                $row++;
            }
        }


        $fittab_opts = "header=0";
        $result = $this->_pdf->fit_table($tbl, $this->getDesign()->getCalendarX(), $this->getDesign()->getCalendarY(), $this->getDesign()->getCalendarWidth() + $this->getDesign()->getCalendarX(), $this->getDesign()->getCalendarHeight() + $this->getDesign()->getCalendarY(), $fittab_opts);
        if ($this->getDesign()->getDisplayYear()) {
            $opts_year = "font=" . $fontBold . " fillcolor={" . $this->getDesign()->getMonthColor() . "} position={top left}  fontsize=" . ($this->getDesign()->getYearSize() ? $this->getDesign()->getYearSize() : 60);
            $this->_pdf->fit_textline($this->getDesign()->getStartYear(), $this->getDesign()->getYearX(), $this->getDesign()->getYearY(), $opts_year);
        }
    }
}
