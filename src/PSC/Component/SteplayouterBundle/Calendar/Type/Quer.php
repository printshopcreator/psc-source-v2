<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Calendar\Type;

use PSC\Component\SteplayouterBundle\Calendar\Type;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * DefaultController fürs ProductionBundle
 *
 * @package    PSC\Backend\Production
 * @subpackage Controller
 */
class Quer extends Type
{
    protected $_id = 7;

    protected function _render()
    {

        $font = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");
        $fontBold = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");
        $startDate = new \DateTime($this->getDesign()->getStartYear() . '-' . $this->getDesign()->getStartMonth() . '-01', new \DateTimeZone('Europe/Berlin'));
        $this->getDesign()->setRenderMonth(($this->getDesign()->getRenderMonth() - 1));
        if ($this->getDesign()->getRenderMonth() > 0) {
            $startDate->modify("+" . $this->getDesign()->getRenderMonth() . " month");
        }
        $maxdays = \cal_days_in_month(CAL_GREGORIAN, $startDate->format("m"), $startDate->format("Y"));
        $row = 1;
        $tf = 0;
        $tbl = 0;
        $capheight = 15;
        $colWidth = round($this->getDesign()->getWidth() / $maxdays);
        $tlcell_opts = "fittextline={font=" . $fontBold . " fillcolor={" . $this->getDesign()->getMonthColor() . "} fontsize=" . ($this->getDesign()->getMonthSize() ? $this->getDesign()->getMonthSize() : 21) . " position={left center}} " .
            " colwidth=" . round($this->getDesign()->getWidth() / 2) . " rowheight=20 colspan=6" .
            " margin=6 ";
/* Add the text line cell */
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, strftime("%B", $startDate->getTimestamp()), $tlcell_opts);
        $row++;
        $so = array();

        for ($i = 1; $i <= $maxdays; $i++) {
            $color = " fillcolor={" . $this->getDesign()->getDayColor() . "}";
            if (strftime("%a", $startDate->getTimestamp()) == "So") {
                $so[] = $i;
                $color = " fillcolor={" . $this->getDesign()->getSunColor() . "}";
            }
            /* Prepare the option list for adding the text line cell */
            $tlcell_opts = "fittextline={font=" . $fontBold . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 21) . " position={center center}} " .
                " colwidth=" . $colWidth . " rowheight=20" .
                " margin=4";
/* Add the text line cell */
            $tbl = $this->_pdf->add_table_cell($tbl, $i, $row, strftime("%a", $startDate->getTimestamp()), $tlcell_opts);
            if ($tbl == 0) {
                throw new \Exception("Error adding cell: " . $this->_pdf->get_errmsg());
            }

            $startDate->modify("+1day");
        }

        $row++;



        for ($i = 1; $i <= $maxdays; $i++) {
/* Prepare the option list for adding the text line cell */
            $color = " fillcolor={" . $this->getDesign()->getDayColor() . "}";
            if (in_array($i, $so)) {
                $color = " fillcolor={" . $this->getDesign()->getSunColor() . "}";
            }

            $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 21) . " position={center center}} " .
                " colwidth=" . $colWidth . " rowheight=20" .
                " margin=4";
/* Add the text line cell */
            $tbl = $this->_pdf->add_table_cell($tbl, $i, $row, $i, $tlcell_opts);
            if ($tbl == 0) {
                throw new Exception("Error adding cell: " . $this->_pdf->get_errmsg());
            }
        }

        $fittab_opts = "header=0";
        $result = $this->_pdf->fit_table($tbl, $this->getDesign()->getX(), $this->getDesign()->getY(), $this->getDesign()->getWidth() + $this->getDesign()->getX(), $this->getDesign()->getHeight() + $this->getDesign()->getY(), $fittab_opts);
    }
}
