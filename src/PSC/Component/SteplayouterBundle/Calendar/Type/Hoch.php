<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Calendar\Type;

use PSC\Component\SteplayouterBundle\Calendar\Type;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * DefaultController fürs ProductionBundle
 *
 * @package    PSC\Backend\Production
 * @subpackage Controller
 */
class Hoch extends Type
{
    protected $_id = 7;

    protected function _render()
    {

        $font = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");
        $fontBold = $this->_pdf->load_font($this->getDesign()->getFontFamily(), "unicode", "embedding fontstyle=normal");
        $startDate = new \DateTime($this->getDesign()->getStartYear() . '-' . $this->getDesign()->getStartMonth() . '-01 00:00:10', new \DateTimeZone('Europe/Berlin'));
        $this->getDesign()->setRenderMonth(($this->getDesign()->getRenderMonth() - 1));
        if ($this->getDesign()->getRenderMonth() > 0) {
            $startDate->modify("+" . $this->getDesign()->getRenderMonth() . " month");
        }
        $maxdays = \cal_days_in_month(CAL_GREGORIAN, $startDate->format("m"), $startDate->format("Y"));
        $row = 1;
        $tf = 0;
        $tbl = 0;
        $capheight = 15;
        if ($this->getDesign()->getDisplayHeightMounthCal() == 0) {
            $this->getDesign()->setDisplayHeightMounthCal(20);
        }
        if ($this->getDesign()->getDisplayMarginCal() == 0) {
            $this->getDesign()->setDisplayMarginCal(7);
        }

        $colWidth = round($this->getDesign()->getWidth() / 2 / (round($maxdays / 7)));
        $tlcell_opts_month = "fittextline={font=" . $fontBold . " fillcolor={" . $this->getDesign()->getMonthColor() . "} fontsize=" . ($this->getDesign()->getMonthSize() ? $this->getDesign()->getMonthSize() : 60) . " position={left center}} " .
            " colwidth=" . round($this->getDesign()->getWidth() / 2) . " rowheight=20" .
            " margin=2";
/* Add the text line cell */
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, strftime("%B", strtotime($startDate->format("Y-m-d"))), $tlcell_opts_month);
        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth * 3);
        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 8, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 9, $row, "", "rowheight=20 margin=2 colwidth=" . $colWidth);
        $row++;
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . round($this->getDesign()->getWidth() / 2));
        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth * 3);
        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 8, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 9, $row, "", " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth);
        $row++;
        $color = " fillcolor={" . $this->getDesign()->getDayColor() . "}";
        $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 28) . " position={center center}} " .
            " colwidth=" . $colWidth . " rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2";
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", "rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . round($this->getDesign()->getWidth() / 2));
        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "", "rowheight=" . $this->getDesign()->getDisplayHeightMounthCal() . " margin=2 colwidth=" . $colWidth * 3);
        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "Mo", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "Di", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "Mi", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "Do", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "Fr", $tlcell_opts);
        $tbl = $this->_pdf->add_table_cell($tbl, 8, $row, "Sa", $tlcell_opts);
        $color = " fillcolor={" . $this->getDesign()->getSunColor() . "}";
        $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 28) . " position={center center}} " .
            " colwidth=" . $colWidth . " rowheight=20 margin=2";
        $tbl = $this->_pdf->add_table_cell($tbl, 9, $row, "So", $tlcell_opts);
        $row++;
        $tbl = $this->_pdf->add_table_cell($tbl, 1, $row, "", " rowheight=5 margin=2 colwidth=" . round($this->getDesign()->getWidth() / 2));
        $tbl = $this->_pdf->add_table_cell($tbl, 2, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth * 3);
        $tbl = $this->_pdf->add_table_cell($tbl, 3, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 4, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 5, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 6, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 7, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 8, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $tbl = $this->_pdf->add_table_cell($tbl, 9, $row, "", " rowheight=5 margin=2 colwidth=" . $colWidth);
        $row++;
        $column = 3;
        $dayoftheweek = $startDate->format("w");
        if ($dayoftheweek == 0) {
            $dayoftheweek = 7;
        }
        $lastrow = 1;
        $so = array();
        for ($i = 1; $i <= ($maxdays + $dayoftheweek + 2); $i++) {
            if ($row == 5 && $i == 1) {
                $column = $dayoftheweek + 2;
            }


            $color = " fillcolor={" . $this->getDesign()->getDayColor() . "}";
            if ($startDate->format("D") == "Sun") {
                $so[] = $i;
                $color = " fillcolor={" . $this->getDesign()->getSunColor() . "}";
            }

            $tlcell_opts = "fittextline={font=" . $font . $color . " fontsize=" . ($this->getDesign()->getDaySize() ? $this->getDesign()->getDaySize() : 28) . " position={center top}} " .
                " colwidth=" . $colWidth . " rowheight=" . ($this->getDesign()->getDaySize() ? ($this->getDesign()->getDaySize()) : 28) . "" .
                " margin=" . $this->getDesign()->getDisplayMarginCal();
/* Add the text line cell */

            if ($maxdays >= $i) {
                $tbl = $this->_pdf->add_table_cell($tbl, $column, $row, $i, $tlcell_opts);
                $lastrow = $row;
            }
            $column++;
            $startDate->modify("+1day");
            if ($column >= 10) {
                $column = 3;
                $row++;
            }
        }

        if ($this->getDesign()->getDisplayYear()) {
            $lastrow++;
            $tbl = $this->_pdf->add_table_cell($tbl, 1, $lastrow, $this->getDesign()->getStartYear(), $tlcell_opts_month);
        }

        $fittab_opts = "header=0";
        $result = $this->_pdf->fit_table($tbl, $this->getDesign()->getX(), $this->getDesign()->getY(), $this->getDesign()->getWidth() + $this->getDesign()->getX(), $this->getDesign()->getHeight() + $this->getDesign()->getY(), $fittab_opts);
    }
}
