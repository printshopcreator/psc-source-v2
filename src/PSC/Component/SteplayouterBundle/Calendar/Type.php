<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Calendar;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CalendarController fürs SteplayouterBundle
 *
 * @package    PSC\Component\Steplayouter
 * @subpackage Controller
 */
class Type
{
    protected $_pdf = null;
    protected $_design = null;
    protected $_outFilename = null;
    protected $_container = null;
    public function __construct(ContainerInterface $container)
    {
        $this->_container = $container;
    }

    /**
     * @return null
     */
    public function getDesign()
    {
        return $this->_design;
    }

    /**
     * @param null $design
     */
    public function setDesign($design)
    {
        $this->_design = $design;
    }

    public function render()
    {

        $this->_outFilename = '/tmp/cal_' . $this->getDesign()->getHash() . ".pdf";
        if (!file_exists($this->_outFilename)) {
            $this->_pdf = new \pdflib();
            $this->_pdf->set_option("errorpolicy=return");
            $this->_pdf->set_option("stringformat=utf8");
        //            if($this->_container->hasParameter('pdflib_lic') && $this->_container->getParameter('pdflib_lic') != "") {
        //                $this->_pdf->set_option("license=".$this->_container->getParameter('pdflib_lic'));
        //            }
            $this->_pdf->set_option("SearchPath=" . $this->_container->get('kernel')->getRootDir() . "/Resources/fonts");
            if ($this->_pdf->begin_document($this->_outFilename, "") == 0) {
                die("Error: " . $this->_pdf->get_errmsg());
            }

            $this->_pdf->set_info("Creator", "PDFlib starter sample");
            $this->_pdf->set_info("Title", "starter_path");
            $this->_pdf->set_option("topdown=true");
            $this->_pdf->begin_page_ext($this->getDesign()->getWidth(), $this->getDesign()->getHeight(), "");
            $this->_render();
            $this->_pdf->end_page_ext("");
            $this->_pdf->end_document("");
        }

        return $this->_outFilename;
    }

    protected function hex2rgb($hex)
    {
        $color = array();
        $color['r'] = hexdec(substr($hex, 1, 2)) / 255;
        $color['g'] = hexdec(substr($hex, 3, 2)) / 255;
        $color['b'] = hexdec(substr($hex, 5, 2)) / 255;
        return $color;
    }
}
