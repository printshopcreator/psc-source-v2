<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Calendar;

/**
 * CalendarController fürs SteplayouterBundle
 *
 * @package    PSC\Component\Steplayouter
 * @subpackage Controller
 */
class Design
{
    protected $_x = 0;
    protected $_y = 0;
    protected $_width = 900;
    protected $_height = 80;
    protected $_font_family = "Neo Sans";
    protected $_start_year = 2014;
    protected $_start_month = 1;
    protected $_render_month = 1;
    protected $_month_color = "#000000";
    protected $_day_color = "#000000";
    protected $_sun_color = "#000000";
    protected $_month_size = 14;
    protected $_day_size = 10;
    protected $_display_year = false;
    protected $_display_height_mounth_cal = 15;
    protected $_display_margin_cal = 6;
    protected $_calendar_x = 0;
    protected $_month_x = 0;
    protected $_month_y = 0;
    protected $_calendar_y = 0;
    protected $_calendar_width = 0;
    protected $_calendar_height = 0;
    protected $_year_x = 0;
    protected $_year_y = 0;
    protected $_year_size = 14;

    public function getHash()
    {
        return md5($this->_x .
            $this->_y .
            $this->_width .
            $this->_height .
            $this->_font_family .
            $this->_start_year .
            $this->_start_month .
            $this->_render_month .
            $this->_day_color .
            $this->_day_size .
            $this->_month_color .
            $this->_month_size .
            $this->_sun_color .
            $this->_calendar_x .
            $this->_calendar_y .
            $this->_year_x .
            $this->_year_y .
            $this->_calendar_width .
            $this->_calendar_height .
            $this->_month_size);
    }

    public function __construct()
    {
        $this->_start_year = date("Y");
    }
    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->_width;
    }

    /**
     * @param int $_width
     */
    public function setWidth($_width = 900)
    {
        $this->_width = $_width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->_height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->_height = $height;
    }

    /**
     * @return string
     */
    public function getFontFamily()
    {
        return $this->_font_family;
    }

    /**
     * @param string $font_family
     */
    public function setFontFamily($font_family)
    {
        $this->_font_family = $font_family;
    }

    /**
     * @return string
     */
    public function getStartMonth()
    {
        return $this->_start_month;
    }

    /**
     * @param string $start_month
     */
    public function setStartMonth($start_month)
    {
        $this->_start_month = $start_month;
    }

    /**
     * @return bool|int|string
     */
    public function getStartYear()
    {
        return $this->_start_year;
    }

    /**
     * @param bool|int|string $start_year
     */
    public function setStartYear($start_year)
    {
        $this->_start_year = $start_year;
    }

    /**
     * @return int
     */
    public function getRenderMonth()
    {
        return $this->_render_month;
    }

    /**
     * @param int $render_month
     */
    public function setRenderMonth($render_month)
    {
        $this->_render_month = $render_month;
    }

    /**
     * @return string
     */
    public function getMonthColor()
    {
        return $this->_month_color;
    }

    /**
     * @param string $month_color
     */
    public function setMonthColor($month_color)
    {
        $this->_month_color = $month_color;
    }

    /**
     * @return string
     */
    public function getDayColor()
    {
        return $this->_day_color;
    }

    /**
     * @param string $day_color
     */
    public function setDayColor($day_color)
    {
        $this->_day_color = $day_color;
    }

    /**
     * @return int
     */
    public function getMonthSize()
    {
        return $this->_month_size;
    }

    /**
     * @param int $month_size
     */
    public function setMonthSize($month_size)
    {
        $this->_month_size = $month_size;
    }

    /**
     * @return int
     */
    public function getDaySize()
    {
        return $this->_day_size;
    }

    /**
     * @param int $day_size
     */
    public function setDaySize($day_size)
    {
        $this->_day_size = $day_size;
    }

    /**
     * @return int
     */
    public function getX()
    {
        return $this->_x;
    }

    /**
     * @param int $x
     */
    public function setX($x)
    {
        $this->_x = $x;
    }

    /**
     * @return int
     */
    public function getY()
    {
        return $this->_y;
    }

    /**
     * @param int $y
     */
    public function setY($y)
    {
        $this->_y = $y;
    }

    /**
     * @return string
     */
    public function getSunColor()
    {
        return $this->_sun_color;
    }

    /**
     * @param string $sun_color
     */
    public function setSunColor($sun_color)
    {
        $this->_sun_color = $sun_color;
    }

    /**
     * @return int
     */
    public function getDisplayYear()
    {
        return $this->_display_year;
    }

    /**
     * @param int $display_year
     */
    public function setDisplayYear($display_year)
    {
        $this->_display_year = $display_year;
    }

    /**
     * @return int
     */
    public function getDisplayHeightMounthCal()
    {
        return $this->_display_height_mounth_cal;
    }

    /**
     * @param int $display_height_mounth_cal
     */
    public function setDisplayHeightMounthCal($display_height_mounth_cal)
    {
        $this->_display_height_mounth_cal = $display_height_mounth_cal;
    }

    /**
     * @return int
     */
    public function getDisplayMarginCal()
    {
        return $this->_display_margin_cal;
    }

    /**
     * @param int $display_margin_cal
     */
    public function setDisplayMarginCal($display_margin_cal)
    {
        $this->_display_margin_cal = $display_margin_cal;
    }

    /**
     * @return int
     */
    public function getYearX()
    {
        return $this->_year_x;
    }

    /**
     * @param int $year_x
     */
    public function setYearX($year_x)
    {
        $this->_year_x = $year_x;
    }

    /**
     * @return int
     */
    public function getCalendarY()
    {
        return $this->_calendar_y;
    }

    /**
     * @param int $calendar_y
     */
    public function setCalendarY($calendar_y)
    {
        $this->_calendar_y = $calendar_y;
    }

    /**
     * @return int
     */
    public function getCalendarX()
    {
        return $this->_calendar_x;
    }

    /**
     * @param int $calendar_x
     */
    public function setCalendarX($calendar_x)
    {
        $this->_calendar_x = $calendar_x;
    }

    /**
     * @return int
     */
    public function getYearY()
    {
        return $this->_year_y;
    }

    /**
     * @param int $year_y
     */
    public function setYearY($year_y)
    {
        $this->_year_y = $year_y;
    }

    /**
     * @return int
     */
    public function getCalendarWidth()
    {
        return $this->_calendar_width;
    }

    /**
     * @param int $calendar_width
     */
    public function setCalendarWidth($calendar_width)
    {
        $this->_calendar_width = $calendar_width;
    }

    /**
     * @return int
     */
    public function getCalendarHeight()
    {
        return $this->_calendar_height;
    }

    /**
     * @param int $calendar_height
     */
    public function setCalendarHeight($calendar_height)
    {
        $this->_calendar_height = $calendar_height;
    }

    /**
     * @return int
     */
    public function getMonthY()
    {
        return $this->_month_y;
    }

    /**
     * @param int $month_y
     */
    public function setMonthY($month_y)
    {
        $this->_month_y = $month_y;
    }

    /**
     * @return int
     */
    public function getMonthX()
    {
        return $this->_month_x;
    }

    /**
     * @param int $month_x
     */
    public function setMonthX($month_x)
    {
        $this->_month_x = $month_x;
    }

    /**
     * @return int
     */
    public function getYearSize()
    {
        return $this->_year_size;
    }

    /**
     * @param int $year_size
     */
    public function setYearSize($year_size)
    {
        $this->_year_size = $year_size;
    }
}
