<?php

namespace PSC\Component\SteplayouterBundle\Queue;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use GuzzleHttp\Client;
use LogicException;
use PSC\Component\SteplayouterBundle\Service\Pdf;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactRole;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Role;
use PSC\Shop\EntityBundle\Entity\ShopContact;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\EntityBundle\Entity\ProductProductgroup;
use PSC\Shop\EntityBundle\Repository\ProductProductgroupRepository;
use PSC\Shop\OrderBundle\Model\Order\Address;
use PSC\Shop\OrderBundle\Model\Order\Payment;
use PSC\Shop\OrderBundle\Model\Order\Position;
use PSC\Shop\OrderBundle\Model\Order\Shipping;
use PSC\Shop\OrderBundle\Service\Order;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use PSC\System\SettingsBundle\Service\Shop;
use Ramsey\Uuid\Uuid;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

require_once(__DIR__ . '/../../../Shop/EntityBundle/Lagacy/TP_Basket_Item.php');

class Render implements QueueInterface, ConfigurableElementInterface
{
    private $_entityManager = null;

    /**
     * @var Shop
     */
    private $_shopService;
    /**
     * @var Pdf
     */
    private $_pdf;


    function __construct(EntityManagerInterface $entityManager, Pdf $pdf)
    {
        $this->_entityManager = $entityManager;
        $this->_pdf = $pdf;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'psc_component_steplayouter_queue_render';
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return 'Layouter';
    }

    public function getDescription()
    {
        return 'Steplayouter Renderer';
    }

    public function getName()
    {
        return 'Steplayouter Renderer';
    }

    public function getForm(FormBuilderInterface $builder, $form_options, EventInterface $event)
    {
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
    }

    public function getTemplate()
    {
    }

    /**
     * @param EventInterface $event
     * @param Queue $urlDoc
     * @return bool
     */
    public function execute(EventInterface $event, Queue $doc)
    {

        try {
            if (false && file_exists("/tmp/renderSteplayouter.txt")) {
                return true;
            }

            file_put_contents("/tmp/renderSteplayouter.txt", "import");

            set_time_limit(0);

            $orderspos = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
                        ->findby(['layouterMode' => 4, 'renderPrint' => 1]);

            /** @var Orderpos $pos */
            foreach ($orderspos as $pos) {
                /** @var \TP_Basket_Item $objPosition */
                $objPosition = unserialize(($pos->getData()));
                if ($objPosition->getLayouterId() != "") {
                    if (!file_exists('/data/www/old/market/steplayouter/basket/' . $pos->getOrder()->getUid() . '/' . $pos->getPos())) {
                        mkdir('/data/www/old/market/steplayouter/basket/' . $pos->getOrder()->getUid() . '/' . $pos->getPos(), 0777, true);
                    }

                    $outfile = '/data/www/old/market/steplayouter/basket/' . $pos->getOrder()->getUid() . '/' . $pos->getPos() . '/' . $pos->getOrder()->getAlias() . '_' . $pos->getPos() . '.pdf';

                    $this->_pdf->create($outfile, $objPosition->getLayouterId(), $pos->getOrder()->getAlias());

                    $pos->setRenderPrint(0);

                    $this->_entityManager->persist($pos);
                    $this->_entityManager->flush();
                }
            }


            unlink("/tmp/renderSteplayouter.txt");
        } catch (\Exception $e) {
            echo $e->getLine();
            echo $e->getMessage();
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->_error;
    }
}
