<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SteplayouterSettingsType extends AbstractType
{
    protected $shop = null;
    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('calendarStartYear', ChoiceType::class, array('required' => false, 'choices' => $this->buildYearChoices(), 'label' => 'Startjahr'))
            ->add('save', SubmitType::class, array('label' => 'Speichern'));
    }

    public function getName()
    {
        return 'steplayouterSettings';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Document\Install',
        ));
    }

    public function buildYearChoices()
    {
        $distance = 5;
        $yearsBefore = date('Y', mktime(0, 0, 0, date("m"), date("d"), date("Y") - $distance));
        $yearsAfter = date('Y', mktime(0, 0, 0, date("m"), date("d"), date("Y") + $distance));
        return array_combine(range($yearsBefore, $yearsAfter), range($yearsBefore, $yearsAfter));
    }
}
