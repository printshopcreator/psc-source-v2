module.exports = {

  build_dir: '../../../../../web/steplayouter',
  compile_dir: 'bin',

  app_files: {
    js: [ 'src/**/*.js', '!src/**/*.spec.js', '!src/assets/**/*.js' ],
    jsunit: [ 'src/**/*.spec.js' ],

    coffee: [ 'src/**/*.coffee', '!src/**/*.spec.coffee' ],
    coffeeunit: [ 'src/**/*.spec.coffee' ],

    atpl: [ 'src/app/**/*.tpl.html' ],
    ttpl: [ 'src/themes/**/app/**/*.tpl.html'],

    html: [ 'src/index.html'],
    less: [ 'src/less/main.less', 'vendor/angular-bootstrap-colorpicker/less/colorpicker.less']
  },

  test_files: {
    js: [
      'vendor/angular-mocks/angular-mocks.js'
    ]
  },

  vendor_files: {
    js: [
        'vendor/ng-file-upload/ng-file-upload-shim.js',
        'vendor/angular/angular.js',
        'vendor/angular-sanitize/angular-sanitize.js',
        'vendor/angular-animate/angular-animate.js',
        'vendor/angular-busy/dist/angular-busy.min.js',
        'vendor/angular-bootstrap/ui-bootstrap-tpls.js',
        'vendor/angular-loading-bar/build/loading-bar.js',
        'vendor/angular-ui-select/dist/select.js',
        'vendor/placeholders/angular-placeholders-0.0.1-SNAPSHOT.min.js',
        'vendor/angular-ui-router/release/angular-ui-router.js',
        'vendor/angular-ui-utils/modules/route/route.js',
        'fabricjs/fabric.min.js',
        'vendor/angular-bootstrap-colorpicker/js/bootstrap-colorpicker-module.js',
        'vendor/ng-file-upload/ng-file-upload.js',
        'vendor/jquery/dist/jquery.js',
        'vendor/ion-range-slider/js/ion.rangeSlider.js',
        'vendor/ion-range-slider/js/ion.rangeSlider.js',
        'vendor/angular-intro.js/lib/intro.min.js',
        'vendor/angular-intro.js/build/angular-intro.min.js',
        'vendor/jquery_mousewheel/jquery.mousewheel.js',
        'vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js',
        'vendor/mathjs/dist/math.js',
        'vendor/underscore/underscore.js',
        'vendor/holderjs/holder.js',
        'vendor/angulartics/dist/angulartics.min.js',
        'vendor/angulartics/dist/angulartics-ga.min.js',
        'vendor/angulartics-google-analytics/dist/angulartics-ga.min.js',
        'vendor/ngInfiniteScroll/build/ng-infinite-scroll.js'
    ],
    css: [
        'vendor/angular-busy/dist/angular-busy.css',
        'vendor/angular-intro.js/lib/introjs.css',
        'vendor/ion-range-slider/css/ion.rangeSlider.css',
        'vendor/angular-ui-select/dist/select.css',
        'vendor/angular-loading-bar/build/loading-bar.css',
        'vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css'
    ],
    assets: [
        'vendor/angular-bootstrap-colorpicker/img/alpha.png',
        'vendor/angular-bootstrap-colorpicker/img/hue.png',
        'vendor/angular-bootstrap-colorpicker/img/saturation.png',
        'vendor/ion-range-slider/img/sprite-skin-nice.png',
        'vendor/ion-range-slider/img/sprite-skin-flat.png',
        'vendor/malihu-custom-scrollbar-plugin/mCSB_buttons.png',
        'vendor/font-awesome/fonts/fontawesome-webfont.woff',
        'vendor/font-awesome/fonts/fontawesome-webfont.woff2',
        'vendor/font-awesome/fonts/fontawesome-webfont.eot',
        'vendor/font-awesome/fonts/fontawesome-webfont.svg',
        'vendor/font-awesome/fonts/fontawesome-webfont.woff',
        'vendor/font-awesome/fonts/fontawesome.otf'
    ]
  }
};
