angular.module( 'layouterApp.elements')

.factory('SiteElementIText', ['dataService',function(dataService) {
    var SiteElementIText = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            pos:0,
            id: "",
            type:'text',
            x: 0,
            y: 0,
            width: 0,
            height: 0,
            rotate: 0,
            selectable: false,
            clipbox: false,
            value : false,
            lineHeight: 1,
            fontSize : 18,
            useWorkImage: false,
            fontFamily : "Verdana",
            color: "#000000",
            bold: false,
            italic: false,
            underline: false,
            align: "",
            limitBoundary: false,

            populateFromBasket: function(obj) {

            },

            paint: function($rootScope, site, paper, element, x, y, width, height, selectable) {

                if(site.passepartout && element.json) {
                    fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {
                        var pass;
                        if(objects[0].path) {
                            pass = new fabric.Path(objects[0].d, {left:0,top:0});

                        }else{
                            pass = fabric.util.groupSVGElements(objects, options);
                        }

                        element.json.clipTo = null;
                        var text = fabric.IText.fromObject(element.json);
                        text.left = site.leftOffset+((text.left-site.trimLeft)*site.scale);
                        text.top = site.topOffset+((text.top-site.trimTop)*site.scale);
                        text.scaleX = site.scale;
                        text.scaleY = site.scale;

                        text.clipBox = element.json.clipBox;

                        text.siteScale = site.scale;
                        text.renderPreview = element.json.renderPreview;
                        text.renderPdf = element.json.renderPdf;
                        text.passepartout = pass;
                        text.passepartoutOrg = {objects: objects, options: options};
                        text.setControlsVisibility({bl:false,br:false,ml:false,mr:false,mb:false,mt:false,tl:false,tr:false});
                        text.templatePrintBinding = element.templatePrintBinding;
                        text.on('selected', function() {
                            var fontSize = this.fontSize;
                            var color = this.fill;
                            var fontFamily = this.fontFamily;
                            var align = this.textAlign;
                            var bold = this.fontWeight;
                            var italic = this.fontStyle;
                            var underline = this.textDecoration;
                            var text = this.text;

                            $rootScope.$broadcast('tools.text.selected', {
                                fontSize: fontSize,
                                fontFamily: fontFamily,
                                color: color,
                                align: align,
                                underline: underline,
                                italic: italic,
                                bold: bold,
                                text: text
                            });
                        });
                        text.siteScale=site.scale;
                        text.clipTo = function (ctx) {

                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(site.passepartoutX*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(site.passepartoutY*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(site.passepartoutWidth)*site.scale/ this.getScaleX())/this.passepartout.width);
                            this.passepartout.scaleY = ((parseFloat(site.passepartoutHeight)*site.scale/ this.getScaleY())/this.passepartout.height);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };


                        paper.add(text);
                        paper.renderAll();

                    })
                }else{

                    var textDecoration = "left", fontWeight = "normal", fontStyle = "normal";
                    if(element.underline) {
                        textDecoration = "underline";
                    }
                    if(element.bold) {
                        fontWeight = "bold";
                    }
                    if(element.italic) {
                        fontStyle = "italic";
                    }

                    if(element.json) {
                        element.json.clipTo = null;
                        var text = fabric.IText.fromObject(element.json);
                        text.left = site.leftOffset+((text.left-site.trimLeft)*site.scale);
                        text.top = site.topOffset+((text.top-site.trimTop)*site.scale);
                        text.scaleX = text.scaleX/1*site.scale;
                        text.scaleY = text.scaleY/1*site.scale;
                        text.pos++;
                    }else{
                        var text = new fabric.IText(element.value, {
                            left: (site.leftOffset+x*site.scale),
                            top: (site.topOffset+y*site.scale),
                            width: width,
                            height: height,
                            lockScalingX: true,
                            originX: 'left',
                            id: element.id,
                            originY: 'top',
                            lockScalingY: true,
                            lineHeight: element.lineHeight,
                            fontSize: element.fontSize,
                            fontFamily: element.fontFamily,
                            centeredRotating: true,
                            centeredScaling: true,
                            scaleX: site.scale,
                            scaleY: site.scale,
                            orgLeft: x,
                            fill: element.color,
                            textAlign: element.align,
                            textDecoration: textDecoration,
                            fontWeight: fontWeight,
                            fontStyle: fontStyle,
                            clipBox: element.clipbox,
                            angle: element.rotate,
                            renderPreview: element.renderPreview,
                            renderPdf: element.renderPdf,
                            orgTop: y,
                            pos: parseFloat(element.pos, 10)
                        });
                    }
                    text.siteScale=site.scale;
                    text.setControlsVisibility({bl:false,br:false,ml:false,mr:false,mb:false,mt:false,tl:false,tr:false});
                    text.templatePrintBinding = element.templatePrintBinding;
                    if(element.clipbox) {

                        text.clipTo = function (ctx) {

                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleX(),
                                parseFloat(this.clipBox[2])*site.scale/ this.getScaleX()+2,
                                parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()+2
                            );

                            ctx.closePath();
                            ctx.restore();
                        };

                    }

                    text.on('selected', function() {

                        var fontSize = this.fontSize;
                        var color = this.fill;
                        var fontFamily = this.fontFamily;
                        var align = this.textAlign;
                        var bold = this.fontWeight;
                        var italic = this.fontStyle;
                        var underline = this.textDecoration;
                        var text = this.text;

                        $rootScope.$broadcast('tools.text.selected', {
                            fontSize: fontSize,
                            fontFamily: fontFamily,
                            color: color,
                            align: align,
                            underline: underline,
                            italic: italic,
                            bold: bold,
                            text: text
                        });
                    });

                    paper.add(text);
                }
            },

            paintPreview: function(site, paper, element, x, y, width, height, selectable) {

                if(site.passepartout && element.json) {
                    fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {
                        var pass;
                        if(objects[0].path) {
                            pass = new fabric.Path(objects[0].d, {left:0,top:0});

                        }else{
                            pass = fabric.util.groupSVGElements(objects, options);
                        }

                        element.json.clipTo = null;
                        var text = fabric.IText.fromObject(element.json);
                        text.left = (100*text.left / site.width);
                        text.top = (100*text.top / site.width);
                        text.passepartout = pass;
                        text.scaleX = site.previewScale;
                        text.scaleY = site.previewScale;
                        text.passepartoutOrg = {objects: objects, options: options};
                        text.templatePrintBinding = element.templatePrintBinding;
                        text.clipTo = function (ctx) {

                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(site.passepartoutX*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(site.passepartoutY*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(site.passepartoutWidth)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                            this.passepartout.scaleY = ((parseFloat(site.passepartoutHeight)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };


                        paper.add(text);
                    });
                }else{

                    var textDecoration = "", fontWeight = "normal", fontStyle = "normal";
                    if(element.underline) {
                        textDecoration = "underline";
                    }
                    if(element.bold) {
                        fontWeight = "bold";
                    }
                    if(element.italic) {
                        fontStyle = "italic";
                    }
                    if(element.json) {
                        element.json.clipTo = null;
                        var text = fabric.IText.fromObject(element.json);
                        text.left = (100*text.left / site.width);
                        text.top = (100*text.top / site.width);
                        text.scaleX = site.previewScale;
                        text.scaleY = site.previewScale;
                    }else{
                        var text = new fabric.IText(element.value, {
                            left: (100*x / site.width),
                            top: (100*y / site.width),
                            fontSize: element.fontSize,
                            fontFamily: element.fontFamily,
                            centeredRotating: true,
                            centeredScaling: true,
                            id: element.id,
                            orgLeft: x,
                            originX: 'left',
                            originY: 'top',
                            fill: element.color,
                            textAlign: element.align,
                            textDecoration: textDecoration,
                            lineHeight: element.lineHeight,
                            fontWeight: fontWeight,
                            fontStyle: fontStyle,
                            clipBox: element.clipbox,
                            angle: element.rotate,
                            orgTop: y,
                            scaleX: site.previewScale,
                            renderPreview: element.renderPreview,
                            renderPdf: element.renderPdf,
                            scaleY: site.previewScale,
                            pos: parseFloat(element.pos, 10)
                        });
                        text.templatePrintBinding = element.templatePrintBinding;
                        if(element.clipbox) {

                            text.clipTo = function (ctx) {

                                ctx.save();
                                ctx.translate(0,0);
                                ctx.rotate(this.getAngle()*-1/180*Math.PI);
                                //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                                ctx.beginPath();
                                ctx.rect(
                                    (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*site.scale))*-1/ this.getScaleX(),
                                    (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*site.scale))*-1/ this.getScaleX(),
                                    parseFloat(this.clipBox[2])*site.scale/ this.getScaleX()+2,
                                    parseFloat(this.clipBox[3])*site.scale/ this.getScaleX()+2
                                );

                                ctx.closePath();
                                ctx.restore();
                            };

                        }
                    }
                    paper.add(text);



                    site.previewJson = paper.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);
                }
            }

        });
        angular.extend(this, data);
    };
    return SiteElementIText;

}])