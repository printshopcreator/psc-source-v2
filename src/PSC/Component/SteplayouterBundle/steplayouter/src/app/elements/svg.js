angular.module( 'layouterApp.elements')

.factory('SiteElementSVG', function() {
    var SiteElementSVG = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            pos:0,
            type:'svg',
            x: 0,
            y: 0,
            id: "",
            width: 0,
            height: 0,
            label: "",
            rotate: 0,
            selectable: false,
            data: false,
            value : false,
            section: false,
            fill: false,
            limitBoundary: false,

            paint: function($rootScope, site, paper, element, x, y, width, height, selectable, success) {

                fabric.loadSVGFromURL(element.value, function(objects, options) {

                    var shape = fabric.util.groupSVGElements(objects, options);

                    shape.left = (site.leftOffset+x*site.scale);
                    shape.top = (site.topOffset+y*site.scale);
                    shape.siteScale = site.scale;
                    shape.originX = 'left';
                    shape.clipBox = element.clipbox;
                    shape.id = element.id;
                    shape.originY = 'top';
                    shape.selectable = selectable;
                    shape.evented = selectable;
                    if(shape.width == 100 && shape.height == 100) {
                        shape.width = ((parseFloat(element.width) * site.scale) );
                        shape.height = ((parseFloat(element.height) * site.scale) );
                    }else{
                        shape.scaleX = ((parseFloat(element.width) * site.scale) / shape.width);
                        shape.scaleY = ((parseFloat(element.height) * site.scale) / shape.height);
                    }
                    shape.pos = parseFloat(element.pos, 10);
                    shape.elementSettings = {element: element, x: x, y: y, width: width, height: height, selectable: selectable};
                    shape.renderPreview = element.renderPreview;
                    shape.renderPdf = element.renderPdf;
                    if(element.fill) {
                        shape.fill = element.fill;
                        angular.forEach(shape.paths, function(row, key) {
                            row.fill = element.fill;
                        });
                    }else if(element.fillColor) {
                        shape.fill = element.fillColor;
                        angular.forEach(shape.paths, function(row, key) {
                            row.fill = element.fillColor;
                        });
                    }
                    if(element.hideOnStart) {
                        shape.visible = false;
                    }
                    shape.on('selected', function() {

                        $rootScope.$broadcast('tools.motiv.selected', {
                        });
                    });
                    if(element.clipbox) {
                        shape.clipBox = element.clipbox;
                        shape.clipTo = function (ctx) {

                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            //ctx.scale(motiv.getScaleX(), motiv.getScaleX());
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-(parseFloat(this.clipBox[0])*this.siteScale))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-(parseFloat(this.clipBox[1])*this.siteScale))*-1/ this.getScaleX(),
                                parseFloat(this.clipBox[2])*this.siteScale/ this.getScaleX(),
                                parseFloat(this.clipBox[3])*this.siteScale/ this.getScaleX()
                            );
                            ctx.closePath();
                            ctx.restore();
                        };

                    }
                    paper.add(shape);
                    if(success) {
                        success();
                    }
                });

            },

            paintPreview: function(site, paper, element, x, y, width, height, selectable) {
                fabric.loadSVGFromURL(element.value, function(objects, options) {
                    var shape = fabric.util.groupSVGElements(objects, options);

                    shape.left = (100*x / site.width);
                    shape.top = (100*y / site.width);
                    shape.siteScale = site.scale;
                    shape.id = element.id,
                    shape.selectable = selectable;
                    shape.evented = selectable;
                    shape.clipBox = element.clipbox;
                    shape.scaleX = ((element.width*site.scale+2)/shape.width);
                    shape.pos = parseFloat(element.pos, 10);
                    shape.renderPreview = element.renderPreview;
                    shape.renderPdf = element.renderPdf;
                    if(element.fill) {
                        shape.fill = element.fill;
                        angular.forEach(shape.paths, function(row, key) {
                            row.fill = element.fill;
                        });
                    }else if(element.fillColor) {
                        shape.fill = element.fillColor;
                        angular.forEach(shape.paths, function(row, key) {
                            row.fill = element.fillColor;
                        });
                    }
                    if(element.hideOnStart) {
                        shape.visible = false;
                    }

                    if(element.clipbox) {

                        shape.clipTo = function (ctx) {
                            ctx.save();
                            ctx.translate(0,0);
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.beginPath();
                            ctx.rect(
                                (this.getPointByOrigin("center", "center").x-site.leftOffset-parseFloat(this.clipBox[0]))*-1/ this.getScaleX(),
                                (this.getPointByOrigin("center", "center").y-site.topOffset-parseFloat(this.clipBox[1]))*-1/ this.getScaleX(),
                                parseFloat(this.clipBox[2])*this.siteScale/ this.getScaleX(),
                                parseFloat(this.clipBox[3])*this.siteScale/ this.getScaleX()
                            );
                            ctx.closePath();
                            ctx.restore();
                        };

                    }

                    paper.add(shape);

                    site.previewJson = paper.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable','lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);

                    site.previewImage = paper.toDataURL({format: "jpeg", quality: 8});
                });

            }

        });
        angular.extend(this, data);
    };
    return SiteElementSVG;

})
