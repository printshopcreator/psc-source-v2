angular.module( 'layouterApp.elements')

.factory('SiteElementRect', ['dataService',function(dataService) {
    var SiteElementRect = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            pos:0,
            type:'rect',
            color: '#cccccc',
            x: 0,
            y: 0,
            id: "",
            limitBoundary: false,
            width: 0,
            height: 0,

            paint: function($rootScope, site, paper, element, x, y, width, height, selectable) {
                if(site.passepartout) {

                    fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                        var pass;
                        if (objects[0].path) {
                            pass = new fabric.Path(objects[0].d, {left: 0, top: 0});
                        } else {
                            pass = fabric.util.groupSVGElements(objects, options);
                        }

                        var rect = new fabric.Rect({
                            left: (site.leftOffset + ((x) * site.scale)),
                            top: (site.topOffset + ((y) * site.scale)),
                            width: (width * site.scale) - 1,
                            selectable: false,
                            height: (height * site.scale) - 1,
                            fill: 'transparent',
                            id: element.id,
                            renderPreview: element.renderPreview,
                            renderPdf: element.renderPdf,
                            pos: parseFloat(element.pos, 10),
                            passepartout: pass,
                            passepartoutOrg: {objects: objects, options: options, element: {value: site.passepartout, x: site.passepartoutX, y: site.passepartoutY, width: site.passepartoutWidth, height: site.passepartoutHeight}}

                        });
                        if (element.fillColor) {
                            rect.fill = element.fillColor;
                        }
                        if (element.color != "transparent") {
                            rect.strokeWidth = 1;
                            rect.strokeDashArray = [2, 5];
                            rect.stroke = element.color;
                            rect.width = (width * site.scale) - 1;
                            rect.height = (height * site.scale) - 1;
                        }

                        rect.clipTo = function (ctx) {
                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x-site.leftOffset)-parseFloat(site.passepartoutX*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y-site.topOffset)-parseFloat(site.passepartoutY*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(site.passepartoutWidth)*site.scale/ this.getScaleX()+2)/this.passepartout.width);
                            this.passepartout.scaleY = ((parseFloat(site.passepartoutHeight)*site.scale/ this.getScaleY()+2)/this.passepartout.height);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };

                        paper.add(rect);
                    });
                }else {
                    var rect = new fabric.Rect({
                        left: (site.leftOffset + ((x) * site.scale)),
                        top: (site.topOffset + ((y) * site.scale)),
                        width: (width * site.scale) - 1,
                        selectable: false,
                        height: (height * site.scale) - 1,
                        fill: 'transparent',
                        id: element.id,
                        renderPreview: element.renderPreview,
                        renderPdf: element.renderPdf,
                        pos: parseFloat(element.pos, 10)

                    });
                    if (element.fillColor) {
                        rect.fill = element.fillColor;
                    }
                    if (element.color != "transparent") {
                        rect.strokeWidth = 1;
                        rect.strokeDashArray = [2, 5];
                        rect.stroke = element.color;
                        rect.width = (width * site.scale) - 1;
                        rect.height = (height * site.scale) - 1;
                    }

                    paper.add(rect);
                }

            },

            paintPreview: function(site, paper, element, x, y, width, height, selectable) {
                if(site.passepartout) {

                    fabric.loadSVGFromURL(dataService.getRawPath(site.passepartout), function(objects, options) {

                        var pass;
                        if (objects[0].path) {
                            pass = new fabric.Path(objects[0].d, {left: 0, top: 0});
                        } else {
                            pass = fabric.util.groupSVGElements(objects, options);
                        }
                        var rect = new fabric.Rect({
                            left: (100 * x / site.width),
                            top: (100 * y / site.width),
                            width: (width * site.previewScale) - 1,
                            selectable: false,
                            height: (height * site.previewScale) - 1,
                            strokeDashArray: [2, 5],
                            id: element.id,
                            fill: 'transparent',
                            stroke: element.color,
                            renderPreview: element.renderPreview,
                            renderPdf: element.renderPdf,
                            pos: parseFloat(element.pos, 10),
                            passepartoutOrg: {objects: objects, options: options, element: {value: site.passepartout, x: site.passepartoutX, y: site.passepartoutY, width: site.passepartoutWidth, height: site.passepartoutHeight}},
                            passepartout: pass
                        });
                        if (element.fillColor) {
                            rect.fill = element.fillColor;
                        }
                        if (element.color != "transparent") {
                            rect.strokeWidth = 1;
                            rect.strokeDashArray = [2, 5];
                            rect.stroke = element.color;
                            rect.width = (width * site.previewScale) - 1;
                            rect.height = (height * site.previewScale) - 1;
                        }
                        rect.clipTo = function (ctx) {
                            this.passepartout.top = 0;
                            this.passepartout.left = 0;
                            this.passepartout.pathOffset = {x: 0, y:0};
                            ctx.save();
                            ctx.rotate(this.getAngle()*-1/180*Math.PI);
                            ctx.translate(0,0);
                            ctx.translate(((this.getPointByOrigin("center", "center").x)-parseFloat(site.passepartoutX*site.scale))*-1/ this.getScaleX(),
                                ((this.getPointByOrigin("center", "center").y)-parseFloat(site.passepartoutY*site.scale))*-1/ this.getScaleX());
                            ctx.beginPath();
                            this.passepartout.opacity=0;
                            this.passepartout.scaleX = ((parseFloat(site.passepartoutWidth)*site.scale/ this.getScaleX()+2)/this.passepartout.width*site.previewScale);
                            this.passepartout.scaleY = ((parseFloat(site.passepartoutHeight)*site.scale/ this.getScaleY()+2)/this.passepartout.height*site.previewScale);
                            this.passepartout.render(ctx);
                            ctx.closePath();
                            ctx.restore();
                        };
                        paper.add(rect);
                    });
                }else{
                    var rect = new fabric.Rect({
                        left: (100 * x / site.width),
                        top: (100 * y / site.width),
                        width: (width * site.previewScale) - 1,
                        selectable: false,
                        height: (height * site.previewScale) - 1,
                        strokeDashArray: [2, 5],
                        id: element.id,
                        fill: 'transparent',
                        stroke: element.color,
                        renderPreview: element.renderPreview,
                        renderPdf: element.renderPdf,
                        pos: parseFloat(element.pos, 10)
                    });
                    if (element.fillColor) {
                        rect.fill = element.fillColor;
                    }
                    if (element.color != "transparent") {
                        rect.strokeWidth = 1;
                        rect.strokeDashArray = [2, 5];
                        rect.stroke = element.color;
                        rect.width = (width * site.previewScale) - 1;
                        rect.height = (height * site.previewScale) - 1;
                    }
                    paper.add(rect);
                }

            }

        });
        angular.extend(this, data);
    };
    return SiteElementRect;

}])