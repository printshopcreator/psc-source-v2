angular.module( 'layouterApp', [
  'templates-app',
  'templates-themes',
  'ngAnimate',
  'ngSanitize',
  'angular-loading-bar',
  'layouterApp.painter',
  'simplecolorpicker',
  'layouterApp.preview',
  'layouterApp.sites',
  'layouterApp.settings',
  'layouterApp.elements',
  'layouterApp.footer',
  'layouterApp.top',
  'layouterApp.tools',
  'layouterApp.motiv',
  'layouterApp.help',
  'layouterApp.wizard',
  'ui.select',
  'angular-intro',
  'ui.bootstrap',
  'angulartics',
  'angulartics.google.analytics',
  'infinite-scroll'

])

.config( function myAppConfig ( $analyticsProvider, cfpLoadingBarProvider ) {
    $analyticsProvider.virtualPageviews(false);
    cfpLoadingBarProvider.latencyThreshold = 500;
})

.run( function run () {
})
.factory('_', function() {
    return window._;
})
.factory('dataService', function($http, $location, Upload, sitesManager, $uibModal) {

    var dataService = {
        article_uuid: null,
        basket_uuid: null,
        shop_uuid: null,
        url_domain: "",
        url_prefix: "../",
        url_session: null,
        url_postfix: "",
        theme: false,
        backendMode: false,
        fonts: false,
        initalFont: false,
        datatable: [],
        dynamicSize: 0,
        dynamicSizeWidth: 0,
        dynamicSizeHeight: 0

    };


    dataService.init = function() {
        var url = $location.search();
        this.article_uuid = url.article_uuid;
        this.shop_uuid = url.shop_uuid;
        this.loadProductFromBasket = false;
        this.loadProductFromJson = false;

        if(url.debug) {
            this.url_prefix = "/apps/app_dev.php/";
            this.url_postfix = "?XDEBUG_SESSION_START";
        }else{
            this.url_prefix = "/apps/";
        }
        if(url.loadProduct) {
            this.loadProductFromJson = true;
        }
        if(url.basket_uuid) {
            this.basket_uuid = url.basket_uuid;
        }
        if(url.loadBasket) {
            this.loadProductFromBasket = true;
        }

        if(url.backendMode) {
            this.backendMode = true;
        }

        if(url.theme) {
            this.theme = url.theme;
        }

        if(url.dynamicSize) {
            this.dynamicSize = 1;
        }

        if(url.dynamicSizeWidth) {
            this.dynamicSizeWidth = url.dynamicSizeWidth;
        }

        if(url.dynamicSizeHeight) {
            this.dynamicSizeHeight = url.dynamicSizeHeight;
        }
    };

    dataService.getDataForDatatable = function() {
        return this.datatable;
    };
    dataService.setDataForDatatable = function(data) {
        this.datatable = data;
    };

    dataService.getGallery = function(callback, limit, start) {
        return $http.get(this.url_prefix + 'shop/motiv/json/gallery/' + this.shop_uuid + '/' + limit + '/' + start + this.url_postfix)
            .success(function(result) {
                callback(result);
        });
    };

    dataService.getMyGallery = function(callback, limit, start) {
        return $http.get(this.url_prefix + 'shop/motiv/json/mygallery/' + this.shop_uuid + '/' + limit + '/' + start + this.url_postfix)
            .success(function(result) {
                callback(result);
            });
    };

    dataService.openPreviewPdf = function() {
        return $http.post(this.url_prefix + 'component/steplayouter/pdf/preview/' + this.article_uuid + this.url_postfix,
            {
                payload: sitesManager.sites,
                datatable: this.datatable,
                dynamicSize: this.dynamicSize,
                dynamicSizeWidth: this.dynamicSizeWidth,
                dynamicSizeHeight: this.dynamicSizeHeight
            }
            )
            .success(function(result) {

                var modalInstance = $uibModal.open({
                    templateUrl: 'showPreviewLink',
                    backdrop: true,
                    windowClass: 'modal fade',

                    controller: function ($scope, $uibModalInstance) {
                        $scope.link = result.file;
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        };


                    }
                });
            });
    };

    dataService.saveProduct = function() {
        var postFix = this.url_postfix;
        var preFix = this.url_prefix;
        var article_uuid = this.article_uuid;
        return $http.post(preFix + 'component/steplayouter/product/save/' + this.article_uuid + postFix,
            {
                payload: sitesManager.sites,
                datatable: this.datatable
            }
        )
        .success(function(result_save) {
            window.parent.document.location.href = "/article/show/uuid/" + result_save.article;
        });
    };

    dataService.saveAndPrint = function() {
        var postFix = this.url_postfix;
        var preFix = this.url_prefix;
        var article_uuid = this.article_uuid;
        return $http.post(preFix + 'component/steplayouter/store/update' + postFix,
            {
                uuid: this.article_uuid,
                payload: sitesManager.sites,
                datatable: this.datatable
            }
        )
        .success(function(result_save) {
                var modalInstance = $uibModal.open({
                    templateUrl: 'showPdfPrint',
                    backdrop: true,
                    windowClass: 'modal fade',

                    controller: function ($scope, $uibModalInstance) {
                        $scope.article_uuid = article_uuid;
                        $scope.cancel = function () {
                            $uibModalInstance.dismiss('cancel');
                        };

                        $scope.print = function () {
                            $uibModalInstance.dismiss('cancel');
                            window.open(preFix + 'component/steplayouter/pdf/print/' + $scope.article_uuid + '/' + this.orderNr);
                            return false;
                        };

                    }
                });
        });
    };

    dataService.finishProduct = function() {

        var postFix = this.url_postfix;
        var preFix = this.url_prefix;
        var article_uuid = this.article_uuid;
        var loadProductFromBasket = this.loadProductFromBasket;

        if(this.loadProductFromJson) {
            return $http.post(preFix + 'component/steplayouter/store/update' + postFix,
                {
                    payload: sitesManager.sites,
                    uuid: this.article_uuid,
                    datatable: this.datatable
                }
            )
            .success(function(result_save) {
                if(loadProductFromBasket) {
                    window.parent.document.location.href = "/article/show/uuid/" + result_save.article + "/" + article_uuid + '?load=1';
                }else{
                    window.parent.document.location.href = "/article/show/uuid/" + result_save.article + "/" + article_uuid;
                }

            });
        }else{
            var xsrf = $.param({uuid: this.article_uuid, modus: 4});
            var datatable = this.datatable;
            var dynamicSize = this.dynamicSize;
            var dynamicSizeWidth = this.dynamicSizeWidth;
            var dynamicSizeHeight = this.dynamicSizeHeight;
            return $http({
                method: 'POST',
                url: '/service/steplayouter/savenewlayouter/format/json',
                data: xsrf,
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).success(function(result) {

                    return $http.post(preFix + 'component/steplayouter/store/save' + postFix,
                        {
                            payload: sitesManager.sites,
                            datatable: datatable,
                            layouter: result.layouter,
                            article: result.article,
                            dynamicSize: dynamicSize,
                            dynamicSizeWidth: dynamicSizeWidth,
                            dynamicSizeHeight: dynamicSizeHeight
                        }
                    )
                        .success(function(result_save) {
                            if(dataService.dynamicSize) {
                                window.parent.document.location.href = "/article/buy?count=1&load=0&uuid=" + result.article;
                            }else{
                                window.parent.document.location.href = "/article/show/uuid/" + result.article + "/" + result.layouter;
                            }
                        });

                });
        }




    };

    dataService.getImageWorkPath = function(value) {
        return this.url_prefix + 'shop/motiv/json/get/work/' + value;
    };

    dataService.getRawPath = function(value) {
        return this.url_prefix + 'shop/motiv/json/get/raw/' + value;
    };

    dataService.getImageMidPath = function(value) {
        return this.url_domain + this.url_prefix + 'shop/motiv/json/get/mid/' + value;
    };

    dataService.getImageResizePath = function(value, width, height) {
        return this.url_domain + this.url_prefix + 'shop/motiv/json/get/resize/' + value + '/' + width + '/' + height;
    };

    dataService.getImageThumbPath = function(value) {
        return this.url_domain + this.url_prefix + 'shop/motiv/json/get/thumb/' + value;
    };

    dataService.getCalImage = function(type, month, year, width, height, month_size, day_size, month_color, day_color, sun_color,
                                       designYear, designHeightMonthCal,  designMarginCal,
                                       calendarX, calendarY, yearX, yearY,
                                       calendarWidth, calendarHeight, monthX, monthY, yearSize, fontFamily ) {
        return this.url_domain + this.url_prefix + 'component/steplayouter/calendar/render/' + type + '/' + month
            + '/' + year + '/' + width + '/' + height + '/' + month_size
            + '/' + day_size + '/' + month_color + '/' + day_color
            + '/' + sun_color + '/' + designYear + '/' + designHeightMonthCal + '/' + designMarginCal
            + '/' + calendarX + '/' + calendarY + '/' + yearX + '/' + yearY
            + '/' + calendarWidth + '/' + calendarHeight + '/' + monthX + '/' + monthY + '/' + yearSize + '/' + fontFamily;
    };

    dataService.loadProduct = function(callback) {
        return $http.get(this.url_prefix + 'component/steplayouter/product/load/' + this.article_uuid + this.url_postfix, { tracker: 'app_busy', ignoreLoadingBar: true })
            .success(function(result) {
                callback(result);
            });

    };

    dataService.loadFromBasket = function(callback) {
        return $http.get(this.url_prefix + 'component/steplayouter/store/load/' + this.article_uuid + this.url_postfix, { tracker: 'app_busy' })
            .success(function(result) {
                callback(result);
            });
    };

    dataService.loadShopSettings = function(callback) {
        return $http.get(this.url_prefix + 'shop/settings/json/' + this.shop_uuid + this.url_postfix, { tracker: 'app_busy' })
            .success(function(result) {
                callback(result);
            });

    };

    dataService.uploadMotiv = function(file, callback, sc) {
        Upload.upload({
            url: this.url_prefix + 'shop/motiv/json/upload' + this.url_postfix,
            file: file
        }).progress(sc).then(function(result, status, headers, config) {
            callback(result.data);
        });

    };

    dataService.init();

    return dataService;

})
.controller( 'AppCtrl', function AppCtrl ( $scope, $location, $window, painterManager, dataService, $rootScope, sitesManager, _ ) {

    $scope.$on('cfpLoadingBar:started', function(){
        $scope.showLoading = true;
    });
    $scope.$on('cfpLoadingBar:completed', function(){
        $scope.showLoading = false;
    });

    $scope.IntroOptions = {
        steps:[
            {
                element: "#closeButton",
                intro: "Layouter schließen",
                position: 'left'
            },
            {
                element: "#addMotiv",
                intro: "Ein eigenes Motiv oder eines aus der Gallerie ins Layout einfügen",
                position: 'left'
            },
            {
                element: "#addText",
                intro: "Hirmit können Sie Text dem Layout hinzufügen",
                position: 'left'
            },
            {
                element: "#actionBar",
                intro: "Hier können sie eine PDF Vorschau Ihres Produktes anfordern oder das Produkt auch speichern um es dann zu bestellen.",
                position: 'top'
            }
        ],
        showStepNumbers: false,
        exitOnOverlayClick: true,
        exitOnEsc:true,
        nextLabel: '<strong>Weiter</strong>',
        prevLabel: '<span style="color:green">Zurück</span>',
        skipLabel: 'Beenden',
        doneLabel: 'Danke'
    };



    $scope.init = function () {
        var url = $location.search();
        $scope.stylePath = 'assets/steplayouter.css';
        $scope.showLoading = false;
        if(url.theme) {
            $scope.theme = url.theme;
            $scope.stylePath = 'assets/themes/' + url.theme + '/less/steplayouter.css';
        }

        WebFontConfig = {

        };

        var fonts = [ 'Mr+Bedfort::latin', 'Schoolbell::latin', 'Fredoka+One::latin', 'UnifrakturCook:700:latin', 'Great+Vibes::latin', 'Audiowide::latin', 'Cinzel+Decorative:400,700,900:latin', 'Ewert::latin', 'Kaushan+Script::latin', 'Seaweed+Script::latin' ];
        if(url.theme == 'dp') {
            fonts =  [ 'Fascinate::latin', 'Rajdhani:400,700:latin', 'Chicle::latin', 'Rouge+Script::latin', 'Permanent+Marker::latin', 'Lato:300,400,700,400italic,700italic:latin', 'Arvo:400,700,400italic,700italic:latin', 'Sarina::latin', 'New+Rocker::latin', 'Lora:400,700,400italic,700italic:latin', 'Henny+Penny::latin', 'Shojumaru::latin', 'Gochi+Hand::latin', 'Josefin+Slab:300,400,700,300italic,400italic,700italic:latin', 'Great+Vibes::latin', 'Vollkorn:400italic,700italic,400,700:latin', 'Ribeye+Marrow::latin', 'Sancreek::latin', 'Abril+Fatface::latin', 'UnifrakturMaguntia::latin', 'Lobster+Two:400,400italic,700italic,700:latin', 'Oregano:400,400italic:latin', 'Luckiest+Guy::latin', 'Creepster::latin', 'Ewert::latin', 'Flavors::latin', 'Architects+Daughter::latin', 'Rock+Salt::latin', 'Pacifico::latin' ];
        }
        if(url.theme == 'niceart') {
            fonts =  ['Open+Sans::latin','Slabo+27px::latin','Open+Sans+Condensed:300:latin','Lora::latin','Yanone+Kaffeesatz::latin','Lobster::latin','Covered+By+Your+Grace::latin','Great+Vibes::latin','Shadows+Into+Light+Two::latin','Marvel::latin','Quattrocento::latin','Sacramento::latin','Telex::latin','Rochester::latin','Londrina+Solid::latin','IM+Fell+English::latin','Bowlby+One+SC::latin','Meddon::latin','Dawning+of+a+New+Day::latin','Medula+One::latin','Cutive+Mono::latin','Herr+Von+Muellerhoff::latin','Zeyada::latin','Ruluko::latin','McLaren::latin','Buda:300:latin','Sail::latin','Antic+Didone::latin','Poppins::latin','Text+Me+One::latin','Architects+Daughter::latin','Josefin+Slab::latin'];
        }
        if(url.theme == 'psc' || url.theme == 'psc_kdv') {
            fonts =  ['Noto+Serif+Display:300,400,700,400italic,700italic:latin', 'Rampart+One::latin', 'IM+Fell+English+SC::latin', 'Creepster::latin', 'Faster+One::latin', 'Homemade Apple::latin', 'Rye::latin', 'UnifrakturMaguntia::latin', 'Grenze+Gotisch::latin',                                                        'Coming+Soon::latin', 'Gloria+Hallelujah::latin', 'Maven+Pro:400:latin', 'Open+Sans::latin','Open+Sans+Condensed:300:latin','Lora::latin','Yanone+Kaffeesatz::latin','Lobster::latin','Covered+By+Your+Grace::latin','Great+Vibes::latin','Shadows+Into+Light+Two::latin','Marvel::latin','Quattrocento::latin','Sacramento::latin','Telex::latin','Rochester::latin','Londrina+Solid::latin','IM+Fell+English::latin','Bowlby+One+SC::latin','Meddon::latin','Dawning+of+a+New+Day::latin','Medula+One::latin','Cutive+Mono::latin','Herr+Von+Muellerhoff::latin','Zeyada::latin','Ruluko::latin','McLaren::latin','Buda:300:latin','Sail::latin','Antic+Didone::latin','Poppins::latin','Text+Me+One::latin','Architects+Daughter::latin','Josefin+Slab::latin'];
        }
        if(url.theme == 'gz') {
            fonts =  ['Noto+Serif+Display:300,400,700,400italic,700italic:latin', 'Rampart+One::latin', 'IM+Fell+English+SC::latin', 'Creepster::latin', 'Faster+One::latin', 'Homemade Apple::latin', 'Rye::latin', 'UnifrakturMaguntia::latin', 'Grenze+Gotisch::latin',                                                        'Coming+Soon::latin', 'Gloria+Hallelujah::latin', 'Maven+Pro:400:latin', 'Open+Sans::latin','Open+Sans+Condensed:300:latin','Lora::latin','Yanone+Kaffeesatz::latin','Lobster::latin','Covered+By+Your+Grace::latin','Great+Vibes::latin','Shadows+Into+Light+Two::latin','Marvel::latin','Quattrocento::latin','Sacramento::latin','Telex::latin','Rochester::latin','Londrina+Solid::latin','IM+Fell+English::latin','Bowlby+One+SC::latin','Meddon::latin','Dawning+of+a+New+Day::latin','Medula+One::latin','Cutive+Mono::latin','Herr+Von+Muellerhoff::latin','Zeyada::latin','Ruluko::latin','McLaren::latin','Buda:300:latin','Sail::latin','Antic+Didone::latin','Poppins::latin','Text+Me+One::latin','Architects+Daughter::latin','Josefin+Slab::latin'];
        }
        if(url.theme == 'astov') {
            fonts =  ['Roboto::latin', 'Coming+Soon::latin', 'Gloria+Hallelujah::latin', 'Maven+Pro:400:latin', 'Open+Sans::latin','Slabo+27px::latin','Open+Sans+Condensed:300:latin','Lora::latin','Yanone+Kaffeesatz::latin','Lobster::latin','Covered+By+Your+Grace::latin','Great+Vibes::latin','Shadows+Into+Light+Two::latin','Marvel::latin','Quattrocento::latin','Sacramento::latin','Telex::latin','Rochester::latin','Londrina+Solid::latin','IM+Fell+English::latin','Bowlby+One+SC::latin','Meddon::latin','Dawning+of+a+New+Day::latin','Medula+One::latin','Cutive+Mono::latin','Herr+Von+Muellerhoff::latin','Zeyada::latin','Ruluko::latin','McLaren::latin','Buda:300:latin','Sail::latin','Antic+Didone::latin','Poppins::latin','Text+Me+One::latin','Architects+Daughter::latin','Josefin+Slab::latin'];
        }
        if(url.theme == 'mypetbooks') {
            fonts =  ['Amatica+SC::latin','Courgette::latin','Leckerli+One::latin','Patrick+Hand::latin','Permanent+Marker::latin','Roboto::latin', 'Coming+Soon::latin', 'Gloria+Hallelujah::latin', 'Maven+Pro:700:latin', 'Open+Sans::latin','Slabo+27px::latin','Open+Sans+Condensed:300:latin','Lora::latin','Yanone+Kaffeesatz::latin','Lobster::latin','Covered+By+Your+Grace::latin','Great+Vibes::latin','Shadows+Into+Light+Two::latin','Marvel::latin','Quattrocento::latin','Sacramento::latin','Telex::latin','Rochester::latin','Londrina+Solid::latin','IM+Fell+English::latin','Bowlby+One+SC::latin','Meddon::latin','Dawning+of+a+New+Day::latin','Medula+One::latin','Cutive+Mono::latin','Herr+Von+Muellerhoff::latin','Zeyada::latin','Ruluko::latin','McLaren::latin','Buda:300:latin','Sail::latin','Antic+Didone::latin','Poppins::latin','Text+Me+One::latin','Architects+Daughter::latin','Josefin+Slab::latin'];
        }

        var tempFonts = [];

        angular.forEach(fonts, function(row, key) {
            var font = row.split(':');
            var fontBold = false;
            var fontBoldItalic = false;
            var fontItalic = false;

            if(font[1] != '') {
                var fontVariant = font[1].split(',');
                if(fontVariant.indexOf('700') > -1) {
                    fontBold = true;
                }

                if(fontVariant.indexOf('700italic') > -1) {
                    fontBoldItalic = true;
                }

                if(fontVariant.indexOf('400italic') > -1) {
                    fontItalic = true;
                }
            }
            tempFonts.push({
                name: font[0].replace("+", " ").replace("+", " ").replace("+", " ").replace("+", " "),
                bold: fontBold,
                boldItalic: fontBoldItalic,
                italic: fontItalic
            });


        });

        dataService.loadShopSettings(function(obj) {
            if(obj.success) {
                if(obj.logo2) {
                    jQuery("#top").css("background-image", "url(../../" + obj.logo2 + ")");
                    jQuery("#top").css("min-height", "72px");
                    jQuery("#top .navbar-brand").css('display', 'none');
                }
                if(obj.googleanalyticscode) {
                    $window._gaq.push(['_setAccount',obj.googleanalyticscode]);
                }
            }
        });

        dataService.initalFont = _.first(tempFonts);

        dataService.fonts = tempFonts;

        WebFont.load({
            active: function() {
                if (dataService.article_uuid != null) { // $routeParams.Id) {
                    if(dataService.loadProductFromJson) {
                        dataService.loadFromBasket(function(obj) {
                            dataService.url_domain = obj.article.server;
                            dataService.url_session = obj.article.session;
                            dataService.datatable = obj.datatable;
                            $rootScope.$broadcast('top.showDatatableSelector', obj.datatable);
                            $rootScope.$broadcast('top.setTitle', obj.article.title);
                            $rootScope.$broadcast('top.setHelp', obj.article.help);
                            $rootScope.$broadcast('product.load',  {'data': obj.article}, obj.data);
                            $rootScope.$broadcast('settings.load', obj.article);

                            if(obj.article.access.saveProduct) {
                                $rootScope.$broadcast('settings.saveProduct', true);
                            }
                            if(obj.article.access.printPdf) {
                                $rootScope.$broadcast('settings.printPdf', true);
                            }
                        });


                    }else{
                        dataService.loadProduct(function(obj) {


                            if(obj.article) {
                                dataService.url_domain = obj.article.server;
                                dataService.url_session = obj.article.session;
                                $rootScope.$broadcast('top.setTitle', obj.article.title);
                                $rootScope.$broadcast('top.setHelp', obj.article.help);
                                $rootScope.$broadcast('top.showDatatableSelector', obj.datatable);
                                $rootScope.$broadcast('top.setTitle', obj.article.title);
                                $rootScope.$broadcast('top.setHelp', obj.article.help);
                                $rootScope.$broadcast('product.load',  {'data': obj.article}, obj.data);
                                $rootScope.$broadcast('settings.load', obj.article);

                                if(obj.article.access.saveProduct) {
                                    $rootScope.$broadcast('settings.saveProduct', true);
                                }

                            }else{
                                dataService.url_domain = obj.data.server;
                                dataService.url_session = obj.data.session;
                                $rootScope.$broadcast('top.setTitle', obj.data.title);
                                $rootScope.$broadcast('top.setHelp', obj.data.help);
                                $rootScope.$broadcast('product.load', obj, []);
                                $rootScope.$broadcast('settings.load', obj.data);

                                if(obj.data.access.saveProduct) {
                                    $rootScope.$broadcast('settings.saveProduct', true);
                                }
                            }
                        });
                    }
                } else {
                    //create a new object
                }

                $scope.isSaving = false;
            },
            google: {
                families: fonts
            }
        });
    };

    var w = angular.element($window);

    $scope.getHeight = function() {
        return w.innerHeight * innerWidth;
    };
    $scope.$watch($scope.getHeight, function(newValue, oldValue) {

    });

    w.bind('resize', function () {
        if(!dataService.theme) {
            painterManager.resize();
            $scope.$apply();
        }
    });

    $scope.init();
}).directive('holderFix', function () {
    return {
        link: function (scope, element, attrs) {
            Holder.run({ images: element[0], nocss: true });
        }
    };
});
