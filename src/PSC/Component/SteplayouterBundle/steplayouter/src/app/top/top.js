angular.module( 'layouterApp.top', [

])

/**
 * Each section or module of the site can also have its own routes. AngularJS
 * will handle ensuring they are all available at run-time, but splitting it
 * this way makes each module more "self-contained".
 */
.config(function config( ) {
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'TopCtrl', function TopController($rootScope, $scope, $uibModal, dataService, _, painterManager ) {

        $scope.actSite = 1;
        $scope.sumSite = 1;
        $scope.title = "";
        $scope.help = "";
        $scope.maxSiteCount = 1;
        $scope.minSiteCount = 1;
        $scope.showSiteCount = false;
        $scope.showSiteCountError = false;
        $scope.datatable = [];
        $scope.datatableLabel = 'firstname';
        $scope.mydatatableSelected = false;

        $scope.mydatatableSelect = function(item) {
            painterManager.changeTextForDataTemplate(item);
        };

        $scope.$on('top.showDatatableSelector', function(e, element) {
            $scope.datatable = element.rows;
            $scope.datatableLabel = element.label;
        });

        $scope.editDatatable = function(item) {
            $rootScope.$broadcast('wizard.showWizard');
        };

        $scope.$on('top.changeSites', function(e, obj) {
            $scope.actSite = obj.actSite;
            $scope.sumSite = obj.sumSite;
            if(obj.sumSite > 1) {
                $scope.showSiteCount = true;
            }

            if(obj.sumSite > 4 && obj.sumSite%4 && obj.sumSite != 13) {
                //$scope.showSiteCountError = true;
                //$scope.minSiteCount = obj.sumSite-(obj.sumSite%4);
                //$scope.maxSiteCount = obj.sumSite+4-(obj.sumSite%4);
            }else{
                $scope.showSiteCountError = false;
            }
        });

        $scope.$on('top.setTitle', function(e, title) {
            $scope.title = title;
        });
        $scope.$on('top.setHelp', function(e, help) {
            $scope.help = help;
        });

        $scope.closeApplication = function() {
            $uibModal.open({
                templateUrl: 'closeApplication',
                backdrop: true,
                windowClass: 'modal fade',
                controller: function ($scope, $uibModalInstance) {
                    $scope.cancel = function () {
                        $uibModalInstance.dismiss('cancel');
                    };

                    $scope.ok = function () {
                        if(dataService.loadProductFromJson) {
                            window.parent.document.location.href = "/article/show/uuid/" + dataService.basket_uuid + "/" + dataService.article_uuid;
                        }else{
                            window.parent.document.location.href = "/article/show/uuid/" + dataService.article_uuid;
                        }
                    };
                }
            });
        };


})

;
