angular.module( 'layouterApp.tools', [
  'colorpicker.module'
])

.config(function config( ) {
})
.controller( 'ToolsCtrl', function ToolsController( $scope, $rootScope, painterManager, dataService, _ ) {
    $scope.show = false;
    $scope.showTextTools = false;
    $scope.showMotivTools = false;
    $scope.showSettingsTools = true;
    $scope.isShowSettingsToolsDetails = false;
    $scope.isShowCalendarSettingsToolsDetails = false;
    $scope.enableSettingsToolButton = false;
    $scope.enableCalendarSettingsToolButton = false;
    $scope.showTools = false;
    $scope.help = "";
    $scope.oneAtATime = true;

    $scope.settings = {
        font: "verdana",
        color: {name: 'black', value: "#000000"},
        colors: false,
        size: 20,
        bold: false,
        underline: false,
        italic: false,
        showBold: dataService.initalFont.bold,
        showItalic: dataService.initalFont.italic,
        alignLeft: false,
        alignRight: false,
        alignCenter: false,
        alignJustify: false,
        picker: false,
        text: '',
        showDownload: false,
        showReset: false,
        showDelete: true,
        motivWidth: 0,
        motivHeight: 0,
        motivUUID: "",
        motivReset: ""
    };
    $scope.$on('top.setHelp', function(e, help) {
        $scope.help = help;
    });

    $scope.$on('settings.showToolsDetails', function(e, obj) {
        $scope.isShowSettingsToolsDetails = obj;
    });

    $scope.$on('settings.showTools', function(e, obj) {
        $scope.showSettingsTools = obj;
    });

    $scope.fonts = dataService.fonts;
    $scope.settings.font = $scope.fonts[0];

    $scope.$on('sites.selected', function(e, obj) {
        $scope.settings.colors = obj.settingsDefaults.createText.colors;
    });

    $scope.$on('settings.showCalendarSettingsButton', function(e, obj) {
        $scope.enableCalendarSettingsToolButton = true;
        $scope.showCalendarSettingsTools = true;
    });

    $scope.addText = function() {
        painterManager.addText();
    };

    $scope.$on('general.showProductSettingsButton', function() {
        $scope.enableSettingsToolButton = true;
    });

    $scope.$on('general.hideProductSettingsButton', function() {
        $scope.enableSettingsToolButton = false;
    });

    $scope.$watch('settings.color', function() {
        if($scope.settings.color !== null && typeof $scope.settings.color === 'object') {
            painterManager.setColor($scope.settings.color.value);
        }else{
            painterManager.setColor($scope.settings.color);
        }
    }); // initialize the watch

    $scope.$watch('settings.size', function() {
        painterManager.setSize($scope.settings.size);
        jQuery("#tools_size").ionRangeSlider("update", {
            from: $scope.settings.size
        });

    });

    $scope.$watch('settings.text', function() {
        painterManager.setText($scope.settings.text);
    });

    $scope.deleteElement = function() {
        painterManager.deleteElement();
    };

    $scope.resetElement = function(id, uuid) {
        painterManager.changeMotivById($scope.settings.motivUUID, dataService.getImageResizePath($scope.settings.motivReset, $scope.settings.motivWidth, $scope.settings.motivHeight));
    };

    $scope.onTop = function() {
        painterManager.onTop();
    };

    $scope.onBottom = function() {
        painterManager.onBottom();
    };

    $scope.moveForward = function() {
        painterManager.moveForward();
    };

    $scope.moveBack = function() {
        painterManager.moveBack();
    };

    $scope.selectFont = function() {
        var selectedFont = $scope.settings.font;
        if(!selectedFont.bold) {
            $scope.settings.bold = "";
            $scope.settings.showBold = false;
            painterManager.resetBold();
        }else{
            $scope.settings.showBold = true;
        }
        if(!selectedFont.italic) {
            $scope.settings.italic = "";
            painterManager.resetItalic();
            $scope.settings.showItalic = false;
        }else{
            $scope.settings.showItalic = true;
        }
        painterManager.setFont(selectedFont.name);
        dataService.initalFont = selectedFont;
    };

    $scope.selectBold = function() {
        if($scope.settings.bold == "active") {
            $scope.settings.bold = "";
            if(dataService.initalFont.italic) {
                $scope.settings.showItalic = true;
            }
        }else{
            $scope.settings.bold = "active";
            if(!dataService.initalFont.boldItalic) {
                $scope.settings.italic = "";
                painterManager.resetItalic();
                $scope.settings.showItalic = false;
            }
        }

        painterManager.setBold();
    };

    $scope.selectItalic = function() {
        if($scope.settings.italic == "active") {
            $scope.settings.italic = "";
            if(dataService.initalFont.bold) {
                $scope.settings.showBold = true;
            }
        }else{
            $scope.settings.italic = "active";
            if(!dataService.initalFont.boldItalic) {
                $scope.settings.bold = "";
                $scope.settings.showBold = false;
                painterManager.resetBold();
            }
        }

        painterManager.setItalic();
    };

    $scope.selectUnderline = function() {
        if($scope.settings.underline == "active") {
            $scope.settings.underline = "";
        }else{
            $scope.settings.underline = "active";
        }
        painterManager.setUnderline();
    };

    $scope.selectAlignLeft = function() {
        $scope.settings.alignLeft = "active";
        $scope.settings.alignCenter = "";
        $scope.settings.alignRight = "";
        $scope.settings.alignJustify = "";
        painterManager.setAlign('left');
    };

    $scope.selectAlignCenter = function() {
        $scope.settings.alignLeft = "";
        $scope.settings.alignCenter = "active";
        $scope.settings.alignRight = "";
        $scope.settings.alignJustify = "";
        painterManager.setAlign('center');
    };

    $scope.selectAlignJustify = function() {
        $scope.settings.alignLeft = "";
        $scope.settings.alignCenter = "";
        $scope.settings.alignRight = "";
        $scope.settings.alignJustify = "active";
    };

    $scope.selectAlignRight = function() {
        $scope.settings.alignLeft = "";
        $scope.settings.alignCenter = "";
        $scope.settings.alignRight = "active";
        $scope.settings.alignJustify = "";
        painterManager.setAlign('right');
    };

    $scope.$on('sites.selected', function() {
        $scope.show = true;
    });

    $scope.$on('tools.hide', function() {
        $scope.showTextTools = false;
        $scope.showTools = false;
        $scope.settings.picker = false;
        $scope.showMotivTools = false;
        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
            $scope.$apply();
        }
    });

    $scope.$on('tools.text.selected', function(e, obj) {
        $scope.settings.picker = false;
        $scope.settings.font = obj.fontFamily;
        $scope.settings.text = obj.text;
        $scope.settings.color = {name: '', value: obj.color};
        var selectedFont = $scope.settings.font;
        selectedFont = _.find(dataService.fonts, function(font){ return font.name == selectedFont; });

        if(!selectedFont.bold) {
            $scope.settings.bold = "";
            $scope.settings.showBold = false;
            painterManager.resetBold();
        }else{
            $scope.settings.showBold = true;
        }
        if(!selectedFont.italic) {
            $scope.settings.italic = "";
            painterManager.resetItalic();
            $scope.settings.showItalic = false;
        }else{
            $scope.settings.showItalic = true;
        }

        $scope.settings.bold = false;
        $scope.settings.italic = false;
        $scope.settings.underline = false;
        $scope.settings.alignLeft = false;
        $scope.settings.alignCenter = false;
        $scope.settings.alignRight = false;

        if(obj.align == "left") {
            $scope.settings.alignLeft = "active";
        }
        if(obj.align == "center") {
            $scope.settings.alignCenter = "active";
        }
        if(obj.align == "right") {
            $scope.settings.alignRight = "active";
        }

        if(obj.bold == "bold") {
            $scope.settings.bold = "active";
        }
        if(obj.italic == "italic") {
            $scope.settings.italic = "active";
        }
        if(obj.underline == "underline") {
            $scope.settings.underline = "active";
        }

        jQuery("#tools_size").ionRangeSlider({
            min: 0,                        // min value
            max: 150,                       // max value
            from: obj.fontSize,                       // overwrite default FROM setting
            to: 150,                         // overwrite default TO setting
            type: "single",                 // slider type
            step: 1,                       // slider step
            postfix: "",             // postfix text
            hasGrid: true,                  // enable grid
            hideMinMax: true,               // hide Min and Max fields
            hideFromTo: true,               // hide From and To fields
            prettify: true,
            onChange: function(obj){        // callback, is called on every change
                $scope.settings.size = obj.fromNumber;
                painterManager.setSize($scope.settings.size);
                jQuery("#tool_size_raw").val($scope.settings.size);
            }
        });
        jQuery("#tools_size").ionRangeSlider("update", {
            from: obj.fontSize
        });
        $scope.settings.size = obj.fontSize;
        $scope.settings.picker = true;
        $scope.showTextTools = true;
        $scope.showTools = true;
        $scope.showMotivTools = false;
        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
            $scope.$apply();
        }
    });

    $scope.$on('tools.motiv.selected', function(e, obj) {
        $scope.showTextTools = false;
        $scope.showMotivTools = true;
        $scope.showTools = true;
        if(obj.deletable == 0) {
            $scope.settings.showDelete = false;
        }else{
            $scope.settings.showDelete = true;
        }
        if(obj.resetable != "") {
            $scope.settings.showReset = true;
            $scope.settings.motivReset = obj.resetable;
        }else{
            $scope.settings.showReset = false;
        }

        $scope.settings.motivUUID = obj.motivUUID;
        $scope.settings.motivWidth = obj.motivWidth;
        $scope.settings.motivHeight = obj.motivHeight;

        $scope.settings.showDownload = dataService.backendMode;
        if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
            $scope.$apply();
        }

    });
})

;
