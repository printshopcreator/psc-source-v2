function setPreview($timeout, fileReader, index) {
    fileReader.onload = function(e) {
        $timeout(function() {
            $scope.dataUrls[index] = e.target.result;
        });
    };
}

angular.module( 'layouterApp.motiv', [
        'ui.bootstrap',
        'ngFileUpload'
    ])

.config(function config( ) {
})


.controller( 'MotivCtrl', function MotivController( $scope, $uibModal, Upload, painterManager, dataService, $timeout ) {

    $scope.MotivGallery = {
        limit: 24,
        start: 0
    };

    $scope.openMotivGallery = function(editMode) {
        var motivCtl = $scope;

        $uibModal.open({
            templateUrl: 'motivGallery',
            backdrop: true,
            size: 'lg',
            windowClass: 'modal fade modalGallery',
            controller: function ($scope, $uibModalInstance, motive) {
                $scope.ds = dataService;
                $scope.editMode = editMode;
                $scope.count = 0;
                $scope.myCount = 0;
                $scope.page = 1;
                $scope.myPage = 1;


                dataService.getGallery(function(data) {
                    $scope.motive = data.rows;
                    $scope.count = data.count;

                },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                dataService.getMyGallery(function(data) {

                        $scope.myMotive = data.rows;
                        $scope.myCount = data.count;
                    },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                $scope.pageChanged = function() {
                    dataService.getGallery(function(data) {
                            $scope.motive = data.rows;
                            $scope.count = data.count;

                        },
                        motivCtl.MotivGallery.limit,
                        (this.page-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.myPageChanged = function() {
                    dataService.getMyGallery(function(data) {

                            $scope.myMotive = data.rows;
                            $scope.myCount = data.count;
                        },
                        motivCtl.MotivGallery.limit,
                        (this.myPage-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.selectMotiv = function(motiv_uuid, resize) {
                    if($scope.editMode) {
                        painterManager.changeMotiv(dataService.getImageMidPath(motiv_uuid), resize);
                        $uibModalInstance.dismiss('cancel');
                    }else{
                        painterManager.addMotiv(dataService.getImageMidPath(motiv_uuid));
                        $uibModalInstance.dismiss('cancel');
                    }
                };


                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {
                motive: function () {
                    return $scope.motive;
                }
            }
        });

    };

    $scope.openClipartGallery = function(editMode) {
        var motivCtl = $scope;



        $uibModal.open({
            templateUrl: 'motivGallery',
            backdrop: true,
            size: 'lg',
            windowClass: 'modal fade modalGallery',
            controller: function ($scope, $uibModalInstance, motive) {
                $scope.ds = dataService;
                $scope.editMode = editMode;
                $scope.count = 0;
                $scope.myCount = 0;
                $scope.page = 1;
                $scope.myPage = 1;


                dataService.getGallery(function(data) {
                        $scope.motive = data.rows;
                        $scope.count = data.count;
                    },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                dataService.getMyGallery(function(data) {

                        $scope.myMotive = data.rows;
                    },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                $scope.pageChanged = function() {
                    dataService.getGallery(function(data) {
                            $scope.motive = data.rows;
                            $scope.count = data.count;

                        },
                        motivCtl.MotivGallery.limit,
                        (this.page-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.myPageChanged = function() {
                    dataService.getMyGallery(function(data) {

                            $scope.myMotive = data.rows;
                            $scope.myCount = data.count;
                        },
                        motivCtl.MotivGallery.limit,
                        (this.myPage-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.selectMotiv = function(motiv_uuid, resize) {
                    if($scope.editMode) {
                        painterManager.changeMotiv(dataService.getImageMidPath(motiv_uuid), resize);
                        $uibModalInstance.dismiss('cancel');
                    }else{
                        painterManager.addClipart(dataService.getImageMidPath(motiv_uuid));
                        $uibModalInstance.dismiss('cancel');
                    }
                };


                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {
                motive: function () {
                    return $scope.motive;
                }
            }
        });

    };

    $scope.selectMotivMethod = function (editMode) {

        var motivCtl = $scope;

        $scope.MotivGallery = {
            limit: 24,
            start: 0
        };

        $uibModal.open({
            templateUrl: 'motivMethod',
            backdrop: true,
            size: 'lg',
            windowClass: 'modal fade',
            controller: function ($scope, $uibModalInstance) {
                $scope.ds = dataService;
                $scope.editMode = editMode;
                $scope.count = 0;
                $scope.myCount = 0;
                $scope.page = 1;
                $scope.myPage = 1;

                dataService.getGallery(function(data) {
                        $scope.motive = data.rows;
                        $scope.count = data.count;
                    },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                dataService.getMyGallery(function(data) {

                        $scope.myMotive = data.rows;
                        $scope.myCount = data.count;
                    },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                $scope.pageChanged = function() {
                    dataService.getGallery(function(data) {
                            $scope.motive = data.rows;
                            $scope.count = data.count;

                        },
                        motivCtl.MotivGallery.limit,
                        (this.page-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.myPageChanged = function() {
                    dataService.getMyGallery(function(data) {

                            $scope.myMotive = data.rows;
                            $scope.myCount = data.count;
                        },
                        motivCtl.MotivGallery.limit,
                        (this.myPage-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.selectMotiv = function(motiv_uuid, resize) {
                    if($scope.editMode) {
                        painterManager.changeMotiv(dataService.getImageMidPath(motiv_uuid), resize);
                        $uibModalInstance.dismiss('cancel');
                    }else{
                        painterManager.addMotiv(dataService.getImageMidPath(motiv_uuid));
                        $uibModalInstance.dismiss('cancel');
                    }
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.onFileSelect = function($files, resize) {
                    $scope.selectedFiles = $files;
                    $scope.dataUrls = [];
                    $scope.progress = [];
                    for (var i = 0; i < $files.length; i++) {
                        var $file = $files[i];
                        if (window.FileReader && $file.type.indexOf('image') > -1) {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL($files[i]);
                            fileReader.onload = function(e) {
                                $timeout(function() {
                                    $scope.dataUrls[i] = e.target.result;
                                });
                            }
                        }
                        $scope.progress[i] = -1;

                        dataService.uploadMotiv($file, function(data) {
                            $scope.selectedFiles = null;
                            if($scope.editMode) {
                                painterManager.changeMotiv(dataService.getImageMidPath(data.uuid), resize);
                                $uibModalInstance.dismiss('cancel');
                            }else{
                                painterManager.addMotiv(dataService.getImageMidPath(data.uuid));
                                $uibModalInstance.dismiss('cancel');
                            }
                        },function(evt) {
                            $scope.progress[0] = parseFloat(100.0 * evt.loaded / evt.total, 10);
                        });

                    }
                };

                $scope.openMotivGallery = function () {

                    motivCtl.openMotivGallery($scope.editMode);
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });

    };

    $scope.downloadMotiv = function() {
        painterManager.downloadMotiv();
    };

    $scope.selectMotivEditMethod = function () {

        var motivCtl = $scope;

        $scope.MotivGallery = {
            limit: 24,
            start: 0
        };

        $uibModal.open({
            templateUrl: 'motivEditMethod',
            backdrop: true,
            size: 'lg',
            windowClass: 'modal fade',
            controller: function ($scope, $uibModalInstance) {
                $scope.ds = dataService;
                $scope.count = 0;
                $scope.myCount = 0;
                $scope.page = 1;
                $scope.myPage = 1;

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                dataService.getGallery(function(data) {
                        $scope.motive = data.rows;
                        $scope.count = data.count;
                    },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                dataService.getMyGallery(function(data) {
                        $scope.myMotive = data.rows;
                        $scope.myCount = data.count;
                    },
                    motivCtl.MotivGallery.limit,
                    motivCtl.MotivGallery.start
                );

                $scope.pageChanged = function() {
                    dataService.getGallery(function(data) {
                            $scope.motive = data.rows;
                            $scope.count = data.count;

                        },
                        motivCtl.MotivGallery.limit,
                        (this.page-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.myPageChanged = function() {
                    dataService.getMyGallery(function(data) {

                            $scope.myMotive = data.rows;
                            $scope.myCount = data.count;
                        },
                        motivCtl.MotivGallery.limit,
                        (this.myPage-1) * motivCtl.MotivGallery.limit
                    );
                };

                $scope.selectMotiv = function(motiv_uuid, resize) {
                    if($scope.editMode) {
                        painterManager.changeMotiv(dataService.getImageMidPath(motiv_uuid), resize);
                        $uibModalInstance.dismiss('cancel');
                    }else{
                        painterManager.addMotiv(dataService.getImageMidPath(motiv_uuid));
                        $uibModalInstance.dismiss('cancel');
                    }
                };

                $scope.onFileSelect = function($files, resize) {
                    $scope.selectedFiles = $files;
                    $scope.dataUrls = [];
                    $scope.progress = [];
                    for (var i = 0; i < $files.length; i++) {
                        var $file = $files[i];
                        if (window.FileReader && $file.type.indexOf('image') > -1) {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL($files[i]);
                            fileReader.onload = function(e) {
                                $timeout(function() {
                                    $scope.dataUrls[i] = e.target.result;
                                });
                            }
                        }
                        $scope.progress[i] = -1;

                        dataService.uploadMotiv($file, function(data) {
                            painterManager.changeMotiv(dataService.getImageMidPath(data.uuid), resize);
                            $modalInstance.dismiss('cancel');
                        },function(evt) {
                            $scope.progress[0] = parseFloat(100.0 * evt.loaded / evt.total, 10);
                        });

                    }
                };

                $scope.openMotivGallery = function (editMode) {

                    motivCtl.openMotivGallery(editMode);
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });

    };
});
