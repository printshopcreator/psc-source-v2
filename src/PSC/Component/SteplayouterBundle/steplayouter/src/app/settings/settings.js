angular.module( 'layouterApp.settings', [
  'colorpicker.module'
])

.config(function config( ) {
})

/**
 * And of course we define a controller for our route.
 */
.controller( 'SettingsCtrl', function SettingsController( $scope, $uibModal, $rootScope, painterManager, dataService ) {
    $scope.show = false;
    $scope.showCalendarSettings = false;
    $scope.settings = [];
    $scope.calendarSetting = false;
    $scope.motivSelects = [];
    $scope.svgSelects = [];
    $scope.selectedColor = {name: "White", value: '#ffffff'};

    $scope.picker = false;

    $scope.$on('general.showProductSettings', function() {
        $scope.show = !$scope.show;
    });

    $scope.$on('settings.showCalendarSettingsButton', function(e, obj) {
        $scope.showCalendarSettings = true;
    });

    $scope.$on('sites.selected', function(e, obj) {
        $scope.selectedColor = {value: obj.backgroundColor, name: ''};
        $scope.show = obj.showsettings;
        $scope.settings = obj.settings;
        if($scope.settings.length > 0) {
            $rootScope.$broadcast('settings.showToolsDetails', true);
        }
        if($scope.settings.length == 0) {
            $rootScope.$broadcast('settings.showTools', false);
        }
    });

    $scope.$on('general.addSetting', function(e, obj) {
        $scope.settings.push(obj);
    });

    $scope.$on('general.addCalendarSetting', function(e, obj) {
        $scope.calendarSetting = obj;
        $scope.calendarSetting.picker = true;
    });

    $scope.changeCalendarSetting = function() {
        painterManager.changeCalSettings($scope.calendarSetting.startMonth, $scope.calendarSetting.startYear);
    };

    $scope.$on('general.addMotivSelect', function(e, obj) {
        $scope.motivSelects.push(obj);
    });

    $scope.$on('general.addSvgSelect', function(e, obj) {
        $scope.svgSelects.push(obj);
    });

    $scope.openMotivSelect = function(id, resize) {
        var motivCtl = $scope;

        $uibModal.open({
            templateUrl: 'motivSelect',
            backdrop: true,
            size: 'lg',
            windowClass: 'modal fade modalGallery',
            controller: function ($scope, $uibModalInstance) {
                $scope.ds = dataService;
                $scope.id = id;

                $scope.motiveElement = _.first(_.filter(motivCtl.motivSelects, function(element){ return id == element.id; }));
                $scope.motive = $scope.motiveElement.motive;
                $scope.selectMotiv = function(motiv_uuid) {
                    if(motiv_uuid == "") {
                        painterManager.hideMotivById(id);
                    }else{
                        if($scope.motiveElement.append) {
                            painterManager.addMotiv(dataService.getImageMidPath(motiv_uuid), $scope.motiveElement.x, $scope.motiveElement.y, $scope.motiveElement.width, $scope.motiveElement.height);
                        }else{
                            if (resize) {
                                painterManager.changeMotivById(id, dataService.getImageResizePath(motiv_uuid, $scope.motiveElement.width, $scope.motiveElement.height));
                            } else {
                                painterManager.changeMotivWithoutResizeById(id, dataService.getImageResizePath(motiv_uuid, $scope.motiveElement.width, $scope.motiveElement.height));
                            }
                            this.motiveElement.value = motiv_uuid;
                            painterManager.showMotivById(id);
                        }
                    }
                    painterManager.removeStatic();
                    $uibModalInstance.dismiss('cancel');
                };


                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {
                motive: function () {
                    return $scope.motive;
                }
            }
        });

    };

    $scope.openSvgSelect = function(id) {
        var motivCtl = $scope;

        $uibModal.open({
            templateUrl: 'svgSelect',
            backdrop: true,
            size: 'lg',
            windowClass: 'modal fade modalGallery',
            controller: function ($scope, $uibModalInstance) {
                $scope.ds = dataService;
                $scope.id = id;

                $scope.motiveElement = _.first(_.filter(motivCtl.svgSelects, function(element){ return id == element.id; }));
                $scope.motive = $scope.motiveElement.motive;
                $scope.selectSvg = function(motiv_uuid) {
                    if(motiv_uuid == "") {
                        painterManager.hideMotivById(id);
                    }else{
                        if($scope.motiveElement.append) {
                            painterManager.addSvg(dataService.getRawPath(motiv_uuid));
                        }else {
                            painterManager.changeSvgById(id, dataService.getRawPath(motiv_uuid), function(painterManager) {
                                painterManager.showMotivById(id);
                            });
                            this.motiveElement.value = motiv_uuid;
                        }
                    }
                    painterManager.removeStatic();
                    $uibModalInstance.dismiss('cancel');
                };


                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {
                motive: function () {
                    return $scope.motive;
                }
            }
        });

    };

    $scope.openCalSettings = function(id) {
        var motivCtl = $scope;
        //var painterManager = painterManager;

        $uibModal.open({
            templateUrl: 'calSettings',
            backdrop: true,
            size: 'lg',
            windowClass: 'modal fade modalGallery',
            controller: function ($scope, $uibModalInstance) {
                $scope.calendarSetting = motivCtl.calendarSetting;
                $scope.changeCalendarSetting = function() {
                    painterManager.changeCalSettings($scope.calendarSetting.startMonth, $scope.calendarSetting.startYear);
                    painterManager.removeStatic();
                };
                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            },
            resolve: {

            }
        });

    };


}).directive('showProductSettings', function(){
        return {
            restrict: 'A',
            link: function(scope, elem, attrs){
                return scope.show;
            }
        }
    })
;
