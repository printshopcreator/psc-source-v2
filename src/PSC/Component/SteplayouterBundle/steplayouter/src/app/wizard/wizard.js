angular.module( 'layouterApp.wizard', [
    'colorpicker.module',
    'ngFileUpload'
])

.config(function config( ) {
})
.factory('wizardManager', function() {
    var wizardManager = {
        steps: [],

        selectableMotive: [],
        multiUploadFiles: []
    };

    return wizardManager;
})
.controller( 'WizardCtrl', function SettingsController( $scope, $uibModal, $rootScope, painterManager, sitesManager, dataService, wizardManager, $timeout, Upload ) {

    $scope.$on('wizard.addSelectableMotiv', function(e, obj) {
        wizardManager.selectableMotive.push(obj);
    });

    $scope.$on('wizard.showWizard', function(e, obj) {


        var wizardCtl = $scope;

        $uibModal.open({
            templateUrl: 'stepSet',
            backdrop: false,
            size: 'lg',
            windowClass: 'modal fade',
            controller: function ($scope, $uibModalInstance) {
                $scope.index = 0;
                $scope.steps = wizardManager.steps;

                $scope.selectableMotive = wizardManager.selectableMotive;
                $scope.multiUploadFiles = wizardManager.multiUploadFiles;

                $scope.steps[0].isDisplayed = true;

                $scope.nextEnabled = true;
                $scope.previousEnabled = false;
                $scope.submitEnabled = false;
                $scope.changes = [];
                $scope.next = function () {
                    if ($scope.steps.length === 0) {
                        return;
                    }
                    // If we're at the last step, then stay there.
                    if ($scope.index == $scope.steps.length - 1) {
                        return;
                    }
                    $scope.steps[$scope.index].isFinished = true;
                    $scope.steps[$scope.index++].isDisplayed = false;
                    $scope.steps[$scope.index].isDisplayed = true;
                    $scope.setButtons();
                };

                $scope.previous = function () {
                    if ($scope.steps.length === 0) {
                        return;
                    }

                    if ($scope.index === 0) {
                        return;
                    }
                    $scope.steps[$scope.index].isFinished = false;
                    $scope.steps[$scope.index--].isDisplayed = false;
                    $scope.steps[$scope.index].isDisplayed = true;
                    $scope.setButtons();

                };

                $scope.setButtons = function () {
                    angular.forEach($scope.changes,function(obj){
                        var bindings = obj.bind.split(",");
                        angular.forEach(bindings,function(bind){
                            painterManager.changeText(bind, obj.value);
                            sitesManager.changeText(bind, obj.value);
                        });
                    });
                    $scope.changes = [];
                    if ($scope.index == $scope.steps.length - 1) {
                        $scope.nextEnabled = false;
                        $scope.previousEnabled = true;
                        $scope.submitEnabled = true;
                    } else if ($scope.index === 0) {
                        $scope.previousEnabled = false;
                        $scope.nextEnabled = true;
                        $scope.submitEnabled = false;
                    } else {
                        $scope.nextEnabled = true;
                        $scope.previousEnabled = true;
                        $scope.submitEnabled = false;
                    }
                };

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.finish = function () {
                    angular.forEach(wizardManager.multiUploadFiles, function(obj, index) {
                        if(wizardManager.selectableMotive[index]) {
                            wizardManager.selectableMotive[index].value =
                                dataService.getImageResizePath(obj.uuid, wizardManager.selectableMotive[index].width+2, wizardManager.selectableMotive[index].height+2);

                            if(painterManager.site.id == wizardManager.selectableMotive[index].site) {
                                painterManager.changeMotivWithoutResizeById(wizardManager.selectableMotive[index].id, dataService.getImageMidPath(obj.uuid));
                            }
                        }
                    });
                    angular.forEach($scope.steps, function(step, indexStep) {
                        angular.forEach(step.elements, function(element, indexElement) {
                            if(element.type == 'datatable') {
                                dataService.setDataForDatatable(element.rows);
                                $rootScope.$broadcast('top.showDatatableSelector', element);
                            }
                        });

                    });
                    angular.forEach($scope.changes,function(obj){
                        var bindings = obj.bind.split(",");
                        angular.forEach(bindings,function(bind){
                            painterManager.changeText(bind, obj.value);
                            sitesManager.changeText(bind, obj.value);
                        });

                    });
                    $scope.changes = [];
                    $scope.steps[$scope.index].isDisplayed = false;
                    $scope.index = 0;
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.changeText = function(element) {
                    var change = _.find($scope.changes, function(obj){ return obj.bind == element.bind; })
                    if(change) {

                    }else{
                        $scope.changes.push(element);
                    };
                };

                $scope.removeRowFromDatatable = function(row, element) {
                    var index = element.rows.indexOf(row);
                    element.rows.splice(index, 1);
                };

                $scope.addRowFromDatatable = function(element) {
                    element.rows.push({});
                };

                $scope.openFileSelect = function(id) {
                    $timeout(function(){document.getElementById('file' + id).click()});

                };

                $scope.changeMotiv = function($files, element) {
                    $scope.selectedFiles = $files;
                    $scope.dataUrls = [];
                    $scope.progress = [];
                    for (var i = 0; i < $files.length; i++) {
                        var $file = $files[i];
                        if (window.FileReader && $file.type.indexOf('image') > -1) {
                            var fileReader = new FileReader();
                            fileReader.readAsDataURL($files[i]);
                            fileReader.onload = function(e) {
                                $timeout(function() {
                                    $scope.dataUrls[i] = e.target.result;
                                });
                            }
                        }
                        $scope.progress[i] = -1;

                        dataService.uploadMotiv($file, function(data) {
                            $scope.selectedFiles = null;
                            var bindings = element.bind.split(",");
                            angular.forEach(bindings,function(bind){
                                painterManager.changeMotivById(bind, dataService.getImageMidPath(data.uuid));
                                sitesManager.changeMotivById(bind, element.value);
                            });


                        },function(evt) {
                            $scope.progress[0] = parseFloat(100.0 * evt.loaded / evt.total, 10);
                        });

                    }
                };

                $scope.changeMotive = function($files, element) {

                    $scope.progress = [];
                    for (var i = 0; i < $files.length; i++) {
                        var $file = $files[i];

                        $scope.progress[i] = -1;

                        dataService.uploadMotiv($file, function(data) {
                            wizardManager.multiUploadFiles.push({
                                url: dataService.getImageMidPath(data.uuid),
                                uuid: data.uuid
                            });

                        },function(evt) {
                            $scope.progress[0] = parseFloat(100.0 * evt.loaded / evt.total, 10);
                        });

                    }
                };
            },
            resolve: {

            }
        });


    });
});