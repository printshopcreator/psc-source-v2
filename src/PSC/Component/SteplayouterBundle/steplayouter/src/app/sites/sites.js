angular.module( 'layouterApp.sites', [
    'cfp.loadingBar'
])

.config(function config( ) {
})

.directive('repeatDone', function($timeout) {
    return function(scope, element, attrs) {
        if (scope.$last) {
            scope.$eval(attrs.repeatDone);
        }
    };
})

.factory('SiteElement', function(painterManager) {
    var SiteElement = function(data) {
        //set defaults properties and functions
        angular.extend(this, {
            id:null,
            internId: null,
            elements: [],
            previewFabric: null,
            pos:0,
            title:0,
            status:'NEW',
            scale: 1,
            leftOffset: 0,
            topOffset: 0,
            width: 0,
            maxWidth: 0,
            height: 0,
            maxHeight: 0,
            trimLeft: 0,
            trimRight: 0,
            trimTop: 0,
            site: 0,
            trimBottom: 0,
            fabricJson: null,
            previewJson: null,
            saveJson: null,
            clipbox: false,
            previewLeftOffset: 0,
            previewTopOffset: 0,
            previewWidth: 0,
            previewImage: null,
            fabricSvg: null,
            useWorkImage: false,
            previewHeight: 0,
            fabricObj: null,
            passepartout: false,
            passepartoutX: false,
            passepartoutY: false,
            passepartoutWidth: false,
            passepartoutHeight: false,
            backgroundColor: false,
            renderPrintCutting: false,
            layoutable: true,
            showsettings: false,
            settings: [],
            calendarSetting: false,

            populateFromBasket: function(obj) {
                this.id = obj.id;
                this.internId = obj.id;
                this.previewFabric = obj.previewFabric;
                this.pos = obj.pos;
                this.status = obj.status;
                this.scale = obj.scale;
                this.leftOffset = obj.leftOffset;
                this.topOffset = obj.topOffset;
                this.width = obj.width;
                this.maxWidth = obj.maxWidth;
                this.calcWidth = obj.calcWidth;
                this.height = obj.height;
                this.maxHeight = obj.maxHeight;
                this.fabricJson = obj.fabricJson;
                this.previewImage = obj.previewImage;
                this.previewJson = obj.previewJson;
                this.previewLeftOffset = obj.previewLeftOffset;
                this.previewTopOffset = obj.previewTopOffset;
                this.previewWidth = obj.previewWidth;
                this.previewImage = obj.previewImage;
                this.fabricSvg = obj.fabricSvg;
                this.previewHeight = obj.previewHeight;

            },

            copyFromObject: function(obj) {

                obj.pos = this.pos;
                obj.status = this.status;
                obj.scale = this.scale;
                obj.leftOffset = this.leftOffset;
                obj.topOffset = this.topOffset;
                obj.width = this.width;
                obj.maxWidth = this.maxWidth;
                obj.calcWidth = this.calcWidth;
                obj.height = this.height;
                obj.maxHeight = this.maxHeight;
                obj.fabricJson = this.fabricJson;
                obj.previewImage = this.previewImage;
                obj.previewJson = this.previewJson;
                obj.previewLeftOffset = this.previewLeftOffset;
                obj.previewTopOffset = this.previewTopOffset;
                obj.previewWidth= this.previewWidth;
                obj.previewImage = this.previewImage;
                obj.fabricSvg = this.fabricSvg;
                obj.previewHeight = this.previewHeight;
                obj.elements = this.elements;
                obj.data = this.data;
                obj.section = this.section;
                obj.id = this.id;
            },

            asArray: function() {
                return {
                    'id': this.id,
                    'internId': this.internId
                };
            },
            convertPreviewJson: function() {
                this.previewJson = this.previewFabric.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable', 'deletable', 'resetable',,'lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);
            },
            isNew:function(){
                return (this.status=='NEW' || this.id == null);
            },
            addElement:function(element) {
                this.elements.push(element);
            },
            addSetting:function(setting) {
                this.settings.push(setting);
            },
            setCalendarSetting:function(setting) {
                this.calendarSetting = setting;
            },
            convertFabricToJson:function() {
                if(this.fabricObj == null) {
                    return;
                }
                this.fabricJson = this.fabricObj.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable', 'deletable', 'resetable', 'lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);
                this.previewImage = this.fabricObj.toDataURL({format: "jpeg", quality: 8});
            },
            convertFabricToJsonOnly:function() {
                this.fabricJson = this.fabricObj.toJSON(['limitBoundary', 'templatePrintBinding', 'passepartoutOrg', 'id', 'renderPreview', 'renderPdf', 'passepartout', 'centeredScaling', 'centeredRotation', 'fontFamily', 'originX', 'originY', 'siteScale', 'clipBox', 'calcWidth', 'name', 'transformMatrix', 'currentWidth', 'currentHeight', 'style', 'height', 'width', 'orgLeft', 'orgTop', 'pos', 'scaleX', 'scaleY', 'angle', 'selectable', 'deletable', 'resetable',,'lockMovementX', 'lockMovementY', 'lockRotation', 'lockScalingX', 'lockScalingY', 'lockUniScaling']);
                this.previewImage = this.fabricObj.toDataURL({format: "jpeg", quality: 8});
            },
            convertFabricToSvg:function() {
                if(this.fabricObj == null) {
                    return;
                }
                this.fabricSvg = this.fabricObjtoSVG({viewBox: {x: this.leftOffset, y: this.topOffset, width: this.width, height: this.height}});
                this.previewImage = this.fabricObj.toDataURL({format: "jpeg", quality: 8});
            },
            preview:function() {
                painterManager.preview(this);

            }
        });
        angular.extend(this, data);
    };
    return SiteElement;

})

.factory('sitesManager', function() {
    var sitesManager = {
        sites: [],

        layouts: [],

        addSite: function(site) {
            this.sites.push(site);
        },

        selectSite: function(which) {

        },

        changeText: function(bind, value) {
            angular.forEach(this.sites,function(site, i){
                angular.forEach(site.elements,function(element, i){
                    if(element.id == bind) {
                        element.value = value;
                    }
                });
            });
        },

        changeMotivById: function(bind, value) {
            angular.forEach(this.sites,function(site, i){
                angular.forEach(site.elements,function(element, i){
                    if(element.id == bind) {
                        element.value = value;
                    }
                });
            });
        },

        asArray: function() {
            var ar = [];

            angular.forEach(this.sites,function(obj, i){
                ar.push(obj.asArray());
            });

            return ar;
        }
    };

    return sitesManager;
})

.controller( 'SitesCtrl', function SiteController( $scope, sitesManager, SiteElement, SiteElementImage, SiteElementRect, SiteElementCal, SiteElementTextbox, SiteElementIText, SiteElementText, SiteElementSVG, $rootScope, dataService, painterManager, previewManager, $timeout, $uibModal, _, cfpLoadingBar, wizardManager, $analytics ) {
    $scope.sites = sitesManager.sites;
    $scope.title = function () {
        if(painterManager.site == null) {
            return "Ansicht";
        }
        return painterManager.site.title;
    };
    $scope.settings = {
        showRemoveSite: false,
        showAddSite: false,
        showSiteBrowser: false,
        showSiteBrowserPrevButton: {show: false, id: 0},
        showSiteBrowserNextButton: {show: false, id: 0},
        showSiteBrowserPrevItem1: {text: '', id:0, show: false},
        showSiteBrowserPrevItem2: {text: '', id:0, show: false},
        showSiteBrowserNextItem1: {text: '', id:0, show: false},
        showSiteBrowserNextItem2: {text: '', id:0, show: false},
    };

    $scope.deleteSite = function() {

        var modalInstance = $uibModal.open({
            templateUrl: 'deleteSite',
            backdrop: true,
            windowClass: 'modal fade',

            controller: function ($scope, $uibModalInstance) {

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.ok = function () {
                    var index = 0;
                    var temp = [];

                    angular.forEach(sitesManager.sites,function(obj, i){

                        if(obj.id != painterManager.site.id) {
                            obj.id = index;
                            obj.internId = index+1;
                            temp[index] = obj;
                            index++;
                        }
                    });

                    $uibModalInstance.close(temp);
                };
            }
        });

        modalInstance.result.then(function (sites) {
            $scope.sites = sites;
            sitesManager.sites = sites;
            painterManager.select(sitesManager.sites[0]);

            $rootScope.$broadcast('top.changeSites', {
                'actSite': 1,
                'sumSite': $scope.sites.length
            });
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });

    };

    $scope.addLayout = function(layout) {

        var modalInstance = $uibModal.open({
            templateUrl: 'addLayout',
            backdrop: true,
            windowClass: 'modal fade',

            controller: function ($scope, $uibModalInstance) {

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.ok = function () {

                    var obj = new SiteElement();
                    sitesManager.sites[layout].copyFromObject(obj);
                    obj.id = sitesManager.sites.length;
                    obj.internId = sitesManager.sites.length+1;
                    sitesManager.sites.push(obj);

                    $uibModalInstance.close();
                };
            }
        });

        modalInstance.result.then(function () {

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });

    };

    $scope.createSite = function() {

        var modalInstance = $uibModal.open({
            templateUrl: 'sitesLayoutGallery',
            backdrop: true,
            windowClass: 'modal fade',
            controller: function ($scope, $uibModalInstance) {

                var layouts = [];
                angular.forEach(sitesManager.layouts,function(obj, i){
                    layouts.push({
                        id: obj.id,
                        vorlageSite: (obj.vorlageSite-1),
                        previewImage: sitesManager.sites[(obj.vorlageSite-1)].previewImage
                    });
                });

                $scope.layouts = layouts;

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };

                $scope.addLayout = function(id) {
                    $uibModalInstance.close(id);
                };
            }
        });

        modalInstance.result.then(function (id) {
            $scope.addLayout(id);
        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.selectSite = function(site) {
        cfpLoadingBar.start();

        $rootScope.$broadcast('top.changeSites', {
            'actSite': site+1,
            'sumSite': $scope.sites.length
        });

        var layoutable = _.filter($scope.sites, function(siteObj){ return siteObj.layoutable; });

        if(layoutable[site-1]) {
            $scope.settings.showSiteBrowserPrevButton.show = true;
            $scope.settings.showSiteBrowserPrevButton.id = site-1;
            $scope.settings.showSiteBrowserPrevItem1.text = layoutable[site-1].title;
            $scope.settings.showSiteBrowserPrevItem1.id = site-1;
            $scope.settings.showSiteBrowserPrevItem1.show = true;
        }else{
            $scope.settings.showSiteBrowserPrevButton.show = false;
            $scope.settings.showSiteBrowserPrevItem1.show = false;
        }
        if(layoutable[site-2]) {
            $scope.settings.showSiteBrowserPrevItem2.text = layoutable[site-2].title;
            $scope.settings.showSiteBrowserPrevItem2.id = site-2;
            $scope.settings.showSiteBrowserPrevItem2.show = true;
        }else{
            $scope.settings.showSiteBrowserPrevItem2.show = false;
        }
        if(layoutable[site+1]) {
            $scope.settings.showSiteBrowserNextButton.show = true;
            $scope.settings.showSiteBrowserNextButton.id = site+1;
            $scope.settings.showSiteBrowserNextItem1.text = layoutable[site+1].title;
            $scope.settings.showSiteBrowserNextItem1.id = site+1;
            $scope.settings.showSiteBrowserNextItem1.show = true;
        }else{
            $scope.settings.showSiteBrowserNextButton.show = false;
            $scope.settings.showSiteBrowserNextItem1.show = false;
        }
        if(layoutable[site+2]) {
            $scope.settings.showSiteBrowserNextItem2.text = layoutable[site+2].title;
            $scope.settings.showSiteBrowserNextItem2.id = site+2;
            $scope.settings.showSiteBrowserNextItem2.show = true;
        }else{
            $scope.settings.showSiteBrowserNextItem2.show = false
        }

        painterManager.select($scope.sites[site]);
        $rootScope.$broadcast('sites.selected', $scope.sites[site]);
    };

    $scope.layoutDone = function(site) {
        var layoutable = _.filter($scope.sites, function(siteObj){ return siteObj.layoutable; });
        if(layoutable.length > 1) {
            $scope.settings.showSiteBrowser = true;
            $scope.settings.showSiteBrowserNextButton.show = true;
            $scope.settings.showSiteBrowserNextButton.id = 1;
            $scope.settings.showSiteBrowserNextItem1.text = layoutable[1].title;
            $scope.settings.showSiteBrowserNextItem1.id = 1;
            $scope.settings.showSiteBrowserNextItem1.show = true;

            if(layoutable[2]) {
                $scope.settings.showSiteBrowserNextItem2.text = layoutable[2].title;
                $scope.settings.showSiteBrowserNextItem2.id = 2;
                $scope.settings.showSiteBrowserNextItem2.show = true;
            }
        }
        $rootScope.$broadcast('top.changeSites', {
            'actSite': 1,
            'sumSite': layoutable.length
        });

        $timeout(function() {
            angular.forEach($scope.sites,function(obj, i){
                if(obj.saveJson !== null) {
                    previewManager.previewFromBasket(obj);
                }else if(obj.fabricJson !== null) {
                    previewManager.previewFromBasket(obj);
                }else{
                    previewManager.preview(obj);
                }
            });
        });
    };

    $scope.$on('sites.added', function($scope, site) {
        sitesManager.addSite(site);
        $scope.sites = sitesManager.sites;
    });

    $scope.$on('basket.load', function($scope, data) {

        if(data.article.sites_layouts.length > 0) {
            $scope.currentScope.settings.showAddSite = true;
            $scope.currentScope.settings.showRemoveSite = true;
            $scope.currentScope.settings.showSiteBrowser = true;
        }else{
            if(data.data.length > 1) {
                $scope.currentScope.settings.showSiteBrowser = true;
            }
        }

        angular.forEach(data.data,function(obj, i){
            var siteElement = new SiteElement();
            angular.forEach(obj.fabricJson.objects,function(obj, i){
                obj.clipTo = null;
            });
            angular.forEach(obj.saveJson.objects,function(obj, i){
                obj.clipTo = null;
            });
            siteElement.populateFromBasket(obj);
            angular.forEach(obj.elements,function(el, i){
                if(el.type == "setting") {
                    $rootScope.$broadcast('general.showProductSettingsButton');
                }
                if(el.type == "formsvgselect" || el.type == "svg") {
                    if(el.id != "background-color") {

                    }

                    if(el.type == "formsvgselect") {

                        $rootScope.$broadcast('general.addSvgSelect', el);
                    }

                    if(el.section == "colors") {

                        $rootScope.$broadcast('general.showProductSettingsButton');
                    }
                }


            });

            painterManager.createLoaded(siteElement);


        });

        sitesManager.layouts = data.article.sites_layouts;

    });

    $scope.$on('product.load', function($scope, data, basket) {
        cfpLoadingBar.start();
        if(data.data.sites_layouts.length > 0) {
            $scope.currentScope.settings.showAddSite = true;
            $scope.currentScope.settings.showRemoveSite = true;
        }

        angular.forEach(data.data.sites,function(obj, i){



            siteElement = new SiteElement();
            siteElement.id = sitesManager.sites.length;
            siteElement.internId = obj.id;
            siteElement.width = parseFloat(obj.width, 10);
            siteElement.maxWidth = parseFloat(obj.maxWidth, 10);
            siteElement.calcWidth = parseFloat(obj.width, 10);
            siteElement.height = parseFloat(obj.height, 10);
            siteElement.trimLeft = parseFloat(obj.trimLeft, 10);
            siteElement.trimRight = parseFloat(obj.trimRight, 10);
            siteElement.trimTop = parseFloat(obj.trimTop, 10);
            siteElement.trimBottom = parseFloat(obj.trimBottom, 10);
            siteElement.maxHeight = parseFloat(obj.maxHeight, 10);
            siteElement.passepartout = obj.passepartout;
            siteElement.maxZoom = obj.maxZoom;
            siteElement.passepartoutX = parseFloat(obj.passepartoutX, 10);
            siteElement.passepartoutY = parseFloat(obj.passepartoutY, 10);
            siteElement.passepartoutWidth = parseFloat(obj.passepartoutWidth, 10);
            siteElement.passepartoutHeight = parseFloat(obj.passepartoutHeight, 10);
            siteElement.renderPrintCutting = obj.renderPrintCutting;
            siteElement.renderPrintMedia = obj.renderPrintMedia;
            siteElement.backgroundColor = obj.backgroundColor;
            siteElement.title = obj.title;
            siteElement.settingsDefaults = obj.settingsDefault;
            siteElement.layoutable = obj.layoutable;
            siteElement.showsettings = obj.showsettings;

            if(dataService.dynamicSize) {
                siteElement.width = parseFloat(dataService.dynamicSizeWidth, 10);
                siteElement.calcWidth = parseFloat(dataService.dynamicSizeWidth, 10);
                siteElement.height = parseFloat(dataService.dynamicSizeHeight, 10);
                siteElement.maxWidth = parseFloat(dataService.dynamicSizeWidth, 10);
                siteElement.maxHeight = parseFloat(dataService.dynamicSizeHeight, 10);
            }

            var basketSite = _.find(basket, function(siteObj){ return siteObj.internId == siteElement.internId; });

            if(basketSite) {
                siteElement.backgroundColor = basketSite.backgroundColor;
            }
            var ids = [];
            var match = null;

            angular.forEach(obj.elements,function(element, ie){

                var basketEl = false;
                var basketElement = false;
                if(basketSite && basketSite.saveJson) {
                    var basketEl = _.find(basketSite.saveJson.objects, function(elemJson){ return elemJson.id == element.id; });
                    var basketElement = _.find(basketSite.elements, function(elemJson){ return elemJson.id == element.id; });
                }else if(basketSite && basketSite.fabricJson) {
                    var basketEl = _.find(basketSite.fabricJson.objects, function(elemJson){ return elemJson.id == element.id; });
                    var basketElement = _.find(basketSite.elements, function(elemJson){ return elemJson.id == element.id; });
                }
                if(element.type == "setting") {
                    var Svg = new SiteElementSVG();
                    Svg.x = element.x;
                    Svg.y = element.y;
                    Svg.id = element.id;
                    Svg.label = element.label;
                    Svg.width = element.width;
                    Svg.height = element.height;
                    Svg.value = dataService.getRawPath(element.value);
                    Svg.data = element.data;
                    Svg.section = element.section;
                    Svg.pos = parseFloat(element.pos, 10);
                    Svg.renderPreview= element.renderPreview;
                    Svg.renderPdf= element.renderPdf;
                    Svg.selectable = element.selectable;
                    Svg.deletable = element.deletable;
                    Svg.resetable = element.resetable;


                    $rootScope.$broadcast('general.showProductSettingsButton');
                    siteElement.addSetting(Svg);
                }

                if(element.type == "rect") {
                    var Rect = new SiteElementRect();
                    Rect.x = element.x;
                    Rect.y = element.y;
                    Rect.label = element.label;
                    Rect.altLabel = element.altLabel;
                    Rect.id = element.id;
                    Rect.width = element.width;
                    Rect.height = element.height;
                    Rect.color = element.value;
                    Rect.fillColor = element.fillColor;
                    Rect.pos = parseFloat(element.pos, 10);
                    Rect.renderPreview= element.renderPreview;
                    Rect.renderPdf= element.renderPdf;
                    Rect.data = element.data;
                    Rect.section = element.section;
                    Rect.selectable = element.selectable;
                    if(basketEl) {
                        Rect.fillColor = basketEl.fill;
                    }
                    Rect.deletable = element.deletable;
                    Rect.resetable = element.resetable;
                    siteElement.addElement(Rect);

                    if(element.section == "colors") {
                        siteElement.addSetting(Rect);
                        $rootScope.$broadcast('general.showProductSettingsButton');
                    }
                }

                if(element.type == "formmotivselect" || element.type == "formuploadmotiv" || element.type == "motiv") {
                    var Image = new SiteElementImage();
                    Image.x = element.x;
                    Image.y = element.y;
                    Image.id = element.id;
                    Image.width = element.width;
                    Image.height = element.height;
                    Image.label = element.label;
                    Image.altLabel = element.altLabel;
                    if(basketEl) {
                        match = basketEl.src.match(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);
                        if(!match) {
                            match = basketEl.src.match(/[0-9a-f]{4}-[0-9a-f]{8}-[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{8}/);
                        }
                        if(!match) {
                            match = basketEl.src.match(/[0-9a-f]{24}/);
                        }
                        element.value = match[0];
                        Image.json = basketEl;
                    }

                    if(element.useWorkImage) {
                        Image.value = dataService.getImageWorkPath(element.value);
                    }else{
                        Image.value = dataService.getImageResizePath(element.value, element.width + 2, element.height + 2);
                    }
                    Image.selectable = element.selectable;
                    Image.pos = parseFloat(element.pos, 10);
                    Image.clipbox = element.clipbox;
                    Image.passepartout = element.passepartout;
                    Image.passepartoutX = parseFloat(element.passepartoutX);
                    Image.passepartoutY = parseFloat(element.passepartoutY);
                    Image.passepartoutWidth = parseFloat(element.passepartoutWidth);
                    Image.passepartoutHeight = parseFloat(element.passepartoutHeight);
                    Image.passepartoutRel = element.passepartoutRel;
                    Image.rotate = parseFloat(element.rotate, 10);
                    Image.useWorkImage= element.useWorkImage;
                    Image.renderPreview= element.renderPreview;
                    Image.renderPdf= element.renderPdf;
                    Image.site = siteElement.id;
                    Image.deletable = element.deletable;
                    Image.resetable = element.resetable;
                    Image.hideOnStart = element.hideOnStart;
                    Image.hideOnUserInput = element.hideOnUserInput;
                    Image.hideTransform = element.hideTransform;
                    Image.limitBoundary = element.limitBoundary;

                    if(element.value != "")
                    {
                        siteElement.addElement(Image);
                    }
                    if (element.type == "formmotivselect") {
                        Image.motive = element.motive;
                        Image.append = element.append;
                        $rootScope.$broadcast('general.addMotivSelect', Image);
                    }

                    if (element.selectable) {
                        $rootScope.$broadcast('wizard.addSelectableMotiv', Image);
                    }

                }

                if(element.type == "formsvgselect" || element.type == "svg") {
                    var Svg = new SiteElementSVG();
                    Svg.x = element.x;
                    Svg.y = element.y;
                    Svg.id = element.id;
                    Svg.label = element.label;
                    Svg.clipbox = element.clipbox;
                    Svg.altLabel = element.altLabel;
                    Svg.width = element.width;
                    Svg.height = element.height;
                    Svg.value = dataService.getRawPath(element.value);
                    Svg.data = element.data;
                    Svg.section = element.section;
                    Svg.pos = parseFloat(element.pos, 10);
                    Svg.renderPreview= element.renderPreview;
                    Svg.renderPdf= element.renderPdf;
                    Svg.fillColor = element.fillColor;
                    Svg.selectable = element.selectable;
                    Svg.hideOnStart = element.hideOnStart;
                    Svg.hideOnUserInput = element.hideOnUserInput;
                    Svg.deletable = element.deletable;
                    Svg.resetable = element.resetable;
                    Svg.limitBoundary = element.limitBoundary;
                    if(basketElement) {
                        match = basketElement.value.match(/[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/);
                        if(!match) {
                            match = basketElement.value.match(/[0-9a-f]{4}-[0-9a-f]{8}-[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{8}/);
                        }
                        Svg.value = dataService.getRawPath(match[0]);
                    }

                    if(basketEl) {
                        Svg.fill = basketEl.fill;
                    }

                    if(element.id != "background-color" && element.value != "") {
                        siteElement.addElement(Svg);
                    }

                    if(element.type == "formsvgselect") {
                        Svg.motive = element.motive;
                        Svg.append = element.append;
                        $rootScope.$broadcast('general.addSvgSelect', Svg);
                    }

                    if(element.section == "colors") {
                        siteElement.addSetting(Svg);
                        $rootScope.$broadcast('general.showProductSettingsButton');
                    }
                }

                if(element.type == "formrte") {
                    var Text = new SiteElementText();
                    Text.x = element.x;
                    Text.y = element.y;
                    Text.width = element.width;
                    Text.height = element.height;
                    Text.id = element.id;
                    Text.value = element.value;
                    Text.selectable = element.selectable;
                    Text.pos = parseFloat(element.pos, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontSize = parseFloat(element.fontSize, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontFamily = element.fontFamily;
                    Text.lineHeight = element.lineHeight;
                    Text.color = element.color;
                    Text.bold = element.bold;
                    Text.clipbox = element.clipbox;
                    Text.italic = element.italic;
                    Text.underline = element.underline;
                    Text.align = element.align;
                    Text.useWorkImage= element.useWorkImage;
                    Text.renderPreview= element.renderPreview;
                    Text.renderPdf= element.renderPdf;
                    Text.templatePrintBinding = element.templatePrintBinding;
                    Text.deletable = element.deletable;
                    Text.resetable = element.resetable;
                    if(basketSite && basketEl) {
                        Text.json = basketEl;
                        siteElement.addElement(Text);
                    }
                    if(!basketSite) {
                        siteElement.addElement(Text);
                    }
                }



                if(!basketSite && element.type == "formtextbox") {
                    var Text = new SiteElementTextbox();
                    Text.x = element.x;
                    Text.y = element.y;
                    Text.width = element.width;
                    Text.height = element.height;
                    Text.id = element.id;
                    Text.value = element.value;
                    Text.selectable = element.selectable;
                    Text.pos = parseFloat(element.pos, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontSize = parseFloat(element.fontSize, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontFamily = element.fontFamily;
                    Text.lineHeight = parseFloat(element.lineHeight, 10);
                    Text.color = element.color;
                    Text.bold = element.bold;
                    Text.clipbox = element.clipbox;
                    Text.italic = element.italic;
                    Text.underline = element.underline;
                    Text.align = element.align;
                    Text.useWorkImage= element.useWorkImage;
                    Text.renderPreview= element.renderPreview;
                    Text.renderPdf= element.renderPdf;
                    Text.templatePrintBinding = element.templatePrintBinding;
                    Text.deletable = element.deletable;
                    Text.resetable = element.resetable;
                    siteElement.addElement(Text);
                }

                if(!basketSite && element.type == "formitext") {
                    var Text = new SiteElementIText();
                    Text.x = element.x;
                    Text.y = element.y;
                    Text.width = element.width;
                    Text.height = element.height;
                    Text.id = element.id;
                    Text.value = element.value;
                    Text.selectable = element.selectable;
                    Text.pos = parseFloat(element.pos, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontSize = parseFloat(element.fontSize, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontFamily = element.fontFamily;
                    Text.lineHeight = parseFloat(element.lineHeight, 10);
                    Text.color = element.color;
                    Text.bold = element.bold;
                    Text.clipbox = element.clipbox;
                    Text.italic = element.italic;
                    Text.underline = element.underline;
                    Text.align = element.align;
                    Text.useWorkImage= element.useWorkImage;
                    Text.renderPreview= element.renderPreview;
                    Text.renderPdf= element.renderPdf;
                    Text.templatePrintBinding = element.templatePrintBinding;
                    Text.deletable = element.deletable;
                    Text.resetable = element.resetable;
                    siteElement.addElement(Text);
                }

                if(!basketSite && element.type == "formtextarea") {
                    var Text = new SiteElementText();
                    Text.x = element.x;
                    Text.y = element.y;
                    Text.width = element.width;
                    Text.height = element.height;
                    Text.id = element.id;
                    Text.value = element.value;
                    Text.selectable = element.selectable;
                    Text.pos = parseFloat(element.pos, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontSize = parseFloat(element.fontSize, 10);
                    Text.rotate = parseFloat(element.rotate, 10);
                    Text.fontFamily = element.fontFamily;
                    Text.lineHeight = parseFloat(element.lineHeight, 10);
                    Text.color = element.color;
                    Text.bold = element.bold;
                    Text.clipbox = element.clipbox;
                    Text.italic = element.italic;
                    Text.underline = element.underline;
                    Text.align = element.align;
                    Text.useWorkImage= element.useWorkImage;
                    Text.renderPreview= element.renderPreview;
                    Text.renderPdf= element.renderPdf;
                    Text.deletable = element.deletable;
                    Text.resetable = element.resetable;
                    siteElement.addElement(Text);
                }

                if(element.type == "calendar") {
                    var Cal = new SiteElementCal();
                    Cal.x = element.x;
                    Cal.y = element.y;
                    Cal.startMonth = 1;
                    Cal.startYear = element.calendarStartYear;
                    Cal.year = element.calendarStartYear;
                    Cal.width = element.width;
                    Cal.type = element.calendarType;
                    Cal.height = element.height;
                    Cal.id = element.id;
                    Cal.month = element.month;
                    Cal.calcMonth = element.month;
                    Cal.data = element.data;
                    Cal.monthSize = element.monthSize;
                    Cal.fontFamily = element.fontFamily;
                    Cal.daySize = element.daySize;
                    Cal.yearSize = element.yearSize;
                    Cal.dayColor = {value:element.dayColor};
                    Cal.monthColor = {value:element.monthColor};
                    Cal.sunColor = {value:element.sunColor};
                    Cal.renderPreview= element.renderPreview;
                    Cal.renderPdf= element.renderPdf;
                    Cal.designYear = element.designYear;
                    Cal.designHeightMonthCal = element.designHeightMonthCal;
                    Cal.designMarginCal = element.designMarginCal;
                    Cal.pos = parseFloat(element.pos, 10);
                    Cal.calendarX = parseFloat(element.calendarX, 10);
                    Cal.calendarY = parseFloat(element.calendarY, 10);
                    Cal.yearX = parseFloat(element.yearX, 10);
                    Cal.yearY = parseFloat(element.yearY, 10);
                    Cal.calendarWidth = parseFloat(element.calendarWidth, 10);
                    Cal.calendarHeight = parseFloat(element.calendarHeight, 10);
                    Cal.monthX = parseFloat(element.monthX, 10);
                    Cal.monthY = parseFloat(element.monthY, 10);
                    Cal.deletable = element.deletable;
                    Cal.resetable = element.resetable;
                    if(basketEl) {
                        var calSettings = basketEl.src.match("[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{1,4}\/[0-9]{1,6}\/[0-9]{1,6}\/[0-9]{1,3}\/[0-9]{1,3}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{1}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/.{1,40}")[0].split("/");
                        Cal.dayColor = {value: "#" + calSettings[8] };
                        Cal.monthColor = {value: "#" + calSettings[7] };
                        Cal.sunColor = {value: "#" + calSettings[9] };
                        Cal.fontFamily = calSettings[22];
                    }

                    siteElement.addElement(Cal);
                    siteElement.setCalendarSetting(Cal);
                    $rootScope.$broadcast('settings.showCalendarSettingsButton');
                    $rootScope.$broadcast('general.addCalendarSetting', Cal);


                }
            });
            if(basketSite && basketSite.saveJson) {
                var positions =[];

                angular.forEach(basketSite.saveJson.objects,function(element, ie){
                    if(element.id) return;

                    var layer = _.find(positions, function(elem){ return elem >= element.pos; });

                    if(layer) {
                        element.pos = layer+2;
                    }

                    if(!positions[element.pos]) {
                        positions[element.pos] = element.pos;
                    }else{
                        positions[element.pos]++;
                        element.pos = positions[element.pos];
                    }

                    if(element.type == "i-text") {
                        var Text = new SiteElementIText();
                        Text.x = element.left;
                        Text.y = element.top;
                        Text.width = element.width;
                        Text.height = element.height;
                        Text.id = element.id;
                        Text.value = element.value;
                        Text.selectable = element.selectable;
                        Text.pos = parseFloat(element.pos, 10);
                        Text.rotate = parseFloat(element.rotate, 10);
                        Text.fontSize = parseFloat(element.fontSize, 10);
                        Text.rotate = parseFloat(element.rotate, 10);
                        Text.fontFamily = element.fontFamily;
                        Text.lineHeight = parseFloat(element.lineHeight, 10);
                        Text.color = element.color;
                        Text.bold = element.bold;
                        Text.clipbox = element.clipbox;
                        Text.italic = element.italic;
                        Text.underline = element.underline;
                        Text.align = element.align;
                        Text.useWorkImage= element.useWorkImage;
                        Text.renderPreview= element.renderPreview;
                        Text.renderPdf= element.renderPdf;
                        Text.visible = true;
                        Text.json = element;
                        Text.deletable = element.deletable;
                        Text.resetable = element.resetable;
                        siteElement.addElement(Text);
                    }

                    if(element.type == "image") {
                        var Image = new SiteElementImage();
                        Image.x = element.left;
                        Image.y = element.top;
                        Image.id = element.id;
                        Image.width = element.width+2;
                        Image.height = element.height+2;
                        Image.label = element.label;

                        Image.value = element.src;

                        Image.selectable = element.selectable;
                        Image.pos = parseFloat(element.pos, 10);
                        Image.clipbox = element.clipbox;
                        Image.rotate = parseFloat(element.rotate, 10);
                        Image.useWorkImage= element.useWorkImage;
                        Image.renderPreview= element.renderPreview;
                        Image.limitBoundary = element.limitBoundary;
                        Image.renderPdf= element.renderPdf;
                        Image.visible = true;
                        Image.json = element;
                        Image.deletable = element.deletable;
                        Image.resetable = element.resetable;
                        siteElement.addElement(Image);
                        if(element.type == "formmotivselect") {
                            Image.motive = element.motive;
                            $rootScope.$broadcast('general.addMotivSelect', Image);
                        }
                    }

                    if(element.type == "image") {

                    }
                });
            }else if(basketSite && basketSite.fabricJson) {
                var positions =[];

                angular.forEach(basketSite.fabricJson.objects,function(element, ie){
                    if(element.id) return;

                    var layer = _.find(positions, function(elem){ return elem >= element.pos; });

                    if(layer) {
                        element.pos = layer+2;
                    }

                    if(!positions[element.pos]) {
                        positions[element.pos] = element.pos;
                    }else{
                        positions[element.pos]++;
                        element.pos = positions[element.pos];
                    }

                    if(element.type == "i-text") {
                        var Text = new SiteElementIText();
                        Text.x = element.x;
                        Text.y = element.y;
                        Text.width = element.width;
                        Text.height = element.height;
                        Text.id = element.id;
                        Text.value = element.value;
                        Text.selectable = element.selectable;
                        Text.pos = parseFloat(element.pos, 10);
                        Text.rotate = parseFloat(element.rotate, 10);
                        Text.fontSize = parseFloat(element.fontSize, 10);
                        Text.rotate = parseFloat(element.rotate, 10);
                        Text.fontFamily = element.fontFamily;
                        Text.lineHeight = parseFloat(element.lineHeight, 10);te
                        Text.color = element.color;
                        Text.bold = element.bold;
                        Text.clipbox = element.clipbox;
                        Text.italic = element.italic;
                        Text.underline = element.underline;
                        Text.align = element.align;
                        Text.useWorkImage= element.useWorkImage;
                        Text.renderPreview= element.renderPreview;
                        Text.renderPdf= element.renderPdf;
                        Text.visible = true;
                        Text.json = element;
                        Text.deletable = element.deletable;
                        Text.resetable = element.resetable;
                        siteElement.addElement(Text);
                    }

                    if(element.type == "image") {
                        var Image = new SiteElementImage();
                        Image.x = element.x;
                        Image.y = element.y;
                        Image.id = element.id;
                        Image.width = element.width+2;
                        Image.height = element.height+2;
                        Image.label = element.label;

                        Image.value = element.src;
                        Image.limitBoundary = element.limitBoundary;

                        Image.selectable = element.selectable;
                        Image.pos = parseFloat(element.pos, 10);
                        Image.clipbox = element.clipbox;
                        Image.rotate = parseFloat(element.rotate, 10);
                        Image.useWorkImage= element.useWorkImage;
                        Image.renderPreview= element.renderPreview;
                        Image.renderPdf= element.renderPdf;
                        Image.visible = true;
                        Image.json = element;
                        Image.deletable = element.deletable;
                        Image.resetable = element.resetable;
                        siteElement.addElement(Image);
                        if(element.type == "formmotivselect") {
                            Image.motive = element.motive;
                            $rootScope.$broadcast('general.addMotivSelect', Image);
                        }
                    }

                    if(element.type == "image") {

                    }
                });
            };

            painterManager.createLoaded(siteElement);
            cfpLoadingBar.inc();

        });
        if(data.data.sites_layouts) {
            sitesManager.layouts = data.data.sites_layouts;
        }
        if(data.data.steps) {
            wizardManager.steps = data.data.steps;
        }
        if(data.data.general.buttons.wizard) {
            $rootScope.$broadcast('wizard.showWizard');
        }

        $analytics.eventTrack('completeLoaded');
    });
})
;
