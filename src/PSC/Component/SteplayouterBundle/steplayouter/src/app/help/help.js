angular.module( 'layouterApp.help', [
  'colorpicker.module'
])

.config(function config( ) {
})

.controller( 'HelpCtrl', function SettingsController( $scope, $uibModal, $rootScope, painterManager, dataService ) {
    $scope.showHelpButton = false;
    $scope.helpValue = false;

    $scope.$on('settings.load', function($scope, obj) {
        if(obj.general.buttons.help) {
            $scope.currentScope.showHelpButton = true;
        }else{
            $scope.currentScope.showHelpButton = false;
        }
        if(obj.general.buttons.help && obj.general.buttons.help.value) {
            $scope.currentScope.helpValue = obj.general.buttons.help.value;
        }


    });

    $scope.openHelpModal = function() {

        var helpCtl = $scope;

        var modalInstance = $uibModal.open({
            templateUrl: 'helpModal',
            backdrop: true,
            windowClass: 'modal fade',

            controller: function ($scope, $uibModalInstance) {

                $scope.url = dataService.getImageMidPath(helpCtl.helpValue);

                $scope.cancel = function () {
                    $uibModalInstance.dismiss('cancel');
                };
            }
        });
    };


});