<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Config\Converter;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Document\Install;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Color;
use Symfony\Component\CssSelector\Parser\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * ProductController für SteplayouterBundle
 *
 * @package    PSC\Component\Steplayouter
 * @subpackage Controller
 */
class ToArray
{
    /** @var AuthorizationChecker  */
    protected $authChecker = null;
    /** @var TokenStorage */
    protected $tokenStorage = null;
    protected $colorService = null;
    protected $mongoDb = null;


    protected $_sites = array();
    protected $_layers = array();
    protected $_datatable = array();
    protected $_version = 1.4;

    /**
     * @var Product
     */
    protected $_product = null;

    public function __construct(Color $colorService, AuthorizationCheckerInterface $authChecker, TokenStorageInterface $tokenStorage, DocumentManager $mongoDb)
    {
        $this->colorService = $colorService;
        $this->authChecker = $authChecker;
        $this->tokenStorage = $tokenStorage;
        $this->mongoDb = $mongoDb;
    }

    public function fromXml(Product $product, $datatable = false)
    {
        $this->_datatable = $datatable;
        $this->_product = $product;
        $conf = simplexml_load_string($product->getSteplayouterXml());
        $this->_version = (float) $conf->version;
        $data = array(
            'version' => (float) $conf->version,
            'general' => array('buttons' => array(), 'steps' => 'header'),
            'access' => array("saveProduct" => false, "printPdf" => false),
            'title' => $product->getTitle(),
            'help' => '',
            'sites' => array(),
            'sites_layouts' => array(),
            'pdfPrintMerge' => false,
            'steps' => array(),
            'loaded' => array(),
            'fonts' => array(
                array('id' => 'Helvetica', 'name' => 'Helvetica', 'rte' => "Helvetica")
            )
        );

        if (isset($_SERVER["SERVER_NAME"]) || isset($_SERVER['HTTP_HOST'])) {
            if ($_SERVER["SERVER_NAME"] == '') {
                if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
                    $data['server'] = 'https://' . $_SERVER['HTTP_HOST'];
                } else {
                    $data['server'] = 'http://' . $_SERVER['HTTP_HOST'];
                }
            } else {
                if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
                    $data['server'] = 'https://' . $_SERVER["SERVER_NAME"];
                } else {
                    $data['server'] = 'http://' . $_SERVER["SERVER_NAME"];
                }
            }
        }
        $data['session'] = session_id();


        if ($this->tokenStorage->getToken() && $this->authChecker->isGranted('ROLE_SHOP')) {
            $data['access']['saveProduct'] = true;
            $data['access']['printPdf'] = true;
        }

        if (isset($conf->general->help)) {
            $data['help'] = (string)$conf->general->help;
        }

        foreach ($conf->general->buttons->children() as $key => $button) {
            $btn = array('id' => $key, 'class' => '');

            if (isset($button['class'])) {
                $btn['cssClass'] = (string) $button['class'];
            }
            if (isset($button['label'])) {
                $btn['label'] = (string) $button['label'];
            }
            if (isset($button['value'])) {
                $btn['value'] = (string) $button['value'];
            }
            $data['general']['buttons'][$key] = $btn;
        }

        if ($conf->rendering->site_layouts) {
            foreach ($conf->rendering->site_layouts->children() as $site) {
                $tmp = array();
                $tmp['id'] = (int) $site['id'];
                $tmp['vorlageSite'] = (int) $site['vorlage-site'];

                $data['sites_layouts'][] = $tmp;
            }
        }

        if (isset($conf->rendering->sites['pdfPrintMerge'])) {
            $data['pdfPrintMerge'] = array(
                'sites' => explode(",", (string) $conf->rendering->sites['pdfPrintMerge']),
                'width' => (string) $conf->rendering->sites['pdfPrintMergeSizeWidth'],
                'height' => (string) $conf->rendering->sites['pdfPrintMergeSizeHeight']
            );

            if (isset($conf->rendering->sites['pdfPrintMergeSizeStanze'])) {
                $data['pdfPrintMerge']['stanze'] = (string) $conf->rendering->sites['pdfPrintMergeSizeStanze'];
            }
        }

        foreach ($conf->rendering->sites->children() as $site) {
            $tmp = array();
            $tmp['id'] = (int) $site['id'];
            $tmp['width'] = (int) $site['width'];
            $tmp['maxWidth'] = (int) $site['width'];
            $tmp['height'] = (int) $site['height'];
            $tmp['maxHeight'] = (int) $site['height'];
            $tmp['passepartout'] = false;
            $tmp['passepartoutX'] = false;
            $tmp['passepartoutY'] = false;
            $tmp['passepartoutWidth'] = false;
            $tmp['passepartoutHeight'] = false;
            $tmp['renderPrintCutting'] = false;
            $tmp['renderPrintMedia'] = false;
            $tmp['trimLeft'] = 0;
            $tmp['mergePosX'] = 0;
            $tmp['mergePosY'] = 0;
            $tmp['trimRight'] = 0;
            $tmp['trimTop'] = 0;
            $tmp['trimBottom'] = 0;
            $tmp['calendar'] = false;
            $tmp['clipBox'] = false;
            $tmp['layoutable'] = true;
            $tmp['showsettings'] = false;
            $tmp['backgroundColor'] = "#ffffff";
            $tmp['title'] = "Ansicht";
            $tmp['maxZoom'] = 2;
            if (isset($site['mergePosX'])) {
                $tmp['mergePosX'] = (string) $site['mergePosX'];
            }
            if (isset($site['max-zoom'])) {
                $tmp['maxZoom'] = floatval($site['max-zoom']);
            }
            if (isset($site['mergePosY'])) {
                $tmp['mergePosY'] = (string) $site['mergePosY'];
            }
            if (isset($site['passepartout'])) {
                $tmp['passepartout'] = (string) $site['passepartout'];
            }
            if (isset($site['clipBox'])) {
                $tmp['clipBox'] = (string) $site['clipBox'];
            }
            if (isset($site['layoutable'])) {
                $tmp['layoutable'] =  (bool) intval($site['layoutable']);
            }
            if (isset($site['showsettings'])) {
                $tmp['showsettings'] = (bool) intval($site['showsettings']);
            }
            if (isset($site['trim-left'])) {
                $tmp['trimLeft'] = (string) $site['trim-left'];
            }
            if (isset($site['trim-right'])) {
                $tmp['trimRight'] = (string) $site['trim-right'];
            }
            if (isset($site['trim-top'])) {
                $tmp['trimTop'] = (string) $site['trim-top'];
            }
            if (isset($site['trim-bottom'])) {
                $tmp['trimBottom'] = (string) $site['trim-bottom'];
            }
            if (isset($site['passepartout-x'])) {
                $tmp['passepartoutX'] = (string) $site['passepartout-x'];
            }
            if (isset($site['passepartout-y'])) {
                $tmp['passepartoutY'] = (string) $site['passepartout-y'];
            }
            if (isset($site['passepartout-width'])) {
                $tmp['passepartoutWidth'] = (string) $site['passepartout-width'];
            }
            if (isset($site['passepartout-height'])) {
                $tmp['passepartoutHeight'] = (string) $site['passepartout-height'];
            }
            if (isset($site['render-print-cutting'])) {
                $tmp['renderPrintCutting'] = intval($site['render-print-cutting']);
            }
            if (isset($site['render-print-media'])) {
                $tmp['renderPrintMedia'] = intval($site['render-print-media']);
            }
            if (isset($site['background-color'])) {
                $tmp['backgroundColor'] = (string) $site['background-color'];
            }
            if (isset($site['title'])) {
                $tmp['title'] = (string) $site['title'];
            }
            $tmp['settingsDefault'] = array();
            if ($site->settings && $site->settings->createText) {
                if ($site->settings->createText['colorContainer']) {
                    $tmp['settingsDefault']['createText'] = array(
                        'pos' => (int) $site->settings->createText['sort'],
                        'colors' => $this->colorService->getColorsForArea((string) $site->settings->createText['colorContainer'], $product),
                        'clipBox' => false,
                        'offset' => false
                    );
                } else {
                    $tmp['settingsDefault']['createText'] = array(
                        'pos' => (int) $site->settings->createText['sort'],
                        'colors' => $this->colorService->getColorsForArea((string) $site->settings->createText['color-container'], $product),
                        'clipBox' => false,
                        'offset' => false
                    );
                }

                if (isset($site->settings->createText['clipBox'])) {
                    $tmp['settingsDefault']['createText']['clipBox'] = explode(" ", $site->settings->createText['clipBox']);
                }
                if (isset($site->settings->createText['offset'])) {
                    $tmp['settingsDefault']['createText']['offset'] = explode(" ", $site->settings->createText['offset']);
                }
            } else {
                $tmp['settingsDefault']['createText'] = array(
                    'pos' => 100,
                    'colors' => $this->colorService->getColorsForArea(null, $product),
                    'clipBox' => false,
                    'offset' => false
                );
            }

            if ($site->settings && $site->settings->createMotiv) {
                $tmp['settingsDefault']['createMotiv'] = array(
                    'pos' => (int) $site->settings->createMotiv['sort'],
                    'clipBox' => false,
                    'offset' => false
                );
                if (isset($site->settings->createMotiv['clipBox'])) {
                    $tmp['settingsDefault']['createMotiv']['clipBox'] = explode(" ", $site->settings->createMotiv['clipBox']);
                }
                if (isset($site->settings->createMotiv['offset'])) {
                    $tmp['settingsDefault']['createMotiv']['offset'] = explode(" ", $site->settings->createMotiv['offset']);
                }
            } else {
                $tmp['settingsDefault']['createMotiv'] = array(
                    'pos' => 100,
                    'clipBox' => false,
                    'offset' => false
                );
            }

            if ($site->settings && $site->settings->createClipart) {
                $tmp['settingsDefault']['createClipart'] = array(
                    'pos' => (int) $site->settings->createClipart['sort'],
                    'clipBox' => false,
                    'offset' => false
                );
                if (isset($site->settings->createClipart['clipBox'])) {
                    $tmp['settingsDefault']['createClipart']['clipBox'] = explode(" ", $site->settings->createClipart['clipBox']);
                }
                if (isset($site->settings->createClipart['offset'])) {
                    $tmp['settingsDefault']['createClipart']['offset'] = explode(" ", $site->settings->createClipart['offset']);
                }
            } else {
                $tmp['settingsDefault']['createClipart'] = array(
                    'pos' => 700,
                    'clipBox' => false,
                    'offset' => false
                );
            }

            $tmp['elements'] = array();
            if ($site->elements) {
                foreach ($site->elements->children() as $key => $element) {
                    $temp = array(
                        'type' => $key,
                        'width' => ((int) $element['width'] + (int) $element['render-pos-width']),
                        'height' => ((int) $element['height'] + (int) $element['render-pos-height']),
                        'x' => (int) $element['x'],
                        'y' => (int) $element['y'],
                        'pos' => count($tmp['elements']),
                        'rotate' => 0,
                        'value' => (string) $element['value'],
                        'selectable' => false,
                        'deletable' => true,
                        'resetable' => false,
                        'label' => false,
                        'altLabel' => false,
                        'mode' => false,
                        'section' => false,
                        'id' => '',
                        'data' => false,
                        'designYear' => 0,
                        'designHeightMonthCal' => 0,
                        'designMarginCal' => 0,
                        'calendarX' => 0,
                        'calendarY' => 0,
                        'calendarStartYear' => date('Y') + 1,
                        'yearX' => 0,
                        'yearY' => 0,
                        'calendarWidth' => 0,
                        'calendarHeight' => 0,
                        'monthX' => 0,
                        'monthY' => 0,
                        'dayColor' => '#000000',
                        'monthColor' => '#000000',
                        'sunColor' => '#000000',
                        'yearSize' => 14,
                        'renderPreview' => 1,
                        'renderPdf' => 1,
                        'limitBoundary' => false,
                        'fillColor' => false,
                        'hideOnStart' => false,
                        'hideOnUserInput' => false,
                        'hideTransform' => false,
                        'clipbox' => false,
                        'passepartout' => false,
                        'passepartoutX' => false,
                        'passepartoutY' => false,
                        'passepartoutWidth' => false,
                        'passepartoutHeight' => false,
                        'passepartoutRel' => 'element',
                        'templatePrintBinding' => false,
                    );

                    /** @var Install $install */
                    $install = $this->mongoDb
                        ->getRepository('PSC\Shop\EntityBundle\Document\Install')
                        ->findOneBy(array('installId' => (string) $product->getShop()->getInstall()->getUid()));

                    if ($install) {
                        $temp['calendarStartYear'] = $install->getCalendarStartYear();
                    }

                    if ($key == 'calendar') {
                        $tmp['calendar'] = $temp;
                    }

                    if ($key == 'barcode') {
                        $temp['rotate'] = -90;
                        $temp['addText'] = "";
                        if (isset($element['addText'])) {
                            $temp['addText'] = (string) $element['addText'];
                        }
                        if (isset($element['addTextX'])) {
                            $temp['addTextX'] = (string) $element['addTextX'];
                        }
                        if (isset($element['addTextY'])) {
                            $temp['addTextY'] = (string) $element['addTextY'];
                        }
                    }

                    if (trim((string) $element) != "") {
                        $temp['value'] = trim(strip_tags(str_replace(array("<br>", "</br>"), array("\n", "\n"), (string) $element)));
                    } else {
                        $temp['value'] = trim(strip_tags(str_replace(array("<br>", "</br>"), array("\n", "\n"), (string) $element['value'])));
                    }
                    if (isset($element['fillColor'])) {
                        $temp['fillColor'] = (string) $element['fillColor'];
                    }
                    if (isset($element['hideOnStart'])) {
                        $temp['hideOnStart'] = (int) $element['hideOnStart'];
                    }
                    if (isset($element['hideOnUserInput'])) {
                        $temp['hideOnUserInput'] = (int) $element['hideOnUserInput'];
                    }
                    if (isset($element['hideTransform'])) {
                        $temp['hideTransform'] = (int) $element['hideTransform'];
                    }
                    if ($temp['width'] > $tmp['maxWidth']) {
                        $tmp['maxWidth'] = $tmp['width'] + (($temp['width'] - $tmp['width']) * 2);
                    }
                    if ($temp['height'] > $tmp['maxHeight']) {
                        $tmp['maxHeight'] = $temp['height'];
                    }
                    if (isset($element['selectable'])) {
                        $temp['selectable'] = intval($element['selectable']);
                    }
                    if (isset($element['deletable'])) {
                        $temp['deletable'] = intval($element['deletable']);
                    }
                    if (isset($element['resetable'])) {
                        $temp['resetable'] = (string)$element['resetable'];
                    }
                    if (isset($element['templatePrintBinding'])) {
                        $temp['templatePrintBinding'] = (string) $element['templatePrintBinding'];
                    }
                    if (isset($element['clipbox'])) {
                        $temp['clipbox'] = explode(" ", (string) $element['clipbox']);
                    }
                    if (isset($element['rotate'])) {
                        $temp['rotate'] = (string) $element['rotate'];
                    }
                    if (isset($element['clipBox'])) {
                        $temp['clipbox'] = explode(" ", (string) $element['clipBox']);
                    }
                    if (isset($element['passepartout'])) {
                        $temp['passepartout'] = (string) $element['passepartout'];
                    }
                    if (isset($element['passepartout-x'])) {
                        $temp['passepartoutX'] = (string) $element['passepartout-x'];
                    }
                    if (isset($element['passepartout-y'])) {
                        $temp['passepartoutY'] = (string) $element['passepartout-y'];
                    }
                    if (isset($element['passepartout-height'])) {
                        $temp['passepartoutHeight'] = (string) $element['passepartout-height'];
                    }
                    if (isset($element['passepartout-rel'])) {
                        $temp['passepartoutRel'] = (string) $element['passepartout-rel'];
                    }
                    if (isset($element['passepartout-width'])) {
                        $temp['passepartoutWidth'] = (string) $element['passepartout-width'];
                    }
                    if (isset($element['label'])) {
                        $temp['label'] = (string) $element['label'];
                    }
                    if (isset($element['altLabel'])) {
                        $temp['altLabel'] = (string) $element['altLabel'];
                    }
                    if (isset($element['id'])) {
                        $temp['id'] = (string) $element['id'];
                    }
                    if (isset($element['mode'])) {
                        $temp['mode'] = (string) $element['mode'];
                    }
                    if (isset($element['render-pos-sort']) && (int) $element['render-pos-sort']) {
                        $temp['pos'] = (int) $element['render-pos-sort'];
                    } else {
                        $temp['pos'] = 1;
                    }
                    if (isset($element['render-font-size']) && (int) $element['render-font-size']) {
                        $temp['fontSize'] = (int) $element['render-font-size'];
                    } else {
                        $temp['fontSize'] = 18;
                    }
                    if (isset($element['render-font-color']) && (string) $element['render-font-color']) {
                        $temp['color'] = (string) $element['render-font-color'];
                    } else {
                        $temp['color'] = "#000000";
                    }
                    if (isset($element['calendar-month'])) {
                        $temp['month'] = (int) $element['calendar-month'];
                    }
                    if (isset($element['design-year'])) {
                        $temp['designYear'] = (int) $element['design-year'];
                    }
                    if (isset($element['design-height-month-cal'])) {
                        $temp['designHeightMonthCal'] = (int) $element['design-height-month-cal'];
                    }
                    if (isset($element['design-margin-cal'])) {
                        $temp['designMarginCal'] = (int) $element['design-margin-cal'];
                    }
                    if (isset($element['calendar-x'])) {
                        $temp['calendarX'] = (int) $element['calendar-x'];
                    }
                    if (isset($element['calendar-y'])) {
                        $temp['calendarY'] = (int) $element['calendar-y'];
                    }
                    if (isset($element['year-x'])) {
                        $temp['yearX'] = (int) $element['year-x'];
                    }
                    if (isset($element['year-y'])) {
                        $temp['yearY'] = (int) $element['year-y'];
                    }
                    if (isset($element['calendar-width'])) {
                        $temp['calendarWidth'] = (int) $element['calendar-width'];
                    }
                    if (isset($element['calendar-height'])) {
                        $temp['calendarHeight'] = (int) $element['calendar-height'];
                    }
                    if (isset($element['month-x'])) {
                        $temp['monthX'] = (int) $element['month-x'];
                    }
                    if (isset($element['month-y'])) {
                        $temp['monthY'] = (int) $element['month-y'];
                    }
                    if (isset($element['calendar-template'])) {
                        $temp['calendarType'] = (int) $element['calendar-template'];
                    }
                    if (isset($element['calendar-monthsize'])) {
                        $temp['monthSize'] = (int) $element['calendar-monthsize'];
                    }
                    if (isset($element['calendar-daysize'])) {
                        $temp['daySize'] = (int) $element['calendar-daysize'];
                    }
                    if (isset($element['calendar-daycolor'])) {
                        $temp['dayColor'] = (string) $element['calendar-daycolor'];
                    }
                    if (isset($element['calendar-monthcolor'])) {
                        $temp['monthColor'] = (string) $element['calendar-monthcolor'];
                    }
                    if (isset($element['calendar-suncolor'])) {
                        $temp['sunColor'] = (string) $element['calendar-suncolor'];
                    }
                    if (isset($element['calendar-yearsize'])) {
                        $temp['yearSize'] = (int) $element['calendar-yearsize'];
                    }
                    if (isset($element['render-preview'])) {
                        $temp['renderPreview'] = (bool) (int) $element['render-preview'];
                    }
                    if (isset($element['render-pdf'])) {
                        $temp['renderPdf'] = (bool) (int) $element['render-pdf'];
                    }
                    if (isset($element['section'])) {
                        $temp['section'] = (string) $element['section'];
                        if ($temp['section'] == 'colors') {
                            $temp['data'] = $this->colorService->getColorsForArea((string) $element['container'], $product);
                        }
                    }
                    if (isset($element['render-font-bold']) && (int) $element['render-font-bold']) {
                        $temp['bold'] = (int) $element['render-font-bold'];
                    } else {
                        $temp['bold'] = false;
                    }

                    if (isset($element['render-font-italic']) && (int) $element['render-font-italic']) {
                        $temp['italic'] = (int) $element['render-font-italic'];
                    } else {
                        $temp['italic'] = false;
                    }

                    if (isset($element['render-font-align']) && (string) $element['render-font-align']) {
                        $temp['align'] = (string) $element['render-font-align'];
                    } else {
                        $temp['align'] = "left";
                    }

                    if (isset($element['render-font-underline']) && (int) $element['render-font-underline']) {
                        $temp['underline'] = (int) $element['render-font-underline'];
                    } else {
                        $temp['underline'] = false;
                    }
                    if (isset($element['limitBoundary']) && (int) $element['limitBoundary']) {
                        $temp['limitBoundary'] = boolval($element['limitBoundary']);
                    } else {
                        $temp['limitBoundary'] = false;
                    }

                    if (isset($element['line-height']) && floatval($element['line-height'])) {
                        $temp['lineHeight'] = floatval($element['line-height']);
                    } else {
                        $temp['lineHeight'] = 1;
                    }

                    if (isset($element['use-work-image']) && (int) $element['use-work-image']) {
                        $temp['useWorkImage'] = (int) $element['use-work-image'];
                    } else {
                        $temp['useWorkImage'] = false;
                    }
                    if (!isset($element['use-work-image'])) {
                        if (isset($element['useWorkImage']) && (int) $element['useWorkImage']) {
                            $temp['useWorkImage'] = (int) $element['useWorkImage'];
                        } else {
                            $temp['useWorkImage'] = false;
                        }
                    }
                    if (isset($element['render-font-family']) && (string) $element['render-font-family']) {
                        $temp['fontFamily'] = (string) $element['render-font-family'];
                    } else {
                        $temp['fontFamily'] = "Verdana";
                    }

                    if ($key == 'formmotivselect' || $key == 'formsvgselect') {
                        $temp['append'] = false;
                        if (isset($element['append'])) {
                            $temp['append'] = true;
                        }
                        $temp['motive'] = array();
                        if (isset($element->options)) {
                            foreach ($element->options->children() as $option) {
                                $temp['motive'][] = array(
                                    'uuid' => (string)$option['motiv'],
                                    'label' => (string)$option['label']
                                );
                            }
                        }
                        if (isset($element['container'])) {
                            $container = (string)$element['container'];

                            $container = explode(",", trim($container));

                            foreach ($container as $cont) {
                                $group = $this->mongoDb
                                    ->getRepository('PSC\Shop\EntityBundle\Document\MotivGroup')
                                    ->findOneBy(['url' => $cont]);

                                $motive = simplexml_load_string($group->getMotive());
                                if (isset($motive)) {
                                    foreach ($motive->children() as $option) {
                                        $temp['motive'][] = array(
                                            'uuid' => (string)$option['motiv'],
                                            'label' => (string)$option['label']
                                        );
                                    }
                                }
                            }
                        }
                    }

                    $tmp['elements'][] = $temp;
                }
            }

            if (isset($tmpElementsArray[(int) $site['id']])) {
                foreach ($tmpElementsArray[$key] as $element) {
                    $tmp['elements'][] = $element;
                }
            }

            $this->_sites[(int) $site['id']] = $tmp;
        }
        if ($conf->rendering->layers) {
            foreach ($conf->rendering->layers->children() as $layer) {
                $tmp = array();
                $tmp['id'] = (int) $layer['id'];
                $tmp['rotate'] = (int) $layer['rotate'];
                $tmp['scaleable'] = (int) $layer['scaleable'];
                $tmp['clipbox'] = (string) $layer['clipbox'];

                $this->_layers[(int) $layer['id']] = $tmp;
            }
        }
        if (isset($conf->steps)) {
            foreach ($conf->steps->children() as $step) {
                $st = $this->getStep($step);

                $data['steps'][] = $st;
            }
        }

        $data['sites'] = $this->_sites;

        //unset($data['steps']);

        return $data;
    }

    protected function getStep($step)
    {
        $st = array();

        $st['id'] = (int) $step['id'];
        $st['label'] = (string) $step['label'];
        $st['headline'] = (string) $step->headline;
        if ($step->step_headline) {
            $st['step_headline'] = (string) $step->step_headline;
        } else {
            $st['step_headline'] = "";
        }

        $st['description'] = (string) $step->description;
        $st['require'] = 0;
        $st['isDisplayed'] = false;
        if (isset($step['require']) && (int) $step['require'] == 1) {
            $st['require'] = 1;
        }

        if (isset($step['layout']) && (string) $step['layout']) {
            $st['layout'] = (string) $step['layout'];
            if (isset($step['columns']) && (string) $step['columns']) {
                $st['columns'] = (string) $step['columns'];
            } else {
                $st['columns'] = '2';
            }
        } else {
            $st['layout'] = 'normal';
            $st['columns'] = '1';
        }

        if ($this->_version > 1.4) {
            $st['elements'] = $this->getElements($step);
        } else {
            $st['elements'] = $this->getOldElements($step);
        }
        return $st;
    }

    protected function getElements($step)
    {
        $elements = array();

        foreach ($step->elements->children() as $keyelement => $element) {
            $el = array();
            $el['id'] = (string) $element['id'];
            $el['type'] = (string) $keyelement;

            $el['bind'] = (string) $element['bind'];
            $el['label'] = (string) $element['label'];
            $el['value'] = (string) $element['value'];


            $elements[] = $el;
        }

        return $elements;
    }

    protected function getOldElements($step)
    {
        $elements = array();

        foreach ($step->elements->children() as $keyelement => $element) {
            $el = array();
            $el['id'] = (string) $element['id'];
            $el['type'] = (string) $keyelement;
            $el['selectable'] = true;
            $el['deletable'] = true;
            $el['renderPreview'] = true;
            $el['renderPdf'] = true;
            $el['section'] = false;
            $el['hideOnStart'] = false;
            $el['hideOnUserInput'] = false;
            $el['hideTransform'] = false;
            $el['fillColor'] = false;


            if (trim((string) $element) != "") {
                $el['value'] = trim(strip_tags(str_replace(array("<br>", "</br>"), array("\n", "\n"), (string) $element)));
            } else {
                $el['value'] = trim(strip_tags(str_replace(array("<br>", "</br>"), array("\n", "\n"), (string) $element['value'])));
            }
            if (isset($element['hideOnStart'])) {
                $el['hideOnStart'] = (int) $element['hideOnStart'];
            }
            if (isset($element['hideOnUserInput'])) {
                $el['hideOnUserInput'] = (int) $element['hideOnUserInput'];
            }
            if (isset($element['hideTransform'])) {
                $el['hideTransform'] = (int) $element['hideTransform'];
            }
            if (isset($element['fillColor'])) {
                $el['fillColor'] = (string) $element['fillColor'];
            }
            $el['label'] = (string) $element['label'];
            $el['altLabel'] = false;
            if (isset($element['altLabel'])) {
                $el['altLabel'] = (string) $element['altLabel'];
            }
            if (isset($element['section'])) {
                $el['section'] = (string) $element['section'];
                if ($el['section'] == 'colors') {
                    $el['data'] = $this->colorService->getColorsForArea((string) $element['container'], $this->_product);
                }
            }

            if (isset($element['width']) && (int) $element['width']) {
                $el['width'] = (int) $element['width'];
            }
            if (!isset($el['width']) && isset($element['render-pos-width']) && (int) $element['render-pos-width']) {
                $el['width'] = (int) $element['render-pos-width'];
            }
            if (isset($element['render-site']) && (string) $element['render-site']) {
                $el['rendersite'] = (string) $element['render-site'];
            }

            if (isset($element['text']) && (string) $element['text']) {
                $el['text'] = (string) $element['text'];
            }

            if (isset($element['height']) && (int) $element['height']) {
                $el['height'] = (int) $element['height'];
            }
            if (!isset($el['height']) && isset($element['render-pos-height']) && (int) $element['render-pos-height']) {
                $el['height'] = (int) $element['render-pos-height'];
            }

            if (isset($element['calendar-month'])) {
                $el['month'] = (int) $element['calendar-month'];
            }

            if (isset($element['render-preview'])) {
                $el['renderPreview'] = (bool) (int) $element['render-preview'];
            }

            if (isset($element['render-pdf'])) {
                $el['renderPdf'] = (bool) (int) $element['render-pdf'];
            }

            if (isset($element['render-pos-x']) && (int) $element['render-pos-x']) {
                $el['x'] = (int) $element['render-pos-x'];
            } else {
                $el['x'] = 0;
            }

            if (isset($element['render-font-size']) && (int) $element['render-font-size']) {
                $el['fontSize'] = (int) $element['render-font-size'];
            } else {
                $el['fontSize'] = 18;
            }

            if (isset($element['render-font-color']) && (string) $element['render-font-color']) {
                $el['color'] = (string) $element['render-font-color'];
            } else {
                $el['color'] = "#000000";
            }

            if (isset($element['render-font-bold']) && (int) $element['render-font-bold']) {
                $el['bold'] = (int) $element['render-font-bold'];
            } else {
                $el['bold'] = false;
            }

            if (isset($element['render-font-italic']) && (int) $element['render-font-italic']) {
                $el['italic'] = (int) $element['render-font-italic'];
            } else {
                $el['italic'] = false;
            }

            if (isset($element['render-font-align']) && (string) $element['render-font-align']) {
                $el['align'] = (string) $element['render-font-align'];
            } else {
                $el['align'] = "left";
            }

            if (isset($element['render-font-underline']) && (int) $element['render-font-underline']) {
                $el['underline'] = (int) $element['render-font-underline'];
            } else {
                $el['underline'] = false;
            }

            if (isset($element['line-height']) && floatval($element['line-height'])) {
                $el['lineHeight'] = floatval($element['line-height']);
            } else {
                $el['lineHeight'] = 1;
            }

            if (isset($element['use-work-image']) && (int) $element['use-work-image']) {
                $el['useWorkImage'] = (int) $element['use-work-image'];
            } else {
                $el['useWorkImage'] = false;
            }

            if (isset($element['render-font-family']) && (string) $element['render-font-family']) {
                $el['fontFamily'] = (string) $element['render-font-family'];
            } else {
                $el['fontFamily'] = "Verdana";
            }

            if (isset($element['render-pos-y']) && (int) $element['render-pos-y']) {
                $el['y'] = (int) $element['render-pos-y'];
            } else {
                $el['y'] = 0;
            }

            if (isset($element['render-pos-sort']) && (int) $element['render-pos-sort']) {
                $el['pos'] = (int) $element['render-pos-sort'];
            } else {
                $el['pos'] = 1;
            }

            if (isset($this->_layers[(int) $element['render-pos-layer']]['rotate']) && (int) $this->_layers[(int) $element['render-pos-layer']]['rotate']) {
                $el['rotate'] = (int) $this->_layers[(int) $element['render-pos-layer']]['rotate'];
            } else {
                $el['rotate'] = 0;
            }

            if (
                isset($this->_layers[(int) $element['render-pos-layer']]['dragable']) && (int) $this->_layers[(int) $element['render-pos-layer']]['dragable'] ||
                isset($this->_layers[(int) $element['render-pos-layer']]['scaleable']) && (int) $this->_layers[(int) $element['render-pos-layer']]['scaleable'] ||
                isset($this->_layers[(int) $element['render-pos-layer']]['rotateable']) && (int) $this->_layers[(int) $element['render-pos-layer']]['rotateable']
            ) {
                $el['selectable'] = true;
            } else {
                $el['selectable'] = false;
            }

            if (isset($this->_layers[(int) $element['render-pos-layer']]['clipbox']) && (string) $this->_layers[(int) $element['render-pos-layer']]['clipbox']) {
                $el['clipbox'] = explode(" ", (string) $this->_layers[(int) $element['render-pos-layer']]['clipbox']);
            } else {
                $el['clipbox'] = false;
            }

            if ($el['id'] == 'substeps') {
                $el['steps'] = array();
                foreach ($element->steps->children() as $substep) {
                    $el['steps'][] = $this->getStep($substep);
                }
            }

            if ($el['type'] == 'datatable') {
                $el['columns'] = array();
                foreach ($element->columns->children() as $option) {
                    $el['columns'][] = array(
                        'id' => (string) $option['id'],
                        'label' => (string) $option
                    );
                }

                $el['rows'] = array();
                if ($this->_datatable) {
                    $el['rows'] = $this->_datatable;
                } else {
                    foreach ($element->rows->children() as $option) {
                        $temp = array();
                        foreach ($option->children() as $key => $child) {
                            $temp[$key] = (string) $child;
                        }
                        $el['rows'][] = $temp;
                    }
                }
            }

            if ($el['type'] == 'formmotivselect' || $el['type'] == 'formsvgselect') {
                $el['append'] = false;
                if (isset($element['append'])) {
                    $el['append'] = true;
                }
                $el['motive'] = array();
                foreach ($element->options->children() as $option) {
                    $el['motive'][] = array(
                        'uuid' => (string) $option['motiv'],
                        'label' => (string) $option['label']
                    );
                }
            }

            if (isset($element['bind'])) {
                $el = array();
                $el['id'] = (string) $element['id'];
                $el['type'] = (string) $keyelement;

                $el['bind'] = (string) $element['bind'];
                $el['label'] = (string) $element['label'];
                $el['value'] = (string) $element['value'];

                $elements[] = $el;

                continue;
            }

            $elements[] = $el;

            if ($el['type'] == "setting" || $el['type'] == "formuploadmotiv" || $el['type'] == "formtextarea" || $el['type'] == "formrte" || $el['type'] == "formmotivselect" || $el['type'] == "formsvgselect") {
                $this->_sites[$el['rendersite']]['elements'][] = $el;
            }
        }

        return $elements;
    }
}
