<?php

namespace PSC\Component\SteplayouterBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;

class ConfigureMenuListener
{
    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureSystem(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();

        $menu->addChild('Creativ Layouter', array(
            'route' => 'psc_component_steplayouter_system_settings_index',
            'extras' => array(
                'icon' => 'fas fa-object-group',
                'orderNumber' => 20
            )
        ));
    }
}
