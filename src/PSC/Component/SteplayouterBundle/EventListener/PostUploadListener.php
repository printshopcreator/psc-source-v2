<?php

namespace PSC\Component\SteplayouterBundle\EventListener;

use Oneup\UploaderBundle\Event\PostUploadEvent;

class PostUploadListener
{
    public function __construct($doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function postUpload(PostUploadEvent $event)
    {
        $event->getResponse()->setWebPath($event->getFile()->getKey());
    }
}
