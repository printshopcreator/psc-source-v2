<?php

namespace PSC\Component\SteplayouterBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use PSC\Component\SteplayouterBundle\Calendar\Design;
use PSC\Component\SteplayouterBundle\Calendar\Generator;
use PSC\Component\SteplayouterBundle\Calendar\Type\Hoch;
use PSC\Component\SteplayouterBundle\Calendar\Type\Hochvariable;
use PSC\Component\SteplayouterBundle\Calendar\Type\Quer;
use PSC\Component\SteplayouterBundle\Config\Converter\ToArray;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Twig\Environment;

/**
 * Pdf
 *
 * @author Thomas Peterson
 */
class Pdf
{
    protected $entityManager;
    protected $pdf_lic;
    protected $kernel;
    protected $mongoDb;
    protected $converter;
    protected $twig;

    public function __construct(EntityManagerInterface $em, $pdflib_lic, KernelInterface $kernel, ToArray $converter, DocumentManager $mongoDb, Environment $twig)
    {
        $this->entityManager = $em;
        $this->pdf_lic = $pdflib_lic;
        $this->kernel = $kernel;
        $this->converter = $converter;
        $this->mongoDb = $mongoDb;
        $this->twig = $twig;
    }

    public function create($outfilename, $layouter_uuid, $alias = "")
    {

        /** @var Layoutdesigndata $layoutData */
        $layoutData = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $layouter_uuid));

        if (!$layoutData) {
            return;
        }

        $data = $layoutData->getDesign();
        $datatable = $layoutData->getDatatable();
        $dynamicSize = $layoutData->isDynamicSize();
        $dynamicSizeWidth = $layoutData->getDynamicWidth();
        $dynamicSizeHeight = $layoutData->getDynamicHeight();

        if (empty($datatable)) {
            $datatable[] = array();
        }

        $product = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $layoutData->getArticleUuid()));

        $p = new \PDFlib();
        $p->set_option("errorpolicy=return");
        $p->set_option("stringformat=utf8");
        if ($this->pdf_lic && $this->pdf_lic != "") {
            @$p->set_parameter("license", $this->pdf_lic);
        }
        $p->set_option("SearchPath=" . $this->kernel->getProjectDir() . "/assets/fonts");



        if ($p->begin_document($outfilename, "") == 0) {
            die("Error: " . $p->get_errmsg());
        }

        $p->set_info("Creator", "PDFlib starter sample");
        $p->set_info("Title", "starter_path");

        $dataArray = $this->converter->fromXml($product);

        $tempFiles = array();

        foreach ($datatable as $row) {
            $pageOpen = false;

            foreach ($data as $key => $site) {
                $id = uniqid();
                if (!isset($site['fabricSVG']) || $site['fabricSVG'] == '') {
                    continue;
                }

                $svgContent = $this->utf8_for_xml($site['fabricSVG']);

                if ($dynamicSize) {
                    $site['width'] = $dynamicSizeWidth;
                    $site['height'] = $dynamicSizeHeight;
                }

                $xml = simplexml_load_string($svgContent);
                $tps = $xml->xpath('//*[string(@templatePrintBinding)]');

                foreach ($tps as $tp) {
                    $tp->{0} = $tp['templatePrintBinding'];
                }
                $svgContent = $xml->asXML();
                preg_match_all('/(image|xlink:href)=(")[^">]+/i', $svgContent, $media);
                $urls = preg_replace('/(image|xlink:href)("|=")(.*)/i', "$3", $media[0]);
                $cal = false;
                foreach ($urls as $url) {
                    $matches = array();

                    $found = preg_match("/[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/", $url, $matches);

                    if ($found == 0) {
                        $found = preg_match("/[a-fA-F0-9]{4}-[a-fA-F0-9]{8}-[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{8}/", $url, $matches);
                    }
                    if ($found == 0) {
                        $found = preg_match("/\/[a-fA-F0-9]{24}/", $url, $matches);
                        if (isset($matches[0])) {
                            $matches[0] = str_replace("/", "", $matches[0]);
                        }
                    }

                    if ($found) {
                        $motiv = $this->mongoDb
                            ->getRepository('PSCEntityBundle:Motiv')
                            ->find($matches[0]);

                        if (!$motiv) {
                            $motiv = $this->entityManager
                                ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($matches[0]);
                        }
                        if ($motiv) {
                            $svgContent = str_replace($url, "../market/motive/" . $motiv->getFileOrginal(), $svgContent);
                        }
                        continue;
                    }
                    if ($found == 0) {
                        $found = preg_match("/calendar\/render\/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{1,4}\/[0-9]{1,6}\/[0-9]{1,6}\/[0-9]{1,3}\/[0-9]{1,3}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{1}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/.{1,40}/", $url, $matches);
                    } elseif ($found == 0) {
                        $found = preg_match("/calendar\/render\/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{1,4}\/[0-9]{1,6}\/[0-9]{1,6}\/[0-9]{1,3}\/[0-9]{1,3}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{1}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}/", $url, $matches);
                    }

                    if ($found) {
                        $matches = explode("/", $matches[0]);

                        $calendarDesign = new Design();

                        switch ($matches[2]) {
                            case 4:
                                $calendarType = new Hoch($this->container);
                                $calendarDesign->setHeight($matches[6]);
                                break;
                            case 8:
                                $calendarType = new Hochvariable($this->container);
                                $calendarDesign->setHeight($matches[6]);
                                break;
                            case 7:
                            default:
                                $calendarType = new Quer($this->container);
                        }

                        $calendarDesign->setRenderMonth($matches[3]);
                        $calendarDesign->setStartYear($matches[4]);
                        $calendarDesign->setWidth($matches[5]);
                        $calendarDesign->setMonthSize($matches[7]);
                        $calendarDesign->setDaySize($matches[8]);
                        $calendarDesign->setMonthColor('#' . $matches[9]);
                        $calendarDesign->setDayColor('#' . $matches[10]);
                        $calendarDesign->setSunColor('#' . $matches[11]);
                        $calendarDesign->setDisplayYear($matches[12]);
                        $calendarDesign->setDisplayHeightMounthCal($matches[13]);
                        $calendarDesign->setDisplayMarginCal($matches[14]);
                        $calendarDesign->setCalendarX($matches[15]);
                        $calendarDesign->setCalendarY($matches[16]);
                        $calendarDesign->setYearX($matches[17]);
                        $calendarDesign->setYearY($matches[18]);
                        $calendarDesign->setCalendarWidth($matches[19]);
                        $calendarDesign->setCalendarHeight($matches[20]);
                        $calendarDesign->setMonthX($matches[21]);
                        $calendarDesign->setMonthY($matches[22]);
                        $calendarDesign->setYearSize($matches[23]);
                        if (isset($matches[24])) {
                            $calendarDesign->setFontFamily(urldecode($matches[24]));
                        }
                        $calendarType->setDesign($calendarDesign);
                        $calendarGenerator = new Generator();
                        $calendarGenerator->setType($calendarType);
                        $cal = $calendarGenerator->render();

                        //$svgContent = preg_replace('/<image.*calendar.+<\/image>/', $cal, $svgContent);
                        $svgContent = str_replace($url, $cal, $svgContent);
                    }
                }

                $template = $this->twig->createTemplate($svgContent);
                $svgContent = $template->render(array(
                    'row' => $row
                ));
                file_put_contents($this->kernel->getProjectDir() . "/web/temp/" . $layouter_uuid . '_' . $key . ".svg", $svgContent);

                $width = $site['width'] + $dataArray['sites'][$key + 1]['trimLeft'] + $dataArray['sites'][$key + 1]['trimRight'];
                $height = $site['height'] + $dataArray['sites'][$key + 1]['trimTop'] + $dataArray['sites'][$key + 1]['trimBottom'];

                if (isset($dataArray['pdfPrintMerge']['sites']) && $key == 0) {
                    $p->begin_page_ext(0, 0, "width=" . $dataArray['pdfPrintMerge']['width'] . " height=" . $dataArray['pdfPrintMerge']['height']);
                    $pageOpen = true;
                } elseif (isset($dataArray['pdfPrintMerge']['sites']) && $key > 0 && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                } else {
                    $p->begin_page_ext(0, 0, "width=" . $width . " height=" . $height . " trimbox={" . $dataArray['sites'][$key + 1]['trimLeft'] . " "
                        . $dataArray['sites'][$key + 1]['trimTop'] . " " . ($site['width'] + $dataArray['sites'][$key + 1]['trimLeft']) . " " . ($site['height'] + $dataArray['sites'][$key + 1]['trimTop']) . "}");
                    $pageOpen = true;
                }

                $graphics = $p->load_graphics("auto", $this->kernel->getProjectDir() . "/web/temp/" . $layouter_uuid . '_' . $key . ".svg", "");

                $optlist = "boxsize={" . $width . " " . $height . "} position={top left} fitmethod=meet";

                if ($p->info_graphics($graphics, "fittingpossible", $optlist) == 1) {
                    if (isset($dataArray['pdfPrintMerge']['sites']) && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                        if ($dataArray['sites'][$key + 1]['clipBox']) {
                            $clipBox = explode(" ", $dataArray['sites'][$key + 1]['clipBox']);
                            $p->save();
                            $p->rect($dataArray['sites'][$key + 1]['mergePosX'] + $clipBox[0], $dataArray['sites'][$key + 1]['mergePosY'] + $clipBox[1], $clipBox[2], $clipBox[3]);
                            $p->clip();
                        }
                        $p->fit_graphics($graphics, $dataArray['sites'][$key + 1]['mergePosX'], $dataArray['sites'][$key + 1]['mergePosY'], $optlist);
                        if ($dataArray['sites'][$key + 1]['clipBox']) {
                            $p->restore();
                        }
                    } else {
                        $p->fit_graphics($graphics, 0, 0, $optlist);
                    }
                }
                $p->close_graphics($graphics);
                $p->end_layer();

                if ($cal) {
                    $indocCal = $p->open_pdi_document($cal, "");
                    if ($indocCal == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $pageCal = $p->open_pdi_page($indocCal, 1, "");
                    if ($pageCal == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $layerCal = $p->define_layer("cal", "");
                    $p->begin_layer($layerCal);

                    $p->fit_pdi_page($pageCal, $dataArray['sites'][$key + 1]['calendar']['x'] + $dataArray['sites'][$key + 1]['trimLeft'], $dataArray['sites'][$key + 1]['trimBottom'] + $site['height'] - $dataArray['sites'][$key + 1]['calendar']['y'] - $dataArray['sites'][$key + 1]['calendar']['height'], "");

                    $p->close_pdi_page($pageCal);

                    $p->end_layer();
                }

                if ($dataArray['sites'][$key + 1]['elements']) {
                    foreach ($dataArray['sites'][$key + 1]['elements'] as $element) {
                        if ($element['type'] == "stanze") {
                            $stanze = $this->mongoDb
                                ->getRepository('PSCEntityBundle:Motiv')
                                ->find($element['value']);

                            if (!$stanze) {
                                $stanze = $this->entityManager
                                    ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($element['value']);
                            }

                            $indocStanze = $p->open_pdi_document($this->kernel->getProjectDir() . "/web/market/motive/" . $stanze->getFileOrginal(), "");
                            if ($indocStanze == 0) {
                                throw new \Exception("Error: " . $p->get_errmsg());
                            }

                            $pageStanze = $p->open_pdi_page($indocStanze, 1, "");
                            if ($pageStanze == 0) {
                                throw new \Exception("Error: " . $p->get_errmsg());
                            }

                            $layerStanze = $p->define_layer("stanze", "");
                            $p->begin_layer($layerStanze);

                            /* Place the imported page on the German layer of the output page */
                            if (isset($dataArray['pdfPrintMerge']['sites']) && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                                $p->fit_pdi_page($pageStanze, $dataArray['sites'][$key + 1]['mergePosX'] + $element['x'], $dataArray['sites'][$key + 1]['mergePosY'] + $element['y'], "");
                            } else {
                                $p->fit_pdi_page($pageStanze, 0, 0, "");
                            }

                            $p->close_pdi_page($pageStanze);

                            $p->end_layer();
                        }
                    }
                }

                if ($dataArray['sites'][$key + 1]['elements']) {
                    foreach ($dataArray['sites'][$key + 1]['elements'] as $element) {
                        if ($element['type'] == "barcode") {
                            $barcodefont = $p->load_font("fre3of9x", "unicode", "embedding");
                            $font = $p->load_font("Verdana", "unicode", "");
                            if ($barcodefont == 0) {
                                throw new Exception("Error: " . $p->get_errmsg());
                            }

                            if (isset($dataArray['pdfPrintMerge']['sites']) && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                                $p->fit_textline($alias, $dataArray['sites'][$key + 1]['mergePosX'] + $element['x'], $dataArray['sites'][$key + 1]['mergePosY'] + $element['y'], "font=" . $barcodefont .
                                    " rotate=90 fontsize=" . $element['fontSize']);
                                $p->fit_textline($alias, $dataArray['sites'][$key + 1]['mergePosX'] + $element['x'] + 10, $dataArray['sites'][$key + 1]['mergePosY'] + $element['y'], "font=" . $font .
                                    " rotate=90 fontsize=10");
                            } else {
                                $p->fit_textline($alias, $element['x'], $element['y'], "font=" . $barcodefont .
                                    " rotate=" . $element['rotate'] . " fontsize=" . $element['fontSize'] . " " .
                                    "fillcolor={black 1} " .
                                    "matchbox={fillcolor={white 1} " .
                                    "boxheight={ascender descender} " .
                                    "offsetleft=-8 offsetright=8 offsettop=8 offsetbottom=-12}");
                                if (isset($element['addTextX'])) {
                                    $p->fit_textline($alias, $element['x'], $element['y'] - 12, "font=" . $font .
                                        " rotate=" . $element['rotate'] . " fontsize=10 " .
                                        "fillcolor={black 1} " .
                                        "matchbox={fillcolor={white 1} " .
                                        "boxheight={ascender descender} " .
                                        "offsetleft=-8 offsetright=8 offsettop=0 offsetbottom=0}");
                                    $p->fit_textline($element['addText'], $element['addTextX'], $element['addTextY'], "font=" . $font .
                                        " rotate=" . $element['rotate'] . " fontsize=10 " .
                                        "fillcolor={black 1} " .
                                        "matchbox={fillcolor={white 1} " .
                                        "boxheight={ascender descender} " .
                                        "offsetleft=-8 offsetright=8 offsettop=0 offsetbottom=0}");
                                } else {
                                    $p->fit_textline($alias . $element['addText'], $element['x'], $element['y'] - 12, "font=" . $font .
                                        " rotate=" . $element['rotate'] . " fontsize=10 " .
                                        "fillcolor={black 1} " .
                                        "matchbox={fillcolor={white 1} " .
                                        "boxheight={ascender descender} " .
                                        "offsetleft=-8 offsetright=8 offsettop=0 offsetbottom=0}");
                                }
                            }
                        }
                    }
                }

                if (!$dataArray['pdfPrintMerge']) {
                    $p->end_page_ext("");
                    $pageOpen = false;
                } elseif (isset($dataArray['pdfPrintMerge']['sites']) && $key > 0 && !in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                    $p->end_page_ext("");
                    $pageOpen = false;
                }
            }

            if ($pageOpen) {
                if (isset($dataArray['pdfPrintMerge']['stanze'])) {
                    $stanze = $this->mongoDb
                        ->getRepository('PSCEntityBundle:Motiv')
                        ->find((string)$dataArray['pdfPrintMerge']['stanze']);

                    if (!$stanze) {
                        $stanze = $this->entityManager
                            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid((string)$dataArray['pdfPrintMerge']['stanze']);
                    }

                    $indocStanze = $p->open_pdi_document($this->kernel->getProjectDir() . "/web/market/motive/" . $stanze->getFileOrginal(), "");
                    if ($indocStanze == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $pageStanze = $p->open_pdi_page($indocStanze, 1, "");
                    if ($pageStanze == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $layerStanze = $p->define_layer("stanze", "");
                    $p->begin_layer($layerStanze);

                    /* Place the imported page on the German layer of the output page */
                    $p->fit_pdi_page($pageStanze, 0, 0, "");

                    $p->close_pdi_page($pageStanze);

                    $p->end_layer();
                }

                $p->end_page_ext("");
            }
        }

        $p->end_document("");


        foreach ($tempFiles as $file) {
            unlink($file);
        }
    }

    private function utf8_for_xml($string)
    {
        return preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
    }
}
