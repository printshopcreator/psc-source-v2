<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Controller\System;

use Doctrine\ODM\MongoDB\DocumentManager;
use GuzzleHttp\Client;
use PSC\Component\SteplayouterBundle\Form\Backend\SteplayouterSettingsType;
use PSC\Shop\EntityBundle\Document\Install;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\SettingsBundle\Form\Backend\ShopSettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * SettingsController Steplayouter
 *
 * @package    PSC\Component\SteplayouterBundle
 * @subpackage Controller
 */
class SettingsController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/settings/index", name="psc_component_steplayouter_system_settings_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Install $install */
        $install = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Install')
            ->findOneBy(array('installId' => (string)$selectedShop->getInstall()->getUid()));
        if ($install === null) {
            $install = new Install();
            $install->setInstallId((string)$selectedShop->getInstall()->getUid());
            $install->setCalendarStartYear(date("Y"));
        }


        $form = $this->createForm(SteplayouterSettingsType::class, $install);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($install);
            $documentManager->flush();
        }
        return array('form' => $form->createView());
    }
}
