<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Controller\System;

use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\Component\SteplayouterBundle\Form\Backend\SteplayouterSettingsType;
use PSC\Shop\EntityBundle\Document\Install;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\SettingsBundle\Form\Backend\ShopSettingsType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * CheckController Steplayouter
 *
 * @package    PSC\Component\SteplayouterBundle
 * @subpackage Controller
 */
class CheckController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/check/index", name="psc_component_steplayouter_system_check_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @return View
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Product');
        $products = $userRepository->createQueryBuilder('product')
            ->andWhere('product.shop = :shop_id')
            ->andWhere("product.uuid != ''")
            ->andWhere("product.active = 1")
            ->andWhere("product.private = 0")
            ->andWhere("product.steplayouterXml != ''")
            ->setParameter("shop_id", $selectedShop->getUid())
            ->orderBy('product.uid', 'ASC')
            ->getQuery()->execute();
        $errProduct = array();
        $fixes = $request->get('uids', array());
/** @var Product $product */
        foreach ($products as $product) {
            $conf = simplexml_load_string($product->getSteplayouterXml());
            $error = false;
            if ($conf->rendering && $conf->rendering->sites) {
                foreach ($conf->rendering->sites->children() as $site) {
                    $ids = array();
                    if ($site->elements) {
                        foreach ($site->elements->children() as $key => $element) {
                            if ((in_array((string)$element['id'], $ids) || (string)$element['id'] == '') && !in_array($product->getUID(), $fixes)) {
                                if (!$error) {
                                    $errProduct[] = $product;
                                }
                                $error = true;
                            } elseif ((in_array((string)$element['id'], $ids) || (string)$element['id'] == '') && in_array($product->getUID(), $fixes)) {
                                $element['id'] = 'id_' . time();
                                $error = true;
                            } else {
                                $ids[] = (string)$element['id'];
                            }
                        }
                    }
                }
            }
            if ($error && in_array($product->getUID(), $fixes)) {
                $product->setSteplayouterXml($conf->saveXml());
                $entityManager->persist($product);
                $entityManager->flush();
            }
        }

        return array('products' => $errProduct);
    }
}
