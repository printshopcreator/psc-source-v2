<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Util\Path;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use PSC\Component\SteplayouterBundle\Config\Converter\ToArray;

/**
 * ProductController für SteplayouterBundle
 *
 * @package    PSC\Backend\Portal
 * @subpackage Controller
 */
class ProductController extends AbstractController
{
    /**
     * Load Steplayouter Xml and convert to Json
     *
     * @Route("/component/steplayouter/product/save/{product_uuid}", name="component_steplayouter_product_save")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $product_uuid
     * @return JsonResponse
     */
    public function saveProductAction(Request $request, EntityManagerInterface $entityManager, $product_uuid)
    {
        $data = $request->get("payload");
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $product_uuid));
        $product->setSteplayouterData($data);
        $entityManager->persist($product);
        $entityManager->flush();
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'article' => $product_uuid)));
        return $json;
    }


    /**
     * Load Steplayouter Xml and convert to Json
     *
     * @Route("/component/steplayouter/product/load/{product_uuid}", name="component_steplayouter_product_load")
     *
     * @param EntityManagerInterface $entityManager
     * @param ToArray $converter
     * @param $product_uuid
     * @return JsonResponse
     */
    public function loadProductAction(EntityManagerInterface $entityManager, ToArray $converter, $product_uuid)
    {
        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $product_uuid));
        $data = $converter->fromXml($product);
        $json = new JsonResponse();
        if ($product->getSteplayouterData()) {
            $json->setContent(json_encode(array('success' => true, 'data' => $product->getSteplayouterData(), 'article' => $data, 'datatable' => array())));
        } else {
            $json->setContent(json_encode(array('success' => true, 'data' => $data)));
        }


        return $json;
    }
}
