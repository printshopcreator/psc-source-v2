<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Util\Path;
use PSC\Component\SteplayouterBundle\Calendar\Type\Hochvariable;
use PSC\Component\SteplayouterBundle\Config\Converter\ToArray;
use PSC\Component\SteplayouterBundle\Service\Pdf;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use PSC\Component\SteplayouterBundle\Calendar\Design;
use PSC\Component\SteplayouterBundle\Calendar\Type\Hoch;
use PSC\Component\SteplayouterBundle\Calendar\Type\Quer;
use PSC\Component\SteplayouterBundle\Calendar\Generator;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Twig\Environment;
use Twig\Extension\StringLoaderExtension;
use Twig_Environment;

/**
 * PdfController für SteplayouterBundle
 *
 * @package    PSC\Backend\Portal
 * @subpackage Controller
 */
class PdfController extends AbstractController
{
    /**
     * Pdf Preview
     *
     * @Route("/component/steplayouter/pdf/imagepreview/{layouter_uuid}/{width}", name="component_steplayouter_pdf_imagepreview")
     *
     * @return void
     */
    public function imagePreviewAction(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        KernelInterface $kernel,
        Environment $twig,
        ToArray $converter,
        $pdflib_lic,
        $layouter_uuid,
        $width = 100
    ) {

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $layouter_uuid));
        if ($product) {
            $data = $product->getSteplayouterData();
            $datatable = array();
        } else {
        /** @var Layoutdesigndata $layoutData */
            $layoutData = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $layouter_uuid));
            $data = $layoutData->getDesign();
            $datatable = $layoutData->getDatatable();
            $product = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $layoutData->getArticleUuid()));
        }

        if (empty($datatable)) {
            $datatable[] = array();
        }

        $p = new \PDFlib();
        $p->set_option("errorpolicy=return");
        $p->set_option("stringformat=utf8");
        if ($pdflib_lic != "") {
            @$p->set_parameter("license", $pdflib_lic);
        }
        $p->set_option("SearchPath=" . $kernel->getProjectDir() . "/assets/fonts");
        $outfilename = "temp/" . uniqid() . ".pdf";
        if ($p->begin_document($outfilename, "") == 0) {
            die("Error: " . $p->get_errmsg());
        }

        $p->set_info("Creator", "PDFlib starter sample");
        $p->set_info("Title", "starter_path");
        $dataArray = $converter->fromXml($product);
        $tempFiles = array();
        foreach ($datatable as $row) {
            $pageOpen = false;
            foreach ($data as $key => $site) {
                $id = uniqid();
                if (!isset($site['fabricSVG']) || $site['fabricSVG'] == '') {
                    continue;
                }

                if ($layoutData->isDynamicSize()) {
                    $site['width'] = $layoutData->getDynamicHeight();
                    $site['height'] = $layoutData->getDynamicHeight();
                }

                $svgContent = $site['fabricSVG'];
                $xml = simplexml_load_string($svgContent);
                $tps = $xml->xpath('//*[string(@templatePrintBinding)]');
                foreach ($tps as $tp) {
                    $tp->{0} = $tp['templatePrintBinding'];
                }
                $svgContent = $xml->asXML();
                preg_match_all('/(image|xlink:href)=(")[^">]+/i', $svgContent, $media);
                $urls = preg_replace('/(image|xlink:href)("|=")(.*)/i', "$3", $media[0]);
                $cal = false;
                foreach ($urls as $url) {
                    $matches = array();
                    $found = preg_match("/[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/", $url, $matches);
                    if ($found == 0) {
                        $found = preg_match("/[a-fA-F0-9]{4}-[a-fA-F0-9]{8}-[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{8}/", $url, $matches);
                    }
                    if ($found == 0) {
                        $found = preg_match("/\/[a-fA-F0-9]{24}/", $url, $matches);
                        if (isset($matches[0])) {
                            $matches[0] = str_replace("/", "", $matches[0]);
                        }
                    }

                    if ($found) {
                        $motiv = $documentManager
                            ->getRepository('PSCEntityBundle:Motiv')
                            ->find($matches[0]);
                        if (!$motiv) {
                            $motiv = $entityManager
                            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($matches[0]);
                        }
                        if ($motiv) {
                            $svgContent = str_replace($url, "../market/motive/" . $motiv->getFileMid(), $svgContent);
                        }
                        continue;
                    }
                    if ($found == 0) {
                        $found = preg_match("/calendar\/render\/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{1,4}\/[0-9]{1,6}\/[0-9]{1,6}\/[0-9]{1,3}\/[0-9]{1,3}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{1}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/.{1,40}/", $url, $matches);
                    } elseif ($found == 0) {
                        $found = preg_match("/calendar\/render\/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{1,4}\/[0-9]{1,6}\/[0-9]{1,6}\/[0-9]{1,3}\/[0-9]{1,3}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{1}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}/", $url, $matches);
                    }

                    if ($found) {
                        $matches = explode("/", $matches[0]);
                        $calendarDesign = new Design();
                        switch ($matches[2]) {
                            case 4:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         $calendarType = new Hoch($this->container);
                                $calendarDesign->setHeight($matches[6]);

                                break;
                            case 8:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     $calendarType = new Hochvariable($this->container);
                                $calendarDesign->setHeight($matches[6]);

                                break;
                            case 7:
                            default:
                                $calendarType = new Quer($this->container);
                        }

                        $calendarDesign->setRenderMonth($matches[3]);
                        $calendarDesign->setStartYear($matches[4]);
                        $calendarDesign->setWidth($matches[5]);
                        $calendarDesign->setMonthSize($matches[7]);
                        $calendarDesign->setDaySize($matches[8]);
                        $calendarDesign->setMonthColor('#' . $matches[9]);
                        $calendarDesign->setDayColor('#' . $matches[10]);
                        $calendarDesign->setSunColor('#' . $matches[11]);
                        $calendarDesign->setDisplayYear($matches[12]);
                        $calendarDesign->setDisplayHeightMounthCal($matches[13]);
                        $calendarDesign->setDisplayMarginCal($matches[14]);
                        $calendarDesign->setCalendarX($matches[15]);
                        $calendarDesign->setCalendarY($matches[16]);
                        $calendarDesign->setYearX($matches[17]);
                        $calendarDesign->setYearY($matches[18]);
                        $calendarDesign->setCalendarWidth($matches[19]);
                        $calendarDesign->setCalendarHeight($matches[20]);
                        $calendarDesign->setMonthX($matches[21]);
                        $calendarDesign->setMonthY($matches[22]);
                        $calendarDesign->setYearSize($matches[23]);
                        if (isset($matches[24])) {
                            $calendarDesign->setFontFamily(urldecode($matches[24]));
                        }
                            $calendarType->setDesign($calendarDesign);
                            $calendarGenerator = new Generator();
                            $calendarGenerator->setType($calendarType);
                            $cal = $calendarGenerator->render();
                            $svgContent = preg_replace('/<image.*calendar.+<\/image>/', "", $svgContent);
                    }
                }

                $template = $twig->createTemplate($svgContent);
                $svgContent = $template->render(array(
                    'row' => $row
                ));
                file_put_contents("temp/" . $layouter_uuid . '_' . $key . ".svg", $svgContent);
                $tempFiles[] = "temp/" . $layouter_uuid . '_' . $key . ".svg";
                $p->begin_page_ext(0, 0, "width=" . $site['width'] . " height=" . $site['height']);
                $graphics = $p->load_graphics("auto", "temp/" . $layouter_uuid . '_' . $key . ".svg", "");
                $optlist = "boxsize={" . $site['width'] . " " . $site['height'] . "} position={center} fitmethod=meet";
                if ($p->info_graphics($graphics, "fittingpossible", $optlist) == 1) {
                            $p->fit_graphics($graphics, 0, 0, $optlist);
                }
                $p->close_graphics($graphics);
                $p->end_layer();
                if ($cal) {
                    $indocCal = $p->open_pdi_document($cal, "");
                    if ($indocCal == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $pageCal = $p->open_pdi_page($indocCal, 1, "");
                    if ($pageCal == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $layerCal = $p->define_layer("cal", "");
                    $p->begin_layer($layerCal);
                    $p->fit_pdi_page($pageCal, $dataArray['sites'][$key + 1]['calendar']['x'] + $dataArray['sites'][$key + 1]['trimLeft'], $dataArray['sites'][$key + 1]['trimBottom'] + $site['height'] - $dataArray['sites'][$key + 1]['calendar']['y'] - $dataArray['sites'][$key + 1]['calendar']['height'], "");
                    $p->close_pdi_page($pageCal);
                    $p->end_layer();
                }

                $p->end_page_ext("");
                break;
            }
        }

        $p->end_document("");
        foreach ($tempFiles as $file) {
            unlink($file);
        }

        $im = new \imagick($outfilename . "[0]");
        $im->thumbnailimage($width, 400, true);
        $im->writeimage($outfilename . ".png");
        unlink($outfilename);
        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', mime_content_type($outfilename . ".png"));
        $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($outfilename . ".png") . '"');
        $response->headers->set('Content-length', filesize($outfilename . ".png"));
        $response->sendHeaders();
        $response->setContent(readfile($outfilename . ".png"));
        return $response;
    }

    /**
     * Pdf Preview
     *
     * @Route("/component/steplayouter/pdf/preview/{product_uuid}", name="component_steplayouter_pdf_preview")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param KernelInterface $kernel
     * @param Environment $twig
     * @param ToArray $converter
     * @param $pdflib_lic
     * @param $product_uuid
     * @return JsonResponse
     * @throws \Exception
     */
    public function previewAction(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        KernelInterface $kernel,
        Environment $twig,
        ToArray $converter,
        $pdflib_lic,
        $product_uuid
    ) {
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $product_uuid));
        if (!$product) {
            $dataDesign = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $product_uuid));
            $product = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $dataDesign->getArticleUuid()));
        }

        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }
        $data = $parametersAsArray["payload"] ?? array();
        $datatable = $parametersAsArray["datatable"] ?? array();
        $dynamicSize = $parametersAsArray["dynamicSize"] ?? false;
        $dynamicSizeWidth = $parametersAsArray["dynamicSizeWidth"] ?? 0;
        $dynamicSizeHeight = $parametersAsArray["dynamicSizeHeight"] ?? 0;
        if (empty($datatable)) {
            $datatable[] = array();
        }

        $p = new \PDFlib();
        $p->set_option("errorpolicy=return");
        $p->set_option("stringformat=utf8");
        if ($pdflib_lic != "") {
            @$p->set_parameter("license", $pdflib_lic);
        }
        $p->set_option("SearchPath=" . $kernel->getProjectDir() . "/assets/fonts");
        $outfilename = "temp/" . uniqid() . ".pdf";
        $optlist = "masterpassword=BoonkerzWichtig1 permissions={" .
            "nocopy noaccessible noassemble}";
        if ($p->begin_document($outfilename, $optlist) == 0) {
            die("Error: " . $p->get_errmsg());
        }

        $p->set_info("Creator", "PDFlib starter sample");
        $p->set_info("Title", "starter_path");
        $dataArray = $converter->fromXml($product);
        $tempFiles = array();
        foreach ($datatable as $row) {
            $pageOpen = false;
            reset($data);
            foreach ($data as $key => $site) {
                $id = uniqid();
                if (!isset($site['fabricSVG']) || $site['fabricSVG'] == '') {
                    continue;
                }

                if ($dynamicSize) {
                    $site['width'] = $dynamicSizeWidth;
                    $site['height'] = $dynamicSizeHeight;
                }

                $svgContent = $site['fabricSVG'];
                $xml = simplexml_load_string($svgContent);
                $tps = $xml->xpath('//*[string(@templatePrintBinding)]');
                foreach ($tps as $tp) {
                    $tp->{0} = $tp['templatePrintBinding'];
                }
                $svgContent = $xml->asXML();
                preg_match_all('/(image|xlink:href)=(")[^">]+/i', $svgContent, $media);
                $urls = preg_replace('/(image|xlink:href)("|=")(.*)/i', "$3", $media[0]);
                $cal = false;
                foreach ($urls as $url) {
                    $matches = array();
                    $found = preg_match("/[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}/", $url, $matches);
                    if ($found == 0) {
                        $found = preg_match("/[a-fA-F0-9]{4}-[a-fA-F0-9]{8}-[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{8}/", $url, $matches);
                    }
                    if ($found == 0) {
                        $found = preg_match("/\/[a-fA-F0-9]{24}/", $url, $matches);
                        if (isset($matches[0])) {
                            $matches[0] = str_replace("/", "", $matches[0]);
                        }
                    }

                    if ($found) {
                        $motiv = $documentManager
                            ->getRepository('PSCEntityBundle:Motiv')
                            ->find($matches[0]);
                        if (!$motiv) {
                            $motiv = $entityManager
                            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($matches[0]);
                        }

                        if ($motiv) {
                            $svgContent = str_replace($url, "../market/motive/" . $motiv->getFileMid(), $svgContent);
                        }
                        continue;
                    }

                    if ($found == 0) {
                        $found = preg_match("/calendar\/render\/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{1,4}\/[0-9]{1,6}\/[0-9]{1,6}\/[0-9]{1,3}\/[0-9]{1,3}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{1}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/.{1,40}/", $url, $matches);
                    } elseif ($found == 0) {
                        $found = preg_match("/calendar\/render\/[0-9]{1,2}\/[0-9]{1,2}\/[0-9]{1,4}\/[0-9]{1,6}\/[0-9]{1,6}\/[0-9]{1,3}\/[0-9]{1,3}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{6}\/[a-fA-F0-9]{1}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}\/[0-9]{1,3}/", $url, $matches);
                    }

                    if ($found) {
                        $matches = explode("/", $matches[0]);
                        $calendarDesign = new Design();
                        switch ($matches[2]) {
                            case 4:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          $calendarType = new Hoch($this->container);
                                $calendarDesign->setHeight($matches[6]);

                                break;
                            case 8:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      $calendarType = new Hochvariable($this->container);
                                $calendarDesign->setHeight($matches[6]);

                                break;
                            case 7:
                            default:
                                $calendarType = new Quer($this->container);
                        }

                        $calendarDesign->setRenderMonth($matches[3]);
                        $calendarDesign->setStartYear($matches[4]);
                        $calendarDesign->setWidth($matches[5]);
                        $calendarDesign->setMonthSize($matches[7]);
                        $calendarDesign->setDaySize($matches[8]);
                        $calendarDesign->setMonthColor('#' . $matches[9]);
                        $calendarDesign->setDayColor('#' . $matches[10]);
                        $calendarDesign->setSunColor('#' . $matches[11]);
                        $calendarDesign->setDisplayYear($matches[12]);
                        $calendarDesign->setDisplayHeightMounthCal($matches[13]);
                        $calendarDesign->setDisplayMarginCal($matches[14]);
                        $calendarDesign->setCalendarX($matches[15]);
                        $calendarDesign->setCalendarY($matches[16]);
                        $calendarDesign->setYearX($matches[17]);
                        $calendarDesign->setYearY($matches[18]);
                        $calendarDesign->setCalendarWidth($matches[19]);
                        $calendarDesign->setCalendarHeight($matches[20]);
                        $calendarDesign->setMonthX($matches[21]);
                        $calendarDesign->setMonthY($matches[22]);
                        $calendarDesign->setYearSize($matches[23]);
                        if (isset($matches[24])) {
                            $calendarDesign->setFontFamily(urldecode($matches[24]));
                        }
                        $calendarType->setDesign($calendarDesign);
                        $calendarGenerator = new Generator();
                        $calendarGenerator->setType($calendarType);
                        $cal = $calendarGenerator->render();
                        $svgContent = preg_replace('/<image.*calendar.+<\/image>/', "", $svgContent);
                    }
                }

                $template = $twig->createTemplate($svgContent);
                $svgContent = $template->render(array(
                    'row' => $row
                ));
                file_put_contents("temp/" . $id . ".svg", $svgContent);
                $tempFiles[] = "temp/" . $id . ".svg";
                $width = $site['width'] + $dataArray['sites'][$key + 1]['trimLeft'] + $dataArray['sites'][$key + 1]['trimRight'];
                $height = $site['height'] + $dataArray['sites'][$key + 1]['trimTop'] + $dataArray['sites'][$key + 1]['trimBottom'];
                if (isset($dataArray['pdfPrintMerge']['sites']) && $key == 0) {
                            $p->begin_page_ext(0, 0, "width=" . $dataArray['pdfPrintMerge']['width'] . " height=" . $dataArray['pdfPrintMerge']['height']);
                            $pageOpen = true;
                } elseif (isset($dataArray['pdfPrintMerge']['sites']) && $key > 0 && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                } else {
                    $p->begin_page_ext(0, 0, "width=" . $site['width'] . " height=" . $site['height']);
                    $pageOpen = true;
                }


                $graphics = $p->load_graphics("auto", "temp/" . $id . ".svg", "");
                $optlist = "boxsize={" . $width . " " . $height . "} position={top left} fitmethod=meet";
                if ($p->info_graphics($graphics, "fittingpossible", $optlist) == 1) {
                    if (isset($dataArray['pdfPrintMerge']['sites']) && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                        $p->fit_graphics($graphics, $dataArray['sites'][$key + 1]['mergePosX'], $dataArray['sites'][$key + 1]['mergePosY'], $optlist);
                    } else {
                        $p->fit_graphics($graphics, $dataArray['sites'][$key + 1]['trimLeft'] * -1, $dataArray['sites'][$key + 1]['trimTop'] * -1, $optlist);
                    }
                }

                $p->close_graphics($graphics);
                $p->end_layer();
                if ($cal) {
                    $indocCal = $p->open_pdi_document($cal, "");
                    if ($indocCal == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $pageCal = $p->open_pdi_page($indocCal, 1, "");
                    if ($pageCal == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $layerCal = $p->define_layer("cal", "");
                    $p->begin_layer($layerCal);
                    $p->fit_pdi_page($pageCal, $dataArray['sites'][$key + 1]['calendar']['x'], $site['height'] - $dataArray['sites'][$key + 1]['calendar']['y'] - $dataArray['sites'][$key + 1]['calendar']['height'], "");
                    $p->close_pdi_page($pageCal);
                    $p->end_layer();
                }

                if ($dataArray['sites'][$key + 1]['elements']) {
                    foreach ($dataArray['sites'][$key + 1]['elements'] as $element) {
                        if ($element['type'] == "stanze") {
                            $stanze = $documentManager
                                ->getRepository('PSCEntityBundle:Motiv')
                                ->find($element['value']);
                            if (!$stanze) {
                                        $stanze = $entityManager
                                    ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($element['value']);
                            }

                            $indocStanze = $p->open_pdi_document($kernel->getProjectDir() . "/web/market/motive/" . $stanze->getFileOrginal(), "");
                            if ($indocStanze == 0) {
                                        throw new \Exception("Error: " . $p->get_errmsg());
                            }

                            $pageStanze = $p->open_pdi_page($indocStanze, 1, "");
                            if ($pageStanze == 0) {
                                throw new \Exception("Error: " . $p->get_errmsg());
                            }

                            $layerStanze = $p->define_layer("stanze", "");
                            $p->begin_layer($layerStanze);
                /* Place the imported page on the German layer of the output page */
                            if (isset($dataArray['pdfPrintMerge']['sites']) && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                                $p->fit_pdi_page($pageStanze, $dataArray['sites'][$key + 1]['mergePosX'] + $element['x'], $dataArray['sites'][$key + 1]['mergePosY'] + $element['y'], "");
                            } else {
                                $p->fit_pdi_page($pageStanze, 0, 0, "");
                            }

                            $p->close_pdi_page($pageStanze);
                            $p->end_layer();
                        }
                    }
                }

                if ($dataArray['sites'][$key + 1]['elements']) {
                    foreach ($dataArray['sites'][$key + 1]['elements'] as $element) {
                        if ($element['type'] == "barcode") {
                            $barcodefont = $p->load_font("fre3of9x", "unicode", "embedding");
                            if ($barcodefont == 0) {
                                        throw new Exception("Error: " . $p->get_errmsg());
                            }

                            if (isset($dataArray['pdfPrintMerge']['sites']) && in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                                $p->fit_textline("*ABC123*", $dataArray['sites'][$key + 1]['mergePosX'] + $element['x'], $dataArray['sites'][$key + 1]['mergePosY'] + $element['y'], "font=" . $barcodefont .
                                " rotate=90 fontsize=" . $element['fontSize']);
                                $font = $p->load_font("Verdana", "unicode", "");
                                $p->fit_textline("2432342423423424", $dataArray['sites'][$key + 1]['mergePosX'] + $element['x'] + 10, $dataArray['sites'][$key + 1]['mergePosY'] + $element['y'], "font=" . $font .
                                    " rotate=90 fontsize=10");
                            } else {
                                $p->fit_textline("*ABC123*", $element['x'], $element['y'], "font=" . $barcodefont .
                                    " rotate=90 fontsize=" . $element['fontSize']);
                            }
                        }
                    }
                }


                $font = $p->load_font("Verdana", "unicode", "");
                $p->fit_textline("---MUSTER---", 0, 0, "font=" . $font .
                    " fontsize=1 textrendering=1 boxsize={" . $site['width'] . " " . $site['height'] . "} stamp=ul2lr" .
                    " strokecolor=red strokewidth=1");
                if (!$dataArray['pdfPrintMerge']) {
                            $p->end_page_ext("");
                            $pageOpen = false;
                } elseif (isset($dataArray['pdfPrintMerge']['sites']) && $key > 0 && !in_array($key + 1, $dataArray['pdfPrintMerge']['sites'])) {
                    $p->end_page_ext("");
                    $pageOpen = false;
                }
            }

            if ($pageOpen) {
                if (isset($dataArray['pdfPrintMerge']['stanze'])) {
                    $stanze = $documentManager
                        ->getRepository('PSCEntityBundle:Motiv')
                        ->find((string)$dataArray['pdfPrintMerge']['stanze']);
                    if (!$stanze) {
                        $stanze = $entityManager
                            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid((string)$dataArray['pdfPrintMerge']['stanze']);
                    }

                    $indocStanze = $p->open_pdi_document($kernel->getProjectDir() . "/web/market/motive/" . $stanze->getFileOrginal(), "");
                    if ($indocStanze == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $pageStanze = $p->open_pdi_page($indocStanze, 1, "");
                    if ($pageStanze == 0) {
                        throw new \Exception("Error: " . $p->get_errmsg());
                    }

                    $layerStanze = $p->define_layer("stanze", "");
                    $p->begin_layer($layerStanze);
/* Place the imported page on the German layer of the output page */
                    $p->fit_pdi_page($pageStanze, 0, 0, "");
                    $p->close_pdi_page($pageStanze);
                    $p->end_layer();
                }

                $p->end_page_ext("");
            }
        }

        $p->end_document("");
        foreach ($tempFiles as $file) {
            unlink($file);
        }

        $json = new JsonResponse();
        $json->headers->set('Access-Control-Allow-Origin', '*');
        $json->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        $json->setContent(json_encode(array('success' => true, 'file' => $request->getBasePath() . '/' . $outfilename)));
        return $json;
    }


    /**
     * Pdf Preview
     *
     * @Route("/component/steplayouter/pdf/check/{layouter_uuid}", name="component_steplayouter_pdf_check")
     *
     * @return void
     */
    public function pdfCheckAction(
        Request $request,
        EntityManagerInterface $entityManager,
        $layouter_uuid
    ) {
        $layoutData = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $layouter_uuid));
        $data = array('success' => true, 'found' => false);
        if ($layoutData) {
            $data = array('success' => true, 'found' => true);
        }

        $jsonResponse = new JsonResponse();
        $jsonResponse->setContent(json_encode($data));
        return $jsonResponse;
    }

    /**
     * Pdf Preview
     *
     * @Route("/component/steplayouter/pdf/print/{layouter_uuid}/{alias}", name="component_steplayouter_pdf_print")
     *
     * @return RedirectResponse
     */
    public function pdfAction(Request $request, Pdf $pdfService, $layouter_uuid, $alias)
    {
        $outfilename = "temp/" . uniqid() . ".pdf";
        $pdfService->create($outfilename, $layouter_uuid, $alias);
        return $this->redirect($request->getBasePath() . '/' . $outfilename);
    }

    protected function utf8_for_xml($string)
    {
        return preg_replace('/[^\x{0009}\x{000a}\x{000d}\x{0020}-\x{D7FF}\x{E000}-\x{FFFD}]+/u', ' ', $string);
    }
}
