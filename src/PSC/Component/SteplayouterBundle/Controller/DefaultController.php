<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * DefaultController fürs SteplayouterBundle
 *
 * @package    PSC\Component\Steplayouter
 * @subpackage Controller
 */
class DefaultController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/component/steplayouter", name="component_steplayouter")
     * @Template()
     *
     * @return View
     */
    public function indexAction()
    {
        return array('stylePath' => '{{stylePath}}');
    }
}
