<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Util\Path;
use PSC\Component\SteplayouterBundle\Config\Converter\ToArray;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Exception\NotFoundResourceException;

/**
 * StroeController für SteplayouterBundle
 *
 * @package    PSC\Backend\Portal
 * @subpackage Controller
 */
class StoreController extends AbstractController
{
    /**
     * Save the Steplayouter Json
     *
     * @Route("/component/steplayouter/store/save", name="component_steplayouter_store_save")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @return JsonResponse
     */
    public function saveAction(Request $request, EntityManagerInterface $entityManager)
    {

        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }
        $data = $parametersAsArray["payload"] ?? array();
        $datatable = $parametersAsArray["datatable"] ?? array();
        $dynamicSize = $parametersAsArray["dynamicSize"] ?? false;
        $dynamicWidth = $parametersAsArray["dynamicSizeWidth"] ?? 0;
        $dynamicHeight = $parametersAsArray["dynamicSizeHeight"] ?? 0;
        $uuid = $parametersAsArray["layouter"] ?? 0;
        $articleUuid = $parametersAsArray["article"] ?? 0;
        $layoutDesignData = new Layoutdesigndata();
        $layoutDesignData->setUuid($uuid);
        $layoutDesignData->setArticleUuid($articleUuid);
        $layoutDesignData->setDesign($data);
        $layoutDesignData->setDatatable($datatable);
        $layoutDesignData->setDynamicSize($dynamicSize);
        $layoutDesignData->setDynamicWidth($dynamicWidth);
        $layoutDesignData->setDynamicHeight($dynamicHeight);
        $entityManager->persist($layoutDesignData);
        $entityManager->flush();
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true)));
        return $json;
    }

    /**
     * Save the Steplayouter Json
     *
     * @Route("/component/steplayouter/store/update", name="component_steplayouter_store_update")
     *
     * @return void
     */
    public function updateAction(Request $request, EntityManagerInterface $entityManager)
    {
        $parametersAsArray = [];
        if ($content = $request->getContent()) {
            $parametersAsArray = json_decode($content, true);
        }
        $data = $parametersAsArray["payload"] ?? array();
        $datatable = $parametersAsArray["datatable"] ?? array();
        $uuid = $parametersAsArray["uuid"] ?? 0;
        $layoutDesignData = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $uuid));
        $layoutDesignData->setDesign($data);
        $layoutDesignData->setDatatable($datatable);
        $entityManager->persist($layoutDesignData);
        $entityManager->flush();
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'article' => $layoutDesignData->getArticleUuid())));
        return $json;
    }

    /**
     * Load the Steplayouter Json
     *
     * @Route("/component/steplayouter/store/load/{uuid}", name="component_steplayouter_store_load")
     *
     * @return void
     */
    public function loadAction(EntityManagerInterface $entityManager, ToArray $converter, $uuid)
    {

        $dataDesign = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $uuid));
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $dataDesign->getArticleUuid()));
        $data = $converter->fromXml($product, $dataDesign->getDatatable());
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'data' => $dataDesign->getDesign(), 'article' => $data, 'datatable' => $dataDesign->getDatatable())));
        return $json;
    }

    /**
     * Copy SteplayouterJson
     *
     * @Route("/component/steplayouter/store/copy/{uuid}", name="component_steplayouter_store_copy")
     *
     * @param EntityManagerInterface $entityManager
     * @param $uuid
     * @return JsonResponse
     */
    public function copyAction(EntityManagerInterface $entityManager, $uuid)
    {

        $dataDesign = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $uuid));
        $dataDesignNew = clone $dataDesign;
        $dataDesignNew->setUid(null);
        $dataDesignNew->setUuid(Uuid::uuid4()->toString());
        $entityManager->persist($dataDesignNew);
        $entityManager->flush();
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'uuid' => $dataDesignNew->getUuid())));
        return $json;
    }
}
