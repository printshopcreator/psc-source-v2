<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Component\SteplayouterBundle\Controller;

use PSC\Component\SteplayouterBundle\Calendar\Type\Hochvariable;
use PSC\Component\SteplayouterBundle\Calendar\Type\Quer;
use PSC\Component\SteplayouterBundle\Calendar\Type\Hoch;
use PSC\Component\SteplayouterBundle\Calendar\Design;
use PSC\Component\SteplayouterBundle\Calendar\Generator;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * CalendarController fürs SteplayouterBundle
 *
 * @package    PSC\Component\Steplayouter
 * @subpackage Controller
 */
class CalendarController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/component/steplayouter/calendar/render/{type}/{month}/{year}/{width}/{height}/{month_size}/{day_size}/{month_color}/{day_color}/{sun_color}/{designYear}/{designHeightMonthCal}/{designMarginCal}/{calendarX}/{calendarY}/{yearX}/{yearY}/{calendarWidth}/{calendarHeight}/{monthX}/{monthY}/{yearSize}/{fontFamily}", name="component_steplayouter_calendar_render")
     *
     * @return View
     */
    public function indexAction($type, $month, $year, $width, $height, $month_size, $day_size, $month_color, $day_color, $sun_color, $designYear, $designHeightMonthCal, $designMarginCal, $calendarX, $calendarY, $yearX, $yearY, $calendarWidth, $calendarHeight, $monthX, $monthY, $yearSize, $fontFamily)
    {
        $result = setlocale(LC_ALL, 'de_DE.utf-8');
        $calendarDesign = new Design();
        $calendarDesign->setWidth($width);
        $calendarDesign->setMonthSize($month_size);
        $calendarDesign->setRenderMonth($month);
        $calendarDesign->setStartYear($year);
        $calendarDesign->setDaySize($day_size);
        $calendarDesign->setCalendarX($calendarX);
        $calendarDesign->setCalendarY($calendarY);
        $calendarDesign->setYearX($yearX);
        $calendarDesign->setYearY($yearY);
        $calendarDesign->setCalendarWidth($calendarWidth);
        $calendarDesign->setCalendarHeight($calendarHeight);
        $calendarDesign->setMonthX($monthX);
        $calendarDesign->setMonthY($monthY);
        $calendarDesign->setDayColor('#' . $day_color);
        $calendarDesign->setMonthColor('#' . $month_color);
        $calendarDesign->setSunColor('#' . $sun_color);
        $calendarDesign->setYearSize($yearSize);
        $calendarDesign->setFontFamily($fontFamily);
        if ($designYear != 0) {
            $calendarDesign->setDisplayYear(true);
        }
        if ($designHeightMonthCal != 0) {
            $calendarDesign->setDisplayHeightMounthCal($designHeightMonthCal);
        }
        if ($designMarginCal != 0) {
            $calendarDesign->setDisplayMarginCal($designMarginCal);
        }
        switch ($type) {
            case 4:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            $calendarType = new Hoch($this->container);
                $calendarDesign->setHeight($height);

                break;
            case 8:
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            $calendarType = new Hochvariable($this->container);
                $calendarDesign->setHeight($height);

                break;
            case 7:
            default:
                $calendarType = new Quer($this->container);
        }
        $startDate = new \DateTime($calendarDesign->getStartYear() . '-' . $calendarDesign->getStartMonth() . '-01', new \DateTimeZone('Europe/Berlin'));
        $calendarType->setDesign($calendarDesign);
        $calendarGenerator = new Generator();
        $calendarGenerator->setType($calendarType);
        $outFilenamePng = 'temp/cal_' . $calendarDesign->getHash() . ".png";
        if (true || !file_exists($outFilenamePng)) {
            $file = $calendarGenerator->render();
            $im = new \imagick();
            $im->setResolution(300, 300);
            $im->readImage($file . '[0]');
            $im->setImageBackgroundColor('transparent');
            $im->thumbnailimage($width * 2, 900, true);
            $im->writeImage($outFilenamePng);
        }

        $bf = file_get_contents($outFilenamePng);
        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', "image/png");
        $response->headers->set('Content-length', strlen($bf));
        $response->setContent($bf);
        return $response;
    }
}
