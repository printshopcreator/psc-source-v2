<?php

namespace PSC\Component\SteplayouterBundle\Controller;

use Oneup\UploaderBundle\Controller\AbstractChunkedController;
use Oneup\UploaderBundle\Controller\AbstractController;
use PSC\Component\SteplayouterBundle\Response\UploadResponse;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Oneup\UploaderBundle\Uploader\Response\EmptyResponse;
use Symfony\Component\HttpFoundation\Request;

class UploadController extends AbstractController
{
    public function upload(): JsonResponse
    {
        $request = $this->container->get('request');
        $translator = $this->container->get('translator');
        $response = new UploadResponse();
        $totalParts = $request->get('qqtotalparts', 1);
        $files = $this->getFiles($request->files);
        $chunked = $totalParts > 1;
        foreach ($files as $file) {
            try {
                $chunked ?
                    $this->handleChunkedUpload($file, $response, $request) :
                    $this->handleUpload($file, $response, $request)
                ;
                $response->setFinish(true);
            } catch (UploadException $e) {
                $response->setSuccess(false);
                $response->setError($translator->trans($e->getMessage(), array(), 'OneupUploaderBundle'));
                $this->errorHandler->addException($response, $e);
    // an error happended, return this error message.
                return $this->createSupportedJsonResponse($response->assemble());
            }
        }

        return $this->createSupportedJsonResponse($response->assemble());
    }

    protected function parseChunkedRequest(Request $request)
    {
        $index = $request->get('qqpartindex');
        $total = $request->get('qqtotalparts');
        $uuid  = $request->get('qquuid');
        $orig  = $request->get('qqfilename');
        $last  = ($total - 1) == $index;
        return array($last, $uuid, $index, $orig);
    }

    /**
     * Creates and returns a JsonResponse with the given data.
     *
     * On top of that, if the client does not support the application/json type,
     * then the content type of the response will be set to text/plain instead.
     *
     * @param mixed $data
     *
     * @return JsonResponse
     */
    protected function createSupportedJsonResponse($data, $statusCode = 200): JsonResponse
    {
        $request = $this->container->get('request');
        $response = new JsonResponse($data);
        $response->headers->set('Vary', 'Accept');
        if (!in_array('application/json', $request->getAcceptableContentTypes())) {
            $response->headers->set('Content-type', 'text/plain');
        }

        return $response;
    }
}
