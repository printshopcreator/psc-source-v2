<?php

namespace PSC\System\SettingsBundle\Resolver;

use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Imagine\Cache\Helper\PathHelper;
use Liip\ImagineBundle\Imagine\Cache\Resolver\ResolverInterface;
use Symfony\Component\Routing\RequestContext;

class AbsolutePathResolver implements ResolverInterface
{
    /**
     * @var RequestContext
     */
    private $requestContext;

    public function __construct(RequestContext $requestContext)
    {
        $this->requestContext = $requestContext;
    }

    /**
     * {@inheritdoc}
     */
    public function isStored($path, $filter)
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function resolve($path, $filter)
    {
        return "data/www/new/web/uploads/media" . PathHelper::filePathToUrlPath($path);
    }

    /**
     * {@inheritdoc}
     */
    public function store(BinaryInterface $binary, $path, $filter)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function remove(array $paths, array $filters)
    {
    }
}
