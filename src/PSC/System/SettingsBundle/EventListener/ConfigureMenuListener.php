<?php

namespace PSC\System\SettingsBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;

class ConfigureMenuListener
{
    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureSystem(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();
        $menu->addChild('Statusverwaltung', array(
            'route' => 'psc_system_status_list',
            'extras' => array(
                'icon' => 'fas fa-hashtag',
                'orderNumber' => 21
            )
        ));
        $menu->addChild('Papierdatenbank', array(
            'route' => 'psc_system_paper_list',
            'extras' => array(
                'icon' => 'fas fa-paper-plane',
                'orderNumber' => 20
            )
        ));
        $menu->addChild('Systemeinstellung', array(
            'route' => 'psc_backend_system_settings_index',
            'extras' => array(
                'icon' => 'fas fa-memory',
                'orderNumber' => 20
            )
        ));
        $menu->addChild('Logs', array(
            'route' => 'psc_backend_system_logs_index',
            'extras' => array(
                'icon' => 'fas fa-layer-group',
                'orderNumber' => 30
            )
        ));
    }
}
