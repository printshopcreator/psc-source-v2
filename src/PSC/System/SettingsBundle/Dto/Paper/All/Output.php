<?php

namespace PSC\System\SettingsBundle\Dto\Paper\All;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    public function __construct(array $paper)
    {
        $this->data = $paper;
        $this->count = count($paper);
    }

    /**
     * @var Paper[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\System\SettingsBundle\Model\Paper::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
