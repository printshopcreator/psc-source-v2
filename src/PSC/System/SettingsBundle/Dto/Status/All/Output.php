<?php

namespace PSC\System\SettingsBundle\Dto\Status\All;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    public function __construct(array $order, array $position)
    {
        $this->position = $position;
        $this->order = $order;
    }

    /**
     * @var Status[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\System\SettingsBundle\Model\Status::class)))
     */
    public array $position = [];


    /**
     * @var Status[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\System\SettingsBundle\Model\Status::class)))
     */
    public array $order = [];
}
