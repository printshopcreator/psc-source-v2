<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ColorType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StatusType extends AbstractType
{
    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code', TextType::class, array('label' => 'Nummer'))
            ->add('internalName', TextType::class, array('label' => 'Interner Name'))
            ->add('externalName', TextType::class, array('label' => 'Externer Name'))
            ->add('color', ColorType::class, array('label' => 'Farbe'))
            ->add('typ', ChoiceType::class, array(
                'label' => 'Typ',
                'choices' => array(
                    'Auftragsstatus' => 1,
                    'Positionsstatus' => 2
                )
            ))
            ->add('enable', CheckboxType::class, array('label' => 'Aktiv', 'required' => false))

        ;
    }

    public function getName()
    {
        return 'status';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\System\SettingsBundle\Document\Status',
        ));
    }
}
