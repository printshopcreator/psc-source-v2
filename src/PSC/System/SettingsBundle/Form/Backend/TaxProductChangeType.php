<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Service\Tax;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaxProductChangeType extends AbstractType
{
    protected $taxService;
    protected $shopService;
    public function __construct(Tax $taxService, \PSC\System\SettingsBundle\Service\Shop $shopService)
    {
        $this->taxService = $taxService;
        $this->shopService = $shopService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $shops = $this->shopService->getMyEditableShops();
        $tmp = [];
        foreach ($shops as $shop) {
            $tmp[$shop->getTitle()] = $shop->getUid();
        }

        $builder
            ->add("shop", ChoiceType::class, array(
                'label' => 'Shop',
                'choices' => $tmp,
            ))
            ->add("from", ChoiceType::class, array(
                'label' => 'MwSt. von',
                'choices' => $this->taxService->getTaxesForForm(),
            ))
            ->add("to", ChoiceType::class, array(
                'label' => 'nach',
                'choices' => $this->taxService->getTaxesForForm(),
            ))
        ;
    }

    public function getName()
    {
        return 'taxChange';
    }
}
