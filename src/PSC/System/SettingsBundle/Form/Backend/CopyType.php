<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Form\Backend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Csrf\TokenStorage\TokenStorageInterface;

class CopyType extends AbstractType
{
    /**
     * @var EntityManager
     */
    private $entityManager;
/**
     * @var Security
     */
    private $security;
    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->security = $security;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $shops = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\ShopContact')->myEditableShops($this->security->getUser());
        $builder


           ->add('which', ChoiceType::class, array(
               'choices' => array(
                   'diesen Shop' => '1',
                   'anderen Shop' => '2',
               ),
                'label' => 'In Welchen Shop',
                'required' => true,
                'expanded' => true,
                'multiple' => false,
                'placeholder' => false,
                'attr' => ['class' => 'radio-inline', 'style' => 'border: 0']
            ))
            ->add('shops', EntityType::class, array('label' => 'Shop',
                'choices' => $shops,
                'class' => Shop::class,
                'choice_value' => 'uid',
                'choice_label' => 'title'
            ))
            ->add('save', SubmitType::class, array('label' => 'Speichern'));
    }

    public function getName()
    {
        return 'copy';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
