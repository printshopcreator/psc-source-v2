<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\PluginBundle\Form\Chain\Field;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{
    protected $shop = null;
    public function __construct(Field $fields, FormFactoryInterface $formFactory)
    {
        $this->fields = $fields;
        $this->formFactory = $formFactory;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numberStart', TextType::class, array('required' => false, 'label' => 'Startnummer'))
            ->add('offerNumberStart', TextType::class, array('required' => false, 'label' => 'Startnummer'))
            ->add('numberPattern', TextType::class, array('required' => false, 'label' => 'Nummernkreisformat Bsp: SP-{{ "now"|date("d.m.Y") }}-{{ number }}'))
            ->add('offerNumberPattern', TextType::class, array('required' => false, 'label' => 'Nummernkreisformat Bsp: SP-{{ "now"|date("d.m.Y") }}-{{ number }}'))
            ->add('smtpOwn', CheckboxType::class, array('required' => false, 'label' => 'Eigene Einstellungen verwenden'))
            ->add('smtpTls', CheckboxType::class, array('required' => false, 'label' => 'TLS'))
            ->add('smtpPort', NumberType::class, array('required' => false, 'label' => 'Port'))
            ->add('smtpHost', TextType::class, array('required' => false, 'label' => 'Host'))
            ->add('smtpUsername', TextType::class, array('required' => false, 'label' => 'Username'))
            ->add('smtpPassword', PasswordType::class, array('required' => false, 'label' => 'Password'))
            ->add('extraSettings', TextareaType::class, array('required' => false, 'label' => 'Extra Einstellungen'))
            ->add('maintenanceMode', CheckboxType::class, array('required' => false, 'label' => 'Wartungsmodus'))
            ->add('maintenanceTitle', TextType::class, array('required' => false, 'label' => 'Wartung Titel'))
            ->add('maintenanceText', TextareaType::class, array('required' => false, 'label' => 'Wartung Text'))

            ->add('supporturl', TextType::class, array('required' => false, 'label' => 'Support URL'))
            ->add('supporttoken', TextType::class, array('required' => false, 'label' => 'API Token'))
            ->add('supportprotokoll', CheckboxType::class, array('required' => false, 'attr' => array('checked' => 'checked', 'readonly' => true), 'label' => 'Fehlerprotokolle an PrintshopCreator übermitteln'))
            ->add('supportlogin', CheckboxType::class, array('required' => false, 'attr' => array('checked' => 'checked', 'readonly' => true), 'label' => 'PSC Support Login erlauben'))
            ->add('monitoringkey', TextType::class, array('required' => false, 'label' => 'Monitoring Key'));
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::System) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup(), FormType::class, null, ['mapped' => false]), $options));
        }
        $builder->add('save', SubmitType::class, array('label' => 'Speichern'));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::System) as $field) {
                $field->formPreSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::System) as $field) {
                $field->formPostSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::System) as $field) {
                $field->formPostSubmit($event);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::System) as $field) {
                $field->formPreSubmit($event);
            }
        });
    }

    public function getName()
    {
        return 'settings';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Document\Instance',
        ));
    }
}
