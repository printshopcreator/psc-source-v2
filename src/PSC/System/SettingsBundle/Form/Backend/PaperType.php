<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PaperType extends AbstractType
{
    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('artNr', TextType::class, array('label' => 'ArtNr'))
            ->add('description1', TextType::class, array('label' => 'Beschreibung 1'))
            ->add('description2', TextType::class, array('label' => 'Beschreibung 2', 'required' => false))
            ->add('grammatur', TextType::class, array('label' => 'Grammatur', 'required' => false))
            ->add('preis', TextType::class, array('label' => 'Preis', 'required' => false))
            ->add('offsetFix', TextType::class, array('label' => 'Offset Fix', 'required' => false))
            ->add('offsetVar', TextType::class, array('label' => 'Offset Var', 'required' => false))
            ->add('digitalFix', TextType::class, array('label' => 'Digital Fix', 'required' => false))
            ->add('digitalVar', TextType::class, array('label' => 'Digital Var', 'required' => false))
            ->add('papierTyp1', CheckboxType::class, array('label' => 'Bogen 63 x 88 cm', 'required' => false))
            ->add('papierTyp2', CheckboxType::class, array('label' => 'Bogen 70 x 100 cm', 'required' => false))
            ->add('papierTyp3', CheckboxType::class, array('label' => 'Bogen 50 x 70 cm', 'required' => false))
            ->add('papierTyp4', CheckboxType::class, array('label' => 'Bogen 43 x 61 cm', 'required' => false))
            ->add('papierTyp5', CheckboxType::class, array('label' => 'Bogen 11,4 x 22,9 cm', 'required' => false))
            ->add('papierTyp6', CheckboxType::class, array('label' => 'Bogen 22,9 x 32,4 cm', 'required' => false))
            ->add('papierTyp7', CheckboxType::class, array('label' => 'Bogen 25 x 35,3 cm', 'required' => false))
            ->add('papierTyp8', CheckboxType::class, array('label' => 'Bogen 16,2 x 22,9 cm', 'required' => false))
            ->add('papierTyp9', CheckboxType::class, array('label' => 'Bogen 11 x 22 cm', 'required' => false))
            ->add('papierTyp10', CheckboxType::class, array('label' => 'Bogen 22 x 22 cm', 'required' => false))
            ->add('papierTyp11', CheckboxType::class, array('label' => 'Bogen 65 x 92 cm', 'required' => false))
            ->add('papierTyp12', CheckboxType::class, array('label' => 'Bogen 45 x 64 cm', 'required' => false))
            ->add('papierTyp13', CheckboxType::class, array('label' => 'Bogen 44 x 31,5 cm', 'required' => false))
            ->add('papierTyp14', CheckboxType::class, array('label' => 'Bogen 72 x 102 cm', 'required' => false))
            ->add('umschlagen', CheckboxType::class, array('label' => 'Umschlagen', 'required' => false))
            ->add('happy', CheckboxType::class, array('label' => 'Happy', 'required' => false))
            ->add('eq', CheckboxType::class, array('label' => 'EQ', 'required' => false))
            ->add('sense', CheckboxType::class, array('label' => 'Sense', 'required' => false))
            ->add('sky', CheckboxType::class, array('label' => 'Sky', 'required' => false))
            ->add('glam', CheckboxType::class, array('label' => 'Glam', 'required' => false))
            ->add('post', CheckboxType::class, array('label' => 'Post', 'required' => false))
            ->add('volume', TextType::class, array('label' => 'Volumen', 'required' => false))

            ->add('save', SubmitType::class, array('label' => 'Speichern'));
    }

    public function getName()
    {
        return 'paper';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\paper',
        ));
    }
}
