<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Form\Backend;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchType extends AbstractType
{
    /** @var Session */
    protected $session;
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $session = $this->session;
        $builder
            ->setMethod('POST')
            ->add('term', TextFilterType::class, array(
                'data' => $session->get('paper_search_term'),
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($session) {

                    $session->set('paper_search_term', $values['value']);
                    if (empty($values['value'])) {
                        return null;
                    }
                    $filterQuery->getQueryBuilder()
                        ->andWhere("
                            paper.id LIKE '%" . $values['value'] . "%' OR
                            paper.description1 LIKE '%" . $values['value'] . "%' OR
                            paper.description1 LIKE '%" . $values['value'] . "%' OR
                            paper.grammatur LIKE '%" . $values['value'] . "%' OR
                            paper.artNr LIKE '%" . $values['value'] . "%'
                        ");
                }));
    }

    public function getName()
    {
        return 'item_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false
        ));
    }
}
