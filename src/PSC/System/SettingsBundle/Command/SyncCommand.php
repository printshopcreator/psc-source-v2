<?php

namespace PSC\System\SettingsBundle\Command;

use GuzzleHttp\Client;
use PSC\Shop\EntityBundle\Entity\Domain;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SyncCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('application:settings:sync')
            ->setDescription('Syncs Ftp')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $instances = $this->getContainer()->get('doctrine_mongodb')
            ->getRepository('PSCEntityBundle:Instance')
            ->findAll();

        $client = new Client();

        foreach ($instances as $instance) {
            $response = $client->post(
                $instance->getSyncServer() . '/index.php/json/get/ftp',
                array(
                    'form_params' => array(
                        'uuid' => $instance->getAppId()
                    )
                )
            );

            $data = json_decode($response->getBody(), true);

            var_dump($data);

            if (
                isset($data['success']) && $data['success']
                && isset($data['found']) && $data['found']
            ) {
                if (isset($data['ftp'])) {
                    $instance->setFtpUsername($data['ftp']['username']);
                    $instance->setFtpPassword($data['ftp']['password']);
                }

                $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
                $dm->persist($instance);
                $dm->flush();

                $domains = $this->getContainer()->get('doctrine.orm.entity_manager')
                    ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findAll();

                $temp = array();

                /** @var Domain $domain */
                foreach ($domains as $domain) {
                    $temp[] = array('name' => strtolower($domain->getHost()), 'letsEncrypt' => $domain->isLetsEncrypt());
                }

                var_dump($temp);
                var_dump($instance->getAppId());

                $response = $client->put($instance->getSyncServer() . '/index.php/json/push/domains', array(
                    'json' => array(
                        'uuid' => $instance->getAppId(),
                        'data' => $temp
                        )
                    ));

                var_dump($response->getBody()->getContents());
            }
        }

        $output->writeln("Successfully");
    }
}
