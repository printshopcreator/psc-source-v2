<?php

namespace PSC\System\SettingsBundle\Command;

use PSC\Shop\EntityBundle\Document\Instance;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CreateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('application:create')
            ->setDescription('Creates Application MongoDb')
            ->addArgument(
                'uuid',
                InputArgument::REQUIRED,
                'Uuid für die App'
            )
            ->addArgument(
                'server',
                InputArgument::REQUIRED,
                'Server für die App'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $uuid = $input->getArgument('uuid');
        $server = $input->getArgument('server');

        $instance = $this->getContainer()->get('doctrine_mongodb')
            ->getRepository('PSCEntityBundle:Instance')
            ->findOneByAppId($uuid);

        $str = "Found";

        if (!$instance) {
            $instance = new Instance();
            $instance->setAppId($uuid);
            $instance->setSyncServer($server);

            $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
            $dm->persist($instance);
            $dm->flush();

            $str = "Not Found";
        } else {
            $instance->setSyncServer($server);

            $dm = $this->getContainer()->get('doctrine_mongodb')->getManager();
            $dm->persist($instance);
            $dm->flush();
        }

        $output->writeln($str);
    }
}
