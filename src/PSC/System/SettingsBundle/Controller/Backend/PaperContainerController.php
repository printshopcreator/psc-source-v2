<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 23.02.16
 * Time: 23:23
 */

namespace PSC\System\SettingsBundle\Controller\Backend;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Install;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use PSC\System\SettingsBundle\Form\Backend\PaperContainerType;
use PSC\System\SettingsBundle\Form\Backend\PaperType;
use PSC\System\SettingsBundle\Form\Backend\SearchType;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * PaperContainerController Backend
 *
 * @package    PSC\System\SettingBundle
 * @subpackage Controller
 */
class PaperContainerController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/papercontainer/settings", name="psc_system_paper_container_settings")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function settingsAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, SessionInterface $session)
    {

        $selectedShop = $shopService->getSelectedShop();
/** @var Install $install */
        $install = $selectedShop->getInstall();
        $form = $this->createForm(PaperContainerType::class, $install);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($install);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'Papiercontainer geupdated!!');
        }
        return array(
            'form' => $form->createView(),
            'install' => $install

        );
    }
}
