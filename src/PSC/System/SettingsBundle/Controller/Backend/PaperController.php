<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 23.02.16
 * Time: 23:23
 */

namespace PSC\System\SettingsBundle\Controller\Backend;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PhpParser\Comment\Doc;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use PSC\System\SettingsBundle\Form\Backend\PaperType;
use PSC\System\SettingsBundle\Form\Backend\SearchType;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * PaperController Backend
 *
 * @package    PSC\System\SettingBundle
 * @subpackage Controller
 */
class PaperController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/paper/index", name="psc_system_paper_list")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @param FilterBuilderUpdaterInterface $filterBuilderUpdater
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(
        Request $request,
        SessionInterface $session,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        PaginatorInterface $paginator,
        FilterBuilderUpdaterInterface $filterBuilderUpdater
    ) {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Paper');
        $qb = $userRepository->createQueryBuilder('paper')
            ->andWhere('paper.install = :install_id')
            ->setParameter("install_id", $selectedShop->getInstall())
            ->orderBy('paper.id', 'ASC');
        $form = $this->createForm(SearchType::class);
        if ($request->isMethod('POST')) {
            $session->set('paper_sort_page', 1);
            $form->handleRequest($request);
        }

        $filterBuilderUpdater->addFilterConditions($form, $qb);
        if ($request->get('direction', false)) {
            $session->set('paper_sort_direction', $request->get('direction', 'ASC'));
            $session->set('paper_sort_field', $request->get('sort', 'paper.id'));
        }
        if ($request->get('page', false)) {
            $session->set('paper_sort_page', $request->get('page', 1));
        }

        $pagination = $paginator->paginate($query = $qb->getQuery(), $request->query->getInt('page', $session->get('paper_sort_page', 1)), 15, [
                'defaultSortFieldName' => $session->get('paper_sort_field', 'paper.id'),
                'defaultSortDirection' => $session->get('paper_sort_direction', 'ASC')
            ]);
        return array(
            'pagination' => $pagination,
            'form' => $form->createView()
        );
    }

    /**
     * Default Seite
     *
     * @Route("/paper/edit/{uuid}", name="psc_system_paper_edit")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, EntityManagerInterface $entityManager, DocumentManager $documentManager, $uuid)
    {

        /** @var Paper $paper */
        $paper = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Paper')
            ->findOneBy(array('id' => $uuid));
/** @var \PSC\Shop\EntityBundle\Document\Paper $paperDoc */
        $paperDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Paper')
            ->findOneBy(array('uid' => (string)$paper->getId()));
        if (!$request->isMethod('POST') && $paperDoc) {
            $paper->setPapierTyp1($paperDoc->isPapierTyp1());
            $paper->setPapierTyp2($paperDoc->isPapierTyp2());
            $paper->setPapierTyp3($paperDoc->isPapierTyp3());
            $paper->setPapierTyp4($paperDoc->isPapierTyp4());
            $paper->setPapierTyp5($paperDoc->isPapierTyp5());
            $paper->setPapierTyp6($paperDoc->isPapierTyp6());
            $paper->setPapierTyp7($paperDoc->isPapierTyp7());
            $paper->setPapierTyp8($paperDoc->isPapierTyp8());
            $paper->setPapierTyp9($paperDoc->isPapierTyp9());
            $paper->setPapierTyp10($paperDoc->isPapierTyp10());
            $paper->setPapierTyp11($paperDoc->isPapierTyp11());
            $paper->setPapierTyp12($paperDoc->isPapierTyp12());
            $paper->setPapierTyp13($paperDoc->isPapierTyp13());
            $paper->setPapierTyp14($paperDoc->isPapierTyp14());
            $paper->setUmschlagen($paperDoc->isUmschlagen());
            $paper->setEq($paperDoc->isEq());
            $paper->setHappy($paperDoc->isHappy());
            $paper->setSense($paperDoc->isSense());
            $paper->setSky($paperDoc->isSky());
            $paper->setGlam($paperDoc->isGlam());
            $paper->setPost($paperDoc->isPost());
            $paper->setVolume($paperDoc->getVolume());
        } elseif (!$paperDoc) {
            $paperDoc = new \PSC\Shop\EntityBundle\Document\Paper();
            $paperDoc->setUid($paper->getId());
        }

        $form = $this->createForm(PaperType::class, $paper);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($paper);
            $entityManager->flush();
            $paperDoc->setPapierTyp1($paper->getPapierTyp1());
            $paperDoc->setPapierTyp2($paper->getPapierTyp2());
            $paperDoc->setPapierTyp3($paper->getPapierTyp3());
            $paperDoc->setPapierTyp4($paper->getPapierTyp4());
            $paperDoc->setPapierTyp5($paper->getPapierTyp5());
            $paperDoc->setPapierTyp6($paper->getPapierTyp6());
            $paperDoc->setPapierTyp7($paper->getPapierTyp7());
            $paperDoc->setPapierTyp8($paper->getPapierTyp8());
            $paperDoc->setPapierTyp9($paper->getPapierTyp9());
            $paperDoc->setPapierTyp10($paper->getPapierTyp10());
            $paperDoc->setPapierTyp11($paper->getPapierTyp11());
            $paperDoc->setPapierTyp12($paper->getPapierTyp12());
            $paperDoc->setPapierTyp13($paper->getPapierTyp13());
            $paperDoc->setPapierTyp14($paper->getPapierTyp14());
            $paperDoc->setUmschlagen($paper->getUmschlagen());
            $paperDoc->setHappy($paper->getHappy());
            $paperDoc->setEq($paper->getEq());
            $paperDoc->setSense($paper->getSense());
            $paperDoc->setSky($paper->getSky());
            $paperDoc->setGlam($paper->getGlam());
            $paperDoc->setPost($paper->getPost());
            $paperDoc->setVolume($paper->getVolume());
            $documentManager->persist($paperDoc);
            $documentManager->flush();
            return $this->redirectToRoute('psc_system_paper_list');
        }

        return array(
            'form' => $form->createView(),
            'paper' => $paper,

        );
    }

    /**
     * Default Seite
     *
     * @Route("/paper/create", name="psc_system_paper_create")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, DocumentManager $documentManager)
    {

        $selectedShop = $shopService->getSelectedShop();
/** @var Paper $paper */
        $paper = new Paper();
        $paper->setUuid(Uuid::uuid4());
        $paper->setInstall($selectedShop->getInstall()->getUid());
/** @var \PSC\Shop\EntityBundle\Document\Paper $paperDoc */
        $paperDoc = new \PSC\Shop\EntityBundle\Document\Paper();
        $form = $this->createForm(PaperType::class, $paper);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($paper);
            $entityManager->flush();
            $paperDoc->setUid($paper->getId());
            $paperDoc->setPapierTyp1($paper->getPapierTyp1());
            $paperDoc->setPapierTyp2($paper->getPapierTyp2());
            $paperDoc->setPapierTyp3($paper->getPapierTyp3());
            $paperDoc->setPapierTyp4($paper->getPapierTyp4());
            $paperDoc->setPapierTyp5($paper->getPapierTyp5());
            $paperDoc->setPapierTyp6($paper->getPapierTyp6());
            $paperDoc->setPapierTyp7($paper->getPapierTyp7());
            $paperDoc->setPapierTyp8($paper->getPapierTyp8());
            $paperDoc->setPapierTyp9($paper->getPapierTyp9());
            $paperDoc->setPapierTyp10($paper->getPapierTyp10());
            $paperDoc->setPapierTyp11($paper->getPapierTyp11());
            $paperDoc->setPapierTyp12($paper->getPapierTyp12());
            $paperDoc->setPapierTyp13($paper->getPapierTyp13());
            $paperDoc->setPapierTyp14($paper->getPapierTyp14());
            $paperDoc->setUmschlagen($paper->getUmschlagen());
            $paperDoc->setHappy($paper->getHappy());
            $paperDoc->setEq($paper->getEq());
            $paperDoc->setSense($paper->getSense());
            $paperDoc->setSky($paper->getSky());
            $paperDoc->setGlam($paper->getGlam());
            $paperDoc->setPost($paper->getPost());
            $paperDoc->setVolume($paper->getVolume());
            $documentManager->persist($paperDoc);
            $documentManager->flush();
            return $this->redirectToRoute('psc_system_paper_list');
        }

        return array(
            'form' => $form->createView(),
            'paper' => $paper

        );
    }

    /**
     * remove
     *
     * @Route("/paper/delete/{uuid}", name="psc_system_paper_delete")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(Request $request, EntityManagerInterface $entityManager, DocumentManager $documentManager, SessionInterface $session, $uuid)
    {
        /** @var Paper $paper */
        $paper = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Paper')
            ->findOneBy(array('id' => $uuid));
/** @var \PSC\Shop\EntityBundle\Document\Paper $paperDoc */
        $paperDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Paper')
            ->findOneBy(array('uid' => (string)$paper->getId()));
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $title = $paper->getDescription1();
                $entityManager->remove($paper);
                $entityManager->flush();
                if ($paperDoc) {
                    $documentManager->remove($paperDoc);
                    $documentManager->flush();
                }

                $session->getFlashBag()->add('success', 'Productgroup \'' . $title . '\' has been deleted!');
                return $this->redirectToRoute('psc_system_paper_list');
            }
            return $this->redirectToRoute('psc_system_paper_list');
        }


        return array(
            'paper' => $paper,
            'form' => $form->createView()
        );
    }
}
