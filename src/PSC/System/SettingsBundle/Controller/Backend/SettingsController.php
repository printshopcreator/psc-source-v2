<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use GuzzleHttp\Client;
use PSC\Backend\DomainBundle\Service\DomainSync;
use PSC\Shop\EntityBundle\Document\Instance;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Form\Backend\SettingsType;
use PSC\System\SettingsBundle\Form\Backend\SmtpTestType;
use PSC\System\SettingsBundle\PSCSystemSettingsBundle;
use PSC\System\SettingsBundle\Service\Log;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * ShopController Backend
 *
 * @package    PSC\Shop\SettingBundle
 * @subpackage Controller
 */
class SettingsController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/settings/index", name="psc_backend_system_settings_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function indexAction(Field $fieldService, Request $request, DocumentManager $documentManager, Log $logService)
    {

        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::System);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::System);
        $instance = $documentManager
            ->getRepository('PSCEntityBundle:Instance')
            ->findOneBy(['appId' => '1']);
        $display = false;
        if ($instance === null) {
            $instance = new Instance();
            $instance->setAppId("1");
        }
        $password = $instance->getSmtpPassword();
        $form = $this->createForm(SettingsType::class, $instance);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($instance->getSmtpPassword() == "") {
                $instance->setSmtpPassword($password);
            }
            $documentManager->persist($instance);
            $documentManager->flush();
            $logService->createLogEntry(new \PSC\Shop\EntityBundle\Entity\Shop(), $this->getUser(), LogEntry::INFO, PSCSystemSettingsBundle::class, "", "Systemsettings saved");
        }


        if (getenv('ftpUsername')) {
            $display = true;
            $instance->setFtpUsername(getenv('ftpUsername'));
            $instance->setFtpPassword(getenv('ftpPassword'));
            return array('form' => $form->createView(), 'instance' => $instance, 'display' => $display, 'ip' => getenv('ftpIp'), 'host' => getenv('ftpHost') . ':' . getenv('ftpPort'),
                'customFields' => $customFields,
                'customGroups' => $customGroups);
        } else {
            $instances = $documentManager
                ->getRepository('PSCEntityBundle:Instance')
                ->findAll();
            if (isset($instances[0])) {
                $instance = $instances[0];
                $display = true;
            }

            $url = parse_url($instance->getSyncServer());
            if (isset($url['host'])) {
                return array('form' => $form->createView(), 'instance' => $instance, 'display' => $display, 'host' => gethostbyname($url['host']), 'ip' => ($url['ip']));
            }
            return array('form' => $form->createView(), 'instance' => $instance, 'display' => $display, 'host' => "127.0.0.1", 'ip' => "127.0.0.1",
                'customFields' => $customFields,
                'customGroups' => $customGroups);
        }
    }

    /**
     * Default Seite
     *
     * @Route("/settings/smtptest", name="psc_backend_system_settings_smtptest")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function smtpTestAction(Request $request, DocumentManager $documentManager, MailerInterface $mailer, Shop $shopService)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $data = [
            'from' => $selectedShop->getBetreiberEmail(),
            'to' => $this->getUser()->getUsername(),
            'subject' => 'Testmail',
            'text' => 'Test'
        ];
        $form = $this->createForm(SmtpTestType::class, $data);
        $form->handleRequest($request);
        $error = "";
        $success = true;
        $send = false;
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $send = true;
            $message = (new Email())
                ->subject($data['subject'])
                ->from($data['from'])
                ->to($data['to'])
                ->text($data['text']);
            try {
                $mailer->send($message);
            } catch (\Exception $e) {
                $error = $e->getMessage();
                $success = false;
            }
        }

        return ['form' => $form->createView(), 'error' => $error, 'success' => $success, 'send' => $send];
    }

    /**
     * dataAction
     *
     * @Route("/settings/upgrade", name="psc_backend_system_settings_upgrade")
     * @Security("is_granted('ROLE_SHOP')")
     *
     */
    public function upgradeAction(DomainSync $sync)
    {
        $sync->upgrade();
        return new JsonResponse(['success' => true]);
    }
}
