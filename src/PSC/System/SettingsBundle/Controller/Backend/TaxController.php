<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 23.02.16
 * Time: 23:23
 */

namespace PSC\System\SettingsBundle\Controller\Backend;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpParser\Comment\Doc;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Document\Status;
use PSC\System\SettingsBundle\Document\Tax;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use PSC\System\SettingsBundle\Form\Backend\PaperType;
use PSC\System\SettingsBundle\Form\Backend\SearchType;
use PSC\System\SettingsBundle\Form\Backend\StatusType;
use PSC\System\SettingsBundle\Form\Backend\TaxProductChangeType;
use PSC\System\SettingsBundle\Form\Backend\TaxType;
use PSC\System\SettingsBundle\PSCSystemSettingsBundle;
use PSC\System\SettingsBundle\Service\Log;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * StatusController Backend
 *
 * @package    PSC\System\SettingBundle
 * @subpackage Controller
 */
class TaxController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/tax/index", name="psc_system_tax_list")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param \PSC\System\SettingsBundle\Service\Tax $taxService
     * @return array
     */
    public function indexAction(Request $request, EntityManagerInterface $entityManager, SessionInterface $session, \PSC\System\SettingsBundle\Service\Tax $taxService, Log $logService)
    {
        $data = [];
        $form = $this->createForm(TaxProductChangeType::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if ($data['from'] != $data['to']) {
                $entityManager
                    ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
                    ->updateTaxes($data['shop'], $data['from'], $data['to']);
                $entityManager
                    ->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')
                    ->updateTaxes($data['shop'], $data['from'], $data['to']);
                $entityManager
                    ->getRepository('PSC\Shop\EntityBundle\Entity\Payment')
                    ->updateTaxes($data['shop'], $data['from'], $data['to']);
                $session->getFlashBag()->add('success', 'Tax has been changed!');
                $logService->createLogEntry(new \PSC\Shop\EntityBundle\Entity\Shop(), $this->getUser(), LogEntry::INFO, PSCSystemSettingsBundle::class, "", "Tax changed saved", $data);
            }
        }
        return array(
            'taxes' => $taxService->getTaxes(),
            'form' => $form->createView()
        );
    }

    /**
     * Create
     *
     * @Route("/tax/create", name="psc_system_tax_create")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function createAction(Request $request, DocumentManager $documentManager, SessionInterface $session)
    {
        $tax = new Tax();
        $form = $this->createForm(TaxType::class, $tax);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($tax);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Tax has been created!');
            return $this->redirectToRoute('psc_system_tax_list');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * Edit Seite
     *
     * @Route("/tax/edit/{uuid}", name="psc_system_tax_edit")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function editAction(Request $request, DocumentManager $documentManager, SessionInterface $session, $uuid)
    {
        /** @var \PSC\System\SettingsBundle\Document\Status $tax */
        $tax = $documentManager
            ->getRepository('PSC\System\SettingsBundle\Document\Tax')
            ->findOneBy(array('id' => (string)$uuid));
        $form = $this->createForm(TaxType::class, $tax);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($tax);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Tax has been created!');
            return $this->redirectToRoute('psc_system_tax_list');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * remove
     *
     * @Route("/tax/delete/{uuid}", name="psc_system_tax_delete")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(Request $request, DocumentManager $documentManager, SessionInterface $session, $uuid)
    {

        /** @var \PSC\System\SettingsBundle\Document\Tax $tax */
        $tax = $documentManager
            ->getRepository('PSC\System\SettingsBundle\Document\Tax')
            ->findOneBy(array('id' => (string)$uuid));
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $documentManager->remove($tax);
                $documentManager->flush();
                $session->getFlashBag()->add('success', 'Tax has been deleted!');
                return $this->redirectToRoute('psc_system_tax_list');
            }
            return $this->redirectToRoute('psc_system_tax_list');
        }


        return array(
            'tax' => $tax,
            'form' => $form->createView()
        );
    }
}
