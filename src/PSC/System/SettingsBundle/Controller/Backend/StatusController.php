<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 23.02.16
 * Time: 23:23
 */

namespace PSC\System\SettingsBundle\Controller\Backend;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Document\Status;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use PSC\System\SettingsBundle\Form\Backend\PaperType;
use PSC\System\SettingsBundle\Form\Backend\SearchType;
use PSC\System\SettingsBundle\Form\Backend\StatusType;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * StatusController Backend
 *
 * @package    PSC\System\SettingBundle
 * @subpackage Controller
 */
class StatusController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/status/index", name="psc_system_status_list")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param \PSC\System\SettingsBundle\Service\Status $statusService
     * @return array
     */
    public function indexAction(Request $request, DocumentManager $documentManager, \PSC\System\SettingsBundle\Service\Status $statusService)
    {

        $statusOrder = $documentManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => Status::$ORDER]);
        if (count($statusOrder) === 0) {
            $statusService->importInitalData();
        }

        $statusOrder = $documentManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => Status::$ORDER]);
        $statusPosition = $documentManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => Status::$POSITION]);
        return array(
            'statusOrder' => $statusOrder,
            'statusPosition' => $statusPosition
        );
    }

    /**
     * Create Seite
     *
     * @Route("/status/create", name="psc_system_status_create")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function createAction(Request $request, DocumentManager $documentManager, SessionInterface $session)
    {
        $status = new Status();
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($status);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Status has been created!');
            return $this->redirectToRoute('psc_system_status_list');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * Edit Seite
     *
     * @Route("/status/edit/{uuid}", name="psc_system_status_edit")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function editAction(Request $request, DocumentManager $documentManager, SessionInterface $session, $uuid)
    {
        /** @var \PSC\System\SettingsBundle\Document\Status $status */
        $status = $documentManager
            ->getRepository('PSC\System\SettingsBundle\Document\Status')
            ->findOneBy(array('id' => (string)$uuid));
        $form = $this->createForm(StatusType::class, $status);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($status);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Status has been created!');
            return $this->redirectToRoute('psc_system_status_list');
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * remove
     *
     * @Route("/status/delete/{uuid}", name="psc_system_status_delete")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(Request $request, DocumentManager $documentManager, SessionInterface $session, $uuid)
    {

        /** @var \PSC\System\SettingsBundle\Document\Status $status */
        $status = $documentManager
            ->getRepository('PSC\System\SettingsBundle\Document\Status')
            ->findOneBy(array('id' => (string)$uuid));
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $documentManager->remove($status);
                $documentManager->flush();
                $session->getFlashBag()->add('success', 'Status has been deleted!');
                return $this->redirectToRoute('psc_system_status_list');
            }
            return $this->redirectToRoute('psc_system_status_list');
        }


        return array(
            'status' => $status,
            'form' => $form->createView()
        );
    }
}
