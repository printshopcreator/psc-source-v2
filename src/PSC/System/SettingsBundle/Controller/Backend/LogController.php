<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 23.02.16
 * Time: 23:23
 */

namespace PSC\System\SettingsBundle\Controller\Backend;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PhpParser\Comment\Doc;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use PSC\System\SettingsBundle\Form\Backend\PaperType;
use PSC\System\SettingsBundle\Form\Backend\SearchType;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * PaperController Backend
 *
 * @package    PSC\System\SettingBundle
 * @subpackage Controller
 */
class LogController extends AbstractController
{
    /**
     * Log Index
     *
     * @Route("/log/index", name="psc_backend_system_logs_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     */
    public function indexAction(DocumentManager $mongoManager, PaginatorInterface $paginator, Request $request)
    {
        $logs = $mongoManager
            ->createQueryBuilder('PSCSystemSettingsBundle:LogEntry')->sort('created', 'desc');
        ;

        $pagination = $paginator->paginate($query = $logs->getQuery(), $request->query->getInt('page', 1), 50);
        return ['pagination' => $pagination];
    }
}
