<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Document\Instance;
use PSC\Shop\QueueBundle\Service\Queue\Manager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

/**
 * FtpController
 *
 * @package    PSC\Backend\Production
 * @subpackage Controller
 */
class FtpController extends AbstractController
{
    /**
     * Meine Ftp Daten
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Template()
     *
     * @param DocumentManager $mongoManager
     * @return array
     */
    public function myDataAction(DocumentManager $mongoManager)
    {

        $instance = new Instance();
        $display = false;
        $displaySupport = false;
        $suppFirst = "https://helpdesk.printshopcreator.de/bridge/simple?X-AUTH-TOKEN=";
        $suppUrl = "https://helpdesk.printshopcreator.de/bridge/extern?X-AUTH-TOKEN=";
        if (getenv('supportUrl')) {
            $displaySupport = true;
            $suppUrl =  $suppUrl . getenv('supportUrl');
            $suppFirst =  $suppFirst . getenv('supportUrl');
        }

        if (getenv('ftpUsername')) {
            $display = true;
            $instance->setFtpUsername(getenv('ftpUsername'));
            $instance->setFtpPassword(getenv('ftpPassword'));
            return array('displaySupport' => $displaySupport, 'supportUrlFirst' => $suppFirst, 'supportUrl' => $suppUrl,'instance' => $instance, 'display' => $display, 'ip' => getenv('ftpIp'), 'host' => getenv('ftpHost') . ':' . getenv('ftpPort'));
        } else {
            $instances = $mongoManager
                ->getRepository('PSCEntityBundle:Instance')
                ->findAll();
            if (isset($instances[0])) {
                $instance = $instances[0];
                $display = true;
            }

            $url = parse_url($instance->getSyncServer());
            if (isset($url['host'])) {
                return array('displaySupport' => $displaySupport, 'supportUrlFirst' => $suppFirst, 'supportUrl' => $suppUrl, 'instance' => $instance, 'display' => $display, 'host' => gethostbyname($url['host']), 'ip' => ($url['ip']));
            }
            return array('displaySupport' => $displaySupport, 'supportUrlFirst' => $suppFirst, 'supportUrl' => $suppUrl, 'instance' => $instance, 'display' => $display, 'host' => "127.0.0.1", 'ip' => "127.0.0.1");
        }
    }
}
