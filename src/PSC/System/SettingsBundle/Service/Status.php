<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Color
 *
 * @author Thomas Peterson
 */
class Status
{
    public static $InternalName = 1;
    public static $ExternalName = 2;

    protected $mongoManager;
    protected $securityContext;

    public function __construct(ManagerRegistry $mongoManager, TokenStorageInterface $securityContext)
    {
        $this->mongoManager = $mongoManager->getManager();
        $this->securityContext = $securityContext;
    }

    public function getOrderStatusAsArray($mode = 1)
    {

        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$ORDER, 'enable' => true], ['code' => 'ASC']);

        $temp = array();

        /** @var \PSC\System\SettingsBundle\Document\Status $status */
        foreach ($statusOrder as $status) {
            if ($mode === self::$InternalName) {
                $temp[$status->getInternalName()] = $status->getCode();
            } else {
                $temp[$status->getExternalName()] = $status->getCode();
            }
        }

        return $temp;
    }

    public function getOrderStatuse()
    {

        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$ORDER, 'enable' => true]);

        return $statusOrder;
    }

    public function getPositionStatusAsArray($mode = 1)
    {

        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$POSITION, 'enable' => true], ['code' => 'ASC']);

        $temp = array();

        /** @var \PSC\System\SettingsBundle\Document\Status $status */
        foreach ($statusOrder as $status) {
            if ($mode === self::$InternalName) {
                $temp[$status->getInternalName()] = $status->getCode();
            } else {
                $temp[$status->getExternalName()] = $status->getCode();
            }
        }

        return $temp;
    }

    public function getOrderPosStatuse()
    {

        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$POSITION, 'enable' => true]);

        return $statusOrder;
    }

    public function getStatusText($key, $mode = 1)
    {

        /** @var \PSC\System\SettingsBundle\Document\Status $statusOrder */
        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findOneBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$ORDER, 'code' => $key]);

        if (!$statusOrder) {
            return "not found";
        }

        if ($mode === self::$InternalName) {
            return $statusOrder->getInternalName();
        }

        return $statusOrder->getExternalName();
    }

    public function getStatusColor($key)
    {

        /** @var \PSC\System\SettingsBundle\Document\Status $statusOrder */
        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findOneBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$ORDER, 'code' => $key]);

        if (!$statusOrder) {
            return "not found";
        }

        return $statusOrder->getColor();
    }

    public function getPosStatusColor($key)
    {

        /** @var \PSC\System\SettingsBundle\Document\Status $statusOrder */
        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findOneBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$POSITION, 'code' => $key]);

        if (!$statusOrder) {
            return "not found";
        }

        return $statusOrder->getColor();
    }

    public function getPosStatusText($key, $mode = 1)
    {
        /** @var \PSC\System\SettingsBundle\Document\Status $statusOrder */
        $statusOrder = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findOneBy(['typ' => \PSC\System\SettingsBundle\Document\Status::$POSITION, 'code' => $key]);

        if (!$statusOrder) {
            return "not found";
        }

        if ($mode === self::$InternalName) {
            return $statusOrder->getInternalName();
        }

        return $statusOrder->getExternalName();
    }

    public function importInitalData()
    {

        if (($handle = fopen(__DIR__ . '/../InitalData/status.csv', "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                $status = new \PSC\System\SettingsBundle\Document\Status();
                $status->setCode($data[0]);
                $status->setTyp($data[1]);
                $status->setInternalName($data[2]);
                $status->setExternalName($data[2]);
                $status->setEnable(true);

                $this->mongoManager->persist($status);
            }
            fclose($handle);

            $this->mongoManager->flush();
        }
    }
}
