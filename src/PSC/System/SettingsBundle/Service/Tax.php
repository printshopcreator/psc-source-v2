<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Color
 *
 * @author Thomas Peterson
 */
class Tax
{
    protected $mongoManager;

    public function __construct(ManagerRegistry $mongoManager)
    {
        $this->mongoManager = $mongoManager->getManager();
    }

    public function getTaxes()
    {
        $taxes = $this->mongoManager
            ->getRepository('PSCSystemSettingsBundle:Tax')
            ->findBy([]);

        if (count($taxes) === 0) {
            $this->importInitalData();

            $taxes = $this->mongoManager
                ->getRepository('PSCSystemSettingsBundle:Tax')
                ->findBy([]);
        }

        return $taxes;
    }

    public function getTaxesForForm()
    {
        $taxes = $this->getTaxes();
        $tmp = [];

        foreach ($taxes as $tax) {
            if ($tax->isEnable()) {
                $tmp[$tax->getName()] = $tax->getValue();
            }
        }

        return $tmp;
    }

    public function importInitalData()
    {

        $tmp = array('20%' => 20, '19%' => 19, '10%' => 10, '7%' => 7, '0%' => 0);

        foreach ($tmp as $key => $row) {
            $tax = new \PSC\System\SettingsBundle\Document\Tax();
            $tax->setName($key);
            $tax->setValue($row);
            $tax->setEnable(true);

            $this->mongoManager->persist($tax);
        }
        $this->mongoManager->flush();
    }
}
