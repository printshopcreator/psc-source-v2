<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Layoutdesigndata;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Upload;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Config\FileLocator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use ZipArchive;

/**
 * Package
 *
 * @author Thomas Peterson
 */
class TemplateVars
{
    private $entityManager;
    private $mongoManager;
    private $tokenStorage;
    private $twig;
    private $projectDir;

    /** @var \PSC\Shop\EntityBundle\Entity\Order */
    protected $order;
    /**
     * @var PaperDB
     */
    protected $paperDb;

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return Orderpos
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /** @var boolean */
    protected $loaded = false;

    /** @var Orderpos */
    protected $positions;

    protected $twigVars = array();

    public function __construct($projectDir, EntityManagerInterface $em, TokenStorageInterface $tokenStorage, DocumentManager $mongoManager, Environment $twig, PaperDB $paperDb)
    {
        $this->entityManager = $em;
        $this->tokenStorage = $tokenStorage;
        $this->mongoManager = $mongoManager;
        $this->projectDir = $projectDir;
        $this->twig = $twig;
        $this->paperDb = $paperDb;
    }


    public function loadOrder($uuid)
    {

        if ($this->loaded && $this->order->getUuid() == $uuid) {
            return true;
        }

        $orderRepo = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');

        $this->order = $orderRepo->findOneBy(array('uuid' => $uuid));
        $this->positions = $this->order->getPositions();
        $this->generateTwigVars();

        $this->loaded = true;

        return true;
    }

    private function generateTwigVars()
    {

        /** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
        $orderObj = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('uid' => (string) $this->order->getUid()));

        $contact = $this->order->getContact();

        /** @var \PSC\Shop\EntityBundle\Document\Contact $contactObj */
        $contactObj = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Contact')
            ->findOneBy(array('uid' => (string) $contact->getId()));
        $invoiceAddress = null;
        $deliveryAddress = null;
        $senderAddress = null;

        if ($orderObj) {
            $invoiceAddress = $orderObj->getInvoiceAddressSaved();
            $deliveryAddress = $orderObj->getDeliveryAddressSaved();
            $senderAddress = $orderObj->getSenderAddressSaved();
        }
        if ($invoiceAddress === null) {
            $invoiceAddress = $this->order->getInvoiceAddress();
        }
        if ($deliveryAddress === null) {
            $deliveryAddress = $this->order->getDeliveryAddress();
        }
        if ($senderAddress === null) {
            $senderAddress = $this->order->getSenderAddress();
        }

        $shop = $this->order->getShop();
        $shipping = $this->order->getShippingType();
        $payment = $this->order->getPaymentType();

        $positions = array();

        $mwert = array();

        /** @var Orderpos $pos */
        foreach ($this->positions as $pos) {

            /** @var Position $objDoc */
            $objDoc = $this->mongoManager
                ->getRepository('PSCEntityBundle:Position')
                ->findOneBy(['uid' => (string)$pos->getId()]);

            /** @var \TP_Basket_Item $objPosition */
            $objPosition = unserialize(($pos->getData()));
            $paperContainer = new PaperContainer();
            $paperContainer->parse(simplexml_load_string($shop->getInstall()->getPaperContainer()));

            /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
            $productDoc = $this->mongoManager
                ->getRepository('PSC\Shop\EntityBundle\Document\Product')
                ->findOneBy(array('uid' => (string) $pos->getProduct()->getUid()));

            if (isset($mwert[$pos->getProduct()->getMwert()])) {
                $mwert[$pos->getProduct()->getMwert()] = ['netto' => $pos->getPriceAllNetto() + $mwert[$pos->getProduct()->getMwert()]['netto'], 'brutto' => $pos->getPriceAllBrutto() + $mwert[$pos->getProduct()->getMwert()]['brutto'], 'steuer' => $pos->getPriceAllSteuer() + $mwert[$pos->getProduct()->getMwert()]['steuer']];
            } else {
                $mwert[$pos->getProduct()->getMwert()] = ['netto' => $pos->getPriceAllNetto(), 'brutto' => $pos->getPriceAllBrutto(), 'steuer' => $pos->getPriceAllSteuer()];
            }

            if (trim($pos->getProduct()->getScaledPrice()) != "") {
                $tmp = array(
                    'pos' => $pos->getPos(),
                    'count' => $objPosition->getOptions()['auflage'],
                    'product' => $pos->getProduct(),
                    'productObj' => $productDoc,
                    'rabatte' => $objPosition->getRabatte(),
                    'obj' => $pos,
                    'objDoc' => $objDoc,
                    'engine' => false,
                    'calc' => false,
                    'set' => false
                );
            } elseif ($pos->hasCalcXml()) {
                $engine = new Engine();
                $engine->setPaperRepository($this->paperDb);
                $engine->setPaperContainer($paperContainer);
                if ($shop->getInstall()->getCalcTemplates()) {
                    $engine->setTemplates('<root>' . $shop->getInstall()->getCalcTemplates() . '</root>');
                }
                $engine->loadString($pos->getCalcXml());
                $engine->setFormulas($shop->getFormel());
                $engine->setParameters($shop->getParameter());
                $engine->setVariables($objPosition->getOptions());
                if (isset($objPosition->getOptions()['kalk_artikel'])) {
                    $engine->setActiveArticle($objPosition->getOptions()['kalk_artikel']);
                }
                $articleCalc = $engine->getArticle();

                $tmp = array(
                    'pos' => $pos->getPos(),
                    'count' => $pos->getCount(),
                    'product' => $pos->getProduct(),
                    'productObj' => $productDoc,
                    'rabatte' => $objPosition->getRabatte(),
                    'obj' => $pos,
                    'objDoc' => $objDoc,
                    'calc' => $articleCalc,
                    'engine' => $engine,
                    'set' => false
                );
            } else {
                $tmp = array(
                    'pos' => $pos->getPos(),
                    'count' => $pos->getCount(),
                    'product' => $pos->getProduct(),
                    'productObj' => $productDoc,
                    'rabatte' => [],
                    'obj' => $pos,
                    'objDoc' => $objDoc,
                    'engine' => false,
                    'calc' => false,
                    'set' => false
                );
            }

            $setConfig = $pos->getProduct()->getSetConfig();

            $productRepo = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Product');

            if ($pos->getProduct()->getOriginalProduct()) {
                $orgProduct = $productRepo->findOneBy(['uid' => $pos->getProduct()->getOriginalProduct()]);
                if ($orgProduct) {
                    $setConfig = $orgProduct->getSetConfig();
                }
            }

            if (count($setConfig) > 0) {
                $productSets = array();
                foreach ($setConfig as $conf) {
                    /** @var Product $product */
                    $product = $productRepo->findOneBy(['uid' => $conf->article_id]);

                    if (isset($setConfig[$product->getId()])) {
                        $productSets[] = [
                            'title' => $product->getTitle(),
                            'articleNrIntern' => $product->getNrIntern(),
                            'articleNrExtern' => $product->getNrExtern(),
                            'textArt' => $product->getTextArt(),
                            'available' => $setConfig[$product->getId()],
                            'count' => $pos->getCount(),
                        ];
                    } else {
                        $productSets[] = [
                            'title' => $product->getTitle(),
                            'articleNrIntern' => $product->getNrIntern(),
                            'articleNrExtern' => $product->getNrExtern(),
                            'textArt' => $product->getTextArt(),
                            'available' => $pos->getCount(),
                            'count' => $pos->getCount(),
                        ];
                    }
                }

                $tmp['set'] = $productSets;
            }

            if ($objDoc && $objDoc->getLayouterId() != "") {
                /** @var Layoutdesigndata $layoutData */
                $layoutData = $this->entityManager
                    ->getRepository('PSC\Shop\EntityBundle\Entity\Layoutdesigndata')->findOneBy(array('uuid' => $objDoc->getLayouterId()));

                if ($layoutData) {
                    $config = $layoutData->getDesign();

                    if (isset($config['products'])) {
                        $productSets = array();

                        foreach ($config['products'] as $art) {
                            if (isset($art['uid'])) {
                                /** @var Product $product */
                                $product = $productRepo->findOneBy(['uid' => $art['uid']]);
                                if ($product) {
                                    $productSets[] = [
                                        'title' => $product->getTitle(),
                                        'articleNrIntern' => $product->getNrIntern(),
                                        'articleNrExtern' => $product->getNrExtern(),
                                        'textArt' => $product->getTextArt(),
                                        'available' => $setConfig[$product->getId()],
                                        'count' => $art['amount'],
                                    ];
                                }
                            }

                            if (isset($art['uuid'])) {
                                /** @var Product $product */
                                $product = $productRepo->findOneBy(['uuid' => $art['uuid']]);
                                if ($product) {
                                    $productSets[] = [
                                        'title' => $product->getTitle(),
                                        'articleNrIntern' => $product->getNrIntern(),
                                        'articleNrExtern' => $product->getNrExtern(),
                                        'textArt' => $product->getTextArt(),
                                        'available' => $setConfig[$product->getId()],
                                        'count' => $art['amount'],
                                    ];
                                }
                            }
                        }


                        $tmp['set'] = $productSets;
                    }
                }
            }

            $positions[$pos->getUuid()] = $tmp;
        }

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shop */
        $shopDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string) $shop->getUid()));

        if ($this->order->getZahlKosten() != 0) {
            if (isset($mwert[$payment->getTaxClass()])) {
                $mwert[$payment->getTaxClass()] = ['netto' => $mwert[$payment->getTaxClass()]['netto'] + $this->order->getZahlKosten(), 'brutto' => $mwert[$payment->getTaxClass()]['brutto'] + $this->order->getZahlKosten() + ($this->order->getZahlKosten() / 100 * $payment->getTaxClass()) , 'steuer' => $mwert[$payment->getTaxClass()]['steuer'] + ($this->order->getZahlKosten() / 100 * $payment->getTaxClass())];
            } else {
                $mwert[$payment->getTaxClass()] = ['netto' => $this->order->getZahlKosten(), 'brutto' => $this->order->getZahlKosten() + ($this->order->getZahlKosten() / 100 * $payment->getTaxClass()), 'steuer' => $this->order->getZahlKosten() / 100 * $payment->getTaxClass()];
            }
        }
        if ($this->order->getVersandKosten() != 0) {
            if (isset($mwert[$shipping->getTaxClass()])) {
                $mwert[$shipping->getTaxClass()] = ['netto' => $mwert[$shipping->getTaxClass()]['netto'] + $this->order->getVersandKosten(), 'brutto' => $mwert[$shipping->getTaxClass()]['brutto'] + $this->order->getVersandKosten() + ($this->order->getVersandKosten() / 100 * $payment->getTaxClass()), 'steuer' => $mwert[$shipping->getTaxClass()]['steuer'] + ($this->order->getVersandKosten() / 100 * $payment->getTaxClass())];
            } else {
                $mwert[$shipping->getTaxClass()] = ['netto' => $this->order->getVersandKosten(), 'brutto' => $this->order->getVersandKosten() + ($this->order->getVersandKosten() / 100 * $payment->getTaxClass()), 'steuer' => $this->order->getVersandKosten() / 100 * $payment->getTaxClass()];
            }
        }

        $this->twigVars = array(
            'contact' => $contact,
            'contactDoc' => $contactObj,
            'account' => $contact->getAccount(),
            'invoiceAddress' => $invoiceAddress,
            'deliveryAddress' => $deliveryAddress,
            'senderAddress' => $senderAddress,
            'shop' => $shop,
            'shopObj' => $shopDoc,
            'shipping' => $shipping,
            'payment' => $payment,
            'mwert' => $mwert,
            'order' => $this->order,
            'orderObj' => $orderObj,
            'positions' => $positions
        );
    }

    /**
     * @return array
     */
    public function getTwigVars()
    {
        return $this->twigVars;
    }

    public function getPosTwigVars($getUuid)
    {
        $this->twigVars['position'] = $this->twigVars['positions'][$getUuid];

        return $this->twigVars;
    }

    public static function getHelp()
    {
        return [[
            \PSC\Shop\QueueBundle\Help\Shop::getColumn(),
            \PSC\Shop\QueueBundle\Help\Contact::getColumn(),
            \PSC\Shop\QueueBundle\Help\Payment::getColumn(),
            \PSC\Shop\QueueBundle\Help\Shipping::getColumn()
        ], [
            \PSC\Shop\QueueBundle\Help\Address::getColumn('invoiceAddress.', 'Rechnungsadresse'),
            \PSC\Shop\QueueBundle\Help\Address::getColumn('deliveryAddress.', 'Lieferadresse'),
            \PSC\Shop\QueueBundle\Help\Address::getColumn('senderAddress.', 'Absenderadresse'),
            \PSC\Shop\QueueBundle\Help\Position::getColumn(),
        ]];
    }

    public function getProductTwigVars(Product $product, Contact $contact, $count, $options, $netto, $steuer, $brutto)
    {
        $shop = $product->getShop();

        $account = new Account();
        $invoiceAddress = new ContactAddress();
        $deliveryAddress = new ContactAddress();
        $senderAddress = new ContactAddress();

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string) $product->getUid()));

        /** @var \PSC\Shop\EntityBundle\Document\Contact $contactObj */
        $contactObj = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Contact')
            ->findOneBy(array('uid' => (string) $contact->getId()));


        $anonym = true;

        if ($contact->getUsername() != 'anonymus') {
            $account = $contact->getAccount();
            $invoiceAddress = $contact->getStandartInvoice();
            $deliveryAddress = $contact->getStandartDelivery();
            $senderAddress = $contact->getStandartSender();
            $anonym = false;
        }

        if ($product->getScaledPrice() != "") {
            $tmp = array(
                'count' => $options['auflage'],
                'product' => $product,
                'productObj' => $productDoc,
                'engine' => false,
                'calc' => false,
                'set' => false,
                'netto' => $netto,
                'steuer' => $steuer,
                'brutto' => $brutto,
            );
        } elseif ($product->hasCalcXml()) {
            $paperContainer = new PaperContainer();
            $paperContainer->parse(simplexml_load_string($shop->getInstall()->getPaperContainer()));

            $engine = new Engine();
            $engine->setPaperRepository($this->paperDb);
            $engine->setPaperContainer($paperContainer);
            if ($shop->getInstall()->getCalcTemplates()) {
                $engine->setTemplates('<root>' . $shop->getInstall()->getCalcTemplates() . '</root>');
            }

            $engine->loadString($product->getCalcXml());
            $engine->setTax($product->getMwert());
            $engine->setFormulas($shop->getFormel());
            $engine->setParameters($shop->getParameter());
            $engine->setVariables($options);
            if (isset($options['kalk_artikel'])) {
                $engine->setActiveArticle($options['kalk_artikel']);
            }

            $articleCalc = $engine->getArticle();

            $tmp = array(
                'count' => (isset($options['auflage'])) ? $options['auflage'] : $count,
                'product' => $product,
                'productObj' => $productDoc,
                'calc' => $articleCalc,
                'engine' => $engine,
                'set' => false,
                'netto' => $engine->getPrice(),
                'steuer' => $engine->getTaxPrice(),
                'brutto' => $engine->getPrice() + $engine->getTaxPrice(),
            );
        } else {
            $tmp = array(
                'count' => $count,
                'product' => $product,
                'productObj' => $productDoc,
                'engine' => false,
                'calc' => false,
                'set' => false,
                'netto' => $netto,
                'steuer' => $steuer,
                'brutto' => $brutto,
            );
        }

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shop */
        $shopDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string) $shop->getUid()));

        return [
            'contact' => $contact,
            'contactDoc' => $contactObj,
            'anonym' => $anonym,
            'account' => $account,
            'invoiceAddress' => $invoiceAddress,
            'deliveryAddress' => $deliveryAddress,
            'senderAddress' => $senderAddress,
            'shop' => $shop,
            'shopObj' => $shopDoc,
            'product' => $product,
            'productObj' => $productDoc,
            'pos' => $tmp
        ];
    }
}
