<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Price
 *
 * @author Thomas Peterson
 */
class Price
{
    protected $entityManager;
    protected $tokenStorageInterface;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorageInterface)
    {
        $this->entityManager = $em;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

    public function getPackagingUnits()
    {

        $temp = [
            'ml' => 1,
            'cl' => 2,
            'dl' => 3,
            'l' => 4,
        ];

        return $temp;
    }
}
