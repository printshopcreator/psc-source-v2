<?php

namespace PSC\System\SettingsBundle\Service;

use Imagine\Image\ImagineInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Shop
 *
 * @author Thomas Peterson
 */
class Assets
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    /**
     * @var Kernel
     */
    private $kernel;

    public function __construct(Filesystem $fileSystem, KernelInterface $kernel)
    {
        $this->filesystem = $fileSystem;
        $this->kernel = $kernel;
    }

    public function install()
    {

        // Create the bundles directory otherwise symlink will fail.
        $bundlesDir = $this->kernel->getProjectDir() . '/web/bundles/';
        $this->filesystem->mkdir($bundlesDir, 0777);

        $validAssetDirs = array();
        /** @var BundleInterface $bundle */
        foreach ($this->kernel->getBundles() as $bundle) {
            if (!is_dir($originDir = $bundle->getPath() . '/Resources/public')) {
                continue;
            }

            $assetDir = preg_replace('/bundle$/', '', strtolower($bundle->getName()));
            $targetDir = $bundlesDir . $assetDir;
            $validAssetDirs[] = $assetDir;

            try {
                $this->filesystem->remove($targetDir);
                $this->hardCopy($originDir, $targetDir);
            } catch (\Exception $e) {
            }
        }

        // remove the assets of the bundles that no longer exist
        $dirsToRemove = Finder::create()->depth(0)->directories()->exclude($validAssetDirs)->in($bundlesDir);
        $this->filesystem->remove($dirsToRemove);
    }

    /**
     * Copies origin to target.
     *
     * @param string $originDir
     * @param string $targetDir
     *
     * @return string
     */
    private function hardCopy($originDir, $targetDir)
    {
        $this->filesystem->mkdir($targetDir, 0777);
        // We use a custom iterator to ignore VCS files
        $this->filesystem->mirror($originDir, $targetDir, Finder::create()->ignoreDotFiles(false)->in($originDir));
    }
}
