<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Color
 *
 * @author Thomas Peterson
 */
class Color
{
    protected $entityManager;
    protected $tokenStorageInterface;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorageInterface)
    {
        $this->entityManager = $em;
        $this->tokenStorageInterface = $tokenStorageInterface;
    }

    public function getColorsForArea($area = "general", Product $product)
    {

        $colorsDB = simplexml_load_string($product->getShop()->getInstall()->getColorDb());

        $colorsTemp = array();
        $colors = $colorsDB->xpath('//item[@id="' . $area . '"]');
        if (!$colors) {
            $colors = $colorsDB->xpath('//item[@id="general"]');
        }
        if ($colors) {
            foreach ($colors[0]->color as $color) {
                $colorsTemp[] = array(
                    'value' => (string)$color['id'],
                    'name' => (string)$color['label']
                );
            }
        }

        return $colorsTemp;
    }
}
