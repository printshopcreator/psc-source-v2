<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use MongoDB\BSON\ObjectId;
use MongoId;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\MediaBundle\Document\Media;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Config\FileLocator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;

require_once(__DIR__ . '/../../../Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
/**
 * Printing
 *
 * @author Thomas Peterson
 */
class Printing
{
    const INVOICE = 1;
    const DELIVERY = 2;
    const LABEL = 3;
    const ORDER = 4;
    const OFFER = 5;
    const STORNO = 6;
    const JOBTICKET = 7;
    const JOBTICKET_PRINTPARTNER = 8;

    protected $entityManager;
    protected $mongoManager;
    protected $tokenStorage;
    protected $twig;
    private $projectDir;
    private $templateVars;
    private $mpdf;

    protected $fileName;

    public function __construct($projectDir, EntityManagerInterface $em, TokenStorageInterface $tokenStorage, DocumentManager $mongoManager, Environment $twig, TemplateVars $templateVars)
    {
        $this->entityManager = $em;
        $this->tokenStorage = $tokenStorage;
        $this->mongoManager = $mongoManager;
        $this->projectDir = $projectDir;
        $this->twig = $twig;
        $this->templateVars = $templateVars;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return '';
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Order $order
     * @param int $type
     * @return bool|string
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function generateOrder(\PSC\Shop\EntityBundle\Entity\Order $order, $type = self::INVOICE)
    {

        $this->mpdf = new \Mpdf\Mpdf(['tempDir' => sys_get_temp_dir(), 'setAutoTopMargin' => 'false']);

        $this->templateVars->loadOrder($order->getUuid());

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $shop */
        $shop = $order->getShop();

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shopObj */
        $shopObj = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$shop->getUid()));


        switch ($type) {
            case self::DELIVERY:
                $reportXml = $shopObj->getDocDeliveryJasperXml();
                $bg = $shopObj->getDocDeliveryBg();
                $fileName = $shopObj->getDocDeliveryFileName();
                $fileNameOrg = "delivery_" . $order->getUID() . ".pdf";
                break;
            case self::LABEL:
                $reportXml = $shopObj->getDocLabelJasperXml();
                $bg = $shopObj->getDocLabelBg();
                $fileName = $shopObj->getDocLabelFileName();
                $fileNameOrg = "label_" . $order->getUID() . ".pdf";
                break;
            case self::JOBTICKET:
                $reportXml = $shopObj->getDocJobticketJasperXml();
                $bg = $shopObj->getDocJobticketBg();
                $fileName = $shopObj->getDocJobticketFileName();
                $fileNameOrg = "jobticket_" . $order->getUID() . ".pdf";
                break;
            case self::JOBTICKET_PRINTPARTNER:
                $reportXml = $shopObj->getDocJobticketPPJasperXml();
                $bg = $shopObj->getDocJobticketPPBg();
                $fileName = $shopObj->getDocJobticketPPFileName();
                $fileNameOrg = "jobticketpp_" . $order->getUID() . ".pdf";
                break;
            case self::OFFER:
                $reportXml = $shopObj->getDocOfferJasperXml();
                $bg = $shopObj->getDocOfferBg();
                $fileName = $shopObj->getDocOfferFileName();
                $fileNameOrg = "offer_" . $order->getUID() . ".pdf";
                break;
            case self::STORNO:
                $reportXml = $shopObj->getDocStornoJasperXml();
                $bg = $shopObj->getDocStornoBg();
                $fileName = $shopObj->getDocStornoFileName();
                $fileNameOrg = "storno_" . $order->getUID() . ".pdf";
                break;
            case self::ORDER:
                $reportXml = $shopObj->getDocOrderJasperXml();
                $bg = $shopObj->getDocOrderBg();
                $fileName = $shopObj->getDocOrderFileName();
                $fileNameOrg = "order_" . $order->getUID() . ".pdf";
                break;
            default:
            case self::INVOICE:
                $reportXml = $shopObj->getDocInvoiceJasperXml();
                $bg = $shopObj->getDocInvoiceBg();
                $fileName = $shopObj->getDocInvoiceFileName();
                $fileNameOrg = "invoice_" . $order->getUID() . ".pdf";
                break;
        }

        if ($reportXml == "") {
            return false;
        }

        $twig = $this->twig->createTemplate($reportXml);
        $html = $twig->render($this->templateVars->getTwigVars());
        if (trim($fileName) != "") {
            $twigFileName = $this->twig->createTemplate($fileName);
            $this->fileName = $twigFileName->render($this->templateVars->getTwigVars());
        } else {
            $this->fileName = $fileNameOrg;
        }

        if ($bg) {
            /** @var Media $bg */
            $bg = $this->mongoManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['_id' => new ObjectId($bg)]);

            if ($bg->getContentType() == 'application/pdf') {
                $this->mpdf->SetDocTemplate($this->projectDir . '/web' . $bg->getUrl(), true);
                $this->mpdf->WriteHTML($html);
            } else {
                $css = '
                
                    @page { 
                        background-image: url("/data/www/new/web' . $bg->getUrl() . '");
                        background-image-resize: 6;
                    }
                ';

                $crawler = new Crawler($html);
                foreach ($crawler->filter('html > head > style') as $el) {
                    $css = $css . $el->nodeValue;
                };

                $this->mpdf->WriteHTML($css, 1);

                $this->mpdf->WriteHTML($html);
            }
        } else {
            $this->mpdf->WriteHTML($html);
        }

        $output = $this->mpdf->Output(null, \Mpdf\Output\Destination::STRING_RETURN);

        return $output;
    }

    /**
     * @param Orderpos $pos
     * @param int $type
     * @return bool|string
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function generatePosition(Orderpos $pos, $type = self::INVOICE)
    {

        $this->mpdf = new \Mpdf\Mpdf(['tempDir' => sys_get_temp_dir(), 'setAutoTopMargin' => 'false']);

        $this->templateVars->loadOrder($pos->getOrder()->getUuid());

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $shop */
        $shop = $pos->getShop();

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shopObj */
        $shopObj = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$shop->getUid()));


        switch ($type) {
            case self::DELIVERY:
                $reportXml = $shopObj->getDocDeliveryPosition();
                $bg = $shopObj->getDocDeliveryPositionBg();
                break;
            case self::LABEL:
                $reportXml = $shopObj->getDocLabelPosition();
                $bg = $shopObj->getDocLabelPositionBg();
                break;
            case self::JOBTICKET:
                $reportXml = $shopObj->getDocJobticketPosition();
                $bg = $shopObj->getDocJobticketPositionBg();
                break;
            case self::JOBTICKET_PRINTPARTNER:
                $reportXml = $shopObj->getDocJobticketPPPosition();
                $bg = $shopObj->getDocJobticketPPPositionBg();
                break;
            case self::OFFER:
                $reportXml = $shopObj->getDocOfferPosition();
                $bg = $shopObj->getDocOfferPositionBg();
                break;
            case self::STORNO:
                $reportXml = $shopObj->getDocStornoPosition();
                $bg = $shopObj->getDocStornoPositionBg();
                break;
            case self::ORDER:
                $reportXml = $shopObj->getDocOrderPosition();
                $bg = $shopObj->getDocOrderPositionBg();
                break;
            default:
            case self::INVOICE:
                $reportXml = $shopObj->getDocInvoicePosition();
                $bg = $shopObj->getDocInvoicePositionBg();
                break;
        }

        if ($reportXml == "") {
            return false;
        }

        $twig = $this->twig->createTemplate($reportXml);
        $html = $twig->render($this->templateVars->getPosTwigVars($pos->getUuid()));

        if ($bg) {
            /** @var Media $bg */
            $bg = $this->mongoManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['_id' => new ObjectId($bg)]);

            if ($bg->getContentType() == 'application/pdf') {
                $this->mpdf->SetDocTemplate($this->projectDir . '/web' . $bg->getUrl(), true);

                $this->mpdf->WriteHTML($html);
            } else {
                $css = '
                
                    @page { 
                        background-image: url("/data/www/new/web' . $bg->getUrl() . '");
                        background-image-resize: 6;
                    }
                ';

                $this->mpdf->WriteHTML($css, 1);
                $this->mpdf->WriteHTML($html, 2);
            }
        } else {
            $this->mpdf->WriteHTML($html);
        }

        $output = $this->mpdf->Output(null, \Mpdf\Output\Destination::STRING_RETURN);

        return $output;
    }

    public function generateProductOffer(Product $product, Contact $contact, $count, $options, $netto, $steuer, $brutto)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $shop */
        $shop = $product->getShop();

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shopObj */
        $shopObj = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$shop->getUid()));

        $reportXml = $shopObj->getDocOfferProductXml();
        $bg = $shopObj->getDocOfferProductBg();

        $this->mpdf = new \Mpdf\Mpdf(['tempDir' => sys_get_temp_dir(), 'setAutoTopMargin' => 'false']);

        $twig = $this->twig->createTemplate($reportXml);
        $html = $twig->render($this->templateVars->getProductTwigVars($product, $contact, $count, $options, $netto, $steuer, $brutto));

        if ($bg) {
            /** @var Media $bg */
            $bg = $this->mongoManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['_id' => new ObjectId($bg)]);

            if ($bg->getContentType() == 'application/pdf') {
                $this->mpdf->SetDocTemplate($this->projectDir . '/web' . $bg->getUrl(), true);

                $this->mpdf->WriteHTML($html);
            } else {
                $css = '
                
                    @page { 
                        background-image: url("/data/www/new/web' . $bg->getUrl() . '");
                        background-image-resize: 6;
                    }
                ';

                $this->mpdf->WriteHTML($css, 1);
                $this->mpdf->WriteHTML($html, 2);
            }
        } else {
            $this->mpdf->WriteHTML($html);
        }


        $output = $this->mpdf->Output(null, \Mpdf\Output\Destination::STRING_RETURN);

        return $output;
    }

    /**
     * @return mixed
     */
    public function getFileName()
    {
        return $this->fileName;
    }

    /**
     * @param mixed $fileName
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
    }
}
