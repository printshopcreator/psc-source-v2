<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Document\LogEntry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * General
 *
 * @author Thomas Peterson
 */
class Log
{
    /**
     * @var DocumentManager
     */
    protected $mongoManager;

    public function __construct(DocumentManager $mongoManager)
    {
        $this->mongoManager = $mongoManager;
    }

    public function createLogEntry(\PSC\Shop\EntityBundle\Entity\Shop $shop, Contact $contact, $type = LogEntry::INFO, $module = "", $referenceUuid = "", $message = "", $data = [])
    {
        $row = new LogEntry();
        $row->setShop((string)$shop->getUuid());
        $row->setShopName((string)$shop->getTitle());
        $row->setContact((string)$contact->getUuid());
        $row->setContactEmail((string)$contact->getEmail());
        $row->setReferenceUuid($referenceUuid);
        $row->setMessage($message);
        $row->setData($data);
        $row->setType($type);
        $row->setModule($module);

        $this->mongoManager->persist($row);
        $this->mongoManager->flush();
    }
}
