<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Shop
 *
 * @author Thomas Peterson
 */
class Instance
{
    protected $mongoManager;

    public function __construct(DocumentManager $mongoManager)
    {
        $this->mongoManager = $mongoManager;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Document\Instance
     * @throws \Doctrine\ORM\ORMException
     */
    public function getInstance()
    {
        try {
            $instance = $this->mongoManager
                ->getRepository('PSCEntityBundle:Instance')
                ->findOneBy(["appId" => "1"]);
        } catch (\Exception $e) {
            $instance = null;
        }

        if ($instance === null) {
            $instance = new \PSC\Shop\EntityBundle\Document\Instance();
            $instance->setAppId("1");
        }

        return $instance;
    }
}
