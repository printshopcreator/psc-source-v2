<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Paper;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Config\FileLocator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use ZipArchive;

/**
 * Package
 *
 * @author Thomas Peterson
 */
class PaperDB implements ObjectRepository
{
    private $entityManager;
    private $mongoManager;


    public function __construct(EntityManagerInterface $entityManager, DocumentManager $mongoManager)
    {
        $this->entityManager = $entityManager;
        $this->mongoManager = $mongoManager;
    }


    /**
     * Finds an object by its primary key / identifier.
     *
     * @param mixed $id The identifier.
     *
     * @return object|null The object.
     */
    public function find($id)
    {
        // TODO: Implement find() method.
    }

    /**
     * Finds all objects in the repository.
     *
     * @return array The objects.
     */
    public function findAll()
    {
        // TODO: Implement findAll() method.
    }

    /**
     * Finds objects by a set of criteria.
     *
     * Optionally sorting and limiting details can be passed. An implementation may throw
     * an UnexpectedValueException if certain values of the sorting or limiting details are
     * not supported.
     *
     * @param array $criteria
     * @param array|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array The objects.
     *
     * @throws \UnexpectedValueException
     */
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        // TODO: Implement findBy() method.
    }

    /**
     * Finds a single object by a set of criteria.
     *
     * @param array $criteria The criteria.
     *
     * @return object|null The object.
     */
    public function findOneBy(array $criteria)
    {
        /** @var Paper $paper */
        $paper = $this->entityManager->getRepository(Paper::class)->findOneBy($criteria);

        if ($paper) {

            /** @var \PSC\Shop\EntityBundle\Document\Paper $paperObj */
            $paperObj = $this->mongoManager->getRepository(\PSC\Shop\EntityBundle\Document\Paper::class)->findOneBy(['uid' => (string)$paper->getId()]);

            if ($paperObj) {
                $paper->setVolume($paperObj->getVolume());
            }
        }

        return $paper;
    }

    /**
     * Returns the class name of the object managed by the repository.
     *
     * @return string
     */
    public function getClassName()
    {
        // TODO: Implement getClassName() method.
    }

    public function getNewObject()
    {
        return new Paper();
    }
}
