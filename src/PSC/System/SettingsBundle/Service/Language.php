<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * @author Thomas Peterson
 */
class Language
{
    protected $entityManager;
    protected $securityContext;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $securityContext)
    {
        $this->entityManager = $em;
        $this->securityContext = $securityContext;
    }

    public function getLanguageArrayForContent()
    {
        return ['Alle' => 'all'];
    }

    public function getShopLocale(): array
    {
        return ['Deutsch' => 'de_DE', 'English' => 'en_US'];
    }
}
