<?php

namespace PSC\System\SettingsBundle\Service;

use DirectoryIterator;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Upload;
use PSC\Shop\OrderBundle\Model\Order;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Config\FileLocator;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Twig\Environment;
use ZipArchive;

/**
 * Package
 *
 * @author Thomas Peterson
 */
class Package
{
    private $entityManager;
    private $mongoManager;
    private $tokenStorage;
    private $twig;
    private $projectDir;

    /** @var Printing */
    private $printing;

    /** @var \ZipArchive */
    protected $archive;

    /** @var \ZipArchive */
    protected $archivePosition;
    private $archivePositionPath;

    /** @var \PSC\Shop\EntityBundle\Entity\Order */
    protected $order;

    /** @var Orderpos */
    protected $position;

    /** @var Order */
    protected $orderObj;

    public function __construct(KernelInterface $kernel, EntityManagerInterface $em, TokenStorageInterface $tokenStorage, DocumentManager $mongoManager, Environment $twig, Printing $printing, \PSC\System\PluginBundle\Service\Package $pluginPackageRegistry)
    {
        $this->entityManager = $em;
        $this->tokenStorage = $tokenStorage;
        $this->mongoManager = $mongoManager;
        $this->projectDir = $kernel->getProjectDir();
        $this->twig = $twig;
        $this->printing = $printing;
        $this->pluginPackageRegistry = $pluginPackageRegistry;
    }

    public function createPackage($path)
    {
        $this->archive = new ZipArchive();

        $pathInfo = pathinfo($path);
        if (!file_exists($pathInfo['dirname'])) {
            mkdir($pathInfo['dirname'], 0777, true);
        }
        if (file_exists($path)) {
            $this->archive->open($path, ZIPARCHIVE::OVERWRITE);
        } else {
            $this->archive->open($path, ZIPARCHIVE::CREATE);
        }
    }

    public function setOrder(\PSC\Shop\EntityBundle\Entity\Order $order)
    {
        $this->order = $order;
    }

    /**
     * @param int $typ Typ
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function addOrderDocument($typ = Printing::JOBTICKET)
    {
        if ($content = $this->printing->generateOrder($this->order, $typ)) {
            $this->archive->addFromString($this->printing->getFileName(), $content);
        }
    }

    /**
     * @param string $name
     * @param string $content
     */
    public function addOrderDocumentFromString($name, $content)
    {
        $this->archive->addFromString($name, $content);
    }

    public function closePackage()
    {
        $this->archive->close();
    }

    /////// Position


    /**
     * @param Orderpos $pos
     */
    public function setPosition(Orderpos $pos)
    {
        $this->position = $pos;
    }

    public function createPositionPackage($path)
    {
        $this->archivePosition = new ZipArchive();

        $pathInfo = pathinfo($path);
        if (!file_exists($pathInfo['dirname'])) {
            mkdir($pathInfo['dirname'], 0777, true);
        }
        if (file_exists($path)) {
            $this->archivePosition->open($path, ZIPARCHIVE::OVERWRITE);
        } else {
            $this->archivePosition->open($path, ZIPARCHIVE::CREATE);
        }

        $this->archivePositionPath = $path;
    }

    /**
     * @param string $name Name
     * @param int $typ Typ
     * @param bool $orderArchive
     * @param string $path
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function addPositionDocument($name, $typ = Printing::JOBTICKET, $orderArchive = false, $path = "")
    {
        if ($content = $this->printing->generatePosition($this->position, $typ)) {
            if ($orderArchive) {
                $this->archive->addFromString($path . '/' . $name, $content);
            } else {
                $this->archivePosition->addFromString($name, $content);
            }
        }
    }

    public function closePositionPackage()
    {
        $this->archivePosition->close();
    }

    public function addPositionPackageToOrderPackage($name)
    {
        $this->archivePosition->close();
        $this->archive->addFile($this->archivePositionPath, $name);
    }

    /**
     * @param Orderpos $orderpos
     * @param bool $orderArchive
     * @param string $path
     */
    public function addUploadsToPackage($orderpos, $orderArchive = false, $path = "")
    {
        /** @var Upload $upload */
        foreach ($orderpos->getUploads() as $upload) {
            if ($orderArchive) {
                $this->archive->addFile($upload->getPath(), $path . '/' . $upload->getTyp() . '_' . $upload->getName());
            } else {
                $this->archivePosition->addFile($upload->getPath(), $path . $upload->getTyp() . '_' . $upload->getName());
            }
        }

        if (file_exists('/data/www/old/market/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos())) {
            foreach (new DirectoryIterator('/data/www/old/market/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos()) as $fileInfo) {
                if ($fileInfo->isDot()) {
                    continue;
                }
                if ($orderArchive) {
                    $this->archive->addFile($fileInfo->getPathname(), $path . $fileInfo->getBasename());
                } else {
                    $this->archivePosition->addFile($fileInfo->getPathname(), $path . $fileInfo->getBasename());
                }
            }
        }
    }

    public function addPluginFilesToOrderPackage()
    {
        foreach ($this->pluginPackageRegistry->getPackageTypes() as $packageType) {
            $packageType->addFilesToOrderArchive($this->archive, $this->orderObj);
        }
    }

    public function addPluginFilesToPositionPackage($orderArchive = false, $path = "")
    {
        foreach ($this->pluginPackageRegistry->getPackageTypes() as $packageType) {
            if ($orderArchive) {
                $packageType->addFileToPositionArchive($this->archive, $this->orderObj, $this->orderObj->getPositionByUuid($this->position->getUuid()), $path);
            } else {
                $packageType->addFileToPositionArchive($this->archivePosition, $this->orderObj, $this->orderObj->getPositionByUuid($this->position->getUuid()), $path);
            }
        }
    }

    /**
     * @param Orderpos $orderpos
     * @param bool $orderArchive
     * @param string $path
     */
    public function addDesignerFilesToPackage($orderpos, $orderArchive = false, $path = "", $name = "final.pdf")
    {
        if ($orderArchive) {
            if (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf')) {
                $this->archive->addFile('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf', $path . $name);
            } elseif (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf')) {
                $this->archive->addFile('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf', $path . 'preview.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf')) {
                $this->archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf', $path . 'preview.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/production.pdf')) {
                $this->archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/production.pdf', $path . 'production.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getUid() . '_' . $orderpos->getPos() . '.pdf')) {
                $this->archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getUid() . '_' . $orderpos->getPos() . '.pdf', $path . 'rendering.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf')) {
                $this->archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf', $path . 'rendering.pdf');
            }
        } else {
            if (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf')) {
                $this->archivePosition->addFile('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf', $path . $name);
            } elseif (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf')) {
                $this->archivePosition->addFile('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf', $path . 'preview.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf')) {
                $this->archivePosition->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf', $path . 'preview.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/production.pdf')) {
                $this->archivePosition->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/production.pdf', $path . 'production.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getUid() . '_' . $orderpos->getPos() . '.pdf')) {
                $this->archivePosition->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getUid() . '_' . $orderpos->getPos() . '.pdf', $path . 'rendering.pdf');
            }

            if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf')) {
                $this->archivePosition->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf', $path . 'rendering.pdf');
            }
        }
    }

    public function addPositionDocumentFromString($name, $content, $orderArchive = false, $path = "")
    {
        if ($orderArchive) {
            $this->archive->addFromString($path . '/' . $name, $content);
        } else {
            $this->archivePosition->addFromString($name, $content);
        }
    }

    public function addDesignerFilesToPackageWithCustomPathname($pos, $orderPosDesignPath)
    {
        if (file_exists('/data/www/old/market/templateprint/basket/' . $pos->getOrder()->getUid() . '/' . $pos->getPos() . '/final.pdf')) {
            $this->archive->addFile('/data/www/old/market/templateprint/basket/' . $pos->getOrder()->getUid() . '/' . $pos->getPos() . '/final.pdf', $orderPosDesignPath);
        }
    }

    /**
     * @return Order
     */
    public function getOrderObj(): Order
    {
        return $this->orderObj;
    }

    /**
     * @param Order $orderObj
     */
    public function setOrderObj(Order $orderObj): void
    {
        $this->orderObj = $orderObj;
    }
}
