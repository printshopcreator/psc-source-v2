<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\System\SettingsBundle\Interfaces\History as PSCHistory;
use Proxies\__CG__\PSC\Shop\EntityBundle\Entity\Account as ProxiesAccount;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

use function array_merge_recursive;
use function array_walk;
use function get_class;

class History
{
    private DocumentManager $documentManager;
    private EntityManagerInterface $entityService;
    private TokenStorageInterface $tokenStorage;

    public function __construct(DocumentManager $documentManager, EntityManagerInterface $entityService, TokenStorageInterface $tokenStorage)
    {
        $this->documentManager = $documentManager;
        $this->entityService = $entityService;
        $this->tokenStorage = $tokenStorage;
    }


    public function createHistoryEntry(PSCHistory $historyObj, $entity = null, $doc = null)
    {
        $changeSet = [];
        if ($entity != null) {
            $uow = $this->entityService->getUnitOfWork();
            $diff = $uow->computeChangeSet($this->entityService->getClassMetadata(get_class($entity)), $entity);
            $changeSet = array_merge($changeSet, $uow->getEntityChangeSet($entity));
        }
        if ($doc != null) {
            $urowd = $this->documentManager->getUnitOfWork();
            $diff = $urowd->computeChangeSet($this->documentManager->getClassMetadata(get_class($doc)), $doc);
            $changeSet = array_merge($changeSet, $urowd->getDocumentChangeSet($doc));
        }

        if (empty($changeSet)) {
            return;
        }
        $newChangeSet = [];
        array_walk($changeSet, function ($item, $key) use (&$newChangeSet) {
            if (is_array($item) && $item[0] instanceof \DateTime && $item[1] instanceof \DateTime) {
                $item[0] = $item[0]->format('H:i:s d.m.Y');
                $item[1] = $item[1]->format('H:i:s d.m.Y');
            }
            if (is_object($item) && $item instanceof PersistentCollection) {
                return;
            }
            if (is_array($item) && ($item[0] instanceof Account || $item[1] instanceof Account)) {
                $newChangeSet[$key] = $item;
                if ($item[0] instanceof Account) {
                    $newChangeSet[$key][0] = $item[0]->getTitle();
                }
                if ($item[1] instanceof Account) {
                    $newChangeSet[$key][1] = $item[1]->getTitle();
                }
                return;
            }
            if ($item[0] !== null && $item[1] !== null) {
                $newChangeSet[$key] = $item;
            }
        });

        if (empty($newChangeSet)) {
            return;
        }
        $historyObj->setChangeset($newChangeSet);
        $historyObj->setContact($this->tokenStorage->getToken()->getUser()->getUuid());
        $historyObj->setUsername($this->tokenStorage->getToken()->getUsername());
        $this->documentManager->persist($historyObj);
        $this->documentManager->flush();
    }

    public function getHistory(PSCHistory $class, $entity)
    {
        return $this->documentManager->getRepository(get_class($class))->findby([$class->getReferenceField() => (string)$entity], ['id' => 'desc']);
    }
}
