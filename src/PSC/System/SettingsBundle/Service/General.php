<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * General
 *
 * @author Thomas Peterson
 */
class General
{
    protected $entityManager;
    protected $securityContext;
    protected TranslatorInterface $translationService;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $securityContext, TranslatorInterface $translationService)
    {
        $this->entityManager = $em;
        $this->securityContext = $securityContext;
        $this->translationService = $translationService;
    }

    public function getSalutation()
    {
        $tmp = array();

        $tmp[$this->translationService->trans(1, [], 'general')] = 1;
        $tmp[$this->translationService->trans(2, [], 'general')] = 2;
        $tmp[$this->translationService->trans(3, [], 'general')] = 3;
        $tmp[$this->translationService->trans(4, [], 'general')] = 4;
        $tmp[$this->translationService->trans(5, [], 'general')] = 5;
        $tmp[$this->translationService->trans(6, [], 'general')] = 6;
        $tmp[$this->translationService->trans(7, [], 'general')] = 7;
        $tmp[$this->translationService->trans(8, [], 'general')] = 8;
        $tmp[$this->translationService->trans(9, [], 'general')] = 9;
        $tmp[$this->translationService->trans(10, [], 'general')] = 10;
        $tmp[$this->translationService->trans(11, [], 'general')] = 11;
        return $tmp;
    }

    public function getAccountTypes()
    {
        $tmp = array();

        $tmp["accountType1"] = 1;
        $tmp["accountType2"] = 2;
        $tmp["accountType3"] = 3;
        $tmp["accountType4"] = 4;
        $tmp["accountType5"] = 5;
        $tmp["accountType6"] = 6;
        $tmp["accountType7"] = 7;
        $tmp["accountType8"] = 8;
        $tmp["accountType9"] = 9;
        $tmp["accountType10"] = 10;
        $tmp["accountType11"] = 11;
        $tmp["accountType12"] = 12;
        return $tmp;
    }
}
