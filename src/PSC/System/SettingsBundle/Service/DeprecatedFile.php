<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Imagine\Image\ImagineInterface;
use Imagine\Imagick\Imagine;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Shop
 *
 * @author Thomas Peterson
 */
class DeprecatedFile
{
    protected $entityManager;
    protected $liip_imagine;

    public function __construct(EntityManagerInterface $em, CacheManager $liip_imagine)
    {
        $this->entityManager = $em;
        $this->liip_imagine = $liip_imagine;
    }

    /**
     * @return string
     */
    public function getFilePath($fileUUID)
    {

        if (strlen($fileUUID) <= 30) {
        }

        $file1 = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Image')->findOneBy(['uid' => $fileUUID]);

        if (!$file1 || !file_exists(str_replace("temp", "uploads_backend", $file1->getPath()))) {
            return false;
        } else {
            if (!file_exists('uploads_backend/' . md5($file1->getPath()) . '.png')) {
                $imagine = $this->imagine->open(str_replace("temp", "uploads_backend", $file1->getPath()));

                $this->imagine->save('uploads_backend/' . md5($file1->getPath()) . '.png');
            }
            return 'uploads_backend/' . md5($file1->getPath()) . '.png';
        }
    }
}
