<?php

namespace PSC\System\SettingsBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Shop as PSCShop;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Shop
 *
 * @author Thomas Peterson
 */
class Shop
{
    protected $entityManager;
    protected $mongoManager;
    protected $tokenStorage;
    protected $requestStack;
    protected $currentShopForMail;

    public function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage, DocumentManager $mongoManager, RequestStack $requestStack)
    {
        $this->entityManager = $em;
        $this->tokenStorage = $tokenStorage;
        $this->mongoManager = $mongoManager;
        $this->requestStack = $requestStack;
    }

    /**
     * @return Shop
     * @throws \Doctrine\ORM\ORMException
     */
    public function getSelectedShop(): PSCShop
    {

        $selectedShop = false;

        if ($this->requestStack->getCurrentRequest()->getSession()->has('selectedShop')) {
            $selectedShop = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findOneBy(['uid' => $this->requestStack->getCurrentRequest()->getSession()->get('selectedShop')]);
        }
        if (!$selectedShop) {
            $selectedShop = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\ShopContact')->myEditableShopsWithNoDeleted($this->tokenStorage->getToken()->getUser())[0];

            $this->requestStack->getCurrentRequest()->getSession()->set('selectedShop', $selectedShop->getUid());
            return $selectedShop;
        }

        return $selectedShop;
    }

    /**
     * @param $uid
     * @return \PSC\Shop\EntityBundle\Document\Shop
     */
    public function getMongoShopByUid($uid): \PSC\Shop\EntityBundle\Document\Shop
    {

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shopDoc */
        $shopDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$uid));

        return $shopDoc;
    }

    /**
     * @param $uid
     * @return \PSC\Shop\EntityBundle\Entity\Shop
     */
    public function getShopByUid($uid)
    {


        $selectedShop = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findOneBy(array('uid' => $uid));

        if(!$selectedShop) {
            $selectedShop = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findOneBy(array('uuid' => $uid));
        }

        return $selectedShop;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Document\Shop
     * @throws \Doctrine\ORM\ORMException
     */
    public function getMongoSelectedShop()
    {

        $selectedShop = false;

        if ($this->requestStack->getCurrentRequest()->getSession()->has('selectedShop')) {
            $selectedShop = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findOneBy(['uid' => $this->requestStack->getCurrentRequest()->getSession()->get('selectedShop')]);
        }
        if (!$selectedShop) {
            $selectedShop = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\ShopContact')->myEditableShops($this->tokenStorage->getToken()->getUser())[0];

            $this->requestStack->getCurrentRequest()->getSession()->set('selectedShop', $selectedShop->getUid());
        }

        $shopDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$selectedShop->getUid()));

        if ($shopDoc === null) {
            $shopDoc = new \PSC\Shop\EntityBundle\Document\Shop();
            $shopDoc->setUid($selectedShop->getUid());
        }

        return $shopDoc;
    }

    /**
     * @return PSCShop
     */
    public function getShopByDomain(): PSCShop
    {

        $selectedShop = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findOneBy(['host' => $_SERVER["SERVER_NAME"]]);




        if (!$selectedShop) {
            $selectedShop = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findAll()[0];

            return $selectedShop;
        }

        return $selectedShop->getShop();
    }


    public function getMongoShopByDomain()
    {
        $shop = $this->getShopByDomain();

        $shopDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$shop->getUid()));

        if ($shopDoc === null) {
            $shopDoc = new \PSC\Shop\EntityBundle\Document\Shop();
            $shopDoc->setUid($shop->getUid());
        }

        return $shopDoc;
    }

    public function getMyEditableShops()
    {

        $shops = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\ShopContact')->myEditableShops($this->tokenStorage->getToken()->getUser());

        return $shops;
    }

    /**
     * @return mixed
     */
    public function getCurrentShopForMail()
    {
        return $this->currentShopForMail;
    }

    /**
     * @param mixed $currentShopForMail
     */
    public function setCurrentShopForMail($currentShopForMail): void
    {
        $this->currentShopForMail = $currentShopForMail;
    }
}
