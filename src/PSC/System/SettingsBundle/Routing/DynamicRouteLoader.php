<?php

namespace PSC\System\SettingsBundle\Routing;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

class DynamicRouteLoader implements LoaderInterface
{
    protected $mongoManager;
    protected $shopService;

    /**
     * DynamicRouteLoader constructor.
     *
     * @param DocumentManager $mongoManager
     * @param Shop $shopService
     */
    public function __construct(DocumentManager $mongoManager, Shop $shopService)
    {
        $this->mongoManager = $mongoManager;
        $this->shopService = $shopService;
    }

    /**
     * Loads a resource.
     *
     * @param mixed       $resource The resource
     * @param string|null $type     The resource type or null if unknown
     *
     * @return RouteCollection
     * @throws \Exception If something went wrong
     */
    public function load($resource, $type = null)
    {
        $collection = new RouteCollection();

        /** @var DynamicPage[] $pages */
        //$pages = $this->em->getRepository(DynamicPage::class)->findAll();
/*
        foreach ($pages as $page) {
            $route = new Route($page->url, ['_controller' => 'AcmeBundle:Dynamic:dynamic', 'templateId' => $page->id]);
            $collection->add('_dynamic_id_'.$page->id, $route);
        }
*/
        return $collection;
    }

    /**
     * Returns whether this class supports the given resource.
     *
     * @param mixed       $resource A resource
     * @param string|null $type     The resource type or null if unknown
     *
     * @return bool True if this class supports the given resource, false otherwise
     */
    public function supports($resource, $type = null)
    {
        return ($type == 'dynamic');
    }

    public function getResolver()
    {
    }

    public function setResolver(LoaderResolverInterface $resolver)
    {
    }
}
