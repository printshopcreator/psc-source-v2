<?php

namespace PSC\System\SettingsBundle\Interfaces;

interface History
{
    public function setChangeset(array $changeset);
    public function setContact(string $uuid);
    public function setUsername(string $username);
    public function getReferenceField(): string;
}
