<?php

namespace PSC\System\SettingsBundle\Twig;

use PSC\System\PluginBundle\Service\ProductType;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Component\Form\ChoiceList\View\ChoiceView;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigTest;

class AppExtension extends AbstractExtension
{
    protected $status;
    protected $productTypeService;

    public function __construct(Status $status = null, ProductType $productTypeService)
    {
        $this->status = $status;
        $this->productTypeService = $productTypeService;
    }

    public function getFilters()
    {
        return array(
            new TwigFilter('status', array($this, 'statusFilter')),
            new TwigFilter('posstatus', array($this, 'posStatusFilter')),
            new TwigFilter('productType', array($this, 'productType')),
            new TwigFilter('md5', array($this, 'md5')),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getTests(): array
    {
        return [
            new TwigTest('validateChoice', array($this, 'validateChoice')),
        ];
    }

    public function statusFilter($code)
    {
        return $this->status->getStatusText($code);
    }

    public function md5($code)
    {
        return md5($code);
    }

    public function posStatusFilter($code)
    {
        return $this->status->getPosStatusText($code);
    }

    public function productType($code)
    {
        return $this->productTypeService->getProductTypeName($code);
    }

    public function getName()
    {
        return 'app_extension';
    }

    function validateChoice(ChoiceView $choice, $selectedValue): bool
    {
        if (\is_array($selectedValue)) {
            return \in_array($choice->value, $selectedValue);
        }

        return $choice->value == $selectedValue;
    }
}
