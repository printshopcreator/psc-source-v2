<?php

namespace PSC\System\SettingsBundle\Twig;

use PSC\System\SettingsBundle\Service\Status;

class InstanceOfExtension extends \Twig_Extension
{
    public function getTests()
    {
        return array(
            new \Twig_SimpleTest('instanceof', array($this, 'isInstanceOf')),
        );
    }

    public function isInstanceOf($var, $instance)
    {
        $reflexionClass = new \ReflectionClass($instance);
        return $reflexionClass->isInstance($var);
    }
}
