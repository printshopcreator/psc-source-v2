<?php

namespace PSC\System\SettingsBundle\Twig;

use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\System\PluginBundle\Service\ProductType;
use PSC\System\SettingsBundle\Service\Status;

class TemplateprintExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('pdfPath', array($this, 'pdfPath')),
        );
    }

    public function pdfPath(Orderpos $orderpos)
    {
        if (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf')) {
            return '/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf';
        } elseif (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf')) {
            return '/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf';
        }

        return null;
    }

    public function getName()
    {
        return 'templateprint_extension';
    }
}
