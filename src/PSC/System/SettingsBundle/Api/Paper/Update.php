<?php

namespace PSC\System\SettingsBundle\Api\Paper;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Paper as PSCPaper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use PSC\System\SettingsBundle\Model\Paper;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class Update extends AbstractController {

    private EntityManagerInterface $entityManager;
    private DocumentManager $documentManager;
    public function __construct(EntityManagerInterface $entityManager, DocumentManager $documentManager)
    {
        $this->entityManager = $entityManager;
        $this->documentManager = $documentManager;  
    }

    /**
     * change paper
     *
     * @Route("/paperdb/update/{artNr}", methods={"PUT"})
     * @ParamConverter("paperObj", class="\PSC\System\SettingsBundle\Model\Paper", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="update paper",
     *     @OA\JsonContent(ref=@Model(type=\PSC\System\SettingsBundle\Model\Paper::class))
     * )
     * @OA\RequestBody(
     *     description="This is a request body",
     *     @Model(type=\PSC\System\SettingsBundle\Model\Paper::class)
     * )
     * @OA\Tag(name="PaperDB")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function update(string $artNr, Paper $paperObj): JsonResponse
    {
        $paper = $this->entityManager->getRepository(PSCPaper::class)->findOneBy(['artNr' => $artNr]);

        if(!$paper) {
            return $this->json(new NotFound('paper not found'));
        } else {
            /** @var \PSC\Shop\EntityBundle\Document\Paper $paperDoc */
            $paperDoc = $this->documentManager
                ->getRepository('PSC\Shop\EntityBundle\Document\Paper')
                ->findOneBy(array('uid' => (string)$paper->getId()));
            if(!$paperDoc) {
                $paperDoc = new \PSC\Shop\EntityBundle\Document\Paper();
                $paperDoc->setUid($paper->getId());
            }
        }
        
        if($paperObj->getGrammatur() !== null) {
            $paper->setGrammatur($paperObj->getGrammatur());
        }
        if($paperObj->getPrice() !== null) {
            $paper->setPreis($paperObj->getPrice());
        }
        if($paperObj->getDescription1() !== null) {
            $paper->setDescription1($paperObj->getDescription1());
        }
        if($paperObj->getDescription2() !== null) {
            $paper->setDescription2($paperObj->getDescription2());
        }
        $this->entityManager->persist($paper);
        $this->entityManager->flush();

            $paperDoc->setPapierTyp1($paperObj->getPapiertyp1());
            $paperDoc->setPapierTyp2($paperObj->getPapiertyp2());
            $paperDoc->setPapierTyp3($paperObj->getPapiertyp3());
            $paperDoc->setPapierTyp4($paperObj->getPapiertyp4());
            $paperDoc->setPapierTyp5($paperObj->getPapiertyp5());
            $paperDoc->setPapierTyp6($paperObj->getPapiertyp6());
            $paperDoc->setPapierTyp7($paperObj->getPapiertyp7());
            $paperDoc->setPapierTyp8($paperObj->getPapiertyp8());
            $paperDoc->setPapierTyp9($paperObj->getPapiertyp9());
            $paperDoc->setPapierTyp10($paperObj->getPapiertyp10());
            $paperDoc->setPapierTyp11($paperObj->getPapiertyp11());
            $paperDoc->setPapierTyp12($paperObj->getPapiertyp12());
            $paperDoc->setPapierTyp13($paperObj->getPapiertyp13());
            $paperDoc->setPapierTyp14($paperObj->getPapiertyp14());

            $paperDoc->setUmschlagen($paperObj->getUmschlagen());

            $paperDoc->setHappy($paperObj->getHappy());

            $paperDoc->setEq($paperObj->getEq());

            $paperDoc->setSense($paperObj->getSense());

            $paperDoc->setSense($paperObj->getSense());

            $paperDoc->setSky($paperObj->getSky());

            $paperDoc->setGlam($paperObj->getGlam());

            $paperDoc->setPost($paperObj->getPost());


        $this->documentManager->persist($paperDoc);
        $this->documentManager->flush();
        
        return $this->json($paperObj);
    }

}
