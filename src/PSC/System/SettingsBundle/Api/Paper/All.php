<?php

namespace PSC\System\SettingsBundle\Api\Paper;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use PSC\System\SettingsBundle\Dto\Paper\All\Output;
use Nelmio\ApiDocBundle\Annotation\Model;
use PSC\System\SettingsBundle\Model\Paper as PSCPaper;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class All extends AbstractController {
    private EntityManagerInterface $entityManager;
    private DocumentManager $documentManager;
    public function __construct(EntityManagerInterface $entityManager, DocumentManager $documentManager)
    {
        $this->entityManager = $entityManager;  
        $this->documentManager = $documentManager;  
    }
    /**
     * get all paper
     *
     * @Route("/paperdb", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get all paper",
     *     @OA\JsonContent(ref=@Model(type=\PSC\System\SettingsBundle\Dto\Paper\All\Output::class))
     * )
     * @OA\Tag(name="PaperDB")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */ 
    public function all(): JsonResponse
    {
        $output = [];
        $result = $this->entityManager->getRepository(Paper::class)->findAll();
        foreach($result as $paper) {
            /** @var \PSC\Shop\EntityBundle\Document\Paper $paperDoc */
            $paperDoc = $this->documentManager
                ->getRepository('PSC\Shop\EntityBundle\Document\Paper')
                ->findOneBy(array('uid' => (string)$paper->getId()));
            $paperObj = new PSCPaper();
            if ($paperDoc) {
                if ($paperDoc->isPapierTyp1()) {
                    $papiertyp1 = true;
                } else {
                    $papiertyp1 = false;
                }
                if ($paperDoc->isPapierTyp2()) {
                    $papiertyp2 = true;
                } else {
                    $papiertyp2 = false;
                }
                if ($paperDoc->isPapierTyp3()) {
                    $papiertyp3 = true;
                } else {
                    $papiertyp3 = false;
                }
                if ($paperDoc->isPapierTyp4()) {
                    $papiertyp4 = true;
                } else {
                    $papiertyp4 = false;
                }
                if ($paperDoc->isPapierTyp5()) {
                    $papiertyp5 = true;
                } else {
                    $papiertyp5 = false;
                }
                if ($paperDoc->isPapierTyp6()) {
                    $papiertyp6 = true;
                } else {
                    $papiertyp6 = false;
                }
                if ($paperDoc->isPapierTyp7()) {
                    $papiertyp7 = true;
                } else {
                    $papiertyp7 = false;
                }
                if ($paperDoc->isPapierTyp8()) {
                    $papiertyp8 = true;
                } else {
                    $papiertyp8 = false;
                }
                if ($paperDoc->isPapierTyp9()) {
                    $papiertyp9 = true;
                } else {
                    $papiertyp9 = false;
                }
                if ($paperDoc->isPapierTyp10()) {
                    $papiertyp10 = true;
                } else {
                    $papiertyp10 = false;
                }
                if ($paperDoc->isPapierTyp11()) {
                    $papiertyp11 = true;
                } else {
                    $papiertyp11 = false;
                }
                if ($paperDoc->isPapierTyp12()) {
                    $papiertyp12 = true;
                } else {
                    $papiertyp12 = false;
                }
                if ($paperDoc->isPapierTyp13()) {
                    $papiertyp13 = true;
                } else {
                    $papiertyp13 = false;
                }
                if ($paperDoc->isPapierTyp14()) {
                    $papiertyp14 = true;
                } else {
                    $papiertyp14 = false;
                }
                $paperObj->setPapiertyp1($papiertyp1);
                $paperObj->setPapiertyp2($papiertyp2);
                $paperObj->setPapiertyp3($papiertyp3);
                $paperObj->setPapiertyp4($papiertyp4);
                $paperObj->setPapiertyp5($papiertyp5);
                $paperObj->setPapiertyp6($papiertyp6);
                $paperObj->setPapiertyp7($papiertyp7);
                $paperObj->setPapiertyp8($papiertyp8);
                $paperObj->setPapiertyp9($papiertyp9);
                $paperObj->setPapiertyp10($papiertyp10);
                $paperObj->setPapiertyp11($papiertyp11);
                $paperObj->setPapiertyp12($papiertyp12);
                $paperObj->setPapiertyp13($papiertyp13);
                $paperObj->setPapiertyp14($papiertyp14);

                if ($paperDoc->isUmschlagen()) {
                    $isUmschlagen = true;
                } else {
                    $isUmschlagen = false;
                }
                $paperObj->setUmschlagen($isUmschlagen);
                if ($paperDoc->isHappy()) {
                    $isHappy = true;
                } else {
                    $isHappy = false;
                }
                $paperObj->setHappy($isHappy);
                if ($paperDoc->isEq()) {
                    $isEq = true;
                } else {
                    $isEq = false;
                }
                $paperObj->setEq($isEq);
                if ($paperDoc->isSense()) {
                    $isSense = true;
                } else {
                    $isSense = false;
                }
                $paperObj->setSense($isSense);
                if ($paperDoc->isSky()) {
                    $isSky = true;
                } else {
                    $isSky = false;
                }
                $paperObj->setSky($isSky);
                if ($paperDoc->isGlam()) {
                    $isGlam = true;
                } else {
                    $isGlam = false;
                }
                $paperObj->setGlam($isGlam);
                if ($paperDoc->isPost()) {
                    $isPost = true;
                } else {
                    $isPost = false;
                }
                $paperObj->setPost($isPost);
            }
            $paperObj->setId($paper->getId());
            $paperObj->setArtNr($paper->getArtNr());
            $paperObj->setDescription1((string)$paper->getDescription1());
            $paperObj->setDescription2((string)$paper->getDescription2());
            $paperObj->setGrammatur((int)$paper->getGrammatur());
            $paperObj->setPrice($paper->getPreis());
            $output[] = $paperObj;
        }
        return $this->json(new Output($output));
    }
}
