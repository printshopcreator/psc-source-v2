<?php

namespace PSC\System\SettingsBundle\Api\Status;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use PSC\System\SettingsBundle\Dto\Status\All\Output;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use PSC\System\SettingsBundle\Model\Status;
use PSC\System\SettingsBundle\Document\Status as StatusDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class All extends AbstractController
{
    private DocumentManager $documentManager;

    public function __construct(DocumentManager $documentManager)
    {
        $this->documentManager = $documentManager;
    }
    /**
     * get all statuse
     *
     * @Route("/status", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get all paper",
     *     @OA\JsonContent(ref=@Model(type=\PSC\System\SettingsBundle\Dto\Status\All\Output::class))
     * )
     * @OA\Tag(name="Status")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="ApiKeyAuth")
     * @Security(name="Bearer")
     */
    public function all(): JsonResponse
    {
        $statusOrder = $this->documentManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => StatusDoc::$ORDER, 'enable' => true]);

        $statusPosition = $this->documentManager
            ->getRepository('PSCSystemSettingsBundle:Status')
            ->findBy(['typ' => StatusDoc::$POSITION, 'enable' => true]);

        $tmpOrder = [];

        foreach ($statusOrder as $status) {
            $tmp = new Status();
            $tmp->setCode((int)$status->getCode());
            $tmp->setColor((string)$status->getColor());
            $tmp->setExternalName((string)$status->getExternalName());
            $tmp->setInternalName((string)$status->getInternalName());
            $tmp->setUuid((string)$status->getId());
            $tmpOrder[] = $tmp;
        }

        $tmpPos = [];

        foreach ($statusPosition as $status) {
            $tmp = new Status();
            $tmp->setCode((int)$status->getCode());
            $tmp->setColor((string)$status->getColor());
            $tmp->setExternalName((string)$status->getExternalName());
            $tmp->setInternalName((string)$status->getInternalName());
            $tmp->setUuid((string)$status->getId());
            $tmpPos[] = $tmp;
        }

        return $this->json(new Output($tmpOrder, $tmpPos));
    }
}
