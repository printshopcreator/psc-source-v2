<?php

namespace PSC\System\SettingsBundle\Api\Papercontainer;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\System\SettingsBundle\Model\Papercontainer;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use PSC\System\SettingsBundle\Model\Paper as PSCPaper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class Update extends AbstractController
{
    private Shop $shopService;

    private EntityManagerInterface $entityManager;

    public function __construct(Shop $shopService, EntityManagerInterface $entityManager)
    {
        $this->shopService = $shopService;
        $this->entityManager = $entityManager;
    }
    /**
     * update papercontainer
     *
     * @Route("/papercontainer", methods={"PUT"})
     * @ParamConverter("papercontainer", class="\PSC\System\SettingsBundle\Model\Papercontainer", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="update papercontainer",
     *     @OA\JsonContent(ref=@Model(type=\PSC\System\SettingsBundle\Model\Papercontainer::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\System\SettingsBundle\Model\Papercontainer::class))
     * )
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     * @OA\Tag(name="PaperDB")
     */
    public function updatePapercontainer(Papercontainer $papercontainer): JsonResponse
    {
        $install = $this->shopService->getShopByDomain()->getInstall();
        $install->setPaperContainer($papercontainer->getContent());
        $this->entityManager->persist($install);
        $this->entityManager->flush();
        return $this->json($papercontainer);
    }
}
