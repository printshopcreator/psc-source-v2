<?php

namespace PSC\System\SettingsBundle\Api\Papercontainer;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\System\SettingsBundle\Model\Papercontainer;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use PSC\System\SettingsBundle\Model\Paper as PSCPaper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class Get extends AbstractController
{
    private Shop $shopService;
    public function __construct(Shop $shopService)
    {
        $this->shopService = $shopService;
    }
    /**
     * get papercontainer
     *
     * @Route("/papercontainer", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get papercontainer",
     *     @OA\JsonContent(ref=@Model(type=\PSC\System\SettingsBundle\Model\Papercontainer::class))
     * )
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     * @OA\Tag(name="PaperDB")
     */
    public function getPapercontainer(): JsonResponse
    {
        $install = $this->shopService->getShopByDomain()->getInstall();
        $output = new Papercontainer();
        $output->setContent($install->getPaperContainer());

        return $this->json($output);
    }
}
