<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedMany;

/**
 * @Document
 */
class LogEntry
{
    const ERROR = 1;
    const WARNING = 2;
    const INFO = 3;
/**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $shop
     *
     * @Field(type="string")
     */
    protected $shop;
/**
     * @var string $shopName
     *
     * @Field(type="string")
     */
    protected $shopName;
/**
     * @var string $contact
     *
     * @Field(type="string")
     */
    protected $contact;
/**
     * @var string $contactEmail
     *
     * @Field(type="string")
     */
    protected $contactEmail;
/**
     * @var \DateTime $created;
     *
     * @Field(type="date")
     */
    protected $created;
/**
     * @var int $type;
     *
     * @Field(type="int")
     */
    protected $type;
/**
     * @var \DateTime $updated;
     *
     * @Field(type="date")
     */
    protected $updated;
/**
     * @var string $referenceUuid
     *
     * @Field(type="string")
     */
    protected $referenceUuid;
/**
     * @var string $module
     *
     * @Field(type="string")
     */
    protected $module;
/**
     * @var string $message
     *
     * @Field(type="string")
     */
    protected $message;
/**
     * @var array
     *
     * @Field(type="hash")
     */
    protected $data;
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->update = new \DateTime();
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getShop(): string
    {
        return $this->shop;
    }

    /**
     * @param string $shop
     */
    public function setShop(string $shop): void
    {
        $this->shop = $shop;
    }

    /**
     * @return \DateTime
     */
    public function getCreated(): \DateTime
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated(\DateTime $created): void
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated(): \DateTime
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated(\DateTime $updated): void
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getReferenceUuid(): string
    {
        return $this->referenceUuid;
    }

    /**
     * @param string $referenceUuid
     */
    public function setReferenceUuid(string $referenceUuid): void
    {
        $this->referenceUuid = $referenceUuid;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData(array $data): void
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getModule(): string
    {
        return $this->module;
    }

    /**
     * @param string $module
     */
    public function setModule(string $module): void
    {
        $this->module = $module;
    }

    /**
     * @return string
     */
    public function getShopName(): string
    {
        return (string)$this->shopName;
    }

    /**
     * @param string $shopName
     */
    public function setShopName(string $shopName): void
    {
        $this->shopName = $shopName;
    }

    /**
     * @return string
     */
    public function getContact(): string
    {
        return (string)$this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact(string $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getContactEmail(): string
    {
        return (string)$this->contactEmail;
    }

    /**
     * @param string $contactEmail
     */
    public function setContactEmail(string $contactEmail): void
    {
        $this->contactEmail = $contactEmail;
    }
}
