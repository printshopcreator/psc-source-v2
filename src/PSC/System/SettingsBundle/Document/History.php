<?php

namespace PSC\System\SettingsBundle\Document;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Date;
use PSC\System\SettingsBundle\Interfaces\History as PSCHistory;

use function iconv;

/**
 * @Document
 */
abstract class History implements PSCHistory
{
    /**
     * @Id
     */
    private string $id;

    /**
     * @Field(type="date")
     */
    private DateTime $created;

    /**
     * @Field(type="string")
     */
    private string $contact;

    /**
     * @Field(type="string")
     */
    private string $username;

    /**
     * @Field(type="hash")
     */
    private array $changeset;

    function __construct()
    {
        $this->created = new DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getContact()
    {
        return $this->contact;
    }

    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    public function getChangeset()
    {
        return $this->changeset;
    }

    public function setChangeset($changeset)
    {
        $this->changeset = $changeset;
    }

    public function setUsername($username)
    {
        $this->username = $username;
    }

    public function getUsername()
    {
        return $this->username;
    }
}
