<?php

namespace PSC\System\SettingsBundle\Loader;

use Liip\ImagineBundle\Model\Binary;

class PdfDataLoader extends \Liip\ImagineBundle\Binary\Loader\FileSystemLoader
{
    /**
     * {@inheritdoc}
     */
    public function find($path)
    {
        $path = $this->locator->locate($path);
        $mime = $this->mimeTypeGuesser->guessMimeType($path);

        if (false === strpos($mime, 'pdf')) {
            throw new \LogicException(sprintf('The mime type of pdf %s must be application/pdf got %s.', $path, $mime));
        }

        $im = new \Imagick();
        $im->setresolution(300, 300);
        $im->setcolorspace(\IMagick::COLORSPACE_RGB);
        $im->readimage($path . '[0]');
        $im->setimageformat('jpeg');

        return new Binary($im->getImageBlob(), $im->getImageMimeType());
    }
}
