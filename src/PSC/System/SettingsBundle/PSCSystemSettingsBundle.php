<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\SettingsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * SettingsBundle
 *
 * @package PSC\System\Search
 */
class PSCSystemSettingsBundle extends Bundle
{
}
