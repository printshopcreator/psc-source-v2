<?php

namespace PSC\System\SettingsBundle\Model;

use OpenApi\Annotations as OA;

class Status
{
    /**
     * @OA\Property(type="string")
     */
    private string $uuid;

    /**
     * @OA\Property(type="integer")
     */
    private int $code;

    /**
     * @OA\Property(type="string")
     */
    private string $color;

    /**
     * @OA\Property(type="string")
     */
    private string $internalName;

    /**
     * @OA\Property(type="string")
     */
    private string $externalName;

    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setCode(int $code): void
    {
        $this->code = $code;
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    public function getColor(): string
    {
        return $this->color;
    }

    public function setInternalName(string $internalName): void
    {
        $this->internalName = $internalName;
    }

    public function getInternalName(): string
    {
        return $this->internalName;
    }

    public function setExternalName(string $externalName): void
    {
        $this->externalName = $externalName;
    }

    public function getExternalName(): string
    {
        return $this->externalName;
    }
}
