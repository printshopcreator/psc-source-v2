<?php

namespace PSC\System\SettingsBundle\Model;

use OpenApi\Annotations as OA;

class Paper {

    /**
     * @OA\Property(type="integer")
     */
    private int $id;

    /**
     * @OA\Property(type="string")
     */
    private string $artNr;

    /**
     * @OA\Property(type="string")
     */
    private ?string $description1 = null;

    /**
     * @OA\Property(type="string")
     */
    private ?string $description2 = null;

    /**
     * @OA\Property(type="integer")
     */
    private ?int $grammatur = null;

    /**
     * @OA\Property(type="number", format="double")
     */
    private ?float $price = null; 

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp1 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp2 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp3 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp4 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp5 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp6 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp7 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp8 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp9 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp10 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp11 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp12 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp13 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $papiertyp14 = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $umschlagen = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $happy = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $eq = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $sense = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $sky = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $glam = false;

    /**
     * @OA\Property(type="boolean")
     */
    private $post = false;

    public function setId(int $id): void {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId(): int {
        return $this->id;
    }

    public function setArtNr(string $artNr): void {
        $this->artNr = $artNr;
    }

    /**
     * @return string
     */
    public function getArtNr(): string 
    {
        return $this->artNr;
    }

    public function setDescription1(string $description1): void
    {
        $this->description1 = $description1;
    }

    /**
     * @return string
     */
    public function getDescription1(): ?string
    {
        return $this->description1;
    }
  
    public function setDescription2(string $description2): void
    {
        $this->description2 = $description2;
    }

    /**
     * @return string
     */
    public function getDescription2(): ?string
    {
        return $this->description2;
    }
 
    public function setGrammatur(int $grammatur): void
    {
        $this->grammatur = $grammatur;
    }

    /**
     * @return int
     */
    public function getGrammatur(): ?int
    {
        return $this->grammatur;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp1()
    {
        return $this->papiertyp1;
    }
 
    public function setPapiertyp1($papiertyp1)
    {
        $this->papiertyp1 = $papiertyp1;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp2()
    {
        return $this->papiertyp2;
    }
 
    public function setPapiertyp2($papiertyp2)
    {
        $this->papiertyp2 = $papiertyp2;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp3()
    {
        return $this->papiertyp3;
    }
 
    public function setPapiertyp3($papiertyp3)
    {
        $this->papiertyp3 = $papiertyp3;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp4()
    {
        return $this->papiertyp4;
    }
 
    public function setPapiertyp4($papiertyp4)
    {
        $this->papiertyp4 = $papiertyp4;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp5()
    {
        return $this->papiertyp5;
    }
 
    public function setPapiertyp5($papiertyp5)
    {
        $this->papiertyp5 = $papiertyp5;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp6()
    {
        return $this->papiertyp6;
    }
 
    public function setPapiertyp6($papiertyp6)
    {
        $this->papiertyp6 = $papiertyp6;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp7()
    {
        return $this->papiertyp7;
    }
 
    public function setPapiertyp7($papiertyp7)
    {
        $this->papiertyp7 = $papiertyp7;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp8()
    {
        return $this->papiertyp8;
    }
 
    public function setPapiertyp8($papiertyp8)
    {
        $this->papiertyp8 = $papiertyp8;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp9()
    {
        return $this->papiertyp9;
    }
 
    public function setPapiertyp9($papiertyp9)
    {
        $this->papiertyp9 = $papiertyp9;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp10()
    {
        return $this->papiertyp10;
    }
 
    public function setPapiertyp10($papiertyp10)
    {
        $this->papiertyp10 = $papiertyp10;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp11()
    {
        return $this->papiertyp11;
    }
 
    public function setPapiertyp11($papiertyp11)
    {
        $this->papiertyp11 = $papiertyp11;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp12()
    {
        return $this->papiertyp12;
    }
 
    public function setPapiertyp12($papiertyp12)
    {
        $this->papiertyp12 = $papiertyp12;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp13()
    {
        return $this->papiertyp13;
    }
 
    public function setPapiertyp13($papiertyp13)
    {
        $this->papiertyp13 = $papiertyp13;
    }

    /**
     * @return boolean
     */
    public function getPapiertyp14()
    {
        return $this->papiertyp14;
    }
 
    public function setPapiertyp14($papiertyp14)
    {
        $this->papiertyp14 = $papiertyp14;
    }

    /**
     * @return boolean
     */
    public function getUmschlagen()
    {
        return $this->umschlagen;
    }
 
    public function setUmschlagen($umschlagen)
    {
        $this->umschlagen = $umschlagen;
    }

    /**
     * @return boolean
     */
    public function getHappy()
    {
        return $this->happy;
    }
 
    public function setHappy($happy)
    {
        $this->happy = $happy;
    }

    /**
     * @return boolean
     */
    public function getEq()
    {
        return $this->eq;
    }
 
    public function setEq($eq)
    {
        $this->eq = $eq;
    }

    /**
     * @return boolean
     */
    public function getSense()
    {
        return $this->sense;
    }
 
    public function setSense($sense)
    {
        $this->sense = $sense;
    }

    /**
     * @return boolean
     */
    public function getSky()
    {
        return $this->sky;
    }
 
    public function setSky($sky)
    {
        $this->sky = $sky;
    }

    /**
     * @return boolean
     */
    public function getGlam()
    {
        return $this->glam;
    }
 
    public function setGlam($glam)
    {
        $this->glam = $glam;
    }

    /**
     * @return boolean
     */
    public function getPost()
    {
        return $this->post;
    }
 
    public function setPost($post)
    {
        $this->post = $post;
    }

}
