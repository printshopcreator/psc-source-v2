<?php

namespace PSC\System\SettingsBundle\Model;

use OpenApi\Annotations as OA;

class Papercontainer
{
    /**
     * @OA\Property(type="string")
     */
    private ?string $content = null;

    public function setContent(string $content)
    {
        $this->content = $content;
    }

    public function getContent()
    {
        return $this->content;
    }
}
