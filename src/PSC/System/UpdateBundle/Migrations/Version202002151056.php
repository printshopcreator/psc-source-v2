<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version202002151056 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE orders MODIFY info text null;");
        $this->entityManager->getConnection()->exec("ALTER TABLE orders MODIFY delivery_address int(8) null;");
    }
}
