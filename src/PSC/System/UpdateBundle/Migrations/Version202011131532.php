<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version202011131532 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY custom_1_text text null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY vorlage_info text null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY confirm int(1) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY confirmone int(1) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY confirmaccount_id int(8) null");
    }
}
