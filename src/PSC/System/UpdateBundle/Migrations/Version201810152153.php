<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version201810152153 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE cms ADD enable INT(1) NULL");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_keywords text null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_description text null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_author varchar(255)  null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_custom_title varchar(255)  null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_og_title varchar(255)  null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_og_type varchar(255)  null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_og_url varchar(255)  null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_og_image varchar(255) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY meta_og_description text null");
    }
}
