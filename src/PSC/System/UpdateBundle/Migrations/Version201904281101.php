<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version201904281101 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdname");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdoptions");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdremoteoptions");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdkeycode");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdurl1");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdurl2");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdurl3");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdurl4");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdurl5");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanydestrict");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanyname");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanystreet");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanycity");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanytel");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanyfax");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanyemail");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanyweb");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN pdcompanyid");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_username");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_password");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_uid");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_gid");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_dir");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_quotafiles");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_quotasize");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_ulbandwidth");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop DROP COLUMN ftp_dlbandwidth");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop MODIFY parameter LONGTEXT");
        $this->entityManager->getConnection()->exec("ALTER TABLE shop MODIFY formel LONGTEXT");
    }
}
