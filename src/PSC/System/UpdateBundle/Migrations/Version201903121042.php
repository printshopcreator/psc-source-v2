<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version201903121042 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY preis float null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY upload_article_status int(4) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY upload_post_status int(4) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY upload_email_status int(4) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY upload_center_status int(4) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE orderspos ADD INDEX (uuid)");
    }
}
