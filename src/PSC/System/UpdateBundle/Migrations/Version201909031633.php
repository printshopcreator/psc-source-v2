<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

use PSC\Shop\EntityBundle\Document\Country;

class Version201909031633 extends Base
{
    public function migrateDatabase()
    {

        $countrys = $this->doctrineMongo
            ->getRepository('PSC\Shop\EntityBundle\Document\Country')
            ->findAll();
        if (count($countrys) == 0) {
            $shops = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findAll();
            foreach ($shops as $shop) {
                $country = new Country();
                $country->setShop($shop->getUid());
                $country->setCode("DE");
                $country->setWithTaxWithUstNr(true);
                $country->setWithTaxWithoutUstNr(true);
                $this->doctrineMongo->getManager()->persist($country);
            }

            $this->doctrineMongo->getManager()->flush();
        }
    }
}
