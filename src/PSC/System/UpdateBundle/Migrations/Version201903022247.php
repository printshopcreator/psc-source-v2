<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version201903022247 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY custom_1_title varchar(255) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY vorlage_file varchar(255) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY confirmaccount_id int(8) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE credit_system MODIFY pre_code varchar(255) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE credit_system MODIFY articlegroup_id varchar(255) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE credit_system MODIFY product_id varchar(255) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE credit_system_detail MODIFY contact_id int(8) null");
    }
}
