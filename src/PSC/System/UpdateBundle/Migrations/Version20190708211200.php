<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version20190708211200 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE shippingtype ADD price_from INT(8) NULL DEFAULT 0");
        $this->entityManager->getConnection()->exec("ALTER TABLE shippingtype ADD price_to INT(8) NULL DEFAULT 0");
    }
}
