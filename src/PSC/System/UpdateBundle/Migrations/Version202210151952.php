<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version202210151952 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("
            alter table layouter_session
                modify xmlconfig mediumtext null;
            alter table layouter_session
                modify xmlpages mediumtext null;
            alter table layouter_session
                modify xmlpagetemplates mediumtext null;
            alter table layouter_session
                modify xmlpageobjects mediumtext null;
            alter table layouter_session
                modify xmlxslfo mediumtext null;
            alter table layouter_session
                modify xmlpreviewxslfo mediumtext null;
            alter table layouter_session
                modify xmlextendpreviewxslfo mediumtext null;
            alter table layouter_session
                modify title varchar(255) null;
            alter table layouter_session
                modify title varchar(255) null;
            alter table layouter_session
                modify preview_path varchar(255) null;
            alter table layouter_session
                modify designer_xml longtext null; 
            alter table layouter_session
                modify template_print_id varchar(255) null;
            ");
    }
}
