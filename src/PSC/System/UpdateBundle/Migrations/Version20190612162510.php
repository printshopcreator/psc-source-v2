<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version20190612162510 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE article_group ADD private INT(1) NULL DEFAULT 0");
        $this->entityManager->getConnection()->exec("CREATE TABLE `contact_productgroup` (
          `contact_id` bigint(20) NOT NULL DEFAULT '0',
          `productgroup_id` bigint(20) NOT NULL DEFAULT '0',
          PRIMARY KEY (`contact_id`,`productgroup_id`),
          KEY `productgroup_id` (`productgroup_id`)
        )");
        $this->entityManager->getConnection()->exec("CREATE TABLE `account_productgroup` (
          `account_id` bigint(20) NOT NULL DEFAULT '0',
          `productgroup_id` bigint(20) NOT NULL DEFAULT '0',
          PRIMARY KEY (`account_id`,`productgroup_id`),
          KEY `productgroup_id` (`productgroup_id`)
        )");
    }
}
