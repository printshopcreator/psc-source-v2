<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version201810061259 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE contact_address MODIFY install_id int(8) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE `contact_address` MODIFY `firstname` varchar(100) null;
                ALTER TABLE `contact_address` MODIFY `lastname` varchar(100) null;
                ALTER TABLE `contact_address` MODIFY `street` varchar(255) null;
                ALTER TABLE `contact_address` MODIFY `house_number` varchar(20) null;
                ALTER TABLE `contact_address` MODIFY `zip` varchar(10) null;
                ALTER TABLE `contact_address` MODIFY `city` varchar(255) null;
                ALTER TABLE `contact_address` MODIFY `phone` varchar(100) null;
                ALTER TABLE `contact_address` MODIFY `mobil_phone` varchar(100) null;
                ALTER TABLE `contact_address` MODIFY `email` varchar(255) null;
                ALTER TABLE `contact_address` MODIFY `company` varchar(100) null;
                ALTER TABLE `contact_address` MODIFY `country` varchar(100) null;");
    }
}
