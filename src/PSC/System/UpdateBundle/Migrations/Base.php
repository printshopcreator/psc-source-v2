<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use PSC\System\PluginBundle\Document\Plugin;
use PSC\System\UpdateBundle\Document\Migration;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

abstract class Base
{
    protected $mustRun = true;
    protected $entityManager;
    protected $doctrineMongo;
    protected $kernelRootDir;
/**
     * Base constructor.
     * @param $entityManager
     * @param $doctrineMongo
     */
    public function __construct(EntityManager $entityManager, ManagerRegistry $doctrineMongo, $kernelRootDir)
    {
        $this->entityManager = $entityManager;
        $this->doctrineMongo = $doctrineMongo;
        $this->kernelRootDir = $kernelRootDir;
    }


    /**
     * @return bool
     */
    public function isMustRun()
    {
        return $this->mustRun;
    }


    /**
     * @param bool $mustRun
     */
    public function setMustRun($mustRun)
    {
        $this->mustRun = $mustRun;
    }

    public function getName()
    {
        return get_class($this);
    }

    public function execute()
    {

        $this->migrateDatabase();
        $this->installDefaultPlugins();
        $migration = new Migration();
        $migration->setVersion($this->getName());
        $migration->setCreated(new \DateTime());
        $this->doctrineMongo->getManager()->persist($migration);
        $this->doctrineMongo->getManager()->flush();
    }

    abstract public function migrateDatabase();
    private function installDefaultPlugins()
    {
        $finder = new Finder();
        $files = $finder
            ->directories()->depth('== 2')
            ->in($this->kernelRootDir . '/var/plugins/');
    /** @var \PSC\System\PluginBundle\Interfaces\Plugin $plugin */
        $plugin = new \Plugin\System\PSC\Bootstrap4\Plugin();
        $plugObj = $this->doctrineMongo->getManager()
            ->getRepository('PSC\System\PluginBundle\Document\Plugin')
            ->findOneBy(['pluginId' => $plugin->getIdentifier()]);
    /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $name = str_replace('/', '\\', '/Plugin/' . $file->getRelativePathname() . '/Plugin');
/** @var \PSC\System\PluginBundle\Interfaces\Plugin $plugin */
            $plugin = new $name();
            if (!$plugObj) {
                $plugObj = new Plugin();
                $plugObj->setInstalled(false);
                $plugObj->setName($plugin->getTitle());
                $plugObj->setNamespace($name);
                $plugObj->setPath($file->getRelativePathname());
                $plugObj->setDescription($plugin->getDescription());
                $plugObj->setPluginId($plugin->getIdentifier());
                $plugObj->setVersion($plugin->getVersion());
                $plugObj->setType($plugin->getType());
                $this->doctrineMongo->getManager()->persist($plugObj);
            }
        }
    }
}
