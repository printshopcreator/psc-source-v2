<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version201811271538 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY sor varchar(10) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY url varchar(255) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY parent int(8) null");
        $this->entityManager->getConnection()->exec("ALTER TABLE cms MODIFY pos varchar(100) null");
    }
}
