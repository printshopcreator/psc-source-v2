<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Migrations;

class Version202206051128 extends Base
{
    public function migrateDatabase()
    {
        $this->entityManager->getConnection()->exec("ALTER TABLE paymenttype MODIFY mwert DOUBLE NULL");
        $this->entityManager->getConnection()->exec("ALTER TABLE article MODIFY mwert DOUBLE NULL");
        $this->entityManager->getConnection()->exec("ALTER TABLE shippingtype MODIFY mwert DOUBLE NULL");
    }
}
