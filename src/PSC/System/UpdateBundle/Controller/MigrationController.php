<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\UpdateBundle\Controller;

use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\System\PluginBundle\Document\Plugin;
use PSC\System\PluginBundle\Event\ClearCache;
use PSC\System\PluginBundle\Service\Manager;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use PSC\System\UpdateBundle\Service\Migration;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * MigrationController
 *
 * @package    PSC\System\Update
 * @subpackage Controller
 */
class MigrationController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/migration/do", name="psc_system_update_migration_do")
     * @Template()
     *
     * @param Request $request
     * @param Migration $migration
     * @return array
     */
    public function doAction(Request $request, Migration $migration)
    {

        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $migration->executeMigrations();
            }
        }


        return array('migrations' => $migration->listMigrations(), 'form' => $form->createView(), 'mustRun' => $migration->checkIfMigrationMustRun());
    }
}
