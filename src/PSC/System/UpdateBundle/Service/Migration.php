<?php

namespace PSC\System\UpdateBundle\Service;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\System\UpdateBundle\Migrations\Base;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Migration Service
 *
 * @author Thomas Peterson
 */
class Migration
{
    protected $entityManager;
    protected $doctrineMongo;
    protected $kernel;

    public function __construct(EntityManagerInterface $entityManager, ManagerRegistry $doctrine_mongodb, KernelInterface $kernel)
    {
        $this->entityManager = $entityManager;
        $this->doctrineMongo = $doctrine_mongodb;
        $this->kernel = $kernel;
    }

    public function listMigrations()
    {
        return $this->buildMigrationArray();
    }

    public function executeMigrations()
    {
        $migrations = $this->buildMigrationArray();

        /** @var Base $migration */
        foreach ($migrations as $migration) {
            if ($migration->isMustRun()) {
                $migration->execute();
            }
        }
    }

    public function checkIfMigrationMustRun()
    {
        $migrations = $this->buildMigrationArray();

        /** @var Base $migration */
        foreach ($migrations as $migration) {
            if ($migration->isMustRun()) {
                return true;
            }
        }
    }

    private function buildMigrationArray()
    {
        $migrations = array();

        $fIt = new Finder();

        /** @var SplFileInfo $file */
        foreach ($fIt->in(__DIR__ . '/../Migrations')->files()->name("Version*.php") as $file) {
            $mg = 'PSC\System\UpdateBundle\Migrations\\' . $file->getBasename('.php');

            /** @var Base $migration */
            $migration = new $mg($this->entityManager, $this->doctrineMongo, $this->kernel->getProjectDir());

            $update = $this->doctrineMongo->getRepository('PSC\System\UpdateBundle\Document\Migration')
                ->findOneBy(['version' => $mg]);

            if ($update) {
                $migration->setMustRun(false);
            }

            $migrations[] = $migration;
        }

        return $migrations;
    }
}
