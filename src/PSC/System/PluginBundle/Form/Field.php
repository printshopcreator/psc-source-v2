<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\Form;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

abstract class Field implements \PSC\System\PluginBundle\Form\Interfaces\Field
{
    protected $tab = null;

    public function getTab()
    {
        return $this->tab;
    }
}
