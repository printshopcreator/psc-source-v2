<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\Form;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

abstract class Group implements \PSC\System\PluginBundle\Form\Interfaces\Group
{
    protected $title = 'general';

    public function getTitle()
    {
        return $this->title;
    }

    public function getId()
    {
        return md5($this->title);
    }
}
