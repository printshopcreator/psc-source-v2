<?php

namespace PSC\System\PluginBundle\Form\Chain;

class Section
{
    private $sections = [];

    public function __construct()
    {
    }

    public function add(\PSC\System\PluginBundle\Form\Interfaces\Section $section)
    {
        if (!isset($this->sections[$section->getModule()])) {
            $this->sections[$section->getModule()] = array();
        }
        $this->sections[$section->getModule()][] = $section;
    }

    public function get($module = false)
    {
        if ($module) {
            if (!isset($this->sections[$module])) {
                return [];
            }
            return $this->sections[$module];
        }

        return $this->sections;
    }
}
