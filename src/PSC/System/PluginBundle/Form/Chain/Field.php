<?php

namespace PSC\System\PluginBundle\Form\Chain;

use PSC\System\PluginBundle\Form\Interfaces\Group;

class Field
{
    /** @var \PSC\System\PluginBundle\Form\Interfaces\Field[] */
    private $fields = [];

    /** @var Group[] */
    private $groups = [];

    /** @var \PSC\System\PluginBundle\Form\Interfaces\Field[] */
    private $specialProductFields = [];

    /** @var Group[] */
    private $specialProductGroups = [];

    /** @var \PSC\System\PluginBundle\Form\Interfaces\Field[] */
    private $specialThemeFields = [];

    /** @var Group[] */
    private $specialThemeGroups = [];

    public function __construct()
    {
    }

    public function addField(\PSC\System\PluginBundle\Form\Interfaces\Field $field)
    {
        if (!isset($this->fields[$field->getModule()])) {
            $this->fields[$field->getModule()] = array();
        }

        $this->fields[$field->getModule()][] = $field;
    }

    public function addGroup(\PSC\System\PluginBundle\Form\Interfaces\Group $group)
    {
        if (!isset($this->groups[$group->getModule()])) {
            $this->groups[$group->getModule()] = array();
        }
        $this->groups[$group->getModule()][] = $group;
    }

    public function addProductField(\PSC\System\PluginBundle\Form\Interfaces\Field $field, $productType = 0)
    {
        if (!isset($this->specialProductFields[$field->getModule()])) {
            $this->specialProductFields[$field->getModule()] = array();
        }
        if (!isset($this->specialProductFields[$field->getModule()][$productType])) {
            $this->specialProductFields[$field->getModule()][$productType] = array();
        }
        $this->specialProductFields[$field->getModule()][$productType][] = $field;
    }

    public function addThemeField(\PSC\System\PluginBundle\Form\Interfaces\Field $field, $themeType = "")
    {
        if (!isset($this->specialThemeFields[$field->getModule()])) {
            $this->specialThemeFields[$field->getModule()] = array();
        }
        if (!isset($this->specialThemeFields[$field->getModule()][$themeType])) {
            $this->specialThemeFields[$field->getModule()][$themeType] = array();
        }
        $this->specialThemeFields[$field->getModule()][$themeType][] = $field;
    }

    public function addProductGroup(\PSC\System\PluginBundle\Form\Interfaces\Group $group, $productType = 0)
    {
        if (!isset($this->specialProductGroups[$group->getModule()])) {
            $this->specialProductGroups[$group->getModule()] = array();
        }
        if (!isset($this->specialProductGroups[$group->getModule()][$productType])) {
            $this->specialProductGroups[$group->getModule()][$productType] = array();
        }
        $this->specialProductGroups[$group->getModule()][$productType][] = $group;
    }

    public function addThemeGroup(\PSC\System\PluginBundle\Form\Interfaces\Group $group, $themeType = "")
    {
        if (!isset($this->specialThemeGroups[$group->getModule()])) {
            $this->specialThemeGroups[$group->getModule()] = array();
        }
        if (!isset($this->specialThemeGroups[$group->getModule()][$themeType])) {
            $this->specialThemeGroups[$group->getModule()][$themeType] = array();
        }
        $this->specialThemeGroups[$group->getModule()][$themeType][] = $group;
    }

    public function getGroups($module = false)
    {
        if ($module) {
            if (!isset($this->groups[$module])) {
                return [];
            }
            return $this->groups[$module];
        }

        return $this->groups;
    }

    public function getProductGroups($module = false, $productType = 0)
    {
        if ($module) {
            if (!isset($this->specialProductGroups[$module]) || !isset($this->specialProductGroups[$module][intval($productType)])) {
                return [];
            }
            return $this->specialProductGroups[$module][intval($productType)];
        }

        return $this->specialProductGroups;
    }

    public function getThemeGroups($module = false, $themeType = "")
    {
        if ($module) {
            if (!isset($this->specialThemeGroups[$module]) || !isset($this->specialThemeGroups[$module][$themeType])) {
                return [];
            }
            return $this->specialThemeGroups[$module][$themeType];
        }

        return $this->specialThemeGroups;
    }

    public function getFields($module = false)
    {
        if ($module) {
            if (!isset($this->fields[$module])) {
                return [];
            }
            return $this->fields[$module];
        }

        return $this->fields;
    }

    public function getProductFields($module = false, $productType = 0)
    {
        if ($module) {
            if (!isset($this->specialProductFields[$module]) || !isset($this->specialProductFields[$module][intval($productType)])) {
                return [];
            }
            return $this->specialProductFields[$module][$productType];
        }

        return $this->specialProductFields;
    }

    public function getThemeFields($module = false, $themeType = "")
    {
        if ($module) {
            if (!isset($this->specialThemeFields[$module]) || !isset($this->specialThemeFields[$module][$themeType])) {
                return [];
            }
            return $this->specialThemeFields[$module][$themeType];
        }

        return $this->specialThemeFields;
    }
}
