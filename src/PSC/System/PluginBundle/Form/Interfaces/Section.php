<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\Form\Interfaces;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

interface Section
{
    const Cms = 1;
    const Product = 2;
    const Shipping = 3;
    const Payment = 4;
    const Productgroup = 5;
    const News = 6;
    const Account = 7;
    const Contact = 8;
    const Shop = 9;
    const Order = 10;
    public function getTitle();
    public function getId();
    public function getModule();
    public function getController();
}
