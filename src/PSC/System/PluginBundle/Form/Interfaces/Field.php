<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\Form\Interfaces;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;

interface Field
{
    const Cms = 1;
    const Product = 2;
    const Shipping = 3;
    const Payment = 4;
    const Productgroup = 5;
    const News = 6;
    const Account = 7;
    const Contact = 8;
    const Shop = 9;
    const Order = 10;
    const Voucher = 11;
    const Theme = 12;
    const System = 13;
    const GroupGeneral = 'general';
    public function formPreSubmit(FormEvent $event);
    public function formPostSubmit(FormEvent $event);
    public function formPostSetData(FormEvent $event);
    public function formPreSetData(FormEvent $event);
    public function getModule();
    public function getGroup();
    public function getTemplate();
    public function buildForm(FormBuilderInterface $builder, array $options);
}
