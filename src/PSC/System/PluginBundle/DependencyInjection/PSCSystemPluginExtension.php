<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\DependencyInjection;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 *
 * @package PSC\Settings
 */
class PSCSystemPluginExtension extends Extension implements PrependExtensionInterface
{
    /**
     * Loads a specific configuration.
     *
     * @param array            $configs   An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws InvalidArgumentException When provided tag is not defined in this extension
     *
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        try {
            $collection = (new \MongoDB\Client('mongodb://mongodb/'))->psc->Plugin;
            $plugins = $collection->find(array('installed' => true));
            foreach ($plugins as $plugin) {
                if (file_exists($container->getParameter('kernel.project_dir') . '/var/plugins/' . $plugin['path'] . '/Resources/config/services.yml')) {
                    $loader = new Loader\YamlFileLoader($container, new FileLocator($container->getParameter('kernel.project_dir') . '/var/plugins/' . $plugin['path'] . '/Resources/config'));
                    $loader->load('services.yml');
                }
            }
        } catch (\Exception $e) {
            dump($e->getMessage());
        }
    }

    public function prepend(ContainerBuilder $container)
    {
        $apiPlatformConfig = ['mapping' => ['paths' => []]];
        try {
            $collection = (new \MongoDB\Client('mongodb://mongodb/'))->psc->Plugin;
            $plugins = $collection->find(array('installed' => true));
            foreach ($plugins as $plugin) {
                if (file_exists($container->getParameter('kernel.project_dir') . '/var/plugins/' . $plugin['path'] . '/Model')) {
                    $apiPlatformConfig['mapping']['paths'][] = $container->getParameter('kernel.project_dir') . '/var/plugins/' . $plugin['path'] . '/Model';
                }
            }
        } catch (\Exception $e) {
            dump($e->getMessage());
        }

        $container->prependExtensionConfig('api_platform', $apiPlatformConfig);
    }
}
