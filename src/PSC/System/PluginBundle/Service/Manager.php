<?php

namespace PSC\System\PluginBundle\Service;

use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\PluginBundle\Document\Plugin;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\SecurityContext;

/**
 * Plugin Manager Service
 *
 * @author Thomas Peterson
 */
class Manager
{
    protected $kernelDir;

    protected $pluginThemeDir;

    public function __construct(KernelInterface $kernelDir)
    {
        $this->kernelDir = $kernelDir->getProjectDir();
        $this->pluginThemeDir = $kernelDir->getProjectDir() . '/web/plugins';
    }

    public function install(Plugin $pluginDoc)
    {

        /** @var \PSC\System\PluginBundle\Interfaces\Plugin $pluginObj */
        $pluginObj = $this->getPluginClass($pluginDoc);

        $this->copyPublicFiles($pluginDoc, $pluginObj);
    }

    public function uninstall(Plugin $plugin)
    {
    }

    private function copyPublicFiles(Plugin $pluginDoc, \PSC\System\PluginBundle\Interfaces\Plugin $pluginObj)
    {
        $fs = new Filesystem();

        $pathToPluginPublic = $this->getPluginDir($pluginDoc) . '/Resources/public';
        $pathToWebPluginPublic = strtolower($this->pluginThemeDir . '/' . $pluginDoc->getPath());
        if (file_exists($pathToPluginPublic)) {
            $fs->symlink($pathToPluginPublic, $pathToWebPluginPublic);
        }
    }

    private function getPluginClass(Plugin $plugin)
    {
        $name = str_replace('/', '\\', '/Plugin/' . $plugin->getPath() . '/Plugin');

        return new $name();
    }

    private function getPluginDir(Plugin $plugin)
    {
        return $this->kernelDir . '/var/plugins/' . $plugin->getPath();
    }
}
