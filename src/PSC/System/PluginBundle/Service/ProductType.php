<?php

namespace PSC\System\PluginBundle\Service;

use PSC\System\PluginBundle\Form\Interfaces\Group;
use PSC\System\PluginBundle\Product\Type;

class ProductType
{
    /** @var Type[] */
    private $productTypes = [];

    public function addProductType(Type $type)
    {
        $this->productTypes[$type->getId()] = $type;
    }

    public function getProductTypes()
    {
        return $this->productTypes;
    }

    public function getProductType($id)
    {
        if (!isset($this->productTypes[$id])) {
            return null;
        }

        return $this->productTypes[$id];
    }

    public function getProductTypeName($id)
    {
        if (!isset($this->productTypes[$id])) {
            return 'nicht installiert';
        }

        return $this->productTypes[$id]->getName();
    }
}
