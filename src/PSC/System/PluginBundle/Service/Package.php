<?php

namespace PSC\System\PluginBundle\Service;

use PSC\System\PluginBundle\Package\Type;

class Package
{
    /** @var Type[] */
    private $packageTypes = [];

    public function addPackageType(Type $type)
    {
        $this->packageTypes[$type->getId()] = $type;
    }

    public function getPackageTypes()
    {
        return $this->packageTypes;
    }

    public function getPackageType($id)
    {
        if (!isset($this->packageTypes[$id])) {
            return null;
        }

        return $this->packageTypes[$id];
    }
}
