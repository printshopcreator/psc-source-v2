<?php

namespace PSC\System\PluginBundle\EventListener;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\DependencyInjection\Loader;
use Twig\Loader\LoaderInterface;

class RequestListener
{
    protected $loader = null;
    protected $kernel_root_dir = null;

    public function __construct(LoaderInterface $loader, $kernel_root_dir)
    {
        $this->loader = $loader;
        $this->kernel_root_dir = $kernel_root_dir;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        try {
            $collection = (new \MongoDB\Client('mongodb://mongodb/'))->psc->Plugin;
        } catch (\Exception $e) {
            return;
        }

        $plugins = $collection->find(array('installed' => true));

        foreach ($plugins as $plugin) {
            if (file_exists($this->kernel_root_dir . '/var/plugins/' . $plugin['path'] . '/Resources/views')) {
                $this->loader->addPath(
                    $this->kernel_root_dir . '/var/plugins/' . $plugin['path'] . '/Resources/views',
                    'Plugin' . str_replace('/', '', $plugin['path'])
                );
            }
        }
    }
}
