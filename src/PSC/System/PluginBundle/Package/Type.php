<?php

namespace PSC\System\PluginBundle\Package;

use PSC\Shop\OrderBundle\Model\Order;
use PSC\Shop\OrderBundle\Model\Order\Position;

interface Type
{
    public function getId();
    public function addFileToPositionArchive(\ZipArchive $archive, Order $order, Position $position, string $path);
    public function addFilesToOrderArchive(\ZipArchive $archive, Order $order);
}
