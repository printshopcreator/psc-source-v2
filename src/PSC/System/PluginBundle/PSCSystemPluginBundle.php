<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use MongoDB\Driver\Exception\ConnectionTimeoutException;
use PSC\System\PluginBundle\Compiler\FormPass;
use PSC\System\PluginBundle\Compiler\PluginCompiler;
use PSC\System\PluginBundle\Compiler\PluginPackageType;
use PSC\System\PluginBundle\Compiler\ProductType;
use PSC\System\PluginBundle\Interfaces\Plugin;
use Symfony\Component\DependencyInjection\Compiler\PassConfig;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader;

/**
 * PluginBundle
 *
 * @package PSC\System\Search
 */
class PSCSystemPluginBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new PluginCompiler(), PassConfig::TYPE_BEFORE_OPTIMIZATION);
        $container->addCompilerPass(new FormPass());
        $container->addCompilerPass(new ProductType());
        $container->addCompilerPass(new PluginPackageType());
    }
}
