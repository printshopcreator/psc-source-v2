<?php

namespace PSC\System\PluginBundle\Event;

use PSC\Shop\QueueBundle\Event\Event;

class EveryRun extends Event
{
    public function getType()
    {
        return 'system_plugin_every_run';
    }

    public function getDescription()
    {
        return 'Bei jeder Ausführung';
    }

    public function getData()
    {
    }

    public function setData($data)
    {
    }
}
