<?php

namespace PSC\System\PluginBundle\Event;

use PSC\Shop\QueueBundle\Event\Event;

class ClearCache extends Event
{
    public function getType()
    {
        return 'system_plugin_clear_cache';
    }

    public function getDescription()
    {
        return 'Clear cache';
    }

    public function getData()
    {
    }

    public function setData($data)
    {
    }
}
