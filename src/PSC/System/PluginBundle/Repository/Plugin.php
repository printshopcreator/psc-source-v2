<?php

namespace PSC\System\PluginBundle\Repository;

use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

class Plugin extends DocumentRepository
{
    public function removeByPluginId($pluginId)
    {
        return $this->createQueryBuilder()
            ->remove()
            ->field('pluginId')->equals($pluginId)
            ->getQuery()
            ->execute();
    }
}
