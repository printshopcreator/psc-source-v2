<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\Interfaces;

interface Plugin
{
    const Frontend = 'Frontend';
    const Backend = 'Backend';
    const Payment = 'Payment';
    const Shipping = 'Shipping';
    const Service = 'Service';
    const Queues = 'Queues';
    const Theme = 'Theme';
    const Cli = 'Cli';
    const Form = 'Form';
    const ProductType = 'ProductType';
    const Layouter = 'Layouter';
    public function getTitle();
    public function getDescription();
    public function getType();
    public function getIdentifier();
    public function getVersion();
}
