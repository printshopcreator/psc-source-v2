<?php

namespace PSC\System\PluginBundle\Compiler;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use PSC\System\PluginBundle\Interfaces\Plugin;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader;

class PluginCompiler implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        try {
            $collection = (new \MongoDB\Client('mongodb://mongodb/'))->psc->Plugin;
            $plugins = $collection->find(array('installed' => true));
/** @var Plugin $plugin */
            foreach ($plugins as $plugin) {
                $container->get('doctrine_mongodb.odm.metadata.chain')->addDriver(new AnnotationDriver(new AnnotationReader(), $container->getParameter('kernel.project_dir') . '/var/plugins/' . $plugin['path'] . '/Document'), $plugin['path']);
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            return;
        }
    }
}
