<?php

namespace PSC\System\PluginBundle\Compiler;

use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\PluginBundle\Form\Chain\Section;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ProductType implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has(\PSC\System\PluginBundle\Service\ProductType::class)) {
            return;
        }

        $definition = $container->findDefinition(\PSC\System\PluginBundle\Service\ProductType::class);

        $taggedServices = $container->findTaggedServiceIds('psc.product.type');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addProductType', [new Reference($id)]);
        }
    }
}
