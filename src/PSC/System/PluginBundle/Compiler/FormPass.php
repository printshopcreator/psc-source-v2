<?php

namespace PSC\System\PluginBundle\Compiler;

use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\PluginBundle\Form\Chain\Section;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class FormPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has(Field::class)) {
            return;
        }

        $definition = $container->findDefinition(Field::class);

        $taggedServices = $container->findTaggedServiceIds('psc.backend.custom.fields');
        foreach ($taggedServices as $id => $tags) {
            if (isset($tags[0]['productType'])) {
                $definition->addMethodCall('addProductField', array(new Reference($id), intval($tags[0]['productType'])));
            } elseif (isset($tags[0]['themeType'])) {
                $definition->addMethodCall('addThemeField', array(new Reference($id), $tags[0]['themeType']));
            } else {
                $definition->addMethodCall('addField', array(new Reference($id)));
            }
        }

        $taggedServices = $container->findTaggedServiceIds('psc.backend.custom.groups');
        foreach ($taggedServices as $id => $tags) {
            if (isset($tags[0]['productType'])) {
                $definition->addMethodCall('addProductGroup', array(new Reference($id), intval($tags[0]['productType'])));
            } elseif (isset($tags[0]['themeType'])) {
                $definition->addMethodCall('addThemeGroup', array(new Reference($id), $tags[0]['themeType']));
            } else {
                $definition->addMethodCall('addGroup', array(new Reference($id)));
            }
        }

        if (!$container->has(Section::class)) {
            return;
        }

        /** @var Section $defSection */
        $defSection = $container->findDefinition(Section::class);

        $taggedServices = $container->findTaggedServiceIds('psc.backend.custom.section');
        foreach ($taggedServices as $id => $tags) {
            $defSection->addMethodCall('add', array(new Reference($id)));
        }
    }
}
