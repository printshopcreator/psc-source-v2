<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\System\PluginBundle\Document\Plugin;
use PSC\System\PluginBundle\Event\ClearCache;
use PSC\System\PluginBundle\Service\Manager;
use PSC\System\SettingsBundle\Service\Assets;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Jungi\Bundle\EnvironmentBundle\Theme\Tag as LocalTag;
use Symfony\Component\Yaml\Yaml;

/**
 * PluginController fürs Backend
 *
 * @package    PSC\Shop\Queue
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/cache/clear", name="psc_system_plugin_backend_clear_cache")
     * @Template()
     *
     * @param Assets $assetsService
     * @return void
     */
    public function clearcacheAction(Assets $assetsService)
    {

        $assetsService->install();
        $fs = new Filesystem();
        $fs->remove($this->getParameter('kernel.cache_dir'));
        header('location: /apps/backend/plugin/list');
        die();
    }

    /**
     * Default Seite
     *
     * @Route("/list", name="psc_system_plugin_backend_list")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param KernelInterface $kernel
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function indexAction(Request $request, DocumentManager $documentManager, KernelInterface $kernel)
    {

        $version = Yaml::parse(file_get_contents($kernel->getProjectDir() . '/version.yaml'));
        $plugins = array();
        $finder = new Finder();
        $files = $finder
            ->directories()->depth('== 2')
            ->in($kernel->getProjectDir() . '/var/plugins/');
        $plugObjs = $documentManager
            ->getRepository('PSC\System\PluginBundle\Document\Plugin')
            ->findAll();
        $plugKeys = array();
/** @var Plugin $plugObj */
        foreach ($plugObjs as $plugObj) {
            if (in_array($plugObj->getPluginId(), $plugKeys)) {
                $documentManager->remove($plugObj);
                continue;
            }
            $plugKeys[] = $plugObj->getPluginId();
        }

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            $name = str_replace('/', '\\', '/Plugin/' . $file->getRelativePathname() . '/Plugin');
/** @var \PSC\System\PluginBundle\Interfaces\Plugin $plugin */
            $plugin = new $name();
            $plugObj = $documentManager
                ->getRepository('PSC\System\PluginBundle\Document\Plugin')
                ->findOneBy(['pluginId' => $plugin->getIdentifier()]);
            if (!$plugObj) {
                $plugObj = new Plugin();
                $plugObj->setInstalled(false);
                $plugObj->setName($plugin->getTitle());
                $plugObj->setNamespace($name);
                $plugObj->setPath($file->getRelativePathname());
                $plugObj->setDescription($plugin->getDescription());
                $plugObj->setPluginId($plugin->getIdentifier());
                $plugObj->setVersion($plugin->getVersion());
                $plugObj->setType($plugin->getType());
                $documentManager->persist($plugObj);
            } else {
                $plugObj->setName($plugin->getTitle());
                $plugObj->setNamespace($name);
                $plugObj->setPath($file->getRelativePathname());
                $plugObj->setDescription($plugin->getDescription());
                $plugObj->setPluginId($plugin->getIdentifier());
                $plugObj->setVersion($plugin->getVersion());
                $plugObj->setType($plugin->getType());
                $documentManager->persist($plugObj);
            }
            $plugKeys = array_diff($plugKeys, [$plugin->getIdentifier()]);
        }

        $documentManager->flush();
        foreach ($plugKeys as $key) {
            $plugins = $documentManager
                ->getRepository('PSC\System\PluginBundle\Document\Plugin')
                ->removeByPluginId($key);
        }

        $plugins = $documentManager
            ->getRepository('PSC\System\PluginBundle\Document\Plugin')
            ->findAll();
        return array(
            'plugins' => $plugins,
            'version' => $version['info']
        );
    }

    /**
     * Disable Seite
     *
     * @Route("/list/disable/{uuid}", name="psc_system_plugin_backend_list_disable")
     * @Security("is_granted('ROLE_SHOP')")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param Manager $pluginManager
     * @param \PSC\Shop\QueueBundle\Service\Event\Manager $eventManager
     * @param $uuid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function disableAction(Request $request, DocumentManager $documentManager, Manager $pluginManager, \PSC\Shop\QueueBundle\Service\Event\Manager $eventManager, $uuid)
    {


        $qb = $documentManager
            ->getRepository('PSC\System\PluginBundle\Document\Plugin')
            ->findOneBy(array('id' => (string)$uuid));
        $qb->setInstalled(false);
        $documentManager->persist($qb);
        $documentManager->flush();
        $pluginManager->uninstall($qb);
        $notify = new ClearCache();
        $notify->setShop(null);
        $eventManager->addJob($notify);
        return $this->redirectToRoute('psc_system_plugin_backend_clear_cache');
    }

    /**
     * enable Seite
     *
     * @Route("/list/enable/{uuid}", name="psc_system_plugin_backend_list_enable")
     * @Security("is_granted('ROLE_SHOP')")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param Manager $pluginManager
     * @param \PSC\Shop\QueueBundle\Service\Event\Manager $eventManager
     * @param $uuid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function enableAction(Request $request, DocumentManager $documentManager, Manager $pluginManager, \PSC\Shop\QueueBundle\Service\Event\Manager $eventManager, $uuid)
    {

        $qb = $documentManager
            ->getRepository('PSC\System\PluginBundle\Document\Plugin')
            ->findOneBy(array('id' => (string)$uuid));
        $qb->setInstalled(true);
        $documentManager->persist($qb);
        $documentManager->flush();
        $pluginManager->install($qb);
        $notify = new ClearCache();
        $notify->setShop(null);
        $eventManager->addJob($notify);
        return $this->redirectToRoute('psc_system_plugin_backend_clear_cache');
    }
}
