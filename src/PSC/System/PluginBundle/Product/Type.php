<?php

namespace PSC\System\PluginBundle\Product;

use PSC\Shop\ProductBundle\Interfaces\IJsonFormProducer;
use PSC\Shop\ProductBundle\Model\Price;
use PSC\Shop\ProductBundle\Model\Product;

interface Type
{
    public function getId();
    public function getName();
    public function getPositionProductTransformer();
    public function getJsonFormProducer(): IJsonFormProducer;
}
