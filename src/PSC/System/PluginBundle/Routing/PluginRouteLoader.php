<?php

namespace PSC\System\PluginBundle\Routing;

use Doctrine\ORM\EntityManager;
use PSC\System\PluginBundle\Document\Plugin;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Yaml\Yaml;

class PluginRouteLoader extends Loader
{
    protected $rootDir;
/**
     * DynamicRouteLoader constructor.
     *
     */
    public function __construct($rootDir)
    {
        $this->rootDir = $rootDir;
    }

    /**
     * Loads a resource.
     *
     * @param mixed       $resource The resource
     * @param string|null $type     The resource type or null if unknown
     *
     * @return RouteCollection
     * @throws \Exception If something went wrong
     */
    public function load($resource, $type = null)
    {
        $routes = new RouteCollection();
        try {
            $collection = (new \MongoDB\Client('mongodb://mongodb/'))->psc->Plugin;
            $plugins = $collection->find(array('installed' => true));
        /** @var Plugin $plugin */
            foreach ($plugins as $plugin) {
                if (file_exists($this->rootDir . '/var/plugins/' . $plugin['path'] . '/Resources/config/routing.yml')) {
                    $importedRoutes = $this->import($this->rootDir . '/var/plugins/' . $plugin['path'] . '/Resources/config/routing.yml', 'yaml');
                    $routes->addCollection($importedRoutes);
                }
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die();
            return $routes;
        }

        return $routes;
    }

    /**
     * Returns whether this class supports the given resource.
     *
     * @param mixed       $resource A resource
     * @param string|null $type     The resource type or null if unknown
     *
     * @return bool True if this class supports the given resource, false otherwise
     */
    public function supports($resource, $type = null)
    {
        return ($type == 'plugin');
    }
}
