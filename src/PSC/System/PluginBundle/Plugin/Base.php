<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\System\PluginBundle\Plugin;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Bundle\BundleInterface;

class Base implements BundleInterface
{
    protected $path;
    protected $namespace ;
    protected $name;
    protected $defaultInstall = false;

    public function getTitle()
    {
        return $this->name;
    }

    public function getName()
    {
        return str_replace('\\', '', $this->getNamespace());
    }

    public function getIdentifier()
    {
        return md5($this->getTitle());
    }

    public function getParent()
    {
        return null;
    }

    public function getPath()
    {
        if (null === $this->path) {
            $reflected = new \ReflectionObject($this);
            $this->path = dirname($reflected->getFileName());
        }

        return $this->path;
    }

    public function boot()
    {
    }

    /**
     * Gets the Bundle namespace.
     *
     * @return string The Bundle namespace
     */
    public function getNamespace()
    {
        if (null === $this->namespace) {
            $this->parseClassName();
        }

        return $this->namespace;
    }

    private function parseClassName()
    {
        $pos = strrpos(static::class, '\\');
        $this->namespace = false === $pos ? '' : substr(static::class, 0, $pos);
        if (null === $this->name) {
            $this->name = false === $pos ? static::class : substr(static::class, $pos + 1);
        }
    }

    public function getContainerExtension()
    {
        return null;
    }

    public function build(ContainerBuilder $container)
    {
    }

    /**
     * Shutdowns the Bundle.
     */
    public function shutdown()
    {
        // TODO: Implement shutdown() method.
    }

    public function setContainer(ContainerInterface $container = null)
    {
        // TODO: Implement setContainer() method.
    }

    /**
     * @return bool
     */
    public function isDefaultInstall()
    {
        return $this->defaultInstall;
    }
}
