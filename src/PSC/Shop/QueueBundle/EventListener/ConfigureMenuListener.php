<?php

namespace PSC\Shop\QueueBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use PSC\Shop\QueueBundle\Service\Queue\Manager;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
/**
     * @var Manager
     */
    private Manager $queueManager;
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, Manager $queueManager)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->queueManager = $queueManager;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $count = $this->queueManager->getErrorJobCount();
            if ($count > 0) {
                $submenu = $menu->addChild('Aktionen', array(
                    'route' => 'psc_shop_queue_backend_list_index',
                    'extras' => array(
                        'icon' => 'fas fa-tasks',
                        'orderNumber' => 13,
                        'error' => $count
                    )
                ));
            } else {
                $submenu = $menu->addChild('Aktionen', array(
                    'route' => 'psc_shop_queue_backend_list_index',
                    'extras' => array(
                        'icon' => 'fas fa-tasks',
                        'orderNumber' => 13,
                        'error' => $count
                    )
                ));
            }
        }
    }
}
