<?php

namespace PSC\Shop\QueueBundle\Teams\Card;

use Sebbmyr\Teams\AbstractCard as Card;

/**
 * Forge card for microsoft teams
 */
class Error extends Card
{
    public function getMessage()
    {
        return [
            "@type" => "MessageCard",
            "@context" => "http://schema.org/extensions",
            "summary" => "Shop Fehler",
            "themeColor" => "FF0000",
            "title" => "Error in Shop " . $this->data['shop']->getTitle() . " of " . $this->data['shop']->getBetreiberName(),
            "sections" => [
                [
                    "activityTitle" => "",
                    "activitySubtitle" => "",
                    "activityImage" => "",
                    "facts" => [
                        [
                            "name" => "Shop:",
                            "value" => $this->data['shop']->getTitle()
                        ],
                        [
                            "name" => "Betreiber:",
                            "value" => $this->data['shop']->getBetreiberName()
                        ],
                        [
                            "name" => "Error:",
                            "value" => $this->data['error']
                        ],
                    ],
                    "text" => "Something went wrong :/"
                ]
            ]
        ];
    }
}
