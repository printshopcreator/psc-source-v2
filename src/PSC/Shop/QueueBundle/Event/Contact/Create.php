<?php

namespace PSC\Shop\QueueBundle\Event\Contact;

use PSC\Shop\QueueBundle\Event\Event;
use PSC\Shop\QueueBundle\Help\Shop;

class Create extends Event
{
    /** @var string */
    protected $contact;
    public function getType()
    {
        return 'contact_create';
    }

    public function getDescription()
    {
        return 'Kunde wurde erstellt';
    }

    public function getData()
    {
        return array(
            'contact' => $this->contact
        );
    }

    public function setData($data)
    {
        $this->contact = $data['contact'];
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    public function getHelp()
    {
        return [[
            Shop::getColumn(),
            \PSC\Shop\QueueBundle\Help\Contact::getColumn(),
        ]];
    }
}
