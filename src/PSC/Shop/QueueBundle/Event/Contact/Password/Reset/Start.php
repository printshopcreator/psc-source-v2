<?php

namespace PSC\Shop\QueueBundle\Event\Contact\Password\Reset;

use PSC\Shop\QueueBundle\Event\Event;

class Start extends Event
{
    /** @var string */
    protected $contact;
    public function getType()
    {
        return 'contact_password_reset_start';
    }

    public function getDescription()
    {
        return 'Password zurücksetzen wurde gestartet';
    }

    public function getData()
    {
        return array(
            'contact' => $this->contact
        );
    }

    public function setData($data)
    {
        $this->contact = $data['contact'];
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }
}
