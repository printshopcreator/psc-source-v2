<?php

namespace PSC\Shop\QueueBundle\Event\Product;

use PSC\Shop\QueueBundle\Event\Event;

class StockMin extends Event
{
    /** @var string */
    protected $product;
    public function getType()
    {
        return 'product_stock_min';
    }

    public function getDescription()
    {
        return 'Lagerbestand unterschritten';
    }

    public function getData()
    {
        return array(
            'product' => $this->product
        );
    }

    public function setData($data)
    {
        $this->product = $data['product'];
    }

    /**
     * @return string
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param string $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
}
