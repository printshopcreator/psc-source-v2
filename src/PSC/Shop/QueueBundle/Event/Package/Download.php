<?php

namespace PSC\Shop\QueueBundle\Event\Package;

use PSC\Shop\QueueBundle\Event\Event;

class Download extends Event
{
    /** @var string */
    protected $order;
    public function getType()
    {
        return 'package_download';
    }

    public function getDescription()
    {
        return 'Paket herunterladen';
    }

    public function getData()
    {
        return array(
            'order' => $this->order
        );
    }

    public function setData($data)
    {
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
}
