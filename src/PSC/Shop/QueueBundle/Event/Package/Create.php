<?php

namespace PSC\Shop\QueueBundle\Event\Package;

use PSC\Shop\QueueBundle\Event\Event;

class Create extends Event
{
    /** @var string */
    protected $order;
    public function getType()
    {
        return 'package_create';
    }

    public function getDescription()
    {
        return 'Paket soll erzeugt werden';
    }

    public function getData()
    {
        return array(
            'order' => $this->order,
        );
    }

    public function setData($data)
    {
        $this->order = $data['order'];
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
}
