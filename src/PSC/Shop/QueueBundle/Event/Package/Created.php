<?php

namespace PSC\Shop\QueueBundle\Event\Package;

use PSC\Shop\QueueBundle\Event\Event;

class Created extends Event
{
    /** @var string */
    protected $order;
/** @var string */
    protected $path;
    public function getType()
    {
        return 'package_created';
    }

    public function getDescription()
    {
        return 'Paket wurde erzeugt';
    }

    public function getData()
    {
        return array(
            'order' => $this->order,
            'path' => $this->path
        );
    }

    public function setData($data)
    {
        $this->order = $data['order'];
        $this->path = $data['path'];
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}
