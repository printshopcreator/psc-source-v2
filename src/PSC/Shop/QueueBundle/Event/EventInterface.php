<?php

namespace PSC\Shop\QueueBundle\Event;

use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Shop\EntityBundle\Document\Queue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

interface EventInterface
{
    public function getType();
    public function getData();
    public function setData($data);
    public function injectCreateDocument(Form $form, Queue $docQueue);
    public function isOverwriteQueueForm();
    public function isOverwriteQueueTemplate();
    public function isOverwriteQueueHandling();
    public function getQueueForm(FormBuilderInterface $builder, $form_options, EventInterface $event);
    public function getQueueTemplate();
    public function injectQueueDocument(Form $form, EventInterface $event, Queue $objQueue);
    public function setFormQueueData(Form $form, EventInterface $event, Queue $queueObj);
    public function executeQueue(EventInterface $event, Queue $queue);
}
