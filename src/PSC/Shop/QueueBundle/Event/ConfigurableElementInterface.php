<?php

namespace PSC\Shop\QueueBundle\Event;

use PSC\Shop\EntityBundle\Document\Queue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

interface ConfigurableElementInterface
{
    /**
     * @param Form $form
     * @return mixed
     */
    public function setFormData(Form $form, Queue $eventObj);
/**
     * @param FormBuilderInterface $builder
     * @param $form_options
     * @return FormBuilderInterface
     */
    public function getForm(FormBuilderInterface $builder, $form_options);
}
