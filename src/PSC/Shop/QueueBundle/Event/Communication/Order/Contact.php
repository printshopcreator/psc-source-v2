<?php

namespace PSC\Shop\QueueBundle\Event\Communication\Order;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Document\Order;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\QueueBundle\Event\Event;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Help\Shop;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class Contact extends Event
{
    protected $overwriteQueueForm = true;
    protected $overwriteQueueTemplate = true;
    protected $overwriteQueueHandling = true;
/** @var string */
    protected $order;
/** @var string */
    protected $subject;
/** @var string */
    protected $text;
/**
     * @var Environment
     */
    private Environment $twig;
/**
     * @var DocumentManager
     */
    private DocumentManager $mongoDB;
/**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
/**
     * @var MailerInterface
     */
    private MailerInterface $mailer;
    public function __construct(DocumentManager $documentManager, EntityManagerInterface $entityManager, Environment $twig, MailerInterface $mailer)
    {
        $this->twig = $twig;
        $this->mongoDB = $documentManager;
        $this->entityManager = $entityManager;
        $this->mailer = $mailer;
    }

    public function getType()
    {
        return 'communication_order_contact';
    }

    public function getDescription()
    {
        return 'Nachricht an Kunde aus Auftrag';
    }

    public function getData()
    {
        return array(
            'order' => $this->order,
            'subject' => $this->subject,
            'text' => $this->text
        );
    }

    public function setData($data)
    {
        $this->order = $data['order'];
        $this->subject = $data['subject'];
        $this->text = $data['text'];
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    public function getHelp()
    {
        return [[
            Shop::getColumn(),
            \PSC\Shop\QueueBundle\Help\Contact::getColumn(),
        ]];
    }

    public function getQueueTemplate()
    {
        return '@PSCShopQueue/event/communication/order/contact.html.twig';
    }

    public function injectQueueDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        $mail = new \PSC\Shop\QueueBundle\Document\Queue\Mail();
        $mail->setFrom($form->get('from')->getData());
        $mail->setTo($form->get('to')->getData());
        $objQueue->setQueueDocument($mail);
    }

    public function setFormQueueData(Form $form, EventInterface $event, Queue $queueObj)
    {
        $form->get('from')->setData($queueObj->getQueueDocument()->getFrom());
        $form->get('to')->setData($queueObj->getQueueDocument()->getTo());
    }

    public function executeQueue(EventInterface $event, Queue $queue)
    {
        $mailDoc = $queue->getQueueDocument();
/** @var Contact $eventDoc */
        $eventDoc = $queue->getEventDocument();
        $orderRepo = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');
/** @var \PSC\Shop\EntityBundle\Entity\Order $order */
        $order = $orderRepo->findOneBy(array('uuid' => $event->getOrder()));
        $contact = $order->getContact();
        $invoiceAddress = $order->getInvoiceAddress();
        $deliveryAddress = $order->getDeliveryAddress();
        $senderAddress = $order->getSenderAddress();
        $shop = $order->getShop();
        $shipping = $order->getShippingType();
        $payment = $order->getPaymentType();
/** @var Order $objDoc */
        $objDoc = $this->mongoDB
            ->getRepository('PSCEntityBundle:Order')
            ->findOneBy(['uid' => (string)$order->getUid()]);
        $params = array(
            'order' => $order,
            'contact' => $contact,
            'invoiceAddress' => $invoiceAddress,
            'deliveryAddress' => $deliveryAddress,
            'senderAddress' => $senderAddress,
            'shop' => $shop,
            'shipping' => $shipping,
            'payment' => $payment,
            'doc' => $objDoc
        );
        try {
            $twigFrom = $this->twig->createTemplate($mailDoc->getFrom());
            $twigTo = $this->twig->createTemplate($mailDoc->getTo());
            $message = (new Email())
                ->subject($event->getSubject())
                ->from($twigFrom->render($params))
                ->to($twigTo->render($params))
                ->text($event->getText(), 'text/html')
            ;
            $this->mailer->send($message);
        } catch (\Exception $e) {
            $this->_error = $e->getMessage();
            return false;
        }

        return true;
    }
}
