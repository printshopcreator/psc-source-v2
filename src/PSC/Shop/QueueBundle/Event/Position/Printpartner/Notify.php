<?php

namespace PSC\Shop\QueueBundle\Event\Position\Printpartner;

use PSC\Shop\QueueBundle\Event\Event;
use PSC\Shop\QueueBundle\Help\Contact;
use PSC\Shop\QueueBundle\Help\Position;
use PSC\Shop\QueueBundle\Help\Shop;

class Notify extends Event
{
    /** @var string */
    protected $position;
/** @var string */
    protected $email;
    public function getType()
    {
        return 'position_printpartner_notify';
    }

    public function getDescription()
    {
        return 'Printpartner Notify';
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getData()
    {
        return array(
            'position' => $this->position,
            'email' => $this->email
        );
    }

    public function setData($data)
    {
        $this->position = $data['position'];
        $this->email = $data['email'];
    }

    public function getHelp()
    {
        return [
            [
                [
                    'label' => 'Allgemein',
                    'helpText' => '',
                    'data' => [
                        [
                            'value' => '{{ printPartnerEmail }}',
                            'label' => 'E-Mail aus dem Formular / Produkt'
                        ],
                        [
                            'value' => '{{ packageLink }}',
                            'label' => 'Link zum Paket ohne Domain'
                        ]
                    ]
                ],
                Position::getColumn(),
                Shop::getColumn(),
                Contact::getColumn()
            ]
        ];
    }
}
