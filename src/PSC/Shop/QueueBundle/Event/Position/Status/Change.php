<?php

namespace PSC\Shop\QueueBundle\Event\Position\Status;

use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\QueueBundle\Document\Event\Status;
use PSC\Shop\QueueBundle\Event\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Event\Event;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

class Change extends Event implements ConfigurableElementInterface
{
    /** @var string */
    protected $position;
/** @var integer */
    protected $status;
    private \PSC\System\SettingsBundle\Service\Status $statusService;
    public function getType()
    {
        return 'position_status_change';
    }

    public function getDescription()
    {
        return 'Position Status geändert';
    }

    public function setStatusService(\PSC\System\SettingsBundle\Service\Status $statusService)
    {
        $this->statusService = $statusService;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function getForm(FormBuilderInterface $builder, $form_options)
    {
        $builder->add("status", ChoiceType::class, array(
            'label' => 'Status',
            'choices'  => $this->statusService->getPositionStatusAsArray(),
            'translation_domain' => 'posstatus'
        ));
    }

    public function setFormData(Form $form, Queue $queue)
    {
        if ($queue->getEventDocument() instanceof Status) {
            $form->get('status')->setData($queue->getEventDocument()->getStatus());
        }
    }

    public function injectUpdateDocument(Form $form, Queue $docQueue)
    {
        $objDoc = new Status();
        $objDoc->setStatus($form->get('status')->getData());
        $docQueue->setEventDocument($objDoc);
    }

    public function doProcess(Queue $queue)
    {
        /** @var \PSC\Shop\QueueBundle\Document\Event\Status $eventDoc */
        $eventDoc = $queue->getEventDocument();
        if ($eventDoc->getStatus()  == $this->getStatus()) {
            return true;
        }
        return false;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    public function getData()
    {
        return array(
            'position' => $this->position,
            'status' => $this->status
        );
    }

    public function setData($data)
    {
        $this->position = $data['position'];
        $this->status = $data['status'];
    }

    public function getTemplate()
    {
        return '@PSCShopQueue/event/position/status/change.html.twig';
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
