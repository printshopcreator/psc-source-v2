<?php

namespace PSC\Shop\QueueBundle\Event\Position\Contact\Approval;

use PSC\Shop\QueueBundle\Event\Event;
use PSC\Shop\QueueBundle\Help\Contact;
use PSC\Shop\QueueBundle\Help\Position;
use PSC\Shop\QueueBundle\Help\Shop;

class Request extends Event
{
    /** @var string */
    protected $position;
/** @var string */
    protected $order;
/** @var string */
    protected $contact;
    public function getType()
    {
        return 'position_contact_approval_request';
    }

    public function getDescription()
    {
        return 'Position Freigabe erforderlich';
    }

    public function getData()
    {
        return array(
            'position' => $this->position,
            'order' => $this->order,
            'contact' => $this->contact
        );
    }


    public function setData($data)
    {
        $this->contact = $data['contact'];
        $this->position = $data['position'];
        $this->order = $data['order'];
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition(string $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder(string $order): void
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getContact(): string
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact(string $contact): void
    {
        $this->contact = $contact;
    }
}
