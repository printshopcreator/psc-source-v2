<?php

namespace PSC\Shop\QueueBundle\Event;

use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Shop\EntityBundle\Document\Queue;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Response;

abstract class Event implements EventInterface, RegistryElementInterface
{
    /** @var bool  */
    protected $overwriteQueueForm = false;
/** @var bool  */
    protected $overwriteQueueTemplate = false;
/** @var bool  */
    protected $overwriteQueueHandling = false;
/** @var string */
    protected $shop;
/**
     * @return string
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param string $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    public function injectCreateDocument(Form $form, Queue $docQueue)
    {
        $event = new \PSC\Shop\QueueBundle\Document\Event\Event();
        $event->setType($this->getType());
        $docQueue->setEventDocument($event);
    }

    public function getTemplate()
    {
        return false;
    }

    public function doProcess(Queue $queue)
    {
        return true;
    }

    public function selfExecute(Queue $docQueue)
    {
        return true;
    }

    public function getHelp()
    {
        return [];
    }

    public function isOverwriteQueueForm()
    {
        return $this->overwriteQueueForm;
    }

    public function isOverwriteQueueTemplate()
    {
        return $this->overwriteQueueTemplate;
    }

    public function isOverwriteQueueHandling()
    {
        return $this->overwriteQueueHandling;
    }

    public function getQueueForm(FormBuilderInterface $builder, $form_options, EventInterface $event)
    {
        $builder->add("from", TextType::class, array('label' => 'Von', 'attr' => array('class' => 'form-element')));
        $builder->add("to", TextType::class, array('label' => 'An', 'attr' => array('class' => 'form-element')));
    }

    public function getQueueTemplate()
    {
        return;
    }

    public function doQueueHandling()
    {
        return;
    }

    public function injectQueueDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        return;
    }

    public function setFormQueueData(Form $form, EventInterface $event, Queue $queueObj)
    {
        return;
    }

    public function executeQueue(EventInterface $event, Queue $queue)
    {
        return;
    }
}
