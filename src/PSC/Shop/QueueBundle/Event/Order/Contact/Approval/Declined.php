<?php

namespace PSC\Shop\QueueBundle\Event\Order\Contact\Approval;

use PSC\Shop\QueueBundle\Event\Event;
use PSC\Shop\QueueBundle\Help\Contact;
use PSC\Shop\QueueBundle\Help\Position;
use PSC\Shop\QueueBundle\Help\Shop;

class Declined extends Event
{
    /** @var string */
    protected $order;
/** @var string */
    protected $contact;
/** @var string */
    protected $message;
    public function getType()
    {
        return 'order_contact_approval_declined';
    }

    public function getDescription()
    {
        return 'Auftrag Freigabe abgelehnt';
    }

    public function getData()
    {
        return array(
            'message' => $this->message,
            'order' => $this->order,
            'contact' => $this->contact
        );
    }

    public function setData($data)
    {
        $this->contact = $data['contact'];
        $this->message = $data['message'];
        $this->order = $data['order'];
    }

    /**
     * @return string
     */
    public function getOrder(): string
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder(string $order): void
    {
        $this->order = $order;
    }

    /**
     * @return string
     */
    public function getContact(): string
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact(string $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }
}
