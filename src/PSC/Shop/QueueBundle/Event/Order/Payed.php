<?php

namespace PSC\Shop\QueueBundle\Event\Order;

use PSC\Shop\QueueBundle\Event\Event;

class Payed extends Event
{
    /** @var string */
    protected $order;
    public function getType()
    {
        return 'order_payed';
    }

    public function getDescription()
    {
        return 'Auftrag wurde bezahlt';
    }

    public function getData()
    {
        return array(
            'order' => $this->order
        );
    }

    public function setData($data)
    {
        $this->order = $data['order'];
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
}
