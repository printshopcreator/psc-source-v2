<?php

namespace PSC\Shop\QueueBundle\Event\Order;

use PSC\Shop\QueueBundle\Event\Event;

class Exported extends Event
{
    /** @var string */
    protected $order;
    public function getType()
    {
        return 'order_exported';
    }

    public function getDescription()
    {
        return 'Auftrag wurde exportiert';
    }

    public function getData()
    {
        return array(
            'order' => $this->order
        );
    }

    public function setData($data)
    {
        $this->order = $data['order'];
    }

    /**
     * @return string
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param string $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }
}
