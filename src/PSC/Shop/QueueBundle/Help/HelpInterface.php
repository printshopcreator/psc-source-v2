<?php

/**
 * Created by PhpStorm.
 * User: thomas
 * Date: 06.04.18
 * Time: 15:43
 */

namespace PSC\Shop\QueueBundle\Help;

interface HelpInterface
{
    /**
     * @param $prefix
     * @return array
     */
    public static function getColumn($prefix, $helpText);
}
