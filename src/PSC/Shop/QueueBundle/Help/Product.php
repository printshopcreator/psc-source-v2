<?php

namespace PSC\Shop\QueueBundle\Help;

class Product implements HelpInterface
{
    /**
     * @param string $prefix
     * @param string $helpText
     * @return array
     */
    public static function getColumn($prefix = 'product.', $helpText = '')
    {
        return [
            'label' => 'Produkt',
            'helpText' => $helpText,
            'data' => [
                [
                    'value' => '{{ ' . $prefix . 'title }}',
                    'label' => 'E-Mail aus den Shopeinstellungen'
                ]
            ]
        ];
    }
}
