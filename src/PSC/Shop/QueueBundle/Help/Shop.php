<?php

namespace PSC\Shop\QueueBundle\Help;

class Shop implements HelpInterface
{
    /**
     * @param string $prefix
     * @param string $helpText
     * @return array
     */
    public static function getColumn($prefix = 'shop.', $helpText = '')
    {
        return [
            'label' => 'Shop',
            'helpText' => $helpText,
            'data' => [
                [
                    'value' => '{{ ' . $prefix . 'betreiberEmail }}',
                    'label' => 'E-Mail aus den Shopeinstellungen'
                ],
                [
                    'value' => '{{ ' . $prefix . 'betreiberName }}',
                    'label' => 'Name aus den Shopeinstellungen'
                ],
            ]
        ];
    }
}
