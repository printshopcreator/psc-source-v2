<?php

namespace PSC\Shop\QueueBundle\Help;

class Payment implements HelpInterface
{
    /**
     * @param string $prefix
     * @param string $helpText
     * @return array
     */
    public static function getColumn($prefix = 'payment.', $helpText = '')
    {
        return [
            'label' => 'Zahlart',
            'helpText' => $helpText,
            'data' => [
                [
                    'value' => '{{ ' . $prefix . 'uid }}',
                    'label' => 'UId'
                ],
                [
                    'value' => '{{ ' . $prefix . 'title }}',
                    'label' => 'Titel'
                ],
                [
                    'value' => '{{ ' . $prefix . 'description }}',
                    'label' => 'Beschreibung'
                ],
                [
                    'value' => '{{ ' . $prefix . 'price }}',
                    'label' => 'Kosten'
                ],
                [
                    'value' => '{{ ' . $prefix . 'pricePercent }}',
                    'label' => 'Kosten Prozentual'
                ],
                [
                    'value' => '{{ ' . $prefix . 'parameter1 }}',
                    'label' => 'Parameter 1'
                ]
            ]
        ];
    }
}
