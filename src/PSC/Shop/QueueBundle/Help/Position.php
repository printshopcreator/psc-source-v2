<?php

namespace PSC\Shop\QueueBundle\Help;

class Position implements HelpInterface
{
    /**
     * @param string $prefix
     * @param string $helpText
     * @return array
     */
    public static function getColumn($prefix = 'position.', $helpText = 'Muss in einer for Schleife verwendet werden {% for position in positions %}')
    {
        return [
            'label' => 'Position',
            'helpText' => $helpText,
            'data' => [
                [
                    'value' => '{{ ' . $prefix . 'obj.priceAllNetto|number_format(2, \',\', \'.\') }}',
                    'label' => 'Preis Netto'
                ],
                [
                    'value' => '{{ ' . $prefix . 'obj.priceAllSteuer|number_format(2, \',\', \'.\') }}',
                    'label' => 'Preis MwSt'
                ],
                [
                    'value' => '{{ ' . $prefix . 'obj.priceAllBrutto|number_format(2, \',\', \'.\') }}',
                    'label' => 'Preis Brutto'
                ],
                [
                    'value' => '{{ ' . $prefix . 'product.title }}',
                    'label' => 'Produktname'
                ],
                [
                    'value' => '{{ ' . $prefix . 'obj.basketField1 }}',
                    'label' => 'Positionsfeld 1'
                ],
                [
                    'value' => '{{ ' . $prefix . 'obj.basketField2 }}',
                    'label' => 'Positionsfeld 2'
                ],
                [
                    'value' => '{{ ' . $prefix . 'calc.getOptionById(\'auflage\').value }}',
                    'label' => 'Wert Auflage aus der Kalkulation'
                ],
            ]
        ];
    }
}
