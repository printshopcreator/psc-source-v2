<?php

namespace PSC\Shop\QueueBundle\Help;

class Address implements HelpInterface
{
    /**
     * @param string $prefix
     * @param string $helpText
     * @return array
     */
    public static function getColumn($prefix = 'invoiceAddress.', $helpText = '')
    {
        return [
            'label' => 'Kunde',
            'helpText' => $helpText,
            'data' => [
                [
                    'value' => '{{ ' . $prefix . 'email }}',
                    'label' => 'E-Mail'
                ],
                [
                    'value' => '{{ ' . $prefix . 'company }}',
                    'label' => 'Firma'
                ],
                [
                    'value' => '{{ ' . $prefix . 'firstname }}',
                    'label' => 'Vorname'
                ],
                [
                    'value' => '{{ ' . $prefix . 'lastname }}',
                    'label' => 'Nachname'
                ],
                [
                    'value' => '{{ ' . $prefix . 'street }}',
                    'label' => 'Straße'
                ],
                [
                    'value' => '{{ ' . $prefix . 'houseNumber }}',
                    'label' => 'Hausnummer'
                ],
                [
                    'value' => '{{ ' . $prefix . 'zip }}',
                    'label' => 'PLZ'
                ],
                [
                    'value' => '{{ ' . $prefix . 'city }}',
                    'label' => 'Ort'
                ]
            ]
        ];
    }
}
