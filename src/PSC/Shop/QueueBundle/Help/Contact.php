<?php

namespace PSC\Shop\QueueBundle\Help;

class Contact implements HelpInterface
{
    /**
     * @param string $prefix
     * @param string $helpText
     * @return array
     */
    public static function getColumn($prefix = 'contact.', $helpText = '')
    {
        return [
            'label' => 'Kunde',
            'helpText' => $helpText,
            'data' => [
                [
                    'value' => '{{ ' . $prefix . 'email }}',
                    'label' => 'E-Mail'
                ]
            ]
        ];
    }
}
