<?php

namespace PSC\Shop\QueueBundle\Type;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\System\PluginBundle\Event\EveryRun;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class Url implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'url';
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return 'Allgemeines';
    }

    public function getDescription()
    {
        return 'Aufruf einer Middleware Url';
    }

    public function getName()
    {
        return 'Url';
    }

    public function getForm(FormBuilderInterface $builder, $form_options, EventInterface $event)
    {
        $builder->add("url", TextType::class, array('label' => 'Url', 'attr' => array('class' => 'form-element')));
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        $url = new \PSC\Shop\QueueBundle\Document\Queue\Url();
        $url->setUrl($form->get('url')->getData());
        $objQueue->setQueueDocument($url);
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
        $form->get('url')->setData($queueObj->getQueueDocument()->getUrl());
    }

    public function getTemplate()
    {
        return '@PSCShopQueue/queue/url.html.twig';
    }

    /**
     * @param EventInterface $event
     * @param Queue $urlDoc
     */
    public function execute(EventInterface $event, Queue $urlDoc)
    {

        /** @var \PSC\Shop\QueueBundle\Document\Queue\Url $doc */
        $doc = $urlDoc->getQueueDocument();
        try {
            if ($event instanceof Notify) {
                $client = new Client();
                $res = $client->request('GET', $doc->getUrl(), [
                    'query' => ['uuid' => $event->getPosition()]
                ]);
            }
            if ($event instanceof EveryRun) {
                $client = new Client();
                $res = $client->request('GET', $doc->getUrl());
            }
        } catch (\Exception $e) {
            $this->_error = $e->getMessage();
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->_error;
    }
}
