<?php

namespace PSC\Shop\QueueBundle\Type;

use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Upload;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Order\Create;
use PSC\Shop\QueueBundle\Event\Package\Created;
use PSC\Shop\QueueBundle\Event\Package\Download;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Help\Shop;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use PSC\System\SettingsBundle\Service\Printing;
use PSC\System\SettingsBundle\Service\TemplateVars;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Twig\Environment;
use ZipArchive;

class Package implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_packageService = null;
    private $_templateVars = null;
    private $_error = null;
    private $_twig = null;
    private $_eventManager = null;
    private \PSC\Shop\OrderBundle\Service\Order $_orderService;
    function __construct(Environment $twig, Manager $eventManager, \PSC\System\SettingsBundle\Service\Package $packageService, TemplateVars $templateVars, FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, \PSC\Shop\OrderBundle\Service\Order $orderService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_packageService = $packageService;
        $this->_templateVars = $templateVars;
        $this->_twig = $twig;
        $this->_eventManager = $eventManager;
        $this->_orderService = $orderService;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'package';
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return 'Allgemeines';
    }

    public function getDescription()
    {
        return 'Generiert einer Zip';
    }

    public function getName()
    {
        return 'Package';
    }

    public function getForm(FormBuilderInterface $builder, $new, EventInterface $event)
    {

        $builder->add('mode', ChoiceType::class, array(
            'label' => 'Modus',
            'choices' => ['Auftragszip mit Positionszips' => 0, 'Auftragszip mit Positionsordnern' => 1, 'Auftragszip ohne Positionen' => 2, 'Nur Positionszips' => 3, 'Auftragszip mit enthaltenen Druckdaten' => 4]
        ));
        $builder->add('orderJobticket', CheckboxType::class, ['required' => false, 'label' => 'Jobticket']);
        $builder->add('orderInvoice', CheckboxType::class, ['required' => false, 'label' => 'Rechnung']);
        $builder->add('orderDelivery', CheckboxType::class, ['required' => false, 'label' => 'Lieferschein']);
        $builder->add('orderOrder', CheckboxType::class, ['required' => false, 'label' => 'Auftrag']);
        $builder->add('orderStorno', CheckboxType::class, ['required' => false, 'label' => 'Storno']);
        $builder->add('orderLabel', CheckboxType::class, ['required' => false, 'label' => 'Label']);
        $builder->add('orderOffer', CheckboxType::class, ['required' => false, 'label' => 'Angebot']);
        $builder->add('orderJobticketPP', CheckboxType::class, ['required' => false, 'label' => 'Jobticket Printpartner']);
        $builder->add('orderFiles', CollectionType::class, array(
            'entry_type' => FileType::class,
            'entry_options' => array('label' => false),
            'allow_add' => true,
            'allow_delete' => true,
            'label' => false
        ));
        if ($new) {
            $builder->add("orderPath", TextType::class, array('data' => '/data/www/old/data/packages/uploads/{{ order.alias }}.zip', 'label' => 'Pfad', 'attr' => array('class' => 'form-element')));
        } else {
            $builder->add("orderPath", TextType::class, array('label' => 'Pfad', 'attr' => array('class' => 'form-element')));
        }

        $builder->add('posJobticket', CheckboxType::class, ['required' => false, 'label' => 'Jobticket']);
        $builder->add('posInvoice', CheckboxType::class, ['required' => false, 'label' => 'Rechnung']);
        $builder->add('posDelivery', CheckboxType::class, ['required' => false, 'label' => 'Lieferschein']);
        $builder->add('posOrder', CheckboxType::class, ['required' => false, 'label' => 'Auftrag']);
        $builder->add('posStorno', CheckboxType::class, ['required' => false, 'label' => 'Storno']);
        $builder->add('posLabel', CheckboxType::class, ['required' => false, 'label' => 'Label']);
        $builder->add('posOffer', CheckboxType::class, ['required' => false, 'label' => 'Angebot']);
        $builder->add('posJobticketPP', CheckboxType::class, ['required' => false, 'label' => 'Jobticket Printpartner']);
        $builder->add('posFiles', CollectionType::class, array(
            'entry_type' => FileType::class,
            'entry_options' => array('label' => false),
            'allow_add' => true,
            'allow_delete' => true,
            'label' => false
        ));
        if ($new) {
            $builder->add("posPath", TextType::class, array('data' => '/data/www/old/data/packages/uploads/{{ order.alias }}_{{ position.pos }}.zip', 'label' => 'Pfad', 'attr' => array('class' => 'form-element')));
            $builder->add("designerPath", TextType::class, array('data' => 'final.pdf', 'label' => 'Layouter Dateiname', 'attr' => array('class' => 'form-element')));
        } else {
            $builder->add("posPath", TextType::class, array('label' => 'Pfad', 'attr' => array('class' => 'form-element')));
            $builder->add("designerPath", TextType::class, array('label' => 'Layouter Dateiname', 'attr' => array('class' => 'form-element')));
        }
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        $package = new \PSC\Shop\QueueBundle\Document\Queue\Package();
        $package->setMode($form->get('mode')->getData());
        $package->setOrderPath($form->get('orderPath')->getData());
        $package->setPosPath($form->get('posPath')->getData());
        $package->setOrderFiles($form->get('orderFiles')->getData());
        $package->setPosFiles($form->get('posFiles')->getData());
        $package->setOrderJobticket($form->get('orderJobticket')->getData());
        $package->setOrderDelivery($form->get('orderDelivery')->getData());
        $package->setOrderInvoice($form->get('orderInvoice')->getData());
        $package->setOrderOffer($form->get('orderOffer')->getData());
        $package->setOrderStorno($form->get('orderStorno')->getData());
        $package->setOrderOrder($form->get('orderOrder')->getData());
        $package->setOrderJobticketPP($form->get('orderJobticketPP')->getData());
        $package->setOrderLabel($form->get('orderLabel')->getData());
        $package->setPosJobticket($form->get('posJobticket')->getData());
        $package->setPosDelivery($form->get('posDelivery')->getData());
        $package->setPosInvoice($form->get('posInvoice')->getData());
        $package->setPosOffer($form->get('posOffer')->getData());
        $package->setPosStorno($form->get('posStorno')->getData());
        $package->setPosOrder($form->get('posOrder')->getData());
        $package->setPosJobticketPP($form->get('posJobticketPP')->getData());
        $package->setPosLabel($form->get('posLabel')->getData());
        $package->setDesignerPath($form->get('designerPath')->getData());
        $objQueue->setQueueDocument($package);
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
        /** @var \PSC\Shop\QueueBundle\Document\Queue\Package $doc */
        $doc = $queueObj->getQueueDocument();
        $form->get('mode')->setData($doc->getMode());
        $form->get('orderPath')->setData($doc->getOrderPath());
        $form->get('posPath')->setData($doc->getPosPath());
        $form->get('orderFiles')->setData($doc->getOrderFiles());
        $form->get('posFiles')->setData($doc->getPosFiles());
        $form->get('posJobticket')->setData($doc->isPosJobticket());
        $form->get('posDelivery')->setData($doc->isPosDelivery());
        $form->get('posInvoice')->setData($doc->isPosInvoice());
        $form->get('posOffer')->setData($doc->isPosOffer());
        $form->get('posStorno')->setData($doc->isPosStorno());
        $form->get('posOrder')->setData($doc->isPosOrder());
        $form->get('posJobticketPP')->setData($doc->isPosJobticketPP());
        $form->get('posLabel')->setData($doc->isPosLabel());
        $form->get('orderJobticket')->setData($doc->isOrderJobticket());
        $form->get('orderDelivery')->setData($doc->isOrderDelivery());
        $form->get('orderInvoice')->setData($doc->isOrderInvoice());
        $form->get('orderOffer')->setData($doc->isOrderOffer());
        $form->get('orderStorno')->setData($doc->isOrderStorno());
        $form->get('orderOrder')->setData($doc->isOrderOrder());
        $form->get('orderJobticketPP')->setData($doc->isOrderJobticketPP());
        $form->get('orderLabel')->setData($doc->isOrderLabel());
        $form->get('designerPath')->setData($doc->getDesignerPath());
    }

    public function getTemplate()
    {
        return '@PSCShopQueue/queue/package.html.twig';
    }

    /**
     * @param EventInterface $event
     * @param Queue $packageDoc
     * @return bool
     * @throws \Throwable
     */
    public function execute(EventInterface $event, Queue $packageDoc)
    {

        /** @var \PSC\System\SettingsBundle\Service\Package $packageService */
        $packageService = $this->_packageService;
/** @var \PSC\System\SettingsBundle\Service\TemplateVars $templateVars */
        $templateVars = $this->_templateVars;
/** @var \PSC\Shop\OrderBundle\Model\Order $orderObj */
        $orderObj = $this->_orderService->getOrderByUuid($event->getOrder());
        $this->_packageService->setOrderObj($orderObj);
        if ($event instanceof Download) {
        /** @var \PSC\Shop\QueueBundle\Document\Queue\Package $queueSettings */
            $queueSettings = $packageDoc->getQueueDocument();
            $templateVars->loadOrder($event->getOrder());
            if ($queueSettings->getMode() == 0) {
                $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                $orderPath = $orderPath->render($templateVars->getTwigVars());
                $packageService->createPackage(sys_get_temp_dir() . "/" . $orderPath);
                $packageService->setOrder($templateVars->getOrder());
                if ($queueSettings->isOrderInvoice()) {
                    $packageService->addOrderDocument(Printing::INVOICE);
                }
                if ($queueSettings->isOrderJobticket()) {
                    $packageService->addOrderDocument(Printing::JOBTICKET);
                }
                if ($queueSettings->isOrderJobticketPP()) {
                    $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                }
                if ($queueSettings->isOrderOffer()) {
                    $packageService->addOrderDocument(Printing::OFFER);
                }
                if ($queueSettings->isOrderLabel()) {
                    $packageService->addOrderDocument(Printing::LABEL);
                }
                if ($queueSettings->isOrderOrder()) {
                    $packageService->addOrderDocument(Printing::ORDER);
                }
                if ($queueSettings->isOrderDelivery()) {
                    $packageService->addOrderDocument(Printing::DELIVERY);
                }
                if ($queueSettings->isOrderStorno()) {
                    $packageService->addOrderDocument(Printing::STORNO);
                }
                $packageService->addPluginFilesToOrderPackage();
                foreach ($queueSettings->getOrderFiles() as $file) {
                    if ($file['name'] != "" && $file['content'] != "") {
                        $name = $this->_twig->createTemplate($file['name']);
                        $content = $this->_twig->createTemplate($file['content']);
                        $name = $name->render($templateVars->getTwigVars());
                        $content = $content->render($templateVars->getTwigVars());
                        $packageService->addOrderDocumentFromString($name, $content);
                    }
                }

                /** @var Orderpos $pos */
                foreach ($templateVars->getOrder()->getPositions() as $pos) {
                    $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                    $orderPosPath = $orderPosPath->render($templateVars->getPosTwigVars($pos->getUuid()));
                    $subPath = str_replace(".zip", "", $orderPosPath);
                    $subPath = $this->checkPath($subPath);
                    $packageService->createPositionPackage(sys_get_temp_dir() . "/" . $orderPosPath);
                    $packageService->setPosition($pos);
                    if ($queueSettings->isPosInvoice()) {
                        $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE);
                    }
                    if ($queueSettings->isPosJobticket()) {
                        $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET);
                    }
                    if ($queueSettings->isPosJobticketPP()) {
                        $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER);
                    }
                    if ($queueSettings->isPosOffer()) {
                        $packageService->addPositionDocument("offer.pdf", Printing::OFFER);
                    }
                    if ($queueSettings->isPosLabel()) {
                        $packageService->addPositionDocument("label.pdf", Printing::LABEL);
                    }
                    if ($queueSettings->isPosOrder()) {
                        $packageService->addPositionDocument("order.pdf", Printing::ORDER);
                    }
                    if ($queueSettings->isPosDelivery()) {
                        $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY);
                    }
                    if ($queueSettings->isPosStorno()) {
                        $packageService->addPositionDocument("storno.pdf", Printing::STORNO);
                    }


                    $packageService->addUploadsToPackage($pos, false, $subPath);
                    $designerPath = $this->_twig->createTemplate($queueSettings->getDesignerPath());
                    $designerPath = $designerPath->render($templateVars->getPosTwigVars($pos->getUuid()));
                    $packageService->addDesignerFilesToPackage($pos, false, $subPath, $designerPath);
                    $packageService->addPluginFilesToPositionPackage(false, $subPath);
                    foreach ($queueSettings->getPosFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                            $name = $this->_twig->createTemplate($file['name']);
                            $content = $this->_twig->createTemplate($file['content']);
                            $name = $name->render($templateVars->getPosTwigVars($pos->getUuid()));
                            $content = $content->render($templateVars->getPosTwigVars($pos->getUuid()));
                            $packageService->addPositionDocumentFromString($name, $content);
                        }
                    }
                    $packageService->addPositionPackageToOrderPackage($orderPosPath);
                }


                $packageService->closePackage();
                return $orderPath;
            }
            if ($queueSettings->getMode() == 1) {
                $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                $orderPath = $orderPath->render($templateVars->getTwigVars());
                $packageService->createPackage(sys_get_temp_dir() . "/" . $orderPath);
                $packageService->setOrder($templateVars->getOrder());
                if ($queueSettings->isOrderInvoice()) {
                    $packageService->addOrderDocument(Printing::INVOICE);
                }
                if ($queueSettings->isOrderJobticket()) {
                    $packageService->addOrderDocument(Printing::JOBTICKET);
                }
                if ($queueSettings->isOrderJobticketPP()) {
                    $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                }
                if ($queueSettings->isOrderOffer()) {
                    $packageService->addOrderDocument(Printing::OFFER);
                }
                if ($queueSettings->isOrderLabel()) {
                    $packageService->addOrderDocument(Printing::LABEL);
                }
                if ($queueSettings->isOrderOrder()) {
                    $packageService->addOrderDocument(Printing::ORDER);
                }
                if ($queueSettings->isOrderDelivery()) {
                    $packageService->addOrderDocument(Printing::DELIVERY);
                }
                if ($queueSettings->isOrderStorno()) {
                    $packageService->addOrderDocument(Printing::STORNO);
                }

                $packageService->addPluginFilesToOrderPackage();
                foreach ($queueSettings->getOrderFiles() as $file) {
                    if ($file['name'] != "" && $file['content'] != "") {
                        $name = $this->_twig->createTemplate($file['name']);
                        $content = $this->_twig->createTemplate($file['content']);
                        $name = $name->render($templateVars->getTwigVars());
                        $content = $content->render($templateVars->getTwigVars());
                        $packageService->addOrderDocumentFromString($name, $content);
                    }
                }

        /** @var Orderpos $pos */
                foreach ($templateVars->getOrder()->getPositions() as $pos) {
                    $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                    $orderPosPath = $orderPosPath->render($templateVars->getPosTwigVars($pos->getUuid()));
                    $orderPosPath = $this->checkPath($orderPosPath);
                    $packageService->setPosition($pos);
                    if ($queueSettings->isPosInvoice()) {
                        $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE, true, $orderPosPath);
                    }
                    if ($queueSettings->isPosJobticket()) {
                        $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET, true, $orderPosPath);
                    }
                    if ($queueSettings->isPosJobticketPP()) {
                        $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER, true, $orderPosPath);
                    }
                    if ($queueSettings->isPosOffer()) {
                        $packageService->addPositionDocument("offer.pdf", Printing::OFFER, true, $orderPosPath);
                    }
                    if ($queueSettings->isPosLabel()) {
                        $packageService->addPositionDocument("label.pdf", Printing::LABEL, true, $orderPosPath);
                    }
                    if ($queueSettings->isPosOrder()) {
                        $packageService->addPositionDocument("order.pdf", Printing::ORDER, true, $orderPosPath);
                    }
                    if ($queueSettings->isPosDelivery()) {
                        $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY, true, $orderPosPath);
                    }
                    if ($queueSettings->isPosStorno()) {
                        $packageService->addPositionDocument("storno.pdf", Printing::STORNO, true, $orderPosPath);
                    }


                    $packageService->addUploadsToPackage($pos, true, $orderPosPath);
                    $designerPath = $this->_twig->createTemplate($queueSettings->getDesignerPath());
                    $designerPath = $designerPath->render($templateVars->getPosTwigVars($pos->getUuid()));
                    $packageService->addDesignerFilesToPackage($pos, true, $orderPosPath, $designerPath);
                    $packageService->addPluginFilesToPositionPackage(true, $orderPosPath);
                    foreach ($queueSettings->getPosFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                            $name = $this->_twig->createTemplate($file['name']);
                            $content = $this->_twig->createTemplate($file['content']);
                            $name = $name->render($templateVars->getPosTwigVars($pos->getUuid()));
                            $content = $content->render($templateVars->getPosTwigVars($pos->getUuid()));
                            $packageService->addPositionDocumentFromString($name, $content, true, $orderPosPath);
                        }
                    }
                }


                $packageService->closePackage();
                return $orderPath;
            }
            if ($queueSettings->getMode() == 4) {
                $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                $orderPath = $orderPath->render($templateVars->getTwigVars());
                $packageService->createPackage(sys_get_temp_dir() . "/" . $orderPath);
                $packageService->setOrder($templateVars->getOrder());
                if ($queueSettings->isOrderInvoice()) {
                    $packageService->addOrderDocument(Printing::INVOICE);
                }
                if ($queueSettings->isOrderJobticket()) {
                    $packageService->addOrderDocument(Printing::JOBTICKET);
                }
                if ($queueSettings->isOrderJobticketPP()) {
                    $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                }
                if ($queueSettings->isOrderOffer()) {
                    $packageService->addOrderDocument(Printing::OFFER);
                }
                if ($queueSettings->isOrderLabel()) {
                    $packageService->addOrderDocument(Printing::LABEL);
                }
                if ($queueSettings->isOrderOrder()) {
                    $packageService->addOrderDocument(Printing::ORDER);
                }
                if ($queueSettings->isOrderDelivery()) {
                    $packageService->addOrderDocument(Printing::DELIVERY);
                }
                if ($queueSettings->isOrderStorno()) {
                    $packageService->addOrderDocument(Printing::STORNO);
                }

                $packageService->addPluginFilesToOrderPackage();
                foreach ($queueSettings->getOrderFiles() as $file) {
                    if ($file['name'] != "" && $file['content'] != "") {
                        $name = $this->_twig->createTemplate($file['name']);
                        $content = $this->_twig->createTemplate($file['content']);
                        $name = $name->render($templateVars->getTwigVars());
                        $content = $content->render($templateVars->getTwigVars());
                        $packageService->addOrderDocumentFromString($name, $content);
                    }
                }

                /** @var Orderpos $pos */
                foreach ($templateVars->getOrder()->getPositions() as $pos) {
                    $designerPath = $this->_twig->createTemplate($queueSettings->getDesignerPath());
                    $designerPath = $designerPath->render($templateVars->getPosTwigVars($pos->getUuid()));
                    $packageService->setPosition($pos);
                    $packageService->addDesignerFilesToPackageWithCustomPathname($pos, $designerPath);
                }

                $packageService->closePackage();
                return $orderPath;
            }
        }

        try {
            if ($event instanceof Create || $event instanceof \PSC\Shop\QueueBundle\Event\Order\Status\Change || $event instanceof \PSC\Shop\QueueBundle\Event\Package\Create) {
            /** @var \PSC\Shop\QueueBundle\Document\Queue\Package $queueSettings */
                $queueSettings = $packageDoc->getQueueDocument();
                $templateVars->loadOrder($event->getOrder());
            /** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
                $orderObj = $this->_doctrine_mongodb
                    ->getRepository('PSC\Shop\EntityBundle\Document\Order')
                    ->findOneBy(array('uid' => (string)$templateVars->getOrder()->getUid()));
                if ($queueSettings->getMode() == 0) {
                    $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                    $orderPath = $orderPath->render($templateVars->getTwigVars());
                    $packageService->createPackage($orderPath);
                    $packageService->setOrder($templateVars->getOrder());
                    if ($queueSettings->isOrderInvoice()) {
                            $packageService->addOrderDocument(Printing::INVOICE);
                    }
                    if ($queueSettings->isOrderJobticket()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET);
                    }
                    if ($queueSettings->isOrderJobticketPP()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                    }
                    if ($queueSettings->isOrderOffer()) {
                        $packageService->addOrderDocument(Printing::OFFER);
                    }
                    if ($queueSettings->isOrderLabel()) {
                        $packageService->addOrderDocument(Printing::LABEL);
                    }
                    if ($queueSettings->isOrderOrder()) {
                        $packageService->addOrderDocument(Printing::ORDER);
                    }
                    if ($queueSettings->isOrderDelivery()) {
                        $packageService->addOrderDocument(Printing::DELIVERY);
                    }
                    if ($queueSettings->isOrderStorno()) {
                        $packageService->addOrderDocument(Printing::STORNO);
                    }

                    $packageService->addPluginFilesToOrderPackage();
                    foreach ($queueSettings->getOrderFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                            $name = $this->_twig->createTemplate($file['name']);
                            $content = $this->_twig->createTemplate($file['content']);
                            $name = $name->render($templateVars->getTwigVars());
                            $content = $content->render($templateVars->getTwigVars());
                            $packageService->addOrderDocumentFromString($name, $content);
                        }
                    }

                    $positionParams = $templateVars->getTwigVars();
        /** @var Orderpos $pos */
                    foreach ($templateVars->getPositions() as $pos) {
                        $positionParams['position'] = $pos;
                        $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                        $orderPosPath = $orderPosPath->render($positionParams);
                        $subPath = str_replace(".zip", "", $orderPosPath);
                        $subPath = $this->checkPath($subPath);
                        $packageService->createPositionPackage(sys_get_temp_dir() . "/" . $orderPosPath);
                        $packageService->setPosition($pos);
                        if ($queueSettings->isPosInvoice()) {
                            $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE);
                        }
                        if ($queueSettings->isPosJobticket()) {
                            $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET);
                        }
                        if ($queueSettings->isPosJobticketPP()) {
                            $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER);
                        }
                        if ($queueSettings->isPosOffer()) {
                            $packageService->addPositionDocument("offer.pdf", Printing::OFFER);
                        }
                        if ($queueSettings->isPosLabel()) {
                            $packageService->addPositionDocument("label.pdf", Printing::LABEL);
                        }
                        if ($queueSettings->isPosOrder()) {
                            $packageService->addPositionDocument("order.pdf", Printing::ORDER);
                        }
                        if ($queueSettings->isPosDelivery()) {
                            $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY);
                        }
                        if ($queueSettings->isPosStorno()) {
                            $packageService->addPositionDocument("storno.pdf", Printing::STORNO);
                        }

                        $designerPath = $this->_twig->createTemplate($queueSettings->getDesignerPath());
                        $designerPath = $designerPath->render($templateVars->getPosTwigVars($pos->getUuid()));
                        $packageService->addUploadsToPackage($pos, false, $subPath);
                        $packageService->addDesignerFilesToPackage($pos, false, $subPath, $designerPath);
                        $packageService->addPluginFilesToPositionPackage(false, $subPath);
                        foreach ($queueSettings->getPosFiles() as $file) {
                            if ($file['name'] != "" && $file['content'] != "") {
                                $name = $this->_twig->createTemplate($file['name']);
                                $content = $this->_twig->createTemplate($file['content']);
                                $name = $name->render($positionParams);
                                $content = $content->render($positionParams);
                                $packageService->addPositionDocumentFromString($name, $content);
                            }
                        }

                        $packageService->addPositionPackageToOrderPackage($orderPosPath);
                    }


                    $packageService->closePackage();
                    $orderObj->setPackageExported(true);
                    $pc = new Created();
                    $pc->setPath($orderPath);
                    $pc->setOrder($templateVars->getOrder()->getUuid());
                    $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                    $this->_eventManager->addJob($pc);
                    $this->_doctrine_mongodb->persist($orderObj);
                    $this->_doctrine_mongodb->flush();
                    return true;
                }
                if ($queueSettings->getMode() == 1) {
                    $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                    $orderPath = $orderPath->render($templateVars->getTwigVars());
                    $packageService->createPackage($orderPath);
                    $packageService->setOrder($templateVars->getOrder());
                    if ($queueSettings->isOrderInvoice()) {
                        $packageService->addOrderDocument(Printing::INVOICE);
                    }
                    if ($queueSettings->isOrderJobticket()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET);
                    }
                    if ($queueSettings->isOrderJobticketPP()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                    }
                    if ($queueSettings->isOrderOffer()) {
                        $packageService->addOrderDocument(Printing::OFFER);
                    }
                    if ($queueSettings->isOrderLabel()) {
                        $packageService->addOrderDocument(Printing::LABEL);
                    }
                    if ($queueSettings->isOrderOrder()) {
                        $packageService->addOrderDocument(Printing::ORDER);
                    }
                    if ($queueSettings->isOrderDelivery()) {
                        $packageService->addOrderDocument(Printing::DELIVERY);
                    }
                    if ($queueSettings->isOrderStorno()) {
                        $packageService->addOrderDocument(Printing::STORNO);
                    }

                    $packageService->addPluginFilesToOrderPackage();
                    foreach ($queueSettings->getOrderFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                            $name = $this->_twig->createTemplate($file['name']);
                            $content = $this->_twig->createTemplate($file['content']);
                            $name = $name->render($templateVars->getTwigVars());
                            $content = $content->render($templateVars->getTwigVars());
                            $packageService->addOrderDocumentFromString($name, $content);
                        }
                    }

                    $positionParams = $templateVars->getTwigVars();
            /** @var Orderpos $pos */
                    foreach ($templateVars->getPositions() as $pos) {
                        $positionParams['position'] = $pos;
                        $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                        $orderPosPath = $orderPosPath->render($positionParams);
                        $orderPosPath = $this->checkPath($orderPosPath);
                        $packageService->setPosition($pos);
                        if ($queueSettings->isPosInvoice()) {
                            $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosJobticket()) {
                            $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosJobticketPP()) {
                            $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosOffer()) {
                            $packageService->addPositionDocument("offer.pdf", Printing::OFFER, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosLabel()) {
                            $packageService->addPositionDocument("label.pdf", Printing::LABEL, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosOrder()) {
                            $packageService->addPositionDocument("order.pdf", Printing::ORDER, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosDelivery()) {
                            $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosStorno()) {
                            $packageService->addPositionDocument("storno.pdf", Printing::STORNO, true, $orderPosPath);
                        }

                        foreach ($queueSettings->getPosFiles() as $file) {
                            if ($file['name'] != "" && $file['content'] != "") {
                                $name = $this->_twig->createTemplate($file['name']);
                                $content = $this->_twig->createTemplate($file['content']);
                                $name = $name->render($positionParams);
                                $content = $content->render($positionParams);
                                $packageService->addPositionDocumentFromString($name, $content, true, $orderPosPath);
                            }
                        }

                        $designerPath = $this->_twig->createTemplate($queueSettings->getDesignerPath());
                        $designerPath = $designerPath->render($templateVars->getPosTwigVars($pos->getUuid()));
                        $packageService->addUploadsToPackage($pos, true, $orderPosPath);
                        $packageService->addDesignerFilesToPackage($pos, true, $orderPosPath, $designerPath);
                        $packageService->addPluginFilesToPositionPackage(true, $orderPosPath);
                    }


                    $packageService->closePackage();
                    $orderObj->setPackageExported(true);
                    $pc = new Created();
                    $pc->setPath($orderPath);
                    $pc->setOrder($templateVars->getOrder()->getUuid());
                    $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                    $this->_eventManager->addJob($pc);
                    $this->_doctrine_mongodb->persist($orderObj);
                    $this->_doctrine_mongodb->flush();
                    return true;
                }
                if ($queueSettings->getMode() == 2) {
                    $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                    $orderPath = $orderPath->render($templateVars->getTwigVars());
                    $packageService->createPackage($orderPath);
                    $packageService->setOrder($templateVars->getOrder());
                    if ($queueSettings->isOrderInvoice()) {
                        $packageService->addOrderDocument(Printing::INVOICE);
                    }
                    if ($queueSettings->isOrderJobticket()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET);
                    }
                    if ($queueSettings->isOrderJobticketPP()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                    }
                    if ($queueSettings->isOrderOffer()) {
                        $packageService->addOrderDocument(Printing::OFFER);
                    }
                    if ($queueSettings->isOrderLabel()) {
                        $packageService->addOrderDocument(Printing::LABEL);
                    }
                    if ($queueSettings->isOrderOrder()) {
                        $packageService->addOrderDocument(Printing::ORDER);
                    }
                    if ($queueSettings->isOrderDelivery()) {
                        $packageService->addOrderDocument(Printing::DELIVERY);
                    }
                    if ($queueSettings->isOrderStorno()) {
                        $packageService->addOrderDocument(Printing::STORNO);
                    }

                    $packageService->addPluginFilesToOrderPackage();
                    foreach ($queueSettings->getOrderFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                            $name = $this->_twig->createTemplate($file['name']);
                            $content = $this->_twig->createTemplate($file['content']);
                            $name = $name->render($templateVars->getTwigVars());
                            $content = $content->render($templateVars->getTwigVars());
                            $packageService->addOrderDocumentFromString($name, $content);
                        }
                    }

                    $packageService->closePackage();
                    $orderObj->setPackageExported(true);
                    $pc = new Created();
                    $pc->setPath($orderPath);
                    $pc->setOrder($templateVars->getOrder()->getUuid());
                    $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                    $this->_eventManager->addJob($pc);
                    $this->_doctrine_mongodb->persist($orderObj);
                    $this->_doctrine_mongodb->flush();
                    return true;
                }
                if ($queueSettings->getMode() == 3) {
                    $positionParams = $templateVars->getTwigVars();
            /** @var Orderpos $pos */
                    foreach ($templateVars->getPositions() as $pos) {
                        $positionParams['position'] = $pos;
                        $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                        $orderPosPath = $orderPosPath->render($positionParams);
                        $packageService->createPositionPackage($orderPosPath);
                        $packageService->setPosition($pos);
                        if ($queueSettings->isPosInvoice()) {
                            $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE);
                        }
                        if ($queueSettings->isPosJobticket()) {
                            $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET);
                        }
                        if ($queueSettings->isPosJobticketPP()) {
                            $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER);
                        }
                        if ($queueSettings->isPosOffer()) {
                            $packageService->addPositionDocument("offer.pdf", Printing::OFFER);
                        }
                        if ($queueSettings->isPosLabel()) {
                            $packageService->addPositionDocument("label.pdf", Printing::LABEL);
                        }
                        if ($queueSettings->isPosOrder()) {
                            $packageService->addPositionDocument("order.pdf", Printing::ORDER);
                        }
                        if ($queueSettings->isPosDelivery()) {
                            $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY);
                        }
                        if ($queueSettings->isPosStorno()) {
                            $packageService->addPositionDocument("storno.pdf", Printing::STORNO);
                        }

                        foreach ($queueSettings->getPosFiles() as $file) {
                            if ($file['name'] != "" && $file['content'] != "") {
                                $name = $this->_twig->createTemplate($file['name']);
                                $content = $this->_twig->createTemplate($file['content']);
                                $name = $name->render($positionParams);
                                $content = $content->render($positionParams);
                                $packageService->addPositionDocumentFromString($name, $content);
                            }
                        }

                        $packageService->addUploadsToPackage($pos);
                        $packageService->addDesignerFilesToPackage($pos);
                        $packageService->closePositionPackage();
                        $pc = new Created();
                        $pc->setPath($orderPosPath);
                        $pc->setOrder($templateVars->getOrder()->getUuid());
                        $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                        $this->_eventManager->addJob($pc);
                    }

                    $orderObj->setPackageExported(true);
                    $this->_doctrine_mongodb->persist($orderObj);
                    $this->_doctrine_mongodb->flush();
                    return true;
                }

                return true;
            }
            if ($event instanceof Change) {
/** @var \PSC\Shop\QueueBundle\Document\Queue\Package $queueSettings */
                $queueSettings = $packageDoc->getQueueDocument();
                $positionRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
/** @var Orderpos $position */
                $position = $positionRepo->findOneBy(array('uuid' => $event->getPosition()));
                $templateVars->loadOrder($position->getOrder()->getUuid());
                if ($queueSettings->getMode() == 0) {
                    $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                    $orderPath = $orderPath->render($templateVars->getTwigVars());
                    $packageService->createPackage($orderPath);
                    $packageService->setOrder($templateVars->getOrder());
                    if ($queueSettings->isOrderInvoice()) {
                        $packageService->addOrderDocument(Printing::INVOICE);
                    }
                    if ($queueSettings->isOrderJobticket()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET);
                    }
                    if ($queueSettings->isOrderJobticketPP()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                    }
                    if ($queueSettings->isOrderOffer()) {
                        $packageService->addOrderDocument(Printing::OFFER);
                    }
                    if ($queueSettings->isOrderLabel()) {
                        $packageService->addOrderDocument(Printing::LABEL);
                    }
                    if ($queueSettings->isOrderOrder()) {
                        $packageService->addOrderDocument(Printing::ORDER);
                    }
                    if ($queueSettings->isOrderDelivery()) {
                        $packageService->addOrderDocument(Printing::DELIVERY);
                    }
                    if ($queueSettings->isOrderStorno()) {
                        $packageService->addOrderDocument(Printing::STORNO);
                    }

                    $packageService->addPluginFilesToOrderPackage();
                    foreach ($queueSettings->getOrderFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                                $name = $this->_twig->createTemplate($file['name']);
                                $content = $this->_twig->createTemplate($file['content']);
                                $name = $name->render($templateVars->getTwigVars());
                                $content = $content->render($templateVars->getTwigVars());
                                $packageService->addOrderDocumentFromString($name, $content);
                        }
                    }

                    $positionParams = $templateVars->getTwigVars();
/** @var Orderpos $pos */
                    foreach ($templateVars->getPositions() as $pos) {
                        if ($pos->getUuid() != $event->getPosition()) {
                            continue;
                        }

                        $positionParams['position'] = $pos;
                        $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                        $orderPosPath = $orderPosPath->render($positionParams);
                        $packageService->createPositionPackage(sys_get_temp_dir() . "/" . $orderPosPath);
                        $packageService->setPosition($pos);
                        if ($queueSettings->isPosInvoice()) {
                            $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE);
                        }
                        if ($queueSettings->isPosJobticket()) {
                            $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET);
                        }
                        if ($queueSettings->isPosJobticketPP()) {
                            $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER);
                        }
                        if ($queueSettings->isPosOffer()) {
                            $packageService->addPositionDocument("offer.pdf", Printing::OFFER);
                        }
                        if ($queueSettings->isPosLabel()) {
                            $packageService->addPositionDocument("label.pdf", Printing::LABEL);
                        }
                        if ($queueSettings->isPosOrder()) {
                            $packageService->addPositionDocument("order.pdf", Printing::ORDER);
                        }
                        if ($queueSettings->isPosDelivery()) {
                            $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY);
                        }
                        if ($queueSettings->isPosStorno()) {
                            $packageService->addPositionDocument("storno.pdf", Printing::STORNO);
                        }

                        $packageService->addUploadsToPackage($pos);
                        $packageService->addDesignerFilesToPackage($pos);
                        foreach ($queueSettings->getPosFiles() as $file) {
                            if ($file['name'] != "" && $file['content'] != "") {
                                $name = $this->_twig->createTemplate($file['name']);
                                $content = $this->_twig->createTemplate($file['content']);
                                $name = $name->render($positionParams);
                                $content = $content->render($positionParams);
                                $packageService->addPositionDocumentFromString($name, $content);
                            }
                        }

                        $packageService->addPositionPackageToOrderPackage($orderPosPath);
                    }


                    $packageService->closePackage();
                    $pc = new Created();
                    $pc->setPath($orderPath);
                    $pc->setOrder($templateVars->getOrder()->getUuid());
                    $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                    $this->_eventManager->addJob($pc);
                    return true;
                }
                if ($queueSettings->getMode() == 1) {
                    $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                    $orderPath = $orderPath->render($templateVars->getTwigVars());
                    $packageService->createPackage($orderPath);
                    $packageService->setOrder($templateVars->getOrder());
                    if ($queueSettings->isOrderInvoice()) {
                        $packageService->addOrderDocument(Printing::INVOICE);
                    }
                    if ($queueSettings->isOrderJobticket()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET);
                    }
                    if ($queueSettings->isOrderJobticketPP()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                    }
                    if ($queueSettings->isOrderOffer()) {
                        $packageService->addOrderDocument(Printing::OFFER);
                    }
                    if ($queueSettings->isOrderLabel()) {
                        $packageService->addOrderDocument(Printing::LABEL);
                    }
                    if ($queueSettings->isOrderOrder()) {
                        $packageService->addOrderDocument(Printing::ORDER);
                    }
                    if ($queueSettings->isOrderDelivery()) {
                        $packageService->addOrderDocument(Printing::DELIVERY);
                    }
                    if ($queueSettings->isOrderStorno()) {
                        $packageService->addOrderDocument(Printing::STORNO);
                    }

                    $packageService->addPluginFilesToOrderPackage();
                    foreach ($queueSettings->getOrderFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                            $name = $this->_twig->createTemplate($file['name']);
                            $content = $this->_twig->createTemplate($file['content']);
                            $name = $name->render($templateVars->getTwigVars());
                            $content = $content->render($templateVars->getTwigVars());
                            $packageService->addOrderDocumentFromString($name, $content);
                        }
                    }

                    $positionParams = $templateVars->getTwigVars();
/** @var Orderpos $pos */
                    foreach ($templateVars->getPositions() as $pos) {
                        if ($pos->getUuid() != $event->getPosition()) {
                            continue;
                        }

                        $positionParams['position'] = $pos;
                        $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                        $orderPosPath = $orderPosPath->render($positionParams);
                        $packageService->setPosition($pos);
                        if ($queueSettings->isPosInvoice()) {
                            $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosJobticket()) {
                            $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosJobticketPP()) {
                            $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosOffer()) {
                            $packageService->addPositionDocument("offer.pdf", Printing::OFFER, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosLabel()) {
                            $packageService->addPositionDocument("label.pdf", Printing::LABEL, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosOrder()) {
                            $packageService->addPositionDocument("order.pdf", Printing::ORDER, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosDelivery()) {
                            $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY, true, $orderPosPath);
                        }
                        if ($queueSettings->isPosStorno()) {
                            $packageService->addPositionDocument("storno.pdf", Printing::STORNO, true, $orderPosPath);
                        }

                        foreach ($queueSettings->getPosFiles() as $file) {
                            if ($file['name'] != "" && $file['content'] != "") {
                                $name = $this->_twig->createTemplate($file['name']);
                                $content = $this->_twig->createTemplate($file['content']);
                                $name = $name->render($positionParams);
                                $content = $content->render($positionParams);
                                $packageService->addPositionDocumentFromString($name, $content, true, $orderPosPath);
                            }
                        }

                        $packageService->addUploadsToPackage($pos, true, $orderPosPath);
                        $packageService->addDesignerFilesToPackage($pos, true, $orderPosPath);
                    }


                    $packageService->closePackage();
                    $pc = new Created();
                    $pc->setPath($orderPath);
                    $pc->setOrder($templateVars->getOrder()->getUuid());
                    $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                    $this->_eventManager->addJob($pc);
                    return true;
                }
                if ($queueSettings->getMode() == 2) {
                    $orderPath = $this->_twig->createTemplate($queueSettings->getOrderPath());
                    $orderPath = $orderPath->render($templateVars->getTwigVars());
                    $packageService->createPackage($orderPath);
                    $packageService->setOrder($templateVars->getOrder());
                    if ($queueSettings->isOrderInvoice()) {
                        $packageService->addOrderDocument(Printing::INVOICE);
                    }
                    if ($queueSettings->isOrderJobticket()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET);
                    }
                    if ($queueSettings->isOrderJobticketPP()) {
                        $packageService->addOrderDocument(Printing::JOBTICKET_PRINTPARTNER);
                    }
                    if ($queueSettings->isOrderOffer()) {
                        $packageService->addOrderDocument(Printing::OFFER);
                    }
                    if ($queueSettings->isOrderLabel()) {
                        $packageService->addOrderDocument(Printing::LABEL);
                    }
                    if ($queueSettings->isOrderOrder()) {
                        $packageService->addOrderDocument(Printing::ORDER);
                    }
                    if ($queueSettings->isOrderDelivery()) {
                        $packageService->addOrderDocument(Printing::DELIVERY);
                    }
                    if ($queueSettings->isOrderStorno()) {
                        $packageService->addOrderDocument(Printing::STORNO);
                    }

                    $packageService->addPluginFilesToOrderPackage();
                    foreach ($queueSettings->getOrderFiles() as $file) {
                        if ($file['name'] != "" && $file['content'] != "") {
                            $name = $this->_twig->createTemplate($file['name']);
                            $content = $this->_twig->createTemplate($file['content']);
                            $name = $name->render($templateVars->getTwigVars());
                            $content = $content->render($templateVars->getTwigVars());
                            $packageService->addOrderDocumentFromString($name, $content);
                        }
                    }

                    $packageService->closePackage();
                    $pc = new Created();
                    $pc->setPath($orderPath);
                    $pc->setOrder($templateVars->getOrder()->getUuid());
                    $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                    $this->_eventManager->addJob($pc);
                    return true;
                }
                if ($queueSettings->getMode() == 3) {
                    $positionParams = $templateVars->getTwigVars();
/** @var Orderpos $pos */
                    foreach ($templateVars->getPositions() as $pos) {
                        if ($pos->getUuid() != $event->getPosition()) {
                            continue;
                        }

                        $positionParams['position'] = $pos;
                        $orderPosPath = $this->_twig->createTemplate($queueSettings->getPosPath());
                        $orderPosPath = $orderPosPath->render($positionParams);
                        $packageService->createPositionPackage($orderPosPath);
                        $packageService->setPosition($pos);
                        if ($queueSettings->isPosInvoice()) {
                            $packageService->addPositionDocument("invoice.pdf", Printing::INVOICE);
                        }
                        if ($queueSettings->isPosJobticket()) {
                            $packageService->addPositionDocument("jobticket.pdf", Printing::JOBTICKET);
                        }
                        if ($queueSettings->isPosJobticketPP()) {
                            $packageService->addPositionDocument("jobticket_pp.pdf", Printing::JOBTICKET_PRINTPARTNER);
                        }
                        if ($queueSettings->isPosOffer()) {
                            $packageService->addPositionDocument("offer.pdf", Printing::OFFER);
                        }
                        if ($queueSettings->isPosLabel()) {
                            $packageService->addPositionDocument("label.pdf", Printing::LABEL);
                        }
                        if ($queueSettings->isPosOrder()) {
                            $packageService->addPositionDocument("order.pdf", Printing::ORDER);
                        }
                        if ($queueSettings->isPosDelivery()) {
                            $packageService->addPositionDocument("delivery.pdf", Printing::DELIVERY);
                        }
                        if ($queueSettings->isPosStorno()) {
                            $packageService->addPositionDocument("storno.pdf", Printing::STORNO);
                        }

                        foreach ($queueSettings->getPosFiles() as $file) {
                            if ($file['name'] != "" && $file['content'] != "") {
                                $name = $this->_twig->createTemplate($file['name']);
                                $content = $this->_twig->createTemplate($file['content']);
                                $name = $name->render($positionParams);
                                $content = $content->render($positionParams);
                                $packageService->addPositionDocumentFromString($name, $content);
                            }
                        }

                        $packageService->addUploadsToPackage($pos);
                        $packageService->addDesignerFilesToPackage($pos);
                        $packageService->closePositionPackage();
                        $pc = new Created();
                        $pc->setPath($orderPosPath);
                        $pc->setOrder($templateVars->getOrder()->getUuid());
                        $pc->setShop($templateVars->getOrder()->getShop()->getUID());
                        $this->_eventManager->addJob($pc);
                    }

                    return true;
                }
            }
        } catch (\Exception $e) {
            $this->_error = $e->getMessage() . $e->getTraceAsString();
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->_error;
    }

    function convert($size)
    {
        $unit = array('b','kb','mb','gb','tb','pb');
        return @round($size / pow(1024, ($i = floor(log($size, 1024)))), 2) . ' ' . $unit[$i] . ' ' . PHP_EOL;
    }

    public function getHelp()
    {
        return TemplateVars::getHelp();
    }

    private function checkPath($path)
    {

        if (substr($path, -1) != "/") {
            return $path . "/";
        }

        return $path;
    }
}
