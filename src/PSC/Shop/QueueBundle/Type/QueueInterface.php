<?php

namespace PSC\Shop\QueueBundle\Type;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\QueueBundle\Event\EventInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;

interface QueueInterface extends RegistryElementInterface
{
    /**
     * @return Response
     */
    public function getDescription();
/**
     * @return Response
     */
    public function getName();
    public function injectDocument(Form $form, EventInterface $event, Queue $doc);
    public function setFormData(Form $form, EventInterface $event, Queue $doc);
    public function execute(EventInterface $event, Queue $doc);
    public function getError();
    public function getGroup();
}
