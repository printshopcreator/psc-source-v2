<?php

namespace PSC\Shop\QueueBundle\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('name', TextType::class, ['label' => 'Name (z.b. xyz.xml)', 'label_attr' => ['class' => 'test']]);
        $builder->add('content', TextareaType::class, array('label' => 'Template', 'attr' => ['rows' => 6]));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
