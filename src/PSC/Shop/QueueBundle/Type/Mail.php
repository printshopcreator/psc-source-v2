<?php

namespace PSC\Shop\QueueBundle\Type;

require_once(__DIR__ . '/../../../Shop/EntityBundle/Lagacy/TP_Basket_Item.php');
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use MongoDB\BSON\ObjectId;
use Plugin\System\PSC\Ftp\Event\Package\Uploaded;
use Plugin\System\PSC\Upload\Document\Upload;
use PSC\Shop\ContactBundle\Event\Contact\Lock;
use PSC\Shop\ContactBundle\Event\Contact\UnLock;
use PSC\Shop\EntityBundle\Document\Order;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\QueueBundle\Event\Contact\Password\Reset\Finish;
use PSC\Shop\QueueBundle\Event\Contact\Password\Reset\Start;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Order\Create;
use PSC\Shop\QueueBundle\Event\Position\Contact\Approval\Accept;
use PSC\Shop\QueueBundle\Event\Position\Contact\Approval\Declined;
use PSC\Shop\QueueBundle\Event\Position\Contact\Approval\Request;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Event\Product\BuyMax;
use PSC\Shop\QueueBundle\Event\Product\StockMin;
use PSC\Shop\QueueBundle\PSCShopQueueBundle;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\Log;
use PSC\System\SettingsBundle\Service\Printing;
use PSC\System\SettingsBundle\Service\TemplateVars;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Message;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
class Mail implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    protected $_formFactory = null;
    protected $_entityManager = null;
    protected $_doctrine_mongodb = null;
    protected $_mailer = null;
    protected $_printing = null;
    protected $_templateVars = null;
    protected $_template = null;
    protected $_error = null;
/**
     * @var Log
     */
    protected Log $_logService;
    function __construct(TemplateVars $templateVars, Printing $printing, FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb, MailerInterface $mailer, \Twig\Environment $template, Log $logService)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
        $this->_mailer = $mailer;
        $this->_template = $template;
        $this->_templateVars = $templateVars;
        $this->_printing = $printing;
        $this->_logService = $logService;
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return 'Allgemeines';
    }

    protected function getQueueDocument()
    {
        return new \PSC\Shop\QueueBundle\Document\Queue\Mail();
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'mail';
    }

    public function getDescription()
    {
        return 'Mail Queue';
    }

    public function getName()
    {
        return 'Mail';
    }

    public function getForm(FormBuilderInterface $builder, $form_options, EventInterface $event)
    {
        if ($event->isOverwriteQueueForm()) {
            $event->getQueueForm($builder, $form_options, $event);
        } else {
            $builder->add("text", TextareaType::class, array('label' => 'Text', 'required' => false, 'attr' => array('rows' => 10, 'class' => 'form-element')));
            $builder->add("textHtml", TextareaType::class, array('label' => 'Text HTML', 'required' => false, 'attr' => array('rows' => 10, 'class' => 'form-element')));
            $builder->add("from", TextType::class, array('label' => 'Von E-Mail*', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("fromName", TextType::class, array('label' => 'Von Name', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("to", TextType::class, array('label' => 'An E-Mail*', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("toName", TextType::class, array('label' => 'An Name', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("subject", TextType::class, array('label' => 'Betreff*', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("bcc", TextType::class, array('label' => 'BCC (info@..,prod@...)', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendInvoice", CheckboxType::class, array('label' => 'Rechnung anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendDelivery", CheckboxType::class, array('label' => 'Lieferschein anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendOrder", CheckboxType::class, array('label' => 'Auftrag anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendOffer", CheckboxType::class, array('label' => 'Angebot anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendJobticket", CheckboxType::class, array('label' => 'Jobticket anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendLabel", CheckboxType::class, array('label' => 'Label anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendStorno", CheckboxType::class, array('label' => 'Storno anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendPosInvoice", CheckboxType::class, array('label' => 'Rechnung anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendPosDelivery", CheckboxType::class, array('label' => 'Lieferschein anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendPosOrder", CheckboxType::class, array('label' => 'Auftrag anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendPosOffer", CheckboxType::class, array('label' => 'Angebot anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendPosJobticket", CheckboxType::class, array('label' => 'Jobticket anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendPosLabel", CheckboxType::class, array('label' => 'Label anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add("sendPosStorno", CheckboxType::class, array('label' => 'Storno anfügen?', 'required' => false, 'attr' => array('class' => 'form-element')));
            $builder->add('files', CollectionType::class, array(
                'entry_type' => FileType::class,
                'entry_options' => array('label' => false),
                'allow_add' => true,
                'allow_delete' => true,
                'label' => false
            ));
        }
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        if ($event->isOverwriteQueueForm()) {
            $event->injectQueueDocument($form, $event, $objQueue);
        } else {
            $mail = $this->getQueueDocument();
            $mail->setText($form->get('text')->getData());
            $mail->setTextHtml($form->get('textHtml')->getData());
            $mail->setFrom($form->get('from')->getData());
            $mail->setFromName($form->get('fromName')->getData());
            $mail->setTo($form->get('to')->getData());
            $mail->setToName($form->get('toName')->getData());
            $mail->setSubject($form->get('subject')->getData());
            $mail->setBcc($form->get('bcc')->getData());
            $mail->setSendInvoice($form->get('sendInvoice')->getData());
            $mail->setSendDelivery($form->get('sendDelivery')->getData());
            $mail->setSendOrder($form->get('sendOrder')->getData());
            $mail->setSendOffer($form->get('sendOffer')->getData());
            $mail->setSendJobticket($form->get('sendJobticket')->getData());
            $mail->setSendLabel($form->get('sendLabel')->getData());
            $mail->setSendStorno($form->get('sendStorno')->getData());
            $mail->setSendPosInvoice($form->get('sendPosInvoice')->getData());
            $mail->setSendPosDelivery($form->get('sendPosDelivery')->getData());
            $mail->setSendPosOrder($form->get('sendPosOrder')->getData());
            $mail->setSendPosOffer($form->get('sendPosOffer')->getData());
            $mail->setSendPosJobticket($form->get('sendPosJobticket')->getData());
            $mail->setSendPosLabel($form->get('sendPosLabel')->getData());
            $mail->setSendPosStorno($form->get('sendPosStorno')->getData());
            $mail->setFiles($form->get('files')->getData());
            $objQueue->setQueueDocument($mail);
        }
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
        if ($event->isOverwriteQueueForm()) {
            $event->setFormQueueData($form, $event, $queueObj);
        } else {
            $form->get('text')->setData($queueObj->getQueueDocument()->getText());
            $form->get('textHtml')->setData($queueObj->getQueueDocument()->getTextHTML());
            $form->get('from')->setData($queueObj->getQueueDocument()->getFrom());
            $form->get('fromName')->setData($queueObj->getQueueDocument()->getFromName());
            $form->get('to')->setData($queueObj->getQueueDocument()->getTo());
            $form->get('toName')->setData($queueObj->getQueueDocument()->getToName());
            $form->get('subject')->setData($queueObj->getQueueDocument()->getSubject());
            $form->get('bcc')->setData($queueObj->getQueueDocument()->getBcc());
            $form->get('sendInvoice')->setData($queueObj->getQueueDocument()->isSendInvoice());
            $form->get('sendDelivery')->setData($queueObj->getQueueDocument()->isSendDelivery());
            $form->get('sendOrder')->setData($queueObj->getQueueDocument()->isSendOrder());
            $form->get('sendOffer')->setData($queueObj->getQueueDocument()->isSendOffer());
            $form->get('sendJobticket')->setData($queueObj->getQueueDocument()->isSendJobticket());
            $form->get('sendLabel')->setData($queueObj->getQueueDocument()->isSendLabel());
            $form->get('sendStorno')->setData($queueObj->getQueueDocument()->isSendStorno());
            $form->get('sendPosInvoice')->setData($queueObj->getQueueDocument()->isSendPosInvoice());
            $form->get('sendPosDelivery')->setData($queueObj->getQueueDocument()->isSendPosDelivery());
            $form->get('sendPosOrder')->setData($queueObj->getQueueDocument()->isSendPosOrder());
            $form->get('sendPosOffer')->setData($queueObj->getQueueDocument()->isSendPosOffer());
            $form->get('sendPosJobticket')->setData($queueObj->getQueueDocument()->isSendPosJobticket());
            $form->get('sendPosLabel')->setData($queueObj->getQueueDocument()->isSendPosLabel());
            $form->get('sendPosStorno')->setData($queueObj->getQueueDocument()->isSendPosStorno());
            $form->get('files')->setData($queueObj->getQueueDocument()->getFiles());
        }
    }

    public function getTemplate()
    {
        return '@PSCShopQueue/queue/mail.html.twig';
    }

    /**
     * @param EventInterface $event
     * @param Queue $queue
     * @return bool
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \MongoException
     * @throws \Throwable
     */
    public function execute(EventInterface $event, Queue $queue)
    {

        /** @var \PSC\Shop\QueueBundle\Document\Queue\Mail $mailDoc */
        $mailDoc = $queue->getQueueDocument();
        $eventDoc = $queue->getEventDocument();
/** @var TemplateVars $templateVars */
        $templateVars = $this->_templateVars;
/** @var Printing $printing */
        $printing = $this->_printing;
        if ($event->isOverwriteQueueHandling()) {
            try {
                $event->executeQueue($event, $queue);
            } catch (\Exception $e) {
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }

        $subject = $this->_template->createTemplate($mailDoc->getSubject());
        $from = $this->_template->createTemplate($mailDoc->getFrom());
        $to = $this->_template->createTemplate($mailDoc->getTo());
        $text = null;
        $html = null;
        $bcc = null;
        if ($mailDoc->getText() != "") {
            $text = $this->_template->createTemplate($mailDoc->getText());
        }
        if ($mailDoc->getTextHtml() != "") {
            $html = $this->_template->createTemplate($mailDoc->getTextHtml());
        }
        if ($mailDoc->getBcc() != "") {
            $bcc = $this->_template->createTemplate($mailDoc->getBcc());
        }

        if ($event instanceof Lock) {
            $contactRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
/** @var Contact $contact */
            $contact = $contactRepo->findOneBy(array('uuid' => $event->getContact()));
            $shopRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shop');
/** @var Shop $shop */
            $shop = $shopRepo->findOneBy(array('uid' => $event->getShop()));
            $params = array(
                'contact' => $contact,
                'shop' => $shop,
            );
            try {
                $message = (new Email())
                    ->subject($subject->render($params))
                    ->from($from->render($params))
                    ->to(trim($to->render($params)));
                if ($text) {
                    $message->text($text->render($params));
                }

                if ($bcc) {
                    $bccArray = explode(",", $bcc->render($params));
                    foreach ($bccArray as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($params));
                }
                $this->_logService->createLogEntry($shop, new Contact(), LogEntry::INFO, PSCShopQueueBundle::class, $queue->getName(), "Lock Mail send", [
                    'message' => $message->getTextBody(),
                    'from' => $from->render($params),
                    'to' => $to->render($params),
                    'subject' => $subject->render($params)
                ]);
                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_logService->createLogEntry($shop, new Contact(), LogEntry::ERROR, PSCShopQueueBundle::class, $queue->getName(), "Lock Mail error", [
                    'error' => $e->getMessage()
                ]);
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof UnLock || $event instanceof \PSC\Shop\QueueBundle\Event\Contact\Create) {
            $contactRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
/** @var Contact $contact */
            $contact = $contactRepo->findOneBy(array('uuid' => $event->getContact()));
/** @var \PSC\Shop\EntityBundle\Document\Contact $contactObj */
            $contactObj = $this->_doctrine_mongodb
                ->getRepository('PSC\Shop\EntityBundle\Document\Contact')
                ->findOneBy(array('uid' => (string) $contact->getId()));
            $shopRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shop');
/** @var Shop $shop */
            $shop = $shopRepo->findOneBy(array('uid' => $event->getShop()));
            $params = array(
                'contact' => $contact,
                'contactDoc' => $contactObj,
                'shop' => $shop,
            );
            try {
                $message = (new Email())
                    ->subject($subject->render($params))
                    ->from($from->render($params))
                    ->to(trim($to->render($params)));
                if ($text) {
                    $message->text($text->render($params));
                }

                if ($bcc) {
                    $bcc = explode(",", $bcc->render($params));
                    foreach ($bcc as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($params));
                }

                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof Start) {
            $contactRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
/** @var Contact $contact */
            $contact = $contactRepo->findOneBy(array('uuid' => $event->getContact()));
            $shopRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shop');
/** @var Shop $shop */
            $shop = $shopRepo->findOneBy(array('uid' => $event->getShop()));
            $contact->setPasswordResetUuid(Uuid::uuid4());
            $contact->setPasswordResetDate(time());
            $this->_entityManager->persist($contact);
            $this->_entityManager->flush();
            $params = array(
                'contact' => $contact,
                'shop' => $shop,
            );
            try {
                $message = (new Email())
                    ->subject($subject->render($params))
                    ->from($from->render($params))
                    ->to(trim($to->render($params)));
                if ($text) {
                    $message->text($text->render($params));
                }

                if ($bcc) {
                    $bcc = explode(",", $bcc->render($params));
                    foreach ($bcc as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($params));
                }

                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof Finish) {
            $contactRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
/** @var Contact $contact */
            $contact = $contactRepo->findOneBy(array('uuid' => $event->getContact()));
            $shopRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shop');
/** @var Shop $shop */
            $shop = $shopRepo->findOneBy(array('uid' => $event->getShop()));
            $contact->setPassword($this->generatePassword(7));
            $this->_entityManager->persist($contact);
            $this->_entityManager->flush();
            $params = array(
                'contact' => $contact,
                'shop' => $shop,
            );
            try {
                $message = (new Email())
                    ->subject($subject->render($params))
                    ->from($from->render($params))
                    ->to(trim($to->render($params)));
                if ($text) {
                    $message->text($text->render($params));
                }

                if ($bcc) {
                    $bcc = explode(",", $bcc->render($params));
                    foreach ($bcc as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($params));
                }

                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof Notify) {
            $positionRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
/** @var Orderpos $position */
            $position = $positionRepo->findOneBy(array('uuid' => $event->getPosition()));
            $templateVars->loadOrder($position->getOrder()->getUuid());
            $params = $templateVars->getPosTwigVars($event->getPosition());
/** @var Position $objDoc */
            $objDoc = $this->_doctrine_mongodb
                ->getRepository('PSCEntityBundle:Position')
                ->findOneBy(['uid' => (string)$position->getId()]);
            $params['printPartnerEmail'] = $event->getEmail();
            $params['doc'] = $objDoc;
            $params['packageLink'] = '/apps/backend/order/detail/package/printpartner/' . $event->getPosition();
            try {
                $message = (new Email())
                    ->subject($subject->render($params))
                    ->from($from->render($params))
                    ->to($to->render($params));
                if ($text) {
                    $message->text($text->render($params));
                }

                if ($bcc) {
                    $bcc = explode(",", $bcc->render($params));
                    foreach ($bcc as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($params));
                }

                $content = $printing->generatePosition($position, Printing::JOBTICKET_PRINTPARTNER);
                if ($content) {
                    $message->attach($content, 'jobticket.pdf', 'application/pdf');
                }

                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof Change) {
            $positionRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');
/** @var Orderpos $position */
            $position = $positionRepo->findOneBy(array('uuid' => $event->getPosition()));
            $templateVars->loadOrder($position->getOrder()->getUuid());
            try {
                $message = (new Email())
                    ->subject($subject->render($templateVars->getPosTwigVars($position->getUuid())))
                    ->from($from->render($templateVars->getPosTwigVars($position->getUuid())))
                    ->to($to->render($templateVars->getPosTwigVars($position->getUuid())));
                if ($text) {
                    $message->text($text->render($templateVars->getPosTwigVars($position->getUuid())));
                }

                if ($bcc) {
                    $bccArray = explode(",", $bcc->render($templateVars->getPosTwigVars($position->getUuid())));
                    foreach ($bccArray as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($templateVars->getPosTwigVars($position->getUuid())));
                }

                if ($mailDoc->isSendInvoice()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::INVOICE);
                    if ($content) {
                        $message->attach($content, 'jobticket.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendDelivery()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::DELIVERY);
                    if ($content) {
                        $message->attach($content, 'jobticket.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendJobticket()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::JOBTICKET);
                    if ($content) {
                        $message->attach($content, 'jobticket.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendLabel()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::LABEL);
                    if ($content) {
                        $message->attach($content, 'jobticket.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOffer()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::OFFER);
                    if ($content) {
                        $message->attach($content, 'jobticket.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOrder()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::ORDER);
                    if ($content) {
                        $message->attach($content, 'jobticket.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendStorno()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::STORNO);
                    if ($content) {
                        $message->attach($content, 'jobticket.pdf', 'application/pdf');
                    }
                }
                if ($mailDoc->isSendPosInvoice()) {
                    $content = $printing->generatePosition($position, Printing::INVOICE);
                    if ($content) {
                        $message->attach($content, 'pos_invoice.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendPosDelivery()) {
                    $content = $printing->generatePosition($position, Printing::DELIVERY);
                    if ($content) {
                        $message->attach($content, 'pos_delivery.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendPosJobticket()) {
                    $content = $printing->generatePosition($position, Printing::JOBTICKET);
                    if ($content) {
                        $message->attach($content, 'pos_jobticket.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendPosLabel()) {
                    $content = $printing->generatePosition($position, Printing::LABEL);
                    if ($content) {
                        $message->attach($content, 'pos_label.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendPosOffer()) {
                    $content = $printing->generatePosition($position, Printing::OFFER);
                    if ($content) {
                        $message->attach($content, 'pos_offer.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendPosOrder()) {
                    $content = $printing->generatePosition($position, Printing::ORDER);
                    if ($content) {
                        $message->attach($content, 'pos_order.pdf', 'application/pdf');
                    }
                }

                if ($mailDoc->isSendPosStorno()) {
                    $content = $printing->generatePosition($position, Printing::STORNO);
                    if ($content) {
                        $message->attach($content, 'pos_storno.pdf', 'application/pdf');
                    }
                }
                foreach ($mailDoc->getFiles() as $file) {
                    if ($file['name'] != "" && $file['content'] != "") {
                        $fileContent = $this->_template->createTemplate($file['content']);
                        $fileName = $this->_template->createTemplate($file['name']);
                        $message->attach($fileContent->render($templateVars->getTwigVars()), $fileName->render($templateVars->getTwigVars()));
                    }
                }
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::INFO, PSCShopQueueBundle::class, $queue->getName(), "Mail send", [
                    'message' => $message->getTextBody(),
                    'from' => $from->render($templateVars->getPosTwigVars($position->getUuid())),
                    'to' => $to->render($templateVars->getPosTwigVars($position->getUuid())),
                    'subject' => $subject->render($templateVars->getPosTwigVars($position->getUuid()))
                ]);
                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::ERROR, PSCShopQueueBundle::class, $queue->getName(), "Request Mail error", [
                    'error' => $e->getMessage()
                ]);
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof Create || $event instanceof Uploaded || $event instanceof \PSC\Shop\QueueBundle\Event\Order\Status\Change || $event instanceof \PSC\Shop\QueueBundle\Event\Offer\Create) {
            $templateVars->loadOrder($event->getOrder());
            try {
                $message = (new Email())
                    ->subject($subject->render($templateVars->getTwigVars()))
                    ->from($from->render($templateVars->getTwigVars()))
                    ->to(trim($to->render($templateVars->getTwigVars())));
                if ($text) {
                    $message->text($text->render($templateVars->getTwigVars()));
                }

                if ($bcc) {
                    $bccArray = explode(",", $bcc->render($templateVars->getTwigVars()));
                    foreach ($bccArray as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($templateVars->getTwigVars()));
                }

                if ($mailDoc->isSendInvoice()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::INVOICE);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendDelivery()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::DELIVERY);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendJobticket()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::JOBTICKET);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendLabel()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::LABEL);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOffer()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::OFFER);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOrder()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::ORDER);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendStorno()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::STORNO);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }
                foreach ($mailDoc->getFiles() as $file) {
                    if ($file['name'] != "" && $file['content'] != "") {
                        $fileContent = $this->_template->createTemplate($file['content']);
                        $fileName = $this->_template->createTemplate($file['name']);
                        $message->attach($fileContent->render($templateVars->getTwigVars()), $fileName->render($templateVars->getTwigVars()));
                    }
                }
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::INFO, PSCShopQueueBundle::class, $queue->getName(), "Mail send", [
                    'message' => $message->getTextBody(),
                    'from' => $from->render($templateVars->getTwigVars()),
                    'to' => trim($to->render($templateVars->getTwigVars()))
                ]);
                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::ERROR, PSCShopQueueBundle::class, $queue->getName(), "error", [
                    'error' => $e->getMessage()
                ]);
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof \Plugin\System\PSC\Upload\Event\Create) {
/** @var Upload $upload */
            $upload = $this->_doctrine_mongodb
                ->getRepository(Upload::class)
                ->findOneBy(['id' => new ObjectId($event->getUpload())]);
            $params = array(
                'upload' => $upload
            );
            try {
                $message = (new Email())
                    ->subject($subject->render($params))
                    ->from($from->render($params))
                    ->to(trim($to->render($params)));
                if ($text) {
                    $message->text($text->render($params));
                }

                if ($bcc) {
                    $bccArray = explode(",", $bcc->render($params));
                    foreach ($bccArray as $bc) {
                        if (trim($bc) != new Contact()) {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($params));
                }
                $this->_logService->createLogEntry(new Shop(), new Contact(), LogEntry::INFO, PSCShopQueueBundle::class, $queue->getName(), "\Plugin\System\PSC\Upload\Event\Create Mail send", [
                    'message' => $message->getTextBody(),
                    'from' => $message->getFrom(),
                    'to' => $message->getTo()
                ]);
                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_logService->createLogEntry(new Shop(), new Contact(), LogEntry::ERROR, PSCShopQueueBundle::class, $queue->getName(), "\Plugin\System\PSC\Upload\Event\Create Mail error", [
                    'error' => $e->getMessage()
                ]);
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof StockMin || $event instanceof BuyMax) {
            $productRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Product');
/** @var Product $product */
            $product = $productRepo->findOneBy(array('uuid' => $event->getProduct()));
            $shopRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shop');
/** @var Shop $shop */
            $shop = $shopRepo->findOneBy(array('uid' => $event->getShop()));
            $params = array(
                'product' => $product,
                'shop' => $shop,
            );
            try {
                $message = (new Email())
                    ->subject($subject->render($params))
                    ->from($from->render($params))
                    ->to(trim($to->render($params)));
                if ($text) {
                    $message->text($text->render($params));
                }

                if ($bcc) {
                    $bccArray = explode(",", $bcc->render($params));
                    foreach ($bccArray as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($params));
                }
                $this->_logService->createLogEntry($shop, new Contact(), LogEntry::INFO, PSCShopQueueBundle::class, $queue->getName(), "StockMin BuyMax Mail send", [
                    'message' => $message->getTextBody(),
                    'from' => $from->render($params),
                    'to' => $to->render($params),
                    'subject' => $subject->render($params)
                ]);
                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_logService->createLogEntry($shop, new Contact(), LogEntry::ERROR, PSCShopQueueBundle::class, $queue->getName(), "StockMin BuyMax Mail error", [
                    'error' => $e->getMessage()
                ]);
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof Request) {
            $templateVars->loadOrder($event->getOrder());
            $vars = $templateVars->getPosTwigVars($event->getPosition());
            $contactRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
/** @var Contact $contact */
            $contact = $contactRepo->findOneBy(array('uuid' => $event->getContact()));
            $vars['approvalContact'] = $contact;
            try {
                $message = (new Email())
                    ->subject($subject->render($vars))
                    ->from($from->render($vars))
                    ->to(trim($to->render($vars)));
                if ($text) {
                    $message->text($text->render($vars));
                }

                if ($bcc) {
                    $bccArray = explode(",", $bcc->render($vars));
                    foreach ($bccArray as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($vars));
                }

                if ($mailDoc->isSendInvoice()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::INVOICE);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendDelivery()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::DELIVERY);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendJobticket()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::JOBTICKET);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendLabel()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::LABEL);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOffer()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::OFFER);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOrder()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::ORDER);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendStorno()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::STORNO);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }
                foreach ($mailDoc->getFiles() as $file) {
                    if ($file['name'] != "" && $file['content'] != "") {
                        $fileContent = $this->_template->createTemplate($file['content']);
                        $fileName = $this->_template->createTemplate($file['name']);
                        $message->attach($fileContent->render($vars), $fileName->render($vars));
                    }
                }
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::INFO, PSCShopQueueBundle::class, $queue->getName(), "Request Mail send", [
                    'message' => $message->getTextBody(),
                    'from' => $from->render($vars),
                    'to' => $to->render($vars),
                    'subject' => $subject->render($vars)
                ]);
                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::ERROR, PSCShopQueueBundle::class, $queue->getName(), "Request Mail error", [
                    'error' => $e->getMessage()
                ]);
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
        if ($event instanceof Accept || $event instanceof Declined) {
            $templateVars->loadOrder($event->getOrder());
            $vars = $templateVars->getPosTwigVars($event->getPosition());
            $contactRepo = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
/** @var Contact $contact */
            $contact = $contactRepo->findOneBy(array('uuid' => $event->getContact()));
            $vars['approvalContact'] = $contact;
            $vars['message'] = $event->getMessage();
            try {
                $message = (new Email())
                    ->subject($subject->render($vars))
                    ->from($from->render($vars))
                    ->to(trim($to->render($vars)));
                if ($text) {
                    $message->text($text->render($vars));
                }

                if ($bcc) {
                    $bccArray = explode(",", $bcc->render($vars));
                    foreach ($bccArray as $bc) {
                        if (trim($bc) != "") {
                            $message->addBcc(trim($bc));
                        }
                    }
                }

                if ($html) {
                    $message->html($html->render($vars));
                }

                if ($mailDoc->isSendInvoice()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::INVOICE);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendDelivery()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::DELIVERY);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendJobticket()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::JOBTICKET);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendLabel()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::LABEL);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOffer()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::OFFER);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendOrder()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::ORDER);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }

                if ($mailDoc->isSendStorno()) {
                    $content = $printing->generateOrder($templateVars->getOrder(), Printing::STORNO);
                    if ($content) {
                        $message->attach($content, $printing->getFileName(), 'application/pdf');
                    }
                }
                foreach ($mailDoc->getFiles() as $file) {
                    if ($file['name'] != "" && $file['content'] != "") {
                        $fileContent = $this->_template->createTemplate($file['content']);
                        $fileName = $this->_template->createTemplate($file['name']);
                        $message->attach($fileContent->render($vars), $fileName->render($vars));
                    }
                }
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::INFO, PSCShopQueueBundle::class, $queue->getName(), "Accept Declined Mail send", [
                    'message' => $message->getTextBody(),
                    'from' => $from->render($vars),
                    'to' => $to->render($vars),
                    'subject' => $subject->render($vars)
                ]);
                $this->_mailer->send($message);
            } catch (\Exception $e) {
                $this->_logService->createLogEntry($templateVars->getOrder()->getShop(), new Contact(), LogEntry::ERROR, PSCShopQueueBundle::class, $queue->getName(), "Accept Declined Mail error", [
                    'error' => $e->getMessage()
                ]);
                $this->_error = $e->getMessage();
                return false;
            }

            return true;
        }
    }

    public function getError()
    {
        return $this->_error;
    }

    private function generatePassword($chars)
    {
        $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
        return substr(str_shuffle($data), 0, $chars);
    }
}
