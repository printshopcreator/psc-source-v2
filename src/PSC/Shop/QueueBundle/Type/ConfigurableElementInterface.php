<?php

namespace PSC\Shop\QueueBundle\Type;

use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\QueueBundle\Event\EventInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;

interface ConfigurableElementInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param $form_options
     * @return FormBuilderInterface
     */
    public function getForm(FormBuilderInterface $builder, $form_options, EventInterface $event);
}
