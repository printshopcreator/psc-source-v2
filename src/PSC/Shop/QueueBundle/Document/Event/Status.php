<?php

namespace PSC\Shop\QueueBundle\Document\Event;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Status extends Event
{
    /**
     * @var string $status
     *
     * @Field(type="string")
     */
    protected $status;
/**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }
}
