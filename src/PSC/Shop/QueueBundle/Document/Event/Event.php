<?php

namespace PSC\Shop\QueueBundle\Document\Event;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Event
{
    /**
     * @var string $type
     *
     * @Field(type="string")
     */
    protected $type;
/**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }
}
