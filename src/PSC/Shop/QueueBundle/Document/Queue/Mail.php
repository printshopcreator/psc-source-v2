<?php

namespace PSC\Shop\QueueBundle\Document\Queue;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Mail
{
    /**
     * @var string $text
     *
     * @Field(type="string")
     */
    protected $text;
/**
     * @var string $textHtml
     *
     * @Field(type="string")
     */
    protected $textHtml;
/**
     * @var string $from
     *
     * @Field(type="string")
     */
    protected $from;
/**
     * @var string $fromName
     *
     * @Field(type="string")
     */
    protected $fromName;
/**
     * @var string $to
     *
     * @Field(type="string")
     */
    protected $to;
/**
     * @var string $toName
     *
     * @Field(type="string")
     */
    protected $toName;
/**
     * @var string $subject
     *
     * @Field(type="string")
     */
    protected $subject;
/**
     * @var string $bcc
     *
     * @Field(type="string")
     */
    protected $bcc;
/**
     * @var boolean $sendInvoice
     *
     * @Field(type="bool")
     */
    protected $sendInvoice;
/**
     * @var boolean $sendDelivery
     *
     * @Field(type="bool")
     */
    protected $sendDelivery;
/**
     * @var boolean $sendLabel
     *
     * @Field(type="bool")
     */
    protected $sendLabel;
/**
     * @var boolean $sendOrder
     *
     * @Field(type="bool")
     */
    protected $sendOrder;
/**
     * @var boolean $sendOffer
     *
     * @Field(type="bool")
     */
    protected $sendOffer;
/**
     * @var boolean $sendJobticket
     *
     * @Field(type="bool")
     */
    protected $sendJobticket;
/**
     * @var boolean $sendStorno
     *
     * @Field(type="bool")
     */
    protected $sendStorno;
/**
     * @var boolean $sendPosInvoice
     *
     * @Field(type="bool")
     */
    protected $sendPosInvoice;
/**
     * @var boolean $sendPosDelivery
     *
     * @Field(type="bool")
     */
    protected $sendPosDelivery;
/**
     * @var boolean $sendPosLabel
     *
     * @Field(type="bool")
     */
    protected $sendPosLabel;
/**
     * @var boolean $sendPosOrder
     *
     * @Field(type="bool")
     */
    protected $sendPosOrder;
/**
     * @var boolean $sendPosOffer
     *
     * @Field(type="bool")
     */
    protected $sendPosOffer;
/**
     * @var boolean $sendPosJobticket
     *
     * @Field(type="bool")
     */
    protected $sendPosJobticket;
/**
     * @var boolean $sendPosStorno
     *
     * @Field(type="bool")
     */
    protected $sendPosStorno;
/**
     * @var array $files
     *
     * @Field(type="collection")
     */
    protected $files;
/**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return trim($this->to);
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getTextHtml()
    {
        return $this->textHtml;
    }

    /**
     * @param string $textHtml
     */
    public function setTextHtml($textHtml)
    {
        $this->textHtml = $textHtml;
    }

    /**
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * @param string $fromName
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;
    }

    /**
     * @return string
     */
    public function getToName()
    {
        return $this->toName;
    }

    /**
     * @param string $toName
     */
    public function setToName($toName)
    {
        $this->toName = $toName;
    }

    /**
     * @return string
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @param string $bcc
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
    }

    /**
     * @return bool
     */
    public function isSendInvoice()
    {
        return $this->sendInvoice;
    }

    /**
     * @param bool $sendInvoice
     */
    public function setSendInvoice($sendInvoice)
    {
        $this->sendInvoice = $sendInvoice;
    }

    /**
     * @return bool
     */
    public function isSendDelivery()
    {
        return $this->sendDelivery;
    }

    /**
     * @param bool $sendDelivery
     */
    public function setSendDelivery($sendDelivery)
    {
        $this->sendDelivery = $sendDelivery;
    }

    /**
     * @return bool
     */
    public function isSendLabel()
    {
        return $this->sendLabel;
    }

    /**
     * @param bool $sendLabel
     */
    public function setSendLabel($sendLabel)
    {
        $this->sendLabel = $sendLabel;
    }

    /**
     * @return bool
     */
    public function isSendOrder()
    {
        return $this->sendOrder;
    }

    /**
     * @param bool $sendOrder
     */
    public function setSendOrder($sendOrder)
    {
        $this->sendOrder = $sendOrder;
    }

    /**
     * @return bool
     */
    public function isSendOffer()
    {
        return $this->sendOffer;
    }

    /**
     * @param bool $sendOffer
     */
    public function setSendOffer($sendOffer)
    {
        $this->sendOffer = $sendOffer;
    }

    /**
     * @return bool
     */
    public function isSendJobticket()
    {
        return $this->sendJobticket;
    }

    /**
     * @param bool $sendJobticket
     */
    public function setSendJobticket($sendJobticket)
    {
        $this->sendJobticket = $sendJobticket;
    }

    /**
     * @return bool
     */
    public function isSendStorno()
    {
        return $this->sendStorno;
    }

    /**
     * @param bool $sendStorno
     */
    public function setSendStorno($sendStorno)
    {
        $this->sendStorno = $sendStorno;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $files
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @return bool
     */
    public function isSendPosInvoice()
    {
        return $this->sendPosInvoice;
    }

    /**
     * @param bool $sendPosInvoice
     */
    public function setSendPosInvoice($sendPosInvoice)
    {
        $this->sendPosInvoice = $sendPosInvoice;
    }

    /**
     * @return bool
     */
    public function isSendPosDelivery()
    {
        return $this->sendPosDelivery;
    }

    /**
     * @param bool $sendPosDelivery
     */
    public function setSendPosDelivery($sendPosDelivery)
    {
        $this->sendPosDelivery = $sendPosDelivery;
    }

    /**
     * @return bool
     */
    public function isSendPosLabel()
    {
        return $this->sendPosLabel;
    }

    /**
     * @param bool $sendPosLabel
     */
    public function setSendPosLabel($sendPosLabel)
    {
        $this->sendPosLabel = $sendPosLabel;
    }

    /**
     * @return bool
     */
    public function isSendPosOrder()
    {
        return $this->sendPosOrder;
    }

    /**
     * @param bool $sendPosOrder
     */
    public function setSendPosOrder($sendPosOrder)
    {
        $this->sendPosOrder = $sendPosOrder;
    }

    /**
     * @return bool
     */
    public function isSendPosOffer()
    {
        return $this->sendPosOffer;
    }

    /**
     * @param bool $sendPosOffer
     */
    public function setSendPosOffer($sendPosOffer)
    {
        $this->sendPosOffer = $sendPosOffer;
    }

    /**
     * @return bool
     */
    public function isSendPosStorno()
    {
        return $this->sendPosStorno;
    }

    /**
     * @param bool $sendPosStorno
     */
    public function setSendPosStorno($sendPosStorno)
    {
        $this->sendPosStorno = $sendPosStorno;
    }

    /**
     * @return bool
     */
    public function isSendPosJobticket()
    {
        return $this->sendPosJobticket;
    }

    /**
     * @param bool $sendPosJobticket
     */
    public function setSendPosJobticket($sendPosJobticket)
    {
        $this->sendPosJobticket = $sendPosJobticket;
    }
}
