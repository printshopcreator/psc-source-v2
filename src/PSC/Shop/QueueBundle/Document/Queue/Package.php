<?php

namespace PSC\Shop\QueueBundle\Document\Queue;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Package
{
    /**
     * @var int $mode
     *
     * @Field(type="int")
     */
    protected $mode;
/**
     * @var string $orderPath
     *
     * @Field(type="string")
     */
    protected $orderPath;
/**
     * @var string $designerPath
     *
     * @Field(type="string")
     */
    protected $designerPath;
/**
     * @var string $posPath
     *
     * @Field(type="string")
     */
    protected $posPath;
/**
     * @var array $orderFiles
     *
     * @Field(type="collection")
     */
    protected $orderFiles;
/**
     * @var array $posFiles
     *
     * @Field(type="collection")
     */
    protected $posFiles;
/**
     * @var boolean $orderJobticket
     *
     * @Field(type="bool")
     */
    protected $orderJobticket;
/**
     * @var boolean $orderInvoice
     *
     * @Field(type="bool")
     */
    protected $orderInvoice;
/**
     * @var boolean $orderDelivery
     *
     * @Field(type="bool")
     */
    protected $orderDelivery;
/**
     * @var boolean $orderOrder
     *
     * @Field(type="bool")
     */
    protected $orderOrder;
/**
     * @var boolean $orderStorno
     *
     * @Field(type="bool")
     */
    protected $orderStorno;
/**
     * @var boolean $orderLabel
     *
     * @Field(type="bool")
     */
    protected $orderLabel;
/**
     * @var boolean $orderOffer
     *
     * @Field(type="bool")
     */
    protected $orderOffer;
/**
     * @var boolean $orderJobticketPP
     *
     * @Field(type="bool")
     */
    protected $orderJobticketPP;
/**
     * @var boolean $posJobticket
     *
     * @Field(type="bool")
     */
    protected $posJobticket;
/**
     * @var boolean $posInvoice
     *
     * @Field(type="bool")
     */
    protected $posInvoice;
/**
     * @var boolean $posDelivery
     *
     * @Field(type="bool")
     */
    protected $posDelivery;
/**
     * @var boolean $posOrder
     *
     * @Field(type="bool")
     */
    protected $posOrder;
/**
     * @var boolean $posStorno
     *
     * @Field(type="bool")
     */
    protected $posStorno;
/**
     * @var boolean $posLabel
     *
     * @Field(type="bool")
     */
    protected $posLabel;
/**
     * @var boolean $posOffer
     *
     * @Field(type="bool")
     */
    protected $posOffer;
/**
     * @var boolean $posJobticketPP
     *
     * @Field(type="bool")
     */
    protected $posJobticketPP;
/**
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @return string
     */
    public function getOrderPath()
    {
        return $this->orderPath;
    }

    /**
     * @param string $orderPath
     */
    public function setOrderPath($orderPath)
    {
        $this->orderPath = $orderPath;
    }

    /**
     * @return array
     */
    public function getOrderFiles()
    {
        return $this->orderFiles;
    }

    /**
     * @param array $orderFiles
     */
    public function setOrderFiles($orderFiles)
    {
        $this->orderFiles = $orderFiles;
    }

    /**
     * @return string
     */
    public function getPosPath()
    {
        return $this->posPath;
    }

    /**
     * @param string $posPath
     */
    public function setPosPath($posPath)
    {
        $this->posPath = $posPath;
    }

    /**
     * @return array
     */
    public function getPosFiles()
    {
        return $this->posFiles;
    }

    /**
     * @param array $posFiles
     */
    public function setPosFiles($posFiles)
    {
        $this->posFiles = $posFiles;
    }

    /**
     * @return bool
     */
    public function isOrderJobticket()
    {
        return $this->orderJobticket;
    }

    /**
     * @param bool $orderJobticket
     */
    public function setOrderJobticket($orderJobticket)
    {
        $this->orderJobticket = $orderJobticket;
    }

    /**
     * @return bool
     */
    public function isOrderInvoice()
    {
        return $this->orderInvoice;
    }

    /**
     * @param bool $orderInvoice
     */
    public function setOrderInvoice($orderInvoice)
    {
        $this->orderInvoice = $orderInvoice;
    }

    /**
     * @return bool
     */
    public function isOrderDelivery()
    {
        return $this->orderDelivery;
    }

    /**
     * @param bool $orderDelivery
     */
    public function setOrderDelivery($orderDelivery)
    {
        $this->orderDelivery = $orderDelivery;
    }

    /**
     * @return bool
     */
    public function isOrderOrder()
    {
        return $this->orderOrder;
    }

    /**
     * @param bool $orderOrder
     */
    public function setOrderOrder($orderOrder)
    {
        $this->orderOrder = $orderOrder;
    }

    /**
     * @return bool
     */
    public function isOrderStorno()
    {
        return $this->orderStorno;
    }

    /**
     * @param bool $orderStorno
     */
    public function setOrderStorno($orderStorno)
    {
        $this->orderStorno = $orderStorno;
    }

    /**
     * @return bool
     */
    public function isOrderLabel()
    {
        return $this->orderLabel;
    }

    /**
     * @param bool $orderLabel
     */
    public function setOrderLabel($orderLabel)
    {
        $this->orderLabel = $orderLabel;
    }

    /**
     * @return bool
     */
    public function isOrderOffer()
    {
        return $this->orderOffer;
    }

    /**
     * @param bool $orderOffer
     */
    public function setOrderOffer($orderOffer)
    {
        $this->orderOffer = $orderOffer;
    }

    /**
     * @return bool
     */
    public function isOrderJobticketPP()
    {
        return $this->orderJobticketPP;
    }

    /**
     * @param bool $orderJobticketPP
     */
    public function setOrderJobticketPP($orderJobticketPP)
    {
        $this->orderJobticketPP = $orderJobticketPP;
    }

    /**
     * @return bool
     */
    public function isPosJobticket()
    {
        return $this->posJobticket;
    }

    /**
     * @param bool $posJobticket
     */
    public function setPosJobticket($posJobticket)
    {
        $this->posJobticket = $posJobticket;
    }

    /**
     * @return bool
     */
    public function isPosInvoice()
    {
        return $this->posInvoice;
    }

    /**
     * @param bool $posInvoice
     */
    public function setPosInvoice($posInvoice)
    {
        $this->posInvoice = $posInvoice;
    }

    /**
     * @return bool
     */
    public function isPosDelivery()
    {
        return $this->posDelivery;
    }

    /**
     * @param bool $posDelivery
     */
    public function setPosDelivery($posDelivery)
    {
        $this->posDelivery = $posDelivery;
    }

    /**
     * @return bool
     */
    public function isPosOrder()
    {
        return $this->posOrder;
    }

    /**
     * @param bool $posOrder
     */
    public function setPosOrder($posOrder)
    {
        $this->posOrder = $posOrder;
    }

    /**
     * @return bool
     */
    public function isPosStorno()
    {
        return $this->posStorno;
    }

    /**
     * @param bool $posStorno
     */
    public function setPosStorno($posStorno)
    {
        $this->posStorno = $posStorno;
    }

    /**
     * @return bool
     */
    public function isPosLabel()
    {
        return $this->posLabel;
    }

    /**
     * @param bool $posLabel
     */
    public function setPosLabel($posLabel)
    {
        $this->posLabel = $posLabel;
    }

    /**
     * @return bool
     */
    public function isPosOffer()
    {
        return $this->posOffer;
    }

    /**
     * @param bool $posOffer
     */
    public function setPosOffer($posOffer)
    {
        $this->posOffer = $posOffer;
    }

    /**
     * @return bool
     */
    public function isPosJobticketPP()
    {
        return $this->posJobticketPP;
    }

    /**
     * @param bool $posJobticketPP
     */
    public function setPosJobticketPP($posJobticketPP)
    {
        $this->posJobticketPP = $posJobticketPP;
    }

    /**
     * @return string
     */
    public function getDesignerPath()
    {
        return $this->designerPath;
    }

    /**
     * @param string $designerPath
     */
    public function setDesignerPath($designerPath)
    {
        $this->designerPath = $designerPath;
    }
}
