<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\QueueBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use MongoDB\BSON\ObjectId;
use PSC\Shop\EntityBundle\Document\JobLog;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Registry;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * QueueController fürs Backend
 *
 * @package    PSC\Shop\Queue
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/list/index", name="psc_shop_queue_backend_list_index")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(
        Request $request,
        Shop $shopService,
        DocumentManager $documentManager,
        Manager $eventManager,
        EntityManagerInterface $entityManager,
        \PSC\Shop\QueueBundle\Service\Event\Registry $eventRegistry,
        \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry,
        PaginatorInterface $paginator
    ) {

        $selectedShop = $shopService->getSelectedShop();
        $qb = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Queue')
            ->findBy(array('shop' => (string)$selectedShop->getUid()));
        $pagination = $paginator->paginate($qb, $request->query->getInt('page', 1), 15);
        $jobs = [];
        foreach ($eventManager->getAll() as $job) {
            $logs = $documentManager->getRepository(JobLog::class)->findBy(['job' => $job->getId()]);
            if ($job->getShop()) {
                $shop = $shopService->getShopByUid($job->getShop());
            } else {
                $shop = new \PSC\Shop\EntityBundle\Entity\Shop();
                $shop->setTitle("System");
            }

            $logsTemp = [];
            foreach ($logs as $log) {
                $queue = $documentManager->getRepository(Queue::class)->findOneBy(['_id' => new ObjectId($log->getQueue())]);
                $logsTemp[] = [
                'log' => $log,
                'queue' => $queue,
                ];
            }

            $jobs[] = [
                'job' => $job,
                'shop' => $shop,
                'logs' => $logsTemp,
            ];
        }

        return array(
            'pagination' => $pagination,
            'jobs' => $jobs,
            'eventRegistry' => $eventRegistry,
            'queueRegistry' => $queueRegistry,
            'queueTypes' => $queueRegistry->getAllByGroup()
        );
    }

    /**
     * Reset JobQueue
     *
     * @Route("/list/reset/jobqueue/{uuid}", name="psc_shop_queue_backend_list_reset_jobqueue")
     *
     * @param $uuid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function resetAction(DocumentManager $documentManager, $uuid)
    {

        $jobLog = $documentManager
            ->getRepository(JobLog::class)
            ->findOneBy(['_id' => new ObjectId($uuid)]);
        $jobLog->setCount(0);
        $documentManager->persist($jobLog);
        $documentManager->flush();
        return $this->redirectToRoute('psc_shop_queue_backend_list_index');
    }

    /**
     * Default Seite
     *
     * @Route("/list/delete/{uuid}", name="psc_shop_queue_backend_list_delete")
     *
     * @param $uuid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Shop $shopService, DocumentManager $documentManager, $uuid)
    {

        $selectedShop = $shopService->getSelectedShop();
        $job = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Job')
            ->findOneBy(['_id' => new ObjectId($uuid)]);
        $documentManager->remove($job);
        $documentManager->flush();
        return $this->redirectToRoute('psc_shop_queue_backend_list_index');
    }
}
