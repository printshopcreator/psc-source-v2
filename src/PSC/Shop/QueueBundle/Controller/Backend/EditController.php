<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\QueueBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\QueueBundle\Event\Registry;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\Url;
use PSC\System\SettingsBundle\Form\Backend\CopyType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * EditController fürs Backend
 *
 * @package    PSC\Shop\Queue
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @Route("/{type}/create", name="psc_shop_queue_backend_edit_create")
     * @Security("is_granted('ROLE_SHOP')")
     * @Template()
     * @param Request $request
     * @param Shop $shopService
     * @param \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry
     * @param \PSC\Shop\QueueBundle\Service\Event\Registry $eventRegistry
     * @param DocumentManager $documentManager
     * @param         $type
     *
     * @return array|Response|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(
        Request $request,
        Shop $shopService,
        \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry,
        \PSC\Shop\QueueBundle\Service\Event\Registry $eventRegistry,
        DocumentManager $documentManager,
        $type
    ) {
        $selectedShop = $shopService->getSelectedShop();
        if (!$queueRegistry->has($type)) {
            throw new NotFoundHttpException('Exporter not found');
        }

        $queue = $queueRegistry->get($type);
        $builder  = $this->createFormBuilder(null, array('action' => $this->generateUrl('psc_shop_queue_backend_edit_create', array('type' => $queue->getType()))));
        $builder->add('name', TextType::class, array('label' => 'Name'));
        $builder->add('pos', NumberType::class, array('label' => 'Position'));
        $builder->add('active', CheckboxType::class, array('label' => 'Aktiv', 'required' => false));
        $builder->add('eventType', ChoiceType::class, array('label' => 'Event','choices'  => $eventRegistry->getAllForChoise()));
        if ($queue instanceof ConfigurableElementInterface) {
            $queue->getForm($builder, true, $eventRegistry->getFirst());
        }

        $builder->add('submit', SubmitType::class, array('label' => 'Speichern'));
        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $docQueue = new Queue();
            $docQueue->setName($form->get('name')->getData());
            $docQueue->setPos($form->get('pos')->getData());
            $docQueue->setQueueType($queue->getType());
            $docQueue->setEventType($form->get('eventType')->getData());
            $docQueue->setActive($form->get('active')->getData());
            $docQueue->setShop($selectedShop->getUid());
            $objEvent = $eventRegistry->get($form->get('eventType')->getData());
            $objEvent->injectCreateDocument($form, $docQueue);
            if ($queue instanceof ConfigurableElementInterface) {
                $queue->injectDocument($form, $objEvent, $docQueue);
            }

            $documentManager->persist($docQueue);
            $documentManager->flush();
            return $this->redirectToRoute('psc_shop_queue_backend_list_index');
        }


        return array('queue' => $queue, 'help' => [], 'form' => $form->createView());
    }

    /**
     * @Route("/{uuid}/edit", name="psc_shop_queue_backend_edit_edit")
     * @Security("is_granted('ROLE_SHOP')")
     * @Template()
     * @param Request $request
     * @param Shop $shopService
     * @param \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry
     * @param \PSC\Shop\QueueBundle\Service\Event\Registry $eventRegistry
     * @param DocumentManager $documentManager
     * @param         $uuid
     *
     * @return array|Response|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(
        Request $request,
        Shop $shopService,
        \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry,
        \PSC\Shop\QueueBundle\Service\Event\Registry $eventRegistry,
        DocumentManager $documentManager,
        $uuid
    ) {
        $selectedShop = $shopService->getSelectedShop();
/** @var \PSC\Shop\EntityBundle\Document\Queue $objQueue */
        $objQueue = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Queue')
            ->findOneBy(array('id' => (string)$uuid));
        $queue = $queueRegistry->get($objQueue->getQueueType());
        $builder  = $this->createFormBuilder(null, array('action' => $this->generateUrl('psc_shop_queue_backend_edit_edit', array('uuid' => $uuid))));
        $builder->add('name', TextType::class, array('label' => 'Name'));
        $builder->add('pos', NumberType::class, array('label' => 'Position'));
        $builder->add('active', CheckboxType::class, array('label' => 'Aktiv', 'required' => false));
        $builder->add('eventType', ChoiceType::class, array('label' => 'Event','choices'  => $eventRegistry->getAllForChoise()));
        $objEvent = $eventRegistry->get($objQueue->getEventType());
        if ($queue instanceof ConfigurableElementInterface) {
            $queue->getForm($builder, false, $objEvent);
        }

        if ($objEvent instanceof \PSC\Shop\QueueBundle\Event\ConfigurableElementInterface) {
            $objEvent->getForm($builder, array());
        }

        $builder->add('submit', SubmitType::class, array('label' => 'Speichern'));
        $form = $builder->getForm();
        $form->setData(array(
            'name' => $objQueue->getName(),
            'pos' => $objQueue->getPos(),
            'active' => $objQueue->isActive(),
            'queueType' => $objQueue->getQueueType(),
            'eventType' => $objQueue->getEventType()
        ));
        if ($queue instanceof ConfigurableElementInterface) {
            $queue->setFormData($form, $objEvent, $objQueue);
        }

        if ($objEvent instanceof \PSC\Shop\QueueBundle\Event\ConfigurableElementInterface) {
            $objEvent->setFormData($form, $objQueue);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $objQueue->setName($form->get('name')->getData());
            $objQueue->setPos($form->get('pos')->getData());
            $objQueue->setActive($form->get('active')->getData());
            $objQueue->setShop($selectedShop->getUid());
            $objQueue->setEventType($form->get('eventType')->getData());
            if ($queue instanceof ConfigurableElementInterface) {
                $queue->injectDocument($form, $objEvent, $objQueue);
            }

            if ($objEvent instanceof \PSC\Shop\QueueBundle\Event\ConfigurableElementInterface) {
                $objEvent->injectUpdateDocument($form, $objQueue);
            }

            $documentManager->persist($objQueue);
            $documentManager->flush();
        }

        return array('queue' => $queue , 'help' => $objEvent->getHelp(), 'form' => $form->createView(), 'event' => $objEvent);
    }

    /**
     * @Route("/{uuid}/delete", name="psc_shop_queue_backend_edit_delete")
     * @Security("is_granted('ROLE_SHOP')")
     * @Template()
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $uuid
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(Request $request, DocumentManager $documentManager, $uuid)
    {
        $objQueue = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Queue')
            ->findOneBy(array('id' => (string)$uuid));
        $documentManager->remove($objQueue);
        $documentManager->flush();
        return $this->redirect($this->generateUrl("psc_shop_queue_backend_list_index"));
    }

    /**
     * Copy Seite
     *
     * @Route("/{uuid}/copy", name="psc_shop_queue_backend_edit_copy")
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @Template()
     */
    public function copyAction(Request $request, DocumentManager $documentManager, $uuid)
    {

        $obj = ['which' => 1];
/** @var Queue $objQueue */
        $objQueue = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Queue')
            ->findOneBy(array('id' => (string)$uuid));
        $form = $this->createForm(CopyType::class, $obj);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            if ($data['which'] == 1) {
                $f = clone $objQueue;
                $f->setId(null);
                $f->setName($f->getName() . ' Kopie');
                $documentManager->detach($f);
                $documentManager->persist($f);
                $documentManager->flush();
                return $this->redirectToRoute('psc_shop_queue_backend_list_index');
            } elseif ($data['which'] == 2) {
                $f = clone $objQueue;
                $f->setId(null);
                $f->setShop($data['shops']->getUid());
                $documentManager->detach($f);
                $documentManager->persist($f);
                $documentManager->flush();
                return $this->redirectToRoute('psc_shop_queue_backend_list_index');
            }
        }

        return array('form' => $form->createView(), 'queue' => $objQueue);
    }
}
