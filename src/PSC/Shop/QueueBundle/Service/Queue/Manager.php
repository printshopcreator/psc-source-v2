<?php

namespace PSC\Shop\QueueBundle\Service\Queue;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Document\Job;
use PSC\Shop\EntityBundle\Document\JobLog;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\QueueBundle\Event\Event;
use PSC\Shop\QueueBundle\Event\EventInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * QueueManager
 *
 * @author Thomas Peterson
 */
class Manager
{
    protected $documentManager;
    protected $registry;

    public function __construct(DocumentManager $dm, Registry $registry)
    {
        $this->documentManager = $dm;
        $this->registry = $registry;
    }

    public function getQueueByEventType($key)
    {
        return $this->documentManager
            ->getRepository('PSCEntityBundle:Queue')
            ->findOneBy(['eventType' => $key]);
    }

    public function getQueueByEventTypeAndShop($key, $shop)
    {

        return $this->documentManager
            ->getRepository('PSCEntityBundle:Queue')
            ->findOneBy(['eventType' => $key, 'shop' => (string)$shop]);
    }

    public function getErrorJobCount()
    {
        return count($this->documentManager->getRepository(JobLog::class)->findBy([
            'count' => 2
        ]));
    }
}
