<?php

namespace PSC\Shop\QueueBundle\Service\Event;

use LogicException;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryInterface;
use PSC\Shop\QueueBundle\Event\EventInterface;

class Registry implements RegistryInterface
{
    /** @var  EventInterface[] */
    private $events = array();
/** @return EventInterface[] */
    public function all()
    {
        return $this->events;
    }

    /**
     * @param $key string
     *
     * @return EventInterface
     */
    public function get($key)
    {
        if (!$this->has($key)) {
            throw new LogicException('Event ' . $key . ' not registered');
        }

        return $this->events[$key];
    }

    /**
     *
     * @return EventInterface
     */
    public function getFirst()
    {
        return array_shift($this->events);
    }

    public function has($type)
    {
        return array_key_exists($type, $this->events);
    }

    public function add($element)
    {
        if ($this->has($element->getType())) {
            throw new LogicException('Event ' . $element->getType() . ' already registered');
        }

        $this->events[$element->getType()] = $element;
    }

    /**
     * @param RegistryElementInterface $element
     */
    public function remove(RegistryElementInterface $element)
    {
        if (!$this->has($element->getType())) {
            throw new LogicException('Event ' . $element->getType() . ' not registered');
        }

        unset($this->events[$element->getType()]);
    }

    public function getAllForChoise()
    {
        $temp = array();
        foreach ($this->events as $event) {
            $temp[$event->getDescription()] = $event->getType();
        }

        return $temp;
    }
}
