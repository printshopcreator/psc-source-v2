<?php

namespace PSC\Shop\QueueBundle\Service\Event;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\EntityBundle\Document\Job;
use PSC\Shop\EntityBundle\Document\JobLog;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\QueueBundle\Event\Event;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Service\Queue\Registry;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Manager
 *
 * @author Thomas Peterson
 */
class Manager
{
    protected $entityManager;
    protected $registry;

    public function __construct(DocumentManager $dm, Registry $registry)
    {
        $this->entityManager = $dm;
        $this->registry = $registry;
    }

    public function getAll()
    {
        return $this->entityManager
            ->getRepository('PSCEntityBundle:Job')
            ->findAll();
    }

    public function getJobByEvent($key)
    {
        return $this->entityManager
            ->getRepository('PSCEntityBundle:Job')
            ->findOneBy(['event' => $key]);
    }

    public function addJob(Event $event)
    {
        $job = new Job();
        $job->setShop($event->getShop());
        $job->setData($event->getData());

        $job->setEvent($event->getType());

        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }

    public function removeJob($job)
    {
        $this->removeLogs($job);
        $this->entityManager->remove($job);
        $this->entityManager->flush();
    }

    public function updateJob($job)
    {
        $job->setUpdated(new \DateTime());
        $this->entityManager->persist($job);
        $this->entityManager->flush();
    }

    protected function removeLogs($job)
    {
        $qb = $this->entityManager->createQueryBuilder(JobLog::class);
        $qb->remove()
            ->field('job')->equals($job->getId())
            ->getQuery()
            ->execute();
    }
}
