<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\QueueBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * ShopQueueBundle
 *
 * @package PSC\Shop\Queue
 */
class PSCShopQueueBundle extends Bundle
{
}
