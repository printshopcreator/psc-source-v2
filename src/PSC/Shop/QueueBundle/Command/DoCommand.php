<?php

namespace PSC\Shop\QueueBundle\Command;

use App\Kernel;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\Shop\EntityBundle\Document\Job;
use PSC\Shop\EntityBundle\Document\JobLog;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\Shop\QueueBundle\Service\Event\Registry;
use PSC\Shop\QueueBundle\Teams\Card\Error;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use PSC\System\PluginBundle\Event\ClearCache;
use PSC\System\PluginBundle\Event\EveryRun;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\Log;
use Sebbmyr\Teams\TeamsConnector;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\KernelInterface;

class DoCommand extends Command
{
    protected static $defaultName = 'application:queue:do';
    /**
     * @var Manager
     */
    private Manager $eventManager;
    /**
     * @var Registry
     */
    private Registry $eventRegistry;
    private \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry;
    /**
     * @var DocumentManager
     */
    private DocumentManager $mongoDb;
    /**
     * @var KernelInterface
     */
    private KernelInterface $kernel;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;
    /**
     * @var Log
     */
    private Log $logService;

    public function __construct(EntityManagerInterface $entityManager, Manager $eventManager, Registry $eventRegistry, \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry, DocumentManager $documentManager, KernelInterface $kernel, Log $logService)
    {
        $this->eventManager = $eventManager;
        $this->eventRegistry = $eventRegistry;
        $this->queueRegistry = $queueRegistry;
        $this->mongoDb = $documentManager;
        $this->kernel = $kernel;
        $this->entityManager = $entityManager;
        $this->logService = $logService;

        // you *must* call the parent constructor
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Run the Jobs')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queues = $this->mongoDb
            ->getRepository('PSC\Shop\EntityBundle\Document\Queue')
            ->findBy(array('eventType' => 'system_plugin_every_run', 'active' => true), [
                'pos' => 'ASC'
            ]);

        /** @var Queue $queue */
        foreach ($queues as $queue) {
            $queueObj = $this->queueRegistry->get($queue->getQueueType());
            try {
                $success = $queueObj->execute(new EveryRun(), $queue);
            } catch (\Exception $exception) {
                $this->logService->createLogEntry(new Shop(), LogEntry::ERROR, "Cron", "", $exception->getMessage(), []);
            }
        }

        /** @var Job $job */
        foreach ($this->eventManager->getAll() as $job) {
            $canBeRemoved = true;

            $event = $this->eventRegistry->get($job->getEvent());

            if ($event instanceof ClearCache) {
                $fs = new Filesystem();
                $fs->remove($this->kernel->getCacheDir());
            } else {
                $event->setData($job->getData());
                $event->setShop($job->getShop());

                $queues = $this->mongoDb
                    ->getRepository('PSCEntityBundle:Queue')
                    ->findBy(array(
                        'shop' => $job->getShop(),
                        'active' => true,
                        'eventType' => $job->getEvent()
                    ), [
                        'pos' => 'ASC'
                    ]);


                /** @var Queue $queue */
                foreach ($queues as $queue) {
                    if (!$event->doProcess($queue)) {
                        continue;
                    }

                    $queueObj = $this->queueRegistry->get($queue->getQueueType());

                    /** @var JobLog $jobLog */
                    $jobLog = $this->mongoDb->getRepository(JobLog::class)->findOneBy([
                        'job' => $job->getId(),
                        'queue' => $queue->getId()
                    ]);

                    if ($jobLog) {
                        if ($jobLog->getError() == "") {
                            continue;
                        }
                        $jobLog->incCount();

                        if ($jobLog->getCount() > 2 && $jobLog->getError() != "") {
                            $canBeRemoved = false;
                            continue;
                        }
                    } else {
                        $jobLog = new JobLog();
                        $jobLog->setJob($job->getId());
                        $jobLog->setQueue($queue->getId());
                    }
                    $jobLog->setUpdated(new \DateTime());
                    try {
                        $success = $queueObj->execute($event, $queue);

                        if (!$success) {
                            $canBeRemoved = false;
                            $jobLog->setError($queueObj->getError());
                        } else {
                            $jobLog->setError("");
                        }
                        $this->mongoDb->persist($jobLog);
                        $this->mongoDb->flush();
                    } catch (\Exception $e) {
                        $jobLog->setError($e->getMessage());
                        $this->mongoDb->persist($jobLog);
                        $this->mongoDb->flush();

                        $canBeRemoved = false;
                    }
                }
            }

            if ($canBeRemoved) {
                $this->eventManager->removeJob($job);
            }
        }

        if (count($this->eventManager->getAll()) == 0) {
            $this->mongoDb->createQueryBuilder(JobLog::class)->remove()->getQuery()->execute();
        }


        $output->writeln("Successfully");
        return 0;
    }
}
