<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\CmsBundle\Form\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\ThemeBundle\Core\ThemeHolder;
use PSC\Shop\ThemeBundle\Core\ThemeHolderInterface;
use PSC\Shop\ThemeBundle\Core\ThemeSource;
use PSC\Shop\ThemeBundle\Core\ThemeSourceInterface;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Language;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CmsType extends AbstractType
{
    protected $shop = null;
/** @var Field  */
    protected $fields;
/** @var ThemeSourceInterface  */
    protected $themeSource;
    protected $formFactory;
/** @var Language */
    public $language;
    public function __construct(Field $fields, Shop $shop, ThemeSource $themeSource, FormFactoryInterface $formFactory, Language $language, EntityManagerInterface $entityManager)
    {
        $this->shop = $shop;
        $this->fields = $fields;
        $this->themeSource = $themeSource;
        $this->language = $language;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
    }

    private function buildCmsTree(array $tmp = [], Cms $cms = null, $level = ''): array
    {

        if ($cms) {
            $sites = $this->entityManager->getRepository(Cms::class)->findBy(['shop' => $this->shop->getSelectedShop()->getUid(), 'parent' => $cms]);
        } else {
            $sites = $this->entityManager->getRepository(Cms::class)->findBy(['shop' => $this->shop->getSelectedShop()->getUid(), 'parent' => [0,null]]);
        }

        /** @var Cms $site */
        foreach ($sites as $site) {
            $tmp[$level . $site->getTitle()] = $site->getUid();
            $tmp = $this->buildCmsTree($tmp, $site, $level . '-');
        }

        return $tmp;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $tmp = $this->buildCmsTree(['Oberste Ebene' => 0]);
        $builder
            ->add('title', TextType::class, ['label' => 'psc_shop_cms.Name', 'required' => false])
            ->add('parentRaw', ChoiceType::class, array(
                'choices' => $tmp,
                'required' => true,
                'label' => 'psc_shop_cms.Parent',

            ))
            ->add('menu', TextType::class, ['label' => 'psc_shop_cms.MenuName', 'required' => true])
            ->add('private', CheckboxType::class, ['label' => 'psc_shop_cms.Private', 'required' => false])
            ->add('displayOnlyWhenLoggedIn', CheckboxType::class, ['label' => 'psc_shop_cms.onlyLoggedIn', 'required' => false])
            ->add('enable', CheckboxType::class, ['label' => 'psc_shop_cms.Active', 'required' => false])
            ->add('notInMenu', CheckboxType::class, ['label' => 'psc_shop_cms.NotInMenu', 'required' => false])
            ->add('displayTitle', CheckboxType::class, ['label' => 'psc_shop_cms.DisplayTitle', 'required' => false])
            ->add('sort', IntegerType::class, ['label' => 'psc_shop_cms.Sort', 'required' => false])
            ->add('url', TextType::class, ['label' => 'psc_shop_cms.Url', 'required' => true])
            ->add('pos', ChoiceType::class, ['label' => 'psc_shop_cms.Region',
                'required' => true,
                'choice_loader' => new CallbackChoiceLoader(function () {

                        return $this->themeSource->getTheme($this->shop->getMongoSelectedShop()->getTheme())->getConfig()->getCms()->getPositionsAsArray();
                })
            ])
            ->add('text', TextareaType::class, array(
                'label' => 'psc_shop_cms.Text',
                'required' => false,
            ))
            ->add('metaAuthor', TextType::class, ['label' => 'psc_shop_cms.MetaAuthor', 'required' => false])
            ->add('metaKeywords', TextAreaType::class, ['label' => 'psc_shop_cms.MetaKeywords', 'required' => false])
            ->add('metaDescription', TextareaType::class, ['label' => 'psc_shop_cms.MetaDescriptions', 'required' => false])
            ->add('metaCustomTitle', TextType::class, ['label' => 'psc_shop_cms.MetaCustomTitle', 'required' => false])
            ->add('metaOgTitle', TextType::class, ['label' => 'psc_shop_cms.OGTitle', 'required' => false])
            ->add('metaOgType', TextType::class, ['label' => 'psc_shop_cms.OGType', 'required' => false])
            ->add('metaOgUrl', TextType::class, ['label' => 'psc_shop_cms.OGType', 'required' => false])
            ->add('metaOgImage', TextType::class, ['label' => 'psc_shop_cms.OGImage', 'required' => false])
            ->add('extraSettings', TextareaType::class, array('required' => false, 'label' => 'psc_shop_cms.ExtendedSettings'))
            ->add('language', ChoiceType::class, array(
                'label' => 'psc_shop_cms.Language',
                'choices' => $this->language->getLanguageArrayForContent(),
                'required' => true,
            ))
            ->add('metaOgDescription', TextAreaType::class, ['label' => 'psc_shop_cms.OGDescription', 'required' => false]);
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup(), FormType::class, null, ['mapped' => false]), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'psc_shop_cms.Save'));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms) as $field) {
                $field->formPreSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms) as $field) {
                $field->formPostSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms) as $field) {
                $field->formPostSubmit($event);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms) as $field) {
                $field->formPreSubmit($event);
            }
        });
    }

    public function getName()
    {
        return 'cms';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Cms',
        ));
    }
}
