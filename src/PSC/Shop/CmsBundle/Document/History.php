<?php

namespace PSC\Shop\CmsBundle\Document;

use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="cmsObj_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $cmsObj;

    function __construct(string $cmsObj = "")
    {
        parent::__construct();
        $this->cmsObj = $cmsObj;
    }

    public function getCmsObj()
    {
        return $this->cmsObj;
    }

    public function setCmsObj(string $cmsObj)
    {
        $this->cmsObj = $cmsObj;
    }

    public function getReferenceField(): string
    {
        return 'cmsObj';
    }
}
