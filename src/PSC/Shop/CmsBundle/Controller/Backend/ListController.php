<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\CmsBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\CmsBundle\Form\Backend\SearchType;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\ThemeBundle\Core\ThemeSource;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * CmsController fürs Backend
 *
 * @package    PSC\Shop\Cms
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/list", name="psc_shop_cms_backend_list")
     * @Template()
     *
     * @param Request $request
     * @return array|View
     */
    public function indexAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, FilterBuilderUpdaterInterface $filterBuilderUpdater, PaginatorInterface $paginator, ThemeSource $sourceService)
    {

        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Cms');
        $qb = $userRepository->createQueryBuilder('cms')
            ->andWhere('cms.shop = :shop_id')
            ->setParameter("shop_id", $selectedShop->getUid());
        $form = $this->createForm(SearchType::class);
        if ($request->isMethod('POST')) {
            $form->handleRequest($request);
        }

        $filterBuilderUpdater->addFilterConditions($form, $qb);
        $pagination = $paginator->paginate($query = $qb->getQuery(), $request->query->getInt('page', 1), 15);
        return array(
            'pagination' => $pagination,
            'form' => $form->createView(),
            'theme' => $sourceService->getTheme($shopService->getMongoSelectedShop()->getTheme())
        );
    }
}
