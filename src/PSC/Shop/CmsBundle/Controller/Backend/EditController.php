<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\CmsBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\AccountBundle\PSCShopAccountBundle;
use PSC\Shop\CmsBundle\Document\History as PSCHistory;
use PSC\Shop\CmsBundle\Form\Backend\CmsType;
use PSC\Shop\CmsBundle\PSCShopCmsBundle;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\ShippingBundle\Form\Backend\DeleteType;
use PSC\Shop\ThemeBundle\Core\Config\Cms\Position;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use PSC\Shop\SettingsBundle\Service\Routing;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * EditController fürs Backend
 *
 * @package    PSC\Shop\Cms
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;

    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }

    /**
     * Create Seite
     *
     * @Route("/create", name="psc_shop_cms_backend_edit_create")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param Routing $routing
     * @param Shop $shopService
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(
        Request $request,
        Field $fieldService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        Routing $routing,
        Shop $shopService
    ) {

        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms);

        $selectedShop = $shopService->getSelectedShop();

        $cms = new Cms();
        $cms->setDisplayTitle(true);
        $cms->setEnable(true);
        $form = $this->createForm(CmsType::class, $cms);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($cms->getPos() instanceof Position) {
                $cms->setPos($cms->getPos()->getId());
            }
            $cms->setShop($selectedShop);
            $entityManager->persist($cms);
            $entityManager->flush();

            $cmsDoc = new \PSC\Shop\EntityBundle\Document\Cms();
            $cmsDoc->setUid($cms->getUid());
            $cmsDoc->setExtraSettings($cms->getExtraSettings());
            $cmsDoc->setPluginSettings($cms->getPluginSettings());

            $documentManager->persist($cmsDoc);
            $documentManager->flush();

            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopCmsBundle::class, $cms->getTitle(), "CMS Site saved");
            return $this->redirectToRoute('psc_shop_cms_backend_list');
        }

        return array(
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups
        );
    }

    /**
     * Edit Seite
     *
     * @Route("/edit/{uid}", name="psc_shop_cms_backend_edit_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param Routing $routing
     * @param $uid
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function editAction(
        Request $request,
        Field $fieldService,
        Shop $shopService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        Routing $routing,
        $uid
    ) {

        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Cms);

        $selectedShop = $shopService->getSelectedShop();

        /** @var Cms $cms */
        $cms = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Cms')->findOneBy(array('shop' => $selectedShop, 'uid' => $uid));

        /** @var \PSC\Shop\EntityBundle\Document\Cms $cmsDoc */
        $cmsDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Cms')
            ->findOneBy(array('uid' => (string) $cms->getUid()));

        if (!$request->isMethod('POST') && $cmsDoc) {
            $cms->setExtraSettings($cmsDoc->getExtraSettings());
            $cms->setPluginSettings($cmsDoc->getPluginSettings());
        } elseif (!$cmsDoc) {
            $cmsDoc = new \PSC\Shop\EntityBundle\Document\Cms();
            $cmsDoc->setUid($cms->getUid());
            $cmsDoc->setExtraSettings($cms->getExtraSettings());

            $documentManager->persist($cmsDoc);
            $documentManager->flush();
        }

        $form = $this->createForm(CmsType::class, $cms);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($cms->getPos() instanceof Position) {
                $cms->setPos($cms->getPos()->getId());
            }
            $cmsDoc->setPluginSettings($cms->getPluginSettings());
            $cmsDoc->setExtraSettings($cms->getExtraSettings());

            $this->historyService->createHistoryEntry(new PSCHistory((string)$cms->getUid()), $cms, $cmsDoc);

            $entityManager->persist($cms);
            $entityManager->flush();

            $documentManager->persist($cmsDoc);
            $documentManager->flush();
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopCmsBundle::class, $cms->getTitle(), "CMS Site saved");
        }



        return array(
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$cms->getUid())
        );
    }

    /**
     * Default Seite
     *
     * @Route("/delete/{uid}", name="psc_shop_cms_backend_edit_delete")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param string $uid
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(
        Request $request,
        EntityManagerInterface $entityManager,
        SessionInterface $session,
        Shop $shopService,
        $uid
    ) {

        $cms = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Cms')->findOneBy(['uid' => $uid]);
        $selectedShop = $shopService->getSelectedShop();

        $form = $this->createForm(DeleteType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $title = $cms->getTitle();
                $entityManager->remove($cms);
                $entityManager->flush();
                $session->getFlashBag()->add(
                    'success',
                    'Site \'' . $title . '\' has been deleted!'
                );
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopCmsBundle::class, $title, "CMS Site deleted");
                return $this->redirectToRoute('psc_shop_cms_backend_list');
            }
            return $this->redirectToRoute('psc_shop_cms_backend_list');
        }

        return array(
            'cms' => $cms,
            'form' => $form->createView()
        );
    }
}
