<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\CmsBundle\Controller\Frontend;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * CmsController fürs Frontend
 *
 * @package    PSC\Shop\Cms
 * @subpackage Controller
 */
class ShowController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/{url}", name="psc_shop_cms_frontend_show")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $url
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, $url)
    {

        $selectedShop = $shopService->getSelectedShop();
        $cms = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Cms')->findOneBy([
            'shop' => $selectedShop,
            'url' => $url,
            'enable' => true
        ]);
        return array(
            'cms' => $cms
        );
    }
}
