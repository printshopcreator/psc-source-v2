<?php

namespace PSC\Shop\CmsBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $submenu = $menu->addChild('CMS', array(
                'route' => 'psc_shop_cms_backend_list',
                'extras' => array(
                    'icon' => 'fas fa-file-alt',
                    'orderNumber' => 17
                )
            ));
        }
    }
}
