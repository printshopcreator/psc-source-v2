<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\AccountBundle\Controller\Backend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\AccountBundle\Form\Backend\SearchType;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * ListController fürs Backend
 *
 * @package    PSC\Shop\Account\Backend
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/list/index", name="psc_shop_account_backend_list")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @param FilterBuilderUpdaterInterface $filterBuilderUpdater
     * @param SessionInterface $session
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator, FilterBuilderUpdaterInterface $filterBuilderUpdater, SessionInterface $session)
    {

        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Account');
        $qb = $userRepository->createQueryBuilder('account')
            ->leftJoin('account.shops', 'shops')
            ->orderBy('account.uid', 'desc');
        $form = $this->createForm(SearchType::class);
        if ($request->isMethod('POST')) {
            $session->set('account_sort_page', 1);
            $form->handleRequest($request);
        }

        $filterBuilderUpdater->addFilterConditions($form, $qb);
        if ($request->get('direction', false)) {
            $session->set('account_sort_direction', $request->get('direction', 'DESC'));
            $session->set('account_sort_field', $request->get('sort', 'account.uid'));
        }
        if ($request->get('page', false)) {
            $session->set('account_sort_page', $request->get('page', 1));
        }

        $qb->andWhere('shops.uid = :shop_id')
        ->setParameter("shop_id", $selectedShop->getUid());
        $pagination = $paginator->paginate($query = $qb->getQuery(), $request->query->getInt('page', $session->get('account_sort_page', 1)), 15, [
                'defaultSortFieldName' => $session->get('account_sort_field', 'account.uid'),
                'defaultSortDirection' => $session->get('account_sort_direction', 'DESC')
            ]);
        return array(
            'pagination' => $pagination,
            'form' => $form->createView()
        );
    }
}
