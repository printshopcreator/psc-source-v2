<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\AccountBundle\Controller\Backend;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\AccountBundle\Document\History as PSCHistory;
use PSC\Shop\AccountBundle\Form\Backend\General\AccountType;
use PSC\Shop\AccountBundle\PSCShopAccountBundle;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * EditController fürs Backend
 *
 * @package    PSC\Shop\Account\Backend
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;
    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }

    /**
     * create Seite
     *
     * @Route("/edit/create", name="psc_shop_account_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param string $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(
        Request $request,
        Field $fieldService,
        Shop $shopService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        $uuid = ""
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Account);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Account);
        $selectedShop = $shopService->getSelectedShop();
/** @var Account $account */
        $account = new Account();
        $account->shops[] = $selectedShop;
/** @var \PSC\Shop\EntityBundle\Document\Account $accountDoc */
        $accountDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Account')
            ->findOneBy(array('uid' => (string)$account->getId()));
        if (!$request->isMethod('POST') && $accountDoc) {
            $account->setCalcValue1($accountDoc->getCalcValue1());
            $account->setCalcValue2($accountDoc->getCalcValue2());
            $account->setPriceFactor($accountDoc->getPriceFactor());
        } elseif (!$accountDoc) {
            $accountDoc = new \PSC\Shop\EntityBundle\Document\Account();
            $accountDoc->setCalcValue1("");
            $accountDoc->setCalcValue2("");
            $accountDoc->setUid($account->getid());
            $accountDoc->setExtraSettings($account->getExtraSettings());
            $accountDoc->setPriceFactor($account->getPriceFactor());
            $documentManager->persist($accountDoc);
            $documentManager->flush();
        }

        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $account->setProducts($this->merge([$account->getProductsOrg(), $account->getProductsSub()]));
            $entityManager->persist($account);
            $entityManager->flush();
            $accountDoc->setCalcValue1($account->getCalcValue1());
            $accountDoc->setCalcValue2($account->getCalcValue2());
            $accountDoc->setPriceFactor($account->getPriceFactor());
            $accountDoc->setExtraSettings($account->getExtraSettings());
            $documentManager->persist($accountDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Account \'' . $account->getTitle() . '\' has been created!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopAccountBundle::class, $account->getTitle(), "Account added");
            return $this->redirectToRoute('psc_shop_account_backend_edit', ['uuid' => $account->getUid()]);
        }

        return array(
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'account' => $account

        );
    }

    /**
     * edit Seite
     *
     * @Route("/edit/edit/{uuid}", name="psc_shop_account_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param string $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function editAction(
        Request $request,
        Field $fieldService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        Shop $shopService,
        $uuid = ""
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Account);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Account);
        $selectedShop = $shopService->getSelectedShop();
/** @var Account $account */
        $account = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Account')
                ->findOneBy(array('uid' => $uuid));
/** @var \PSC\Shop\EntityBundle\Document\Account $accountDoc */
        $accountDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Account')
            ->findOneBy(array('uid' => (string)$account->getId()));
        if (!$request->isMethod('POST') && $accountDoc) {
            $account->setCalcValue1($accountDoc->getCalcValue1());
            $account->setCalcValue2($accountDoc->getCalcValue2());
            $account->setExtraSettings($accountDoc->getExtraSettings());
            $account->setPriceFactor($accountDoc->getPriceFactor());
            $account->setProductsOrg($account->getProducts());
            $account->setProductsSub($account->getProducts());
        } elseif (!$accountDoc) {
            $accountDoc = new \PSC\Shop\EntityBundle\Document\Account();
            $accountDoc->setCalcValue1("");
            $accountDoc->setCalcValue2("");
            $accountDoc->setUid($account->getid());
            $accountDoc->setExtraSettings($account->getExtraSettings());
            $accountDoc->setPriceFactor($account->getPriceFactor());
            $documentManager->persist($accountDoc);
            $documentManager->flush();
        }

        $form = $this->createForm(AccountType::class, $account);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $account->setProducts($this->merge([$account->getProductsOrg(), $account->getProductsSub()]));
            $accountDoc->setCalcValue1($account->getCalcValue1());
            $accountDoc->setCalcValue2($account->getCalcValue2());
            $accountDoc->setExtraSettings($account->getExtraSettings());
            $accountDoc->setPriceFactor($account->getPriceFactor());
            $this->historyService->createHistoryEntry(new PSCHistory((string)$account->getUid()), $account, $accountDoc);
            $entityManager->persist($account);
            $entityManager->flush();
            $documentManager->persist($accountDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Account \'' . $account->getTitle() . '\' has been upgraded!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopAccountBundle::class, $account->getTitle(), "Account saved");
        }

        return array(
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'account' => $account,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$account->getUid())

        );
    }

    /**
     * @param array... $arrayCollections
     * @return ArrayCollection
     */
    private function merge($arrayCollections)
    {
        $returnCollection = new ArrayCollection();
/**
         * @var ArrayCollection $arrayCollection
         */
        foreach ($arrayCollections as $arrayCollection) {
            if ($returnCollection->count() === 0) {
                $returnCollection = $arrayCollection;
            } else {
                $arrayCollection->map(function ($element) use (&$returnCollection) {

                    if (!$returnCollection->contains($element)) {
                            $returnCollection->add($element);
                    }
                });
            }
        }

        return $returnCollection;
    }
}
