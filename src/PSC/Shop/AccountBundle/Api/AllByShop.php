<?php

namespace PSC\Shop\AccountBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\AccountBundle\Model\Account as ModelAccount;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use PSC\Shop\AccountBundle\Dto\All\Output;
use PSC\Shop\AccountBundle\Transformer\Account as TransformerAccount;
use PSC\Shop\EntityBundle\Entity\Account;

class AllByShop extends AbstractController
{
    /**
     * get accounts by shop
     *
     * @Route("allbyshop/{shop_uuid}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="account",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\AccountBundle\Dto\All\Output::class))
     * )
     * @OA\Tag(name="Account")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="Bearer")
     */
    public function AllAction(
        EntityManagerInterface $entityManager,
        TransformerAccount $transformerAccount,
        string $shop_uuid = ""
    ): JsonResponse {

        $output = new Output();

        $accountRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Account');
        $qb = $accountRepository->createQueryBuilder('account')
            ->leftJoin('account.shops', 'shops')
            ->orderBy('account.uid', 'desc');

        $qb->andWhere('shops.uid = :shop_id')
            ->setParameter("shop_id", $shop_uuid);

        $accounts = $qb->getQuery()->execute();

        foreach ($accounts as $account) {
            $c = new ModelAccount();
            $transformerAccount->fromEntity($c, $account);

            $output->data[] = $c;
        }

        return $this->json($output);
    }
}
