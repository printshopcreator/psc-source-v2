<?php

namespace PSC\Shop\AccountBundle\Document;

use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="accountObj_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $accountObj;

    function __construct(string $accountObj = "")
    {
        parent::__construct();
        $this->accountObj = $accountObj;
    }

    public function getAccountObj()
    {
        return $this->accountObj;
    }

    public function setAccountObj(string $accountObj)
    {
        $this->accountObj = $accountObj;
    }

    public function getReferenceField(): string
    {
        return 'accountObj';
    }
}
