<?php

namespace PSC\Shop\AccountBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Account
{
    /**
     * @OA\Property(type="integer")
     */
    private int $uid = 0;

    /**
     * @OA\Property(type="string")
     */
    private string $uuid = "";

    /**
     * @OA\Property(type="string")
     */
    private string $appendix;

    /**
     * @OA\Property(type="string")
     */
    private string $title;

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $value): void
    {
        $this->uuid = $value;
    }

    public function getAppendix(): string
    {
        return $this->appendix;
    }

    public function setAppendix(string $value): void
    {
        $this->appendix = $value;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $value): void
    {
        $this->title = $value;
    }

    public function getUid(): int
    {
        return $this->uid;
    }

    public function setUid(int $value): void
    {
        $this->uid = $value;
    }
}
