<?php

namespace PSC\Shop\AccountBundle\Dto\All;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Shop\ContactBundle\Model\Contact;

class Output
{
    /**
     * @var Contact[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\AccountBundle\Model\Account::class)))
     */
    public array $data = [];
}
