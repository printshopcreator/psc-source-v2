<?php

namespace PSC\Shop\AccountBundle\Transformer;

use PSC\Shop\EntityBundle\Entity\Account as EntityAccount;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Account
{
    public function fromEntity(\PSC\Shop\AccountBundle\Model\Account $account, EntityAccount $accountEntity)
    {
        $account->setUid((string)$accountEntity->getUid());
        $account->setUuid((string)$accountEntity->getUid());
        $account->setAppendix((int)$accountEntity->getUid());
        $account->setTitle($accountEntity->getTitle());
    }
}
