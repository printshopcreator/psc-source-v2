<?php

namespace PSC\Shop\AccountBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $menu->addChild('Firmen', array(
                'route' => 'psc_shop_account_backend_list',
                'extras' => array(
                    'icon' => 'fas fa-user-friends',
                    'orderNumber' => 14
                )
            ));
        }
    }
}
