<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\AccountBundle\Form\Backend\General;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\General;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountType extends AbstractType
{
    /** @var Field  */
    protected $fields;
/** @var General  */
    protected $general;
    protected $shop = null;
    protected $formFactory;
    public function __construct(Shop $shop, Field $fields, FormFactoryInterface $formFactory, General $general)
    {
        $this->fields = $fields;
        $this->formFactory = $formFactory;
        $this->general = $general;
        $this->shop = $shop;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Name'])
            ->add('payments', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Payment',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop')->andWhere('u.private = 1')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('shippings', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Shipping',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop')->andWhere('u.private = 1')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('productGroups', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Productgroup',
                'choice_label' => function (Productgroup $choice, $key, $value) {

                    if ($choice->isEnable()) {
                        return $choice->getTitle();
                    } else {
                        return $choice->getTitle() . ' (disabled)';
                    }
                },
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop')->andWhere('u.private = 1')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('parent', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Account',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'label' => 'psc.shop.account.form.parent',
                'multiple' => false,
                'required' => false,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->join('u.shops', 's')
                        ->where('s.uid = :shop')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('productsOrg', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Product',
                'choice_label' => 'nrTitle',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop')->andWhere('u.private = 1 AND u.originalProduct = 0')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('productsSub', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Product',
                'choice_label' => 'nrTitle',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop')->andWhere('u.private = 1 AND u.originalProduct != 0')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('appendix', TextType::class, ['required' => false, 'label' => 'additive'])
            ->add('locked', CheckboxType::class, ['required' => false, 'label' => 'Locked'])
            ->add('street', TextType::class, ['required' => false, 'label' => 'street'])
            ->add('destrict', TextType::class, ['required' => false, 'label' => 'district'])
            ->add('zip', TextType::class, ['required' => false, 'label' => 'zip'])
            ->add('city', TextType::class, ['required' => false, 'label' => 'city'])
            ->add('state', TextType::class, ['required' => false, 'label' => 'state'])
            ->add('country', TextType::class, ['required' => false, 'label' => 'country'])
            ->add('email', TextType::class, ['required' => false, 'label' => 'Email'])
            ->add('houseNumber', TextType::class, ['required' => false, 'label' => 'housenumber'])
            ->add('homepage', TextType::class, ['required' => false, 'label' => 'Homepage'])
            ->add('extraSettings', TextareaType::class, array('required' => false, 'label' => 'extrasettings'))

            ->add('phoneAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('phonePrefix', TextType::class, ['required' => false, 'label' => 'prefix'])
            ->add('phone', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('phoneAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('priceFactor', NumberType::class, array('required' => false, 'label' => 'factor', 'scale' => 2, 'html5' => true,
                'attr' => array(
                    'min' => -0.01,
                    'max' => 2.00,
                    'step' => 0.01,
            )))

            ->add('mobileAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('mobilePrefix', TextType::class, ['required' => false, 'label' => 'prefix'])
            ->add('mobile', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('mobileAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('faxAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('faxPrefix', TextType::class, ['required' => false, 'label' => 'prefix'])
            ->add('fax', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('faxAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('alternativAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('alternativType', TextType::class, ['required' => false, 'label' => 'Typ'])
            ->add('alternativ', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('alternativAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('bankKtoName', TextType::class, ['required' => false, 'label' => 'accountowner'])
            ->add('bankKTO', TextType::class, ['required' => false, 'label' => 'accountnumber'])
            ->add('bankBLZ', TextType::class, ['required' => false, 'label' => 'bankcode'])
            ->add('bankIban', TextType::class, ['required' => false, 'label' => 'Iban'])
            ->add('bankBic', TextType::class, ['required' => false, 'label' => 'BIC'])
            ->add('bankName', TextType::class, ['required' => false, 'label' => 'bankname'])

            ->add('ustid', TextType::class, ['required' => false, 'label' => 'VatId'])
            ->add('typ', ChoiceType::class, ['choices' => $this->general->getAccountTypes(),'translation_domain' => 'general', 'required' => false, 'label' => 'Typ'])
            ->add('image', MediaType::class, ['required' => false, 'label' => 'pic'])
            ->add('templateSwitch', TextType::class, ['required' => false, 'label' => 'Alternatetemplate'])
            ->add('information', TextareaType::class, ['required' => false, 'label' => 'information'])

            ->add('megaCode', TextType::class, ['required' => false, 'label' => 'MegaCode'])
            ->add('hotelName', TextType::class, ['required' => false, 'label' => 'Hotelname'])
            ->add('subHotelName', TextType::class, ['required' => false, 'label' => 'SubHotelname'])
            ->add('emailGm', TextType::class, ['required' => false, 'label' => 'EmailGM'])
            ->add('room', TextType::class, ['required' => false, 'label' => 'room'])
            ->add('gz', TextType::class, ['required' => false, 'label' => 'LegalAffiliation'])
            ->add('gf', TextType::class, ['required' => false, 'label' => 'managingDirector'])
            ->add('gSitz', TextType::class, ['required' => false, 'label' => 'seatofthecompany'])
            ->add('handelsRegister', TextType::class, ['required' => false, 'label' => 'commercialregister'])
            ->add('hrb', TextType::class, ['required' => false, 'label' => 'HRB'])
            ->add('vorsitz', TextType::class, ['required' => false, 'label' => 'ChairmanoftheBoard'])

            ->add('salutation1', ChoiceType::class, ['choices' => $this->general->getSalutation(),'translation_domain' => 'general', 'required' => false, 'label' => 'salutation1'])
            ->add('firstname1', TextType::class, ['required' => false, 'label' => 'firsname1'])
            ->add('lastname1', TextType::class, ['required' => false, 'label' => 'lastname1'])
            ->add('salutation2', ChoiceType::class, ['choices' => $this->general->getSalutation(),'translation_domain' => 'general', 'required' => false, 'label' => 'salutation2'])
            ->add('firstname2', TextType::class, ['required' => false, 'label' => 'firsname2'])
            ->add('lastname2', TextType::class, ['required' => false, 'label' => 'lastname2'])
            ->add('calcValue1', TextType::class, ['required' => false, 'label' => 'value1'])
            ->add('calcValue2', TextType::class, ['required' => false, 'label' => 'value2']);
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Account) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup()), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'account';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Account',
            'translation_domain' => 'core_account_create_and_edit'
        ));
    }
}
