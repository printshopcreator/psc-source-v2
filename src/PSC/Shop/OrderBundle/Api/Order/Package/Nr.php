<?php

namespace PSC\Shop\OrderBundle\Api\Order\Package;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\OrderBundle\Dto\Order\Package\Nr\Input;
use PSC\Shop\OrderBundle\Dto\Order\Package\Nr\Output;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Nr extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * change packagenumber for order
     *
     * @Route("/order/package/nr", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\OrderBundle\Dto\Order\Package\Nr\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="order",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\OrderBundle\Dto\Order\Package\Nr\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\OrderBundle\Dto\Order\Package\Nr\Input::class))
     * )
     * @OA\Tag(name="Order")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function saveNumber(Input $data): JsonResponse
    {
        /** @var Order $order */
        $order = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(['uuid' => $data->order]);

        if (!$order) {
            /** @var Order $order */
            $order = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(['alias' => $data->order]);
        }
        if (!$order) {
            /** @var Order $order */
            $order = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(['uid' => $data->order]);
        }

        if ($order) {
            $order->setPackage($data->nr);

            $this->entityManager->persist($order);
            $this->entityManager->flush();

            return $this->json(new Output());
        }

        return $this->json(new NotFound("Order not found"));
    }
}
