<?php

namespace PSC\Shop\OrderBundle\Api\Order;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\OrderBundle\Dto\Order\GetOneByUuid\Input;
use PSC\Shop\OrderBundle\Model\Order\Position\DummyProductTypeObject;
use PSC\Shop\OrderBundle\Service\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class GetOneByUuid extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Order $orderService;

    public function __construct(EntityManagerInterface $entityManager, Order $orderService)
    {
        $this->entityManager = $entityManager;
        $this->orderService = $orderService;
    }

    /**
     * get order by uuid
     *
     * @Route("/order/getonebyuuid", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\OrderBundle\Dto\Order\GetOneByUuid\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="orders",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\OrderBundle\Dto\Order\GetOneByUuid\Input::class))
     * )
     * @OA\Tag(name="Order")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="ApiKeyAuth")
     * @Security(name="Bearer")
     */
    public function getonebyuuid(Input $data): JsonResponse
    {
        $row = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneByUuid($data->uuid);

        if ($row) {
            $order = $this->orderService->getOrderByUid($row->getUid());

            return $this->json($order);
        }

        return $this->json(new NotFound("order not found"));
    }
}
