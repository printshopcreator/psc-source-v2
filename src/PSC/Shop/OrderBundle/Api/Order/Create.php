<?php

namespace PSC\Shop\OrderBundle\Api\Order;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\OrderBundle\Model\Order as ModelOrder;
use PSC\Shop\OrderBundle\Model\Order\Position\DummyProductTypeObject;
use PSC\Shop\OrderBundle\Service\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Create extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Order $orderService;

    public function __construct(EntityManagerInterface $entityManager, Order $orderService)
    {
        $this->entityManager = $entityManager;
        $this->orderService = $orderService;
    }

    /**
     * create order
     *
     * @Route("/order/create", methods={"POST"})
     * @ParamConverter("order", class="\PSC\Shop\OrderBundle\Model\Order", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="orders",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\OrderBundle\Model\Order::class))
     * )
     * @OA\Tag(name="Order")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="ApiKeyAuth")
     * @Security(name="Bearer")
     */
    public function create(ModelOrder $order): JsonResponse
    {
        $this->orderService->storeOrder($order);
        return $this->json($order);
    }
}
