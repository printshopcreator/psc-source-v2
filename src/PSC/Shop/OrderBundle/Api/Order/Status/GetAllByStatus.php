<?php

namespace PSC\Shop\OrderBundle\Api\Order\Status;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Shop\OrderBundle\Dto\Order\Status\GetAllByStatus\Input;
use PSC\Shop\OrderBundle\Dto\Order\Status\GetAllByStatus\Output;
use PSC\Shop\OrderBundle\Model\Order\Position\DummyProductTypeObject;
use PSC\Shop\OrderBundle\Service\Order;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class GetAllByStatus extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Order $orderService;

    public function __construct(EntityManagerInterface $entityManager, Order $orderService)
    {
        $this->entityManager = $entityManager;
        $this->orderService = $orderService;
    }

    /**
     * get orders by status
     *
     * @Route("/order/status/getallbystatus", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\OrderBundle\Dto\Order\Status\GetAllByStatus\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="orders",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\OrderBundle\Dto\Order\Status\GetAllByStatus\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\OrderBundle\Dto\Order\Status\GetAllByStatus\Input::class))
     * )
     * @OA\Tag(name="Order")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function getallbystatus(Input $data): JsonResponse
    {
        $rows = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findByStatus($data->status);

        $temp = [];

        foreach ($rows as $row) {
            $order = $this->orderService->getOrderByUid($row->getUid());
            foreach ($order->getPositions() as $position) {
                $position->setSpecialProductTypeObject(new DummyProductTypeObject());
            }

            $temp[] = $order;
        }


        return $this->json(new Output($temp));
    }
}
