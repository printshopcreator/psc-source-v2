<?php

namespace PSC\Shop\OrderBundle\Api\Order\Status;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\OrderBundle\Dto\Order\Status\Change\Input;
use PSC\Shop\OrderBundle\Dto\Order\Status\Change\Output;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Change extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Manager $eventManager;
    private Shop $serviceShop;

    public function __construct(EntityManagerInterface $entityManager, Shop $serviceShop, Manager $eventManager)
    {
        $this->entityManager = $entityManager;
        $this->serviceShop = $serviceShop;
        $this->eventManager = $eventManager;
    }

    /**
     * change status for order
     *
     * @Route("/order/status/change", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\OrderBundle\Dto\Order\Status\Change\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="order",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\OrderBundle\Dto\Order\Status\Change\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\OrderBundle\Dto\Order\Status\Change\Input::class))
     * )
     * @OA\Tag(name="Order")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function change(Input $data): JsonResponse
    {
        /** @var Order $order */
        $order = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(['uuid' => $data->order]);

        if (!$order) {
            /** @var Order $order */
            $order = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(['alias' => $data->order]);
        }
        if (!$order) {
            /** @var Order $order */
            $order = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(['uid' => $data->order]);
        }

        if ($order) {
            $order->setStatus($data->status);

            $this->entityManager->persist($order);
            $this->entityManager->flush();

            $notify = new \PSC\Shop\QueueBundle\Event\Order\Status\Change();
            $notify->setShop($this->serviceShop->getShopByDomain()->getUID());
            $notify->setOrder($order->getUuid());
            $notify->setStatus($order->getStatus());
            $this->eventManager->addJob($notify);

            return $this->json(new Output());
        }

        return $this->json(new NotFound("Order not found"));
    }
}
