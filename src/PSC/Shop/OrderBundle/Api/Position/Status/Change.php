<?php

namespace PSC\Shop\OrderBundle\Api\Position\Status;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\OrderBundle\Dto\Position\Status\Change\Input;
use PSC\Shop\OrderBundle\Dto\Position\Status\Change\Output;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Change extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Manager $eventManager;
    private Shop $serviceShop;

    public function __construct(EntityManagerInterface $entityManager, Shop $serviceShop, Manager $eventManager)
    {
        $this->entityManager = $entityManager;
        $this->serviceShop = $serviceShop;
        $this->eventManager = $eventManager;
    }

    /**
     * change status for position
     *
     * @Route("/position/status/change", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\OrderBundle\Dto\Position\Status\Change\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="position",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\OrderBundle\Dto\Position\Status\Change\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\OrderBundle\Dto\Position\Status\Change\Input::class))
     * )
     * @OA\Tag(name="Position")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="ApiKeyAuth")
     * @Security(name="Bearer")
     */
    public function change(Input $data): JsonResponse
    {
        /** @var Orderpos $orderpos */
        $orderpos = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
            ->findOneBy(['uuid' => $data->position]);

        if ($orderpos) {
            $orderpos->setStatus($data->status);

            $this->entityManager->persist($orderpos);
            $this->entityManager->flush();

            $notify = new \PSC\Shop\QueueBundle\Event\Position\Status\Change();
            $notify->setShop($this->serviceShop->getShopByDomain()->getUID());
            $notify->setPosition($orderpos->getUuid());
            $notify->setStatus($orderpos->getStatus());
            $this->eventManager->addJob($notify);

            return $this->json(new Output());
        }

        return $this->json(new NotFound("Position not found"));
    }
}
