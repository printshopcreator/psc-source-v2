<?php

namespace PSC\Shop\OrderBundle\Api\Position\Upload;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\OrderBundle\Dto\Position\Upload\Delete\Input;
use PSC\Shop\OrderBundle\Dto\Position\Upload\Delete\Output;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Delete extends AbstractController
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * delete uploads for position
     *
     * @Route("/position/upload/delete", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\OrderBundle\Dto\Position\Upload\Delete\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="position",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\OrderBundle\Dto\Position\Upload\Delete\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\OrderBundle\Dto\Position\Upload\Delete\Input::class))
     * )
     * @OA\Tag(name="Position")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function delete(Input $data): JsonResponse
    {
        /** @var Orderpos $orderpos */
        $orderpos = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
            ->findOneBy(['uuid' => $data->position]);

        if ($orderpos) {
            foreach ($orderpos->getUploads() as $upload) {
                $this->entityManager->remove($upload);
            }
            $this->entityManager->flush();

            return $this->json(new Output());
        }

        return $this->json(new NotFound("Position not found"));
    }
}
