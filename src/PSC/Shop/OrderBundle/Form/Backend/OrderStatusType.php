<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrderStatusType extends AbstractType
{
    protected $status = array();
    public function __construct(Status $status)
    {
        $this->status = $status->getOrderStatusAsArray();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('status', ChoiceType::class, array(
                'choices'  => $this->status,
                'required' => false,
                'label' => 'cartstatus'
            ))
            ->add('package', TextType::class, array(
                'required' => false,
                'label' => 'packagenumber'
            ))
            ->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'status';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'status',
            'choice_translation_domain' => 'status',
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Order',
            'translation_domain' => 'core_order_detail'
        ));
    }
}
