<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Form\Backend;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    protected $status = array();
/** @var Session */
    protected $session;
    public function __construct(Session $session, Status $status)
    {
        $this->status = $status->getOrderStatusAsArray();
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $session = $this->session;
        $builder
            ->setMethod('POST')
            ->add('term', TextFilterType::class, array(
                'data' => $session->get('order_search_term'),
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($session) {

                    $session->set('order_search_term', $values['value']);
                    if (empty($values['value'])) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()
                        ->andWhere("(
                            orders.basketfield1 LIKE '%" . $values['value'] . "%' OR
                            orders.basketfield2 LIKE '%" . $values['value'] . "%' OR
                            contact.firstname LIKE '%" . $values['value'] . "%' OR
                            contact.lastname LIKE '%" . $values['value'] . "%' OR
                            contact.zip LIKE '%" . $values['value'] . "%' OR
                            contact.city LIKE '%" . $values['value'] . "%' OR
                            contact.houseNumber LIKE '%" . $values['value'] . "%' OR
                            contact.street LIKE '%" . $values['value'] . "%' OR
                            contact.uid = '" . $values['value'] . "' OR
                            orders.alias LIKE '%" . $values['value'] . "%')
                        ");
                }))
            ->add('status', ChoiceType::class, array(
                'choices'  => $this->status,
                'required' => false,
                'multiple' => true,
                //'expanded' => true,
                'label_attr' => ['class' => 'float-left'],
                'data' => $session->get('order_search_status'),
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($session) {

                    $session->set('order_search_status', $values['value']);
                    if (empty($values['value'])) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()
                        ->andWhere("
                            orders.status in (" . implode(",", $values['value']) . ")
                        ");
                }
            ));
    }

    public function getName()
    {
        return 'item_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'status',
            'choice_translation_domain' => 'status',
            'csrf_protection'   => false
        ));
    }
}
