<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Form\Backend\Detail;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactDeliveryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('company', TextType::class, array('label' => 'company', 'required' => false))
            ->add('company2', TextType::class, array('label' => 'additive', 'required' => false))
            ->add('firstname', TextType::class, array('label' => 'firstname', 'required' => false))
            ->add('lastname', TextType::class, array('label' => 'lastname', 'required' => false))
            ->add('city', TextType::class, array('label' => 'city', 'required' => false))
            ->add('phone', TextType::class, array('label' => 'phone', 'required' => false))
            ->add('zip', TextType::class, array('label' => 'zip', 'required' => false))
            ->add('country', TextType::class, array('label' => 'country', 'required' => false))
            ->add('houseNumber', TextType::class, array('label' => 'houseNumber', 'required' => false))
            ->add('street', TextType::class, array('label' => 'street', 'required' => false))
            ->add('email', TextType::class, array('label' => 'EMail', 'required' => false))

            ->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getBlockPrefix()
    {
        return 'contact_delivery';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Document\Embed\ContactAddress',
            'translation_domain' => 'core_order_detail'
        ));
    }
}
