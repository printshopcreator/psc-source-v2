<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProdinfoType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('customerInfo', TextareaType::class, array('required' => true, 'label' => 'Dem Kunden zeigen'))
            ->add('positioninfo', HiddenType::class);
    }

    public function getName()
    {
        return 'prodinfo';
    }
}
