<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Model;

use PSC\Shop\OrderBundle\Model\Order\Account;
use PSC\Shop\ContactBundle\Model\Address;
use PSC\Shop\ContactBundle\Model\Contact;
use PSC\Shop\OrderBundle\Model\Order\Payment;
use PSC\Shop\OrderBundle\Model\Order\Position;
use PSC\Shop\OrderBundle\Model\Order\Shipping;
use PSC\Shop\OrderBundle\Model\Order\Tax;
use Ramsey\Uuid\Uuid;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

abstract class Base
{
    /** @var \Ramsey\Uuid\UuidInterface
     *
     * @OA\Property(type="string")
     */
    private $uuid;
/** @var \DateTime
     *
     * @OA\Property(type="date", type="string", format="date-time")
     */
    private \DateTime $created;
/** @var \DateTime
     *
     * @OA\Property(type="date", type="string", format="date-time")
     */
    private \DateTime $updated;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $alias;
/** @var Address
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\ContactBundle\Model\Address::class))
     */
    private $deliveryAddress;
/** @var Address
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\ContactBundle\Model\Address::class))
     */
    private $senderAddress;
/** @var Address
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\ContactBundle\Model\Address::class))
     */
    private $invoiceAddress;
/**
     * @var Shipping
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Shipping::class))
     */
    private $shipping;
/** @var Payment
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Payment::class))
     */
    private $payment;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $net;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $vat;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $gross;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $paymentCosts;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $shippingCosts;
/** @var bool
     *
     * @OA\Property(type="boolean")
     */
    private $enable;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $info;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $basketField1;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $basketField2;
/** @var int
     *
     * @OA\Property(type="integer")
     */
    private $couponType;
/** @var Tax[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Tax::class)))
     */
    private $taxes = [];
/** @var int */
    private $shop;
/** @var int
     *
     * @OA\Property(type="integer")
     */
    private $status;
/** @var Position[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Position::class)))
     */
    private $positions = [];
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $externalOrderNumber;
/**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    private $uid;
/**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    private $type = 1;
/** @var Payment
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\ContactBundle\Model\Contact::class))
     */
    private Contact $contact;
/** @var Payment
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Account::class))
     */
    private Account $account;
    public function __construct()
    {
        $this->uuid = Uuid::uuid4()->toString();
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        $this->deliveryAddress = new Address();
        $this->contact = new Contact();
        $this->account = new Account();
        $this->invoiceAddress = new Address();
        $this->senderAddress = new Address();
        $this->shipping = new Shipping();
        $this->payment = new Payment();
        $this->net = 0;
        $this->vat = 0;
        $this->gross = 0;
        $this->paymentCosts = 0;
        $this->shippingCosts = 0;
        $this->enable = true;
        $this->couponType = 0;
        $this->externalOrderNumber = "";
        $this->basketField1 = "";
        $this->basketField2 = "";
        $this->status = 10;
    }

    public function getTypeAsString(): string
    {
        switch ($this->type) {
            case 1:
                return 'AF';
            case 2:
                return 'AN';
            case 3:
                return 'ST';
            case 4:
                return 'GU';
            case 5:
                return 'RE';
            case 6:
                return 'TRE';
            case 7:
                return 'TST';
            default:
                return 'AF';
        }
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function getType(): int
    {
        return $this->type;
    }

    public function setType(int $var): void
    {
        $this->type = $var;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    public function setContact(Contact $contact): void
    {
        $this->contact = $contact;
    }

    public function getContact(): Contact
    {
        return $this->contact;
    }

    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return Address
     */
    public function getDeliveryAddress()
    {
        return $this->deliveryAddress;
    }

    /**
     * @param Address $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return Address
     */
    public function getSenderAddress()
    {
        return $this->senderAddress;
    }

    /**
     * @param Address $senderAddress
     */
    public function setSenderAddress($senderAddress)
    {
        $this->senderAddress = $senderAddress;
    }

    /**
     * @return Address
     */
    public function getInvoiceAddress()
    {
        return $this->invoiceAddress;
    }

    /**
     * @param Address $invoiceAddress
     */
    public function setInvoiceAddress($invoiceAddress)
    {
        $this->invoiceAddress = $invoiceAddress;
    }

    /**
     * @return Shipping
     */
    public function getShipping()
    {
        return $this->shipping;
    }

    /**
     * @param Shipping $shipping
     */
    public function setShipping($shipping)
    {
        $this->shipping = $shipping;
    }

    /**
     * @return Payment
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * @param Payment $payment
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;
    }

    /**
     * @return float
     */
    public function getGross()
    {
        return $this->gross;
    }

    /**
     * @param float $net
     */
    public function setGross($gross)
    {
        $this->gross = $gross;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $net
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }

    /**
     * @return float
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * @param float $net
     */
    public function setNet($net)
    {
        $this->net = $net;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param string $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return float
     */
    public function getShippingCosts()
    {
        return $this->shippingCosts;
    }

    /**
     * @param float $versandKosten
     */
    public function setShippingCosts($value)
    {
        $this->shippingCosts = $value;
    }

    /**
     * @return float
     */
    public function getPaymentCosts()
    {
        return $this->paymentCosts;
    }

    /**
     * @param float $zahlKosten
     */
    public function setPaymentCosts($value)
    {
        $this->paymentCosts = $value;
    }

    /**
     * @return int
     */
    public function getCouponType()
    {
        return $this->couponType;
    }

    /**
     * @param int $couponType
     */
    public function setCouponType($couponType)
    {
        $this->couponType = $couponType;
    }

    /**
     * @return Tax[]
     */
    public function getTaxes()
    {
        return $this->taxes;
    }

    /**
     * @param Tax[] $taxes
     */
    public function setTaxes($taxes)
    {
        $this->taxes = $taxes;
    }

    /**
     * @param Tax $tax
     */
    public function addTax($tax)
    {
        $found = false;
        foreach ($this->taxes as $taxEntry) {
            if ($taxEntry->getName() == $tax->getName()) {
                $taxEntry->setValue($taxEntry->getValue() + $tax->getValue());
                $found = true;
            }
        }

        if (!$found) {
            $this->taxes[] = $tax;
        }
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return Position[]
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @param Position[] $positions
     */
    public function setPositions($positions)
    {
        $this->positions = $positions;
    }

    public function addPosition(Position $position)
    {
        $this->positions[] = $position;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getExternalOrderNumber(): string
    {
        return $this->externalOrderNumber;
    }

    /**
     * @param string $externalOrderNumber
     */
    public function setExternalOrderNumber(string $externalOrderNumber): void
    {
        $this->externalOrderNumber = $externalOrderNumber;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    public function getPositionByUuid(string $uuid): ?Position
    {
        foreach ($this->positions as $position) {
            if ($position->getUuid() == $uuid) {
                return $position;
            }
        }

        return null;
    }

    public function setBasketField1(string $text)
    {
        $this->basketField1 = $text;
    }

    public function getBasketField1()
    {
        return $this->basketField1;
    }

    public function setBasketField2(string $text)
    {
        $this->basketField2 = $text;
    }

    public function getBasketField2()
    {
        return $this->basketField2;
    }
}
