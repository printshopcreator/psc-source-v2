<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Model;

use PSC\Shop\OrderBundle\Model\Order\Account;
use PSC\Shop\OrderBundle\Model\Order\Address;
use PSC\Shop\OrderBundle\Model\Order\Contact;
use PSC\Shop\OrderBundle\Model\Order\Payment;
use PSC\Shop\OrderBundle\Model\Order\Position;
use PSC\Shop\OrderBundle\Model\Order\Shipping;
use PSC\Shop\OrderBundle\Model\Order\Tax;
use Ramsey\Uuid\Uuid;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class ParcelCancellation extends Order
{
    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    private $type = 7;
}
