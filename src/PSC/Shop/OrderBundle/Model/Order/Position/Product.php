<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Model\Order\Position;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Product
{
    /*
     * @OA\Property(type="string")
     */
    private string $title = "";
/*
     * @OA\Property(type="string")
     */
    private string $uuid = "";
/*
     * @OA\Property(type="string")
     */
    private string $nrIntern = "";
/*
     * @OA\Property(type="string")
     */
    private string $nrExtern = "";
/*
     * @OA\Property(type="integer")
     */
    private string $id = "";
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setNrExtern(string $var): void
    {
        $this->nrExtern = $var;
    }

    public function getNrExtern(): string
    {
        return $this->nrExtern;
    }

    public function setNrIntern(string $var): void
    {
        $this->nrIntern = $var;
    }

    public function getNrIntern(): string
    {
        return $this->nrIntern;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
