<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Model\Order\Position;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class AdditionalInfo
{
    /** @var string
     *
     * @OA\Property(type="string")
     */
    private $key;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $value;
    public function __construct()
    {
        $this->key = "";
        $this->value = "";
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->key = $key;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    public function asArray()
    {
        return [
            'key' => $this->getKey(),
            'value' => $this->getValue()
        ];
    }
}
