<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Model\Order\Position;

class DummyProductTypeObject implements IProductTypeObject
{
    public function getName()
    {
        return "Dummy Product Type";
    }

    public function getTyp()
    {
        return 0;
    }
}
