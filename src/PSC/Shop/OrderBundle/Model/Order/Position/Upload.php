<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Model\Order\Position;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Upload
{
    /** @var string
     *
     * @OA\Property(type="string")
     */
    private string $fileName = "";
/** @var string
     *
     * @OA\Property(type="string")
     */
    private string $uuid = "";
/** @var string
     *
     * @OA\Property(type="string")
     */
    private string $path = "";
/** @var string
     *
     * @OA\Property(type="string")
     */
    private string $chunkTitle = "";
/** @var string
     *
     * @OA\Property(type="string")
     */
    private string $typ = "";
    public function asArray()
    {
        return [
            'fileName' => $this->getFileName(),
            'uuid' => $this->getUuid(),
            'path' => $this->getPath(),
            'chunkTitle' => $this->getChunkTitle(),
            'typ' => $this->getTyp(),
        ];
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @param string $fileName
     */
    public function setFileName(string $fileName): void
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getChunkTitle(): string
    {
        return $this->chunkTitle;
    }

    /**
     * @param string $chunkTitle
     */
    public function setChunkTitle(string $chunkTitle): void
    {
        $this->chunkTitle = $chunkTitle;
    }

    /**
     * @return string
     */
    public function getTyp(): string
    {
        return $this->typ;
    }

    /**
     * @param string $typ
     */
    public function setTyp(string $typ): void
    {
        $this->typ = $typ;
    }
}
