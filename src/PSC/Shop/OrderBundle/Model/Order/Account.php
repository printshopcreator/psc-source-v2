<?php

namespace PSC\Shop\OrderBundle\Model\Order;

class Account
{
    private int $id;

    private string $appendix;

    private string $title;

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getAppendix(): string
    {
        return $this->appendix;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function setAppendix(string $value): void
    {
        $this->appendix = $value;
    }

    public function setTitle(string $value): void
    {
        $this->title = $value;
    }
}
