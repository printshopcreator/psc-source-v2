<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Model\Order;

use PSC\Shop\OrderBundle\Model\Order\Position\AdditionalInfo;
use PSC\Shop\OrderBundle\Model\Order\Position\IProductTypeObject;
use PSC\Shop\OrderBundle\Model\Order\Position\Upload;
use Ramsey\Uuid\Uuid;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Position
{
    /** @var int
     *
     * @OA\Property(type="integer")
     */
    private $pos;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $uuid;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $customerInfo;
/** @var int
     *
     * @OA\Property(type="integer")
     */
    private $status;
/** @var int
     *
     * @OA\Property(type="integer")
     */
    private $typ;
/** @var int
     *
     * @OA\Property(type="integer")
     */
    private $product;
/** @var bool
     *
     * @OA\Property(type="boolean")
     */
    private $copyProduct = false;
/** @var int
     *
     * @OA\Property(type="integer")
     */
    private $uid;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $net;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $allNet;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $vat;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $allVat;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $gross;
/** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    private $allGross;
/** @var int
     *
     * @OA\Property(type="integer")
     */
    private $count;
/** @var int
     *
     * @OA\Property(type="string")
     */
    protected $externalOrderNumber;
/** @var AdditionalInfo[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Position\AdditionalInfo::class)))
     */
    private $additionalInfos = [];
/** @var Upload[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Position\Upload::class)))
     */
    private $uploads = [];
    private $data;
/** @var IProductTypeObject
     *
     * @OA\Property(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order\Position\IProductTypeObject::class))
     */
    private $specialProductTypeObject;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $basketField1;
/** @var string
     *
     * @OA\Property(type="string")
     */
    private $basketField2;
    public function __construct()
    {
        $this->uuid = Uuid::uuid4()->toString();
        $this->status = 30;
        $this->typ = 0;
        $this->count = 1;
        $this->net = 0;
        $this->allNet = 0;
        $this->vat = 0;
        $this->allVat = 0;
        $this->gross = 0;
        $this->allGross = 0;
        $this->externalOrderNumber = "";
        $this->basketField1 = "";
        $this->basketField2 = "";
    }

    /**
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param int $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * @param int $typ
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return int
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param int $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return bool
     */
    public function isCopyProduct()
    {
        return $this->copyProduct;
    }

    /**
     * @param bool $copyProduct
     */
    public function setCopyProduct($copyProduct)
    {
        $this->copyProduct = $copyProduct;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return float
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * @param float $net
     */
    public function setNet($net)
    {
        $this->net = $net;
    }

    /**
     * @return float
     */
    public function getGross()
    {
        return $this->gross;
    }

    /**
     * @param float $gross
     */
    public function setGross($gross)
    {
        $this->gross = $gross;
    }

    /**
     * @return float
     */
    public function getVat()
    {
        return $this->vat;
    }

    /**
     * @param float $vat
     */
    public function setVat($vat)
    {
        $this->vat = $vat;
    }


    /**
     * @return AdditionalInfo[]
     */
    public function getAdditionalInfos()
    {
        return $this->additionalInfos;
    }

    /**
     * @param AdditionalInfo[] $additionalInfos
     */
    public function setAdditionalInfos($additionalInfos)
    {
        $this->additionalInfos = $additionalInfos;
    }

    /**
     * @param AdditionalInfo $additionalInfo
     */
    public function addAdditionalInfo($additionalInfo)
    {
        $this->additionalInfos[] = $additionalInfo;
    }

    /**
     * @return float
     */
    public function getAllNet()
    {
        return $this->allNet;
    }

    /**
     * @param float $allNet
     */
    public function setAllNet($allNet)
    {
        $this->allNet = $allNet;
    }

    /**
     * @return float
     */
    public function getAllGross()
    {
        return $this->allGross;
    }

    /**
     * @param float $allGross
     */
    public function setAllGross($allGross)
    {
        $this->allGross = $allGross;
    }

    /**
     * @return float
     */
    public function getAllVat()
    {
        return $this->allVat;
    }

    /**
     * @param float $allVat
     */
    public function setAllVat($allVat)
    {
        $this->allVat = $allVat;
    }

    public function setData($data)
    {
        $this->data = $data;
    }

    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getExternalOrderNumber(): string
    {
        return $this->externalOrderNumber;
    }

    /**
     * @param string $externalOrderNumber
     */
    public function setExternalOrderNumber(string $externalOrderNumber): void
    {
        $this->externalOrderNumber = $externalOrderNumber;
    }

    /**
     * @return Upload[]
     */
    public function getUploads(): array
    {
        return $this->uploads;
    }

    /**
     * @param Upload[] $uploads
     */
    public function setUploads(array $uploads): void
    {
        $this->uploads = $uploads;
    }

    /**
     * @param Upload $upload
     */
    public function addUpload($upload)
    {
        $this->uploads[] = $upload;
    }

    /**
     * @return IProductTypeObject
     */
    public function getSpecialProductTypeObject(): IProductTypeObject
    {
        return $this->specialProductTypeObject;
    }

    /**
     * @param IProductTypeObject $specialProductTypeObject
     */
    public function setSpecialProductTypeObject(IProductTypeObject $specialProductTypeObject): void
    {
        $this->specialProductTypeObject = $specialProductTypeObject;
    }

    public function getCustomerInfo(): string
    {
        return (string)$this->customerInfo;
    }

    public function setCustomerInfo(string $var): void
    {
        $this->customerInfo = $var;
    }

    public function setBasketField1(string $var): void
    {
        $this->basketField1 = $var;
    }

    public function getBasketField1(): string
    {
        return $this->basketField1;
    }

    public function setBasketField2(string $var): void
    {
        $this->basketField2 = $var;
    }

    public function getBasketField2(): string
    {
        return $this->basketField2;
    }
}
