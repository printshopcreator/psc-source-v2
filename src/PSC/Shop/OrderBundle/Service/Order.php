<?php

namespace PSC\Shop\OrderBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\OrderBundle\Model\Base as PSCBase;
use PSC\Shop\OrderBundle\Transformer\Order\Position;
use PSC\Shop\QueueBundle\Event\Order\Create;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Order
 *
 * @author Thomas Peterson
 */
class Order
{
    protected $entityManager;
    protected $mongoManager;
    /**
     * @var Shop
     */
    private $shopService;
    /**
     * @var \PSC\Shop\OrderBundle\Transformer\Order
     */
    private $orderTransformer;
    /**
     * @var Position
     */
    private $positionTransformer;

    /** @var \PSC\Shop\OrderBundle\Model\Order */
    private $currentOrder;

    /** @var Manager */
    private $eventManager;

    private Alias $aliasService;

    private bool $load_only_order = false;

    public function __construct(Alias $aliasService, EntityManagerInterface $em, DocumentManager $mongoManager, Shop $shopService, \PSC\Shop\OrderBundle\Transformer\Order $orderTransformer, Position $positionTransformer, Manager $eventManager)
    {
        $this->entityManager = $em;
        $this->mongoManager = $mongoManager;
        $this->shopService = $shopService;
        $this->orderTransformer = $orderTransformer;
        $this->positionTransformer = $positionTransformer;
        $this->eventManager = $eventManager;
        $this->aliasService = $aliasService;
    }

    public function storeOrder(\PSC\Shop\OrderBundle\Model\Order $order)
    {
        if ($order->getUid() != 0) {
            $this->updateOrder($order);
        } else {
            $result = $this->checkIfOrderExists($order);
            if ($result === false) {
                $this->newOrder($order);
            }
        }
    }

    protected function checkIfOrderExists(\PSC\Shop\OrderBundle\Model\Order $order): bool
    {
        $orderRepo = $this->entityManager->getRepository(\PSC\Shop\EntityBundle\Entity\Order::class);
        $order = $orderRepo->findOneBy(['alias' => $order->getAlias(), 'shop' => $order->getShop()]);
        if ($order) {
            return true;
        }

        return false;
    }

    protected function updateOrder(\PSC\Shop\OrderBundle\Model\Order $order)
    {
        if ($this->load_only_order) {
            die("Do Not Update");
        }

        $orderRepo = $this->entityManager->getRepository(\PSC\Shop\EntityBundle\Entity\Order::class);
        $orderDocRepo = $this->mongoManager->getRepository(\PSC\Shop\EntityBundle\Document\Order::class);

        $posRepo = $this->entityManager->getRepository(Orderpos::class);
        $posDocRepo = $this->mongoManager->getRepository(\PSC\Shop\EntityBundle\Document\Position::class);

        foreach ($order->getPositions() as $position) {
            /** @var Orderpos $positionEntity */
            $positionEntity = $posRepo->findOneBy(array('id' => (string)$position->getUid()));

            /** @var \PSC\Shop\EntityBundle\Document\Position $positionDoc */
            $positionDoc = $posDocRepo->findOneBy(array('uid' => (string)$position->getUid()));

            $positionEntity->setStatus($position->getStatus());
            $positionDoc->setExternalOrderNumber($position->getExternalOrderNumber());

            $this->entityManager->persist($positionEntity);
            $this->mongoManager->persist($positionDoc);
        }

        $this->entityManager->flush();
        $this->mongoManager->flush();
        $this->entityManager->clear();
        $this->mongoManager->clear();
    }

    protected function newOrder(\PSC\Shop\OrderBundle\Model\Order $order)
    {
        $orderEntity = new \PSC\Shop\EntityBundle\Entity\Order();
        $this->orderTransformer->toEntity($order, $orderEntity);
        if ($orderEntity->getAlias() == "") {
            $orderEntity->setAlias($this->aliasService->generateAlias($orderEntity->getType()));
        }
        $this->entityManager->persist($orderEntity);
        $this->entityManager->flush();

        $order->setUid($orderEntity->getUID());

        $orderDoc = new \PSC\Shop\EntityBundle\Document\Order();
        $orderDoc->setUid($orderEntity->getUID());
        $this->orderTransformer->toDocument($order, $orderDoc);
        $this->mongoManager->persist($orderDoc);
        $this->mongoManager->flush();

        $i = 1;
        foreach ($order->getPositions() as $position) {
            $position->setPos($i);

            $positionEntity = new Orderpos();
            $positionEntity->setOrder($orderEntity);
            $this->positionTransformer->toEntity($position, $positionEntity);
            $this->entityManager->persist($positionEntity);
            $this->entityManager->flush();

            $positionDoc = new \PSC\Shop\EntityBundle\Document\Position();
            $positionDoc->setUid($positionEntity->getUid());
            $this->positionTransformer->toDocument($position, $positionDoc);
            $this->mongoManager->persist($positionDoc);

            $i++;
        }

        $this->entityManager->flush();
        $this->mongoManager->flush();
        $this->entityManager->clear();
        $this->mongoManager->clear();

        $notify = new Create();
        $notify->setShop($order->getShop());
        $notify->setOrder($order->getUuid());
        $this->eventManager->addJob($notify);

        $this->currentOrder = $order;
    }

    /**
     * @param $uuid
     * @return \PSC\Shop\OrderBundle\Model\Order
     */
    public function getOrderByUuid($uuid, $load_only_order = false)
    {
        $this->load_only_order = $load_only_order;
        $orderRepo = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');

        /** @var \PSC\Shop\EntityBundle\Entity\Order $orderEntity */
        $orderEntity = $orderRepo->findOneBy(array('uuid' => $uuid));

        return $this->parseToModel($orderEntity);
    }

    /**
     * @param $uid
     * @return \PSC\Shop\OrderBundle\Model\Order
     */
    public function getOrderByUid($uid): PSCBase
    {
        $orderRepo = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');

        $orderEntity = $orderRepo->findOneBy(array('uid' => $uid));

        return $this->parseToModel($orderEntity);
    }

    /**
     * @param $uuid
     * @return \PSC\Shop\OrderBundle\Model\Order\Position
     */

    public function getPositionByUuid($positionUuid)
    {
        $orderRepo = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos');

        /** @var Orderpos $orderposEntity */
        $orderposEntity = $orderRepo->findOneBy(array('uuid' => $positionUuid));

        $this->parseToModel($orderposEntity->getOrder());

        foreach ($this->currentOrder->getPositions() as $position) {
            if ($position->getUuid() == $positionUuid) {
                return $position;
            }
        }
    }

    public function getNewOrder()
    {
        return new \PSC\Shop\OrderBundle\Model\Order();
    }

    /**
     * @return \PSC\Shop\OrderBundle\Model\Order
     */
    public function getCurrentOrder()
    {
        return $this->currentOrder;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Order $orderEntity
     * @return \PSC\Shop\OrderBundle\Model\Order
     */
    private function parseToModel($orderEntity)
    {

        $this->currentOrder = new \PSC\Shop\OrderBundle\Model\Order();
        if ($orderEntity->getType() == 2) {
            $this->currentOrder = new \PSC\Shop\OrderBundle\Model\Offer();
        }
        if ($orderEntity->getType() == 3) {
            $this->currentOrder = new \PSC\Shop\OrderBundle\Model\Cancelation();
        }
        if ($orderEntity->getType() == 4) {
            $this->currentOrder = new \PSC\Shop\OrderBundle\Model\Credit();
        }
        if ($orderEntity->getType() == 5) {
            $this->currentOrder = new \PSC\Shop\OrderBundle\Model\Invoice();
        }
        if ($orderEntity->getType() == 6) {
            $this->currentOrder = new \PSC\Shop\OrderBundle\Model\ParcelInvoice();
        }
        if ($orderEntity->getType() == 7) {
            $this->currentOrder = new \PSC\Shop\OrderBundle\Model\ParcelCancellation();
        }

        $this->orderTransformer->fromEntity($this->currentOrder, $orderEntity);

        /** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
        $orderDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('uid' => (string)$this->currentOrder->getUid()));

        if ($orderDoc) {
            $this->orderTransformer->fromDocument($this->currentOrder, $orderDoc);
        }

        if (!$this->load_only_order) {
            /** @var Orderpos $pos */
            foreach ($orderEntity->getPositions() as $pos) {
                $position = new \PSC\Shop\OrderBundle\Model\Order\Position();

                $this->positionTransformer->fromEntity($position, $pos);

                /** @var \PSC\Shop\EntityBundle\Document\Position $positionDoc */
                $positionDoc = $this->mongoManager
                    ->getRepository('PSC\Shop\EntityBundle\Document\Position')
                    ->findOneBy(array('uid' => (string)$position->getUid()));
                if ($positionDoc) {
                    $this->positionTransformer->fromDocument($position, $positionDoc);
                }

                $this->currentOrder->addPosition($position);
            }
        }

        return $this->currentOrder;
    }
}
