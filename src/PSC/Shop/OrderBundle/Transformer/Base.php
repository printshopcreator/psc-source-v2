<?php

namespace PSC\Shop\OrderBundle\Transformer;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Order
 *
 * @author Thomas Peterson
 */
abstract class Base
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    protected $entityManager;

    protected $mongoManager;
    /**
     * @var Shop
     */
    protected $shopService;

    public function __construct(EntityManagerInterface $em, DocumentManager $mongoManager, Shop $shopService)
    {
        $this->entityManager = $em;
        $this->mongoManager = $mongoManager;
        $this->shopService = $shopService;
    }
}
