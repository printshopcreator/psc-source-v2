<?php

namespace PSC\Shop\OrderBundle\Transformer\Order;

use PSC\Shop\EntityBundle\Document\Embed\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Order
 *
 * @author Thomas Peterson
 */
class Address
{
    public function toDocument(\PSC\Shop\ContactBundle\Model\Address $address, ContactAddress $addressDoc)
    {
        $addressDoc->setCompany($address->getCompany());
        $addressDoc->setCompany2($address->getCompany2());
        $addressDoc->setZip($address->getZip());
        $addressDoc->setCity($address->getCity());
        $addressDoc->setEmail($address->getEmail());
        $addressDoc->setFax($address->getFax());
        $addressDoc->setFirstname($address->getFirstname());
        $addressDoc->setLastname($address->getLastname());
        $addressDoc->setStreet($address->getStreet());
        $addressDoc->setHouseNumber($address->getHouseNumber());
        $addressDoc->setPhone($address->getPhone());
        $addressDoc->setPosition($address->getPosition());
    }

    public function fromDocument(\PSC\Shop\ContactBundle\Model\Address $address, ContactAddress $addressDoc)
    {
        $address->setCompany($addressDoc->getCompany());
        $address->setCompany2($addressDoc->getCompany2());
        $address->setZip($addressDoc->getZip());
        $address->setCity($addressDoc->getCity());
        $address->setEmail($addressDoc->getEmail());
        $address->setFax($addressDoc->getFax());
        $address->setFirstname($addressDoc->getFirstname());
        $address->setLastname($addressDoc->getLastname());
        $address->setStreet($addressDoc->getStreet());
        $address->setHouseNumber($addressDoc->getHouseNumber());
        $address->setPhone($addressDoc->getPhone());
        $address->setPosition($addressDoc->getPosition());
    }
}
