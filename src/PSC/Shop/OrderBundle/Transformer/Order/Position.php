<?php

namespace PSC\Shop\OrderBundle\Transformer\Order;

use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\OrderBundle\Model\Order\Position\DummyProductTypeObject;
use PSC\Shop\OrderBundle\Model\Order\Position\Product as AliasedProduct;
use PSC\Shop\OrderBundle\Model\Order\Position\Upload;
use PSC\Shop\OrderBundle\Transformer\Base;
use PSC\Shop\OrderBundle\Transformer\Order\Position\Product as PSCProduct;
use PSC\System\PluginBundle\Service\ProductType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Position
 *
 * @author Thomas Peterson
 */
class Position extends Base
{
    /**
     * @var \PSC\Shop\ProductBundle\Service\Product
     */
    private $productService;
    private ProductType $productTypeRegistry;
    private PSCProduct $productTransformer;

    /**
     * @required
     */
    public function setProductService(\PSC\Shop\ProductBundle\Service\Product $service, ProductType $productType, PSCProduct $productTransformer)
    {
        $this->productService = $service;
        $this->productTypeRegistry = $productType;
        $this->productTransformer = $productTransformer;
    }

    public function toEntity(\PSC\Shop\OrderBundle\Model\Order\Position $position, Orderpos $positionEntity)
    {
        $positionEntity->setPos($position->getPos());
        $positionEntity->setUuid($position->getUuid());
        $positionEntity->setTyp($position->getTyp());
        $positionEntity->setStatus($position->getStatus());
        $positionEntity->setLayouterMode(0);
        $positionEntity->setRenderPrint(0);
        $positionEntity->setCount($position->getCount());
        $positionEntity->setPriceAllNetto($position->getAllNet());
        $positionEntity->setPriceOneNetto($position->getNet());
        $positionEntity->setPriceAllBrutto($position->getAllGross());
        $positionEntity->setPriceOneBrutto($position->getGross());
        $positionEntity->setPriceAllSteuer($position->getAllVat());
        $positionEntity->setPriceOneSteuer($position->getVat());
        $positionEntity->setResalePrice(0);
        $positionEntity->setBasketField1((string)$position->getBasketField1());
        $positionEntity->setBasketField2((string)$position->getBasketField2());

        /** @var Product $product */
        $product = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uid' => $position->getProduct()));

        if ($position->isCopyProduct()) {
            $product = $this->productService->copy($product, false, true);
        }

        $positionEntity->setProduct($product);
    }

    public function toDocument(\PSC\Shop\OrderBundle\Model\Order\Position $position, \PSC\Shop\EntityBundle\Document\Position $positionDoc)
    {
        $temp = [];
        foreach ($position->getAdditionalInfos() as $info) {
            $temp[] = $info->asArray();
        }
        $positionDoc->setAdditionalInfos($temp);
        $positionDoc->setCustomerInfo($position->getCustomerInfo());
    }

    public function fromEntity(\PSC\Shop\OrderBundle\Model\Order\Position $position, Orderpos $pos)
    {
        $position->setProduct(new AliasedProduct());
        $this->productTransformer->fromEntity($position->getProduct(), $pos->getProduct());
        $position->setPos($pos->getPos());
        $position->setUuid($pos->getUuid());
        $position->setUid($pos->getId());
        $position->setCount($pos->getCount());
        $position->setNet($pos->getPriceOneNetto());
        $position->setAllNet($pos->getPriceAllNetto());
        $position->setVat($pos->getPriceOneSteuer());
        $position->setAllVat($pos->getPriceAllSteuer());
        $position->setGross($pos->getPriceOneBrutto());
        $position->setAllGross($pos->getPriceAllBrutto());
        $position->setStatus($pos->getStatus());
        $position->setData($pos->getData());
        $position->setBasketField1((string)$pos->getBasketField1());
        $position->setBasketField2((string)$pos->getBasketField2());

        /** @var \PSC\Shop\EntityBundle\Entity\Upload $up */
        foreach ($pos->getUploads() as $up) {
            $upload = new Upload();
            $upload->setFileName((string)$up->getName());
            $upload->setChunkTitle((string)$up->getChunktitle());
            $upload->setTyp((string)$up->getTyp());
            $upload->setUuid((string)$up->getUuid());
            $upload->setPath((string)$up->getPath());
            $position->addUpload($upload);
        }
        if ($this->productTypeRegistry->getProductType($pos->getProduct()->getType())) {
            $specialProductTransformer = $this->productTypeRegistry->getProductType($pos->getProduct()->getType())->getPositionProductTransformer();

            $specialProductTransformer->fromEntity($position, $pos);
        } else {
            $position->setSpecialProductTypeObject(new DummyProductTypeObject());
        }
    }

    public function fromDocument(\PSC\Shop\OrderBundle\Model\Order\Position $position, \PSC\Shop\EntityBundle\Document\Position $positionDoc)
    {
        if ($positionDoc->getExternalOrderNumber()) {
            $position->setExternalOrderNumber($positionDoc->getExternalOrderNumber());
        }
        $position->setAdditionalInfos($positionDoc->getAdditionalInfos());
        $position->setCustomerInfo((string)$positionDoc->getCustomerInfo());
    }
}
