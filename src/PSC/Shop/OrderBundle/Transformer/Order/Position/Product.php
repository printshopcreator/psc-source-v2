<?php

namespace PSC\Shop\OrderBundle\Transformer\Order\Position;

use PSC\Shop\EntityBundle\Entity\Product as PSCProduct;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Product
{
    public function fromEntity(\PSC\Shop\OrderBundle\Model\Order\Position\Product $product, PSCProduct $productEntity)
    {
        $product->setId($productEntity->getId());
        $product->setTitle((string)$productEntity->getTitle());
        $product->setUuid((string)$productEntity->getUUID());
        $product->setNrIntern((string)$productEntity->getNrIntern());
        $product->setNrExtern((string)$productEntity->getNrExtern());
    }
}
