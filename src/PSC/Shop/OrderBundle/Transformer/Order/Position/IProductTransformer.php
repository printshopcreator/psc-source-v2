<?php

namespace PSC\Shop\OrderBundle\Transformer\Order\Position;

use PSC\Shop\EntityBundle\Document\Embed\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\OrderBundle\Model\Order\Position;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

interface IProductTransformer
{
    public function toDocument();

    public function fromDocument();

    public function fromEntity(Position $position, Orderpos $pos);

    public function toEntity();
}
