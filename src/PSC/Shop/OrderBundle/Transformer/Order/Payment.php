<?php

namespace PSC\Shop\OrderBundle\Transformer\Order;

use PSC\Shop\EntityBundle\Entity\Payment as PSCPayment;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Payment
{
    public function fromEntity(\PSC\Shop\OrderBundle\Model\Order\Payment $payment, PSCPayment $paymentEntity)
    {
        $payment->setUid($paymentEntity->getUID());
        $payment->setTitle($paymentEntity->getTitle());
    }
}
