<?php

namespace PSC\Shop\OrderBundle\Transformer\Order;

use PSC\Shop\EntityBundle\Entity\Shipping as PSCShipping;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Shipping
{
    public function fromEntity(\PSC\Shop\OrderBundle\Model\Order\Shipping $shipping, PSCShipping $shippingEntity)
    {
        $shipping->setUid($shippingEntity->getUID());
        $shipping->setTitle($shippingEntity->getTitle());
    }
}
