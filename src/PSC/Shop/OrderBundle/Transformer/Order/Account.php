<?php

namespace PSC\Shop\OrderBundle\Transformer\Order;

use PSC\Shop\EntityBundle\Entity\Account as PSCAccount;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class Account
{
    public function fromEntity(\PSC\Shop\OrderBundle\Model\Order\Account $account, PSCAccount $accountEntity)
    {
        $account->setId($accountEntity->getId());
        $account->setTitle((string)$accountEntity->getTitle());
        $account->setAppendix((string)$accountEntity->getAppendix());
    }
}
