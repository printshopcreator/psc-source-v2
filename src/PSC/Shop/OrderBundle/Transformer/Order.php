<?php

namespace PSC\Shop\OrderBundle\Transformer;

use PSC\Shop\EntityBundle\Document\Embed\ContactAddress;
use PSC\Shop\OrderBundle\Model\Order\Contact as PSCContact;
use PSC\Shop\OrderBundle\Transformer\Order\Account;
use PSC\Shop\OrderBundle\Transformer\Order\Address;
use PSC\Shop\ContactBundle\Transformer\Contact;
use PSC\Shop\OrderBundle\Transformer\Order\Payment;
use PSC\Shop\OrderBundle\Transformer\Order\Shipping;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Order
 *
 * @author Thomas Peterson
 */
class Order extends Base
{
    private Address $addressTransformer;

    private Contact $contactTransformer;

    private Account $accountTransformer;

    private Payment $paymentTransformer;

    private Shipping $shippingTransformer;
    /**
     * @required
     */
    public function setAddressTransformer(Address $addressTransformer, Contact $contactTransformer, Account $accountTransformer, Payment $paymentTransformer, Shipping $shippingTransformer)
    {
        $this->addressTransformer = $addressTransformer;
        $this->contactTransformer = $contactTransformer;
        $this->accountTransformer = $accountTransformer;
        $this->paymentTransformer = $paymentTransformer;
        $this->shippingTransformer = $shippingTransformer;
    }

    public function fromEntity(\PSC\Shop\OrderBundle\Model\Base $order, \PSC\Shop\EntityBundle\Entity\Order $orderEntity)
    {
        $order->setUuid($orderEntity->getUuid());
        $order->setCreated($orderEntity->getCreated());
        $order->setType($orderEntity->getType());
        $order->setUid($orderEntity->getUID());
        $order->setAlias($orderEntity->getAlias());
        $order->setShop($orderEntity->getShop()->getUID());
        $order->setStatus($orderEntity->getStatus());
        $order->setBasketField1((string)$orderEntity->getBasketfield1());
        $order->setBasketField2((string)$orderEntity->getBasketfield2());
        $order->setNet($orderEntity->getNetto());
        $order->setVat($orderEntity->getSteuer());
        $order->setGross($orderEntity->getBrutto());
        $order->setExternalOrderNumber((string)$orderEntity->getPackage());

        $this->contactTransformer->fromEntity($order->getContact(), $orderEntity->getContact());
        if ($orderEntity->getAccount()) {
            $this->accountTransformer->fromEntity($order->getAccount(), $orderEntity->getAccount());
        }
        $this->paymentTransformer->fromEntity($order->getPayment(), $orderEntity->getPaymentType());
        $this->shippingTransformer->fromEntity($order->getShipping(), $orderEntity->getShippingType());
    }

    public function fromDocument(\PSC\Shop\OrderBundle\Model\Base $order, \PSC\Shop\EntityBundle\Document\Order $orderDoc)
    {
        $invoice = new \PSC\Shop\ContactBundle\Model\Address();
        if ($orderDoc->getInvoiceAddressSaved() == null) {
            $this->addressTransformer->fromDocument($invoice, new ContactAddress());
        } else {
            $this->addressTransformer->fromDocument($invoice, $orderDoc->getInvoiceAddressSaved());
        }
        $order->setInvoiceAddress($invoice);

        $delivery = new \PSC\Shop\ContactBundle\Model\Address();
        if ($orderDoc->getDeliveryAddressSaved() == null) {
            $this->addressTransformer->fromDocument($delivery, new ContactAddress());
        } else {
            $this->addressTransformer->fromDocument($delivery, $orderDoc->getDeliveryAddressSaved());
        }
        $order->setDeliveryAddress($delivery);

        $sender = new \PSC\Shop\ContactBundle\Model\Address();
        if ($orderDoc->getSenderAddressSaved() == null) {
            $this->addressTransformer->fromDocument($sender, new ContactAddress());
        } else {
            $this->addressTransformer->fromDocument($sender, $orderDoc->getSenderAddressSaved());
        }
        $order->setSenderAddress($sender);
        if ($orderDoc->getExternalOrderNumber()) {
            $order->setExternalOrderNumber($orderDoc->getExternalOrderNumber());
        }
    }

    public function toEntity(\PSC\Shop\OrderBundle\Model\Order $order, \PSC\Shop\EntityBundle\Entity\Order $orderEntity)
    {
        $orderEntity->setUuid($order->getUuid());
        $orderEntity->setCreated(new \DateTime());
        $orderEntity->setUpdated(new \DateTime());
        $orderEntity->setAlias($order->getAlias());
        $orderEntity->setNetto($order->getNet());
        $orderEntity->setSteuer($order->getVat());
        $orderEntity->setBrutto($order->getGross());
        $orderEntity->setVersandKosten($order->getShippingCosts());
        $orderEntity->setZahlKosten($order->getPaymentCosts());
        $orderEntity->setEnable($order->isEnable());
        $orderEntity->setInfo($order->getInfo());
        $orderEntity->setDeliverySame(0);
        $orderEntity->setSenderSame(0);
        $orderEntity->setGutscheinAbzugTyp(0);
        $orderEntity->setUseAccountAsInvoice(false);
        $orderEntity->setMwertAlle(json_encode([]));
        $orderEntity->setVersion(1);
        $orderEntity->setShop($this->shopService->getShopByUid($order->getShop()));
        $orderEntity->setShippingType($order->getShipping()->getUid());
        $orderEntity->setPaymentType($order->getPayment()->getUid());
        $orderEntity->setStatus(10);
        $orderEntity->setBasketfield1($order->getBasketField1());
        $orderEntity->setType($order->getType());
        $orderEntity->setBasketfield2($order->getBasketField2());
    }

    public function toDocument(\PSC\Shop\OrderBundle\Model\Order $order, \PSC\Shop\EntityBundle\Document\Order $orderDoc)
    {
        $invoiceDoc = new ContactAddress();
        $this->addressTransformer->toDocument($order->getInvoiceAddress(), $invoiceDoc);
        $orderDoc->setInvoiceAddressSaved($invoiceDoc);

        $deliveryDoc = new ContactAddress();
        $this->addressTransformer->toDocument($order->getDeliveryAddress(), $deliveryDoc);
        $orderDoc->setDeliveryAddressSaved($deliveryDoc);

        $senderDoc = new ContactAddress();
        $this->addressTransformer->toDocument($order->getSenderAddress(), $senderDoc);
        $orderDoc->setSenderAddressSaved($senderDoc);
    }
}
