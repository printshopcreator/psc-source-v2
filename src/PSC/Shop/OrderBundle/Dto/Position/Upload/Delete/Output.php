<?php

namespace PSC\Shop\OrderBundle\Dto\Position\Upload\Delete;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    /**
     * @var bool
     *
     * @OA\Property(type="boolean")
     */
    public bool $success = true;
}
