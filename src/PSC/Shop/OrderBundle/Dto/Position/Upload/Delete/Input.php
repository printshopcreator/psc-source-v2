<?php

namespace PSC\Shop\OrderBundle\Dto\Position\Upload\Delete;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $position = "";
}
