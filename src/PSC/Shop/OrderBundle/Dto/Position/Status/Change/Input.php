<?php

namespace PSC\Shop\OrderBundle\Dto\Position\Status\Change;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $position = "";

    /**
     * @var int
     *
     * @OA\Property(type="int")
     */
    public int $status = 10;
}
