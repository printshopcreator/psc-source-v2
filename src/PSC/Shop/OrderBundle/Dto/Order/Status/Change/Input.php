<?php

namespace PSC\Shop\OrderBundle\Dto\Order\Status\Change;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $order = "";

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $status = "";
}
