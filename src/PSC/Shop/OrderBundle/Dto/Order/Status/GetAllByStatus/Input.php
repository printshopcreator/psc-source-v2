<?php

namespace PSC\Shop\OrderBundle\Dto\Order\Status\GetAllByStatus;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $status = "";
}
