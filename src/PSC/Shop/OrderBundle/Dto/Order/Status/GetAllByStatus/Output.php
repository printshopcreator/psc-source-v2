<?php

namespace PSC\Shop\OrderBundle\Dto\Order\Status\GetAllByStatus;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Shop\OrderBundle\Model\Order;

class Output
{
    public function __construct(array $shops)
    {
        $this->data = $shops;
        $this->count = count($shops);
    }

    /**
     * @var Order[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\OrderBundle\Model\Order::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
