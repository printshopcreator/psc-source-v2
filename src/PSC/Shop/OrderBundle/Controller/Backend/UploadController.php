<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * UploadController fürs Backend
 *
 * @package    PSC\Shop\Order\Backend
 * @subpackage Controller
 */
class UploadController extends AbstractController
{
    /**
     * Delete Seite
     *
     * @Route("/upload/deleteAll/{uuid}", name="psc_shop_order_backend_upload_deleteAll")
     * @Template()
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param string $uuid
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAllAction(
        Request $request,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        $uuid = ""
    ) {

        $selectedShop = $shopService->getSelectedShop();
        $order = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                foreach ($order->getPositions() as $position) {
                    foreach ($position->getUploads() as $upload) {
                        if (file_exists($upload->getPath())) {
                                unlink($upload->getPath());
                        }
                    }
                }
                return $this->redirectToRoute('psc_shop_order_backend_list_index');
            }
            return $this->redirectToRoute('psc_shop_order_backend_list_index');
        }

        return array(
            'order' => $order,
            'form' => $form->createView()
        );
    }
}
