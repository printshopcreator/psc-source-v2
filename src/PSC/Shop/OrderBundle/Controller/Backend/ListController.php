<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Controller\Backend;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\EntityBundle\Document\Order;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\OrderBundle\Form\Backend\SearchType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * ListController fürs Backend
 *
 * @package    PSC\Shop\Order\Backend
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/list/index", name="psc_shop_order_backend_list_index")
     * @Template()
     *
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $em
     * @param \PSC\System\SettingsBundle\Service\Shop $shop
     * @param Session $session
     * @param FilterBuilderUpdaterInterface $query_builder_updater
     * @param Status $statusService
     * @param \PSC\Shop\OrderBundle\Service\Order $orderService
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(
        Request $request,
        PaginatorInterface $paginator,
        EntityManagerInterface $em,
        \PSC\System\SettingsBundle\Service\Shop $shop,
        Session $session,
        FilterBuilderUpdaterInterface $query_builder_updater,
        Status $statusService,
        \PSC\Shop\OrderBundle\Service\Order $orderService
    ) {

        $selectedShop = $shop->getSelectedShop();
        $userRepository = $em->getRepository('PSC\Shop\EntityBundle\Entity\Order');
        $qb = $userRepository->createQueryBuilder('orders')
            ->leftJoin('orders.contact', 'contact')
            ->leftJoin('orders.invoiceAddress', 'invoiceAddress')
            ->orderBy('orders.uid', 'desc');
        $qbCount = $userRepository->createQueryBuilder('orders')
            ->select('count(orders.uid)')
            ->leftJoin('orders.contact', 'contact')
            ->leftJoin('orders.invoiceAddress', 'invoiceAddress')
            ->orderBy('orders.uid', 'desc');
        $form = $this->createForm(SearchType::class);
        if ($request->isMethod('POST')) {
            $session->set('order_sort_page', 1);
            $form->handleRequest($request);
        }

        $query_builder_updater->addFilterConditions($form, $qb);
        $query_builder_updater->addFilterConditions($form, $qbCount);
        if ($request->get('direction', false)) {
            $session->set('order_sort_direction', $request->get('direction', 'desc'));
            $session->set('order_sort_field', $request->get('sort', 'orders.uid'));
        }
        if ($request->get('page', false)) {
            $session->set('order_sort_page', $request->get('page', 1));
        }

        $qb->andWhere('orders.shop = :shop_id')
            ->setParameter("shop_id", $selectedShop->getUid());
        $qbCount->andWhere('orders.shop = :shop_id')
            ->setParameter("shop_id", $selectedShop->getUid());
        $query = $qb->getQuery();
        $count = $qbCount->getQuery()->getSingleScalarResult();
        $query->setHint('knp_paginator.count', $count);
        $pagination = $paginator->paginate($query, $request->query->getInt('page', $session->get('order_sort_page', 1)), 30, [
                'defaultSortFieldName' => $session->get('order_sort_field', 'orders.uid'),
                'defaultSortDirection' => $session->get('order_sort_direction', 'desc'),
                'distinct' => false
            ]);
        return array(
            'pagination' => $pagination,
            'orderStatuse' => $statusService,
            'orderService' => $orderService,
            'form' => $form->createView()
        );
    }

    /**
     * Switch Pos Seite
     *
     * @Route("/list/switchstatus/{order}/{status}", name="psc_shop_order_backend_list_switchstatus")
     * @Template()
     *
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param SessionInterface $session
     * @param EntityManagerInterface $emService
     * @param \PSC\Shop\QueueBundle\Service\Event\Manager $eventManagerService
     * @param string $order
     * @param int|string $status
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function switchStatusAction(\PSC\System\SettingsBundle\Service\Shop $shopService, SessionInterface $session, EntityManagerInterface $emService, \PSC\Shop\QueueBundle\Service\Event\Manager $eventManagerService, $order = "", $status = 10)
    {
        $selectedShop = $shopService->getSelectedShop();
/**
         * @var \PSC\Shop\EntityBundle\Entity\Order $order
         */
        $order = $emService
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $order));
        $order->setStatus($status);
        $emService->persist($order);
        $emService->flush();
        $session->getFlashBag()->add('success', 'Status erfolgreich gesetzt');
        $notify = new \PSC\Shop\QueueBundle\Event\Order\Status\Change();
        $notify->setShop($selectedShop->getUID());
        $notify->setOrder($order->getUuid());
        $notify->setStatus($order->getStatus());
        $eventManagerService->addJob($notify);
        return $this->redirectToRoute('psc_shop_order_backend_list_index');
    }

    /**
     * Switch Pos Seite
     *
     * @Route("/list/switchstatuspos/{position}/{status}", name="psc_shop_order_backend_list_switchstatus_pos")
     * @Template()
     *
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param SessionInterface $session
     * @param EntityManagerInterface $emService
     * @param \PSC\Shop\QueueBundle\Service\Event\Manager $eventManagerService
     * @param string $position
     * @param int|string $status
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function switchStatusPosAction(\PSC\System\SettingsBundle\Service\Shop $shopService, SessionInterface $session, EntityManagerInterface $emService, \PSC\Shop\QueueBundle\Service\Event\Manager $eventManagerService, $position = "", $status = 10)
    {
        $selectedShop = $shopService->getSelectedShop();
/**
         * @var \PSC\Shop\EntityBundle\Entity\Orderpos $orderPos
         */
        $orderPos = $emService
            ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $position));
        $orderPos->setStatus($status);
        $emService->persist($orderPos);
        $emService->flush();
        $session->getFlashBag()->add('success', 'Status erfolgreich gesetzt');
        $notify = new \PSC\Shop\QueueBundle\Event\Position\Status\Change();
        $notify->setShop($selectedShop->getUID());
        $notify->setPosition($orderPos->getUuid());
        $notify->setStatus($orderPos->getStatus());
        $eventManagerService->addJob($notify);
        return $this->redirectToRoute('psc_shop_order_backend_list_index');
    }
}
