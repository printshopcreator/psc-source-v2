<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use JasperReport\Datasource\ArrayDatasource;
use JasperReport\JasperReport;
use JasperReport\OutputAdapter\FPDFOutputAdapter;
use JasperReport\OutputAdapter\PdflibOutputAdapter;
use JasperReport\Page;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactDeliveryType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactInvoiceType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactSenderType;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use PSC\System\SettingsBundle\Service\Printing;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

require_once(__DIR__ . '/../../../EntityBundle/Lagacy/TP_Basket_Item.php');

/**
 * DetailController fürs Backend
 *
 * @package    PSC\Shop\Order\Backend
 * @subpackage Controller
 */
class PrintController extends AbstractController
{
    /**
     * Detail Seite
     *
     * @Route("/detail/print/{type}/{uuid}", name="psc_shop_order_backend_detail_print")
     * @Template()
     *
     * @param Request $request
     * @param Printing $printingService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param int $type
     * @param string $uuid
     * @return StreamedResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function printAction(
        Request $request,
        Printing $printingService,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        $type = Printing::INVOICE,
        $uuid = ""
    ) {

        if ($uuid == 'last') {
            $selectedShop = $shopService->getSelectedShop();
/** @var Order $order */
            $order = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->createQueryBuilder('orders')
                ->andWhere('orders.shop = :shop_id')
                ->setParameter("shop_id", $selectedShop->getUid())
                ->setMaxResults(1)
                ->orderBy('orders.uid', 'DESC')
                ->getQuery()->getSingleResult();
        } else {
        /** @var Order $order */
            $order = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(array('uuid' => $uuid));
        }
        $content = $printingService->generateOrder($order, $type);
        $response = new StreamedResponse(function () use ($content) {

                echo $content;
        });
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Cache-Control', '');
        $response->headers->set('Content-Length', strlen($content));
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));
        $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $printingService->getFileName());
        $response->headers->set('Content-Disposition', $contentDisposition);
        $response->prepare($request);
        return $response;
    }

    /**
     * Detail Seite
     *
     * @Route("/detail/printpos/{type}/{uuid}/{posuuid}", name="psc_shop_order_backend_detail_print_pos")
     * @Template()
     *
     * @param Request $request
     * @param Printing $printingService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param int $type
     * @param string $uuid
     * @param string $posuuid
     * @return StreamedResponse
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function printPosAction(
        Request $request,
        Printing $printingService,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        $type = Printing::INVOICE,
        $uuid = "",
        $posuuid = ""
    ) {

        if ($uuid == 'last') {
            $selectedShop = $shopService->getSelectedShop();
/** @var Order $order */
            $order = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->createQueryBuilder('orders')
                ->andWhere('orders.shop = :shop_id')
                ->setParameter("shop_id", $selectedShop->getUid())
                ->setMaxResults(1)
                ->orderBy('orders.uid', 'DESC')
                ->getQuery()->getSingleResult();
        } else {
        /** @var Order $order */
            $order = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(array('uuid' => $uuid));
        }

        $content = "";
/** @var Orderpos $position */
        foreach ($order->getPositions() as $position) {
            if ($position->getUuid() == $posuuid) {
                $content = $printingService->generatePosition($position, $type);
            }
        }

        if ($content == "") {
            $content = $printingService->generatePosition($order->getPositions()[0], $type);
        }

        $response = new StreamedResponse(function () use ($content) {

                echo $content;
        });
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Cache-Control', '');
        $response->headers->set('Content-Length', strlen($content));
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));
        switch ($type) {
            case Printing::INVOICE:
                                                                                                                                                                                                                                                                                                           $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'invoice_' . $order->getAlias() . '.pdf');

                break;
            case Printing::DELIVERY:
                                                                                                                                                                                                                                                                                                       $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'delivery_' . $order->getAlias() . '.pdf');

                break;
            case Printing::LABEL:
                                                                                                                                                                                                                                                                                                       $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'label_' . $order->getAlias() . '.pdf');

                break;
            case Printing::ORDER:
                                                                                                                                                                                                                                                                                                       $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'order_' . $order->getAlias() . '.pdf');

                break;
            case Printing::OFFER:
                                                                                                                                                                                                                                                                                                       $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'offer_' . $order->getAlias() . '.pdf');

                break;
            case Printing::STORNO:
                                                                                                                                                                                                                                                                                                       $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'storno_' . $order->getAlias() . '.pdf');

                break;
            case Printing::JOBTICKET:
                                                                                                                                                                                                                                                                                                       $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'jobticket_' . $order->getAlias() . '.pdf');

                break;
            case Printing::JOBTICKET_PRINTPARTNER:
                                                                                                                                                                                                                                                                                                       $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'jobticket_pp_' . $order->getAlias() . '.pdf');

                break;
        }


        $response->headers->set('Content-Disposition', $contentDisposition);
        $response->prepare($request);
        return $response;
    }
}
