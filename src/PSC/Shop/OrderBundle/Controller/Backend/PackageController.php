<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Upload;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactDeliveryType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactInvoiceType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactSenderType;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use PSC\Shop\QueueBundle\Event\Package\Download;
use PSC\Shop\QueueBundle\Service\Event\Registry;
use PSC\Shop\QueueBundle\Service\Queue\Manager;
use PSC\System\SettingsBundle\Service\Package;
use PSC\System\SettingsBundle\Service\Printing;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use ZipArchive;

require_once(__DIR__ . '/../../../EntityBundle/Lagacy/TP_Basket_Item.php');

/**
 * DetailController fürs Backend
 *
 * @package    PSC\Shop\Order\Backend
 * @subpackage Controller
 */
class PackageController extends AbstractController
{
    /**
     * Package Download
     *
     * @Route("/detail/package/download/{uuid}", name="psc_shop_order_backend_detail_package_download")
     * @param Registry $eventRegistry
     * @param \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry
     * @param Manager $queueManager
     * @param EntityManagerInterface $entityManager
     * @param string $uuid
     * @return BinaryFileResponse
     */
    public function downloadAction(
        Registry $eventRegistry,
        \PSC\Shop\QueueBundle\Service\Queue\Registry $queueRegistry,
        Manager $queueManager,
        EntityManagerInterface $entityManager,
        $uuid = ""
    ) {
        /** @var Download $event */
        $event = $eventRegistry->get('package_download');
        $event->setOrder($uuid);
        $queueObj = $queueRegistry->get('package');
/** @var Order $order */
        $order = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('uuid' => $uuid));
        $queue = $queueManager->getQueueByEventTypeAndShop('package_download', $order->getShop()->getUID());
        if (!$queue) {
            die("Bitte Aktion für Paket herrunterladen anlegen");
        }

        $name = $queueObj->execute($event, $queue);
        $response = new BinaryFileResponse(sys_get_temp_dir() . "/" . $name);
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Cache-Control', '');
        $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $name);
        $response->headers->set('Content-Disposition', $contentDisposition);
        return $response;
    }

    /**
     * Printpartner Download
     *
     * @Route("/detail/package/printpartner/{uuid}", name="psc_shop_order_backend_detail_package_printpartner")
     * @Template()
     *
     * @param Printing $printingService
     * @param EntityManagerInterface $entityManager
     * @param string $uuid
     * @return BinaryFileResponse
     * @throws \Mpdf\MpdfException
     * @throws \Throwable
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Syntax
     */
    public function printpartnerAction(Printing $printingService, EntityManagerInterface $entityManager, $uuid = "")
    {

        if ($uuid == 'last') {
/** @var Orderpos $orderpos */
            $orderpos = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
                ->getLastEntity();
        } else {
        /** @var Orderpos $orderpos */
            $orderpos = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
                ->findOneBy(array('uuid' => $uuid));
        }

        $archive = new ZipArchive();
        if (file_exists(sys_get_temp_dir() . "/" . md5($uuid) . '.zip')) {
            $archive->open(sys_get_temp_dir() . "/" . md5($uuid) . '.zip', ZIPARCHIVE::OVERWRITE);
        } else {
            $archive->open(sys_get_temp_dir() . "/" . md5($uuid) . '.zip', ZIPARCHIVE::CREATE);
        }

        $archive->addFromString('jobticket.pdf', $printingService->generatePosition($orderpos, Printing::JOBTICKET_PRINTPARTNER));
/** @var Upload $upload */
        foreach ($orderpos->getUploads() as $upload) {
            $archive->addFile($upload->getPath(), $upload->getTyp() . '_' . $upload->getName());
        }

        if (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf')) {
            $archive->addFile('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/final.pdf', $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '_final.pdf');
            $addzip = true;
        } elseif (file_exists('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf')) {
            $archive->addFile('/data/www/old/market/templateprint/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf', $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '_preview.pdf');
        }

        if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf')) {
            $archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/preview.pdf', $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '_preview.pdf');
        }

        if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/production.pdf')) {
            $archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/production.pdf', $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf');
        }

        if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getUid() . '_' . $orderpos->getPos() . '.pdf')) {
            $archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getUid() . '_' . $orderpos->getPos() . '.pdf', $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf');
        }

        if (file_exists('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf')) {
            $archive->addFile('/data/www/old/market/steplayouter/basket/' . $orderpos->getOrder()->getUid() . '/' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf', $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '/' . $orderpos->getOrder()->getAlias() . '_' . $orderpos->getPos() . '.pdf');
        }


        $archive->close();
        $response = new BinaryFileResponse(sys_get_temp_dir() . "/" . md5($uuid) . '.zip');
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Cache-Control', '');
        $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $uuid . '.zip');
        $response->headers->set('Content-Disposition', $contentDisposition);
        return $response;
    }
}
