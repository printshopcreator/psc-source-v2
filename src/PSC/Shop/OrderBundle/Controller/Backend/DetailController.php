<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\OrderBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Product;
use PSC\Shop\EntityBundle\Entity\Upload;
use PSC\Shop\OrderBundle\Form\Backend\OrderStatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactDeliveryType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactInvoiceType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactSenderType;
use PSC\Shop\OrderBundle\Form\Backend\ProdType;
use PSC\Shop\OrderBundle\Form\Backend\ProdinfoType;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use PSC\Shop\QueueBundle\Event\Order\Create;
use PSC\Shop\QueueBundle\Event\Order\Updated;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\PluginBundle\Form\Chain\Section;
use PSC\System\SettingsBundle\Service\PaperDB;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

require_once(__DIR__ . '/../../../EntityBundle/Lagacy/TP_Basket_Item.php');

/**
 * DetailController fürs Backend
 *
 * @package    PSC\Shop\Order\Backend
 * @subpackage Controller
 */
class DetailController extends AbstractController
{
    /**
     * Detail Seite
     *
     * @Route("/detail/show/{uuid}/{enableDelivery}", defaults={"enableDelivery" = 0}, name="psc_shop_order_backend_detail_show")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param Section $sectionService
     * @param Shop $shopService
     * @param DocumentManager $mongoService
     * @param EntityManagerInterface $entityManager
     * @param Manager $eventManagerService
     * @param Status $statusService
     * @param PaperDB $paperService
     * @param SessionInterface $session
     * @param string $uuid
     * @param boolean $enableDelivery
     *
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function showAction(
        Request $request,
        Field $fieldService,
        Section $sectionService,
        Shop $shopService,
        DocumentManager $mongoService,
        EntityManagerInterface $entityManager,
        Manager $eventManagerService,
        Status $statusService,
        PaperDB $paperService,
        SessionInterface $session,
        FormFactoryInterface $formFactory,
        $uuid = "",
        $enableDelivery = false
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Order);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Order);
        $customSections = $sectionService->get(\PSC\System\PluginBundle\Form\Interfaces\Section::Order);
        $selectedShop = $shopService->getSelectedShop();
/** @var \PSC\Shop\EntityBundle\Document\Shop $shop */
        $shop = $mongoService
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$selectedShop->getUid()));
        if ($shop === null) {
            $shop = new \PSC\Shop\EntityBundle\Document\Shop();
            $shop->setUid($selectedShop->getUid());
        }

        /** @var Order $order */
        $order = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));
/** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
        $orderObj = $mongoService
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('uid' => (string)$order->getUid()));
        if (!$orderObj) {
            $orderObj = new \PSC\Shop\EntityBundle\Document\Order();
            $orderObj->setUid((string)$order->getUid());
        }

        $formStatus = $this->createForm(OrderStatusType::class, $order);
        $formStatus->handleRequest($request);
        if ($formStatus->isSubmitted() && $formStatus->isValid()) {
            $entityManager->persist($order);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'Status erfolgreich gesetzt');
            $notify = new \PSC\Shop\QueueBundle\Event\Order\Status\Change();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $notify->setStatus($order->getStatus());
            $eventManagerService->addJob($notify);
        }

        $legacyMode = false;
        $invoiceObj = $orderObj->getInvoiceAddressSaved();
        $deliveryObj = $orderObj->getDeliveryAddressSaved();
        $senderObj = $orderObj->getSenderAddressSaved();
        if ($invoiceObj == null) {
            $legacyMode = true;
            $invoiceObj = new \PSC\Shop\EntityBundle\Document\Embed\ContactAddress();
            $invoiceObj->setCompany($order->getInvoiceAddress()->getCompany());
            $invoiceObj->setCompany2($order->getInvoiceAddress()->getCompany2());
            $invoiceObj->setFirstname($order->getInvoiceAddress()->getFirstname());
            $invoiceObj->setLastname($order->getInvoiceAddress()->getLastname());
            $invoiceObj->setStreet($order->getInvoiceAddress()->getStreet());
            $invoiceObj->setHouseNumber($order->getInvoiceAddress()->getHouseNumber());
            $invoiceObj->setZip($order->getInvoiceAddress()->getZip());
            $invoiceObj->setCity($order->getInvoiceAddress()->getCity());
            $invoiceObj->setEmail($order->getInvoiceAddress()->getEmail());
            $invoiceObj->setPhone($order->getInvoiceAddress()->getPhone());
            $invoiceObj->setCountry($order->getInvoiceAddress()->getCountry());
        }
        if ($deliveryObj == null) {
            $legacyMode = true;
            $deliveryObj = new \PSC\Shop\EntityBundle\Document\Embed\ContactAddress();
            $deliveryObj->setCompany($order->getDeliveryAddress()->getCompany());
            $deliveryObj->setCompany2($order->getDeliveryAddress()->getCompany2());
            $deliveryObj->setFirstname($order->getDeliveryAddress()->getFirstname());
            $deliveryObj->setLastname($order->getDeliveryAddress()->getLastname());
            $deliveryObj->setStreet($order->getDeliveryAddress()->getStreet());
            $deliveryObj->setHouseNumber($order->getDeliveryAddress()->getHouseNumber());
            $deliveryObj->setZip($order->getDeliveryAddress()->getZip());
            $deliveryObj->setCity($order->getDeliveryAddress()->getCity());
            $deliveryObj->setEmail($order->getDeliveryAddress()->getEmail());
            $deliveryObj->setPhone($order->getDeliveryAddress()->getPhone());
            $deliveryObj->setCountry($order->getDeliveryAddress()->getCountry());
        }

        $formInvoice = $this->createForm(ContactInvoiceType::class, $invoiceObj);
        $formDelivery = $this->createForm(ContactDeliveryType::class, $deliveryObj);
        $formSender = $this->createForm(ContactSenderType::class, $senderObj);
        $formInvoice->handleRequest($request);
        $formSender->handleRequest($request);
        $formDelivery->handleRequest($request);
        if (!$legacyMode && $formInvoice->isSubmitted() && $formInvoice->isValid()) {
            $orderObj->setInvoiceAddressSaved($invoiceObj);
            $mongoService->persist($orderObj);
            $mongoService->flush();
            $notify = new Updated();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $eventManagerService->addJob($notify);
        }
        if (!$legacyMode && $formDelivery->isSubmitted() && $formDelivery->isValid()) {
            $orderObj->setDeliveryAddressSaved($deliveryObj);
            $mongoService->persist($orderObj);
            $mongoService->flush();
            $notify = new Updated();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $eventManagerService->addJob($notify);
        }

        if (!$legacyMode && $formSender->isSubmitted() && $formSender->isValid()) {
            $orderObj->setSenderAddressSaved($senderObj);
            $mongoService->persist($orderObj);
            $mongoService->flush();
            $notify = new Updated();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $eventManagerService->addJob($notify);
        }

        $positions = array();
/** @var Orderpos $pos */
        foreach ($order->getPositions() as $pos) {
            if ($pos->getProduct()->getOriginalProduct() != 0) {
            /** @var Product $objProduct */
                $objProduct = $mongoService
                    ->getRepository('PSCEntityBundle:Product')
                    ->findOneBy(['uid' => (string)$pos->getProduct()->getOriginalProduct()]);
            } else {
            /** @var Product $objProduct */
                $objProduct = $mongoService
                    ->getRepository('PSCEntityBundle:Product')
                    ->findOneBy(['uid' => (string)$pos->getProduct()->getUid()]);
            }

            /** @var Position $objDoc */
            $objDoc = $mongoService
                ->getRepository('PSCEntityBundle:Position')
                ->findOneBy(['uid' => (string)$pos->getId()]);
            $objPosition = unserialize(($pos->getData()));
            if ($pos->hasCalcXml()) {
                $paperContainer = new PaperContainer();
                $paperContainer->parse(simplexml_load_string($selectedShop->getInstall()->getPaperContainer()));
                $engine = new Engine();
                $engine->setPaperRepository($paperService);
                $engine->setPaperContainer($paperContainer);
                if ($selectedShop->getInstall()->getCalcTemplates()) {
                    $engine->setTemplates('<root>' . $selectedShop->getInstall()->getCalcTemplates() . '</root>');
                }
                $engine->loadString($pos->getCalcXml());
                $engine->setFormulas($order->getShop()->getFormel());
                $engine->setParameters($order->getShop()->getParameter());
                $engine->setVariables($objPosition->getOptions());
                if ($objDoc) {
                    $engine->setSavedCalcReferences($objDoc->getCalcReferences());
                }
                if (isset($objPosition->getOptions()['kalk_artikel'])) {
                    $engine->setActiveArticle($objPosition->getOptions()['kalk_artikel']);
                }
                $articleCalc = $engine->getArticle();
            } else {
                $articleCalc = null;
            }

            if ($objProduct) {
                $formData = ['position' => $pos->getUuid(), 'printPartnerEmail' => $objProduct->getPrintPartnerEmail()];
                $formDatas = ['positioninfo' => $pos->getUuid(), 'customerInfo' => $objDoc ? $objDoc->getCustomerInfo() : ''];
            } else {
                $formData = ['position' => $pos->getUuid(), 'printPartnerEmail' => ''];
                $formDatas = ['positioninfo' => $pos->getUuid(), 'customerInfo' => ''];
            }

            $formProd = $formFactory->createNamedBuilder('formProd' . $pos->getId(), ProdType::class, $formData)->getForm();
            $formProdinfo = $formFactory->createNamedBuilder('formProdinfo' . $pos->getId(), ProdinfoType::class, $formDatas)->getForm();
            $formProdinfo->handleRequest($request);
            if ($formProdinfo->isSubmitted() && $formProdinfo->isValid()) {
                $data = $formProdinfo->getData();
                $objDoc->setCustomerInfo($data['customerInfo']);
                $mongoService->persist($objDoc);
                $mongoService->flush();
            }

            $formProd->handleRequest($request);
            if ($formProd->isSubmitted() && $formProd->isValid()) {
                $data = $formProd->getData();
                $notify = new Notify();
                $notify->setShop($selectedShop->getUID());
                $notify->setEmail($data['printPartnerEmail']);
                $notify->setPosition($pos->getUuid());
                $eventManagerService->addJob($notify);
            }

            $positions[] = array(
                'obj' => $pos,
                'formProd' => $formProd->createView(),
                'formProdinfo' => $formProdinfo->createView(),
                'objDoc' => $objDoc,
                'objCalc' => $articleCalc
            );
        }
        return array(
            'order' => $order,
            'orderObj' => $orderObj,
            'positions' => $positions,
            'formInvoice' => $formInvoice->createView(),
            'formDelivery' => $formDelivery->createView(),
            'formSender' => $formSender->createView(),
            'orderStatuse' => $statusService,
            'formStatus' => $formStatus->createView(),
            'shop' => $shop,
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'customSections' => $customSections
        );
    }

    /**
     * Switch Pos Seite
     *
     * @Route("/detail/switchposstatus/{order}/{pos}/{status}", name="psc_shop_order_backend_detail_switchposstatus")
     * @Template()
     *
     * @param EntityManagerInterface $entityManagerService
     * @param Manager $eventManagerService
     * @param Shop $shopService
     * @param string $order
     * @param string $pos
     * @param int|string $status
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @internal param Request $request
     */
    public function switchPosStatusAction(EntityManagerInterface $entityManagerService, Manager $eventManagerService, Shop $shopService, $order = "", $pos = "", $status = 10)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/**
         * @var Orderpos $pos
         */
        $pos = $entityManagerService
            ->getRepository('PSC\Shop\EntityBundle\Entity\Orderpos')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $pos));
        $pos->setStatus($status);
        $entityManagerService->persist($pos);
        $entityManagerService->flush();
        $notify = new Change();
        $notify->setShop($selectedShop->getUID());
        $notify->setPosition($pos->getUuid());
        $notify->setStatus($pos->getStatus());
        $eventManagerService->addJob($notify);
        return $this->redirectToRoute('psc_shop_order_backend_detail_show', array('uuid' => $order));
    }

    /**
     * Switch Pos Seite
     *
     * @Route("/package/exported/{uuid}", name="psc_shop_order_backend_change_exported")
     * @Template()
     *
     * @param string $uuid
     * @return RedirectResponse
     */
    public function setPackageExportedAction(DocumentManager $mongoService, EntityManagerInterface $entityManagerService, Manager $eventManagerService, Shop $shopService, $uuid = "")
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Order $order */
        $order = $entityManagerService
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));
        $qb = $entityManagerService->createQueryBuilder();
        $q = $qb->update('PSC\Shop\EntityBundle\Entity\Orderpos', 'o')
            ->set('o.renderPrint', '?1')
            ->where('o.order = ?2')
            ->setParameter(1, 1)
            ->setParameter(2, $order->getUID())
            ->getQuery();
        $q->execute();
/** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
        $orderObj = $mongoService
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('uid' => (string)$order->getUid()));
        $orderObj->setPackageExported(false);
        $mongoService->persist($orderObj);
        $mongoService->flush();
        $notify = new \PSC\Shop\QueueBundle\Event\Package\Create();
        $notify->setShop($selectedShop->getUID());
        $notify->setOrder($order->getUuid());
        $eventManagerService->addJob($notify);
        return $this->redirectToRoute('psc_shop_order_backend_detail_show', array('uuid' => $uuid));
    }

    /**
     * Switch Pos Seite
     *
     * @Route("/detail/create/new/order/status/{uuid}", name="psc_shop_order_backend_create_new_order_status")
     * @Template()
     *
     * @param string $uuid
     * @return RedirectResponse
     */
    public function createNewOrderStatus(DocumentManager $mongoService, EntityManagerInterface $entityManagerService, Manager $eventManagerService, Shop $shopService, $uuid = "")
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Order $order */
        $order = $entityManagerService
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));
        $qb = $entityManagerService->createQueryBuilder();
        $q = $qb->update('PSC\Shop\EntityBundle\Entity\Orderpos', 'o')
            ->set('o.renderPrint', '?1')
            ->where('o.order = ?2')
            ->setParameter(1, 1)
            ->setParameter(2, $order->getUID())
            ->getQuery();
        $q->execute();
/** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
        $orderObj = $mongoService
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('uid' => (string)$order->getUid()));
        $orderObj->setPackageExported(false);
        $mongoService->persist($orderObj);
        $mongoService->flush();
        $notify = new Create();
        $notify->setShop($selectedShop->getUID());
        $notify->setOrder($order->getUuid());
        $eventManagerService->addJob($notify);
        return $this->redirectToRoute('psc_shop_order_backend_detail_show', array('uuid' => $uuid));
    }

    /**
     * SDelete
     *
     * @Route("/detail/delete/{uuid}", name="psc_shop_order_backend_detail_delete")
     * @Template()
     *
     * @param string $uuid
     * @return RedirectResponse
     */
    public function deleteOrder(DocumentManager $mongoService, EntityManagerInterface $entityManagerService, Manager $eventManagerService, Shop $shopService, $uuid = "")
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Order $order */
        $order = $entityManagerService
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));
/** @var Orderpos $pos */
        foreach ($order->getPositions() as $pos) {
/** @var Upload $upload */
            foreach ($pos->getUploads() as $upload) {
                $entityManagerService->remove($upload);
            }
            $entityManagerService->remove($pos);
        }
        $entityManagerService->remove($order);
        $entityManagerService->flush();
        return $this->redirectToRoute('psc_shop_order_backend_list_index');
    }
}
