<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Frontend\Product;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\NewsBundle\Form\Backend\DeleteType;
use PSC\Shop\ProductBundle\Form\Backend\ProductSearchType;
use PSC\System\PluginBundle\Service\ProductType;
use PSC\System\SettingsBundle\Service\Printing;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Offercontroller fürs Frontend
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 * @Route("/offer")
 */
class OfferController extends AbstractController
{
    /**
     * Default Seite
     *
     *
     * @Route("/{uuid}", name="psc_shop_product_offer_print")
     *
     * @return StreamedResponse
     */
    public function printAction(Request $request, Printing $printingService, EntityManagerInterface $entityManager, $uuid)
    {
        require_once(__DIR__ . '/../../../../EntityBundle/Lagacy/TP_Basket_Item.php');
        $options = array();
        $count = 1;
        $basketItem = new \TP_Basket_Item();
        $netto = 0;
        $steuer = 0;
        $brutto = 0;
        if (isset($_SESSION['Basket']['TempProduct'][$uuid])) {
        /** @var \TP_Basket_Item $basketItem */
            $basketItem = unserialize(serialize($_SESSION['Basket']['TempProduct'][$uuid]));
            $options = $basketItem->getOptions();
            $count = $basketItem->getCount();
            $netto = $basketItem->getNetto();
            $steuer = $basketItem->getSteuer();
            $brutto = $basketItem->getBrutto();
        }

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
            ->findOneBy(array('uuid' => $uuid));
        $content = $printingService->generateProductOffer($product, $this->getUser() ?? new Contact(), $count, $options, $netto, $steuer, $brutto);
        $response = new StreamedResponse(function () use ($content) {

                echo $content;
        });
        $response->headers->set('Content-Type', 'application/pdf');
        $response->headers->set('Cache-Control', '');
        $response->headers->set('Content-Length', strlen($content));
        $response->headers->set('Last-Modified', gmdate('D, d M Y H:i:s'));
        $contentDisposition = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'offer_' . date('Y-m-d') . '.pdf');
        $response->headers->set('Content-Disposition', $contentDisposition);
        $response->prepare($request);
        return $response;
    }
}
