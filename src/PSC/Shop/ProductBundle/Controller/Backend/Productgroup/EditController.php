<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Backend\Productgroup;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Document\Product;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\EntityBundle\Entity\Shipping;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Helper\MediaManager;
use PSC\Shop\ProductBundle\Document\ProductGroup\History as PSCHistory;
use PSC\Shop\ProductBundle\Form\Backend\Productgroup\ProductgroupEditType;
use PSC\Shop\ProductBundle\Form\Backend\Productgroup\ProductgroupType;
use PSC\Shop\ProductBundle\PSCShopProductBundle;
use PSC\Shop\ShippingBundle\Form\Backend\DeleteType;
use PSC\Shop\ShippingBundle\Form\Backend\ShippingType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * EditController fürs Backend
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;
    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }
    /**
     * edit
     *
     * @Route("/edit/save/lang/data/{uid}", name="backend_production_productgroup_edit_save_lang_data")
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function saveLangDataAction(
        Request $request,
        EntityManagerInterface $entityManager,
        $uid
    ) {
        $productgroup = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findOneBy(['uid' => $uid]);
        if ($productgroup) {
            $tmp = $productgroup->getLangData();
            $tmp[$request->get('langcode')] = ['title' => $request->get('title'), 'uuid' => Uuid::uuid4()];
            $productgroup->setLangData($tmp);
            $entityManager->persist($productgroup);
            $entityManager->flush();
        }

        return new JsonResponse(['success' => true, 'langcode' => $request->get('langcode')]);
    }

    /**
     * edit
     *
     * @Route("/edit/add/lang/data/{uid}", name="backend_production_productgroup_edit_add_lang_data")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return JsonResponse
     */
    public function addLangDataAction(
        Request $request,
        EntityManagerInterface $entityManager,
        $uid
    ) {
        $productgroup = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findOneBy(['uid' => $uid]);
        if ($productgroup) {
            $tmp = $productgroup->getLangData();
            if (!isset($tmp[$request->get('langcode')])) {
                $tmp[$request->get('langcode')] = ['title' => $productgroup->getTitle(), 'uuid' => Uuid::uuid4()];
                $productgroup->setLangData($tmp);
                $entityManager->persist($productgroup);
                $entityManager->flush();
            }
        }

        return new JsonResponse(['success' => true, 'langcode' => $request->get('langcode')]);
    }

    /**
     * edit
     *
     * @Route("/edit/fetch/lang/data/{uid}", name="backend_production_productgroup_edit_fetch_lang_data")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return JsonResponse
     */
    public function fetchLangDataAction(
        Request $request,
        EntityManagerInterface $entityManager,
        $uid
    ) {
        $productgroup = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findOneBy(['uid' => $uid]);
        if ($productgroup) {
            $tmp = array();
            foreach ($productgroup->getLangData() as $key => $row) {
                $tmp[] = [
                    'code' => $key,
                    'title' => $row['title'],
                    'uuid' => $row['uuid']
                ];
            }

            return new JsonResponse(['success' => true, 'langData' => $tmp]);
        }

        return new JsonResponse(['success' => true, 'langData' => []]);
    }

    /**
     * edit
     *
     * @Route("/edit/check/url/{url}", name="backend_production_productgroup_check_url")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param $url
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function checkUrlAction(
        Request $request,
        EntityManagerInterface $entityManager,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        $url
    ) {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $products = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findBy(array('url' => $url, 'shop' => $selectedShop));
        if (count($products) > 0) {
            $tmp = array();
        /** @var Productgroup $product */
            foreach ($products as $product) {
                $tmp[] = $product->getTitle();
            }
            return new JsonResponse(['success' => true, 'found' => true, 'error' => implode(", ", $tmp)]);
        }

        return new JsonResponse(['success' => true, 'found' => false]);
    }

    /**
     * create
     *
     * @Route("/edit/create", name="psc_shop_productgroup_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param MediaManager $mediaManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(Request $request, Field $fieldService, EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager, SessionInterface $session, MediaManager $mediaManager)
    {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Productgroup);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Productgroup);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $productgroup = new Productgroup();
        $productgroup->setShop($selectedShop);
        $form = $this->createForm(ProductgroupType::class, $productgroup);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($productgroup);
            $entityManager->flush();
            $productGroupDoc = new \PSC\Shop\EntityBundle\Document\Productgroup();
            $productGroupDoc->setUid($productgroup->getUid());
            $productGroupDoc->setTextArt($productgroup->getTextArt());
            $productGroupDoc->setTextFormat($productgroup->getTextFormat());
            $productGroupDoc->setDescription($productgroup->getDescription());
            $productGroupDoc->setEinleitung($productgroup->getEinleitung());
            $productGroupDoc->setExtraSettings($productgroup->getExtraSettings());
            $productGroupDoc->setUid($productgroup->getUid());
            $documentManager->persist($productGroupDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Productgroup \'' . $productgroup->getTitle() . '\' has been created!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $productgroup->getTitle(), "Productgroup created");
            return $this->redirectToRoute('psc_shop_productgroup_backend_list');
        }

        return array(
            'productgroup' => $productgroup,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'mediamanager' => $mediaManager
        );
    }


    /**
     * edit
     *
     * @Route("/edit/edit/{uid}", name="psc_shop_productgroup_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param MediaManager $mediaManager
     * @param SessionInterface $session
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function editAction(
        Request $request,
        Field $fieldService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        MediaManager $mediaManager,
        SessionInterface $session,
        $uid
    ) {

        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Productgroup);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Productgroup);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Productgroup $productgroup */
        $productgroup = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findOneBy(['uid' => $uid]);
/** @var \PSC\Shop\EntityBundle\Document\Productgroup $productGroupDoc */
        $productGroupDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Productgroup')
            ->findOneBy(array('uid' => (string)$productgroup->getUid()));
        if ($productGroupDoc) {
            $productgroup->setTextArt($productGroupDoc->getTextArt());
            $productgroup->setEinleitung($productGroupDoc->getEinleitung());
            $productgroup->setDescription($productGroupDoc->getDescription());
            $productgroup->setTextFormat($productGroupDoc->getTextFormat());
            $productgroup->setExtraSettings($productGroupDoc->getExtraSettings());
        } else {
            $productGroupDoc = new \PSC\Shop\EntityBundle\Document\Productgroup();
        }

        $form = $this->createForm(ProductgroupEditType::class, $productgroup);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $productGroupDoc->setTextArt($productgroup->getTextArt());
            $productGroupDoc->setTextFormat($productgroup->getTextFormat());
            $productGroupDoc->setDescription($productgroup->getDescription());
            $productGroupDoc->setEinleitung($productgroup->getEinleitung());
            $productGroupDoc->setExtraSettings($productgroup->getExtraSettings());
            $productGroupDoc->setUid($productgroup->getUid());
            $this->historyService->createHistoryEntry(new PSCHistory((string)$productgroup->getUid()), $productgroup, $productGroupDoc);
            $entityManager->persist($productgroup);
            $entityManager->flush();
            $documentManager->persist($productGroupDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Productgroup \'' . $productgroup->getTitle() . '\' has been upgraded!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $productgroup->getTitle(), "Productgroup updated");
            return $this->redirectToRoute('psc_shop_productgroup_backend_list');
        }

        return array(
            'productgroup' => $productgroup,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'mediamanager' => $mediaManager,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$productgroup->getUid())
        );
    }

    /**
     * remove
     *
     * @Route("/edit/delete/{uid}", name="psc_shop_productgroup_backend_delete")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, SessionInterface $session, EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, $uid)
    {
        /** @var Productgroup $productgroup */
        $productgroup = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findOneBy(['uid' => $uid]);
        $subGroups = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findByParent($uid);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        if (count($subGroups) > 0) {
            $session->getFlashBag()->add('danger', 'Productgroup \'' . $productgroup->getTitle() . '\' has subgroups and can not be deleted!');
            return $this->redirectToRoute('psc_shop_productgroup_backend_list');
        }

        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $title = $productgroup->getTitle();
                $entityManager->remove($productgroup);
                $entityManager->flush();
                $session->getFlashBag()->add('success', 'Productgroup \'' . $title . '\' has been deleted!');
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $title, "Productgroup deleted");
                return $this->redirectToRoute('psc_shop_productgroup_backend_list');
            }
            return $this->redirectToRoute('psc_shop_productgroup_backend_list');
        }


        return array(
            'productgroup' => $productgroup,
            'form' => $form->createView()
        );
    }
}
