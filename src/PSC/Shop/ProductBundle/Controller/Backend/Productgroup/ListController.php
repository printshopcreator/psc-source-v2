<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Backend\Productgroup;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\ProductBundle\Form\Backend\Productgroup\SearchType;
use PSC\Shop\ProductBundle\Form\Backend\ProductSearchType;
use PSC\System\SettingsBundle\Service\DeprecatedFile;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * DashboardController fürs Backend
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/list/index", name="psc_shop_productgroup_backend_list")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @param DeprecatedFile $deprecatedFile
     * @return array|View
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, SessionInterface $session, Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator, FilterBuilderUpdaterInterface $queryBuilder)
    {
        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup');
        $qb = $userRepository->createQueryBuilder('productgroup')
            ->orderBy('productgroup.uid', 'ASC');
        $form = $this->createForm(SearchType::class);
        if ($request->isMethod('POST')) {
            $session->set('productgroup_sort_page', 1);
            $form->handleRequest($request);
        }

        $queryBuilder->addFilterConditions($form, $qb);
        if ($request->get('direction', false)) {
            $session->set('productgroup_sort_direction', $request->get('direction', 'ASC'));
            $session->set('productgroup_sort_field', $request->get('sort', 'productgroup.uid'));
        }
        if ($request->get('page', false)) {
            $session->set('productgroup_sort_page', $request->get('page', 1));
        }

        $qb->andWhere('productgroup.shop = :shop_id')
            ->setParameter("shop_id", $selectedShop->getUid());
        $pagination = $paginator->paginate($query = $qb->getQuery(), $request->query->getInt('page', $session->get('productgroup_sort_page', 1)), 25, [
                'defaultSortFieldName' => $session->get('productgroup_sort_field', 'productgroup.uid'),
                'defaultSortDirection' => $session->get('productgroup_sort_direction', 'ASC')
            ]);
        return array(
            'pagination' => $pagination,
            'form' => $form->createView()
        );
    }
}
