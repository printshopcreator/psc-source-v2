<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Backend\Product;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use PSC\Shop\ProductBundle\Form\Backend\StockbookingSettingsType;
use PSC\Shop\ProductBundle\Form\Backend\StockbookingType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * StockController fürs Backend
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 */
class StockController extends AbstractController
{
    /**
     * add
     *
     * @Route("/stock/index/{product}", name="backend_production_product_stock_index")
     * @template()
     *
     * @param Request $request Request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @param string $product Uuid
     *
     * @return array|View
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator, $product)
    {

        $selectedShop = $shopService->getSelectedShop();
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $product, 'shop' => $selectedShop));
        $form = $this->createForm(StockbookingSettingsType::class, $product);
        $formAdd = $this->createForm(StockbookingType::class, new Stockbooking($product->getUid()));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($product);
            $entityManager->flush();
        }

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Stockbooking');
        $qb = $userRepository->createQueryBuilder('stock')
            ->andWhere('stock.product = :product_id')
            ->leftJoin('stock.contact', 'c')
            ->setParameter("product_id", $product->getUid())
            ->orderBy('stock.created', 'DESC');
        $pagination = $paginator->paginate($qb, $request->query->getInt('page', 1), 20, array('distinct' => false));
        return array(
            'form' => $form->createView(),
            'formAdd' => $formAdd->createView(),
            'product' => $product,
            'pagination' => $pagination
        );
    }

    /**
     * add
     *
     * @Route("/stock/add/{product}", name="backend_production_product_stock_add")
     * @template()
     *
     * @param Request $request Request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param string $product Uuid
     *
     * @return View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function addAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, $product)
    {

        $selectedShop = $shopService->getSelectedShop();
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $product, 'shop' => $selectedShop));
        $booking = new Stockbooking($product->getUid());
        $form = $this->createForm(StockbookingType::class, $booking);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $booking->setProduct($product);
            $booking->setContact($this->getUser());
            $product->setStockCount($product->getStockCount() + $booking->getAmount());
            $entityManager->persist($product);
            $entityManager->persist($booking);
            $entityManager->flush();
        }

        return $this->redirectToRoute("backend_production_product_stock_index", array("product" => $product->getUuid()));
    }
}
