<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Backend\Product;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Form\Transformer\EntityToIdObjectTransformer;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use PSC\Shop\ProductBundle\Form\Backend\Product\TemplateprintUploadType;
use PSC\Shop\ProductBundle\Form\Backend\ProductType;
use PSC\Shop\ProductBundle\Form\Backend\Xml\EditType;
use PSC\Shop\ProductBundle\PSCShopProductBundle;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\Log;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * XmlController für Produkte
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 */
class TemplateprintController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    public function __construct(Log $logService)
    {
        $this->logService = $logService;
    }
    /**
     * delete
     *
     * @Route("/templateprint/delete/{uuid}", name="backend_production_product_templateprint_delete")
     * @template()
     *
     * @param Request $request Request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param string $uuid Uuid
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $dirTemplateprint, $uuid)
    {
        $selectedShop = $shopService->getSelectedShop();
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));
        $form = $this->createForm(DeleteType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                if (file_exists($dirTemplateprint . 'orginal/' . $product->getTemplatePrintDirectory() . '/product.zip')) {
                    unlink($dirTemplateprint . 'orginal/' . $product->getTemplatePrintDirectory() . '/product.zip');
                }
                if (file_exists($dirTemplateprint . 'orginal/' . $product->getTemplatePrintDirectory() . '/final.pdf')) {
                    unlink($dirTemplateprint . 'orginal/' . $product->getTemplatePrintDirectory() . '/final.pdf');
                }
                if (file_exists($dirTemplateprint . 'orginal/' . $product->getTemplatePrintDirectory() . '/preview.pdf')) {
                    unlink($dirTemplateprint . 'orginal/' . $product->getTemplatePrintDirectory() . '/preview.pdf');
                }
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $product->getTitle(), "Templateprint File deleted");
                return $this->redirectToRoute('backend_production_product_edit', ['uuid' => $uuid]);
            }
            return $this->redirectToRoute('backend_production_product_edit', ['uuid' => $uuid]);
        }

        return array(
            'form' => $form->createView(),
            'product' => $product
        );
    }

    /**
     * upload
     *
     * @Route("/templateprint/upload/{uuid}", name="backend_production_product_templateprint_upload")
     * @template()
     *
     * @param Request $request Request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param string $uuid Uuid
     *
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function uploadAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uuid)
    {
        $selectedShop = $shopService->getSelectedShop();
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));
        $form = $this->createForm(TemplateprintUploadType::class, null);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $path = str_split($product->getUID());
        /** @var UploadedFile $file */
            $file = $form->getData()['file'];
            if (!file_exists('/data/www/old/market/templateprint/orginal/' . implode('/', $path))) {
                mkdir('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 0777, true);
            }

            $product->setTemplatePrintDirectory(implode('/', $path) . '/');
            $entityManager->persist($product);
            $entityManager->flush();
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $product->getTitle(), "Templateprint File uploaded");
            $file->move('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 'product.zip');
            return $this->redirectToRoute('backend_production_product_edit', ['uuid' => $uuid]);
        }

        return array(
            'form' => $form->createView(),
            'product' => $product
        );
    }

    /**
     * download
     *
     * @Route("/templateprint/download/{uuid}", name="backend_production_product_templateprint_download")
     *
     * @param Request $request Request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param string $uuid Uuid
     *
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     */
    public function downloadAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uuid)
    {
        $selectedShop = $shopService->getSelectedShop();
/** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));
        $path = str_split($product->getUID());
        $zipName = '/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/product.zip';
        $response = new Response(file_get_contents($zipName));
        $response->headers->set('Content-Type', 'application/zip');
        $response->headers->set('Content-Disposition', 'attachment;filename="product.zip"');
        $response->headers->set('Content-length', filesize($zipName));
        return $response;
    }
}
