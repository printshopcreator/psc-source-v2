<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Backend\Product;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use PSC\Shop\ProductBundle\Form\Backend\Product\PermissionType;
use PSC\Shop\ProductBundle\Form\Backend\StockbookingSettingsType;
use PSC\Shop\ProductBundle\Form\Backend\StockbookingType;
use PSC\Shop\ProductBundle\PSCShopProductBundle;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\Log;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * StockController fürs Backend
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 */
class PermissionController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    public function __construct(Log $logService)
    {
        $this->logService = $logService;
    }
    /**
     * add
     *
     * @Route("/permission/index/{product}", name="backend_production_product_permission_index")
     * @template()
     *
     * @param Request $request Request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @param string $product Uuid
     *
     * @return array|View
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator, $product)
    {

        $selectedShop = $shopService->getSelectedShop();
/** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $product, 'shop' => $selectedShop));
        $form = $this->createForm(PermissionType::class, $product);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($product);
            $entityManager->flush();
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $product->getTitle(), "Product Permission set");
        }

        return array(
            'form' => $form->createView(),
            'product' => $product,
        );
    }
}
