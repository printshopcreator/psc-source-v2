<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Backend\Product;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\ORM\Mapping\Entity;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use PSC\Shop\NewsBundle\Form\Backend\DeleteType;
use PSC\Shop\ProductBundle\Document\Product\History as PSCHistory;
use PSC\Shop\ProductBundle\Form\Backend\Product\ProductType;
use PSC\Shop\ProductBundle\PSCShopProductBundle;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Form\Backend\CopyType;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use PSC\System\SettingsBundle\Service\Status;

/**
 * DashboardController fürs Backend
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;

    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }
    /**
     * edit
     *
     * @Route("/edit/save/lang/data/{uid}", name="backend_production_product_edit_save_lang_data")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return JsonResponse
     */
    public function saveLangDataAction(Request $request, EntityManagerInterface $entityManager, $uid)
    {
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(['uid' => $uid]);


        if ($product) {
            $tmp = $product->getLangData();


            $tmp[$request->get('langcode')] = [
                'title' => $request->get('title'),
                'einleitung' => $request->get('einleitung'),
                'text_art' => $request->get('text_art'),
                'info' => $request->get('info'),
                'uuid' => Uuid::uuid4()
            ];

            $product->setLangData($tmp);
            $entityManager->persist($product);
            $entityManager->flush();
        }

        return new JsonResponse(['success' => true, 'langcode' => $request->get('langcode')]);
    }

    /**
     * edit
     *
     * @Route("/edit/add/lang/data/{uid}", name="backend_production_product_edit_add_lang_data")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return JsonResponse
     */
    public function addLangDataAction(Request $request, EntityManagerInterface $entityManager, $uid)
    {
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(['uid' => $uid]);


        if ($product) {
            $tmp = $product->getLangData();

            if (!isset($tmp[$request->get('langcode')])) {
                $tmp[$request->get('langcode')] = ['title' => $product->getTitle(), 'einleitung' => '', 'text_art' => '', 'info' => '', 'uuid' => Uuid::uuid4()];

                $product->setLangData($tmp);
                $entityManager->persist($product);
                $entityManager->flush();
            }
        }

        return new JsonResponse(['success' => true, 'langcode' => $request->get('langcode')]);
    }

    /**
     * edit
     *
     * @Route("/edit/fetch/lang/data/{uid}", name="backend_production_product_edit_fetch_lang_data")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return JsonResponse
     */
    public function fetchLangDataAction(Request $request, EntityManagerInterface $entityManager, $uid)
    {
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(['uid' => $uid]);

        if ($product) {
            $tmp = array();

            foreach ($product->getLangData() as $key => $row) {
                $tmp[] = [
                    'code' => $key,
                    'title' => $row['title'],
                    'einleitung' => $row['einleitung'],
                    'text_art' => $row['text_art'],
                    'info' => $row['info'],
                    'uuid' => $row['uuid']
                ];
            }

            return new JsonResponse(['success' => true, 'langData' => $tmp]);
        }

        return new JsonResponse(['success' => true, 'langData' => []]);
    }

    /**
     * edit
     *
     * @Route("/edit/check/url/{url}", name="backend_production_product_check_url")
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param $url
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function checkUrlAction(Request $request, EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, $url)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        $products = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findBy(array('enable' => 1, 'url' => $url, 'shop' => $selectedShop));

        if (count($products) > 0) {
            $tmp = array();
            /** @var Product $product */
            foreach ($products as $product) {
                $tmp[] = $product->getTitle();
            }
            return new JsonResponse(['success' => true, 'found' => true, 'error' => implode(", ", $tmp)]);
        }

        return new JsonResponse(['success' => true, 'found' => false]);
    }

    /**
     * edit
     *
     * @Route("/edit/create/{type}", name="backend_production_product_create")
     * @template()
     *
     * @param Request $request Request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param $type
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(
        Request $request,
        Field $fieldService,
        EntityManagerInterface $entityManager,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        DocumentManager $documentManager,
        SessionInterface $session,
        $type
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Product);

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /** @var Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string) $selectedShop->getUid()));

        $articlegroups = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findBy(array('parent' => 0, 'shop' => $selectedShop));

        $customProductFields = $fieldService->getProductFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $type);
        $customProductGroups = $fieldService->getProductGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $type);

        /** @var Domain[] $domains */
        $domains = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->getAllByShopId($selectedShop);

        $product = new Product();
        $product->setShop($selectedShop);
        $product->setInstall($selectedShop->getInstall()->getUid());
        $product->setType($type);

        $form = $this->createForm(ProductType::class, $product, ['productType' => $type]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($product);
            $entityManager->flush();

            $productDoc = new \PSC\Shop\EntityBundle\Document\Product();
            $productDoc->setAblaufDatum($product->getAblaufDatum());
            $productDoc->setAnsprechPartner($product->getAnsprechPartner());
            $productDoc->setPrintPartnerEmail($product->getPrintPartnerEmail());
            $productDoc->setZusatzAbmessung($product->getZusatzAbmessung());
            $productDoc->setZusatzDesigner($product->getZusatzDesigner());
            $productDoc->setZusatzShipping($product->getZusatzShipping());
            $productDoc->setPluginSettings($product->getPluginSettings());
            $productDoc->setCustom1($product->getCustom1());
            $productDoc->setCustom2($product->getCustom2());
            $productDoc->setCustom3($product->getCustom3());
            $productDoc->setCustom4($product->getCustom4());
            $productDoc->setCustom5($product->getCustom5());
            $productDoc->setCustom6($product->getCustom6());
            $productDoc->setCustom7($product->getCustom7());
            $productDoc->setCustom8($product->getCustom8());
            $productDoc->setCustom9($product->getCustom9());
            $productDoc->setCustom10($product->getCustom10());
            $productDoc->setCustom11($product->getCustom11());
            $productDoc->setCustom12($product->getCustom12());
            $productDoc->setCollectingOrdersChangePicture($product->isCollectingOrdersChangePicture());
            $productDoc->setCollectingOrdersCopy($product->isCollectingOrdersCopy());
            $productDoc->setCollectingOrdersInviteContact($product->isCollectingOrdersInviteContact());
            $productDoc->setCollectingOrdersNewContact($product->isCollectingOrdersNewContact());
            $productDoc->setCollectingOrdersAccountFilter($product->getCollectingOrdersAccountFilter());
            $productDoc->setCollectingOrdersAccount($product->getCollectingOrdersAccount());
            $productDoc->setCollectingOrdersIntegrated($product->isCollectingOrdersIntegrated());
            $productDoc->setText($product->getText());
            $productDoc->setUid($product->getUid());
            $productDoc->setExtraSettings($product->getExtraSettings());
            $productDoc->setWeight($product->getWeight());
            $productDoc->setAvailability($product->getAvailability());
            $productDoc->setPackagingUnit($product->getPackagingUnit());
            $productDoc->setSalesUnit($product->getSalesUnit());
            $productDoc->setBaseUnit($product->getBaseUnit());
            $productDoc->setUploadFromLatestOrder($product->isUploadFromLatestOrder());
            $productDoc->setUploadFromLatestOrderInitalStatus((int)$product->getUploadFromLatestOrderInitalStatus());
            $productDoc->setUploadProvided($product->isUploadProvided());
            $productDoc->setUploadProvidedFile($product->getUploadProvidedFile());
            $productDoc->setUploadProvidedInitalStatus((int)$product->getUploadProvidedInitalStatus());
            $productDoc->setCustomTabEnable($product->isCustomTabEnable());
            $productDoc->setHintEnable($product->isHintEnable());
            $productDoc->setAsRequest($product->isAsRequest());

            $documentManager->persist($productDoc);
            $documentManager->flush();

            $session->getFlashBag()->add(
                'success',
                'Product \'' . $product->getTitle() . '\' has been created!'
            );
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $product->getTitle(), "Product has been created");
            return $this->redirectToRoute('backend_production_product_edit', ['uuid' => $product->getUUID()]);
        }

        return array(
            'domain' => $domains[0]->getHost(),
            'form' => $form->createView(),
            'product' => $product,
            'articlegroups' => $articlegroups,
            'shopDoc' => $shop,
            'customFields' => $customFields,
            'customProductFields' => $customProductFields,
            'customProductGroups' => $customProductGroups,
            'customGroups' => $customGroups,
            'selectedShop' => $selectedShop
        );
    }

    /**
     * edit
     *
     * @Route("/edit/index/{uuid}", name="backend_production_product_edit")
     * @template()
     *
     * @param Request $request Request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param PaginatorInterface $paginator
     * @param SessionInterface $session
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param Status $statusService
     * @param \PSC\Shop\OrderBundle\Service\Order $orderService
     * @param string $uuid Uuid
     *
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(
        Request $request,
        Field $fieldService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        PaginatorInterface $paginator,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        Status $statusService,
        \PSC\Shop\OrderBundle\Service\Order $orderService,
        $uuid
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Product);

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /** @var Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string) $selectedShop->getUid()));

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string) $product->getUid()));


        $articlegroups = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')->findBy(array('parent' => 0, 'shop' => $selectedShop));


        $customProductFields = $fieldService->getProductFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $product->getType());
        $customProductGroups = $fieldService->getProductGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $product->getType());
        $product->setUpdatedAt(new \DateTime());

        if (!$request->isMethod('POST') && $productDoc) {
            $product->setAnsprechPartner($productDoc->getAnsprechPartner());
            $product->setAblaufDatum($productDoc->getAblaufDatum());
            $product->setPrintPartnerEmail($productDoc->getPrintPartnerEmail());
            $product->setZusatzDesigner($productDoc->getZusatzDesigner());
            $product->setZusatzAbmessung($productDoc->getZusatzAbmessung());
            $product->setZusatzShipping($productDoc->getZusatzShipping());
            $product->setPluginSettings($productDoc->getPluginSettings());
            $product->setCustom1($productDoc->getCustom1());
            $product->setCustom2($productDoc->getCustom2());
            $product->setCustom3($productDoc->getCustom3());
            $product->setCustom4($productDoc->getCustom4());
            $product->setCustom5($productDoc->getCustom5());
            $product->setCustom6($productDoc->getCustom6());
            $product->setCustom7($productDoc->getCustom7());
            $product->setCustom8($productDoc->getCustom8());
            $product->setCustom9($productDoc->getCustom9());
            $product->setCustom10($productDoc->getCustom10());
            $product->setCustom11($productDoc->getCustom11());
            $product->setCustom12($productDoc->getCustom12());
            $product->setCollectingOrdersChangePicture($productDoc->isCollectingOrdersChangePicture());
            $product->setCollectingOrdersCopy($productDoc->isCollectingOrdersCopy());
            $product->setCollectingOrdersInviteContact($productDoc->isCollectingOrdersInviteContact());
            $product->setCollectingOrdersNewContact($productDoc->isCollectingOrdersNewContact());
            $product->setCollectingOrdersAccountFilter($productDoc->getCollectingOrdersAccountFilter());
            $product->setCollectingOrdersAccount($productDoc->getCollectingOrdersAccount());
            $product->setCollectingOrdersIntegrated($productDoc->isCollectingOrdersIntegrated());
            $product->setText($productDoc->getText());
            $product->setExtraSettings($productDoc->getExtraSettings());
            $product->setWeight($productDoc->getWeight());
            $product->setAvailability($productDoc->getAvailability());
            $product->setPackagingUnit($productDoc->getPackagingUnit());
            $product->setSalesUnit($productDoc->getSalesUnit());
            $product->setBaseUnit($productDoc->getBaseUnit());
            $product->setUploadFromLatestOrder($productDoc->isUploadFromLatestOrder());
            $product->setUploadFromLatestOrderInitalStatus((int)$productDoc->getUploadFromLatestOrderInitalStatus());
            $product->setUploadProvidedInitalStatus((int)$productDoc->getUploadProvidedInitalStatus());
            $product->setUploadProvided($productDoc->isUploadProvided());
            $product->setUploadProvidedFile($productDoc->getUploadProvidedFile());
            $product->setCustomTabEnable($productDoc->isCustomTabEnable());
            $product->setHintEnable($productDoc->isHintEnable());
            $product->setAsRequest($productDoc->isAsRequest());
        } elseif (!$productDoc) {
            $productDoc = new \PSC\Shop\EntityBundle\Document\Product();
            $productDoc->setAblaufDatum($product->getAblaufDatum());
            $productDoc->setAnsprechPartner($product->getAnsprechPartner());
            $productDoc->setPrintPartnerEmail($product->getPrintPartnerEmail());
            $productDoc->setZusatzAbmessung($product->getZusatzAbmessung());
            $productDoc->setZusatzDesigner($product->getZusatzDesigner());
            $productDoc->setZusatzShipping($product->getZusatzShipping());
            $productDoc->setPluginSettings($product->getPluginSettings());
            $productDoc->setCustom1($product->getCustom1());
            $productDoc->setCustom2($product->getCustom2());
            $productDoc->setCustom3($product->getCustom3());
            $productDoc->setCustom4($product->getCustom4());
            $productDoc->setCustom5($product->getCustom5());
            $productDoc->setCustom6($product->getCustom6());
            $productDoc->setCustom7($product->getCustom7());
            $productDoc->setCustom8($product->getCustom8());
            $productDoc->setCustom9($product->getCustom9());
            $productDoc->setCustom10($product->getCustom10());
            $productDoc->setCustom11($product->getCustom11());
            $productDoc->setCustom12($product->getCustom12());
            $productDoc->setCollectingOrdersChangePicture($product->isCollectingOrdersChangePicture());
            $productDoc->setCollectingOrdersCopy($product->isCollectingOrdersCopy());
            $productDoc->setCollectingOrdersInviteContact($product->isCollectingOrdersInviteContact());
            $productDoc->setCollectingOrdersNewContact($product->isCollectingOrdersNewContact());
            $productDoc->setCollectingOrdersAccountFilter($product->getCollectingOrdersAccountFilter());
            $productDoc->setCollectingOrdersAccount($product->getCollectingOrdersAccount());
            $productDoc->setCollectingOrdersIntegrated($product->isCollectingOrdersIntegrated());
            $productDoc->setText($product->getText());
            $productDoc->setUid($product->getUid());
            $productDoc->setExtraSettings($product->getExtraSettings());
            $productDoc->setWeight($product->getWeight());
            $productDoc->setAvailability($product->getAvailability());
            $productDoc->setPackagingUnit($product->getPackagingUnit());
            $productDoc->setSalesUnit($product->getSalesUnit());
            $productDoc->setBaseUnit($product->getBaseUnit());
            $productDoc->setUploadFromLatestOrder($product->isUploadFromLatestOrder());
            $productDoc->setUploadFromLatestOrderInitalStatus((int)$product->getUploadFromLatestOrderInitalStatus());
            $productDoc->setUploadProvided($product->isUploadProvided());
            $productDoc->setUploadProvidedFile($product->getUploadProvidedFile());
            $productDoc->setUploadProvidedInitalStatus((int)$product->getUploadProvidedInitalStatus());
            $productDoc->setCustomTabEnable($product->isCustomTabEnable());
            $productDoc->setHintEnable($product->isHintEnable());
            $productDoc->setAsRequest($product->isAsRequest());

            $documentManager->persist($productDoc);
            $documentManager->flush();

            $product->setAblaufDatum($productDoc->getAblaufDatum());
            $product->setAnsprechPartner($productDoc->getAnsprechPartner());
            $product->setPrintPartnerEmail($productDoc->getPrintPartnerEmail());
            $product->setZusatzShipping($productDoc->getZusatzShipping());
            $product->setZusatzDesigner($productDoc->getZusatzDesigner());
            $product->setZusatzAbmessung($productDoc->getZusatzAbmessung());
            $product->setPluginSettings($productDoc->getPluginSettings());
            $product->setCustom1($productDoc->getCustom1());
            $product->setCustom2($productDoc->getCustom2());
            $product->setCustom3($productDoc->getCustom3());
            $product->setCustom4($productDoc->getCustom4());
            $product->setCustom5($productDoc->getCustom5());
            $product->setCustom6($productDoc->getCustom6());
            $product->setCustom7($productDoc->getCustom7());
            $product->setCustom8($productDoc->getCustom8());
            $product->setCustom9($productDoc->getCustom9());
            $product->setCustom10($productDoc->getCustom10());
            $product->setCustom11($productDoc->getCustom11());
            $product->setCustom12($productDoc->getCustom12());
            $product->setCollectingOrdersChangePicture($productDoc->isCollectingOrdersChangePicture());
            $product->setCollectingOrdersCopy($productDoc->isCollectingOrdersCopy());
            $product->setCollectingOrdersInviteContact($productDoc->isCollectingOrdersInviteContact());
            $product->setCollectingOrdersNewContact($productDoc->isCollectingOrdersNewContact());
            $product->setCollectingOrdersAccount($productDoc->getCollectingOrdersAccount());
            $product->setCollectingOrdersAccountFilter($productDoc->getCollectingOrdersAccountFilter());
            $product->setCollectingOrdersIntegrated($productDoc->isCollectingOrdersIntegrated());
            $product->setText($productDoc->getText());
            $product->setExtraSettings($productDoc->getExtraSettings());
            $product->setWeight($productDoc->getWeight());
            $product->setAvailability($productDoc->getAvailability());
            $product->setPackagingUnit($productDoc->getPackagingUnit());
            $product->setSalesUnit($productDoc->getSalesUnit());
            $product->setBaseUnit($productDoc->getBaseUnit());
            $product->setUploadFromLatestOrder($productDoc->isUploadFromLatestOrder());
            $product->setUploadFromLatestOrderInitalStatus((int)$productDoc->getUploadFromLatestOrderInitalStatus());
            $product->setUploadProvidedInitalStatus((int)$productDoc->getUploadProvidedInitalStatus());
            $product->setUploadProvided($productDoc->isUploadProvided());
            $product->setUploadProvidedFile($productDoc->getUploadProvidedFile());
            $product->setCustomTabEnable($productDoc->isCustomTabEnable());
            $product->setHintEnable($productDoc->isHintEnable());
            $product->setAsRequest($productDoc->isAsRequest());
        }


        $form = $this->createForm(ProductType::class, $product, ['productType' => intval($product->getType()), 'productDoc' => $productDoc, 'product' => $product]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productDoc->setAnsprechPartner($product->getAnsprechPartner());
            $productDoc->setAblaufDatum($product->getAblaufDatum());
            $productDoc->setZusatzAbmessung($product->getZusatzAbmessung());
            $productDoc->setZusatzDesigner($product->getZusatzDesigner());
            $productDoc->setZusatzShipping($product->getZusatzShipping());
            $productDoc->setPrintPartnerEmail($product->getPrintPartnerEmail());
            $productDoc->setPluginSettings($product->getPluginSettings());
            $productDoc->setCustom1($product->getCustom1());
            $productDoc->setCustom2($product->getCustom2());
            $productDoc->setCustom3($product->getCustom3());
            $productDoc->setCustom4($product->getCustom4());
            $productDoc->setCustom5($product->getCustom5());
            $productDoc->setCustom6($product->getCustom6());
            $productDoc->setCustom7($product->getCustom7());
            $productDoc->setCustom8($product->getCustom8());
            $productDoc->setCustom9($product->getCustom9());
            $productDoc->setCustom10($product->getCustom10());
            $productDoc->setCustom11($product->getCustom11());
            $productDoc->setCustom12($product->getCustom12());
            $productDoc->setCollectingOrdersChangePicture($product->isCollectingOrdersChangePicture());
            $productDoc->setCollectingOrdersCopy($product->isCollectingOrdersCopy());
            $productDoc->setCollectingOrdersInviteContact($product->isCollectingOrdersInviteContact());
            $productDoc->setCollectingOrdersNewContact($product->isCollectingOrdersNewContact());
            $productDoc->setCollectingOrdersAccountFilter($product->getCollectingOrdersAccountFilter());
            $productDoc->setCollectingOrdersAccount($product->getCollectingOrdersAccount());
            $productDoc->setCollectingOrdersIntegrated($product->isCollectingOrdersIntegrated());
            $productDoc->setText($product->getText());
            $productDoc->setExtraSettings($product->getExtraSettings());
            $productDoc->setWeight($product->getWeight());
            $productDoc->setAvailability($product->getAvailability());
            $productDoc->setPackagingUnit($product->getPackagingUnit());
            $productDoc->setBaseUnit($product->getBaseUnit());
            $productDoc->setSalesUnit($product->getSalesUnit());
            $productDoc->setUploadFromLatestOrder($product->isUploadFromLatestOrder());
            $productDoc->setUploadFromLatestOrderInitalStatus((int)$product->getUploadFromLatestOrderInitalStatus());
            $productDoc->setUploadProvided($product->isUploadProvided());
            $productDoc->setUploadProvidedFile($product->getUploadProvidedFile());
            $productDoc->setUploadProvidedInitalStatus((int)$product->getUploadProvidedInitalStatus());
            $productDoc->setCustomTabEnable($product->isCustomTabEnable());
            $productDoc->setHintEnable($product->isHintEnable());
            $productDoc->setAsRequest($product->isAsRequest());

            $this->historyService->createHistoryEntry(new PSCHistory((string)$product->getUID()), $product, $productDoc);
            $entityManager->persist($product);
            $entityManager->flush();

            $documentManager->persist($productDoc);
            $documentManager->flush();

            $session->getFlashBag()->add(
                'success',
                'Product \'' . $product->getTitle() . '\' has been updated!'
            );
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $product->getTitle(), "Product has been updated");
        }

        /** @var Domain[] $domains */
        $domains = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->getAllByShopId($selectedShop);

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');

        $qb = $userRepository->createQueryBuilder('orders')
            ->leftJoin('orders.positions', 'pos')
            ->andwhere('pos.product = :product_id')->setParameter("product_id", $product->getUid())
            ->orderBy('orders.uid', 'desc');

        $qbCount = $userRepository->createQueryBuilder('orders')
            ->select('count(orders.uid)')
            ->leftJoin('orders.positions', 'pos')
            ->andwhere('pos.product = :product_id')->setParameter("product_id", $product->getUid())
            ->orderBy('orders.uid', 'desc');


        $query = $qb->getQuery();

        $count = $qbCount->getQuery()->getSingleScalarResult();

        $query->setHint('knp_paginator.count', $count);

        $pagination2 = $paginator->paginate(
            $query,
            $request->query->getInt('pages_order', 1),
            15,
            [
                'distinct' => false,
                'pageParameterName' => 'pages_order',
                'sortFieldParameterName' => 'sorts'
            ]
        );

        return array(
            'domain' => $domains[0]->getHost(),
            'form' => $form->createView(),
            'product' => $product,
            'articlegroups' => $articlegroups,
            'shopDoc' => $shop,
            'customFields' => $customFields,
            'customProductFields' => $customProductFields,
            'customProductGroups' => $customProductGroups,
            'customGroups' => $customGroups,
            'selectedShop' => $selectedShop,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$product->getUid()),
            'pagination2' => $pagination2,
            'orderStatuse' => $statusService,
            'orderService' => $orderService,
        );
    }

    /**
     * @Route("/{uuid}/delete", name="psc_shop_product_backend_edit_delete")
     * @Security("is_granted('ROLE_SHOP')")
     * @Template()
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param $uuid
     * @return array|Response|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAction(
        Request $request,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        SessionInterface $session,
        $uuid
    ) {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        $sql = 'select count(article_id) as sum from orderspos where article_id = :article_id';

        $stmt = $entityManager->getConnection()->prepare($sql);
        $result = $stmt->executeQuery(array('article_id' => $product->getUID()));
        $count = (int)$result->fetchOne();

        $form = $this->createForm(DeleteType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $title = $product->getTitle();
                $entityManager->remove($product);
                $entityManager->flush();
                $session->getFlashBag()->add(
                    'success',
                    'Product \'' . $title . '\' has been deleted!'
                );
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopProductBundle::class, $product->getTitle(), "Product has been deleted");
                return $this->redirectToRoute('psc_shop_product_backend_list');
            }
            return $this->redirectToRoute('psc_shop_product_backend_list');
        }


        return array(
            'product' => $product,
            'count' => $count,
            'form' => $form->createView()
        );
    }

    /**
     * Copy Seite
     *
     * @Route("/{uuid}/copy", name="psc_shop_product_backend_edit_copy")
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param \PSC\Shop\ProductBundle\Service\Product $productService
     * @param EntityManagerInterface $entityManager
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     * @Template()
     */
    public function copyAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, \PSC\Shop\ProductBundle\Service\Product $productService, EntityManagerInterface $entityManager, $uuid)
    {

        $obj = ['which' => 1];

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        $form = $this->createForm(CopyType::class, $obj);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            if ($data['which'] == 1) {
                $productService->copy($product, true);

                return $this->redirectToRoute('psc_shop_product_backend_list');
            } elseif ($data['which'] == 2) {
                $productService->copy($product, true, false, $data['shops']);

                return $this->redirectToRoute('psc_shop_product_backend_list');
            }
        }

        return array('form' => $form->createView(), 'product' => $product);
    }
}
