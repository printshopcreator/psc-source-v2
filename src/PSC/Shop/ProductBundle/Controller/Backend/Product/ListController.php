<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Controller\Backend\Product;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\NewsBundle\Form\Backend\DeleteType;
use PSC\Shop\ProductBundle\Form\Backend\ProductSearchType;
use PSC\System\PluginBundle\Service\ProductType;
use PSC\System\SettingsBundle\Service\Shop;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * DashboardController fürs Backend
 *
 * @package    PSC\Shop\Product
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/list", name="psc_shop_product_backend_list")
     * @Template()
     *
     * @param Request $request
     * @param ProductType $productType
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @param FilterBuilderUpdaterInterface $filterBuilderUpdater
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, ProductType $productType, Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator, FilterBuilderUpdaterInterface $filterBuilderUpdater)
    {
        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Product');
        $qb = $userRepository->createQueryBuilder('product')
            ->andWhere('product.shop = :shop_id')
            ->andWhere("product.uuid != ''")
            ->setParameter("shop_id", $selectedShop->getUid())
            ->orderBy('product.uid', 'ASC');
        $form = $this->createForm(ProductSearchType::class);
        if ($request->isMethod('GET')) {
            $form->handleRequest($request);
            $filterBuilderUpdater->addFilterConditions($form, $qb);
        }

        $pagination = $paginator->paginate($query = $qb->getQuery(), $request->query->getInt('page', 1), 25);
        return array(
            'pagination' => $pagination,
            'form' => $form->createView(),
            'productTypes' => $productType->getProductTypes()
        );
    }

    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/toggle/{field}/{uuid}", name="psc_shop_product_backend_toogle")
     *
     * @param Request $request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $field
     * @param $uuid
     * @return array|JsonResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function toggleAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, $field, $uuid)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Product $product */
        $product = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));
        if ($field == 'enable') {
            $product->setEnable(!$product->isEnable());
            $entityManager->persist($product);
            $entityManager->flush();
        }

        return new JsonResponse([]);
    }
}
