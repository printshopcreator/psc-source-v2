<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Form\Backend\Productgroup;

use Doctrine\ORM\EntityRepository;
use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchType extends AbstractType
{
    /** @var Shop */
    protected $shop;
/** @var Session */
    protected $session;
    public function __construct(SessionInterface $session, Shop $shop)
    {
        $this->shop = $shop;
        $this->session = $session;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $session = $this->session;
        $builder
            ->setMethod('POST')
            ->add('parent', EntityType::class, array(
                'class' => 'PSCEntityBundle:Productgroup',
                'choice_value' => 'uid',
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('g')
                        ->andWhere('g.shop = :shop')
                        ->setParameter('shop', $this->shop->getSelectedShop()->getUid())
                        ->orderBy('g.title', 'ASC');
                },
                'choice_label' => 'title',
                'data' => $session->get('productgroup_search_parent'),
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($session) {

                    $session->set('productgroup_search_parent', $values['value']);
                    if (empty($values['value'])) {
                        return null;
                    }
                    $filterQuery->getQueryBuilder()
                        ->andWhere("$field = '" . $values['value']->getUid() . "'");
                },
                'required' => false,
                'label' => 'predecessor'
            ))
            ->add('enable', CheckboxType::class, array(
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($session) {

                    $session->set('productgroup_search_enable', $values['value']);
                    if ($values['value'] == 1) {
                        $filterQuery->getQueryBuilder()
                            ->andWhere("$field = '" . $values['value'] . "'");
                    }
                },
                'required' => false,
                'data' => $session->get('productgroup_search_enable', true),
                'label' => 'Showactiveonly'
            ))
            ->add('term', TextFilterType::class, array(
                'data' => $session->get('productgroup_search_term'),
                'attr' => ['placeholder' => 'keyword'],
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($session) {

                    $session->set('productgroup_search_term', $values['value']);
                    if (empty($values['value'])) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()
                        ->andWhere("
                            productgroup.uid LIKE '%" . $values['value'] . "%' OR
                            productgroup.title LIKE '%" . $values['value'] . "%'
                        ");
                }))
            ->add('filter', SubmitType::class, [
                'icon_before' => 'fas fa-search',
                'label' => 'Filtern',
                'attr' => array('class' => 'btn btn-sm btn-success')
            ]);
    }

    public function getName()
    {
        return 'item_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'translation_domain' => 'core_productgroup_list'
        ));
    }
}
