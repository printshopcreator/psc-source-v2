<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Form\Backend\Productgroup;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\MediaBundle\Form\Type\ImageType;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;

class ProductgroupType extends AbstractType
{
    protected $shop = null;
/** @var Status */
    protected $statusService = null;
/** @var Field  */
    protected $fields;
    protected $formFactory;
    public function __construct(Shop $shop, Status $statusService, Field $fields, FormFactory $formFactory)
    {
        $this->shop = $shop;
        $this->statusService = $statusService;
        $this->fields = $fields;
        $this->formFactory = $formFactory;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Title'))
            ->add('parent', EntityType::class, array(
                'class' => Productgroup::class,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('g')
                        ->andWhere('g.shop = :shop')
                        ->setParameter('shop', $this->shop->getSelectedShop()->getUid())
                        ->orderBy('g.title', 'ASC');
                },
                'choice_label' => function (Productgroup $choice, $key, $value) {

                    if ($choice->isEnable()) {
                        return $choice->getTitle();
                    } else {
                        return $choice->getTitle() . ' (disabled)';
                    }
                },
                'choice_value' => 'uid',
                'required' => false,
                'placeholder' => 'Highestlevel',
                'label' => 'predecessor'
            ))
            ->add('image1', MediaType::class, array())
            ->add('image2', MediaType::class, array())
            ->add('meta_keywords', TextareaType::class, array('label' => 'Keywords', 'required' => false))
            ->add('meta_description', TextareaType::class, array('label' => 'description', 'required' => false))
            ->add('meta_author', TextType::class, array('label' => 'Author', 'required' => false))
            ->add('meta_custom_title', TextType::class, array('label' => 'Othertitle', 'required' => false))
            ->add('meta_og_title', TextType::class, array('label' => 'OGTitle', 'required' => false))
            ->add('meta_og_type', TextType::class, array('label' => 'OGTyp', 'required' => false))
            ->add('meta_og_url', TextType::class, array('label' => 'OGUrl', 'required' => false))
            ->add('meta_og_image', TextType::class, array('label' => 'OGImage', 'required' => false))
            ->add('meta_og_description', TextareaType::class, array('label' => 'OGDescription', 'required' => false))
            ->add('url', TextType::class, array('label' => 'Url', 'required' => true))
            ->add('pos', IntegerType::class, array('label' => 'Pos', 'required' => true))
            ->add('enable', CheckboxType::class, array('label' => 'Active', 'required' => false))
            ->add('private', CheckboxType::class, array('required' => false, 'label' => 'Private'))
            ->add('extraSettings', TextareaType::class, array('required' => false, 'label' => 'Extrasettings'))
            ->add('language', ChoiceType::class, [
                'label' => 'language',
                'choices' => [
                    'Alle' => 'all',
                    'Deutsch' => 'de_DE',
                    'Englisch' => 'en_EN'
                ],
                'required' => true])
            ->add('notInMenu', CheckboxType::class, array('label' => 'Hideinnavigation', 'required' => false))
            ->add('displayTitle', CheckboxType::class, array('label' => 'Showtitle', 'required' => false))
            ->add('textArt', TextType::class, ['required' => false, 'label' => 'Art'])
            ->add('textFormat', TextType::class, ['required' => false, 'label' => 'Format'])
            ->add('einleitung', TextareaType::class, ['required' => false, 'label' => 'initiation'])
            ->add('text', TextareaType::class, array('label' => 'Additionaldescription', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'description', 'required' => false))
            ;
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Productgroup) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup()), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'productgroup';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Productgroup',
            'translation_domain' => 'core_productgroup_create_and_edit'
        ));
    }
}
