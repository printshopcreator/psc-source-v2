<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Form\Backend;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductSearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('mainProduct', CheckboxType::class, array(
                'label' => 'showsubproducts',
                'required' => false,
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {

                    if (empty($values['value'])) {
                        $filterQuery->getQueryBuilder()
                            ->andWhere("
                            product.originalProduct = 0
                        ");
                    }
                }))
            ->add('term', TextFilterType::class, array(
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {

                    if (empty($values['value'])) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()
                        ->andWhere("
                            (product.uid LIKE '%" . $values['value'] . "%' OR
                            product.title LIKE '%" . $values['value'] . "%' OR
                            product.calcXml LIKE '%" . $values['value'] . "%' OR
                            product.textFormat LIKE '%" . $values['value'] . "%' OR
                            product.description LIKE '%" . $values['value'] . "%' OR
                            product.nrIntern LIKE '%" . $values['value'] . "%' OR
                            product.nrExtern LIKE '%" . $values['value'] . "%' OR
                            product.textArt LIKE '%" . $values['value'] . "%')
                        ");
                }))->add('filter', SubmitType::class, [
                'icon_before' => 'fas fa-search',
                'label' => 'Filter',
                'attr' => array('class' => 'btn btn-sm btn-success')
                ]);
    }

    public function getName()
    {
        return 'item_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'translation_domain' => 'core_product_list'
        ));
    }
}
