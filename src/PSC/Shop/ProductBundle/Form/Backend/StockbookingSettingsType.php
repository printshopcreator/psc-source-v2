<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Form\Backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StockbookingSettingsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('isStock', CheckboxType::class, array('disabled' => false, 'required' => false, 'label' => 'Manageinventory'))
            ->add('stockCount', NumberType::class, array('disabled' => false, 'required' => false, 'label' => 'Currentinventory'))
            ->add('stockMinCount', NumberType::class, array('disabled' => false, 'required' => false, 'label' => 'Minimumstock'))
            ->add('stockPlace', TextType::class, array('disabled' => false, 'required' => false, 'label' => 'Storagelocation'))
            ->add('stockMaxBuy', NumberType::class, array('disabled' => false, 'required' => false, 'label' => 'Maximumbuy'))
            ->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'stockbookingsettings';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Product',
            'translation_domain' => 'core_product_stock'
        ));
    }
}
