<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Form\Backend;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StockbookingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('product', HiddenType::class)
            ->add('delivery', TextType::class, array('disabled' => false, 'required' => false, 'label' => 'Deliverynote'))
            ->add('amount', IntegerType::class, array('disabled' => false, 'required' => false, 'label' => 'number'))
            ->add('description', TextareaType::class, array('disabled' => false, 'required' => false, 'label' => 'description'))
            ->add('save', SubmitType::class, array('label' => 'add'));
    }

    public function getName()
    {
        return 'stockbooking';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Stockbooking',
            'translation_domain' => 'core_product_stock'
        ));
    }
}
