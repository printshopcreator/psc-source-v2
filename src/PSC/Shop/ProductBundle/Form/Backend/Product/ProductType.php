<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Form\Backend\Product;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\MediaBundle\Form\Type\ImageType;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\General;
use PSC\System\SettingsBundle\Service\Price;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    protected $shop = null;
    /** @var Status */
    protected $statusService = null;

    /** @var Field  */
    protected $fields;

    protected $formFactory;

    protected $generalService;

    protected $entity_manager;
    /**
     * @var Price
     */
    private Price $priceService;

    public function __construct(Price $priceService, Shop $shop, Status $statusService, Field $fields, FormFactory $formFactory, General $generalService, EntityManagerInterface $entityManager)
    {
        $this->shop = $shop;
        $this->statusService = $statusService;
        $this->fields = $fields;
        $this->formFactory = $formFactory;
        $this->generalService = $generalService;
        $this->entity_manager = $entityManager;
        $this->priceService = $priceService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('uuid', TextType::class, array('disabled' => true))
            ->add('buyed', NumberType::class, array('disabled' => false, 'required' => false, 'label' => 'Sales'))
            ->add('enable', CheckboxType::class, array('required' => false, 'label' => 'Active'))
            ->add('asOffer', CheckboxType::class, array('required' => false, 'label' => 'showcreatepdfbtn'))
            ->add('asRequest', CheckboxType::class, array('required' => false, 'label' => 'showcreaterequestbtn'))
            ->add('private', CheckboxType::class, array('required' => false, 'label' => 'Private'))
            ->add('canBuyWithNoPrice', CheckboxType::class, array('required' => false, 'label' => 'pricenull'))
            ->add('displayNoPrice', CheckboxType::class, array('required' => false, 'label' => 'notpriceshow'))
            ->add('displayNotInOverview', CheckboxType::class, array('required' => false, 'label' => 'notshow'))
            ->add('notBuy', CheckboxType::class, array('required' => false, 'label' => 'notshoworderbtn'))
            ->add('notEdit', CheckboxType::class, array('required' => false, 'label' => 'Sellasproduct'))
            ->add('url', TextType::class, array('required' => true, 'label' => 'Url'))
            ->add('title', TextType::class)
            ->add('createdAt', DatePickerType::class, array('disabled' => true))
            ->add('image1', MediaType::class, array())
            ->add('image2', MediaType::class, array())
            ->add('image3', MediaType::class, array())
            ->add('image4', MediaType::class, array())
            ->add('image5', MediaType::class, array())
            ->add('image6', MediaType::class, array())
            ->add('image7', MediaType::class, array())
            ->add('image8', MediaType::class, array())
            ->add('hintEnable', CheckboxType::class, array('required' => false, 'label' => 'Active'))
            ->add('hintFile1', MediaType::class, array())
            ->add('hintFile2', MediaType::class, array())
            ->add('hintFile3', MediaType::class, array())
            ->add('hintFile4', MediaType::class, array())
            ->add('updatedAt', DatePickerType::class, array('disabled' => true))
            ->add('ablaufDatum', DatePickerType::class, array('required' => false, 'label' => 'ExpiryDate'))
            ->add('ansprechPartner', TextType::class, array('required' => false, 'label' => 'contactperson'))
            ->add('printPartnerEmail', EmailType::class, array('required' => false, 'label' => 'Printingpartneremail'))
            ->add('zusatzAbmessung', TextType::class, array('required' => false, 'label' => 'Additionaldimension'))
            ->add('zusatzDesigner', TextType::class, array('required' => false, 'label' => 'Additiondesigner'))
            ->add('zusatzShipping', TextType::class, array('required' => false, 'label' => 'Additionalshippinginformation'))
            ->add('extraSettings', TextareaType::class, array('required' => false, 'label' => 'Extrasettings'))
            ->add('description', TextareaType::class, array(
                'label' => 'description',
                'required' => false
            ))
            ->add('text', TextareaType::class, array(
                'label' => 'Additionaldescription',
                'required' => false
            ))
            ->add('hintText', TextareaType::class, array(
                'label' => 'description',
                'required' => false
            ))
            ->add('customTabEnable', CheckboxType::class, array('required' => false, 'label' => 'Active'))
            ->add('customTabTitle', TextType::class, array('required' => false, 'label' => 'Tabname'))
            ->add('customTabText', TextareaType::class, array(
                'label' => 'Tabtext',
                'required' => false
            ))
            ->add('product_groups', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Productgroup',
                'choice_label' => function (Productgroup $choice, $key, $value) {
                    if ($choice->isEnable()) {
                        return $choice->getTitle();
                    } else {
                        return $choice->getTitle() . ' (disabled)';
                    }
                },
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('releatedProducts', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Product',
                'choice_label' => 'title',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop AND u.originalProduct = 0')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('language', ChoiceType::class, array(
                'choices' => array(
                    'Alle' => 'all',
                    'de_DE' => 'de_DE',
                    'en_US' => 'en_US',
                    'fr_FR' => 'fr_FR',
                    'nl_NL' => 'nl_NL',
                    'il_IL' => 'il_IL',
                ),
                'label' => 'language'
            ))
            ->add('availability', ChoiceType::class, array(
                'choices' => array(
                    'instock' => 'in_stock',
                    'outofstock' => 'out_of_stock',
                    'preorder' => 'preorder'
                ),
                'label' => 'Availability'
            ))
            ->add('videoId', TextType::class, ['required' => false, 'label' => 'VideoId'])
            ->add('metaKeywords', TextType::class, ['required' => false, 'label' => 'Keywords'])
            ->add('metaDescription', TextareaType::class, ['required' => false, 'label' => 'description'])
            ->add('metaAuthor', TextType::class, ['required' => false, 'label' => 'Author'])
            ->add('metaCustomTitle', TextType::class, ['required' => false, 'label' => 'CustomTitle'])
            ->add('metaOgTitle', TextType::class, ['required' => false, 'label' => 'OGTitle'])
            ->add('metaOgType', TextType::class, ['required' => false, 'label' => 'OGType'])
            ->add('metaOgUrl', TextType::class, ['required' => false, 'label' => 'OGUrl'])
            ->add('metaOgImage', TextType::class, ['required' => false, 'label' => 'OGImage'])
            ->add('metaOgDescription', TextareaType::class, ['required' => false, 'label' => 'OGDescription'])
            ->add('nrIntern', TextType::class, ['required' => false, 'label' => 'Itemnumberintern'])
            ->add('nrExtern', TextType::class, ['required' => false, 'label' => 'Itemnumberextern'])
            ->add('packagingUnit', ChoiceType::class, ['required' => true, 'label' => 'Unitofmeasurement', 'choices' => $this->priceService->getPackagingUnits()])
            ->add('salesUnit', NumberType::class, ['required' => false, 'label' => 'Salesunit'])
            ->add('baseUnit', NumberType::class, ['required' => false, 'label' => 'Basicunit'])
            ->add('kostenstelle', TextType::class, ['required' => false, 'label' => 'Costcentre'])
            ->add('textArt', TextType::class, ['required' => false, 'label' => 'Art'])
            ->add('textFormat', TextType::class, ['required' => false, 'label' => 'Format'])
            ->add('einleitung', TextareaType::class, ['required' => false, 'label' => 'introduction'])
            ->add('pos', IntegerType::class, ['required' => false, 'label' => 'Position'])
            ->add('custom1', TextType::class, ['required' => false, 'label' => 'Custom 1'])
            ->add('custom2', TextType::class, ['required' => false, 'label' => 'Custom 2'])
            ->add('custom3', TextType::class, ['required' => false, 'label' => 'Custom 3'])
            ->add('custom4', TextType::class, ['required' => false, 'label' => 'Custom 4'])
            ->add('custom5', TextType::class, ['required' => false, 'label' => 'Custom 5'])
            ->add('custom6', TextType::class, ['required' => false, 'label' => 'Custom 6'])
            ->add('custom7', TextType::class, ['required' => false, 'label' => 'Custom 7'])
            ->add('custom8', TextType::class, ['required' => false, 'label' => 'Custom 8'])
            ->add('custom9', TextType::class, ['required' => false, 'label' => 'Custom 9'])
            ->add('custom10', TextType::class, ['required' => false, 'label' => 'Custom 10'])
            ->add('custom11', TextType::class, ['required' => false, 'label' => 'Custom 11'])
            ->add('custom12', TextType::class, ['required' => false, 'label' => 'Custom 12'])
            ->add('uploadProduct', CheckboxType::class, ['required' => false, 'label' => 'Activate'])
            ->add("uploadProductInitalStatus", ChoiceType::class, array(
                'label' => 'Initialstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ))
            ->add('uploadFromLatestOrder', CheckboxType::class, ['required' => false, 'label' => 'Activate'])
            ->add("uploadFromLatestOrderInitalStatus", ChoiceType::class, array(
                'label' => 'Initialstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ))
            ->add("initialStatus", ChoiceType::class, array(
                'label' => 'Masterinitialstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ))
            ->add('uploadPost', CheckboxType::class, ['required' => false, 'label' => 'Activate'])
            ->add("uploadPostInitalStatus", ChoiceType::class, array(
                'label' => 'Initialstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ))
            ->add('uploadEmail', CheckboxType::class, ['required' => false, 'label' => 'Activate'])
            ->add("uploadEmailInitalStatus", ChoiceType::class, array(
                'label' => 'Initialstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ))
            ->add('uploadProvided', CheckboxType::class, ['required' => false, 'label' => 'Activate'])
            ->add('uploadProvidedFile', MediaType::class, array('label' => 'Datei'))
            ->add("uploadProvidedInitalStatus", ChoiceType::class, array(
                'label' => 'Initalstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ))
            ->add('uploadCenter', CheckboxType::class, ['required' => false, 'label' => 'Activate'])
            ->add("uploadCenterInitalStatus", ChoiceType::class, array(
                'label' => 'Initialstatus',
                'choices' => $this->statusService->getPositionStatusAsArray(),
                'translation_domain' => 'posstatus'
            ));

        /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup()), $options));
        }

        /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getProductFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $options['productType']) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup()), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'save'));

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product) as $field) {
                $field->formPreSetData($event);
            }
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getProductFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $options['productType']) as $field) {
                $field->formPreSetData($event);
            }
        });

        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($options) {
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product) as $field) {
                $field->formPostSetData($event);
            }
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getProductFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $options['productType']) as $field) {
                $field->formPostSetData($event);
            }
        });

        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($options) {
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product) as $field) {
                $field->formPostSubmit($event);
            }
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getProductFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $options['productType']) as $field) {
                $field->formPostSubmit($event);
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($options) {
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product) as $field) {
                $field->formPreSubmit($event);
            }
            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getProductFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Product, $options['productType']) as $field) {
                $field->formPreSubmit($event);
            }
        });
    }

    public function getName()
    {
        return 'product';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Product',
            'productType' => 0,
            'product' => null,
            'productDoc' => null,
            'translation_domain' => 'core_product_edit'
        ));
    }
}
