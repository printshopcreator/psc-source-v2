<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Form\Backend\Product;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\MediaBundle\Form\Type\ImageType;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\General;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PermissionType extends AbstractType
{
    protected $shop = null;
    /** @var Status */
    protected $statusService = null;

    /** @var Field  */
    protected $fields;

    protected $formFactory;

    protected $generalService;

    protected $entity_manager;
    public function __construct(Shop $shop, Status $statusService, Field $fields, FormFactory $formFactory, General $generalService, EntityManagerInterface $entityManager)
    {
        $this->shop = $shop;
        $this->statusService = $statusService;
        $this->fields = $fields;
        $this->formFactory = $formFactory;
        $this->generalService = $generalService;
        $this->entity_manager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $contacts = $this->entity_manager->createQuery('
                SELECT c.uid,c.firstname,c.lastname FROM PSCEntityBundle:Contact c 
                JOIN c.shops s WHERE s.uid = :shop_id')
            ->setParameter('shop_id', $this->shop->getSelectedShop()->getUid())
            ->getResult(Query::HYDRATE_ARRAY);

        $tmpContacts = [];

        foreach ($contacts as $contact) {
            $tmpContacts[$contact['firstname'] . ' ' . $contact['lastname']] = $contact['uid'];
        }

        $builder
            ->add('confirm', CheckboxType::class, array('required' => false, 'label' => 'Requiresapproval'))
            ->add('confirmOne', CheckboxType::class, array('required' => false, 'label' => 'Onlycontact'))
            ->add('confirmAccount', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Account',
                'choice_label' => 'title',
                'multiple' => false,
                'required' => false,
                'label' => 'Company',
                'placeholder' => 'nothingselected',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->leftJoin('u.shops', 's')
                        ->where('s.uid = :shop')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('confirm_contacts', ChoiceType::class, array(
                'choices' => $tmpContacts,
                'multiple' => true,
                'label' => 'Persons',
            ));


        $builder->get('confirm_contacts')
            ->addModelTransformer(new CallbackTransformer(
                function ($collection) {
                    if ($collection != null) {
                        $temp = [];
                        foreach ($collection as $item) {
                            $temp[$item->getFirstname() . ' ' . $item->getLastname()] = $item->getUid();
                        }
                        return $temp;
                    }
                },
                function ($arrayValues) {
                    $contacts = $this->entity_manager->createQuery('
                        SELECT c FROM PSCEntityBundle:Contact c 
                        JOIN c.shops s WHERE s.uid = :shop_id AND c.uid in (:contact_uid)')
                        ->setParameter('shop_id', $this->shop->getSelectedShop()->getUid())
                        ->setParameter('contact_uid', array_values($arrayValues))
                        ->execute();


                    return $contacts;
                }
            ));

        $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'product';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Product',
            'productType' => 0,
            'translation_domain' => 'core_product_list'
        ));
    }
}
