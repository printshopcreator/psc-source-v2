<?php

namespace PSC\Shop\ProductBundle\Hydrate;

use Doctrine\ORM\EntityManagerInterface;

class Product
{
    private EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function hydrateToModel(\PSC\Shop\EntityBundle\Entity\Product $productEntity)
    {

        $productObj = new \PSC\Shop\ProductBundle\Model\Product();
        $productObj->id = $productEntity->getUID();
        $productObj->title = $productEntity->getTitle();
        $productObj->uuid = $productEntity->getUUID();
        $productObj->language = $productEntity->getLanguage();
        $productObj->langData = $productEntity->getLangData();
        $productObj->textArt = $productEntity->getTextArt();
        $productObj->price = $productEntity->getPrice();
        $productObj->packageFormat = $productEntity->getPackageFormat();
        $productObj->description = $productEntity->getDescription();
        $productObj->shopUUId = $productEntity->getShop()->getUid();
        if ($productEntity->getImage1() != "") {
            $file1 = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Image')->findOneBy(['uid' => $productEntity->getImage1()]);
            if ($file1) {
                $productObj->image1 = $file1->getPath();
            }
        }

        if ($productEntity->getImage2() != "") {
            $file2 = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Image')->findOneBy(['uid' => $productEntity->getImage2()]);
            if ($file2) {
                $productObj->image2 = $file2->getPath();
            }
        }

        return $productObj;
    }
}
