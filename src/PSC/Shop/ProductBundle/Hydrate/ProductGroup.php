<?php

namespace PSC\Shop\ProductBundle\Hydrate;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Shop;

class ProductGroup
{
    private EntityManagerInterface $entityManager;
    private Shop $shopService;
    private \PSC\Shop\ProductBundle\Hydrate\Product $hydrateProduct;
    public function __construct(EntityManagerInterface $entityManager, Shop $shopService, \PSC\Shop\ProductBundle\Hydrate\Product $productHydrate)
    {
        $this->entityManager = $entityManager;
        $this->shopService = $shopService;
        $this->hydrateProduct = $productHydrate;
    }

    public function hydrateToModel(\PSC\Shop\EntityBundle\Entity\ProductGroup $productGroupEntity, bool $withProducts = true)
    {

        $productGroup = $this->recursiveProductgroups($productGroupEntity, $withProducts);
        return $productGroup;
    }

    protected function recursiveProductgroups(\PSC\Shop\EntityBundle\Entity\Productgroup $group, bool $withProducts = true): \PSC\Shop\ProductBundle\Model\ProductGroup
    {
        $productGroup = new \PSC\Shop\ProductBundle\Model\ProductGroup();
        $productGroup->uuid = (string)$group->getUid();
        $productGroup->id = $group->getUid();
        $productGroup->title = $group->getTitle();
        $productGroup->langData = $group->getLangData();
        if ($group->getImage1() != "") {
            $file1 = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Image')->findOneBy(['uid' => $group->getImage1()]);
            if ($file1) {
                $productGroup->image1 = $file1->getPath();
            }
        }

        if ($group->getImage2() != "") {
            $file2 = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Image')->findOneBy(['uid' => $group->getImage2()]);
            if ($file2) {
                $productGroup->image2 = $file2->getPath();
            }
        }

        $productGroups = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')
            ->getAllByShopAndParent($this->shopService->getShopByDomain(), $group->getUid(), true);
        $temp = array();
        foreach ($productGroups as $gb) {
            $temp[] = $this->recursiveProductgroups($gb, $withProducts);
        }

        $productGroup->children = $temp;

        if($withProducts) {
            $products = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
                ->getAllByProductgroup($group, []);
            /** @var Product $product */
            foreach ($products as $product) {
                $productObj = $this->hydrateProduct->hydrateToModel($product);
                $productGroup->addProduct($productObj);
            }
        }
        return $productGroup;
    }
}
