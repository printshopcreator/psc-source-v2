<?php
namespace PSC\Shop\ProductBundle\Interfaces;

use PSC\Shop\ProductBundle\Model\Price;
use PSC\Shop\ProductBundle\Model\Product;

interface IJsonFormProducer {

    public function setProduct(Product $product): void;
    public function getPrice(): Price;
    public function getJsonForm(): array;

}