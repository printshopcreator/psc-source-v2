<?php

namespace PSC\Shop\ProductBundle\Api\ProductGroup;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Shop\ProductBundle\Dto\Cms\Cms\Output;
use PSC\Shop\ProductBundle\Dto\Error\NotFound;
use PSC\Shop\ProductBundle\Dto\Shop\Shops;
use PSC\Shop\ProductBundle\Hydrate\Cms;
use PSC\Shop\ProductBundle\Hydrate\ProductGroup;
use PSC\Shop\ProductBundle\Model\Shop;
use PSC\Shop\ProductBundle\Model\Shop\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class GetWithProducts extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private ProductGroup $hydrateProductGroup;
    private \PSC\System\SettingsBundle\Service\Shop $shopService;

    public function __construct(EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, ProductGroup $hydrateProductGroup)
    {
        $this->entityManager = $entityManager;
        $this->hydrateProductGroup = $hydrateProductGroup;
        $this->shopService = $shopService;
    }

    /**
     * Productgroups
     *
     * @Route("/productgroup/getallwithproducts", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="productgroup",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ProductBundle\Dto\ProductGroup\GetWithProducts\Output::class))
     * )
     * @OA\Tag(name="ProductGroup")
     */
    public function allAction(): JsonResponse
    {
        $productGroups = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Productgroup')
            ->getAllByShopAndParent($this->shopService->getShopByDomain(), 0, true);

        $temp = array();

        foreach ($productGroups as $productGroup) {
            $temp[] = $this->hydrateProductGroup->hydrateToModel($productGroup);
        }

        return $this->json(new \PSC\Shop\ProductBundle\Dto\ProductGroup\GetWithProducts\Output($temp));
    }
}
