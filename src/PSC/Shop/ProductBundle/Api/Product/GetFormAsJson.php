<?php

namespace PSC\Shop\ProductBundle\Api\Product;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\ProductBundle\Dto\Product\GetFormAsJson\Output;
use PSC\Shop\ProductBundle\Hydrate\Product as PSCProduct;
use PSC\Shop\QueueBundle\Event\Position\Contact\Approval\Request;
use PSC\System\PluginBundle\Service\ProductType;
use PSC\System\SettingsBundle\Model\Papercontainer;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use PSC\System\SettingsBundle\Model\Paper as PSCPaper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class GetFormAsJson extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private PSCProduct $productHydrate;
    private ProductType $productTypeRegistry;

    public function __construct(EntityManagerInterface $entityManager, ProductType $productType, PSCProduct $productHydrate)
    {
        $this->entityManager = $entityManager;
        $this->productHydrate = $productHydrate;
        $this->productTypeRegistry = $productType;
    }
    /**
     * get product
     *
     * @Route("/product/get/form/json/{uuid}", methods={"POST"})
     * @OA\Response(
     *     response=200,
     *     description="get form",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ProductBundle\Model\Product::class))
     * )
     * @OA\Tag(name="Product")
     */
    public function getForm(string $uuid): JsonResponse
    {
        /** @var Product $product */
        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['uuid' => $uuid]);

        if ($product) {

            $output = new Output();

            $productObj = $this->productTypeRegistry->getProductType($product->getType());

            /** @var \PSC\Shop\ProductBundle\Model\Product $productModel */
            $productModel = $this->productHydrate->hydrateToModel($product);

            $producer = $productObj->getJsonFormProducer();
            $producer->setProduct($productModel);
            $output->jsonForm = $producer->getJsonForm();
            $output->price = $producer->getPrice();

        } else {
            $output = new NotFound("Product not found");
        }
        return $this->json($output);
    }
}
