<?php

namespace PSC\Shop\ProductBundle\Api\Product;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\ProductBundle\Dto\Product\GetAllByProductGroup\Output;
use PSC\Shop\ProductBundle\Hydrate\Product as PSCProduct;
use PSC\System\SettingsBundle\Model\Papercontainer;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use PSC\System\SettingsBundle\Model\Paper as PSCPaper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class GetAllByProductGroup extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private PSCProduct $productHydrate;

    public function __construct(EntityManagerInterface $entityManager, PSCProduct $productHydrate)
    {
        $this->entityManager = $entityManager;
        $this->productHydrate = $productHydrate;
    }
    /**
     * get product
     *
     * @Route("/product/getallbyproductgroup/{uuid}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get papercontainer",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ProductBundle\Dto\Product\GetAllByProductGroup\Output::class))
     * )
     * @OA\Tag(name="Product")
     */
    public function getAll(string $uuid): JsonResponse
    {
        $group = $this->entityManager->getRepository(Productgroup::class)->findOneBy(['uid' => $uuid]);

        if ($group) {
            $temp = [];

            $products = $this->entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
                ->getAllByProductgroup($group, []);
            /** @var Product $product */
            foreach ($products as $product) {
                $temp[] = $this->productHydrate->hydrateToModel($product);
            }

            $output = new Output($temp);
        } else {
            $output = new NotFound("Productgroup not found");
        }
        return $this->json($output);
    }
}
