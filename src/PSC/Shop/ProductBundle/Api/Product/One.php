<?php

namespace PSC\Shop\ProductBundle\Api\Product;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\ProductBundle\Hydrate\Product as PSCProduct;
use PSC\System\SettingsBundle\Model\Papercontainer;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use PSC\System\SettingsBundle\Model\Paper as PSCPaper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class One extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private PSCProduct $productHydrate;

    public function __construct(EntityManagerInterface $entityManager, PSCProduct $productHydrate)
    {
        $this->entityManager = $entityManager;
        $this->productHydrate = $productHydrate;
    }
    /**
     * get product
     *
     * @Route("/product/{uuid}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get papercontainer",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ProductBundle\Model\Product::class))
     * )
     * @OA\Tag(name="Product")
     */
    public function getOne(string $uuid): JsonResponse
    {
        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['uuid' => $uuid]);

        if ($product) {
            $output = $this->productHydrate->hydrateToModel($product);
        } else {
            $output = new NotFound("Product not found");
        }
        return $this->json($output);
    }
}
