<?php

namespace PSC\Shop\ProductBundle\Api\Product;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Paper;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\ProductBundle\Dto\Product\GetAllByProductGroup\Output;
use PSC\Shop\ProductBundle\Hydrate\Product as PSCProduct;
use PSC\System\SettingsBundle\Model\Papercontainer;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use PSC\System\SettingsBundle\Model\Paper as PSCPaper;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Nelmio\ApiDocBundle\Annotation\Security;

class GetAllByTerm extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private PSCProduct $productHydrate;

    public function __construct(EntityManagerInterface $entityManager, PSCProduct $productHydrate)
    {
        $this->entityManager = $entityManager;
        $this->productHydrate = $productHydrate;
    }
    /**
     * get product
     *
     * @Route("/product/getallbyterm/{shop_uuid}/{term}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get papercontainer",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ProductBundle\Dto\Product\GetAllByProductGroup\Output::class))
     * )
     * @OA\Tag(name="Product")
     */
    public function getAll(string $term, string $shop_uuid): JsonResponse
    {
            $temp = [];

        $products = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Product')
            ->createQueryBuilder('product')
            ->leftJoin('product.shop', 'shop')
            ->andWhere("
                    shop.uid = :shop_id and
                    (product.uid LIKE '%" . $term . "%' OR
                    product.title LIKE '%" . $term . "%' OR
                    product.calcXml LIKE '%" . $term . "%' OR
                    product.textFormat LIKE '%" . $term . "%' OR
                    product.description LIKE '%" . $term . "%' OR
                    product.nrIntern LIKE '%" . $term . "%' OR
                    product.nrExtern LIKE '%" . $term . "%' OR
                    product.textArt LIKE '%" . $term . "%')
                ")->setParameter('shop_id', $shop_uuid)->getQuery()->execute();

        /** @var Product $product */
        foreach ($products as $product) {
            $temp[] = $this->productHydrate->hydrateToModel($product);
        }

        $output = new Output($temp);

        return $this->json($output);
    }
}
