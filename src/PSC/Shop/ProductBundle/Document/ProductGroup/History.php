<?php

namespace PSC\Shop\ProductBundle\Document\ProductGroup;

use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="producktgroupObj_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $producktgroupObj;

    function __construct(string $producktgroupObj = "")
    {
        parent::__construct();
        $this->producktgroupObj = $producktgroupObj;
    }

    public function getproducktgroupObj()
    {
        return $this->producktgroupObj;
    }

    public function setproducktgroupObj(string $producktgroupObj)
    {
        $this->producktgroupObj = $producktgroupObj;
    }

    public function getReferenceField(): string
    {
        return 'producktgroupObj';
    }
}
