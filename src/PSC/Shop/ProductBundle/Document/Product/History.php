<?php

namespace PSC\Shop\ProductBundle\Document\Product;

use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="productObj_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $productObj;

    function __construct(string $productObj = "")
    {
        parent::__construct();
        $this->productObj = $productObj;
    }

    public function getProductObj()
    {
        return $this->productObj;
    }

    public function setProductObj(string $productObj)
    {
        $this->productObj = $productObj;
    }

    public function getReferenceField(): string
    {
        return 'productObj';
    }
}
