<?php

namespace PSC\Shop\ProductBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ConfigureMenuListener
{
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $factory = $event->getFactory();
            $submenu = $menu->addChild('products', array(
                'uri' => '#',
                'label' => 'Produkte',
                'extras' => array(
                    'icon' => 'fas fa-tags',
                    'orderNumber' => 13
                )
            ));
            $submenu->addChild('Produkte', array(
                'route' => 'psc_shop_product_backend_list',
                'extras' => array(
                    'icon' => 'fas fa-tags',
                    'orderNumber' => 3
                )
            ));
            $submenu->addChild('Produktgruppen', array(
                'route' => 'psc_shop_productgroup_backend_list',
                'extras' => array(
                    'icon' => 'fas fa-folder',
                    'orderNumber' => 4,
                    'routes' => array('psc_shop_productgroup_backend_create', 'psc_shop_productgroup_backend_edit')
                )
            ));
        }
    }
}
