<?php

namespace PSC\Shop\ProductBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Price
{
    public float $net;
    /** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    public float $allNet;
    /** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    public float $vat;
    /** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    public float $allVat;
    /** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    public float $gross;
    /** @var double
     *
     * @OA\Property(type="number", format="double")
     */
    public float $allGross;
}