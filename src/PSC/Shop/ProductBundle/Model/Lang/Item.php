<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Model\Lang;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Item
{
    /**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $title = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $code = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $einleitung = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $text_art = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $info = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $uuid = "";
}
