<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ProductBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Product
{
    /**
     * @OA\Property(type="string", maxLength=255)
     */
    public int $id;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $uuid = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $shopUUId = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $title = "";
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $image1;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public string $image2;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $nrExtern;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $description;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $packageFormat;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $textArt;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $price;
/**
     * @OA\Property(type="string", maxLength=255)
     */
    public ?string $language;
/**
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\ProductBundle\Model\Lang\Item::class)))
     */
    public array $langData = [];
}
