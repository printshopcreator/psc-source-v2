<?php

namespace PSC\Shop\ProductBundle\Service;

use LogicException;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;

class Registry implements RegistryInterface
{
    /** @var  QueueInterface[] */
    private $products = array();
/** @return QueueInterface[] */
    public function all()
    {
        return $this->queues;
    }

    public function getAllByGroup()
    {
        $temp = array();
        foreach ($this->queues as $type => $queue) {
            $temp[$queue->getGroup()][$type] = $queue;
        }
        return $temp;
    }

    /**
     * @param $key string
     *
     * @return ExporterInterface
     */
    public function get($key)
    {
        if (!$this->has($key)) {
            throw new LogicException('Exporter ' . $key . ' not registered');
        }

        return $this->queues[$key];
    }

    public function has($type)
    {
        return array_key_exists($type, $this->queues);
    }

    /**
     * @param RegistryElementInterface $element
     */
    public function add(RegistryElementInterface $element)
    {
        if ($this->has($element->getType())) {
            throw new LogicException('Exporter ' . $element->getType() . ' already registered');
        }

        $this->queues[$element->getType()] = $element;
    }

    /**
     * @param RegistryElementInterface $element
     */
    public function remove(RegistryElementInterface $element)
    {
        if (!$this->has($element->getType())) {
            throw new LogicException('Exporter ' . $element->getType() . ' not registered');
        }

        unset($this->queues[$element->getType()]);
    }
}
