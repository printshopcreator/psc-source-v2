<?php

namespace PSC\Shop\ProductBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ORM\EntityManagerInterface;
use PSC\System\SettingsBundle\Service\Shop;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Product
 *
 * @author Thomas Peterson
 */
class Product
{
    protected $entityManager;
    protected $mongoManager;
    /**
     * @var Shop
     */
    private $shopService;

    public function __construct(EntityManagerInterface $em, DocumentManager $mongoManager, Shop $shopService)
    {
        $this->entityManager = $em;
        $this->mongoManager = $mongoManager;
        $this->shopService = $shopService;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Product $product
     * @param bool $markAsCopy
     * @param bool $markAsSubProduct
     * @param bool $shop
     * @return \PSC\Shop\EntityBundle\Entity\Product
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function copy($product, $markAsCopy = true, $markAsSubProduct = false, $shop = false)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Product $product */
        $uid = $product->getUID();

        $f = clone $product;
        $f->setUID(null);
        $f->setUUID(Uuid::uuid4());
        if ($markAsCopy) {
            $f->setUrl($f->getUrl() . '_kopie');
            $f->setTitle($f->getTitle() . ' Kopie');
        }
        if ($markAsSubProduct) {
            $f->setOriginalProduct($uid);
        }
        if ($shop) {
            $f->setShop($shop);
        }

        $this->entityManager->persist($f);
        $this->entityManager->flush();

        $path = str_split($f->getUID());

        $original = $f->getTemplatePrintDirectory();
        $f->setTemplatePrintDirectory(implode('/', $path) . '/');

        if (file_exists('/data/www/old/market/templateprint/orginal/' . $original . '/product.zip')) {
            mkdir('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 0777, true);
            copy('/data/www/old/market/templateprint/orginal/' . $original . '/product.zip', '/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
            chmod('/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/product.zip', 0777);
        }

        if (file_exists('/data/www/old/market/templateprint/orginal/' . $original . '/final.pdf')) {
            mkdir('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 0777, true);
            copy('/data/www/old/market/templateprint/orginal/' . $original . '/final.pdf', '/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/final.pdf');
            chmod('/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/final.pdf', 0777);
        }

        if (file_exists('/data/www/old/market/templateprint/orginal/' . $original . '/preview.pdf')) {
            mkdir('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 0777, true);
            copy('/data/www/old/market/templateprint/orginal/' . $original . '/preview.pdf', '/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/preview.pdf');
            chmod('/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/preview.pdf', 0777);
        }
        exec(' chmod -R 0777 /data/www/old/market/templateprint/orginal/' . implode('/', $path));
        $this->entityManager->persist($f);
        $this->entityManager->flush();

        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
        $productDoc = $this->mongoManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Product')
            ->findOneBy(array('uid' => (string)$uid));

        if ($productDoc) {
            $d = clone $productDoc;
            $d->setId(null);
            $d->setUid($f->getUID());
            $this->mongoManager->detach($d);
            $this->mongoManager->persist($d);
            $this->mongoManager->flush();
        }

        return $f;
    }
}
