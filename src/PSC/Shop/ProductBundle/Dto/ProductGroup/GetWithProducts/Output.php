<?php

namespace PSC\Shop\ProductBundle\Dto\ProductGroup\GetWithProducts;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Shop\ProductBundle\Model\ProductGroup;

class Output
{
    public function __construct(array $productGroups)
    {
        $this->data = $productGroups;
        $this->count = count($productGroups);
    }

    /**
     * @var ProductGroup[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\ProductBundle\Model\ProductGroup::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
