<?php

namespace PSC\Shop\ProductBundle\Dto\Product\GetFormAsJson;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Shop\ProductBundle\Model\Price;
use PSC\Shop\ProductBundle\Model\Product;

class Output
{
    /**
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\ProductBundle\Model\Price::class)))
     */
    public Price $price;

    /**
     * @OA\Property(type="array")
     */
    public array $jsonForm;
}
