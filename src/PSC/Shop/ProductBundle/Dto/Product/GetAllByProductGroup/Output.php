<?php

namespace PSC\Shop\ProductBundle\Dto\Product\GetAllByProductGroup;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Shop\ProductBundle\Model\Product;

class Output
{
    public function __construct(array $products)
    {
        $this->data = $products;
        $this->count = count($products);
    }

    /**
     * @var Product[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\ProductBundle\Model\Product::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
