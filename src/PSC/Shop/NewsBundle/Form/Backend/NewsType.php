<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\NewsBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsType extends AbstractType
{
    /** @var Field  */
    protected $fields;
    public function __construct(Field $fields)
    {
        $this->fields = $fields;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Title', 'required' => false])
            ->add('enable', CheckboxType::class, ['label' => 'active', 'required' => false])
            ->add('sortDate', DateType::class, ['label' => 'Sortdate', 'required' => false])
            ->add('url', TextType::class, ['label' => 'Url', 'required' => false])
            ->add('introduction', TextareaType::class, ['label' => 'Introduction', 'required' => false])
            ->add('language', ChoiceType::class, [
                'label' => 'language',
                'choices' => [
                    'Alle' => 'all',
                    'Deutsch' => 'de_DE',
                    'Englisch' => 'en_EN'
                ],
                'required' => true])
            ->add('text', TextareaType::class, array(
                'label' => 'Text',
                'required' => false
            ));
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::News) as $field) {
            $field->buildForm($builder, $options);
        }

        $builder->add('save', SubmitType::class, array('label' => 'save'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            $data = $event->getData();
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::News) as $field) {
                $field->formPreSubmit($data);
            }
            $event->setData($data);
        });
    }

    public function getName()
    {
        return 'news';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\NewsBundle\Entity\News',
            'translation_domain' => 'core_news_edit'
        ));
    }
}
