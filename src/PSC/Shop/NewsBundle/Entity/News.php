<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\NewsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Cms
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="PSC\Shop\NewsBundle\Repository\NewsRepository")
 *
 * @package    PSC\Shop\News
 * @subpackage Entitys
 */
class News
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;
/**
     * Titel
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;
/**
     * Titel
     *
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    protected $url;
/**
     * Titel
     *
     * @var string
     *
     * @ORM\Column(name="einleitung", type="string", length=255)
     */
    protected $introduction;
/**
     * Language
     *
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5)
     */
    protected $language;
/**
     * Text
     *
     * @var string
     *
     * @ORM\Column(name="text", type="string")
     */
    protected $text;
/**
     * Shop zu welcher die News gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PSC\Shop\EntityBundle\Entity\Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * SortDate
     *
     * @var date
     *
     * @ORM\Column(name="sort_date", type="date")
     */
    protected $sortDate;
/**
     * enable
     *
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    protected $enable;
/**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getIntroduction()
    {
        return $this->introduction;
    }

    /**
     * @param string $introduction
     */
    public function setIntroduction($introduction)
    {
        $this->introduction = $introduction;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return date
     */
    public function getSortDate()
    {
        return $this->sortDate;
    }

    /**
     * @param date $sortDate
     */
    public function setSortDate($sortDate)
    {
        $this->sortDate = $sortDate;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        if ($this->language == '') {
            return 'de';
        }
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }
}
