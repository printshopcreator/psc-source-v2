<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\NewsBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\NewsBundle\Document\History as PSCHistory;
use PSC\Shop\NewsBundle\Entity\News;
use PSC\Shop\NewsBundle\Form\Backend\DeleteType;
use PSC\Shop\NewsBundle\Form\Backend\NewsType;
use PSC\Shop\NewsBundle\PSCShopNewsBundle;
use PSC\Shop\PaymentBundle\Form\Backend\PaymentType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * NewsController
 *
 * @package    PSC\Shop\News
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;
    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }

    /**
     * Create
     *
     * @Route("/edit/create", name="psc_shop_news_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param Field $field
     * @param EntityManagerInterface $entityManager
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(Request $request, SessionInterface $session, Field $field, EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService)
    {
        $customFields = $field->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::News);
        $customGroups = $field->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::News);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $news = new News();
        $news->setShop($selectedShop);
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($news);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'News Entry \'' . $news->getTitle() . '\' has been created!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopNewsBundle::class, $news->getTitle(), "News Entry saved");
            return $this->redirectToRoute('psc_shop_news_backend_list');
        }

        return array(
            'news' => $news,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups
        );
    }

    /**
     * edit
     *
     * @Route("/edit/edit/{uid}", name="psc_shop_news_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $field
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(Request $request, Field $field, SessionInterface $session, EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, $uid)
    {
        $customFields = $field->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::News);
        $customGroups = $field->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::News);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var News $news */
        $news = $entityManager
            ->getRepository('PSC\Shop\NewsBundle\Entity\News')->findOneBy(array('uid' => $uid, 'shop' => $selectedShop));
        $form = $this->createForm(NewsType::class, $news);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->historyService->createHistoryEntry(new PSCHistory((string)$news->getUid()), $news);
            $entityManager->persist($news);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'News Entry \'' . $news->getTitle() . '\' has been updated!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopNewsBundle::class, $news->getTitle(), "News Entry saved");
            return $this->redirectToRoute('psc_shop_news_backend_list');
        }
        return array(
            'news' => $news,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$news->getUid())
        );
    }

    /**
     * remove
     *
     * @Route("/edit/delete/{uid}", name="psc_shop_news_backend_delete")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, SessionInterface $session, EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService, $uid)
    {
        $news = $entityManager->getRepository('PSC\Shop\NewsBundle\Entity\News')->findOneBy(['uid' => $uid]);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $orgTitle = $news->getTitle();
                $entityManager->remove($news);
                $entityManager->flush();
                $session->getFlashBag()->add('success', 'News Entry \'' . $orgTitle . '\' has been deleted!');
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopNewsBundle::class, $orgTitle, "News Entry deleted");
                return $this->redirectToRoute('psc_shop_news_backend_list');
            }
            return $this->redirectToRoute('psc_shop_news_backend_list');
        }


        return array(
            'news' => $news,
            'form' => $form->createView()
        );
    }
}
