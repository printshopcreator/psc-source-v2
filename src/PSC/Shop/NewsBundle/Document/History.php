<?php

namespace PSC\Shop\NewsBundle\Document;

use PSC\Shop\EntityBundle\Entity\News;
use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="news_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $news;

    function __construct(string $news = "")
    {
        parent::__construct();
        $this->news = $news;
    }

    public function getNews()
    {
        return $this->news;
    }

    public function setNews(string $news)
    {
        $this->news = $news;
    }

    public function getReferenceField(): string
    {
        return 'news';
    }
}
