<?php

namespace PSC\Shop\SettingsBundle\Service;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\ObjectId;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\SettingsBundle\Document\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * General
 *
 * @author Thomas Peterson
 */
class Routing
{
    public static $MODULE_LAGACY = 1;

    public static $CONTROLLER_PRODUCT = 1;
    public static $CONTROLLER_CMS = 2;
    public static $CONTROLLER_PRODUCTGROUP = 3;
    public static $CONTROLLER_REDIRECT = 4;

    public static $ACTION_INDEX = 1;

    public static $ACTION_SHOW = 2;

    protected $mongoManager;

    public function __construct(DocumentManager $mongoManager)
    {
        $this->mongoManager = $mongoManager;
    }

    public function createRoute(\PSC\Shop\EntityBundle\Entity\Shop $shop, int $module = 1, int $controller = 1, int $action = 1, string $url, $referenceUuid, $lang = 'de', $parameters = array(), $crudSave = true)
    {
        $route = $this->mongoManager->getRepository(Route::class)
            ->findOneBy(array(
                    'shop' => (string)$shop->getUuid(),
                    'module' => $module,
                    'controller' => $controller,
                    'action' => $action,
                    'referenceUuid' => (string)$referenceUuid,
                    'crudSave' => $crudSave,
                    'lang' => (string)$lang
                ));

        if ($referenceUuid == null || $url == null) {
            return;
        }

        if (!$route) {
            $route = new Route();
        }

        $route->setShop($shop->getUuid());
        $route->setModule($module);
        $route->setController($controller);
        $route->setAction($action);
        $route->setUrl($url);
        $route->setReferenceUuid($referenceUuid);
        $route->setParameter($parameters);
        $route->setCrudSave($crudSave);
        $route->setLang($lang);

        $this->mongoManager->persist($route);
        $this->mongoManager->flush();
    }

    public function clearRoutes(\PSC\Shop\EntityBundle\Entity\Shop $shop, int $module = 1, int $controller = 1, int $action = 1)
    {
        $coll = $this->mongoManager->getDocumentCollection(Route::class);
        $coll->deleteMany(array(
                'shop' => (string)$shop->getUuid(),
                'module' => $module,
                'controller' => $controller,
                'action' => $action
            ));
    }

    public function deleteRoute(string $uuid)
    {
        $route = $this->mongoManager
            ->getRepository(Route::class)
            ->findOneBy(array('id' => new ObjectId($uuid)));
        $this->mongoManager->remove($route);
        $this->mongoManager->flush();
    }
}
