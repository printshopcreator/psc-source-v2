<?php

namespace PSC\Shop\SettingsBundle\Event;

final class MenuEvents
{
    /**
     * @var string
     */
    const CONFIGURE_MAIN = 'psc.backend.settings.main';

    /**
     * @var string
     */
    const CONFIGURE_LAYOUTER = 'psc.backend.layouter';

    /**
     * @var string
     */
    const CONFIGURE_TOP = 'psc.backend.settings.top';
}
