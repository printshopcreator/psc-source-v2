<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\SettingsBundle\Form\Backend;

use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoutingRedirectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('url', TextType::class, array('required' => false, 'label' => 'URL'))
            ->add('mode', ChoiceType::class, array('required' => false, 'label' => 'Mode', 'choices' => ['Redirect 301' => 'redirect_301']))
            ->add('target', TextType::class, array('required' => false, 'label' => 'Target'));

        $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'routing_redirect';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'core_settings_edit'
        ));
    }
}
