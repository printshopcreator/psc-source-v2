<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\SettingsBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RoutingConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('routingConfigCMS', TextType::class, array('required' => false, 'label' => 'CMS'))
            ->add('routingConfigProduct', TextType::class, array('required' => false, 'label' => 'Product'))
            ->add('routingConfigProductGroup', TextType::class, array('required' => false, 'label' => 'ProductGroup'));
        $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'routingconfig';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'core_settings_edit'
        ));
    }
}
