<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\SettingsBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Language;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryBuilderInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopSettingsType extends AbstractType
{
    /** @var Field  */
    protected $fields;
    protected $formFactory;
    protected Language $localeService;
    public function __construct(Language $localeService, Field $fields, FormFactoryInterface $formFactory)
    {
        $this->fields = $fields;
        $this->formFactory = $formFactory;
        $this->localeService = $localeService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('required' => false, 'label' => 'Title'))
            ->add('uid', HiddenType::class)
            ->add('subTitle', TextType::class, array('required' => false, 'label' => 'subtitle'))
            ->add('keywords', TextareaType::class, array('required' => false, 'label' => 'Keywords'))
            ->add('description', TextareaType::class, array('required' => false, 'label' => 'description'))
            ->add('author', TextareaType::class, array('required' => false, 'label' => 'Autor'))
            ->add('copyright', TextareaType::class, array('required' => false, 'label' => 'Copyright'))
            ->add('logo1', MediaType::class, array('label' => 'Logo'))
            ->add('sitemap', MediaType::class, array('label' => 'Sitemap'))
            ->add('robots', MediaType::class, array('label' => 'robots'))
            ->add('logo2', MediaType::class, array('label' => 'Logo2'))
            ->add('private', CheckboxType::class, array('required' => false, 'label' => 'Private'))
            ->add('guestEnable', CheckboxType::class, array('required' => false, 'label' => 'guestEnable'))
            ->add('registration', CheckboxType::class, array('required' => false, 'label' => 'Registrationpossible'))
            ->add('noVerify', CheckboxType::class, array('required' => false, 'label' => 'Withoutactivation'))

            ->add('betreiberCompany', TextType::class, array('required' => false, 'label' => 'company'))
            ->add('betreiberName', TextType::class, array('required' => false, 'label' => 'Name'))
            ->add('betreiberStreet', TextType::class, array('required' => false, 'label' => 'Streethousenumber'))
            ->add('betreiberAddress', TextType::class, array('required' => false, 'label' => 'ZipCity'))
            ->add('betreiberEmail', TextType::class, array('required' => false, 'label' => 'EMail'))
            ->add('betreiberTel', TextType::class, array('required' => false, 'label' => 'Phone'))
            ->add('betreiberFax', TextType::class, array('required' => false, 'label' => 'Fax'))
            ->add('betreiberUid', TextType::class, array('required' => false, 'label' => 'VATnumber'))
            ->add('betreiberHid', TextType::class, array('required' => false, 'label' => 'registrationnumber'))
            ->add('betreiberSid', TextType::class, array('required' => false, 'label' => 'taxnumber'))
            ->add('betreiberRegister', TextType::class, array('required' => false, 'label' => 'register'))
            ->add('betreiberVb', TextType::class, array('required' => false, 'label' => 'Authorizedrepresentative'))
            ->add('betreiberRechtsform', TextType::class, array('required' => false, 'label' => 'legalform'))
            ->add('betreiberWeb', TextType::class, array('required' => false, 'label' => 'Homepage'))

            ->add('senderCompany', TextType::class, array('required' => false, 'label' => 'company'))
            ->add('senderFirstname', TextType::class, array('required' => false, 'label' => 'firstname'))
            ->add('senderLastname', TextType::class, array('required' => false, 'label' => 'lastname'))
            ->add('senderStreet', TextType::class, array('required' => false, 'label' => 'Street'))
            ->add('senderHouseNumber', TextType::class, array('required' => false, 'label' => 'housenumber'))
            ->add('senderZip', TextType::class, array('required' => false, 'label' => 'Zip'))
            ->add('senderCity', TextType::class, array('required' => false, 'label' => 'City'))
            ->add('senderCountry', TextType::class, array('required' => false, 'label' => 'Country'))
            ->add('senderTel', TextType::class, array('required' => false, 'label' => 'Phone'))
            ->add('senderFax', TextType::class, array('required' => false, 'label' => 'Fax'))
            ->add('senderMobile', TextType::class, array('required' => false, 'label' => 'mobilephonenumber'))
            ->add('senderEmail', TextType::class, array('required' => false, 'label' => 'EMail'))

            ->add('priceFactor', NumberType::class, array('required' => false, 'label' => 'Factor', 'scale' => 5, 'html5' => true,
                'attr' => array(
                    'min' => -0.00001,
                    'max' => 2.00,
                    'step' => 0.00001,
            )))

            ->add('ownNumber', CheckboxType::class, array('required' => false, 'label' => 'Useyourownnumberrange'))
            ->add('numberStart', TextType::class, array('required' => false, 'label' => 'startnumber'))
            ->add('numberPattern', TextType::class, array('required' => false, 'label' => 'Numberrangeformatexample'))
            ->add('contactOwnNumber', CheckboxType::class, array('required' => false, 'label' => 'Useyourownnumberrange'))
            ->add('offerOwnNumber', CheckboxType::class, array('required' => false, 'label' => 'Useyourownnumberrange'))
            ->add('deleted', CheckboxType::class, array('required' => false, 'label' => 'Storedisabled'))
            ->add('contactNumberStart', TextType::class, array('required' => false, 'label' => 'startnumber'))
            ->add('offerNumberStart', TextType::class, array('required' => false, 'label' => 'startnumber'))
            ->add('contactNumberPattern', TextType::class, array('required' => false, 'label' => 'Numberrangeformatexample'))
            ->add('offerNumberPattern', TextType::class, array('required' => false, 'label' => 'Numberrangeformatexample'))
            ->add('redirectAfterBuy', TextType::class, array('required' => false, 'label' => 'Redirectafterorder'))
            ->add('redirectLogin', TextType::class, array('required' => false, 'label' => 'Redirectafterlogin'))
            ->add('redirectLogout', TextType::class, array('required' => false, 'label' => 'Redirectafterlogout'))
            ->add('redirectToSSL', CheckboxType::class, array('required' => false, 'label' => 'alwaysforwardtoSSL'))
            ->add('createPackageAfterBuy', CheckboxType::class, array('required' => false, 'label' => 'Generatepackagedirectly'))
            ->add('tpSaveUserData', CheckboxType::class, array('required' => false, 'label' => 'Restorecustomerdata'))
            ->add('apiKey', TextType::class, array('required' => false, 'disabled' => true, 'label' => 'APIKey'))
            ->add('docJobticketJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docJobticketBg', MediaType::class, array('label' => 'background'))
            ->add('docOfferProductBg', MediaType::class, array('label' => 'background'))
            ->add('docOfferFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docOfferProductXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docJobticketPositionBg', MediaType::class, array('label' => 'background'))
            ->add('docJobticketFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docJobticketPosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docJobticketPPJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docJobticketPPBg', MediaType::class, array('label' => 'background'))
            ->add('docJobticketPPFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docJobticketPPPositionBg', MediaType::class, array('label' => 'background'))
            ->add('docJobticketPPPosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docOfferJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docOfferBg', MediaType::class, array('label' => 'background'))
            ->add('docOfferFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docOfferPositionBg', MediaType::class, array('label' => 'background'))
            ->add('docOfferPosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docOrderJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docOrderBg', MediaType::class, array('label' => 'background'))
            ->add('docOrderFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docOrderPositionBg', MediaType::class, array('label' => 'background'))
            ->add('docOrderPosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docInvoiceJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docInvoiceBg', MediaType::class, array('label' => 'background'))
            ->add('docInvoiceFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docInvoicePositionBg', MediaType::class, array('label' => 'background'))
            ->add('docInvoicePosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docLabelJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docLabelBg', MediaType::class, array('label' => 'background'))
            ->add('docLabelFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docLabelPositionBg', MediaType::class, array('label' => 'background'))
            ->add('docLabelPosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docDeliveryJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docDeliveryBg', MediaType::class, array('label' => 'background'))
            ->add('docDeliveryFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docDeliveryPositionBg', MediaType::class, array('label' => 'background'))
            ->add('docDeliveryPosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docStornoJasperXml', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('docStornoBg', MediaType::class, array('label' => 'background'))
            ->add('docStornoFileName', TextType::class, array('label' => 'filename', 'required' => false))
            ->add('docStornoPositionBg', MediaType::class, array('label' => 'background'))
            ->add('docStornoPosition', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
            ->add('productFieldName1', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field1Designation'))
            ->add('productFieldName2', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field2Designation'))
            ->add('productFieldName3', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field3Designation'))
            ->add('productFieldName4', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field4Designation'))
            ->add('productFieldName5', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field5Designation'))
            ->add('productFieldName6', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field6Designation'))
            ->add('productFieldName7', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field7Designation'))
            ->add('productFieldName8', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field8Designation'))
            ->add('productFieldName9', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field9Designation'))
            ->add('productFieldName10', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field10Designation'))
            ->add('productFieldName11', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field11Designation'))
            ->add('productFieldName12', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field12Designation'))

            ->add('customerFieldName1', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field1Designation'))
            ->add('customerFieldName2', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field2Designation'))
            ->add('customerFieldName3', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field3Designation'))
            ->add('customerFieldName4', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field4Designation'))
            ->add('customerFieldName5', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field5Designation'))
            ->add('customerFieldName6', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field6Designation'))
            ->add('customerFieldName7', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field7Designation'))
            ->add('customerFieldName8', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field8Designation'))
            ->add('customerFieldName9', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field9Designation'))
            ->add('customerFieldName10', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field10Designation'))
            ->add('customerFieldName11', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field11Designation'))
            ->add('customerFieldName12', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field12Designation'))
            ->add('customerFieldName13', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field13Designation'))
            ->add('customerFieldName14', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field14Designation'))
            ->add('customerFieldName15', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field15Designation'))
            ->add('customerFieldName16', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field16Designation'))
            ->add('customerFieldName17', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field17Designation'))
            ->add('customerFieldName18', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field18Designation'))
            ->add('customerFieldName19', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field19Designation'))
            ->add('customerFieldName20', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field20Designation'))
            ->add('customerFieldName21', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field21Designation'))
            ->add('customerFieldName22', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field22Designation'))
            ->add('customerFieldName23', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field23Designation'))
            ->add('customerFieldName24', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Field24Designation'))

            ->add('defaultLocale', ChoiceType::class, array('choices' => $this->localeService->getShopLocale(), 'required' => true, 'label' => 'defaultLanguage'))
            ->add('defaultCurrency', CurrencyType::class, array('required' => true, 'label' => 'defaultCurrency'))
            ->add('extraSettings', TextareaType::class, array('required' => false, 'label' => 'extrasettings'))

            ->add('mailOrderCustomerSubjectTemplate', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'Ordermailtocustomersubject'))
            ->add('mailOrderCustomerTextTemplate', TextareaType::class, array(
                'required' => false,
                'disabled' => false,
                'label' => 'Ordermailtocustomer',
                'attr' => ['rows' => 10]))
            ->add('imprintText', TextareaType::class, array(
                'label' => 'Text',
                'required' => false,
            ))
            ->add('conditionsText', TextareaType::class, array(
                'label' => 'Text',
                'required' => false,
            ))
            ->add('cancellationTermsText', TextareaType::class, array(
                'label' => 'Text',
                'required' => false,
            ))
            ->add('privacyPolicyText', TextareaType::class, array(
                'label' => 'Text',
                'required' => false,
            ))
            ->add('productSort', ChoiceType::class, array(
                'label' => 'productsorting',
                'choices' => ['Position' => 'pos', 'Name' => 'name'],
                'required' => false,
            ))
            ->add('productSortDir', ChoiceType::class, array(
                'label' => 'productsorting',
                'choices' => ['Ascending' => 'asc', 'Descending' => 'desc'],
                'required' => false,
            ));
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shop) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup(), FormType::class, null, ['mapped' => false]), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'save'));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shop) as $field) {
                $field->formPreSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shop) as $field) {
                $field->formPostSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shop) as $field) {
                $field->formPostSubmit($event);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shop) as $field) {
                $field->formPreSubmit($event);
            }
        });
    }

    public function getName()
    {
        return 'shopSettings';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Document\Shop',
            'translation_domain' => 'core_settings_edit'
        ));
    }
}
