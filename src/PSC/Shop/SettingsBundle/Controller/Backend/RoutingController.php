<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\SettingsBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\Shop\EntityBundle\Entity\Cms;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\SettingsBundle\Form\Backend\RoutingRedirectType;
use PSC\Shop\SettingsBundle\Form\Backend\RoutingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use PSC\Shop\SettingsBundle\Form\Backend\RoutingConfigType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PSC\Shop\SettingsBundle\Service\Routing;

class RoutingController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/routing/index", name="psc_backend_shop_routing_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, Routing $routingService, DocumentManager $documentManager)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var \PSC\Shop\EntityBundle\Document\Shop $selectedShopMongo */
        $selectedShopMongo = $shopService->getMongoSelectedShop();
        $data = [
            'routingConfigProduct' => $selectedShopMongo->getRoutingConfigProduct(),
            'routingConfigProductGroup' => $selectedShopMongo->getRoutingConfigProductGroup(),
            'routingConfigCMS' => $selectedShopMongo->getRoutingConfigCMS()];
        $form = $this->createForm(RoutingConfigType::class, $data);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $selectedShopMongo->setRoutingConfigCMS((string)$data['routingConfigCMS']);
            $selectedShopMongo->setRoutingConfigProduct((string)$data['routingConfigProduct']);
            $selectedShopMongo->setRoutingConfigProductGroup((string)$data['routingConfigProductGroup']);
            $documentManager->persist($selectedShopMongo);
            $documentManager->flush();
        }

        $dataRouting = [];
        $formRedirectRouting = $this->createForm(RoutingRedirectType::class, $dataRouting);
        $formRedirectRouting->handleRequest($request);
        if ($formRedirectRouting->isSubmitted() && $formRedirectRouting->isValid()) {
            $dataRouting = $formRedirectRouting->getData();
            $routingService->createRoute($selectedShop, Routing::$MODULE_LAGACY, Routing::$CONTROLLER_REDIRECT, Routing::$ACTION_INDEX, $dataRouting['url'], md5($dataRouting['url']), 'de', ['target' => $dataRouting['target'], 'mode' => $dataRouting['mode']], false);
        }

        $routes = $documentManager
        ->getRepository('PSC\Shop\SettingsBundle\Document\Route')
        ->findBy(array('shop' => (string)$selectedShop->getUuid()));

        return ['routes' => $routes, 'form' => $form->createView(), 'formRedirectRouting' => $formRedirectRouting->createView()];
    }

    /**
     * Default Seite
     *
     * @Route("/routing/delete/{uuid}", name="psc_backend_shop_routing_delete")
     * @Security("is_granted('ROLE_SHOP')")
     *
     */
    public function deleteAction(string  $uuid, Routing $routingService): RedirectResponse
    {
        $routingService->deleteRoute($uuid);

        return $this->redirectToRoute('psc_backend_shop_routing_index');
    }

    /**
     * Default Seite
     *
     * @Route("/routing/regenerate/{type}", name="psc_backend_shop_routing_regenerate")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     */
    public function regenerateAction(int $type = 1, \Twig\Environment $template, Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, Routing $routingService, EntityManagerInterface $entityManager): RedirectResponse
    {

        $selectedShop = $shopService->getSelectedShop();
        $selectedShopMongo = $shopService->getMongoSelectedShop();
        if ($type === Routing::$CONTROLLER_CMS) {
            $pattern = $template->createTemplate($selectedShopMongo->getRoutingConfigCMS());
            $rows = $entityManager->getRepository(Cms::class)->findBy(['shop' => $selectedShop->getUID()]);
            foreach ($rows as $cms) {
                $url = $pattern->render(['cms' => $cms, 'obj' => $cms]);
                $routingService->createRoute($selectedShop, Routing::$MODULE_LAGACY, Routing::$CONTROLLER_CMS, Routing::$ACTION_INDEX, $url, $cms->getUid(), 'de', ['page' => $cms->getUrl()]);
            }
        }

        if ($type === Routing::$CONTROLLER_PRODUCT) {
            $pattern = $template->createTemplate($selectedShopMongo->getRoutingConfigProduct());
            $rows = $entityManager->getRepository(Product::class)->findBy(['shop' => $selectedShop->getUID(), 'enable' => true]);
            foreach ($rows as $product) {
                $url = $pattern->render(['product' => $product, 'obj' => $product]);
                $routingService->createRoute($selectedShop, Routing::$MODULE_LAGACY, Routing::$CONTROLLER_PRODUCT, Routing::$ACTION_SHOW, $url, $product->getUid(), 'de', ['article' => $product->getUrl()]);
            }
        }

        if ($type === Routing::$CONTROLLER_PRODUCTGROUP) {
            $pattern = $template->createTemplate($selectedShopMongo->getRoutingConfigProductGroup());
            $rows = $entityManager->getRepository(Productgroup::class)->findBy(['shop' => $selectedShop->getUID(), 'enable' => true]);
            foreach ($rows as $productgroup) {
                $url = $pattern->render(['productgroup' => $productgroup, 'obj' => $productgroup]);
                $routingService->createRoute($selectedShop, Routing::$MODULE_LAGACY, Routing::$CONTROLLER_PRODUCTGROUP, Routing::$ACTION_SHOW, $url, $productgroup->getUid(), 'de', ['id' => $productgroup->getUrl()]);
            }
        }

        return $this->redirectToRoute('psc_backend_shop_routing_index');
    }

    /**
     * Default Seite
     *
     * @Route("/routing/clear/{type}", name="psc_backend_shop_routing_clear")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     */
    public function clearAction(int $type = 1, \Twig\Environment $template, Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, Routing $routingService, EntityManagerInterface $entityManager): RedirectResponse
    {
        $selectedShop = $shopService->getSelectedShop();
        $selectedShopMongo = $shopService->getMongoSelectedShop();
        if ($type === Routing::$CONTROLLER_CMS) {
            $routingService->clearRoutes($selectedShop, Routing::$MODULE_LAGACY, Routing::$CONTROLLER_CMS, Routing::$ACTION_INDEX);
        }
        if ($type === Routing::$CONTROLLER_PRODUCT) {
            $routingService->clearRoutes($selectedShop, Routing::$MODULE_LAGACY, Routing::$CONTROLLER_PRODUCT, Routing::$ACTION_SHOW);
        }
        if ($type === Routing::$CONTROLLER_PRODUCTGROUP) {
            $routingService->clearRoutes($selectedShop, Routing::$MODULE_LAGACY, Routing::$CONTROLLER_PRODUCTGROUP, Routing::$ACTION_SHOW);
        }

        return $this->redirectToRoute('psc_backend_shop_routing_index');
    }
}
