<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\SettingsBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use GuzzleHttp\Client;
use MongoDB\BSON\ObjectId;
use PhpParser\Comment\Doc;
use PSC\Shop\EntityBundle\Document\Country;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\SettingsBundle\Form\Backend\CountryType;
use PSC\Shop\SettingsBundle\Form\Backend\ShopSettingsType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\TemplateVars;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * ShopController Backend
 *
 * @package    PSC\Shop\SettingBundle
 * @subpackage Controller
 */
class CountryController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/country/index", name="psc_backend_shop_settings_country_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $country = new Country();
        $country->setShop($selectedShop->getUid());
        $form = $this->createForm(CountryType::class, $country);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($country);
            $documentManager->flush();
        }

        $countrys = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Country')
            ->findBy(array('shop' => (string)$selectedShop->getUid()));
        return [
            'form' => $form->createView(),
            'countrys' => $countrys
        ];
    }

    /**
     * Delete Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/country/delete/{id}", name="psc_backend_shop_settings_country_delete")
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @param $id
     * @return array|JsonResponse|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager, $id)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Country $country */
        $country = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Country')
            ->findOneBy(array('shop' => (string)$selectedShop->getUid(), 'id' => new ObjectId($id)));
        $documentManager->remove($country);
        $documentManager->flush();
        return $this->redirectToRoute('psc_backend_shop_settings_country_index');
    }

    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/country/toggle/{field}/{id}", name="psc_backend_shop_settings_country_toogle")
     *
     * @return array|JsonResponse
     */
    public function toggleAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager, $field, $id)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Country $country */
        $country = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Country')
            ->findOneBy(array('shop' => (string)$selectedShop->getUid(), 'id' => new ObjectId($id)));
        if ($field == 'withTaxWithoutUstNr') {
            $country->setWithTaxWithoutUstNr(!$country->isWithTaxWithoutUstNr());
        }
        if ($field == 'withTaxWithUstNr') {
            $country->setWithTaxWithUstNr(!$country->isWithTaxWithUstNr());
        }

        $documentManager->persist($country);
        $documentManager->flush();
        return new JsonResponse([]);
    }
}
