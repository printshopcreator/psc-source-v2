<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\SettingsBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\SettingsBundle\Document\History as PSCHistory;
use PSC\Shop\SettingsBundle\Form\Backend\ShopSettingsType;
use PSC\Shop\SettingsBundle\PSCShopSettingsBundle;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use PSC\System\SettingsBundle\Service\TemplateVars;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * ShopController Backend
 *
 * @package    PSC\Shop\SettingBundle
 * @subpackage Controller
 */
class SettingsController extends AbstractController
{
    private History $historyService;
    public function __construct(History $historyService)
    {
        $this->historyService = $historyService;
    }


    /**
     * Default Seite
     *
     * @Route("/settings/index", name="psc_backend_shop_settings_index")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param Request $request
     * @param Field $fieldService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param DocumentManager $documentManager
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(
        Request $request,
        Field $fieldService,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        SessionInterface $session,
        DocumentManager $documentManager,
        Log $logService
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shop);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Shop);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$selectedShop->getUid()));
        if ($shop === null) {
            $shop = new Shop();
            $shop->setUid($selectedShop->getUid());
        }

        $shop->basketField1 = $selectedShop->getBasketfield1();
        $shop->basketField2 = $selectedShop->getBasketfield2();
        $shop->basketPosField1 = $selectedShop->getBasketposfield1();
        $shop->basketPosField2 = $selectedShop->getBasketposfield2();
        $shop->productSort = $selectedShop->getProductSort();
        $shop->productSortDir = $selectedShop->getProductSortDir();
        $shop->title = $selectedShop->getTitle();
        $shop->subTitle = $selectedShop->getSubTitle();
        $shop->keywords = $selectedShop->getKeywords();
        $shop->description = $selectedShop->getDescription();
        $shop->deleted = $selectedShop->isDeleted();
        $shop->copyright = $selectedShop->getCopyright();
        $shop->author = $selectedShop->getAuthor();
        $shop->private = $selectedShop->isPrivate();
        $shop->registration = $selectedShop->isRegistration();
        $shop->noVerify = $selectedShop->isNoVerify();
        $shop->betreiberCompany = $selectedShop->getBetreiberCompany();
        $shop->betreiberName = $selectedShop->getBetreiberName();
        $shop->betreiberStreet = $selectedShop->getBetreiberStreet();
        $shop->betreiberAddress = $selectedShop->getBetreiberAddress();
        $shop->betreiberEmail = $selectedShop->getBetreiberEmail();
        $shop->betreiberTel = $selectedShop->getBetreiberTel();
        $shop->betreiberUid = $selectedShop->getBetreiberUid();
        $shop->betreiberHid = $selectedShop->getBetreiberHid();
        $shop->betreiberSid = $selectedShop->getBetreiberSid();
        $shop->betreiberRegister = $selectedShop->getBetreiberRegister();
        $shop->betreiberVb = $selectedShop->getBetreiberVb();
        $shop->betreiberRechtsform = $selectedShop->getBetreiberRechtsform();
        $shop->betreiberWeb = $selectedShop->getBetreiberWeb();
        $shop->redirectLogin = $selectedShop->getRedirectLogin();
        $shop->redirectLogout = $selectedShop->getRedirectLogout();
        $shop->logo1 = $selectedShop->getLogo1();
        $shop->logo2 = $selectedShop->getLogo2();
        $shop->sitemap = $selectedShop->getSitemap();
        $shop->robots = $selectedShop->getRobots();
        if ($selectedShop->getApiKey() != "" && $shop->getApiKey() == "") {
            $shop->setApiKey($selectedShop->getApiKey());
        }

        $form = $this->createForm(ShopSettingsType::class, $shop);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if (intval($shop->getUid()) != $selectedShop->getId()) {
                $session->getFlashBag()->add('danger', 'Shop has not been updated! (Saved data does not match selected shop)');
                return $this->redirectToRoute('psc_backend_shop_settings_index');
            }

            $selectedShop->setBasketfield1($shop->basketField1);
            $selectedShop->setBasketfield2($shop->basketField2);
            $selectedShop->setBasketposfield1($shop->basketPosField1);
            $selectedShop->setBasketposfield2($shop->basketPosField2);
            $selectedShop->setProductSort($shop->productSort);
            $selectedShop->setProductSortDir($shop->productSortDir);
            $selectedShop->setDeleted($shop->deleted);
            $selectedShop->setLogo1($shop->logo1);
            $selectedShop->setLogo2($shop->logo2);
            $selectedShop->setSitemap($shop->sitemap);
            $selectedShop->setRobots($shop->robots);
            $selectedShop->setTitle($shop->title);
            $selectedShop->setSubTitle($shop->subTitle);
            $selectedShop->setKeywords($shop->keywords);
            $selectedShop->setDescription($shop->description);
            $selectedShop->setCopyright($shop->copyright);
            $selectedShop->setAuthor($shop->author);
            $selectedShop->setPrivate($shop->private);
            $selectedShop->setRegistration($shop->registration);
            $selectedShop->setNoVerify($shop->noVerify);
            $selectedShop->setBetreiberCompany($shop->betreiberCompany);
            $selectedShop->setBetreiberName($shop->betreiberName);
            $selectedShop->setBetreiberStreet($shop->betreiberStreet);
            $selectedShop->setBetreiberAddress($shop->betreiberAddress);
            $selectedShop->setBetreiberEmail($shop->betreiberEmail);
            $selectedShop->setBetreiberTel($shop->betreiberTel);
            $selectedShop->setBetreiberUid($shop->betreiberUid);
            $selectedShop->setBetreiberHid($shop->betreiberHid);
            $selectedShop->setBetreiberSid($shop->betreiberSid);
            $selectedShop->setBetreiberRegister($shop->betreiberRegister);
            $selectedShop->setBetreiberVb($shop->betreiberVb);
            $selectedShop->setBetreiberRechtsform($shop->betreiberRechtsform);
            $selectedShop->setBetreiberWeb($shop->betreiberWeb);
            $selectedShop->setRedirectLogin($shop->redirectLogin);
            $selectedShop->setRedirectLogout($shop->redirectLogout);
            if ($selectedShop->isDeleted()) {
                $request->getSession()->set('displayDeletedShop', true);
            }

            $this->historyService->createHistoryEntry(new PSCHistory((string)$selectedShop->getId()), $selectedShop, $shop);
            $documentManager->persist($shop);
            $documentManager->flush();
            $entityManager->persist($selectedShop);
            $entityManager->flush();
            $logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopSettingsBundle::class, $selectedShop->getUuid(), "Shopsettings saved");
            if ($selectedShop->isDeleted()) {
                return $this->redirectToRoute('psc_backend_shop_settings_index');
            }
        }



        return array(
            'form' => $form->createView(),
            'shop' => $selectedShop,
            'help' => TemplateVars::getHelp(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$selectedShop->getId())
        );
    }

    /**
     * Renew ApiKey
     *
     * @Route("/settings/renewapikey", name="psc_backend_shop_settings_renewapikey")
     * @Template()
     * @Security("is_granted('ROLE_SHOP')")
     *
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @param EntityManagerInterface $entityManager
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function renewApiKeyAction(\PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager, EntityManagerInterface $entityManager)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$selectedShop->getUid()));
        $selectedShop->setApiKey(Uuid::uuid4());
        if ($shop === null) {
            $shop = new Shop();
            $shop->setUid($selectedShop->getUid());
        }
        $shop->setApiKey($selectedShop->getApiKey());
        $entityManager->persist($selectedShop);
        $entityManager->flush();
        $documentManager->persist($shop);
        $documentManager->flush();
        return $this->redirectToRoute('psc_backend_shop_settings_index');
    }
}
