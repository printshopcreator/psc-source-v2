<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\SettingsBundle\Controller\Json;

use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\Util\Path;
use Imagine\Image\Box;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Document\Motiv as DocumentMotiv;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * ShopController Json
 *
 * @package    PSC\Shop\SettingBundle
 * @subpackage Controller
 */
class ShopController extends AbstractController
{
    /**
     * Get Logo2
     *
     * @Route("/shop/settings/json/{shop_uuid}", name="shop_settings_json")
     *
     * @return void
     */
    public function getSettingsAction(EntityManagerInterface $entityManager, $shop_uuid)
    {

        $shop = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findOneByUuid($shop_uuid);
        $data = array(
            'success' => true,
            'logo2' => false,
            'googleanalyticscode' => $shop->getGoogleanalyticscode()
        );
        if ($shop->getLogo2() != "") {
            $image = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Image')->findOneBy(['uid' => $shop->getLogo2()]);
            if ($image) {
                $data['logo2'] = $image->getPath();
            }
        }

        $json = new JsonResponse($data);
        $json->headers->set('Access-Control-Allow-Origin', '*');
        $json->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        return $json;
    }
}
