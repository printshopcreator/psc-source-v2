<?php

namespace PSC\Shop\SettingsBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use PSC\Shop\SettingsBundle\Event\MenuEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ConfigureMenuListener
{
    protected $eventDispatcher;
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;

    /**
     * @param FactoryInterface @factory
     */
    public function __construct($eventDispatcher, $authorizationChecker)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $factory = $event->getFactory();

            $submenu = $menu->addChild('settings', array(
                'uri' => '#',
                'label' => 'Shop Einstellungen',
                'extras' => array(
                    'icon' => 'fas fa-wrench',
                    'orderNumber' => 1000
                )
            ));

            $submenu->addChild(
                'general',
                array(
                    'route' => 'psc_backend_shop_settings_index',
                    'label' => 'Allgemeines',
                    'extras' => array(
                        'icon' => 'fas fa-list'
                    )
                )
            );

            $submenu->addChild(
                'countrys',
                array(
                    'route' => 'psc_backend_shop_settings_country_index',
                    'label' => 'Länder',
                    'extras' => array(
                        'icon' => 'fas fa-globe-europe'
                    )
                )
            );

            $submenuLayouter = $menu->addChild('settingsLayouter', array(
                'uri' => '#',
                'label' => 'Layouter',
                'extras' => array(
                    'icon' => 'fas fa-wrench',
                    'orderNumber' => 2001
                )
            ));


            $submenu->addChild('Routing', array(
                'route' => 'psc_backend_shop_routing_index',
                'extras' => array(
                    'icon' => 'fas fa-layer-group'
                )
            ));

            $this->eventDispatcher->dispatch(new ConfigureMenuEvent($factory, $submenu), MenuEvents::CONFIGURE_MAIN);
            $this->eventDispatcher->dispatch(new ConfigureMenuEvent($factory, $submenuLayouter), MenuEvents::CONFIGURE_LAYOUTER);
        }
    }
}
