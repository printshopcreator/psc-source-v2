<?php

namespace PSC\Shop\SettingsBundle\Document;

use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="shopObj_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $shopObj;

    function __construct(string $shopObj = "")
    {
        parent::__construct();
        $this->shopObj = $shopObj;
    }

    public function getShopObj()
    {
        return $this->shopObj;
    }

    public function setShopObj(string $shopObj)
    {
        $this->shopObj = $shopObj;
    }

    public function getReferenceField(): string
    {
        return 'shopObj';
    }
}
