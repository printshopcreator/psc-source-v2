<?php

namespace PSC\Shop\SettingsBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;

/**
 * @Document
 */
class Route
{
    /**
     * @Id
     */
    protected $id;
/**
     * @var string $shop
     *
     * @Field(type="string")
     */
    protected $shop;
/**
     * @var \DateTime $created;
     *
     * @Field(type="date")
     */
    protected $created;
/**
     * @var \DateTime $updated;
     *
     * @Field(type="date")
     */
    protected $updated;
/**
     * @var string $referenceUuid
     *
     * @Field(type="string")
     */
    protected $referenceUuid;
/**
     * @var bool $crudSave
     *
     * @Field(type="bool")
     */
    protected $crudSave;
/**
     * @var integer $module
     *
     * @Field(type="int")
     */
    protected $module;
/**
     * @var string $lang
     *
     * @Field(type="string")
     */
    protected $lang;
/**
     * @var integer $controller
     *
     * @Field(type="int")
     */
    protected $controller;
/**
     * @var integer $action
     *
     * @Field(type="int")
     */
    protected $action;
/**
     * @var string $url
     *
     * @Field(type="string")
     */
    protected $url;
/**
     * @var array $parameter
     *
     * @Field(type="hash")
     */
    protected $parameter;
    public function __construct()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param string $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getReferenceUuid()
    {
        return $this->referenceUuid;
    }

    /**
     * @param string $referenceUuid
     */
    public function setReferenceUuid($referenceUuid)
    {
        $this->referenceUuid = $referenceUuid;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return array
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @param array $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * @return bool
     */
    public function isCrudSave()
    {
        return $this->crudSave;
    }

    /**
     * @param bool $crudSave
     */
    public function setCrudSave($crudSave)
    {
        $this->crudSave = $crudSave;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return int
     */
    public function getModule()
    {
        return $this->module;
    }

    /**
     * @param int $module
     */
    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @return int
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @param int $controller
     */
    public function setController($controller)
    {
        $this->controller = $controller;
    }

    /**
     * @return int
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param int $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }
}
