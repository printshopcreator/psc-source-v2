<?php

namespace PSC\Shop\SettingsBundle\Mailer\Transport;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Document\Instance;
use PSC\System\SettingsBundle\Service\Shop;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\Exception\UnsupportedSchemeException;
use Symfony\Component\Mailer\Transport\AbstractTransportFactory;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mailer\Transport\Smtp\EsmtpTransport;
use Symfony\Component\Mailer\Transport\Smtp\Stream\SocketStream;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class PSCTransportFactory extends AbstractTransportFactory
{
    /**
     * @var DocumentManager
     */
    private DocumentManager $documentManager;
    /**
     * @var Shop
     */
    private Shop $shopService;

    public function __construct(EventDispatcherInterface $dispatcher = null, HttpClientInterface $client = null, LoggerInterface $logger = null, DocumentManager $dm, Shop $shopService)
    {
        parent::__construct($dispatcher, $client, $logger);
        $this->documentManager = $dm;
        $this->shopService = $shopService;
    }

    public function create(Dsn $dsn): TransportInterface
    {

        $instance = $this->documentManager
            ->getRepository('PSCEntityBundle:Instance')
            ->findOneBy(['appId' => '1']);

        if ($instance === null) {
            $instance = new Instance();
            $instance->setAppId("1");
        }

        if ($instance->isSmtpOwn()) {
            $tls = $instance->isSmtpTls() ? true : null;
            $transport = new EsmtpTransport($instance->getSmtpHost(), $instance->getSmtpPort() ? $instance->getSmtpPort() : 587, $tls, $this->dispatcher, $this->logger);

            $transport->setUsername($instance->getSmtpUsername());
            $transport->setPassword($instance->getSmtpPassword());

            return $transport;
        }

        $tls = 'smtps' === $dsn->getScheme() ? true : null;
        $port = $dsn->getPort(0);
        $host = $dsn->getHost();

        $transport = new EsmtpTransport($host, $port, $tls, $this->dispatcher, $this->logger);

        if ('' !== $dsn->getOption('verify_peer') && !filter_var($dsn->getOption('verify_peer', true), \FILTER_VALIDATE_BOOLEAN)) {
            /** @var SocketStream $stream */
            $stream = $transport->getStream();
            $streamOptions = $stream->getStreamOptions();

            $streamOptions['ssl']['verify_peer'] = false;
            $streamOptions['ssl']['verify_peer_name'] = false;

            $stream->setStreamOptions($streamOptions);
        }

        if ($user = $dsn->getUser()) {
            $transport->setUsername($user);
        }

        if ($password = $dsn->getPassword()) {
            $transport->setPassword($password);
        }

        if (null !== ($localDomain = $dsn->getOption('local_domain'))) {
            $transport->setLocalDomain($localDomain);
        }

        if (null !== ($restartThreshold = $dsn->getOption('restart_threshold'))) {
            $transport->setRestartThreshold((int) $restartThreshold, (int) $dsn->getOption('restart_threshold_sleep', 0));
        }

        if (null !== ($pingThreshold = $dsn->getOption('ping_threshold'))) {
            $transport->setPingThreshold((int) $pingThreshold);
        }

        return $transport;
    }

    protected function getSupportedSchemes(): array
    {
        return ['psc'];
    }
}
