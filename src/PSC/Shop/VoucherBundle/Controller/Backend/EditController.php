<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\VoucherBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\VoucherBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Service\History;
use Port\Csv\CsvWriter;
use Port\Reader\ArrayReader;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Voucher;
use PSC\Shop\EntityBundle\Entity\VoucherItem;
use PSC\Shop\NewsBundle\Form\Backend\DeleteType;
use PSC\Shop\PaymentBundle\Form\Backend\PaymentType;
use PSC\Shop\VoucherBundle\Form\Backend\VoucherType;
use PSC\System\PluginBundle\Form\Chain\Field;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * EditController
 *
 * @package    PSC\Shop\Voucher
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    private History $historyService;
    public function __construct(History $historyService)
    {
        $this->historyService = $historyService;
    }

    /**
     * Create
     *
     * @Route("/edit/create", name="psc_shop_voucher_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param SessionInterface $session
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(Request $request, Field $fieldService, SessionInterface $session, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager)
    {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Voucher);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Voucher);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $voucher = new Voucher();
        $voucher->setShop($selectedShop);
        $form = $this->createForm(VoucherType::class, $voucher);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($voucher);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'Voucher \'' . $voucher->getTitle() . '\' has been created!');
            return $this->redirectToRoute('psc_shop_voucher_backend_list');
        }

        return array(
            'voucher' => $voucher,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups
        );
    }

    /**
     * edit
     *
     * @Route("/edit/edit/{uid}", name="psc_shop_voucher_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param SessionInterface $session
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(Request $request, Field $fieldService, SessionInterface $session, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uid)
    {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Voucher);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Voucher);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Voucher $voucher */
        $voucher = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Voucher')->findOneBy(array('uid' => $uid, 'shop' => $selectedShop));
        $form = $this->createForm(VoucherType::class, $voucher);
        $voucher->setUpdatedAt(new \DateTime());
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->historyService->createHistoryEntry(new PSCHistory((string)$voucher->getUid()), $voucher);
            $entityManager->persist($voucher);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'Voucher \'' . $voucher->getTitle() . '\' has been updated!');
            return $this->redirectToRoute('psc_shop_voucher_backend_list');
        }

        return array(
            'voucher' => $voucher,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$voucher->getUid())
        );
    }

    /**
     * remove
     *
     * @Route("/edit/generate/{uid}", name="psc_shop_voucher_backend_generate")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function generateAction(Request $request, SessionInterface $session, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, $uid)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Voucher $voucher */
        $voucher = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Voucher')->findOneBy(array('uid' => $uid, 'shop' => $selectedShop));
        $this->generateVoucherCodes($entityManager, $selectedShop, $voucher);
        $entityManager->flush();
        $session->getFlashBag()->add('success', 'Voucher \'' . $voucher->getTitle() . '\' has been generated!');
        return $this->redirectToRoute('psc_shop_voucher_backend_list');
    }

    private function uuidCredit($serverID = 1)
    {

        $t = explode(" ", microtime());
        return sprintf(
            '%04x%04x',
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }

    private function generateVoucherCodes(EntityManagerInterface $entityManager, Shop $shop, Voucher $voucher)
    {
        for ($i = 1; $i <= $voucher->getCount(); $i++) {
            $credit = new VoucherItem();
            $credit->setVoucher($voucher);
            $credit->setShop($shop);
            if ($voucher->isMore() == true && $voucher->getCode() != "") {
                $credit->setCode($voucher->getCode());
            } else {
                $credit->setCode($voucher->getCode() . $this->uuidCredit());
            }
            $credit->setUsed(false);
            $entityManager->persist($credit);
        }
    }

    /**
     * remove
     *
     * @Route("/edit/delete/{uid}", name="psc_shop_voucher_backend_delete")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, SessionInterface $session, EntityManagerInterface $entityManager, $uid)
    {
        $voucher = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Voucher')->findOneBy(['uid' => $uid]);
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $orgTitle = $voucher->getTitle();
                $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\VoucherItem')->deleteAllByVoucher($voucher);
                $entityManager->remove($voucher);
                $entityManager->flush();
                $session->getFlashBag()->add('success', 'Voucher \'' . $orgTitle . '\' has been deleted!');
                return $this->redirectToRoute('psc_shop_voucher_backend_list');
            }
            return $this->redirectToRoute('psc_shop_voucher_backend_list');
        }


        return array(
            'voucher' => $voucher,
            'form' => $form->createView()
        );
    }

    /**
     * remove
     *
     * @Route("/edit/export/{uid}", name="psc_shop_voucher_backend_export")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return StreamedResponse
     */
    public function exportAction(Request $request, EntityManagerInterface $entityManager, $uid)
    {
        /** @var Voucher $voucher */
        $voucher = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Voucher')->findOneBy(['uid' => $uid]);
        $temp = array();
        $temp[] = array(
            'code' => 'code',
            'used' => 'used',
            'contact' => 'contact',
        );
/** @var VoucherItem $item */
        foreach ($voucher->getVoucherItems() as $item) {
            $temp[] = ['code' => $item->getCode(), 'used' => $item->isUsed(), 'contact' => $item->getContact()];
        }

        $reader = new ArrayReader($temp);
        $writer = new CsvWriter();
        $writer->setStream(fopen('php://output', 'w'));
        foreach ($reader as $row) {
            $writer->writeItem($row);
        }

        $response = new StreamedResponse();
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment;filename="export.csv"');
        $response->setCallback(function () use ($writer) {

            $writer->finish();
        });
        $response->send();
        return $response;
    }
}
