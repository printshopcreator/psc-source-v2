<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\VoucherBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class VoucherType extends AbstractType
{
    /** @var Field  */
    protected $fields;
    public function __construct(Field $fields)
    {
        $this->fields = $fields;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, ['label' => 'Name', 'required' => false])
            ->add('enable', CheckboxType::class, ['label' => 'active', 'required' => false])
            ->add('fromDate', DateType::class, ['label' => 'from', 'required' => false])
            ->add('toDate', DateType::class, ['label' => 'to', 'required' => false])
            ->add('percent', CheckboxType::class, ['label' => 'Percent', 'required' => false])
            ->add('more', CheckboxType::class, ['label' => 'notindividualcodes', 'required' => false])
            ->add('value', NumberType::class, ['label' => 'Value', 'required' => false])
            ->add('count', NumberType::class, ['label' => 'Count', 'required' => false])
            ->add('code', TextType::class, ['label' => 'Code', 'required' => false])
            ->add('mode', ChoiceType::class, array(
                'label' => 'Modus',
                'choices' => [
                    'Totalshoppingcart' => 1,
                    'Calculationvalue' => 2
                ],
                'required' => true,
            ))
            ->add('productIds', TextType::class, ['label' => 'ProductID', 'required' => false])
            ->add('productGroupIds', TextType::class, ['label' => 'ProductgroupID', 'required' => false])
            ;
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Voucher) as $field) {
            $field->buildForm($builder, $options);
        }
        $builder->add('save', SubmitType::class, array('label' => 'save'));
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            $data = $event->getData();
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Voucher) as $field) {
                $field->formPreSubmit($data);
            }
            $event->setData($data);
        });
    }

    public function getName()
    {
        return 'voucher';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Voucher',
            'translation_domain' => 'core_voucher_edit'
        ));
    }
}
