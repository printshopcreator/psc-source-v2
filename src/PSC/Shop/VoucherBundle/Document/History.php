<?php

namespace PSC\Shop\VoucherBundle\Document;

use PSC\Shop\EntityBundle\Entity\Voucher;
use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="voucher_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $voucher;

    function __construct(string $voucher = "")
    {
        parent::__construct();
        $this->voucher = $voucher;
    }

    public function getVoucher()
    {
        return $this->voucher;
    }

    public function setVoucher(string $voucher)
    {
        $this->voucher = $voucher;
    }

    public function getReferenceField(): string
    {
        return 'voucher';
    }
}
