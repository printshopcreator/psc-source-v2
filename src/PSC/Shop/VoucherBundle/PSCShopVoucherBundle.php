<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\VoucherBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * PSCShopVoucherBundle
 *
 * @package PSC\Shop\Voucher
 */
class PSCShopVoucherBundle extends Bundle
{
}
