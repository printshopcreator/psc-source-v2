<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\UserBundle\Security\Authentication\Provider;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\UserBundle\Security\Authentication\Token\pscAppToken;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Security Factory
 *
 * @package    BS\Basic\AuthBundle
 * @subpackage Provider
 *
 */
class pscAppProvider implements AuthenticationProviderInterface
{
    /**
     * Provider
     *
     * @var \Symfony\Component\Security\Core\User\UserProviderInterface
     */
    private $userProvider;
    private $entityManager;
/**
     * Constructor
     *
     * @param UserProviderInterface $userProvider Userprovider
     */
    public function __construct(UserProviderInterface $userProvider, EntityManagerInterface $entityManager)
    {
        $this->userProvider = $userProvider;
        $this->entityManager = $entityManager;
    }

    /**
     * Authenticate
     *
     * @param TokenInterface $token Token
     *
     * @return pscAppToken|TokenInterface
     * @throws \Symfony\Component\Security\Core\Exception\AuthenticationException
     */
    public function authenticate(TokenInterface $token)
    {
        if ($token->getUser()->getId() != 0) {
            $user = $this->userProvider->loadUserByUId($token->getUser()->getId());
            if ($user) {
                $authenticatedToken = new pscAppToken($user->getRolesAsString());
                $authenticatedToken->setUser($user);
                return $authenticatedToken;
            }
        } else {
            $user = $this->userProvider->loadUserByUsername($token->getUsername());
            if ($user) {
                $authenticatedToken = new pscAppToken($user->getRolesAsString());
                $authenticatedToken->setUser($user);
                return $authenticatedToken;
            }
        }

        throw new AuthenticationException('The PSC authentication failed.');
    }

    /**
     * Supports
     *
     * @param TokenInterface $token Token
     *
     * @return bool
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof pscAppToken;
    }
}
