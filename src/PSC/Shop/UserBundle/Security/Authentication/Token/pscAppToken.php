<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\UserBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Guard\Token\GuardTokenInterface;

/**
 * Security Factory
 *
 * @package    PSC\Shop\UserBundle
 * @subpackage Token
 *
 */
class pscAppToken extends AbstractToken
{
    /**
     * Erzeugt
     *
     * @var date
     */
    public $created;
/**
     * Digist
     *
     * @var string
     */
    public $digest;
/**
     * Nonce
     *
     * @var string
     */
    public $nonce;
    public $selectedShop;
/**
     * Constructor
     *
     * @param array $roles Roles
     */
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);
        $this->setAuthenticated(count($roles) > 0);
    }

    public function getKey()
    {
        return 'anonymous';
    }

    /**
     * Credentials
     *
     * @return mixed|string
     */
    public function getCredentials()
    {
        return '';
    }

    /**
     * @return mixed
     */
    public function getSelectedShop()
    {
        return $this->selectedShop;
    }

    /**
     * @param mixed $selectedShop
     */
    public function setSelectedShop($selectedShop)
    {
        $this->selectedShop = $selectedShop;
    }
}
