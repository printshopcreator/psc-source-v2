<?php

namespace PSC\Shop\UserBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\System\SettingsBundle\Service\Instance;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class ApiKeyAuthenticator extends AbstractGuardAuthenticator
{
    private $instance;
    /**
     * @var EntityManagerInterface
     */
    private EntityManagerInterface $entityManager;

    public function __construct(Instance $instance, EntityManagerInterface $entityManager)
    {
        $this->instance = $instance;
        $this->entityManager = $entityManager;
    }

    /**
     * Called on every request to decide if this authenticator should be
     * used for the request. Returning `false` will cause this authenticator
     * to be skipped.
     */
    public function supports(Request $request)
    {
        return $request->headers->has('apiKey');
    }

    /**
     * Called on every request. Return whatever credentials you want to
     * be passed to getUser() as $credentials.
     */
    public function getCredentials(Request $request)
    {
        return $request->headers->get('apiKey');
    }

    public function getUser($credentials, UserProviderInterface $userProvider)
    {

        if (null === $credentials) {
            // The token header was empty, authentication fails with HTTP Status
            // Code 401 "Unauthorized"
            return null;
        }

        /** @var Shop $shop */
        $shop = $this->entityManager->getRepository(Shop::class)->findOneBy(['apiKey' => $credentials]);

        if ($shop) {
            $user = new User("api", $shop->getApiKey(), ['ROLE_API']);
            return $user;
        }

        $instance = $this->instance->getInstance();

        if ($instance->getSupporttoken() != "") {
            $user = new User("api", $instance->getSupporttoken(), ['ROLE_API']);
            return $user;
        }

        return null;
    }

    public function checkCredentials($credentials, UserInterface $user)
    {

        return $user->getPassword() == $credentials;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        // on success, let the request continue
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            // you may want to customize or obfuscate the message first
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())

            // or to translate this message
            // $this->translator->trans($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Called when authentication is needed, but it's not sent
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            // you might translate this message
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function supportsRememberMe()
    {
        return false;
    }
}
