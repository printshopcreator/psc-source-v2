<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\UserBundle\Security\Firewall;

use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\UserBundle\Security\Authentication\Token\pscAppToken;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;

/**
 * Security Factory
 *
 * @package    BS\Basic\AuthBundle
 * @subpackage Firewall
 *
 */
class pscAppListener
{
    /**
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    protected $securityContext;
/**
     * @var \Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface
     */
    protected $authManager;
/**
     * Constructor
     *
     * @param TokenStorageInterface       $securityContext
     * @param AuthenticationManagerInterface $authManager
     */
    public function __construct(TokenStorageInterface $securityContext, AuthenticationManagerInterface $authManager)
    {
        $this->securityContext = $securityContext;
        $this->authManager = $authManager;
    }

    public function __invoke(RequestEvent $event)
    {

        $event->getRequest();
        try {
            if (!isset($_SESSION) || !isset($_SESSION['Zend_Auth']) || !isset($_SESSION['Zend_Auth']['storage'])) {
                $token = new AnonymousToken("anonymus", new Contact("anonymus"), array("IS_AUTHENTICATED_ANONYMOUSLY"));
                $this->securityContext->setToken($token);
                return;
            }

            $token = new pscAppToken(array("IS_AUTHENTICATED_ANONYMOUSLY"));
            $contact = new Contact();
            $contact->setEmail($_SESSION['Zend_Auth']['storage']['self_email']);
            $contact->setId($_SESSION['Zend_Auth']['storage']['id']);
            $token->setUser($contact);
            $authToken = $this->authManager->authenticate($token);
            $this->securityContext->setToken($authToken);
            return;
        } catch (AuthenticationException $failed) {
            $token = new pscAppToken(array("IS_AUTHENTICATED_ANONYMOUSLY"));
            $this->securityContext->setToken($token);
            return;
        }
    }
}
