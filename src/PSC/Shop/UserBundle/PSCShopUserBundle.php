<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\UserBundle;

use PSC\Shop\UserBundle\DependencyInjection\Security\Factory\pscAppFactory;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * UserBundle
 *
 * @package PSC\Shop\User
 */
class PSCShopUserBundle extends Bundle
{
    /**
     * Bundle erzeugen
     * Inject der eigenen AuthFactoryClass
     *
     * @param ContainerBuilder $container Container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new pscAppFactory());
    }
}
