<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\UserBundle\DependencyInjection\Security\Factory;

use PSC\Shop\UserBundle\Security\Authentication\Provider\pscAppProvider;
use PSC\Shop\UserBundle\Security\Firewall\pscAppListener;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;

/**
 * Security Factory
 *
 * @package    PSC\Shop\UserBundle
 * @subpackage Factory
 *
 */
class pscAppFactory implements SecurityFactoryInterface
{
    /**
     * Zentrale Steuerung und erzeugen des AuthHandlings
     *
     * @param ContainerBuilder $container         Container
     * @param string           $uid               Id
     * @param mixin            $config            Config
     * @param Provider         $userProvider      Userprovider
     * @param EntryPoint       $defaultEntryPoint Entrypoint
     *
     * @return array
     */
    public function create(ContainerBuilder $container, $uid, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.psc.' . $uid;
        $container
            ->setDefinition($providerId, new ChildDefinition(pscAppProvider::class))
            ->setArgument(0, new Reference($userProvider));
        $listenerId = 'security.authentication.listener.psc.' . $uid;
        $container->setDefinition($listenerId, new ChildDefinition(pscAppListener::class));
        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * Position im Auth
     *
     * @return string
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * Key der Auth Verweiß in security.yml
     *
     * @return string
     */
    public function getKey()
    {
        return 'psc';
    }

    public function addConfiguration(NodeDefinition $node)
    {
    }
}
