<?php

namespace PSC\Shop\BasketBundle\Event\Legacy;

use PSC\Shop\BasketBundle\Dto\Legacy\Input;
use Symfony\Contracts\EventDispatcher\Event;

class Add extends Event
{
    public const NAME = 'basket.legacy.add';

    private Input $data;

    public function __construct(Input $data)
    {
        $this->data = $data;
    }

    public function getData(): Input
    {
        return $this->data;
    }
}
