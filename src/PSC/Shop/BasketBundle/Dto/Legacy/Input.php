<?php

namespace PSC\Shop\BasketBundle\Dto\Legacy;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @OA\Property(type="integer")
     */
    public int $count = 1;

    /**
     * @OA\Property(type="string")
     */
    public string $productUUId = "";

    /**
     * @OA\Property(type="string")
     */
    public string $uploadMode = "none";

    public array $values = [];
}
