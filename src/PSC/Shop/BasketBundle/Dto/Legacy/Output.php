<?php

namespace PSC\Shop\BasketBundle\Dto\Legacy;

class Output
{
    public bool $success = true;

    public string $basketUUId = "";
}
