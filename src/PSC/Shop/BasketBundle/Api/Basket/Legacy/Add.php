<?php

namespace PSC\Shop\BasketBundle\Api\Basket\Legacy;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\BasketBundle\Dto\Legacy\Input;
use PSC\Shop\BasketBundle\Dto\Legacy\Output;
use PSC\Shop\BasketBundle\Event\Legacy\Add as PSCAdd;
use PSC\Shop\EntityBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class Add extends AbstractController
{
    private EventDispatcherInterface $eventDispatcher;
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher)
    {
        $this->entityManager = $entityManager;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * change temp basketitem
     * @deprecated
     *
     * @Route("/legacy/add", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\BasketBundle\Dto\Legacy\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="basket",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\BasketBundle\Dto\Legacy\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\BasketBundle\Dto\Legacy\Input::class))
     * )
     * @OA\Tag(name="Basket")
     */
    public function add(Input $data): JsonResponse
    {
        require_once(__DIR__ . '/../../../../EntityBundle/Lagacy/TP_Basket_Item.php');
        require_once(__DIR__ . '/../../../../EntityBundle/Lagacy/TP_Basket_Collection.php');
        require_once(__DIR__ . '/../../../../EntityBundle/Lagacy/TP_Util.php');

        $product = $this->entityManager->getRepository(Product::class)->findOneBy(['uuid' => $data->productUUId]);

        if ($product) {
            $event = new PSCAdd($data);
            $this->eventDispatcher->dispatch($event, PSCAdd::NAME);

            $articles = $_SESSION['Basket']['Articles'];
            $basketItem = $_SESSION['Basket']['TempProduct'][$data->productUUId];
            $itemId = $articles->addItem($basketItem);

            $output = new Output();
            $output->success = true;
            $output->basketUUId = $itemId;

            $_SESSION['Basket']['TempProduct'][$data->productUUId] = new \TP_Basket_Item();

            return $this->json(new Output());
        }

        return $this->json(new NotFound("order not found"));
    }
}
