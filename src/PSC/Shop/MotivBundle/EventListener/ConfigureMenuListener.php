<?php

namespace PSC\Shop\MotivBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationCheckerInterface  */
    public $authorizationChecker = null;

    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();

            $menu->addChild('Motive', array(
                'route' => 'psc_shop_motive_backend_list_index',
                'extras' => array(
                    'icon' => 'fas fa-image',
                    'orderNumber' => 20
                )
            ));
            $menu->addChild('Motivgruppen', array(
                'route' => 'psc_shop_motive_backend_group_list_index',
                'extras' => array(
                    'icon' => 'fas fa-images',
                    'orderNumber' => 20
                )
            ));
        }
    }
}
