<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MotivBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\Entity;
use Gaufrette\FilesystemMapInterface;
use Knp\Bundle\GaufretteBundle\FilesystemMap;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\MotivBundle\Form\Backend\MotivType;
use PSC\Shop\MotivBundle\Form\Backend\SearchType;
use PSC\Shop\MotivBundle\Form\Backend\SwapType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * MotivController fürs Backend
 *
 * @package    PSC\Shop\Motiv\Backend
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/list/index", name="psc_shop_motive_backend_list_index")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param PaginatorInterface $paginator
     * @param EntityManagerInterface $entityManager
     * @param FilterBuilderUpdaterInterface $filterBuilderUpdater
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, PaginatorInterface $paginator, EntityManagerInterface $entityManager, FilterBuilderUpdaterInterface $filterBuilderUpdater)
    {

        $selectedShop = $shopService->getSelectedShop();

        $form = $this->createForm(SearchType::class, null, ['selectedShop' => $selectedShop]);

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Motiv');
        $qb = $userRepository->createQueryBuilder('motiv')
            ->orderBy('motiv.created', 'DESC');

        if ($request->isMethod('GET')) {
            $form->handleRequest($request);
            // build the query from the given form object
            $filterBuilderUpdater->addFilterConditions($form, $qb);
        }

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            15
        );

        return array(
            'pagination' => $pagination,
            'form' => $form->createView(),
        );
    }

    /**
     * Swap
     *
     * @Route("/list/swap/{uuid}", name="psc_shop_motive_backend_list_swap")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param FilesystemMap $filesystemMap
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \ImagickException
     */
    public function swapAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, FilesystemMap $filesystemMap, $uuid)
    {
        $selectedShop = $shopService->getSelectedShop();

        $motiv = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        $form = $this->createForm(SwapType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->get('file')->getData();

            if ($file instanceof UploadedFile) {
                $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());

                $fileContainer = $filesystemMap->get('steplayouter_motiv');

                $fileContainer->write($filename, file_get_contents($file->getPathname()));

                $im = new \imagick();
                $im->readimageblob($fileContainer->read($filename));
                $res = $im->getImageResolution();
                $res['x'] = intval($res['x']);
                if ($res['x'] != '300') {
                    $res['x'] = 300;
                    $im->setImageResolution(300, 300);
                }

                if ($im->getImageUnits() == 2) {
                    $im->setImageUnits(1);
                    $res['x'] = $res['x'] * 2.54;
                } elseif ($im->getImageUnits() == 0) {
                    $im->setImageUnits(1);
                }
                $im->setimagecompressionquality(100);
                $im->setcompressionquality(100);
                $imgwork = clone $im;
                $fileWorkName = sprintf('%s/%s/%s/work_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileWorkName, $imgwork->getimageblob());
                $imgmid = clone $im;
                $imgmid->thumbnailImage(500, null);
                $fileMidName = sprintf('%s/%s/%s/mid_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileMidName, $imgmid->getimageblob());
                $imgthumb = clone $im;
                $imgthumb->thumbnailImage(null, 110);
                $fileThumbName = sprintf('%s/%s/%s/thumb_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileThumbName, $imgthumb->getimageblob());

                $motiv->setTitle($file->getClientOriginalName());
                $motiv->setDpiOrg($res['x']);
                $motiv->setOrgFilename($filename);

                $motiv->setFileOrginal($fileContainer->get($filename)->getKey());
                $motiv->setFileWork($fileContainer->get($fileWorkName)->getKey());
                $motiv->setFileMid($fileContainer->get($fileMidName)->getKey());
                $motiv->setFileThumb($fileContainer->get($fileThumbName)->getKey());
                $motiv->setWidth($im->getImageWidth());
                $motiv->setHeight($im->getImageHeight());
                $motiv->setMidWidth($imgmid->getImageWidth());
                $motiv->setMidHeight($imgmid->getImageHeight());
                $motiv->setType($im->getFormat());
                $motiv->setExif(json_encode($im->getImageProperties("exif:*")));

                $entityManager->persist($motiv);
                $entityManager->flush();

                if (strpos(mime_content_type("market/motive/" . $motiv->getFileOrginal()), "svg")) {
                    file_put_contents("market/motive/" . $motiv->getFileOrginal(), $motiv->svg);
                }
            }

            return $this->redirectToRoute('psc_shop_motive_backend_list_index');
        }

        return array(
            'form' => $form->createView(),
            'motiv' => $motiv
        );
    }

    /**
     * Default Seite
     *
     * @Route("/list/upload", name="psc_shop_motive_backend_list_upload")
     * @Template()
     *
     * @return View
     */
    public function uploadAction()
    {
        return array();
    }

    /**
     * Default Seite
     *
     * @Route("/list/upload/do", name="psc_shop_motive_backend_list_upload_do")
     *
     * @param Request $request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param FilesystemMapInterface $filesystemMap
     * @return JsonResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \ImagickException
     */
    public function doUploadAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, FilesystemMap $filesystemMap)
    {
        if ($request->files->has("file")) {
            $file = $request->files->get("file");

            if ($file instanceof UploadedFile) {
                $selectedShop = $shopService->getSelectedShop();


                $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());

                $fileContainer = $filesystemMap->get('steplayouter_motiv');

                $fileContainer->write($filename, file_get_contents($file->getPathname()));


                $im = new \imagick();
                $im->readimageblob($fileContainer->read($filename));
                $res = $im->getImageResolution();
                $res['x'] = intval($res['x']);
                if ($res['x'] != '300') {
                    $res['x'] = 300;
                    $im->setImageResolution(300, 300);
                }

                if ($im->getImageUnits() == 2) {
                    $im->setImageUnits(1);
                    $res['x'] = $res['x'] * 2.54;
                } elseif ($im->getImageUnits() == 0) {
                    $im->setImageUnits(1);
                }
                $im->setimagecompressionquality(100);
                $im->setcompressionquality(100);
                $imgwork = clone $im;
                $fileWorkName = sprintf('%s/%s/%s/work_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileWorkName, $imgwork->getimageblob());
                $imgmid = clone $im;
                $imgmid->thumbnailImage(500, null);
                $fileMidName = sprintf('%s/%s/%s/mid_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileMidName, $imgmid->getimageblob());
                $imgthumb = clone $im;
                $imgthumb->thumbnailImage(null, 110);
                $fileThumbName = sprintf('%s/%s/%s/thumb_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileThumbName, $imgthumb->getimageblob());

                $motive = new Motiv();
                $motive->setAccount($this->getUser()->getAccount());
                $motive->setShop($selectedShop);
                $motive->setContact($this->getUser());

                $motive->setInstallId($this->getUser()->getInstallId());
                $motive->setResaleShop(false);
                $motive->setResaleMarket(false);
                $motive->setResaleDownload(false);

                $motive->setTitle($file->getClientOriginalName());
                $motive->setDpiOrg($res['x']);
                $motive->setDpiCon('300');
                $motive->setOrgFilename($filename);

                $motive->setFileOrginal($fileContainer->get($filename)->getKey());
                $motive->setFileWork($fileContainer->get($fileWorkName)->getKey());
                $motive->setFileMid($fileContainer->get($fileMidName)->getKey());
                $motive->setFileThumb($fileContainer->get($fileThumbName)->getKey());
                $motive->setStatus(50);
                $motive->setWidth($im->getImageWidth());
                $motive->setHeight($im->getImageHeight());
                $motive->setMidWidth($imgmid->getImageWidth());
                $motive->setMidHeight($imgmid->getImageHeight());
                $motive->setType($im->getFormat());
                $motive->setCopyright($im->getImageProperty('exif:Copyright'));
                $motive->setExif(json_encode($im->getImageProperties("exif:*")));

                $entityManager->persist($motive);
                $entityManager->flush();
            }
        }

        $json = new JsonResponse(array('success' => true));
        $json->headers->set('Access-Control-Allow-Origin', '*');
        $json->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');

        return $json;
    }

    /**
     * dataAction
     *
     * @Route("/list/edit/{uuid}", name="backend_production_motive_list_edit")
     * @template()
     *
     * @param Request $request Request
     * @param string  $uuid    Uuid
     *
     * @return array
     */
    public function editAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, $uuid)
    {

        $selectedShop = $shopService->getSelectedShop();

        $motiv = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        $motivThemes = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Motivtheme')->findBy(array('parent' => 0, 'shop' => $selectedShop));

        if (strpos(mime_content_type("market/motive/" . $motiv->getFileOrginal()), "svg")) {
            $motiv->svg = file_get_contents("market/motive/" . $motiv->getFileOrginal());
        }

        $form = $this->createForm(MotivType::class, $motiv);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($motiv);
            $entityManager->flush();

            if (strpos(mime_content_type("market/motive/" . $motiv->getFileOrginal()), "svg")) {
                file_put_contents("market/motive/" . $motiv->getFileOrginal(), $motiv->svg);
            }
        }

        return array(
            'form' => $form->createView(),
            'motiv' => $motiv,
            'themes' => $motivThemes
        );
    }

    /**
     * dataAction
     *
     * @Route("/list/delete/{uuid}", name="backend_production_motive_list_delete")
     *
     * @param string $uuid Uuid
     *
     * @return View
     */
    public function deleteAction($uuid, Shop $shopService, EntityManagerInterface $entityManager)
    {
        $selectedShop = $shopService->getSelectedShop();

        $motiv = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneBy(array('uuid' => $uuid, 'shop' => $selectedShop));

        $entityManager->remove($motiv);
        $entityManager->flush();

        return $this->redirect($this->generateUrl("psc_shop_motive_backend_list_index"));
    }
}
