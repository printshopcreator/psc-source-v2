<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MotivBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\ObjectId;
use PSC\Shop\EntityBundle\Document\MotivGroup;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\MotivBundle\Form\Backend\DeleteType;
use PSC\Shop\MotivBundle\Form\Backend\MotivGroupType;
use PSC\Shop\MotivBundle\Form\Backend\MotivType;
use PSC\Shop\MotivBundle\Form\Backend\SearchType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * MotivController fürs Backend
 *
 * @package    PSC\Shop\Motiv\Backend
 * @subpackage Controller
 */
class GroupController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/group/list/index", name="psc_shop_motive_backend_group_list_index")
     * @Template()
     *
     * @param Shop $shopService
     * @param DocumentManager $documentManager
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Shop $shopService, DocumentManager $documentManager)
    {

        $selectedShop = $shopService->getSelectedShop();

        $groups = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\MotivGroup')
            ->findBy(array('shop' => (string)$selectedShop->getUid()));

        return array(
            'groups' => $groups,
        );
    }

    /**
     * Gruppe erzeugen
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/group/create", name="psc_shop_motive_backend_group_create")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param SessionInterface $session
     * @param DocumentManager $documentManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(Request $request, Shop $shopService, SessionInterface $session, DocumentManager $documentManager)
    {
        $selectedShop = $shopService->getSelectedShop();

        $group = new MotivGroup();
        $group->setShop($selectedShop->getUid());

        $form = $this->createForm(MotivGroupType::class, $group);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($group);
            $documentManager->flush();

            $session->getFlashBag()->add(
                'success',
                'Motivgroup \'' . $group->getTitle() . '\' has been created!'
            );

            return $this->redirectToRoute('psc_shop_motive_backend_group_list_index');
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * Gruppe bearbeiten
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/group/edit/{uuid}", name="psc_shop_motive_backend_group_edit")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param DocumentManager $documentManager
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function editAction(Request $request, SessionInterface $session, DocumentManager $documentManager, $uuid)
    {
        $group = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\MotivGroup')
            ->findOneBy(['_id' => new ObjectId($uuid)]);

        $form = $this->createForm(MotivGroupType::class, $group);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($group);
            $documentManager->flush();

            $session->getFlashBag()->add(
                'success',
                'Motivgroup \'' . $group->getTitle() . '\' has been updated!'
            );

            return $this->redirectToRoute('psc_shop_motive_backend_group_list_index');
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * Gruppe löschen
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/group/delete/{uuid}", name="psc_shop_motive_backend_group_delete")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param DocumentManager $documentManager
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(Request $request, SessionInterface $session, DocumentManager $documentManager, $uuid)
    {
        $group = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\MotivGroup')
            ->findOneBy(['_id' => new ObjectId($uuid)]);

        $form = $this->createForm(DeleteType::class);

        $form->handleRequest($request);

        if ($form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $orgTitle = $group->getTitle();
                $documentManager->remove($group);
                $documentManager->flush();
                $session->getFlashBag()->add(
                    'success',
                    'Motivgroup \'' . $orgTitle . '\' has been deleted!'
                );
                return $this->redirectToRoute('psc_shop_motive_backend_group_list_index');
            }
            return $this->redirectToRoute('psc_shop_motive_backend_group_list_index');
        }

        return array(
            'group' => $group,
            'form' => $form->createView(),
        );
    }
}
