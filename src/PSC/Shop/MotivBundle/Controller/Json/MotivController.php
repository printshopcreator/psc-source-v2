<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MotivBundle\Controller\Json;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Gaufrette\FilesystemMapInterface;
use Gaufrette\Util\Path;
use Imagine\Image\Box;
use Imagine\Imagick\Imagine;
use Knp\Bundle\GaufretteBundle\FilesystemMap;
use Liip\ImagineBundle\Controller\ImagineController;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Document\Motiv as DocumentMotiv;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * MotivController Json
 *
 * @package    PSC\Shop\MotivBundle
 * @subpackage Controller
 */
class MotivController extends AbstractController
{
    /**
     * Default Route fürs Backend
     *
     * @Route("/shop/motiv/json/upload", name="shop_motiv_json_upload")
     *
     * @return void
     */
    public function uploadMotivAction(Request $request, SessionInterface $session, AuthorizationCheckerInterface $authorizationChecker, FilesystemMap $filesystemMap, DocumentManager $documentManager)
    {
        if ($request->files->has("file")) {
            $file = $request->files->get("file");

            if ($file instanceof UploadedFile && $authorizationChecker->isGranted('ROLE_USER')) {
                $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());

                $fileContainer = $filesystemMap->get('steplayouter_motiv');

                $im = new \Imagick();
                $im->readimageblob(file_get_contents($file->getPathname()));
                $im->setCompressionQuality(100);
                $im->stripImage();

                $fileContainer->write($filename, $im->getimageblob());

                $im = new \imagick();
                $im->readimageblob($fileContainer->read($filename));
                $res = $im->getImageResolution();
                $res['x'] = intval($res['x']);
                if ($res['x'] != '300') {
                    $res['x'] = 300;
                    $im->setImageResolution(300, 300);
                }

                if ($im->getImageUnits() == 2) {
                    $im->setImageUnits(1);
                    $res['x'] = $res['x'] * 2.54;
                } elseif ($im->getImageUnits() == 0) {
                    $im->setImageUnits(1);
                }
                $im->setimagecompressionquality(100);
                $im->setcompressionquality(100);
                $imgwork = clone $im;
                $fileWorkName = sprintf('%s/%s/%s/work_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileWorkName, $imgwork->getimageblob());
                $imgmid = clone $im;
                $imgmid->thumbnailImage(500, null);
                $fileMidName = sprintf('%s/%s/%s/mid_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileMidName, $imgmid->getimageblob());
                $imgthumb = clone $im;
                $imgthumb->thumbnailImage(null, 110);
                $fileThumbName = sprintf('%s/%s/%s/thumb_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileThumbName, $imgthumb->getimageblob());

                $motiv = new DocumentMotiv();
                $motiv->setTitle($file->getClientOriginalName());
                $motiv->setContactId($this->getUser()->getId());
                $motiv->setDpiOrg($res['x']);
                $motiv->setOrgFilename($filename);
                $motiv->setFileOrginal($fileContainer->get($filename)->getKey());
                $motiv->setFileWork($fileContainer->get($fileWorkName)->getKey());
                $motiv->setFileMid($fileContainer->get($fileMidName)->getKey());
                $motiv->setFileThumb($fileContainer->get($fileThumbName)->getKey());

                $motiv->setWidth($im->getImageWidth());
                $motiv->setHeight($im->getImageHeight());
                $motiv->setMidWidth($imgmid->getImageWidth());
                $motiv->setMidHeight($imgmid->getImageHeight());

                $motiv->setFormat($im->getFormat());
                $motiv->setCopyright($im->getImageProperty('exif:Copyright'));
                $motiv->setExif($im->getImageProperties("exif:*"));

                $motiv->setSessionId($session->getId());

                $documentManager->persist($motiv);
                $documentManager->flush();

                $json = new JsonResponse(array('success' => true, 'uuid' => $motiv->getId()));
                $json->headers->set('Access-Control-Allow-Origin', '*');
                $json->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
            } elseif ($file instanceof UploadedFile) {
                $filename = sprintf('%s/%s/%s/%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());

                $fileContainer = $filesystemMap->get('steplayouter_motiv');

                $im = new \Imagick();
                $im->readimageblob(file_get_contents($file->getPathname()));
                $im->stripImage();

                $fileContainer->write($filename, $im->getimageblob());

                $im = new \imagick();
                $im->readimageblob($fileContainer->read($filename));
                $res = $im->getImageResolution();
                $res['x'] = intval($res['x']);
                if ($res['x'] != '300') {
                    $res['x'] = 300;
                    $im->setImageResolution(300, 300);
                }

                if ($im->getImageUnits() == 2) {
                    $im->setImageUnits(1);
                    $res['x'] = $res['x'] * 2.54;
                } elseif ($im->getImageUnits() == 0) {
                    $im->setImageUnits(1);
                }
                $im->setimagecompressionquality(100);
                $im->setcompressionquality(100);
                $imgwork = clone $im;
                $fileWorkName = sprintf('%s/%s/%s/work_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileWorkName, $imgwork->getimageblob());
                $imgmid = clone $im;
                $imgmid->thumbnailImage(500, null);
                $fileMidName = sprintf('%s/%s/%s/mid_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileMidName, $imgmid->getimageblob());
                $imgthumb = clone $im;
                $imgthumb->thumbnailImage(null, 110);
                $fileThumbName = sprintf('%s/%s/%s/thumb_%s.%s', date('Y'), date('m'), date('d'), uniqid(), $file->getClientOriginalExtension());
                $fileContainer->write($fileThumbName, $imgthumb->getimageblob());

                $motiv = new DocumentMotiv();
                $motiv->setTitle($file->getClientOriginalName());
                $motiv->setDpiOrg($res['x']);
                $motiv->setOrgFilename($filename);
                $motiv->setFileOrginal($fileContainer->get($filename)->getKey());
                $motiv->setFileWork($fileContainer->get($fileWorkName)->getKey());
                $motiv->setFileMid($fileContainer->get($fileMidName)->getKey());
                $motiv->setFileThumb($fileContainer->get($fileThumbName)->getKey());

                $motiv->setWidth($im->getImageWidth());
                $motiv->setHeight($im->getImageHeight());
                $motiv->setMidWidth($imgmid->getImageWidth());
                $motiv->setMidHeight($imgmid->getImageHeight());

                $motiv->setFormat($im->getFormat());
                $motiv->setCopyright($im->getImageProperty('exif:Copyright'));
                $motiv->setExif($im->getImageProperties("exif:*"));

                $motiv->setSessionId($session->getId());

                $documentManager->persist($motiv);
                $documentManager->flush();

                $json = new JsonResponse(array('success' => true, 'uuid' => $motiv->getId()));
                $json->headers->set('Access-Control-Allow-Origin', '*');
                $json->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
            }
        }

        return $json;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/gallery/{shop_uuid}/{limit}/{start}", name="shop_motiv_json_gallery")
     *
     * @return void
     */
    public function getMotivGalleryAction(EntityManagerInterface $entityManager, $shop_uuid, $limit, $start)
    {
        $motive = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->getMotiveByShopUuid($shop_uuid, $limit, $start);

        $count = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->countMotiveByShopUuid($shop_uuid);

        $tmp = array();
        foreach ($motive as $motiv) {
            $tmp[] = array(
                'uuid' => $motiv->getUuid(),
                'title' => $motiv->getTitle(),
                'motiv_mid' => $motiv->getTitle(),
                'motiv_thumb' => $motiv->getTitle(),
            );
        }

        $json = new JsonResponse();
        $json->headers->set('Access-Control-Allow-Origin', '*');
        $json->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        $json->setContent(json_encode(array('success' => true, 'rows' => $tmp, 'count' => $count)));

        return $json;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/mygallery/{shop_uuid}/{limit}/{start}", name="shop_motiv_json_mygallery")
     *
     * @return void
     */
    public function getMyMotivGalleryAction(AuthorizationCheckerInterface $authorizationChecker, SessionInterface $session, DocumentManager $documentManager, $shop_uuid, $limit, $start)
    {

        if ($authorizationChecker->isGranted('ROLE_USER')) {
            $motive = $documentManager
                ->getRepository('PSCEntityBundle:Motiv')
                ->findByContactId((string) $this->getUser()->getId(), $start, $limit);

            $count = $documentManager
                ->getRepository('PSCEntityBundle:Motiv')
                ->countByContactId((string) $this->getUser()->getId());
        } else {
            $motive = $documentManager
                ->getRepository('PSCEntityBundle:Motiv')
                ->findBySessionId($session->getId(), $start, $limit);

            $count = $documentManager
                ->getRepository('PSCEntityBundle:Motiv')
                ->countBySessionId($session->getId());
        }
        $tmp = array();
        foreach ($motive as $motiv) {
            $tmp[] = array(
                'uuid' => $motiv->getId(),
                'title' => $motiv->getTitle()
            );
        }

        $json = new JsonResponse();
        $json->headers->set('Access-Control-Allow-Origin', '*');
        $json->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        $json->setContent(json_encode(array('success' => true, 'rows' => $tmp, 'count' => $count)));

        return $json;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/get/work/{motiv_uuid}", name="shop_motiv_json_get_work_image")
     *
     * @return void
     */
    public function getMotivWorkByUuid(Request $request, DocumentManager $documentManager, EntityManagerInterface $entityManager, ImagineController $imagineController, $dir_market_motiv, $motiv_uuid)
    {
        $motiv = $documentManager
            ->getRepository('PSCEntityBundle:Motiv')
            ->find($motiv_uuid);

        if (!$motiv) {
            $motiv = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($motiv_uuid);
        }

        // RedirectResponse object
        $imagemanagerResponse = $imagineController
            ->filterAction(
                $request,
                $dir_market_motiv . $motiv->getFileWork(),      // original image you want to apply a filter to
                'steplayouter_motiv'              // filter defined in config.yml
            );

        $imagemanagerResponse->headers->set('Access-Control-Allow-Origin', '*');
        $imagemanagerResponse->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        return $imagemanagerResponse;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/get/resize/{motiv_uuid}/{width}/{height}", name="shop_motiv_json_get_resize_image")
     *
     * @return void
     */
    public function getMotivResizeByUuid(DocumentManager $documentManager, EntityManagerInterface $entityManager, Imagine $imagine, $dir_market_motiv, $motiv_uuid, $width, $height)
    {
        $motiv = $documentManager
            ->getRepository('PSCEntityBundle:Motiv')
            ->find($motiv_uuid);

        if (!$motiv) {
            $motiv = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($motiv_uuid);
        }

        $imagine = $imagine->open($dir_market_motiv . $motiv->getFileWork());

        $imagine = $imagine->thumbnail(new Box($width, $height));
        // RedirectResponse object
        $response = new Response();
        $response->setContent($imagine->get('png', array('quality' => 9)));
        $response->headers->set('Content-type', 'image/png');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        return $response;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/get/raw/{motiv_uuid}", name="shop_motiv_json_get_raw_image")
     *
     * @param DocumentManager $documentManager
     * @param EntityManagerInterface $entityManager
     * @param Imagine $imagine
     * @param $dir_market_motiv
     * @param $motiv_uuid
     * @return Response
     */
    public function getMotivRawByUuid(DocumentManager $documentManager, EntityManagerInterface $entityManager, $dir_market_motiv, $motiv_uuid)
    {

        $motiv = $documentManager
            ->getRepository('PSCEntityBundle:Motiv')
            ->find($motiv_uuid);

        if (!$motiv) {
            $motiv = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($motiv_uuid);
        }

        $filePath = $dir_market_motiv . $motiv->getFileOrginal();

        // check if file exists
        if (!file_exists($filePath)) {
            throw $this->createNotFoundException();
        }
        $response = new Response(file_get_contents($filePath));
        $response->headers->set('Content-Type', 'image/svg+xml');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');

        return $response;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/get/download/{motiv_uuid}", name="shop_motiv_json_get_download_image")
     *
     * @return void
     */
    public function downloadMotivByUuid(DocumentManager $documentManager, EntityManagerInterface $entityManager, Imagine $imagine, $dir_market_motiv, $motiv_uuid)
    {

        /** @var \PSC\Shop\EntityBundle\Document\Motiv $motiv */
        $motiv = $documentManager
            ->getRepository('PSCEntityBundle:Motiv')
            ->find($motiv_uuid);

        if (!$motiv) {
            $motiv = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($motiv_uuid);
        }

        $filePath = $dir_market_motiv . $motiv->getFileOrginal();

        // check if file exists
        if (!file_exists($filePath)) {
            throw $this->createNotFoundException();
        }

        $response = new BinaryFileResponse($filePath);
        $response->setContentDisposition('attachment');
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');

        return $response;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/get/mid/{motiv_uuid}", name="shop_motiv_json_get_mid_image")
     *
     * @return void
     */
    public function getMotivMidByUuid(Request $request, DocumentManager $documentManager, EntityManagerInterface $entityManager, ImagineController $imagineController, $dir_market_motiv, $motiv_uuid)
    {

        $motiv = $documentManager
            ->getRepository('PSCEntityBundle:Motiv')
            ->find($motiv_uuid);

        if (!$motiv) {
            $motiv = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($motiv_uuid);
        }

        $imagemanagerResponse = $imagineController
            ->filterAction(
                $request,
                $dir_market_motiv . $motiv->getFileMid(),      // original image you want to apply a filter to
                'steplayouter_motiv'              // filter defined in config.yml
            );

        $imagemanagerResponse->headers->set('Access-Control-Allow-Origin', '*');
        $imagemanagerResponse->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        return $imagemanagerResponse;
    }

    /**
     * Default Route für Motiv Gallery
     *
     * @Route("/shop/motiv/json/get/thumb/{motiv_uuid}", name="shop_motiv_json_get_thumb_image")
     *
     * @return void
     */
    public function getMotivThumbByUuid(Request $request, DocumentManager $documentManager, ImagineController $imagineController, $dir_market_motiv, EntityManagerInterface $entityManager, $motiv_uuid)
    {
        $motiv = $documentManager
            ->getRepository('PSCEntityBundle:Motiv')
            ->find($motiv_uuid);

        if (!$motiv) {
            $motiv = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Motiv')->findOneByUuid($motiv_uuid);
        }

        // RedirectResponse object
        $imagemanagerResponse = $imagineController
            ->filterAction(
                $request,
                $dir_market_motiv . $motiv->getFileWork(),      // original image you want to apply a filter to
                'steplayouter_motiv_thumb'              // filter defined in config.yml
            );

        $imagemanagerResponse->headers->set('Access-Control-Allow-Origin', '*');
        $imagemanagerResponse->headers->set('Access-Control-Allow-Headers', 'X-Requested-With');
        return $imagemanagerResponse;
    }
}
