<?php

/*
 * This file is part of the Sylius package.
 *
 * (c) Paweł Jędrzejewski
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PSC\Shop\MotivBundle\Storage;

use Sylius\Bundle\CartBundle\Model\CartInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Stores current cart id inside the user session.
 *
 * @author Paweł Jędrzejewski <pjedrzejewski@diweb.pl>
 */
class SessionMotivStorage
{
    const KEY = '_psc.motive';

    /**
     * Session.
     *
     * @var SessionInterface
     */
    protected $session;

    /**
     * Key to store the cart id in session.
     *
     * @var string
     */
    protected $key;

    /**
     * Constructor.
     *
     * @param SessionInterface $session
     * @param string           $key
     */
    public function __construct(SessionInterface $session, $key = self::KEY)
    {
        $this->session = $session;
        $this->key = $key;

        if (!$this->session->has($this->key)) {
            $this->session->set($this->key, array());
            $this->session->save();
        }
    }

    public function addMotiv($key, $motiv)
    {



        $motivCollection = $this->session->get($this->key);

        $motivCollection[$key] = $motiv;

        $this->session->set($this->key, $motivCollection);
        $this->session->save();
    }

    public function getMotiv($key)
    {

        $motivCollection = $this->session->get($this->key);

        if (!isset($motivCollection[$key])) {
            return false;
        }

        return $motivCollection[$key];
    }

    public function hasMotiv($key)
    {

        $motivCollection = $this->session->get($this->key);

        if (!isset($motivCollection[$key])) {
            return false;
        }

        return true;
    }

    public function removeMotiv($key)
    {
        $motivCollection = $this->session->get($this->key);

        if (!isset($motivCollection[$key])) {
            return false;
        }

        unset($motivCollection[$key]);

        $this->session->set($this->key, $motivCollection);
        $this->session->save();

        return true;
    }

    public function getAllMotive()
    {
        $motivCollection = $this->session->get($this->key);

        return $motivCollection;
    }
}
