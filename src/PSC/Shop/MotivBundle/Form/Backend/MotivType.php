<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MotivBundle\Form\Backend;

use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class MotivType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid', TextType::class, array('disabled' => true))
            ->add('title', TextType::class)
            ->add('copyright', TextType::class, array('required' => false))
            ->add('width', TextType::class, array('disabled' => true))
            ->add('height', TextType::class, array('disabled' => true))
            ->add('file_thumb', TextType::class, array('disabled' => true))
            ->add('file_mid', TextType::class, array('disabled' => false))
            ->add('file_work', TextType::class, array('disabled' => false))
            ->add('file_orginal', TextType::class, array('disabled' => true))
            ->add('created', DatePickerType::class, array('disabled' => true))
            ->add('resale_shop', CheckboxType::class, array('disabled' => false, 'required' => false))
            ->add('svg', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '100%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'highlight_active_line' => null,
                'required' => false
            ))
            ->add('save', SubmitType::class, array('label' => 'Speichern'));
    }

    public function getName()
    {
        return 'motiv';
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Motiv',
        ));
    }
}
