<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MotivBundle\Form\Backend;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('allShops', CheckboxType::class, array(
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($options) {

                    if (!$values['value']) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()
                        ->andWhere("
                            motiv.shop = '" . $options['selectedShop']->getUid() . "'
                        ");
                },
                'mapped' => false,
                'data' => true,
                'required' => false))
            ->add('search', TextFilterType::class, array(
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) {

                    if (empty($values['value'])) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()
                        ->andWhere("
                            motiv.uuid LIKE '%" . $values['value'] . "%' OR
                            motiv.title LIKE '%" . $values['value'] . "%' OR
                            motiv.copyright LIKE '%" . $values['value'] . "%'
                        ");
                },
                'mapped' => false));
    }

    public function getName()
    {
        return 'item_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false,
            'validation_groups' => array('filtering'),
            'selectedShop' => null
        ));
    }
}
