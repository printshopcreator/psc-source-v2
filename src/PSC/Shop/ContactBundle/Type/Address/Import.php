<?php

namespace PSC\Shop\ContactBundle\Type\Address;

use DirectoryIterator;
use Doctrine\Bundle\MongoDBBundle\ManagerRegistry;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use LogicException;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\QueueBundle\Event\EventInterface;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Type\ConfigurableElementInterface;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use PSC\Shop\UploadBundle\Document\Upload;
use PSC\Shop\UploadBundle\Event\Create;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use ZipArchive;

class Import implements QueueInterface, ConfigurableElementInterface
{
    /** @var Form */
    private $_formFactory = null;
    private $_entityManager = null;
    private $_doctrine_mongodb = null;
    private $_error = null;
    function __construct(FormFactoryInterface $formFactory, EntityManagerInterface $entityManager, DocumentManager $doctrine_mongodb)
    {
        $this->_formFactory = $formFactory;
        $this->_entityManager = $entityManager;
        $this->_doctrine_mongodb = $doctrine_mongodb;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return 'shop_contact_address_import';
    }

    /**
     * @return string
     */
    public function getGroup()
    {
        return 'Kontakt';
    }

    public function getDescription()
    {
        return 'Importiert Adressen in alle Kontakte einer Gruppe';
    }

    public function getName()
    {
        return 'Adressen import';
    }

    public function getForm(FormBuilderInterface $builder, $new, EventInterface $event)
    {
        if ($new) {
            $builder->add("path", TextType::class, array('data' => '/data/www/packages/adresses', 'label' => 'Pfad', 'attr' => array('class' => 'form-element')));
            $builder->add("contacts", TextType::class, array('data' => '1,2,3', 'label' => 'Kontakte', 'attr' => array('class' => 'form-element'), 'required' => false));
            $builder->add("accounts", TextType::class, array('data' => '1,2,3', 'label' => 'Firmen', 'attr' => array('class' => 'form-element'), 'required' => false));
        } else {
            $builder->add("path", TextType::class, array('label' => 'Pfad', 'attr' => array('class' => 'form-element')));
            $builder->add("contacts", TextType::class, array('label' => 'Kontakte', 'attr' => array('class' => 'form-element'), 'required' => false));
            $builder->add("accounts", TextType::class, array('label' => 'Firmen', 'attr' => array('class' => 'form-element'), 'required' => false));
        }
    }

    public function injectDocument(Form $form, EventInterface $event, Queue $objQueue)
    {
        $package = new \PSC\Shop\ContactBundle\Document\Queue\Address\Import();
        $package->setPath($form->get('path')->getData());
        $package->setContacts($form->get('contacts')->getData());
        $package->setAccounts($form->get('accounts')->getData());
        $objQueue->setQueueDocument($package);
    }

    public function setFormData(Form $form, EventInterface $event, Queue $queueObj)
    {
        $form->get('path')->setData($queueObj->getQueueDocument()->getPath());
        $form->get('contacts')->setData($queueObj->getQueueDocument()->getContacts());
        $form->get('accounts')->setData($queueObj->getQueueDocument()->getAccounts());
    }

    public function getTemplate()
    {
        return 'PSCShopContactBundle:Queue/Address:import.html.twig';
    }

    /**
     * @param EventInterface $event
     * @param Queue $packageDoc
     */
    public function execute(EventInterface $event, Queue $packageDoc)
    {

        try {
            if (trim($packageDoc->getQueueDocument()->getContacts()) == "") {
                $contacts = array();
            } else {
                $contacts = explode(",", $packageDoc->getQueueDocument()->getContacts());
            }

            $accounts = explode(",", $packageDoc->getQueueDocument()->getAccounts());
            if (!empty($accounts)) {
                $contactsQuery = $this->_entityManager
                    ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
                    ->createQueryBuilder('contact')
                    ->andWhere('contact.account in (:accounts)')
                    ->setParameter("accounts", implode(",", $accounts))
                    ->getQuery()->execute();
            /** @var Contact $cont */
                foreach ($contactsQuery as $cont) {
                    array_push($contacts, $cont->getId());
                }
            }



            foreach (new DirectoryIterator($packageDoc->getQueueDocument()->getPath()) as $fileInfo) {
                if ($fileInfo->isDot() || $fileInfo->isDir()) {
                    continue;
                }

                if (($handle = fopen($fileInfo->getPathname(), "r")) !== false) {
                    while (($data = fgetcsv($handle, 1000, ";", '"')) !== false) {
                        if (strtolower($data[0]) == "id" || trim(strtolower($data[0])) == "") {
                            continue;
                        }

                        reset($contacts);
                        foreach ($contacts as $contact) {
    /** @var ContactAddress $ca */
                            $ca = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
                            ->findOneBy(array('contact' => $contact, 'ustid' => $data[0], 'type' => 2));
    /** @var ContactAddress $ca */
                            $c = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
                            ->findOneBy(array('uid' => $contact));
                            if (!$c) {
                                        continue;
                            }

                            if ($ca) {
                            } else {
                                $ca = new ContactAddress();
                                $ca->setContact($c);
                                $ca->setInstallId(1);
                                $ca->setUstid($data[0]);
                            }

                            $ca->setCity($data[8]);
                            $ca->setFirstname($data[10]);
                            $ca->setLastname($data[9]);
                            $ca->setEmail($data[13]);
                            $ca->setZip($data[7]);
                            $ca->setPhone($data[12]);
                            $ca->setStreet($data[5]);
                            $ca->setCompany($data[3]);
                            $ca->setCompany2($data[4]);
                            $ca->setKundenNr($data[0]);
                            $ca->setZusatz1($data[1]);
                            $ca->setZusatz2($data[2]);
                            $ca->setHouseNumber("");
                            $ca->setType(2);
                            $ca->setAnrede(1);
                            if ($data[2] == "weiblich") {
                                $ca->setAnrede(2);
                            }

                            $this->_entityManager->persist($ca);
                            $this->_entityManager->flush();
    /** @var ContactAddress $ca */
                            $ca = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
                            ->findOneBy(array('contact' => $contact, 'ustid' => $data[0], 'type' => 2));
    /** @var \PSC\Shop\EntityBundle\Document\ContactAddress $contactDoc */
                            $cam = $this->_doctrine_mongodb
                            ->getRepository('PSC\Shop\EntityBundle\Document\ContactAddress')
                            ->findOneBy(array('uid' => (string)$ca->getUid()));
                            if ($cam) {
                            } else {
                                $cam = new \PSC\Shop\EntityBundle\Document\ContactAddress();
                                $cam->setUid((string)$ca->getUid());
                            }
                            $cam->setKundenNr($data[1]);
                            $this->_doctrine_mongodb->getManager()->persist($cam);
                            $this->_doctrine_mongodb->getManager()->flush();
    /** @var ContactAddress $ca */
                            $ca = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
                            ->findOneBy(array('contact' => $contact, 'ustid' => $data[0], 'type' => 1));
                            if ($ca) {
                            } else {
                                $ca = new ContactAddress();
                                $ca->setContact($c);
                                $ca->setInstallId(1);
                                $ca->setUstid($data[0]);
                            }

                            $ca->setCity($data[8]);
                            $ca->setFirstname($data[10]);
                            $ca->setLastname($data[9]);
                            $ca->setEmail($data[13]);
                            $ca->setZip($data[7]);
                            $ca->setPhone($data[12]);
                            $ca->setStreet($data[5]);
                            $ca->setCompany($data[3]);
                            $ca->setCompany2($data[4]);
                            $ca->setKundenNr($data[0]);
                            $ca->setZusatz1($data[1]);
                            $ca->setZusatz2($data[2]);
                            $ca->setHouseNumber("");
                            $ca->setType(1);
                            $ca->setAnrede(1);
                            if ($data[2] == "weiblich") {
                                $ca->setAnrede(2);
                            }

                            $this->_entityManager->persist($ca);
                            $this->_entityManager->flush();
    /** @var ContactAddress $ca */
                            $ca = $this->_entityManager->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
                            ->findOneBy(array('contact' => $contact, 'ustid' => $data[0], 'type' => 1));
    /** @var \PSC\Shop\EntityBundle\Document\ContactAddress $contactDoc */
                            $cam = $this->_doctrine_mongodb
                            ->getRepository('PSC\Shop\EntityBundle\Document\ContactAddress')
                            ->findOneBy(array('uid' => (string)$ca->getUid()));
                            if ($cam) {
                            } else {
                                $cam = new \PSC\Shop\EntityBundle\Document\ContactAddress();
                                $cam->setUid((string)$ca->getUid());
                            }
                            $cam->setKundenNr($data[1]);
                            $this->_doctrine_mongodb->getManager()->persist($cam);
                            $this->_doctrine_mongodb->getManager()->flush();
                        }
                    }
                }

                if (!file_exists($packageDoc->getQueueDocument()->getPath() . '/finish')) {
                    mkdir($packageDoc->getQueueDocument()->getPath() . '/finish');
                }

                rename($fileInfo->getPathname(), $packageDoc->getQueueDocument()->getPath() . '/finish/' . $fileInfo->getFilename());
            }
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            $this->_error = $e->getMessage();
            return false;
        }

        return true;
    }

    public function getError()
    {
        return $this->_error;
    }
}
