<?php

namespace PSC\Shop\ContactBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Contact
{
    /**
     * @OA\Property(type="integer")
     */
    private int $uid = 0;

        /**
     * @OA\Property(type="string")
     */
    private string $uuid = "";


    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private string $username = "";

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private string $password = "";

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private string $email = "";

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $value): void
    {
        $this->uuid = $value;
    }

    public function getEMail(): string
    {
        return $this->email;
    }

    public function setEMail(string $value): void
    {
        $this->email = $value;
    }

    public function getUsername(): string
    {
        return $this->username;
    }

    public function setUsername(string $value): void
    {
        $this->username = $value;
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $value): void
    {
        $this->password = $value;
    }

    public function getUid(): int
    {
        return $this->uid;
    }

    public function setUid(int $value): void
    {
        $this->uid = $value;
    }
}
