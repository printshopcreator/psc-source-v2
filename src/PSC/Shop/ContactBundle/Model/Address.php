<?php

namespace PSC\Shop\ContactBundle\Model;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Address
{
    /**
     * @OA\Property(type="integer")
     */
    private int $uid = 0;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private string $uuid = "";

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $firstname;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $lastname;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $street;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $houseNumber;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $zip;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $city;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $phone;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $mobilPhone;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $email;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $company;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $anrede;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $country;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $fax;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $company2;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $kostenstellung;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $abteilung;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $position;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $ustid;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $zusatz1;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $zusatz2;

   /**
     * @OA\Property(type="int")
     */
    private $pos;

    /**
     * @OA\Property(type="string", maxLength=255)
     */
    private $kundenNr;

    public function getUid(): ?int
    {
        return $this->uid;
    }

    public function setUid(int $uid): void
    {
        $this->uid = $uid;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): void
    {
        $this->uuid = $uuid;
    }

    public function getFirstname()
    {
        return $this->firstname;
    }

    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMobilPhone()
    {
        return $this->mobilPhone;
    }

    /**
     * @param string $mobilPhone
     */
    public function setMobilPhone($mobilPhone)
    {
        $this->mobilPhone = $mobilPhone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getAnrede()
    {
        return $this->anrede;
    }

    /**
     * @param string $anrede
     */
    public function setAnrede($anrede)
    {
        $this->anrede = $anrede;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getCompany2()
    {
        return $this->company2;
    }

    /**
     * @param string $company2
     */
    public function setCompany2($company2)
    {
        $this->company2 = $company2;
    }

    /**
     * @return string
     */
    public function getKostenstellung()
    {
        return $this->kostenstellung;
    }

    /**
     * @param string $kostenstellung
     */
    public function setKostenstellung($kostenstellung)
    {
        $this->kostenstellung = $kostenstellung;
    }

    /**
     * @return string
     */
    public function getAbteilung()
    {
        return $this->abteilung;
    }

    /**
     * @param string $abteilung
     */
    public function setAbteilung($abteilung)
    {
        $this->abteilung = $abteilung;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getUstid()
    {
        return $this->ustid;
    }

    /**
     * @param string $ustid
     */
    public function setUstid($ustid)
    {
        $this->ustid = $ustid;
    }

    /**
     * @return string
     */
    public function getZusatz1()
    {
        return $this->zusatz1;
    }

    /**
     * @param string $zusatz1
     */
    public function setZusatz1($zusatz1)
    {
        $this->zusatz1 = $zusatz1;
    }

    /**
     * @return string
     */
    public function getZusatz2()
    {
        return $this->zusatz2;
    }

    /**
     * @param string $zusatz2
     */
    public function setZusatz2($zusatz2)
    {
        $this->zusatz2 = $zusatz2;
    }

    /**
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param int $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }
}
