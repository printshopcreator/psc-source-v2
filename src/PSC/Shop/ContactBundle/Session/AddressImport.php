<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Session;

class AddressImport implements \Serializable
{
    protected $filePath;
    protected $ignoreFirstLine = false;
    protected $overwrite = false;
    protected $columns = array();
/**
     * String representation of object
     * @link http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
                $this->filePath,
                $this->ignoreFirstLine,
                $this->overwrite,
                $this->columns,
            ]);
    }

    /**
     * Constructs the object
     * @link http://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        list(
            $this->filePath,
            $this->ignoreFirstLine,
            $this->overwrite,
            $this->columns,
            ) = $data;
    }

    /**
     * @return mixed
     */
    public function getFilePath()
    {
        return $this->filePath;
    }

    /**
     * @param mixed $filePath
     */
    public function setFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @return bool
     */
    public function isIgnoreFirstLine()
    {
        return $this->ignoreFirstLine;
    }

    /**
     * @param bool $ignoreFirstLine
     */
    public function setIgnoreFirstLine($ignoreFirstLine)
    {
        $this->ignoreFirstLine = $ignoreFirstLine;
    }

    /**
     * @return bool
     */
    public function isOverwrite()
    {
        return $this->overwrite;
    }

    /**
     * @param bool $overwrite
     */
    public function setOverwrite($overwrite)
    {
        $this->overwrite = $overwrite;
    }

    /**
     * @return array
     */
    public function getColumns()
    {
        return $this->columns;
    }

    /**
     * @param array $columns
     */
    public function setColumns($columns)
    {
        $this->columns = $columns;
    }
}
