<?php

namespace PSC\Shop\ContactBundle\Transformer;

use PSC\Shop\EntityBundle\Entity\Contact as PSCContact;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Order
 *
 * @author Thomas Peterson
 */
class Contact
{
    public function fromEntity(\PSC\Shop\ContactBundle\Model\Contact $contact, PSCContact $contactEntity)
    {
        $contact->setUUId((string)$contactEntity->getUuid());
        $contact->setEMail((string)$contactEntity->getEmail());
        $contact->setUid((int)$contactEntity->getUid());
        $contact->setUsername($contactEntity->getEmail());
    }
}
