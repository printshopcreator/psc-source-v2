<?php

namespace PSC\Shop\ContactBundle\Transformer;

use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Order
 *
 * @author Thomas Peterson
 */
class Address
{
    public function fromEntity(\PSC\Shop\ContactBundle\Model\Address $address, ContactAddress $addressEntity)
    {
        $address->setUUId((string)$addressEntity->getUuid());
        $address->setEMail((string)$addressEntity->getEmail());
        $address->setUid($addressEntity->getUid());
        $address->setFirstname($addressEntity->getFirstname());
        $address->setLastname($addressEntity->getLastname());
        $address->setCompany($addressEntity->getCompany());
        $address->setCompany2($addressEntity->getCompany2());
        $address->setStreet($addressEntity->getStreet());
        $address->setHouseNumber($addressEntity->getHouseNumber());
        $address->setZip($addressEntity->getZip());
        $address->setCity($addressEntity->getCity());
    }
}
