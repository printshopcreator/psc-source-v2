<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\ContactBundle\Form\Backend\SearchType;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * ListController fürs Backend
 *
 * @package    PSC\Shop\Contact\Backend
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Security("is_granted('ROLE_ADMIN')")
     *
     * @Route("/list/index", name="psc_shop_contact_backend_list")
     * @Template()
     *
     * @return array
     */
    public function indexAction(
        Request $request,
        SessionInterface $session,
        Shop $shopService,
        EntityManagerInterface $entityManager,
        PaginatorInterface $paginator,
        FilterBuilderUpdaterInterface $filterBuilderUpdater
    ) {

        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
        $qb = $userRepository->createQueryBuilder('contact')
            ->leftJoin('contact.shops', 'shops')
            ->orderBy('contact.uid', 'desc');
        $form = $this->createForm(SearchType::class);
        if ($request->isMethod('POST')) {
            $session->set('account_sort_page', 1);
            $form->handleRequest($request);
        }

        $filterBuilderUpdater->addFilterConditions($form, $qb);
        if ($request->get('direction', false)) {
            $session->set('contact_sort_direction', $request->get('direction', 'DESC'));
            $session->set('contact_sort_field', $request->get('sort', 'contact.uid'));
        }
        if ($request->get('page', false)) {
            $session->set('contact_sort_page', $request->get('page', 1));
        }

        $qb->andwhere('shops.uid = :shop_id')
            ->andWhere('(contact.username != :sysadminUser OR contact.username is null)')
            ->setParameter("shop_id", $selectedShop->getUid())
            ->setParameter("sysadminUser", 'sysadmin@printshopcreator.de');
        $pagination = $paginator->paginate($query = $qb->getQuery(), $request->query->getInt('page', $session->get('contact_sort_page', 1)), 25, [
                'defaultSortFieldName' => $session->get('contact_sort_field', 'contact.uid'),
                'defaultSortDirection' => $session->get('contact_sort_direction', 'DESC')
            ]);
        return array(
            'selectedShop' => $shopService->getSelectedShop(),
            'pagination' => $pagination,
            'form' => $form->createView()
        );
    }
}
