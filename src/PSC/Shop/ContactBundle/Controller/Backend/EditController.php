<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Controller\Backend;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PSC\Shop\ContactBundle\Document\History as PSCHistory;
use PSC\Shop\ContactBundle\Event\Contact\Lock;
use PSC\Shop\ContactBundle\Event\Contact\UnLock;
use PSC\Shop\ContactBundle\Form\Backend\General\ContactType;
use PSC\Shop\ContactBundle\Form\Backend\General\AnonymType;
use PSC\Shop\ContactBundle\PSCShopContactBundle;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\ContactRole;
use PSC\Shop\EntityBundle\Entity\Role;
use PSC\Shop\EntityBundle\Entity\ShopContact;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\PluginBundle\Form\Chain\Section;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use Ramsey\Uuid\Uuid;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use PSC\System\SettingsBundle\Service\Status;

/**
 * EditController fürs Backend
 *
 * @package    PSC\Shop\Contact\Backend
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;

    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }

    /**
     * edit
     *
     * @Route("/edit/create", name="psc_shop_contact_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param PaginatorInterface $paginator
     * @param string $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(
        Request $request,
        Field $fieldService,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        PaginatorInterface $paginator,
        $uuid = ""
    ) {

        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact);

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /** @var Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string) $selectedShop->getUid()));

        /** @var Contact $contact */
        $contact = new Contact();
        $contact->setShops([$selectedShop]);

        /** @var Role $shop */
        $role = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Role')->findOneBy(['level' => 10]);

        $contact->setRolesForm([$role]);

        $form = $this->createForm(ContactType::class, $contact);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setProducts($this->merge([$contact->getProductsOrg(), $contact->getProductsSub()]));

            $contact->setUuid(Uuid::uuid4());

            $entityManager->persist($contact);

            $entityManager->flush();

            $contactDoc = new \PSC\Shop\EntityBundle\Document\Contact();
            $contactDoc->setUid($contact->getid());
            $contactDoc->setKundenNr($contact->getKundenNr());
            $contactDoc->setCalcValue1($contact->getCalcValue1());
            $contactDoc->setCalcValue2($contact->getCalcValue2());
            $contactDoc->setCustom1($contact->getCustom1());
            $contactDoc->setCustom2($contact->getCustom2());
            $contactDoc->setCustom3($contact->getCustom3());
            $contactDoc->setCustom4($contact->getCustom4());
            $contactDoc->setCustom5($contact->getCustom5());
            $contactDoc->setCustom6($contact->getCustom6());
            $contactDoc->setCustom7($contact->getCustom7());
            $contactDoc->setCustom8($contact->getCustom8());
            $contactDoc->setCustom9($contact->getCustom9());
            $contactDoc->setCustom10($contact->getCustom10());
            $contactDoc->setCustom11($contact->getCustom11());
            $contactDoc->setCustom12($contact->getCustom12());
            $contactDoc->setCustom13($contact->getCustom13());
            $contactDoc->setCustom14($contact->getCustom14());
            $contactDoc->setCustom15($contact->getCustom15());
            $contactDoc->setCustom16($contact->getCustom16());
            $contactDoc->setCustom17($contact->getCustom17());
            $contactDoc->setCustom18($contact->getCustom18());
            $contactDoc->setCustom19($contact->getCustom19());
            $contactDoc->setCustom20($contact->getCustom20());
            $contactDoc->setCustom21($contact->getCustom21());
            $contactDoc->setCustom22($contact->getCustom22());
            $contactDoc->setCustom23($contact->getCustom23());
            $contactDoc->setCustom24($contact->getCustom24());
            $contactDoc->setExtraSettings($contact->getExtraSettings());
            $contactDoc->setPriceFactor($contact->getPriceFactor());

            $documentManager->persist($contactDoc);
            $documentManager->flush();

            $session->getFlashBag()->add(
                'success',
                'Contact \'' . $contact->getUsername() . '\' has been created!'
            );
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopContactBundle::class, $contact->getUsername(), "Contact saved");
            return $this->redirectToRoute('psc_shop_contact_backend_edit', ['uuid' => $contact->getUuid()]);
        }

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress');
        $qb = $userRepository->createQueryBuilder('contactAddress')
            ->andWhere('contactAddress.contact = :contact_id')
            ->setParameter("contact_id", $contact->getId())
            ->orderBy('contactAddress.uid', 'asc');

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            15
        );

        return array(
            'form' => $form->createView(),
            'contact' => $contact,
            'pagination' => $pagination,
            'shopDoc' => $shop,
            'customFields' => $customFields,
            'customGroups' => $customGroups

        );
    }

    /**
     * edit
     *
     * @Route("/edit/edit/{uuid}", name="psc_shop_contact_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param Section $sectionService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param PaginatorInterface $paginator
     * @param SessionInterface $session
     * @param Manager $eventManager
     * @param Status $statusService
     * @param \PSC\Shop\OrderBundle\Service\Order $orderService
     * @param string $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(
        Request $request,
        Field $fieldService,
        Section $sectionService,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        PaginatorInterface $paginator,
        SessionInterface $session,
        Manager $eventManager,
        Status $statusService,
        \PSC\Shop\OrderBundle\Service\Order $orderService,
        $uuid = ""
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact);

        $customSections = $sectionService->get(\PSC\System\PluginBundle\Form\Interfaces\Section::Contact);

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        /** @var Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string) $selectedShop->getUid()));

        /** @var Contact $contact */
        $contact = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->findOneBy(array('uuid' => $uuid));

        /** @var \PSC\Shop\EntityBundle\Document\Contact $contactDoc */
        $contactDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Contact')
            ->findOneBy(array('uid' => (string) $contact->getId()));

        $contactEnable = $contact->isEnable();

        if (!$request->isMethod('POST') && $contactDoc) {
            $contact->setKundenNr($contactDoc->getKundenNr());
            $contact->setCalcValue1($contactDoc->getCalcValue1());
            $contact->setCalcValue2($contactDoc->getCalcValue2());
            $contact->setCustom1($contactDoc->getCustom1());
            $contact->setCustom2($contactDoc->getCustom2());
            $contact->setCustom3($contactDoc->getCustom3());
            $contact->setCustom4($contactDoc->getCustom4());
            $contact->setCustom5($contactDoc->getCustom5());
            $contact->setCustom6($contactDoc->getCustom6());
            $contact->setCustom7($contactDoc->getCustom7());
            $contact->setCustom8($contactDoc->getCustom8());
            $contact->setCustom9($contactDoc->getCustom9());
            $contact->setCustom10($contactDoc->getCustom10());
            $contact->setCustom11($contactDoc->getCustom11());
            $contact->setCustom12($contactDoc->getCustom12());
            $contact->setCustom13($contactDoc->getCustom13());
            $contact->setCustom14($contactDoc->getCustom14());
            $contact->setCustom15($contactDoc->getCustom15());
            $contact->setCustom16($contactDoc->getCustom16());
            $contact->setCustom17($contactDoc->getCustom17());
            $contact->setCustom18($contactDoc->getCustom18());
            $contact->setCustom19($contactDoc->getCustom19());
            $contact->setCustom20($contactDoc->getCustom20());
            $contact->setCustom21($contactDoc->getCustom21());
            $contact->setCustom22($contactDoc->getCustom22());
            $contact->setCustom23($contactDoc->getCustom23());
            $contact->setCustom24($contactDoc->getCustom24());
            $contact->setShowOtherOrders($contactDoc->isShowOtherOrders());
            $contact->setShowOtherOrdersAccount(new Account($contactDoc->getShowOtherOrdersAccount()));
            $contact->setShowOtherOrdersAccountFilter($contactDoc->getShowOtherOrdersAccountFilter());
            $contact->setPluginSettings($contactDoc->getPluginSettings());
            $contact->setExtraSettings($contactDoc->getExtraSettings());
            $contact->setPriceFactor($contactDoc->getPriceFactor());
            $contact->setDefaultPayment($contactDoc->getDefaultPayment());
            $contact->setDefaultShipment($contactDoc->getDefaultShipment());

            $contact->setProductsOrg($contact->getProducts());
            $contact->setProductsSub($contact->getProducts());
        } elseif (!$contactDoc) {
            $contactDoc = new \PSC\Shop\EntityBundle\Document\Contact();
            $contactDoc->setKundenNr("");
            $contactDoc->setCalcValue1("");
            $contactDoc->setCalcValue2("");
            $contactDoc->setUid($contact->getid());
            $contactDoc->setExtraSettings($contact->getExtraSettings());
            $contactDoc->setPriceFactor($contact->getPriceFactor());
            $contactDoc->setDefaultPayment($contact->getDefaultPayment());
            $contactDoc->setDefaultShipment($contact->getDefaultShipment());

            $contact->setProductsOrg($contact->getProducts());
            $contact->setProductsSub($contact->getProducts());

            $documentManager->persist($contactDoc);
            $documentManager->flush();
        }

        $form = $this->createForm(ContactType::class, $contact, [
            'products' => $contact->getProducts(),
            'productGroups' => $contact->getProductGroups(),
            'payments' => $contact->payments,
            'shippings' => $contact->shippings,
            'cms' => $contact->cms
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setProducts($this->merge([$contact->getProductsOrg(), $contact->getProductsSub()]));

            $contactDoc->setKundenNr($contact->getKundenNr());
            $contactDoc->setCalcValue1($contact->getCalcValue1());
            $contactDoc->setCalcValue2($contact->getCalcValue2());
            $contactDoc->setCustom1($contact->getCustom1());
            $contactDoc->setCustom2($contact->getCustom2());
            $contactDoc->setCustom3($contact->getCustom3());
            $contactDoc->setCustom4($contact->getCustom4());
            $contactDoc->setCustom5($contact->getCustom5());
            $contactDoc->setCustom6($contact->getCustom6());
            $contactDoc->setCustom7($contact->getCustom7());
            $contactDoc->setCustom8($contact->getCustom8());
            $contactDoc->setCustom9($contact->getCustom9());
            $contactDoc->setCustom10($contact->getCustom10());
            $contactDoc->setCustom11($contact->getCustom11());
            $contactDoc->setCustom12($contact->getCustom12());
            $contactDoc->setCustom13($contact->getCustom13());
            $contactDoc->setCustom14($contact->getCustom14());
            $contactDoc->setCustom15($contact->getCustom15());
            $contactDoc->setCustom16($contact->getCustom16());
            $contactDoc->setCustom17($contact->getCustom17());
            $contactDoc->setCustom18($contact->getCustom18());
            $contactDoc->setCustom19($contact->getCustom19());
            $contactDoc->setCustom20($contact->getCustom20());
            $contactDoc->setCustom21($contact->getCustom21());
            $contactDoc->setCustom22($contact->getCustom22());
            $contactDoc->setCustom23($contact->getCustom23());
            $contactDoc->setCustom24($contact->getCustom24());
            $contactDoc->setExtraSettings($contact->getExtraSettings());
            $contactDoc->setShowOtherOrders($contact->isShowOtherOrders());
            $contactDoc->setPriceFactor($contact->getPriceFactor());
            $contactDoc->setShowOtherOrdersAccountFilter($contact->getShowOtherOrdersAccountFilter());
            if ($contact->getShowOtherOrdersAccount()) {
                $contactDoc->setShowOtherOrdersAccount($contact->getShowOtherOrdersAccount()->getUid());
            } else {
                $contactDoc->setShowOtherOrdersAccount(null);
            }
            $contactDoc->setDefaultShipment($contact->getDefaultShipment());
            $contactDoc->setDefaultPayment($contact->getDefaultPayment());

            $this->historyService->createHistoryEntry(new PSCHistory((string)$contact->getUid()), $contact, $contactDoc);
            $entityManager->persist($contact);
            $entityManager->flush();

            $documentManager->persist($contactDoc);
            $documentManager->flush();

            $r = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\ShopContact');
            $r->resetAdmin($contact->getId(), false);
            if ($request->get('admin', false)) {
                foreach ($request->get('admin', false) as $key => $value) {
                    $r->updateAdmin($contact->getId(), $value);
                }
            }

            if ($contactEnable != $contact->isEnable()) {
                if ($contact->isEnable()) {
                    $notify = new UnLock();
                    $notify->setShop($selectedShop->getUID());
                    $notify->setContact($contact->getUuid());
                    $eventManager->addJob($notify);
                } else {
                    $notify = new Lock();
                    $notify->setShop($selectedShop->getUID());
                    $notify->setContact($contact->getUuid());
                    $eventManager->addJob($notify);
                }
            }

            $session->getFlashBag()->add(
                'success',
                'Contact \'' . $contact->getUsername() . '\' has been updated!'
            );
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopContactBundle::class, $contact->getUsername(), "Contact updated");
        }

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress');
        $qb = $userRepository->createQueryBuilder('contactAddress')
            ->andWhere('contactAddress.contact = :contact_id')
            ->setParameter("contact_id", $contact->getId())
            ->orderBy('contactAddress.uid', 'asc');

        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            15,
            [
                'pageParameterName' => 'page',
                'sortFieldParameterName' => 'sort'
            ]
        );


        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Order');
        $qb = $userRepository->createQueryBuilder('orders')
            ->leftJoin('orders.contact', 'contact')
            ->andwhere('contact.uid = :contact_id')->setParameter("contact_id", $contact->getUid())
            ->orderBy('orders.uid', 'desc');

        $qbCount = $userRepository->createQueryBuilder('orders')
            ->select('count(orders.uid)')
            ->leftJoin('orders.contact', 'contact')
            ->andwhere('contact.uid = :contact_id')->setParameter("contact_id", $contact->getUid())
            ->orderBy('orders.uid', 'desc');


        $qb->andWhere('orders.shop = :shop_id')
            ->setParameter("shop_id", $selectedShop->getUid());

        $qbCount->andWhere('orders.shop = :shop_id')
            ->setParameter("shop_id", $selectedShop->getUid());


        $query = $qb->getQuery();

        $count = $qbCount->getQuery()->getSingleScalarResult();

        $query->setHint('knp_paginator.count', $count);

        $pagination2 = $paginator->paginate(
            $query,
            $request->query->getInt('pages_order', 1),
            10,
            [
                'distinct' => false,
                'pageParameterName' => 'pages_order',
                'sortFieldParameterName' => 'sorts'
            ]
        );

        return array(
            'form' => $form->createView(),
            'contact' => $contact,
            'contactDoc' => $contactDoc,
            'pagination' => $pagination,
            'shopDoc' => $shop,
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'customSections' => $customSections,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$contact->getUid()),
            'pagination2' => $pagination2,
            'orderStatuse' => $statusService,
            'orderService' => $orderService,
        );
    }

    /**
     * Anonymisieren Seite
     *
     * @Route("/edit/anonymisieren/{uuid}", name="psc_shop_contact_backend_edit_anonymisieren")
     * @Template()
     *
     * @param Request $request
     * @param string $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function anonymisierenAction(
        Request $request,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        $uuid = ""
    ) {

        /**
         * @var Contact $contact
         * @var ContactAddress $contactAddress
         */
        $contact = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->findOneBy(array('uuid' => $uuid));

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();

        $form = $this->createForm(AnonymType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $orgEmail = $contact->getEmail();
                $contact->anonymisieren();

                $contactAddresses = $entityManager
                    ->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
                    ->findBy(array('contact' => $contact));

                foreach ($contactAddresses as $contactAddress) {
                    $contactAddress->anonymisieren();
                    $entityManager->persist($contactAddress);
                }

                $entityManager->persist($contact);
                $entityManager->flush();
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopContactBundle::class, $orgEmail, "Contact anonym");
            }

            return $this->redirectToRoute('psc_shop_contact_backend_list');
        }

        return array(
            'contact' => $contact,
            'form' => $form->createView()
        );
    }

    /**
     * @param array... $arrayCollections
     * @return ArrayCollection
     */
    private function merge($arrayCollections)
    {
        $returnCollection = new ArrayCollection();

        /**
         * @var ArrayCollection $arrayCollection
         */
        foreach ($arrayCollections as $arrayCollection) {
            if ($returnCollection->count() === 0) {
                $returnCollection = $arrayCollection;
            } else {
                $arrayCollection->map(function ($element) use (&$returnCollection) {
                    if (!$returnCollection->contains($element)) {
                        $returnCollection->add($element);
                    }
                });
            }
        }

        return $returnCollection;
    }
}
