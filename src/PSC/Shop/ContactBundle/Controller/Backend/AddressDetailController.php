<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\ContactBundle\Document\Address\History as PSCHistory;
use PSC\Shop\ContactBundle\Form\Backend\General\ContactAddressType;
use PSC\Shop\ContactBundle\Form\Backend\General\ContactType;
use PSC\Shop\ContactBundle\Form\Backend\General\AnonymType;
use PSC\Shop\ContactBundle\PSCShopContactBundle;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * DetailController fürs Backend
 *
 * @package    PSC\Shop\Contact\Backend
 * @subpackage Controller
 */
class AddressDetailController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;
    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }

    /**
     * Delete Seite
     *
     * @Route("/address/detail/edit/{uuid}", name="psc_shop_contact_backend_address_detail_edit")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param string $uuid
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function editAction(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        $uuid = ""
    ) {
        /** @var ContactAddress $contact */
        $contactAddress = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
                ->findOneBy(array('uuid' => $uuid));
/** @var \PSC\Shop\EntityBundle\Document\ContactAddress $contactDoc */
        $contactAddressDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\ContactAddress')
            ->findOneBy(array('uid' => (string)$contactAddress->getUid()));
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Contact $contact */
        $contact = $contactAddress->getContact();
/** @var \PSC\Shop\EntityBundle\Document\Contact $contactDoc */
        $contactDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Contact')
            ->findOneBy(array('uid' => (string)$contact->getId()));
        if (!$request->isMethod('POST') && $contactAddressDoc) {
            $contactAddress->setKundenNr($contactAddressDoc->getKundenNr());
            $contactAddress->setHomepage($contactAddressDoc->getHomepage());
        } elseif (!$contactAddressDoc) {
            if (!$contactDoc) {
                $contactDoc = new \PSC\Shop\EntityBundle\Document\Contact();
                $contactDoc->setKundenNr("");
                $contactDoc->setUid($contact->getid());
                $documentManager->persist($contactDoc);
            }

            $contactAddressDoc = new \PSC\Shop\EntityBundle\Document\ContactAddress();
            $contactAddressDoc->setKundenNr($contactDoc->getKundenNr());
            $contactAddressDoc->setUid($contactAddress->getUid());
            $documentManager->persist($contactAddressDoc);
            $documentManager->flush();
            $contactAddress->setKundenNr($contactDoc->getKundenNr());
        }

        $form = $this->createForm(ContactAddressType::class, $contactAddress);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $contactAddressDoc->setKundenNr($contactAddress->getKundenNr());
            $contactAddressDoc->setHomepage($contactAddress->getHomepage());
            $this->historyService->createHistoryEntry(new PSCHistory((string)$contactAddress->getUid()), $contactAddress, $contactAddressDoc);
            $entityManager->persist($contactAddress);
            $entityManager->flush();
            $documentManager->persist($contactAddressDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Address has been updated!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopContactBundle::class, $contactAddress->getUuid(), "Address has been updated", ['contact' => $contact->getEmail()]);
            return $this->redirectToRoute('psc_shop_contact_backend_edit', array('uuid' => $contact->getUuid()));
        }

        return array(
            'form' => $form->createView(),
            'contact' => $contact,
            'contactAddress' => $contactAddress,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$contactAddress->getUid())
        );
    }

    /**
     * Delete Seite
     *
     * @Route("/address/detail/create/{contactUUID}", name="psc_shop_contact_backend_address_detail_create")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param string $contactUUID
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function createAction(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        $contactUUID = ""
    ) {

        /** @var Contact $contact */
        $contact = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->findOneBy(array('uuid' => $contactUUID));
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var ContactAddress $contactAddress */
        $contactAddress = new ContactAddress();
        $contactAddress->setInstallId($contact->getInstallId());
        $contactAddress->setContact($contact);
/** @var \PSC\Shop\EntityBundle\Document\ContactAddress $contactDoc */
        $contactAddressDoc = new \PSC\Shop\EntityBundle\Document\ContactAddress();
/** @var \PSC\Shop\EntityBundle\Document\Contact $contactDoc */
        $contactDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Contact')
            ->findOneBy(array('uid' => (string)$contact->getId()));
        $form = $this->createForm(ContactAddressType::class, $contactAddress);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($contactAddress);
            $entityManager->flush();
            $contactAddressDoc->setKundenNr($contactAddress->getKundenNr());
            $contactAddressDoc->setHomepage($contactAddress->getHomepage());
            $contactAddressDoc->setUid($contactAddress->getUid());
            $documentManager->persist($contactAddressDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Address has been created!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopContactBundle::class, $contactAddress->getUuid(), "Address has been created", ['contact' => $contact->getEmail()]);
            return $this->redirectToRoute('psc_shop_contact_backend_edit', array('uuid' => $contact->getUuid()));
        }

        return array(
            'form' => $form->createView(),
            'contact' => $contact,
            'contactAddress' => $contactAddress

        );
    }

    /**
     * remove
     *
     * @Route("/edit/delete/{uuid}", name="psc_shop_contact_backend_address_detail_delete")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        $uuid
    ) {

        /** @var ContactAddress $contactAddress */
        $contactAddress = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
            ->findOneBy(array('uuid' => $uuid));
/** @var \PSC\Shop\EntityBundle\Document\ContactAddress $contactDoc */
        $contactAddressDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\ContactAddress')
            ->findOneBy(array('uid' => (string)$contactAddress->getUid()));
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        $uuid = $contactAddress->getContact()->getUuid();
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $email = $contactAddress->getContact()->getEmail();
                $entityManager->remove($contactAddress);
                $entityManager->flush();
                if ($contactAddressDoc) {
                    $documentManager->remove($contactAddressDoc);
                    $documentManager->flush();
                }

                $session->getFlashBag()->add('success', 'Address has been deleted!');
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopContactBundle::class, $uuid, "Address has been deleted", ['contact' => $email]);
                return $this->redirectToRoute('psc_shop_contact_backend_edit', array('uuid' => $uuid));
            }
            return $this->redirectToRoute('psc_shop_contact_backend_edit', array('uuid' => $uuid));
        }


        return array(
            'address' => $contactAddress,
            'contact' => $contactAddress->getContact(),
            'form' => $form->createView()
        );
    }
}
