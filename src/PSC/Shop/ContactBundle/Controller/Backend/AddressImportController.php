<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PSC\Shop\ContactBundle\Form\Backend\AddressImport\Step1Type;
use PSC\Shop\ContactBundle\Form\Backend\AddressImport\Step2Type;
use PSC\Shop\ContactBundle\Form\Backend\General\ContactAddressType;
use PSC\Shop\ContactBundle\Form\Backend\General\ContactType;
use PSC\Shop\ContactBundle\Form\Backend\General\AnonymType;
use PSC\Shop\ContactBundle\Session\AddressImport;
use PSC\Shop\EntityBundle\Document\Shop;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * DetailController fürs Backend
 *
 * @package    PSC\Shop\Contact\Backend
 * @subpackage Controller
 */
class AddressImportController extends AbstractController
{
    /**
     * Delete Seite
     *
     * @Route("/address/import/step1/{uuid}", name="psc_shop_contact_backend_address_import_step1")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param string $uuid
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function step1Action(
        Request $request,
        EntityManagerInterface $entityManager,
        $uuid = ""
    ) {

        /** @var Contact $contact */
        $contact = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->findOneBy(array('uuid' => $uuid));
        $form = $this->createForm(Step1Type::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
        /** @var UploadedFile $file */
            $file = $form['file']->getData();
            $file->move(sys_get_temp_dir(), $file->getClientOriginalName());
            $ai = new AddressImport();
            $ai->setFilePath(sys_get_temp_dir() . '/' . $file->getClientOriginalName());
            $columns = [];
            $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($ai->getFilePath());
            $firstRow = $spreadsheet->getActiveSheet()
                ->rangeToArray('A' . 1 .
                    ':' .
                    $spreadsheet->getActiveSheet()->getHighestColumn() . 1);
        /** @var Cell $cell */
            foreach ($firstRow[0] as $cell) {
                if ($cell != "") {
                        $columns[] = ['key' => $cell, 'value' => ''];
                }
            }

                    $ai->setColumns($columns);
            $request->getSession()->set('addressImport', $ai);
            return $this->redirectToRoute("psc_shop_contact_backend_address_import_step2", ['uuid' => $uuid]);
        }

        return array(
            'contact' => $contact,
            'form' => $form->createView()
        );
    }

    /**
     * Delete Seite
     *
     * @Route("/address/import/step2/{uuid}", name="psc_shop_contact_backend_address_import_step2")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param string $uuid
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function step2Action(
        Request $request,
        EntityManagerInterface $entityManager,
        $uuid = ""
    ) {
        /** @var Contact $contact */
        $contact = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->findOneBy(array('uuid' => $uuid));
/** @var AddressImport $ai */
        $ai = $request->getSession()->get('addressImport');
        $form = $this->createForm(Step2Type::class, ['mapping' => $ai->getColumns() ,'overwrite' => $ai->isOverwrite(), 'ignoreFirstLine' => $ai->isIgnoreFirstLine()]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ai->setColumns($form['mapping']->getData());
            $ai->setIgnoreFirstLine($form['ignoreFirstLine']->getData());
            $ai->setOverwrite($form['overwrite']->getData());
            $request->getSession()->set('addressImport', $ai);
            return $this->redirectToRoute("psc_shop_contact_backend_address_import_step3", ['uuid' => $uuid]);
        }

        return [
            'contact' => $contact,
            'form' => $form->createView()
        ];
    }

    /**
     * Step3 Seite
     *
     * @Route("/address/import/step3/{uuid}", name="psc_shop_contact_backend_address_import_step3")
     * @Template()
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param string $uuid
     * @return array|View|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function step3Action(
        Request $request,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        $uuid = ""
    ) {
        /** @var Contact $contact */
        $contact = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->findOneBy(array('uuid' => $uuid));
/** @var AddressImport $ai */
        $ai = $request->getSession()->get('addressImport');
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($ai->getFilePath());
                $activeSheet = $spreadsheet->getActiveSheet();
                $columns = $ai->getColumns();
                foreach ($activeSheet->getRowIterator() as $row) {
                    if ($ai->isIgnoreFirstLine() && $row->getRowIndex() == 1) {
                        continue;
                    }

                    /** @var ContactAddress $contactAddress */
                    $contactAddress = new ContactAddress();
                    $contactAddress->setInstallId($contact->getInstallId());
                    $contactAddress->setContact($contact);
                    $i = 0;
                    foreach ($row->getCellIterator() as $cell) {
                        if (isset($columns[$i]) && $columns[$i]['value'] !== null) {
                            $field = 'set' . $columns[$i]['value'];
                            $contactAddress->$field($cell->getFormattedValue());
                        }

                        $i++;
                    }

                    if ($contactAddress->getFirstname() == "" && $contactAddress->getLastname() == "" && $contactAddress->getEmail() == "") {
                        continue;
                    }

                    if ($ai->isOverwrite()) {
                        $cat = $entityManager
                        ->getRepository('PSC\Shop\EntityBundle\Entity\ContactAddress')
                        ->findOneBy(array('email' => $contactAddress->getEmail(), 'contact' => $contact, 'type' => $contactAddress->getType()));
                        if ($cat) {
                            foreach ($columns as $column) {
                                if ($column['value'] !== null) {
                                    $seter = 'set' . $column['value'];
                                    $geter = 'get' . $column['value'];
                                    $cat->$seter($contactAddress->$geter());
                                }
                            }

                            $entityManager->persist($cat);
                            $entityManager->flush();
                        } else {
                            $entityManager->persist($contactAddress);
                            $entityManager->flush();
                            $contactAddressDoc = new \PSC\Shop\EntityBundle\Document\ContactAddress();
                            $contactAddressDoc->setKundenNr($contactAddress->getKundenNr());
                            $contactAddressDoc->setUid($contactAddress->getUid());
                            $documentManager->persist($contactAddressDoc);
                            $documentManager->flush();
                        }
                    } else {
                        $entityManager->persist($contactAddress);
                        $entityManager->flush();
                        $contactAddressDoc = new \PSC\Shop\EntityBundle\Document\ContactAddress();
                        $contactAddressDoc->setKundenNr($contactAddress->getKundenNr());
                        $contactAddressDoc->setUid($contactAddress->getUid());
                        $documentManager->persist($contactAddressDoc);
                        $documentManager->flush();
                    }
                }
            }

            $request->getSession()->remove("addressImport");
            return $this->redirectToRoute("psc_shop_contact_backend_edit", ['uuid' => $uuid]);
        }

        return [
            'contact' => $contact,
            'form' => $form->createView()
        ];
    }
}
