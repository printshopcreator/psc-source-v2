<?php

namespace PSC\Shop\ContactBundle\Dto\Exists;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    /**
     * @var bool
     *
     * @OA\Property(type="boolean")
     */
    public string $exists;
}
