<?php

namespace PSC\Shop\ContactBundle\Dto\Address\GetAllForType;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Shop\ContactBundle\Model\Address;

class Output
{
    /**
     * @var Address[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\ContactBundle\Model\Address::class)))
     */
    public array $data = [];
}
