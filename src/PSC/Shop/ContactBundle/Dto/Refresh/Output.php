<?php

namespace PSC\Shop\ContactBundle\Dto\Refresh;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $token;
}
