<?php

namespace PSC\Shop\ContactBundle\Dto\ExistsWithPassword;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Input
{
    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $username = "";

    /**
     * @var string
     *
     * @OA\Property(type="string")
     */
    public string $password = "";
}
