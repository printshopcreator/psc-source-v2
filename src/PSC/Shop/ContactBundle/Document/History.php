<?php

namespace PSC\Shop\ContactBundle\Document;

use PSC\Shop\EntityBundle\Entity\contactObj;
use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="contactObj_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $contactObj;

    function __construct(string $contactObj = "")
    {
        parent::__construct();
        $this->contactObj = $contactObj;
    }

    public function getContactObj()
    {
        return $this->contactObj;
    }

    public function setContactObj(string $contactObj)
    {
        $this->contactObj = $contactObj;
    }

    public function getReferenceField(): string
    {
        return 'contactObj';
    }
}
