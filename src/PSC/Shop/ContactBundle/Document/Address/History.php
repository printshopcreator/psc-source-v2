<?php

namespace PSC\Shop\ContactBundle\Document\Address;

use PSC\Shop\EntityBundle\Entity\addressObj;
use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="addressObj_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $addressObj;

    function __construct(string $addressObj = "")
    {
        parent::__construct();
        $this->addressObj = $addressObj;
    }

    public function getAddressObj()
    {
        return $this->addressObj;
    }

    public function setAddressObj(string $addressObj)
    {
        $this->addressObj = $addressObj;
    }

    public function getReferenceField(): string
    {
        return 'addressObj';
    }
}
