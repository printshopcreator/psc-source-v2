<?php

namespace PSC\Shop\ContactBundle\Document\Queue\Address;

use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/** @EmbeddedDocument */
class Import
{
    /**
     * @var string $path
     *
     * @Field(type="string")
     */
    protected $path;
/**
     * @var string $contacts
     *
     * @Field(type="string")
     */
    protected $contacts;
/**
     * @var string $accounts
     *
     * @Field(type="string")
     */
    protected $accounts;
/**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param string $contacts
     */
    public function setContacts($contacts)
    {
        $this->contacts = $contacts;
    }

    /**
     * @return string
     */
    public function getAccounts()
    {
        return $this->accounts;
    }

    /**
     * @param string $accounts
     */
    public function setAccounts($accounts)
    {
        $this->accounts = $accounts;
    }
}
