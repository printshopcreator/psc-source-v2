<?php

namespace PSC\Shop\ContactBundle\Event\Contact;

use PSC\Shop\QueueBundle\Event\Event;

class UnLock extends Event
{
    /** @var string */
    protected $contact;
    public function getType()
    {
        return 'contact_unlock';
    }

    public function getDescription()
    {
        return 'Kunde freigeschaltet';
    }

    public function getData()
    {
        return array(
            'contact' => $this->contact
        );
    }

    public function setData($data)
    {
        $this->contact = $data['contact'];
    }

    /**
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param string $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }
}
