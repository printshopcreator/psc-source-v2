<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Form\Backend;

use Lexik\Bundle\FormFilterBundle\Filter\Form\Type\TextFilterType;
use Lexik\Bundle\FormFilterBundle\Filter\Query\QueryInterface;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SearchType extends AbstractType
{
    /** @var Session */
    protected $session;
/** @var Shop */
    protected $shop;
    public function __construct(SessionInterface $session, Shop $shop)
    {
        $this->session = $session;
        $this->shop = $shop;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $session = $this->session;
        $builder
            ->setMethod('POST')
            ->add('term', TextFilterType::class, array(
                'data' => $session->get('contact_search_term'),
                'apply_filter' => function (QueryInterface $filterQuery, $field, $values) use ($session) {
                    $session->set('contact_search_term', $values['value']);
                    if (empty($values['value'])) {
                        return null;
                    }

                    $filterQuery->getQueryBuilder()
                        ->leftJoin('contact.shopContact', 's')
                        ->andWhere("
                            s.shop = " . $this->shop->getSelectedShop()->getUid() . " AND
                            (contact.uid LIKE '%" . $values['value'] . "%' OR
                            contact.firstname LIKE '%" . $values['value'] . "%' OR
                            contact.lastname LIKE '%" . $values['value'] . "%' OR
                            contact.zip LIKE '%" . $values['value'] . "%' OR
                            contact.city LIKE '%" . $values['value'] . "%' OR
                            contact.houseNumber LIKE '%" . $values['value'] . "%' OR
                            contact.street LIKE '%" . $values['value'] . "%' OR
                            contact.username LIKE '%" . $values['value'] . "%')
                        ");
                }));
    }

    public function getName()
    {
        return 'item_filter';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection'   => false
        ));
    }
}
