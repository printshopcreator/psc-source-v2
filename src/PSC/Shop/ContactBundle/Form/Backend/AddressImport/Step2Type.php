<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Form\Backend\AddressImport;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class Step2Type extends AbstractType
{
    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ignoreFirstLine', CheckboxType::class, array('label' => 'Ignoriere 1 Zeile beim Import', 'required' => false))
            ->add('overwrite', CheckboxType::class, array('label' => 'Überschreibe vorhandene Einträge', 'required' => false))
            ->add('mapping', CollectionType::class, array(
                // each entry in the array will be an "email" field
                'entry_type' => MappingType::class,
                // these options are passed to each "email" type
                'entry_options' => array(
                    'attr' => array('class' => 'email-box'),
                ),
            ));
    }

    public function getName()
    {
        return 'file';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
