<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Form\Backend\AddressImport;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MappingType extends AbstractType
{
    public function __construct()
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('key', TextType::class, array('label' => 'Spalte'))
            ->add('value', ChoiceType::class, array(
                'label' => 'Feld',
                'placeholder' => 'Nicht verwenden',
                'required' => false,
                'choices' => [
                    'Email' => 'email',
                    'Vorname' => 'firstname',
                    'Nachname' => 'lastname',
                    'Ort' => 'city',
                    'Plz' => 'zip',
                    'Typ (1 Rechnung, 2 Liefer, 3 Absender)' => 'type',
                    'Straße' => 'street',
                    'Hausnummer' => 'houseNumber',
                    'Firma' => 'company',
                    'Firma Zusatz' => 'company2',
                    'Sortierung' => 'pos',
                    'Land' => 'country',
                ]

            ))
            ;
    }

    public function getName()
    {
        return 'file';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
