<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Form\Backend\General;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Payment;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\EntityBundle\Entity\Shipping;
use PSC\Shop\MediaBundle\Form\Type\MediaType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\General;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Status;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    /** @var Field  */
    protected $fields;
/** @var General  */
    protected $shop = null;
    protected $statusService = null;
    protected $formFactory;
    protected EntityManagerInterface $entityManager;
    public function __construct(EntityManagerInterface $entityManager, Shop $shop, Field $fields, FormFactoryInterface $formFactory, General $general, Status $statusService)
    {
        $this->fields = $fields;
        $this->formFactory = $formFactory;
        $this->general = $general;
        $this->shop = $shop;
        $this->statusService = $statusService;
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tempProducts = [1];
        $tempProductGroups = [1];
        $tempCms = [1];
        $tempPayments = [1];
        $tempShippings = [1];
        foreach ($options['products'] as $product) {
            $tempProducts[] = $product->getUid();
        }

        foreach ($options['productGroups'] as $productgroup) {
            $tempProductGroups[] = $productgroup->getUid();
        }

        foreach ($options['cms'] as $cms) {
            $tempCms[] = $cms->getUid();
        }

        foreach ($options['payments'] as $payment) {
            $tempPayments[] = $payment->getUid();
        }

        foreach ($options['shippings'] as $shipping) {
            $tempShippings[] = $shipping->getUid();
        }

        $payments = $this->entityManager->getRepository(Payment::class)->findBy(['shop' => $this->shop->getSelectedShop()->getId()]);
        $defaultPayments = [];
        foreach ($payments as $payment) {
            $defaultPayments[$payment->getTitle()] = $payment->getUid();
        }

        $shippings = $this->entityManager->getRepository(Shipping::class)->findBy(['shop' => $this->shop->getSelectedShop()->getId()]);
        $defaultShipments = [];
        foreach ($shippings as $shipping) {
            $defaultShipments[$shipping->getTitle()] = $shipping->getUid();
        }


        $builder
            ->add('uuid', TextType::class, array('disabled' => true))
            ->add('username', TextType::class)
            ->add('account', EntityType::class, array(
                'label' => 'Company',
                'class' => 'PSC\Shop\EntityBundle\Entity\Account',
                'choice_label' => 'titleAppendix',
                'choice_value' => 'uid',
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->join('u.shops', 's')
                        ->where('s.uid = :shop')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('payments', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Payment',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($tempPayments) {

                    return $er->createQueryBuilder('u')
                        ->where('(u.shop = :shop OR u.uid in (' . implode(",", $tempPayments) . '))')->andWhere('u.private = 1')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('defaultPayment', ChoiceType::class, ['required' => false, 'label' => 'defaultPayment', 'choices' => $defaultPayments])
            ->add('defaultShipment', ChoiceType::class, ['required' => false, 'label' => 'defaultShipment', 'choices' => $defaultShipments])
            ->add('productGroups', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Productgroup',
                'choice_label' => function (Productgroup $choice, $key, $value) {

                    if ($choice->isEnable()) {
                        return $choice->getTitle();
                    } else {
                        return $choice->getTitle() . ' (disabled)';
                    }
                },
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($tempProductGroups) {

                    return $er->createQueryBuilder('u')
                        ->where('(u.shop = :shop OR u.uid in (' . implode(",", $tempProductGroups) . '))')->andWhere('u.private = 1')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('shippings', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Shipping',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($tempShippings) {

                    return $er->createQueryBuilder('u')
                        ->where('(u.shop = :shop OR u.uid in (' . implode(",", $tempShippings) . '))')->andWhere('u.private = 1')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('productsOrg', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Product',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($tempProducts) {

                    return $er->createQueryBuilder('u')
                        ->where('(u.shop = :shop OR u.uid in (' . implode(",", $tempProducts) . '))')->andWhere('u.private = 1 AND u.originalProduct = 0')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('productsSub', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Product',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($tempProducts) {

                    return $er->createQueryBuilder('u')
                        ->where('(u.shop = :shop OR u.uid in (' . implode(",", $tempProducts) . '))')->andWhere('u.private = 1 AND u.originalProduct != 0')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('cms', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Cms',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) use ($tempCms) {

                    return $er->createQueryBuilder('u')
                        ->where('(u.shop = :shop OR u.uid in (' . implode(",", $tempCms) . '))')->andWhere('u.private = 1')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('collectingOrdersAccounts', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Account',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->join('u.shops', 's')
                        ->where('s.uid = :shop')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))
            ->add('shops', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Shop',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true
            ))
            ->add('rolesForm', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Role',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => true
            ))
            ->add('bankKtoName', TextType::class, ['required' => false, 'label' => 'accountowner'])
            ->add('bankKTO', TextType::class, ['required' => false, 'label' => 'accountnumber'])
            ->add('bankBLZ', TextType::class, ['required' => false, 'label' => 'bankcode'])
            ->add('bankIban', TextType::class, ['required' => false, 'label' => 'Iban'])
            ->add('bankBic', TextType::class, ['required' => false, 'label' => 'BIC'])
            ->add('bankName', TextType::class, ['required' => false, 'label' => 'bankname'])
            ->add('image', MediaType::class, ['required' => false, 'label' => 'pic'])
            ->add('information', TextareaType::class, ['required' => false, 'label' => 'information'])
            ->add('vonwo', TextType::class, ['required' => false, 'label' => 'Fromwhere'])
            ->add('newsletter', CheckboxType::class, ['required' => false, 'label' => 'newsletter'])
            ->add('extraSettings', TextareaType::class, array('required' => false, 'label' => 'Extrasettings'))

            ->add('isSek', CheckboxType::class, ['required' => false, 'label' => 'Candobulkorders'])
            ->add('collectingOrders', CheckboxType::class, ['required' => false, 'label' => 'Approvedforbulkorders'])
            ->add('language', LocaleType::class, ['required' => false, 'label' => 'language'])

            ->add('priceFactor', NumberType::class, array('required' => false, 'label' => 'factor', 'scale' => 5, 'html5' => true,
                'attr' => array(
                    'min' => -0.00001,
                    'max' => 2.00,
                    'step' => 0.00001,
            )))

            ->add('enable', CheckboxType::class, ['required' => false, 'label' => 'active'])
            ->add('virtual', CheckboxType::class, ['required' => false, 'label' => 'virtual'])
            ->add('locked', CheckboxType::class, ['required' => false, 'label' => 'Locked'])
            ->add('showCalc', CheckboxType::class, ['required' => false, 'label' => 'calculationshow'])
            ->add('budget', IntegerType::class, ['required' => false, 'label' => 'Budget'])
            ->add('testCalc', CheckboxType::class, ['required' => false, 'label' => 'calculationtesting'])
            ->add('mwert', CheckboxType::class, ['required' => false, 'label' => 'vat'])
            ->add('kundenNr', TextType::class, ['required' => false, 'label' => 'Customernumber'])
            ->add('calcValue1', TextType::class, ['required' => false, 'label' => 'value1'])
            ->add('calcValue2', TextType::class, ['required' => false, 'label' => 'value2'])

            ->add('company', TextType::class, ['required' => false, 'label' => 'Company'])
            ->add('company2', TextType::class, ['required' => false, 'label' => 'Company'])
            ->add('salutation', ChoiceType::class, [ 'label' => 'salutations', 'choices' => $this->general->getSalutation(), 'required' => false])
            ->add('firstname', TextType::class, ['required' => false, 'label' => 'firstname'])
            ->add('lastname', TextType::class, ['required' => false, 'label' => 'lastname'])
            ->add('street', TextType::class, ['required' => false, 'label' => 'street'])
            ->add('houseNumber', TextType::class, ['required' => false, 'label' => 'housenumber'])
            ->add('zip', TextType::class, ['required' => false, 'label' => 'zip'])
            ->add('city', TextType::class, ['required' => false, 'label' => 'city'])
            ->add('destrict', TextType::class, ['required' => false, 'label' => 'destrict'])
            ->add('state', TextType::class, ['required' => false, 'label' => 'state'])
            ->add('country', TextType::class, ['required' => false, 'label' => 'country'])
            ->add('homepage', TextType::class, ['required' => false, 'label' => 'homepage'])

            ->add('phoneAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('phonePrefix', TextType::class, ['required' => false, 'label' => 'prefix'])
            ->add('phone', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('phoneAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('mobileAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('mobilePrefix', TextType::class, ['required' => false, 'label' => 'prefix'])
            ->add('mobile', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('mobileAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('faxAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('faxPrefix', TextType::class, ['required' => false, 'label' => 'prefix'])
            ->add('fax', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('faxAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('alternativAreaCode', TextType::class, ['required' => false, 'label' => 'Countrycode'])
            ->add('alternativType', TextType::class, ['required' => false, 'label' => 'Typ'])
            ->add('alternativ', TextType::class, ['required' => false, 'label' => 'number'])
            ->add('alternativAppendix', TextType::class, ['required' => false, 'label' => 'extension'])

            ->add('birthday', TextType::class, ['required' => false, 'label' => 'birthday'])
            ->add('kostenstellung', TextType::class, ['required' => false, 'label' => 'kostenstellung'])

            ->add('ustid', TextType::class, ['required' => false, 'label' => 'vatid'])
            ->add('function', TextType::class, ['required' => false, 'label' => 'function'])
            ->add('abteilung', TextType::class, ['required' => false, 'label' => 'department'])
            ->add('position', TextType::class, ['required' => false, 'label' => 'position'])
            ->add('title', TextType::class, ['required' => false, 'label' => 'title'])

            ->add('showOtherOrders', CheckboxType::class, ['required' => false, 'label' => 'Mayseeordersfromothers'])
            ->add('showOtherOrdersAccountFilter', ChoiceType::class, array(
                'choices' => array(
                    'Allunblocked' => 1,
                    'Samecompany' => 2,
                    'Filterselectedcompany' => 3,
                ),
                'label' => 'Companyfilter'
            ))
            ->add('showOtherOrdersAccount', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Account',
                'choice_label' => 'title',
                'choice_value' => 'uid',
                'multiple' => false,
                'expanded' => false,
                'required' => false,
                'label' => 'Chosencompany',
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('a')
                        ->join('a.shops', 's')
                        ->where('s.uid = :shop')->setParameter('shop', $this->shop->getSelectedShop()->getId());
                }
            ))


            ->add('custom1', TextType::class, ['required' => false, 'label' => 'Custom 1'])
            ->add('custom2', TextType::class, ['required' => false, 'label' => 'Custom 2'])
            ->add('custom3', TextType::class, ['required' => false, 'label' => 'Custom 3'])
            ->add('custom4', TextType::class, ['required' => false, 'label' => 'Custom 4'])
            ->add('custom5', TextType::class, ['required' => false, 'label' => 'Custom 5'])
            ->add('custom6', TextType::class, ['required' => false, 'label' => 'Custom 6'])
            ->add('custom7', TextType::class, ['required' => false, 'label' => 'Custom 7'])
            ->add('custom8', TextType::class, ['required' => false, 'label' => 'Custom 8'])
            ->add('custom9', TextType::class, ['required' => false, 'label' => 'Custom 9'])
            ->add('custom10', TextType::class, ['required' => false, 'label' => 'Custom 10'])
            ->add('custom11', TextType::class, ['required' => false, 'label' => 'Custom 11'])
            ->add('custom12', TextType::class, ['required' => false, 'label' => 'Custom 12'])
            ->add('custom13', TextType::class, ['required' => false, 'label' => 'Custom 13'])
            ->add('custom14', TextType::class, ['required' => false, 'label' => 'Custom 14'])
            ->add('custom15', TextType::class, ['required' => false, 'label' => 'Custom 15'])
            ->add('custom16', TextType::class, ['required' => false, 'label' => 'Custom 16'])
            ->add('custom17', TextType::class, ['required' => false, 'label' => 'Custom 17'])
            ->add('custom18', TextType::class, ['required' => false, 'label' => 'Custom 18'])
            ->add('custom19', TextType::class, ['required' => false, 'label' => 'Custom 19'])
            ->add('custom20', TextType::class, ['required' => false, 'label' => 'Custom 20'])
            ->add('custom21', TextType::class, ['required' => false, 'label' => 'Custom 21'])
            ->add('custom22', TextType::class, ['required' => false, 'label' => 'Custom 22'])
            ->add('custom23', TextType::class, ['required' => false, 'label' => 'Custom 23'])
            ->add('custom24', TextType::class, ['required' => false, 'label' => 'Custom 24'])
            ->add('kundenNr', TextType::class, ['required' => false, 'label' => 'Customernumber'])
            ->add('password', TextType::class, array('required' => false, 'disabled' => false, 'label' => 'password'))
            ->add('productionStatus', ChoiceType::class, array(
            'label' => 'Productionstatus',
            'required' => false,
            'choices'  => $this->statusService->getOrderStatusAsArray(),
            'translation_domain' => 'status'
            ));
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup(), FormType::class, null, ['mapped' => false]), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'Save'));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact) as $field) {
                $field->formPreSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact) as $field) {
                $field->formPostSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact) as $field) {
                $field->formPostSubmit($event);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Contact) as $field) {
                $field->formPreSubmit($event);
            }
        });
    }

    public function getName()
    {
        return 'contact';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Contact',
            'products' => [],
            'productGroups' => [],
            'cms' => [],
            'payments' => [],
            'shippings' => [],
            'translation_domain' => 'core_contact_edit'
        ));
    }
}
