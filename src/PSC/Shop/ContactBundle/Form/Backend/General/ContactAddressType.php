<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ContactBundle\Form\Backend\General;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use PSC\System\SettingsBundle\Service\General;

class ContactAddressType extends AbstractType
{
    private General $generalService;
    public function __construct(General $generalService)
    {
        $this->generalService = $generalService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid', TextType::class, array('disabled' => true))
            ->add('firstname', TextType::class, ['required' => false, 'label' => 'firstname'])
            ->add('lastname', TextType::class, ['required' => false, 'label' => 'lastname'])
            ->add('street', TextType::class, ['required' => false, 'label' => 'street'])
            ->add('houseNumber', TextType::class, ['required' => false, 'label' => 'housenumber'])
            ->add('zip', TextType::class, ['required' => false, 'label' => 'zip'])
            ->add('city', TextType::class, ['required' => false, 'label' => 'city'])
            ->add('homepage', TextType::class, ['required' => false, 'label' => 'Homepage'])
            ->add('phone', TextType::class, ['required' => false, 'label' => 'Phonenumber'])
            ->add('mobilPhone', TextType::class, ['required' => false, 'label' => 'mobilenumber'])
            ->add('email', TextType::class, ['required' => false, 'label' => 'Email'])
            ->add('company', TextType::class, ['required' => false, 'label' => 'Company'])
            ->add('company2', TextType::class, ['required' => false, 'label' => 'Company2'])
            ->add('country', TextType::class, ['required' => false, 'label' => 'country'])
            ->add('fax', TextType::class, ['required' => false, 'label' => 'faxnumber'])
            ->add('ustid', TextType::class, ['required' => false, 'label' => 'vatid'])
            ->add('zusatz1', TextType::class, ['required' => false, 'label' => 'additiveone'])
            ->add('zusatz2', TextType::class, ['required' => false, 'label' => 'additivetwo'])
            ->add('pos', TextType::class, ['required' => false, 'label' => 'sort'])
            ->add('salutation', ChoiceType::class, [
              'required' => true,
              'label' => 'salutations',
              'choices' => $this->generalService->getSalutation()
            ])
            ->add('type', ChoiceType::class, ['required' => false, 'label' => 'Art',
                'choices'  => array(
                    'Rechnungsadresse' => 1,
                    'Lieferadresse' => 2,
                    'Absenderadresse' => 3,
                ),
                'placeholder' => false,
            ])
            ->add('kundenNr', TextType::class, ['required' => false, 'label' => 'customernumber'])

            ->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'contactAddress';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\contactAddress',
            'translation_domain' => 'core_contact_detail_create_and_edit'
        ));
    }
}
