<?php

namespace PSC\Shop\ContactBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Shop\ContactBundle\Dto\All\Output;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\ContactBundle\Model\Contact;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\ContactBundle\Form\Backend\SearchType;

class AllByShop extends AbstractController
{
    /**
     * User all by shop
     *
     * @Route("allbyshop/{shop_uuid}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="contact",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ContactBundle\Dto\All\Output::class))
     * )
     * @OA\Tag(name="Contact")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="Bearer")
     */
    public function AllAction(
        Shop $shopService,
        EntityManagerInterface $entityManager,
        Request $request,
        PaginatorInterface $paginator,
        \PSC\Shop\ContactBundle\Transformer\Contact $contactTransformer,
        string $shop_uuid
    ): JsonResponse {

        $output = new Output();

        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Contact');
        $qb = $userRepository->createQueryBuilder('contact')
            ->leftJoin('contact.shops', 'shops')
            ->orderBy('contact.uid', 'desc');

        if ($request->get('term', false)) {
            $term = $request->get('term', false);
            $qb
                        ->leftJoin('contact.shopContact', 's')
                        ->andWhere("
                            (contact.uid LIKE '%" . $term . "%' OR
                            contact.firstname LIKE '%" . $term . "%' OR
                            contact.lastname LIKE '%" . $term . "%' OR
                            contact.zip LIKE '%" . $term . "%' OR
                            contact.city LIKE '%" . $term . "%' OR
                            contact.houseNumber LIKE '%" . $term . "%' OR
                            contact.street LIKE '%" . $term . "%' OR
                            contact.username LIKE '%" . $term . "%')
                        ");
        }

        $qb->andwhere('shops.uuid = :shop_id')
            ->andWhere('(contact.username != :sysadminUser OR contact.username is null)')
            ->setParameter("shop_id", $shop_uuid)
            ->setParameter("sysadminUser", 'sysadmin@printshopcreator.de');

        $pagination = $paginator->paginate(
            $query = $qb->getQuery(),
            $request->query->getInt('page', 1),
            25,
            [
                'defaultSortFieldName' => 'contact.name',
                'defaultSortDirection' => 'DESC'
            ]
        );


        foreach ($pagination->getItems() as $contact) {
            $c = new Contact();
            $contactTransformer->fromEntity($c, $contact);

            $output->data[] = $c;
        }

        return $this->json($output);
    }
}
