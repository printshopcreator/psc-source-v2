<?php

namespace  PSC\Shop\ContactBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Shop\ContactBundle\Dto\Refresh\Output;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;

class Refresh extends AbstractController
{
    /**
     * User exists in database
     *
     * @Route("/refresh", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="contact",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ContactBundle\Dto\Refresh\Output::class))
     * )
     * @OA\Tag(name="Contact")
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     */
    public function refreshAction(JWTTokenManagerInterface $jwtManager): JsonResponse
    {

        $output = new Output();

        $output->token = $jwtManager->create($this->getUser());


        return $this->json($output);
    }
}
