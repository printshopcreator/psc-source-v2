<?php

namespace PSC\Shop\ContactBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Shop\ContactBundle\Dto\Exists\Input;
use PSC\Shop\ContactBundle\Dto\Exists\Output;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Component\ApiBundle\Dto\Shop\Shops;
use PSC\Component\ApiBundle\Model\Shop;
use PSC\Component\ApiBundle\Model\Shop\Domain;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class Exists extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private \PSC\System\SettingsBundle\Service\Shop $shopService;

    public function __construct(EntityManagerInterface $entityManager, \PSC\System\SettingsBundle\Service\Shop $shopService)
    {
        $this->entityManager = $entityManager;
        $this->shopService = $shopService;
    }

    /**
     * User exists in database
     *
     * @Route("/exists", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\ContactBundle\Dto\Exists\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="contact",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ContactBundle\Dto\Exists\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\ContactBundle\Dto\Exists\Input::class))
     * )
     * @OA\Tag(name="Contact")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function existsAction(Input $data): JsonResponse
    {

        $output = new Output();
        $contact = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->getContactByEmailAndShop(
                $data->username,
                $this->shopService->getShopByDomain()->getUID()
            );

        $output->exists = false;
        if ($contact) {
            $output->exists = true;
        }

        return $this->json($output);
    }

    /**
     * User exists in database
     *
     * @Route("/existswithpassword", methods={"POST"})
     * @ParamConverter("data", class="\PSC\Shop\ContactBundle\Dto\ExistsWithPassword\Input", converter="psc_rest.request_body")
     * @OA\Response(
     *     response=200,
     *     description="contact",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ContactBundle\Dto\Exists\Output::class))
     * )
     * @OA\RequestBody(
     *    description="This is a request body",
     *    @Model(type=\PSC\Shop\ContactBundle\Dto\ExistsWithPassword\Input::class))
     * )
     * @OA\Tag(name="Contact")
     * @IsGranted("ROLE_API")
     * @Security(name="ApiKeyAuth")
     */
    public function existswithpasswordAction(\PSC\Shop\ContactBundle\Dto\ExistsWithPassword\Input $data): JsonResponse
    {

        $output = new Output();
        $contact = $this->entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')
            ->getContactByEmailAndShop(
                $data->username,
                $this->shopService->getShopByDomain()->getUID()
            );

        $output->exists = false;
        if ($contact) {
            if ($contact->getContact()->getPassword() == $data->password || password_verify($data->password, $contact->getContact()->getPassword())) {
                $output->exists = true;
            }
        }

        return $this->json($output);
    }
}
