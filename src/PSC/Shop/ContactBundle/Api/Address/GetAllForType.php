<?php

namespace PSC\Shop\ContactBundle\Api\Address;

use Doctrine\ORM\EntityManagerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use OpenApi\Annotations as OA;
use PSC\Shop\ContactBundle\Dto\Address\GetAllForType\Output;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Yaml\Yaml;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Knp\Component\Pager\PaginatorInterface;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdaterInterface;
use PSC\Shop\ContactBundle\Form\Backend\SearchType;
use PSC\Shop\ContactBundle\Model\Address;
use PSC\Shop\ContactBundle\Transformer\Address as TransformerAddress;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\ContactAddress;

class GetAllForType extends AbstractController
{
    /**
     * getUsers address for type
     *
     * @Route("address/getallfortype/{contact}/{type}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="contact",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ContactBundle\Dto\Address\GetAllForType\Output::class))
     * )
     * @OA\Tag(name="Contact")
     * @IsGranted("ROLE_SHOP")
     * @Security(name="Bearer")
     */
    public function AllAction(
        Shop $shopService,
        EntityManagerInterface $entityManager,
        TransformerAddress $transformerAddress,
        int $type = 1,
        string $contact = ""
    ): JsonResponse {

        $output = new Output();

        $selectedShop = $shopService->getSelectedShop();

        $contact = $entityManager->getRepository(Contact::class)->findOneBy(['uuid' => $contact]);

        $addresses = $entityManager->getRepository(ContactAddress::class)->findBy([
            'type' => $type,
            'contact' => $contact
        ]);


        foreach ($addresses as $address) {
            $c = new Address();
            $transformerAddress->fromEntity($c, $address);

            $output->data[] = $c;
        }

        return $this->json($output);
    }
}
