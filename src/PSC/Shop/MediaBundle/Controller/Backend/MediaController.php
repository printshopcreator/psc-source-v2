<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use MongoDB\BSON\ObjectId;
use MongoId;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Document\Folder;
use PSC\Shop\MediaBundle\Form\Backend\FolderType;
use PSC\Shop\MediaBundle\Helper\MediaManager;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * MediaController fürs Backend
 *
 * @package    PSC\Shop\Media
 * @subpackage Controller
 */
class MediaController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/detail/show/{uuid}", defaults={"uuid" = false}, name="psc_shop_media_backend_media_detail")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param MediaManager $mediaManager
     * @param EntityManagerInterface $entityManager
     * @param $uuid
     * @return array
     */
    public function detailAction(Request $request, DocumentManager $documentManager, MediaManager $mediaManager, EntityManagerInterface $entityManager, $uuid)
    {

        $media = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Media')
            ->findOneBy(['_id' => new ObjectId($uuid)]);
        $searchUUID = '%' . $uuid . '%';
        $cms = $entityManager->createQuery('SELECT c
                    FROM PSC\Shop\EntityBundle\Entity\Cms c
                    WHERE c.text LIKE :id')
            ->setParameter("id", $searchUUID)
            ->execute();
        $news = $entityManager->createQuery('SELECT c
                    FROM PSC\Shop\NewsBundle\Entity\News c
                    WHERE c.text LIKE :id')
            ->setParameter("id", $searchUUID)
            ->execute();
        $products = $entityManager->createQuery('SELECT p
                    FROM PSC\Shop\EntityBundle\Entity\Product p
                    WHERE p.description LIKE :id
                    OR p.image1 = :image1
                    OR p.image2 = :image2
                    OR p.image3 = :image3
                    OR p.image4 = :image4
                    OR p.image5 = :image5
                    OR p.image6 = :image6
                    OR p.image7 = :image7
                    OR p.image8 = :image8
                    ')
            ->setParameter("id", $searchUUID)
            ->setParameter("image1", $uuid)
            ->setParameter("image2", $uuid)
            ->setParameter("image3", $uuid)
            ->setParameter("image4", $uuid)
            ->setParameter("image5", $uuid)
            ->setParameter("image6", $uuid)
            ->setParameter("image7", $uuid)
            ->setParameter("image8", $uuid)
            ->execute();
        $productGroups = $entityManager->createQuery('SELECT g
                    FROM PSC\Shop\EntityBundle\Entity\Productgroup g
                    WHERE g.text LIKE :id
                    OR g.image1 = :image1
                    OR g.image2 = :image2')
            ->setParameter("id", $searchUUID)
            ->setParameter("image1", $uuid)
            ->setParameter("image2", $uuid)
            ->execute();
        $contacts = $entityManager->createQuery('SELECT g
                    FROM PSC\Shop\EntityBundle\Entity\Contact g
                    WHERE g.image = :image')
            ->setParameter("image", $uuid)
            ->execute();
        $accounts = $entityManager->createQuery('SELECT g
                    FROM PSC\Shop\EntityBundle\Entity\Account g
                    WHERE g.image = :image')
            ->setParameter("image", $uuid)
            ->execute();
        return array(
            'media' => $media,
            'mediamanager' => $mediaManager,
            'refs' => [
                'contacts' => $contacts,
                'accounts' => $accounts,
                'productGroups' => $productGroups,
                'products' => $products,
                'cms' => $cms,
                'news' => $news,
            ]
        );
    }

    /**
     * remove
     *
     * @Route("/delete/{uuid}", defaults={"uuid" = false}, name="psc_shop_media_backend_media_delete")
     * @Template()
     *
     * @param Request $request
     * @param DocumentManager $documentManager
     * @param $uuid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function deleteAction(Request $request, DocumentManager $documentManager, $uuid)
    {
        $media = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Media')
            ->findOneBy(['_id' => new ObjectId($uuid)]);
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $documentManager->remove($media);
                $documentManager->flush();
                return $this->redirectToRoute('psc_shop_media_backend_folder_show');
            }
            return $this->redirectToRoute('psc_shop_media_backend_folder_show');
        }


        return array(
            'media' => $media,
            'form' => $form->createView()
        );
    }
}
