<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\Cursor;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Iterator\CachingIterator;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Query\Expr;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use MongoDB\BSON\ObjectId;
use MongoDB\BSON\Regex;
use MongoId;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\MediaBundle\Document\Folder;
use PSC\Shop\MediaBundle\Form\Backend\FolderType;
use PSC\Shop\MediaBundle\Form\Backend\SearchType;
use PSC\Shop\MediaBundle\Helper\Media\AbstractMediaHandler;
use PSC\Shop\MediaBundle\Service\MediaManager;
use PSC\System\SettingsBundle\Form\Backend\DeleteType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * FolderController fürs Backend
 *
 * @package    PSC\Shop\Media
 * @subpackage Controller
 */
class FolderController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/list/folder/show/{uuid}/{modal}/{htmlId}", defaults={"uuid" = false, "modal" = false, "htmlId" = false}, name="psc_shop_media_backend_folder_show")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function showAction(
        Request $request,
        \PSC\Shop\MediaBundle\Helper\MediaManager $mediaManager,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        DocumentManager $documentManager,
        PaginatorInterface $paginator,
        $uuid,
        $modal,
        $htmlId
    ) {


        $searchForm = $this->createForm(SearchType::class);
        $session = $request->getSession();
// Check when user switches between thumb -and list view
        $viewMode = $request->query->get('viewMode', 'list-view');
        if ($viewMode && $viewMode == 'thumb-view') {
            $session->set('media-thumb-view', true);
        } elseif ($viewMode && $viewMode == 'list-view') {
            $session->remove('media-thumb-view');
        }

        if ($uuid) {
            $session->set('media-folder-uuid', $uuid);
        } else {
            if ($session->has('media-folder-uuid')) {
                $uuid = $session->get('media-folder-uuid');
                ;
            }
        }

        $type            = $request->get('type');
/** @var AbstractMediaHandler $handler */
        $handler = null;
        if ($type) {
            $handler = $mediaManager->getHandlerForType($type);
        }

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $cat = new Folder();
        $folderForm = $this->createForm(FolderType::class, $cat);
        $folderForm->handleRequest($request);
        $searchForm->handleRequest($request);
        if ($folderForm->isSubmitted() && $folderForm->isValid()) {
            $cat->setIcon($request->get('icon', 'fa-file'));
            $documentManager->persist($cat);
            $documentManager->flush();
        }


        /** @var CachingIterator $folders */
        $folders = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
            ->createQueryBuilder('folder')
            ->sort('title', 'ASC')->getQuery()->execute();
        if (count($folders->toArray()) == 0) {
            $this->createDefaultFolder($documentManager);
        /** @var Folder[] $folders */
            $folders = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
                ->createQueryBuilder('folder')
                ->sort('title', 'ASC')->getQuery()->execute();
        }


        $tempFolders = array();
        foreach ($folders as $cat) {
            $count = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->createQueryBuilder('media')
                ->field('folder.$id')->equals(new ObjectId($cat->getId()))->count()
                ->getQuery()->execute();
            $cat->setMedia($count);
            $tempFolders[] = $cat;
        }

        if (!$uuid) {
            $folders->rewind();
            $uuid = $folders->current()->getId();
        }

        $selectedFolder = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
            ->findOneBy(array('id' => $uuid));
        $searchTerm = $request->query->get('term', '');
        $page = $request->query->getInt('page', 1);
        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchTerm = $searchForm->get('term')->getData();
            $page = 1;
        } else {
            $searchForm->get('term')->setData($searchTerm);
        }

        if ($searchTerm != "") {
            $qb = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->createQueryBuilder('media')
                ->field('media.title')->where('function(){ return ((this.title && this.title.indexOf("' . $searchTerm . '") != -1) || (this.url && this.url.indexOf("' . $searchTerm . '") != -1)); }')
                ->field('folder.$id')->equals(new ObjectId($selectedFolder->getId()))
                ->sort('media.title', 'ASC');
        } else {
            $qb = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->createQueryBuilder('media')
                ->field('folder.$id')->equals(new ObjectId($selectedFolder->getId()))
                ->sort('media.title', 'ASC');
        }

        $pagination = $paginator->paginate($query = $qb->getQuery(), $page, 15);
        $pagination->setParam('term', $searchTerm);
        return array(
            'pagination' => $pagination,
            'mediamanager' => $mediaManager,
            'folders' => $tempFolders,
            'selectedFolder' => $selectedFolder,
            'folderForm' => $folderForm->createView(),
            'modal' => $modal,
            'handler' => $handler,
            'htmlId' => $htmlId,
            'searchForm' => $searchForm->createView(),
            'viewMode' => $viewMode
        );
    }

    /**
     * Default Seite
     *
     * @Route("/list/folder/delete/{uuid}/{modal}/{htmlId}", defaults={"uuid" = false, "modal" = false, "htmlId" = false}, name="psc_shop_media_backend_folder_delete")
     * @Template()
     *
     * @param Request $request
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param DocumentManager $documentManager
     * @param $uuid
     * @param $modal
     * @param $htmlId
     * @return array
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function deleteAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager, $uuid, $modal, $htmlId)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $folder = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
            ->findOneBy(['_id' => new ObjectId($uuid)]);
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $documentManager->remove($folder);
                $documentManager->flush();
                $session = $request->getSession();
                if ($session->has('media-folder-uuid')) {
                    $session->remove('media-folder-uuid');
                }

                return $this->redirectToRoute('psc_shop_media_backend_folder_show');
            }
            return $this->redirectToRoute('psc_shop_media_backend_folder_show');
        }


        return array(
            'folder' => $folder,
            'form' => $form->createView()
        );
    }

    /**
     * @param Request $request
     * @param int     $folderId
     *
     * @Route("/list/folder/create", name="psc_shop_media_backend_folder_sub_create")
     * @Template()
     *
     * @return array
     */
    public function subCreateAction(Request $request, $folderId)
    {
    }

    private function createDefaultFolder(DocumentManager $documentManager)
    {

        $folder = new Folder();
        $folder->setTitle("Bilder");
        $folder->setIcon("fa-file-picture-o");
        $documentManager->persist($folder);
        $folder = new Folder();
        $folder->setTitle("PDF's");
        $folder->setIcon("fa-file-pdf-o");
        $documentManager->persist($folder);
        $folder = new Folder();
        $folder->setTitle("Sonstige");
        $folder->setIcon("fa-file-o");
        $documentManager->persist($folder);
        $documentManager->flush();
    }
}
