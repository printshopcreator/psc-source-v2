<?php

namespace PSC\Shop\MediaBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Knp\Component\Pager\PaginatorInterface;
use MongoDB\BSON\ObjectId;
use MongoId;
use PSC\Shop\MediaBundle\Document\Folder;
use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Form\Backend\FolderType;
use PSC\Shop\MediaBundle\Form\Backend\SearchType;
use PSC\Shop\MediaBundle\Helper\FolderManager;
use PSC\Shop\MediaBundle\Helper\Media\AbstractMediaHandler;
use PSC\Shop\MediaBundle\Helper\MediaManager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * ChooserController.
 */
class ChooserController extends AbstractController
{
    /**
     * @Route("/chooser", name="psc_shop_media_backend_chooser_index")
     *
     * @param Request $request
     *
     * @param Shop $shopService
     * @param DocumentManager $documentManager
     * @return RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function chooserIndexAction(Request $request, Shop $shopService, DocumentManager $documentManager)
    {

        $selectedShop = $shopService->getSelectedShop();
        $session  = $request->getSession();
        $folderId = false;

        $type            = $request->get('type', 'all');
        $cKEditorFuncNum = $request->get('CKEditorFuncNum');
        $linkChooser     = $request->get('linkChooser');

        // Go to the last visited folder
        if ($session->get('last-media-folder')) {
            $selectedFolder = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
                ->findOneBy(array('shop' => (string)$selectedShop->getUid(), 'id' => $session->get('last-media-folder')));
            if ($selectedFolder) {
                $folderId = $session->get('last-media-folder');
            } else {
                $folderId = false;
            }
        }

        if (!$folderId) {
            // Redirect to the first top folder
            /* @var Folder $firstFolder */
            $firstFolder = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
                ->findAll();
            if (count($firstFolder) == 0) {
                $this->createDefaultFolder($documentManager);

                $firstFolder = $documentManager
                    ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
                    ->findAll();
            }
            $folderId    = $firstFolder[0]->getId();
        }

        $params = array(
            'folderId'        => $folderId,
            'type'            => $type,
            'CKEditorFuncNum' => $cKEditorFuncNum,
            'linkChooser'     => $linkChooser
        );

        return $this->redirect($this->generateUrl('psc_shop_media_backend_chooser_show_folder', $params));
    }

    /**
     * @param Request $request
     * @param int     $folderId The folder id
     *
     * @Route("/chooser/{folderId}", name="psc_shop_media_backend_chooser_show_folder")
     * @Template()
     *
     * @return array
     */
    public function chooserShowFolderAction(Request $request, PaginatorInterface $paginator, Shop $shopService, DocumentManager $documentManager, MediaManager $mediaManager, FolderManager $folderManager, $folderId)
    {
        $session = $request->getSession();
        $searchForm = $this->createForm(SearchType::class);
        $cat = new Folder();
        $folderForm = $this->createForm(FolderType::class, $cat);

        $folderForm->handleRequest($request);
        $searchForm->handleRequest($request);
        if ($folderForm->isSubmitted() && $folderForm->isValid()) {
            $cat->setIcon($request->get('icon', 'fa-file'));
            $documentManager->persist($cat);
            $documentManager->flush();
        }


        $type            = $request->get('type');
        $cKEditorFuncNum = $request->get('CKEditorFuncNum');
        $linkChooser     = $request->get('linkChooser');

        // Remember the last visited folder in the session
        $session->set('last-media-folder', $folderId);

        // Check when user switches between thumb -and list view
        $viewMode = $request->query->get('viewMode');
        if ($viewMode && $viewMode == 'thumb-view') {
            $session->set('media-thumb-view', true);
        } elseif ($viewMode && $viewMode == 'list-view') {
            $session->remove('media-thumb-view');
        }

        /* @var Folder $selectedFolder */
        $selectedFolder = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
            ->findOneBy(array('id' => $session->get('last-media-folder')));

        /** @var AbstractMediaHandler $handler */
        $handler = null;
        if ($type) {
            $handler = $mediaManager->getHandlerForType($type);
        }

        $linkChooserLink = null;
        if (!empty($linkChooser)) {
            $params = array();
            if (!empty($cKEditorFuncNum)) {
                $params['CKEditorFuncNum'] = $cKEditorFuncNum;
                $routeName                 = 'KunstmaanNodeBundle_ckselecturl';
            } else {
                $routeName = 'KunstmaanNodeBundle_selecturl';
            }
            $linkChooserLink = $this->generateUrl($routeName, $params);
        }

        $searchTerm = $request->query->get('term', '');
        $page = $request->query->getInt('page', 1);

        if ($searchForm->isSubmitted() && $searchForm->isValid()) {
            $searchTerm = $searchForm->get('term')->getData();
            $page = 1;
        } else {
            $searchForm->get('term')->setData($searchTerm);
        }

        if ($searchTerm != "") {
            $qb = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->createQueryBuilder('media')
                ->field('media.title')->where('function(){ return ((this.title && this.title.indexOf("' . $searchTerm . '") != -1) || (this.url && this.url.indexOf("' . $searchTerm . '") != -1)); }')
                ->field('folder.$id')->equals(new ObjectId($selectedFolder->getId()))
                ->sort('media.title', 'ASC');
        } else {
            $qb = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->createQueryBuilder('media')
                ->field('folder.$id')->equals(new ObjectId($selectedFolder->getId()))
                ->sort('media.title', 'ASC');
        }


        $pagination = $paginator->paginate(
            $query = $qb->getQuery(),
            $page,
            15
        );

        $pagination->setParam('term', $searchTerm);

        /** @var Folder[] $folders */
        $folders = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
            ->createQueryBuilder('folder')
            ->sort('title', 'ASC')->getQuery()->execute();

        $tempFolders = array();
        foreach ($folders as $cat) {
            $count = $documentManager
                ->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->createQueryBuilder('media')
                ->field('folder.$id')->equals(new ObjectId($cat->getId()))
                ->count()->getQuery()->execute();
            $cat->setMedia($count);
            $tempFolders[] = $cat;
        }

        $viewVariabels = array(
            'cKEditorFuncNum' => $cKEditorFuncNum,
            'linkChooser'     => $linkChooser,
            'linkChooserLink' => $linkChooserLink,
            'mediamanager'    => $mediaManager,
            'folders'         => $tempFolders,
            'foldermanager'   => $folderManager,
            'handler'         => $handler,
            'type'            => $type,
            'selectedFolder'  => $selectedFolder,
            'searchForm' => $searchForm->createView(),
            'adminlist'       => $pagination,
            'folderForm' => $folderForm->createView()
        );

        /* generate all forms */
        $forms = array();

        foreach ($mediaManager->getFolderAddActions() as $addAction) {
            //$forms[$addAction['type']] = $this->createTypeFormView($mediaManager, $addAction['type']);
        }

        $viewVariabels['forms'] = $forms;

        return $viewVariabels;
    }

    /**
     * @param MediaManager $mediaManager
     * @param String       $type
     *
     * @return \Symfony\Component\Form\FormView
     */
    private function createTypeFormView(MediaManager $mediaManager, $type)
    {
        $handler = $mediaManager->getHandlerForType($type);
        $media   = new Media();
        $helper  = $handler->getFormHelper($media);

        return $this->createForm($handler->getFormType(), $helper, $handler->getFormTypeOptions())->createView();
    }

    private function createDefaultFolder(DocumentManager $documentManager)
    {
        $folder = new Folder();
        $folder->setTitle("Bilder");
        $folder->setIcon("fa-file-picture-o");

        $documentManager->persist($folder);

        $folder = new Folder();
        $folder->setTitle("PDF's");
        $folder->setIcon("fa-file-pdf-o");

        $documentManager->persist($folder);

        $folder = new Folder();
        $folder->setTitle("Sonstige");
        $folder->setIcon("fa-file-o");

        $documentManager->persist($folder);

        $documentManager->flush();
    }
}
