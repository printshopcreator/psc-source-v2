<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\BSON\ObjectId;
use MongoId;
use Monolog\Handler\Mongo;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\MediaBundle\Document\Folder;
use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Form\Backend\FolderType;
use PSC\Shop\MediaBundle\Form\Backend\FileType;
use PSC\Shop\MediaBundle\Helper\MediaManager;
use PSC\Shop\MediaBundle\Service\Pool;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * UploadController fürs Backend
 *
 * @package    PSC\Shop\Media
 * @subpackage Controller
 */
class UploadController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/upload/create/{provider}", defaults={"provider" = false}, name="psc_shop_media_backend_upload_create")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function createAction(Request $request = null, Pool $pool)
    {

        if (!$request->get('provider') && $request->isMethod('get')) {
            return [
                'providers' => $pool->getProvidersByContext($request->get('context', $pool->getDefaultContext())),
                'action' => 'create',
            ];
        }

        return array(

        );
    }

    /**
     * swap
     *
     * @Route("/upload/swap/{folder}/{media}", defaults={"uuid" = false}, name="psc_shop_media_backend_upload_swap")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param DocumentManager $documentManager
     * @param MediaManager $mediaManager
     * @param $folder
     * @param $media
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     */
    public function swapAction(Request $request, SessionInterface $session, DocumentManager $documentManager, MediaManager $mediaManager, $folder, $media)
    {
        $media = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Media')
            ->findOneBy(['_id' => new ObjectId($media)]);
        $handler      = $mediaManager->getHandlerForType('pdf');
        $helper       = $handler->getFormHelper($media);
        $form = $this->createForm($handler->getFormType(), $helper);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
        //$media = $helper->getMedia();
            $documentManager->detach($media);
            $documentManager->persist($media);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Media \'' . $media->getTitle() . '\' has been swapped!');
            return $this->redirectToRoute('psc_shop_media_backend_folder_show', array('uuid' => $folder, 'modal' => false));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * Add Seite
     *
     * @Route("/upload/add/{uuid}/{modal}/{htmlId}", defaults={"uuid" = false, "modal" = false, "htmlId" = false}, name="psc_shop_media_backend_upload_add")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param SessionInterface $session
     * @param DocumentManager $documentManager
     * @param MediaManager $mediaManager
     * @param $uuid
     * @param $modal
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function addAction(Request $request, Shop $shopService, SessionInterface $session, DocumentManager $documentManager, MediaManager $mediaManager, $uuid, $modal)
    {

        $selectedShop = $shopService->getSelectedShop();
        $selectedFolder = $documentManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Folder')
            ->findOneBy(array('id' => $uuid));
        $handler      = $mediaManager->getHandlerForType('pdf');
        $media        = new Media();
        $helper       = $handler->getFormHelper($media);
        $form = $this->createForm($handler->getFormType(), $helper);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $media = $helper->getMedia();
            $media->setFolder($selectedFolder);
            $media->setShop($selectedShop->getUid());
            $documentManager->persist($media);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Media \'' . $media->getTitle() . '\' has been created!');
            return $this->redirectToRoute('psc_shop_media_backend_folder_show', array('uuid' => $uuid, 'modal' => $modal));
        }

        return array(
            'form' => $form->createView()
        );
    }
}
