<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;
use Doctrine\ODM\MongoDB\Mapping\Annotations\ReferenceOne;
use Imagine\Image\Box;
use PSC\Shop\MediaBundle\Model\MediaInterface;

/**
 * @Document
 */
class Media implements MediaInterface
{
    /**
     * @Id
     */
    protected $id;
/**
     * @var string $shop
     *
     * @Field(type="string")
     */
    protected $shop;
/**
     * @var string
     *
     * @Field(type="string")
     */
    protected $title;
/**
     * @var string
     *
     * @Field(type="string")
     */
    protected $description;
/**
     * @var string
     *
     * @Field(type="string")
     */
    protected $copyright;
/**
     * @var string
     *
     * @Field(type="string"))
     */
    protected $location;
/**
     * @var string
     *
     * @Field(type="string")
     */
    protected $contentType;
/**
     * @var array
     *
     * @Field(type="hash")
     */
    protected $metadata = array();
/**
     * @var \DateTime
     *
     * @Field(type="date")
     */
    protected $createdAt;
/**
     * @var \DateTime
     *
     * @Field(type="date")
     */
    protected $updatedAt;
/**
     * @var Folder
     *
     * @ReferenceOne(targetDocument="PSC\Shop\MediaBundle\Document\Folder")
     */
    protected $folder;
/**
     * @var mixed
     */
    protected $content;
/**
     * @var int
     *
     * @Field(type="int")
     */
    protected $filesize;
/**
     * @var string
     *
     * @Field(type="string")
     */
    protected $url;
/**
     * @var string
     *
     * @Field(type="string")
     */
    protected $originalFilename;
/**
     * @var bool
     *
     * @Field(type="bool")
     */
    protected $deleted;
/**
     * @Field(type="string")
     */
    protected $providerName;
/**
     * @Field(type="int")
     */
    protected $providerStatus;
/**
     * @Field(type="string")
     */
    protected $providerReference;
/**
     * @Field(type="hash")
     */
    protected $providerMetadata = [];
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
        $this->setUpdatedAt(new \DateTime());
        $this->deleted = false;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param string $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }
    /**
     * @return string
     */
    public function getFileSize()
    {
        $size = $this->filesize;
        if ($size < 1024) {
            return $size . "b";
        } else {
            $help = $size / 1024;
            if ($help < 1024) {
                return round($help, 1) . "kb";
            } else {
                return round(($help / 1024), 1) . "mb";
            }
        }
    }

    /**
     * @return int
     */
    public function getFileSizeBytes()
    {
        return $this->filesize;
    }

    /**
     * @param int $filesize
     *
     * @return Media
     */
    public function setFileSize($filesize)
    {
        $this->filesize = $filesize;
        return $this;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return Media
     */
    public function setLocation($location)
    {
        $this->location = $location;
        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set contentType
     *
     * @param string $contentType
     *
     * @return Media
     */
    public function setContentType($contentType)
    {
        $this->contentType = $contentType;
        return $this;
    }

    /**
     * Get contentType
     *
     * @return string
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Get contentType
     *
     * @return string
     */
    public function getContentTypeShort()
    {
        $contentType = $this->contentType;
        $array       = explode("/", $contentType);
        $contentType = end($array);
        return $contentType;
    }

    /**
     * Set metadata
     *
     * @param array $metadata
     *
     * @return Media
     */
    public function setMetadata($metadata)
    {
        $this->metadata = $metadata;
        return $this;
    }

    /**
     * Get metadata
     *
     * @return array
     */
    public function getMetadata()
    {
        return $this->metadata;
    }

    /**
     * Set the specified metadata value
     *
     * @param string $name
     * @param mixed  $value
     *
     * @return Media
     */
    public function setMetadataValue($name, $value)
    {
        $metadata = $this->getProviderMetadata();
        $metadata[$name] = $value;
        $this->setProviderMetadata($metadata);
    }

    /**
     * {@inheritdoc}
     */
    public function getMetadataValue($name, $default = null)
    {
        $metadata = $this->getProviderMetadata();
        return isset($metadata[$name]) ? $metadata[$name] : $default;
    }

    /**
     * {@inheritdoc}
     */
    public function unsetMetadataValue($name)
    {
        $metadata = $this->getProviderMetadata();
        unset($metadata[$name]);
        $this->setProviderMetadata($metadata);
    }

    /**
     * {@inheritdoc}
     */
    public function setCreatedAt(\DateTime $createdAt = null)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * {@inheritdoc}
     */
    public function setUpdatedAt(\DateTime $updatedAt = null)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set content
     *
     * @param mixed $content
     *
     * @return Media
     */
    public function setContent($content)
    {
        $this->content = $content;
        $this->setUpdatedAt(new \DateTime());
        return $this;
    }

    /**
     * Get content
     *
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     *
     * @return Media
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return Media
     */
    public function setUrl($url)
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string $copyright
     *
     * @return Media
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
        return $this;
    }

    /**
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * @param string $originalFilename
     *
     * @return Media
     */
    public function setOriginalFilename($originalFilename)
    {
        $this->originalFilename = $originalFilename;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalFilename()
    {
        return $this->originalFilename;
    }

    /**
     * @param string $description
     *
     * @return Media
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param Folder $folder
     */
    public function setFolder($folder)
    {
        $this->folder = $folder;
    }

    /**
     * @inheritDoc
     */
    public function setBinaryContent($binaryContent)
    {
        // TODO: Implement setBinaryContent() method.
    }

    /**
     * @inheritDoc
     */
    public function getBinaryContent()
    {
        // TODO: Implement getBinaryContent() method.
    }

    /**
     * @inheritDoc
     */
    public function resetBinaryContent()
    {
        // TODO: Implement resetBinaryContent() method.
    }

    /**
     * @inheritDoc
     */
    public function setName($name)
    {
        // TODO: Implement setName() method.
    }

    /**
     * @inheritDoc
     */
    public function getName()
    {
        // TODO: Implement getName() method.
    }

    /**
     * @inheritDoc
     */
    public function setEnabled($enabled)
    {
        // TODO: Implement setEnabled() method.
    }

    /**
     * @inheritDoc
     */
    public function getEnabled()
    {
        // TODO: Implement getEnabled() method.
    }

    /**
     * {@inheritdoc}
     */
    public function setProviderName($providerName)
    {
        $this->providerName = $providerName;
    }

    /**
     * {@inheritdoc}
     */
    public function getProviderName()
    {
        if ($this->providerName === null) {
            return 'psc.shop.media.provider.image';
        }
        return $this->providerName;
    }

    /**
     * {@inheritdoc}
     */
    public function setProviderStatus($providerStatus)
    {
        $this->providerStatus = $providerStatus;
    }

    /**
     * {@inheritdoc}
     */
    public function getProviderStatus()
    {
        if ($this->providerStatus === null) {
            return MediaInterface::STATUS_OK;
        }
        return $this->providerStatus;
    }

    /**
     * {@inheritdoc}
     */
    public function setProviderReference($providerReference)
    {
        $this->providerReference = $providerReference;
    }

    /**
     * {@inheritdoc}
     */
    public function getProviderReference()
    {
        return $this->providerReference;
    }

    /**
     * {@inheritdoc}
     */
    public function setProviderMetadata(array $providerMetadata = [])
    {
        $this->providerMetadata = $providerMetadata;
    }

    /**
     * {@inheritdoc}
     */
    public function getProviderMetadata()
    {
        return $this->providerMetadata;
    }

    /**
     * @inheritDoc
     */
    public function setWidth($width)
    {
        // TODO: Implement setWidth() method.
    }

    /**
     * @inheritDoc
     */
    public function getWidth()
    {
        // TODO: Implement getWidth() method.
    }

    /**
     * @inheritDoc
     */
    public function setHeight($height)
    {
        // TODO: Implement setHeight() method.
    }

    /**
     * @inheritDoc
     */
    public function getHeight()
    {
        // TODO: Implement getHeight() method.
    }

    /**
     * @inheritDoc
     */
    public function setLength($length)
    {
        // TODO: Implement setLength() method.
    }

    /**
     * @inheritDoc
     */
    public function getLength()
    {
        // TODO: Implement getLength() method.
    }

    /**
     * @inheritDoc
     */
    public function setAuthorName($authorName)
    {
        // TODO: Implement setAuthorName() method.
    }

    /**
     * @inheritDoc
     */
    public function getAuthorName()
    {
        // TODO: Implement getAuthorName() method.
    }

    /**
     * @inheritDoc
     */
    public function setContext($context)
    {
        // TODO: Implement setContext() method.
    }

    /**
     * @inheritDoc
     */
    public function getContext()
    {
        // TODO: Implement getContext() method.
    }

    /**
     * @inheritDoc
     */
    public function setCdnIsFlushable($cdnIsFlushable)
    {
        // TODO: Implement setCdnIsFlushable() method.
    }

    /**
     * @inheritDoc
     */
    public function getCdnIsFlushable()
    {
        // TODO: Implement getCdnIsFlushable() method.
    }

    /**
     * @inheritDoc
     */
    public function setCdnFlushIdentifier($cdnFlushIdentifier)
    {
        // TODO: Implement setCdnFlushIdentifier() method.
    }

    /**
     * @inheritDoc
     */
    public function getCdnFlushIdentifier()
    {
        // TODO: Implement getCdnFlushIdentifier() method.
    }

    /**
     * @inheritDoc
     */
    public function setCdnFlushAt(\DateTime $cdnFlushAt = null)
    {
        // TODO: Implement setCdnFlushAt() method.
    }

    /**
     * @inheritDoc
     */
    public function getCdnFlushAt()
    {
        // TODO: Implement getCdnFlushAt() method.
    }

    /**
     * @inheritDoc
     */
    public function getExtension()
    {
        $providerReference = $this->getProviderReference();
        if (!$providerReference) {
            return null;
        }

        // strips off query strings or hashes, which are common in URIs remote references
        return preg_replace('{(\?|#).*}', '', pathinfo($providerReference, PATHINFO_EXTENSION));
    }

    /**
     * @inheritDoc
     */
    public function setSize($size)
    {
        // TODO: Implement setSize() method.
    }

    /**
     * @inheritDoc
     */
    public function getSize()
    {
        // TODO: Implement getSize() method.
    }

    /**
     * @inheritDoc
     */
    public function setCdnStatus($cdnStatus)
    {
        // TODO: Implement setCdnStatus() method.
    }

    /**
     * @inheritDoc
     */
    public function getCdnStatus()
    {
        // TODO: Implement getCdnStatus() method.
    }

    /**
     * @inheritDoc
     */
    public function getBox()
    {
        // TODO: Implement getBox() method.
    }

    /**
     * @inheritDoc
     */
    public function setGalleryHasMedias($galleryHasMedias)
    {
        // TODO: Implement setGalleryHasMedias() method.
    }

    /**
     * @inheritDoc
     */
    public function getGalleryHasMedias()
    {
        // TODO: Implement getGalleryHasMedias() method.
    }

    /**
     * @inheritDoc
     */
    public function getPreviousProviderReference()
    {
        // TODO: Implement getPreviousProviderReference() method.
    }
}
