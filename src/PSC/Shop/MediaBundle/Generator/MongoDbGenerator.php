<?php

namespace PSC\Shop\MediaBundle\Generator;

use PSC\Shop\MediaBundle\Model\MediaInterface;

class MongoDbGenerator implements GeneratorInterface
{
    /**
     * {@inheritdoc}
     */
    public function generatePath(MediaInterface $media)
    {
        return sprintf('%s/%04s/%s', $media->getContext(), $media->getId(), $media->getOriginalFilename());
    }
}
