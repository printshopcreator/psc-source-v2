<?php

namespace PSC\Shop\MediaBundle\Generator;

use PSC\Shop\MediaBundle\Model\MediaInterface;

interface GeneratorInterface
{
    /**
     * @return string
     */
    public function generatePath(MediaInterface $media);
}
