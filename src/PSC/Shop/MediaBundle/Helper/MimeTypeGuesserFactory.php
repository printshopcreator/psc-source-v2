<?php

namespace PSC\Shop\MediaBundle\Helper;

use PSC\Shop\MediaBundle\Helper\File\SVGMimeTypeGuesser;
use Symfony\Component\Mime\MimeTypes;

class MimeTypeGuesserFactory implements MimeTypeGuesserFactoryInterface
{
    /**
     * Should return a mime type guesser instance, used for file uploads
     *
     * NOTE: If you override this, you'll probably still have to register the SVGMimeTypeGuesser as last guesser...
     *
     * @return MimeTypes
     */
    public function get()
    {
        $mimeTypes = new MimeTypes();

        return $mimeTypes;
    }
}
