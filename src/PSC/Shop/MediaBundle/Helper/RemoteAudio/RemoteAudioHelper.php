<?php

namespace PSC\Shop\MediaBundle\Helper\RemoteAudio;

use PSC\Shop\MediaBundle\Helper\Remote\AbstractRemoteHelper;
use PSC\Shop\MediaBundle\Helper\Remote\RemoteInterface;
use PSC\Shop\MediaBundle\Document\Media;

/**
 * PSC\Shop\MediaBundle\Entity\Audio
 * Class that defines audio in the system
 */
class RemoteAudioHelper extends AbstractRemoteHelper implements RemoteInterface
{
    /**
     * @param Media $media
     */
    public function __construct(Media $media)
    {
        parent::__construct($media);
        $this->media->setContentType(RemoteAudioHandler::CONTENT_TYPE);
    }
}
