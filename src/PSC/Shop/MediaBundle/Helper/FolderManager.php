<?php

namespace PSC\Shop\MediaBundle\Helper;

use Doctrine\ODM\MongoDB\DocumentManager;
use PSC\Shop\MediaBundle\Document\Folder;
use PSC\System\SettingsBundle\Service\Shop;

class FolderManager
{
    /** @var DocumentManager */
    private $mongoDb;

    /** @var Shop */
    private $serviceShop;


    /**
     * @var DocumentManager $repository
     * @var Shop $serviceShop
     */
    public function __construct(DocumentManager $mongoDb, Shop $serviceShop)
    {
        $this->mongoDb = $mongoDb;
        $this->serviceShop = $serviceShop;
    }

    /**
     *
     * @return array|string
     */
    public function getFolderHierarchy()
    {
        $folders = $this->mongoDb->getRepository('PSC\Shop\MediaBundle\Document\Folder')
            ->findBy(array('shop' => (string)$this->serviceShop->getSelectedShop()->getUid()));

        return $folders;
    }

    /**
     * @param Folder $folder
     *
     * @return Folder
     */
    public function getRootFolderFor(Folder $folder)
    {

        return new Folder();
    }

    /**
     * @param Folder $folder
     *
     * @return array
     */
    public function getParentIds(Folder $folder)
    {
        return array();
    }

    /**
     * @param Folder $folder
     *
     * @return array
     */
    public function getParents(Folder $folder)
    {
        return array();
    }
}
