<?php

namespace PSC\Shop\MediaBundle\Helper\RemoteSlide;

use PSC\Shop\MediaBundle\Helper\Remote\AbstractRemoteHelper;
use PSC\Shop\MediaBundle\Helper\Remote\RemoteInterface;
use PSC\Shop\MediaBundle\Document\Media;

/**
 * PSC\Shop\MediaBundle\Entity\Video
 * Class that defines a video in the system
 */
class RemoteSlideHelper extends AbstractRemoteHelper implements RemoteInterface
{
    /**
     * @param Media $media
     */
    public function __construct(Media $media)
    {
        parent::__construct($media);
        $this->media->setContentType(RemoteSlideHandler::CONTENT_TYPE);
    }
}
