<?php

namespace PSC\Shop\MediaBundle\Helper\RemoteVideo;

use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Helper\Remote\AbstractRemoteHelper;
use PSC\Shop\MediaBundle\Helper\Remote\RemoteInterface;

/**
 * PSC\Shop\MediaBundle\Entity\Video
 * Class that defines a video in the system
 */
class RemoteVideoHelper extends AbstractRemoteHelper implements RemoteInterface
{
    /**
     * @param Media $media
     */
    public function __construct(Media $media)
    {
        parent::__construct($media);
        $this->media->setContentType(RemoteVideoHandler::CONTENT_TYPE);
    }
}
