<?php

namespace PSC\Shop\MediaBundle\Helper\Transformer;

interface PreviewTransformerInterface
{
    /**
     * Return the path of the preview file.
     *
     * @param string $absolutePath
     *
     * @return string
     */
    public function getPreviewFilename($absolutePath);
}
