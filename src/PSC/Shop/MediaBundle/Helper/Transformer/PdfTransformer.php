<?php

namespace PSC\Shop\MediaBundle\Helper\Transformer;

class PdfTransformer implements PreviewTransformerInterface
{
    /**
     * Apply the transformer on the absolute path and return an altered version of it.
     *
     * @param string $absolutePath
     *
     * @return string|false
     */
    public function apply($absolutePath)
    {
        $info = pathinfo($absolutePath);

        if (isset($info['extension']) && false !== strpos(strtolower($info['extension']), 'pdf') && file_exists($absolutePath)) {
            // If it doesn't exist yet, extract the first page of the PDF
            $previewFilename = $this->getPreviewFilename($absolutePath);

            if (!file_exists($previewFilename)) {
                exec("convert -background white -alpha remove -density 140 -resample 100 -colorspace sRGB '" . $absolutePath . "[0]' -quality 75 " . $previewFilename);
            }

            $absolutePath = $previewFilename;
        }

        return $absolutePath;
    }

    /**
     * @param string $absolutePath
     *
     * @return string
     */
    public function getPreviewFilename($absolutePath)
    {
        return $absolutePath . '.jpg';
    }
}
