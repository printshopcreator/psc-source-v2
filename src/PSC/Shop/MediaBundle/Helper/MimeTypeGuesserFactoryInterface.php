<?php

namespace PSC\Shop\MediaBundle\Helper;

use Symfony\Component\Mime\MimeTypeGuesserInterface;

interface MimeTypeGuesserFactoryInterface
{
    /**
     * @return MimeTypeGuesserInterface
     */
    public function get();
}
