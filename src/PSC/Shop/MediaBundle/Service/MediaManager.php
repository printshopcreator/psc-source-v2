<?php

namespace PSC\Shop\MediaBundle\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Helper\Media\AbstractMediaHandler;
use Symfony\Component\HttpFoundation\File\File;
use MongoDB\BSON\ObjectId;

/**
 * MediaManager
 */
class MediaManager
{
    /** @var DocumentManager */
    private $mongoDb;

    private $cacheManager;

    /**
     * @var DocumentManager $repository
     */
    public function __construct(DocumentManager $mongoDb, CacheManager $cacheManager)
    {
        $this->mongoDb = $mongoDb;
        $this->cacheManager = $cacheManager;
    }

    public function getMedia($id)
    {
        /** @var Media $media */
        $media = $this->mongoDb->getRepository('PSC\Shop\MediaBundle\Document\Media')
            ->findOneBy(['_id' => new ObjectId($id)]);

        return $media;
    }

    public function retrieveThumbnailPath(Media $media, $mode = 'psc_backend_portal_image')
    {
        /** @var string */
        $resolvedPath = $this->cacheManager->getBrowserPath($media->getUrl(), $mode);

        return $resolvedPath;
    }
}
