<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle;

use PSC\Shop\MediaBundle\DependencyInjection\Compiler\AddProviderCompilerPass;
use PSC\Shop\MediaBundle\DependencyInjection\Compiler\MediaHandlerCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * ShopMediaBundle
 *
 * @package PSC\Shop\Media
 */
class PSCShopMediaBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container A ContainerBuilder instance
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new MediaHandlerCompilerPass());
        $container->addCompilerPass(new AddProviderCompilerPass());
    }
}
