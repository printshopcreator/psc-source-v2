<?php

namespace PSC\Shop\MediaBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;

class ConfigureMenuListener
{
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
    public function __construct(AuthorizationChecker $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $submenu = $menu->addChild('Media', array(
                'route' => 'psc_shop_media_backend_folder_show',
                'extras' => array(
                    'icon' => 'fas fa-upload',
                    'orderNumber' => 19
                )
            ));
        }
    }
}
