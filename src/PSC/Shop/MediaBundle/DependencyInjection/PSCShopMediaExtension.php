<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Yaml\Yaml;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 *
 * @package PSC\Backend\Production
 */
class PSCShopMediaExtension extends Extension implements PrependExtensionInterface
{
    /**
     * Loads a specific configuration.
     *
     * @param array            $configs   An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws InvalidArgumentException When provided tag is not defined in this extension
     *
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('handlers.yml');
        $loader->load('pdf.yml');
        $loader->load('twig.yml');
        $loader->load('provider.yml');
        $loader->load('filesystem.yml');
        $loader->load('media.yml');
        $loader->load('generator.yml');
        $this->configureFilesystemAdapter($container, $config);
        $this->configureCdnAdapter($container, $config);
        $pool = $container->getDefinition('PSC\Shop\MediaBundle\Service\Pool');
        $pool->addMethodCall('setDefaultContext', [$config['default_context']]);
        $strategies = [];
        foreach ($config['contexts'] as $name => $settings) {
            $formats = [];
            foreach ($settings['formats'] as $format => $value) {
                $formats[$name . '_' . $format] = $value;
            }

            $strategies[] = $settings['download']['strategy'];
            $pool->addMethodCall('addContext', [$name, $settings['providers'], $formats, $settings['download']]);
        }

        $container->setParameter('psc.shop.media.admin_format', $config['admin_format']);
        $container->setParameter('psc.shop.media.resizer.simple.adapter.mode', $config['resizer']['simple']['mode']);
        $container->setParameter('psc.shop.media.resizer.square.adapter.mode', $config['resizer']['square']['mode']);
        $this->configureAdapters($container, $config);
        $this->configureProviders($container, $config);
        $this->configureResizers($container, $config);
        $container->setParameter('psc.shop.soundcloud_api_key', false);
        $container->setParameter('psc.shop.remote_video', false);
        $container->setParameter('psc.shop.enable_pdf_preview', true);
        $container->setParameter('psc.shop.blacklisted_extensions', array());
    }


    public function configureCdnAdapter(ContainerBuilder $container, array $config)
    {
        // add the default configuration for the server cdn
        if ($container->hasDefinition('psc.shop.media.cdn.server') && isset($config['cdn']['server'])) {
            $container->getDefinition('psc.shop.media.cdn.server')
                ->replaceArgument(0, $config['cdn']['server']['path'])
            ;
        } else {
            $container->removeDefinition('psc.shop.media.cdn.server');
        }

        if ($container->hasDefinition('psc.shop.media.cdn.fallback') && isset($config['cdn']['fallback'])) {
            $container->getDefinition('psc.shop.media.cdn.fallback')
                ->replaceArgument(0, new Reference($config['cdn']['fallback']['master']))
                ->replaceArgument(1, new Reference($config['cdn']['fallback']['fallback']))
            ;
        } else {
            $container->removeDefinition('psc.shop.media.cdn.fallback');
        }
    }

    public function prepend(ContainerBuilder $container)
    {
        $liipConfig = Yaml::parse(file_get_contents(__DIR__ . '/../Resources/config/imagine_filters.yml'));
        $container->prependExtensionConfig('liip_imagine', $liipConfig['liip_imagine']);
    }

    private function configureAdapters(ContainerBuilder $container, array $config)
    {
        foreach (['gd', 'imagick', 'gmagick'] as $adapter) {
            if ($container->hasParameter('psc.shop.media.adapter.image.' . $adapter . '.class')) {
                $container->register('psc.shop.media.adapter.image.' . $adapter, $container->getParameter('psc.shop.media.adapter.image.' . $adapter . '.class'));
            }
        }
        $container->setAlias('psc.shop.media.adapter.image.default', $config['adapters']['default']);
    }

    public function configureProviders(ContainerBuilder $container, array $config)
    {
        $container->getDefinition('psc.shop.media.provider.image')
            ->replaceArgument(5, array_map('strtolower', $config['providers']['image']['allowed_extensions']))
            ->replaceArgument(6, $config['providers']['image']['allowed_mime_types'])
            ->replaceArgument(7, new Reference($config['providers']['image']['adapter']))
        ;
        $container->getDefinition('psc.shop.media.provider.file')
            ->replaceArgument(5, $config['providers']['file']['allowed_extensions'])
            ->replaceArgument(6, $config['providers']['file']['allowed_mime_types'])
        ;
//$container->getDefinition('sonata.media.provider.youtube')->replaceArgument(7, $config['providers']['youtube']['html5']);
    }

    private function configureResizers(ContainerBuilder $container, array $config)
    {
        if ($container->hasParameter('psc.shop.media.resizer.simple.class')) {
            $class = $container->getParameter('psc.shop.media.resizer.simple.class');
            $definition = new Definition($class, [
                new Reference('psc.shop.media.adapter.image.default'),
                '%psc.shop.media.resizer.simple.adapter.mode%',
                new Reference('psc.shop.media.metadata.proxy'),
            ]);
            $definition->addTag('psc.shop.media.resizer');
            $container->setDefinition('psc.shop.media.resizer.simple', $definition);
        }

        if ($container->hasParameter('psc.shop.media.resizer.square.class')) {
            $class = $container->getParameter('psc.shop.media.resizer.square.class');
            $definition = new Definition($class, [
                new Reference('psc.shop.media.adapter.image.default'),
                '%psc.shop.media.resizer.square.adapter.mode%',
                new Reference('psc.shop.media.metadata.proxy'),
            ]);
            $definition->addTag('psc.shop.media.resizer');
            $container->setDefinition('psc.shop.media.resizer.square', $definition);
        }

        $container->setAlias('psc.shop.media.resizer.default', $config['resizers']['default']);
    }

    public function configureFilesystemAdapter(ContainerBuilder $container, array $config)
    {
        // add the default configuration for the local filesystem
        if ($container->hasDefinition('psc.shop.media.adapter.filesystem.local') && isset($config['filesystem']['local'])) {
            $container->getDefinition('psc.shop.media.adapter.filesystem.local')
                ->addArgument($config['filesystem']['local']['directory'])
                ->addArgument($config['filesystem']['local']['create'])
            ;
        } else {
            $container->removeDefinition('psc.shop.media.adapter.filesystem.local');
        }
    }
}
