<?php

namespace PSC\Shop\MediaBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * MediaHandlerCompilerPass
 */
class MediaHandlerCompilerPass implements CompilerPassInterface
{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param \Symfony\Component\DependencyInjection\ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if ($container->hasDefinition('PSC\Shop\MediaBundle\Helper\MediaManager')) {
            $definition = $container->getDefinition('PSC\Shop\MediaBundle\Helper\MediaManager');

            foreach ($container->findTaggedServiceIds('psc.shop.media.media_handler') as $id => $attributes) {
                $definition->addMethodCall('addHandler', array(new Reference($id)));
            }
        }

        if ($container->hasDefinition('psc.shop.media.icon_font_manager')) {
            $definition = $container->getDefinition('psc.shop.media.icon_font_manager');

            foreach ($container->findTaggedServiceIds('psc.shop.media.icon_font.loader') as $id => $attributes) {
                $definition->addMethodCall('addLoader', array(new Reference($id), $id));
            }
        }

        if (!$container->hasParameter('twig.form.resources')) {
            return;
        }
        $container->setParameter('twig.form.resources', array_merge(
            array('@PSCShopMedia/form/media_image_widget.html.twig', '@PSCShopMedia/form/media_widget.html.twig'),
            $container->getParameter('twig.form.resources')
        ));
    }
}
