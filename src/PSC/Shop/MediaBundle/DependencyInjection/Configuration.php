<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 *
 * @package PSC\Shop\Order
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('psc_shop_media');
        $node = $treeBuilder->getRootNode();
        $node
            ->children()
            ->scalarNode('default_context')->isRequired()->end()
            ->arrayNode('admin_format')
            ->info('Configures the thumbnail preview for the admin')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('width')->defaultValue(200)->end()
            ->scalarNode('height')->defaultValue(null)->end()
            ->scalarNode('quality')->defaultValue(90)->end()
            ->scalarNode('format')->defaultValue('jpg')->end()
            ->scalarNode('constraint')->defaultValue(true)->end()
            ->end()
            ->end()
            ->end()
        ;
        $this->addContextsSection($node);
        $this->addFilesystemSection($node);
        $this->addProvidersSection($node);
        $this->addCdnSection($node);
        $this->addResizerSection($node);
        $this->addAdapterSection($node);
        return $treeBuilder;
    }

    private function addFilesystemSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('filesystem')
            ->children()
            ->arrayNode('local')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('directory')->defaultValue('%kernel.project_dir%/web/uploads/media')->end()
            ->scalarNode('create')->defaultValue(false)->end()
            ->end()
            ->end()

            ->arrayNode('ftp')
            ->children()
            ->scalarNode('directory')->isRequired()->end()
            ->scalarNode('host')->isRequired()->end()
            ->scalarNode('username')->isRequired()->end()
            ->scalarNode('password')->isRequired()->end()
            ->scalarNode('port')->defaultValue(21)->end()
            ->scalarNode('passive')->defaultValue(false)->end()
            ->scalarNode('create')->defaultValue(false)->end()
            ->scalarNode('mode')->defaultValue(\defined('FTP_BINARY') ? FTP_BINARY : false)->end()
            ->end()
            ->end()

            ->arrayNode('s3')
            ->children()
            ->scalarNode('directory')->defaultValue('')->end()
            ->scalarNode('bucket')->isRequired()->end()
            ->scalarNode('accessKey')->isRequired()->end()
            ->scalarNode('secretKey')->isRequired()->end()
            ->scalarNode('create')->defaultValue(false)->end()
            ->scalarNode('storage')
            ->defaultValue('standard')
            ->validate()
            ->ifNotInArray(['standard', 'reduced'])
            ->thenInvalid('Invalid storage type - "%s"')
            ->end()
            ->end()
            ->scalarNode('cache_control')->defaultValue('')->end()
            ->scalarNode('acl')
            ->defaultValue('public')
            ->validate()
            ->ifNotInArray(['private', 'public', 'open', 'auth_read', 'owner_read', 'owner_full_control'])
            ->thenInvalid('Invalid acl permission - "%s"')
            ->end()
            ->end()
            ->scalarNode('encryption')
            ->defaultValue('')
            ->validate()
            ->ifNotInArray(['aes256'])
            ->thenInvalid('Invalid encryption type - "%s"')
            ->end()
            ->end()
            ->scalarNode('region')->defaultValue('s3.amazonaws.com')->end()
            ->scalarNode('version')->defaultValue('latest')->end()
            ->enumNode('sdk_version')
            ->values([2, 3])
            ->defaultValue(2)
            ->end()
            ->arrayNode('meta')
            ->useAttributeAsKey('name')
            ->prototype('scalar')
            ->end()
            ->end()
            ->end()
            ->end()

            ->arrayNode('mogilefs')
            ->children()
            ->scalarNode('domain')->isRequired()->end()
            ->arrayNode('hosts')
            ->prototype('scalar')->end()
            ->isRequired()
            ->end()
            ->end()
            ->end()

            ->arrayNode('replicate')
            ->children()
            ->scalarNode('master')->isRequired()->end()
            ->scalarNode('slave')->isRequired()->end()
            ->end()
            ->end()
            ->arrayNode('openstack')
            ->children()
            ->scalarNode('url')->isRequired()->end()
            ->arrayNode('secret')
            ->children()
            ->scalarNode('username')->isRequired()->end()
            ->scalarNode('password')->isRequired()->end()
            ->end()
            ->end()
            ->scalarNode('region')->end()
            ->scalarNode('containerName')->defaultValue('media')->end()
            ->scalarNode('create_container')->defaultValue(false)->end()
            ->end()
            ->end()
            ->arrayNode('rackspace')
            ->children()
            ->scalarNode('url')->isRequired()->end()
            ->arrayNode('secret')
            ->children()
            ->scalarNode('username')->isRequired()->end()
            ->scalarNode('apiKey')->isRequired()->end()
            ->end()
            ->end()
            ->scalarNode('region')->isRequired()->end()
            ->scalarNode('containerName')->defaultValue('media')->end()
            ->scalarNode('create_container')->defaultValue(false)->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
        ;
    }

    private function addCdnSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('cdn')
            ->children()
            ->arrayNode('server')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('path')->defaultValue('/uploads/media')->end()
            ->end()
            ->end()

            ->arrayNode('panther')
            ->children()
            ->scalarNode('path')
            ->info('e.g. http://domain.pantherportal.com/uploads/media')
            ->isRequired()
            ->end()
            ->scalarNode('site_id')->isRequired()->end()
            ->scalarNode('password')->isRequired()->end()
            ->scalarNode('username')->isRequired()->end()
            ->end()
            ->end()

            ->arrayNode('cloudfront')
            ->children()
            ->scalarNode('path')
            ->info('e.g. http://xxxxxxxxxxxxxx.cloudfront.net/uploads/media')
            ->isRequired()
            ->end()
            ->scalarNode('distribution_id')->isRequired()->end()
            ->scalarNode('key')->isRequired()->end()
            ->scalarNode('secret')->isRequired()->end()
            ->end()
            ->end()

            ->arrayNode('fallback')
            ->children()
            ->scalarNode('master')->isRequired()->end()
            ->scalarNode('fallback')->isRequired()->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
        ;
    }

    private function addResizerSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('resizer')
            ->addDefaultsIfNotSet()
            ->children()
            ->arrayNode('simple')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('mode')->defaultValue('inset')->end()
            ->end()
            ->end()
            ->arrayNode('square')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('mode')->defaultValue('inset')->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->arrayNode('resizers')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('default')->defaultValue('psc.shop.media.resizer.simple')->end()
            ->end()
            ->end()
            ->end()
        ;
    }

    private function addContextsSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('contexts')
            ->useAttributeAsKey('id')
            ->prototype('array')
            ->children()
            ->arrayNode('download')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('strategy')->defaultValue('sonata.media.security.superadmin_strategy')->end()
            ->scalarNode('mode')->defaultValue('http')->end()
            ->end()
            ->end()
            ->arrayNode('providers')
            ->prototype('scalar')
            ->defaultValue([])
            ->end()
            ->end()
            ->arrayNode('formats')
            ->useAttributeAsKey('id')
            ->prototype('array')
            ->children()
            ->scalarNode('width')->defaultValue(null)->end()
            ->scalarNode('height')->defaultValue(null)->end()
            ->scalarNode('quality')->defaultValue(80)->end()
            ->scalarNode('format')->defaultValue('jpg')->end()
            ->scalarNode('constraint')->defaultValue(true)->end()
            ->scalarNode('resizer')->defaultNull()->end()
            ->arrayNode('resizer_options')
            ->info('options directly passed to selected resizer. e.g. {use_crop: true, crop_gravity: center}')
            ->defaultValue([])
            ->useAttributeAsKey('name')
            ->prototype('scalar')
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
            ->end()
        ;
    }

    private function addProvidersSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('providers')
            ->addDefaultsIfNotSet()
            ->children()
            ->arrayNode('file')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('service')->defaultValue('psc.shop.media.provider.file')->end()
            ->scalarNode('resizer')->defaultValue(false)->end()
            ->scalarNode('filesystem')->defaultValue('psc.shop.media.filesystem.local')->end()
            ->scalarNode('cdn')->defaultValue('psc.shop.media.cdn.server')->end()
            ->scalarNode('generator')->defaultValue('psc.shop.media.generator.default')->end()
            ->scalarNode('thumbnail')->defaultValue('psc.shop.media.thumbnail.format')->end()
            ->arrayNode('allowed_extensions')
            ->prototype('scalar')->end()
            ->defaultValue([
                'pdf', 'txt', 'rtf',
                'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx',
                'odt', 'odg', 'odp', 'ods', 'odc', 'odf', 'odb',
                'csv',
                'xml',
            ])
            ->end()
            ->arrayNode('allowed_mime_types')
            ->prototype('scalar')->end()
            ->defaultValue([
                'application/pdf', 'application/x-pdf', 'application/rtf', 'text/html', 'text/rtf', 'text/plain',
                'application/excel', 'application/msword', 'application/vnd.ms-excel', 'application/vnd.ms-powerpoint',
                'application/vnd.ms-powerpoint', 'application/vnd.oasis.opendocument.text', 'application/vnd.oasis.opendocument.graphics', 'application/vnd.oasis.opendocument.presentation', 'application/vnd.oasis.opendocument.spreadsheet', 'application/vnd.oasis.opendocument.chart', 'application/vnd.oasis.opendocument.formula', 'application/vnd.oasis.opendocument.database', 'application/vnd.oasis.opendocument.image',
                'text/comma-separated-values',
                'text/xml', 'application/xml',
                'application/zip', // seems to be used for xlsx document ...
            ])
            ->end()
            ->end()
            ->end()

            ->arrayNode('image')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('service')->defaultValue('psc.shop.media.provider.image')->end()
            ->scalarNode('resizer')->defaultValue('psc.shop.media.resizer.default')->end()
            ->scalarNode('filesystem')->defaultValue('psc.shop.media.filesystem.local')->end()
            ->scalarNode('cdn')->defaultValue('psc.shop.media.cdn.server')->end()
            ->scalarNode('generator')->defaultValue('psc.shop.media.generator.default')->end()
            ->scalarNode('thumbnail')->defaultValue('psc.shop.media.thumbnail.format')->end()
            ->scalarNode('adapter')->defaultValue('psc.shop.media.adapter.image.default')->end()
            ->arrayNode('allowed_extensions')
            ->prototype('scalar')->end()
            ->defaultValue(['jpg', 'png', 'jpeg'])
            ->end()
            ->arrayNode('allowed_mime_types')
            ->prototype('scalar')->end()
            ->defaultValue([
                'image/pjpeg',
                'image/jpeg',
                'image/png',
                'image/x-png',
            ])
            ->end()
            ->end()
            ->end()




            ->end()
            ->end()
            ->end()
        ;
    }

    private function addAdapterSection(ArrayNodeDefinition $node)
    {
        $node
            ->children()
            ->arrayNode('adapters')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('default')->defaultValue('psc.shop.media.adapter.image.gd')->end()
            ->end()
            ->end()
            ->end()
        ;
    }
}
