<?php

namespace PSC\Shop\MediaBundle\Twig\TokenParser;

use PSC\Shop\MediaBundle\Twig\Node\ThumbnailNode;
use Twig\Node\Expression\ArrayExpression;
use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

class ThumbnailTokenParser extends AbstractTokenParser
{
    /**
     * @var string
     */
    protected $extensionName;
/**
     * @param string $extensionName
     */
    public function __construct($extensionName)
    {
        $this->extensionName = $extensionName;
    }

    /**
     * {@inheritdoc}
     */
    public function parse(Token $token)
    {
        $media = $this->parser->getExpressionParser()->parseExpression();
        $this->parser->getStream()->next();
        $format = $this->parser->getExpressionParser()->parseExpression();
// attributes
        if ($this->parser->getStream()->test(Token::NAME_TYPE, 'with')) {
            $this->parser->getStream()->next();
            $attributes = $this->parser->getExpressionParser()->parseExpression();
        } else {
            $attributes = new ArrayExpression(array(), $token->getLine());
        }

        $this->parser->getStream()->expect(Token::BLOCK_END_TYPE);
        return new ThumbnailNode($this->extensionName, $media, $format, $attributes, $token->getLine(), $this->getTag());
    }

    /**
     * {@inheritdoc}
     */
    public function getTag()
    {
        return 'thumbnail';
    }
}
