<?php

namespace PSC\Shop\MediaBundle\Twig\TokenParser;

use PSC\Shop\MediaBundle\Twig\Node\ReportMediaNode;
use Twig\Token;

class ReportMediaParser extends \Twig_TokenParser
{
    /**
     * @var string
     */
    protected $extensionName;
/**
     * @param string $extensionName
     */
    public function __construct($extensionName)
    {
        $this->extensionName = $extensionName;
    }

    /**
     * {@inheritdoc}
     */
    public function parse(Token $token)
    {
        $media = $this->parser->getExpressionParser()->parseExpression();
        $this->parser->getStream()->next();
        $format = $this->parser->getExpressionParser()->parseExpression();
        if ($this->parser->getStream()->test(Token::NAME_TYPE, 'with')) {
            $this->parser->getStream()->next();
            $attributes = $this->parser->getExpressionParser()->parseExpression();
        } else {
            $attributes = new \Twig_Node_Expression_Array(array(), $token->getLine());
        }

        $this->parser->getStream()->expect(Token::BLOCK_END_TYPE);
        return new ReportMediaNode($this->extensionName, $media, $format, $attributes, $token->getLine(), $this->getTag());
    }

    /**
     * {@inheritdoc}
     */
    public function getTag()
    {
        return 'reportMedia';
    }
}
