<?php

namespace PSC\Shop\MediaBundle\Twig\TokenParser;

use PSC\Shop\MediaBundle\Twig\Node\PathNode;
use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

class PathTokenParser extends AbstractTokenParser
{
    /**
     * @var string
     */
    protected $extensionName;
/**
     * @param string $extensionName
     */
    public function __construct($extensionName)
    {
        $this->extensionName = $extensionName;
    }

    /**
     * {@inheritdoc}
     */
    public function parse(Token $token)
    {
        $media = $this->parser->getExpressionParser()->parseExpression();
        $this->parser->getStream()->next();
        $format = $this->parser->getExpressionParser()->parseExpression();
        $this->parser->getStream()->expect(Token::BLOCK_END_TYPE);
        return new PathNode($this->extensionName, $media, $format, $token->getLine(), $this->getTag());
    }

    /**
     * {@inheritdoc}
     */
    public function getTag()
    {
        return 'path';
    }
}
