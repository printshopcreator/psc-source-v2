<?php

namespace PSC\Shop\MediaBundle\Twig\Node;

use Twig\Node\Node;

class MediaNode extends Node
{
    /**
     * @var string
     */
    protected $extensionName;
/**
     * @param string                $extensionName
     * @param \Twig\Node\Expression\AbstractExpression $media
     * @param \Twig\Node\Expression\AbstractExpression $format
     * @param \Twig\Node\Expression\AbstractExpression $attributes
     * @param int                   $lineno
     * @param string                $tag
     */
    public function __construct($extensionName, \Twig\Node\Expression\AbstractExpression $media, \Twig\Node\Expression\AbstractExpression $format, \Twig\Node\Expression\AbstractExpression $attributes, $lineno, $tag = null)
    {
        $this->extensionName = $extensionName;
        parent::__construct(array('media' => $media, 'format' => $format, 'attributes' => $attributes), array(), $lineno, $tag);
    }

    /**
     * {@inheritdoc}
     */
    public function compile(\Twig\Compiler $compiler)
    {
        $compiler
            ->addDebugInfo($this)
            ->write(sprintf("echo \$this->env->getExtension('%s')->media(", $this->extensionName))
            ->subcompile($this->getNode('media'))
            ->raw(', ')
            ->subcompile($this->getNode('format'))
            ->raw(', ')
            ->subcompile($this->getNode('attributes'))
            ->raw(");\n")
        ;
    }
}
