<?php

namespace PSC\Shop\MediaBundle\Twig\Extension;

/**
 * Class FMElfinderExtension.
 */
class SummernoteExtension extends \Twig_Extension
{
    /**
     * @var \Twig_Environment
     */
    protected $twig;
/**
     * @param \Twig_Environment $twig
     */
    public function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public function getFunctions()
    {
        $options = array('is_safe' => array('html'));
        return array(
            new \Twig_SimpleFunction('summernote_mediabundle_init', array($this, 'summernote'), $options),
        );
    }

    /**
     * @param string $instance
     * @param string $selector
     * @param array  $parameters
     *
     * @return mixed
     *
     * @throws \Twig_Error_Runtime
     */
    public function summernote($instance = 'default', $selector = '.summenote', $parameters = array('width' => 900, 'height' => 450, 'title' => 'MediaBundle'))
    {
        if (!is_string($instance)) {
            throw new \Twig_Error_Runtime('The function can be applied to strings only.');
        }
        return $this->twig->render(
            '@PSCShopMedia/summernote/helper/_summernote.html.twig',
            array(
                'instance' => $instance,
                'selector' => $selector,
                'width'    => $parameters['width'],
                'height'   => $parameters['height'],
                'title'    => $parameters['title'],
            )
        );
    }
    /**
     * (non-PHPdoc).
     *
     * @see Twig_ExtensionInterface::getName()
     */
    public function getName()
    {
        return 'summernote_init';
    }
}
