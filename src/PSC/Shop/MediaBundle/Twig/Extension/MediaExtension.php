<?php

namespace PSC\Shop\MediaBundle\Twig\Extension;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use MongoDB\BSON\ObjectId;
use PSC\Shop\MediaBundle\Document\Media;
use PSC\Shop\MediaBundle\Helper\MediaManager;
use PSC\Shop\MediaBundle\Model\MediaInterface;
use PSC\Shop\MediaBundle\Service\Pool;
use PSC\Shop\MediaBundle\Twig\TokenParser\MediaTokenParser;
use PSC\Shop\MediaBundle\Twig\TokenParser\PathTokenParser;
use PSC\Shop\MediaBundle\Twig\TokenParser\ReportMediaParser;
use PSC\Shop\MediaBundle\Twig\TokenParser\ThumbnailTokenParser;
use Twig\Environment;

class MediaExtension extends \Twig\Extension\AbstractExtension
{
    /**
     * @var Pool
     */
    protected $mediaService;
/**
     * @var array
     */
    protected $resources = array();
/**
     * @var ManagerInterface
     */
    protected $mediaManager;
    protected $mongoManager;
/**
     * @var \Twig_Environment
     */
    protected $environment;
/**
     * @param Pool $mediaService
     * @param MediaManager $mediaManager
     * @param DocumentManager $mongoManager
     * @param Environment $environment
     */
    public function __construct(Pool $mediaService, MediaManager $mediaManager, DocumentManager $mongoManager, Environment $environment)
    {
        $this->mediaService = $mediaService;
        $this->mediaManager = $mediaManager;
        $this->mongoManager = $mongoManager;
        $this->environment = $environment;
    }

    /**
     * {@inheritdoc}
     */
    public function getTokenParsers()
    {
        return array(
            new MediaTokenParser(get_called_class()),
            new ThumbnailTokenParser(get_called_class()),
            new PathTokenParser(get_called_class()),
            new ReportMediaParser(get_called_class()),
        );
    }

    /**
     * @param MediaInterface $media
     * @param string         $format
     * @param array          $options
     *
     * @return string
     */
    public function media($media, $format, $options = array())
    {
        $media = $this->getMedia($media);
        if (!$media) {
            return '';
        }

        $provider = $this
            ->getMediaService()
            ->getProvider($media->getProviderName());
        $format = $provider->getFormatName($media, $format);
        $options = $provider->getHelperProperties($media, $format, $options);
        return $this->render($provider->getTemplate('helper_view'), array(
            'media' => $media,
            'format' => $format,
            'options' => $options,
        ));
    }

    public function reportMedia($media, $format, $options = array())
    {
        $media = $this->getMedia($media);
        if (!$media) {
            return '';
        }

        $provider = $this->getMediaService()
            ->getProvider($media->getProviderName());
        $format = $provider->getFormatName($media, $format);
        $format_definition = $provider->getFormat($format);
// build option
        $defaultOptions = array(
            'title' => $media->getName(),
            'alt' => $media->getName(),
        );
        if (isset($format_definition['width'])) {
            $defaultOptions['width'] = $format_definition['width'];
        }
        if (isset($format_definition['height'])) {
            $defaultOptions['height'] = $format_definition['height'];
        }

        $options = array_merge($defaultOptions, $options);
        $options['src'] = $provider->generateAbsolutePath($media, $format);
        if (strpos($options['src'], '/data/www') === false) {
            $options['src'] = '/data/www/new/web' . $options['src'];
        }

        return $this->render($provider->getTemplate('helper_thumbnail'), array(
            'media' => $media,
            'options' => $options,
        ));
    }

    /**
     * Returns the thumbnail for the provided media.
     *
     * @param MediaInterface $media
     * @param string         $format
     * @param array          $options
     *
     * @return string
     */
    public function thumbnail($media, $format, $options = array())
    {
        $media = $this->getMedia($media);
        if (!$media) {
            return '';
        }


        $provider = $this->getMediaService()
            ->getProvider($media->getProviderName());
        $format = $provider->getFormatName($media, $format);
        $format_definition = $provider->getFormat($format);
// build option
        $defaultOptions = array(
            'title' => $media->getName(),
            'alt' => $media->getName(),
        );
        if (isset($format_definition['width'])) {
            $defaultOptions['width'] = $format_definition['width'];
        }
        if (isset($format_definition['height'])) {
            $defaultOptions['height'] = $format_definition['height'];
        }

        $options = array_merge($defaultOptions, $options);
        $options['src'] = $provider->generatePublicUrl($media, $format);
        return $this->render($provider->getTemplate('helper_thumbnail'), array(
            'media' => $media,
            'options' => $options,
        ));
    }

    /**
     * @param string $template
     * @param array  $parameters
     *
     * @return mixed
     */
    public function render($template, array $parameters = array())
    {
        if (!isset($this->resources[$template])) {
            $this->resources[$template] = $this->environment->loadTemplate('@' . $template);
        }

        return $this->resources[$template]->render($parameters);
    }

    /**
     * @param MediaInterface $media
     * @param string         $format
     *
     * @return string
     */
    public function path($media, $format)
    {
        $media = $this->getMedia($media);
        if (!$media) {
            return '';
        }

        $provider = $this->getMediaService()
            ->getProvider($media->getProviderName());
        $format = $provider->getFormatName($media, $format);
        return $provider->generatePublicUrl($media, $format);
    }

    /**
     * @return Pool
     */
    public function getMediaService()
    {
        return $this->mediaService;
    }

    /**
     * @param mixed $media
     *
     * @return Media|null|bool
     * @throws \MongoException
     */
    private function getMedia($media)
    {
        if (strlen($media) > 30) {
            return false;
        }

        if (!$media instanceof MediaInterface && strlen($media) > 0) {
            $media = $this->mongoManager->getRepository('PSC\Shop\MediaBundle\Document\Media')
                ->findOneBy(['id' => new ObjectId($media)]);
        }

        if (!$media instanceof MediaInterface) {
            return false;
        }

        if ($media->getProviderStatus() !== MediaInterface::STATUS_OK) {
            return false;
        }

        return $media;
    }
}
