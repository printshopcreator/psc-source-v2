<?php

namespace PSC\Shop\MediaBundle\Metadata;

use PSC\Shop\MediaBundle\Model\MediaInterface;

/**
 * @final since sonata-project/media-bundle 3.21.0
 */
class NoopMetadataBuilder implements MetadataBuilderInterface
{
    /**
     * {@inheritdoc}
     */
    public function get(MediaInterface $media, $filename)
    {
        return [];
    }
}
