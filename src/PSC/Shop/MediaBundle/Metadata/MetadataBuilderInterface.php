<?php

namespace PSC\Shop\MediaBundle\Metadata;

use PSC\Shop\MediaBundle\Model\MediaInterface;

interface MetadataBuilderInterface
{
    /**
     * Get metadata for media object.
     *
     * @param string $filename
     *
     * @return array
     */
    public function get(MediaInterface $media, $filename);
}
