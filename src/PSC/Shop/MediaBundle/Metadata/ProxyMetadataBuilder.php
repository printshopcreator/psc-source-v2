<?php

namespace PSC\Shop\MediaBundle\Metadata;

use Gaufrette\Adapter\AmazonS3;
use Gaufrette\Adapter\AwsS3;
use PSC\Shop\MediaBundle\Filesystem\Replicate;
use PSC\Shop\MediaBundle\Model\MediaInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @final since sonata-project/media-bundle 3.21.0
 */
class ProxyMetadataBuilder implements MetadataBuilderInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * NEXT_MAJOR: remove the second parameter $map.
     *
     * @param array $map
     */
    public function __construct(ContainerInterface $container, array $map = null)
    {
        $this->container = $container;

        if (null !== $map) {
            @trigger_error(
                'The "map" parameter is deprecated since version 2.4 and will be removed in 4.0.',
                E_USER_DEPRECATED
            );
        }
    }

    /**
     * {@inheritdoc}
     */
    public function get(MediaInterface $media, $filename)
    {
        //get adapter for current media
        if (!$this->container->has($media->getProviderName())) {
            return [];
        }

        if ($meta = $this->getAmazonBuilder($media, $filename)) {
            return $meta;
        }

        if (!$this->container->has('psc.shop.media.metadata.noop')) {
            return [];
        }

        return $this->container->get('psc.shop.media.metadata.noop')->get($media, $filename);
    }

    /**
     * @param string $filename
     *
     * @return array|bool
     */
    protected function getAmazonBuilder(MediaInterface $media, $filename)
    {
        $adapter = $this->container->get($media->getProviderName())->getFilesystem()->getAdapter();

        //handle special Replicate adapter
        if ($adapter instanceof Replicate) {
            $adapterClassNames = $adapter->getAdapterClassNames();
        } else {
            $adapterClassNames = [\get_class($adapter)];
        }

        //for amazon s3
        if ((!\in_array(AmazonS3::class, $adapterClassNames, true) && !\in_array(AwsS3::class, $adapterClassNames, true)) || !$this->container->has('psc.shop.media.metadata.amazon')) {
            return false;
        }

        return $this->container->get('psc.shop.media.metadata.amazon')->get($media, $filename);
    }
}
