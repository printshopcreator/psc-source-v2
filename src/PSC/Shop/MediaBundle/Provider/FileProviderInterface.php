<?php

namespace PSC\Shop\MediaBundle\Provider;

interface FileProviderInterface extends MediaProviderInterface
{
    /**
     * @return string[]
     */
    public function getAllowedExtensions();

    /**
     * @return string[]
     */
    public function getAllowedMimeTypes();
}
