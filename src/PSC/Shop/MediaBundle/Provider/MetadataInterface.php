<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Provider;

interface MetadataInterface
{
    public function getTitle();
    public function getDescription();
    public function getImage();
    public function getDomain();
/**
     * @return array<string, mixed>
     */
    public function getOptions();
/**
     * @param string $name    The option key
     * @param mixed  $default The default value if option not found
     */
    public function getOption($name, $default = null);
}
