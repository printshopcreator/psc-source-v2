<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Resizer;

use Imagine\Image\ImageInterface;
use Imagine\Image\ImagineInterface;

/**
 * This trait is used to provide compatibility with Imagine >= 1.0.0.
 */
trait ImagineCompatibleResizerTrait
{
    /**
     * Convert mode for compatibility with imagine >= 1.0.0.
     *
     * @param int|string $mode
     *
     * @return int|string
     */
    final protected function convertMode($mode)
    {
        if (\is_string($mode) && version_compare(ImagineInterface::VERSION, '1.0.0', '>=')) {
            if ('inset' === $mode) {
                $mode = ImageInterface::THUMBNAIL_INSET;
            } elseif ('outbound' === $mode) {
                $mode = ImageInterface::THUMBNAIL_OUTBOUND;
            } elseif (is_numeric($mode)) {
                $mode = (int) $mode;
            }
        }

        return $mode;
    }
}
