<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Resizer;

use Gaufrette\File;
use Imagine\Image\Box;
use PSC\Shop\MediaBundle\Model\MediaInterface;

interface ResizerInterface
{
    /**
     * @param string $format
     */
    public function resize(MediaInterface $media, File $in, File $out, $format, array $settings);

    /**
     * @return Box
     */
    public function getBox(MediaInterface $media, array $settings);
}
