<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;

class FolderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('attr' => array('placeholder' => 'Folder')));
    }

    public function getName()
    {
        return 'folder';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\MediaBundle\Document\Folder',
        ));
    }
}
