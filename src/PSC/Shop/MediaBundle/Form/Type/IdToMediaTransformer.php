<?php

namespace PSC\Shop\MediaBundle\Form\Type;

use Doctrine\Common\Collections\Collection;
use MongoDB\BSON\ObjectId;
use PSC\Shop\MediaBundle\Document\Media;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\Form\Exception\UnexpectedTypeException;

/**
 * IdToMediaTransformer
 */
class IdToMediaTransformer implements DataTransformerInterface
{
    /**
     * @var \Doctrine\ODM\MongoDB\DocumentManager
     */
    private $mongoManager;

    /**
     *
     * @var CurrentValueContainer
     */
    private $currentValueContainer;

    /**
     * @param CurrentValueContainer $currentValueContainer The current value container
     */
    public function __construct($mongoManager, CurrentValueContainer $currentValueContainer)
    {
        $this->mongoManager          = $mongoManager;
        $this->currentValueContainer = $currentValueContainer;
    }

    /**
     * @param Media $entity The value in the original representation
     *
     * @return mixed The value in the transformed representation
     *
     * @throws UnexpectedTypeException when the argument is not an object
     * @throws \InvalidArgumentException when the parameter is a collection
     */
    public function transform($entity)
    {
        if (empty($entity)) {
            return '';
        }

        if (strlen($entity) > 30) {
            return [
                'id'  => $entity
            ];
        }

        $this->currentValueContainer->setCurrentValue($entity);

        $entity = $this->mongoManager
            ->getRepository('PSC\Shop\MediaBundle\Document\Media')
            ->findOneBy(['id' => new ObjectId($entity)]);

        if ($entity) {
            return [
                'ent' => $entity,
                'id' => $entity->getId()
            ];
        }

        return '';
    }

    /**
     * @param string $key
     *
     * @return Media
     *
     * @throws UnexpectedTypeException when the parameter is not numeric
     * @throws TransformationFailedException when the media item cannot be loaded/found
     */
    public function reverseTransform($key)
    {
        if (empty($key)) {
            return null;
        }
        /*if (!is_numeric($key)) {
            throw new UnexpectedTypeException($key, 'numeric');
        }*/
        /*if (!($entity = $this->objectManager->getRepository('KunstmaanMediaBundle:Media')->find($key))) {
            throw new TransformationFailedException(sprintf('The entity with key "%s" could not be found', $key));
        }*/
        //$this->currentValueContainer->setCurrentValue($entity);

        //return $entity;

        return $key;
    }
}
