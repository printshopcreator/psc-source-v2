<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Thumbnail;

use PSC\Shop\MediaBundle\Model\MediaInterface;
use PSC\Shop\MediaBundle\Provider\MediaProviderInterface;

interface ThumbnailInterface
{
    /**
     * @param string $format
     */
    public function generatePublicUrl(MediaProviderInterface $provider, MediaInterface $media, $format);

    /**
     * @param string $format
     */
    public function generatePrivateUrl(MediaProviderInterface $provider, MediaInterface $media, $format);

    /**
     * @param string $format
     */
    public function generateAbsolutePath(MediaProviderInterface $provider, MediaInterface $media, $format);

    public function generate(MediaProviderInterface $provider, MediaInterface $media);

    /**
     * @param string|array $formats
     */
    public function delete(MediaProviderInterface $provider, MediaInterface $media, $formats = null);
}
