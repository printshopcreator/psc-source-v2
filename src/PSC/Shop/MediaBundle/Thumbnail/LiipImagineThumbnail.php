<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\MediaBundle\Thumbnail;

use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use PSC\Shop\MediaBundle\Model\MediaInterface;
use PSC\Shop\MediaBundle\Provider\MediaProviderInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * @final since sonata-project/media-bundle 3.21.0
 */
class LiipImagineThumbnail implements ThumbnailInterface
{
    /**
     * @deprecated Since version 3.3, will be removed in 4.0.
     *
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var CacheManager
     */
    private $cacheManager;
    private FilterManager $filterManager;
    private DataManager $dataManager;

    /**
     * @param RouterInterface|CacheManager $cacheManager
     */
    public function __construct($cacheManager, DataManager $dataManager, FilterManager $filterManager)
    {
        if ($cacheManager instanceof RouterInterface) {
            @trigger_error(sprintf(
                'Using an instance of %s is deprecated since version 3.3 and will be removed in 4.0. Use %s.',
                RouterInterface::class,
                CacheManager::class
            ), E_USER_DEPRECATED);
            $this->router = $cacheManager;
        }
        $this->dataManager = $dataManager;
        $this->filterManager = $filterManager;
        $this->cacheManager = $cacheManager;
    }

    /**
     * {@inheritdoc}
     */
    public function generatePublicUrl(MediaProviderInterface $provider, MediaInterface $media, $format)
    {

        $path = $provider->getReferenceImage($media);

        if (MediaProviderInterface::FORMAT_ADMIN === $format || MediaProviderInterface::FORMAT_REFERENCE === $format) {
            return $path;
        }

        if ($this->router instanceof RouterInterface && !($this->cacheManager instanceof CacheManager)) {
            $path = $this->router->generate(
                sprintf('_imagine_%s', $format),
                ['path' => sprintf('%s/%s_%s.jpg', $provider->generatePath($media), $media->getId(), $format)]
            );
        }
        $path = $provider->getCdnPath($path, $media->getCdnIsFlushable());
        if ($this->cacheManager instanceof CacheManager) {
            $path = $this->cacheManager->getBrowserPath($path, $format);
        }

        return $path;
    }

    public function generateAbsolutePath($provider, $media, $format)
    {
        $path = $provider->getReferenceImage($media);
        $path = $provider->getCdnPath($path, $media->getCdnIsFlushable());

        $image = $this->dataManager->find($format, $path);
        $image = $this->filterManager->applyFilter($image, $format);

        /** @var CacheManager */
        $this->cacheManager->store($image, $path, $format);

        $url = $this->cacheManager->resolve($path, $format);

        return str_replace('apps', 'data/www/new/web', parse_url($url, PHP_URL_PATH));
    }

    /**
     * {@inheritdoc}
     */
    public function generatePrivateUrl(MediaProviderInterface $provider, MediaInterface $media, $format)
    {
        if (MediaProviderInterface::FORMAT_REFERENCE !== $format) {
            throw new \RuntimeException('No private url for LiipImagineThumbnail');
        }

        $path = $provider->getReferenceImage($media);

        return $path;
    }

    /**
     * {@inheritdoc}
     */
    public function generate(MediaProviderInterface $provider, MediaInterface $media)
    {
        // nothing to generate, as generated on demand
    }

    /**
     * {@inheritdoc}
     */
    public function delete(MediaProviderInterface $provider, MediaInterface $media, $formats = null)
    {
        // feature not available
    }
}
