<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\CommunicationBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\CommunicationBundle\Form\Backend\OrderType;
use PSC\Shop\EntityBundle\Document\Position;
use PSC\Shop\EntityBundle\Document\Product;
use PSC\Shop\OrderBundle\Form\Backend\OrderStatusType;
use PSC\Library\Calc\Engine;
use PSC\Library\Calc\PaperContainer;
use PSC\Shop\EntityBundle\Entity\ContactAddress;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Orderpos;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactDeliveryType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactInvoiceType;
use PSC\Shop\OrderBundle\Form\Backend\Detail\ContactSenderType;
use PSC\Shop\OrderBundle\Form\Backend\ProdType;
use PSC\Shop\OrderBundle\Form\Backend\Upload\DeleteType;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Order\Updated;
use PSC\Shop\QueueBundle\Event\Position\Printpartner\Notify;
use PSC\Shop\QueueBundle\Event\Position\Status\Change;
use PSC\Shop\QueueBundle\Service\Event\Manager;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;
use Twig_Environment;
use Twig_Loader_String;

/**
 * OrderController fürs Backend
 *
 * @package    PSC\Shop\Communication\Backend
 * @subpackage Controller
 */
class OrderController extends AbstractController
{
    /**
     * Detail Seite
     *
     * @Route("/order/mail/{uuid}", defaults={"enableDelivery" = 0}, name="psc_shop_communication_backend_order_mail")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param Manager $eventManager
     * @param Shop $shopService
     * @param DocumentManager $documentManager
     * @param Environment $twig
     * @param EntityManagerInterface $entityManager
     * @param string $uuid
     * @return array|RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\SyntaxError
     * @internal param bool $enableDelivery
     */
    public function mailAction(Request $request, SessionInterface $session, Manager $eventManager, Shop $shopService, DocumentManager $documentManager, Environment $twig, EntityManagerInterface $entityManager, $uuid = "")
    {

        $selectedShop = $shopService->getSelectedShop();
/** @var \PSC\Shop\EntityBundle\Document\Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$selectedShop->getUid()));
        if ($shop === null) {
            $shop = new \PSC\Shop\EntityBundle\Document\Shop();
            $shop->setUid($selectedShop->getUid());
        }

        /** @var Order $order */
        $order = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
                ->findOneBy(array('shop' => $selectedShop, 'uuid' => $uuid));
/** @var \PSC\Shop\EntityBundle\Document\Order $orderObj */
        $orderObj = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Order')
            ->findOneBy(array('uid' => (string)$order->getUid()));
        $twigSubject = $twig->createTemplate($shop->getMailOrderCustomerSubjectTemplate());
        $twigText = $twig->createTemplate($shop->getMailOrderCustomerTextTemplate());
        $invoiceAddress = $order->getInvoiceAddress();
        $deliveryAddress = $order->getDeliveryAddress();
        $senderAddress = $order->getSenderAddress();
        $data = array(
            'subject' => $twigSubject->render(array(
                'user' => $this->getUser(),
                'order' => $order,
                'orderObj' => $orderObj,
                'shop' => $shop,
                'invoiceAddress' => $invoiceAddress,
                'deliveryAddress' => $deliveryAddress,
                'senderAddress' => $senderAddress
            )),
            'text' => $twigText->render(array(
                'user' => $this->getUser(),
                'order' => $order,
                'orderObj' => $orderObj,
                'shop' => $shop,
                'invoiceAddress' => $invoiceAddress,
                'deliveryAddress' => $deliveryAddress,
                'senderAddress' => $senderAddress
            ))
        );
        $form = $this->createForm(OrderType::class, $data);
        $form->handleRequest($request);
        if ($request->isMethod('post') && $form->isValid()) {
            $session->getFlashBag()->add('success', 'Mail send');
            $notify = new \PSC\Shop\QueueBundle\Event\Communication\Order\Contact();
            $notify->setShop($selectedShop->getUID());
            $notify->setOrder($order->getUuid());
            $notify->setSubject($data['subject']);
            $notify->setText($data['text']);
            $eventManager->addJob($notify);
            return $this->redirectToRoute('psc_shop_order_backend_detail_show', array('uuid' => $uuid));
        }

        return array(
            'order' => $order,
            'orderObj' => $orderObj,
            'shop' => $shop,
            'form' => $form->createView()
        );
    }
}
