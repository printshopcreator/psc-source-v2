<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Shipping;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\ShippingBundle\Document\History as PSCHistory;
use PSC\Shop\ShippingBundle\Form\Backend\DeleteType;
use PSC\Shop\ShippingBundle\Form\Backend\ShippingType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\History;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * EditController fürs Backend
 *
 * @package    PSC\Shop\Shipping
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    private History $historyService;
    public function __construct(History $historyService)
    {
        $this->historyService = $historyService;
    }

    /**
     * create
     *
     * @Route("/edit/create", name="psc_shop_shipping_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(
        Request $request,
        Field $fieldService,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        SessionInterface $session,
        EntityManagerInterface $entityManager
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shipping);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Shipping);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $shipping = new Shipping();
        $shipping->setShop($selectedShop);
        $form = $this->createForm(ShippingType::class, $shipping);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($shipping);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'Shipping \'' . $shipping->getTitle() . '\' has been created!');
            return $this->redirectToRoute('psc_shop_shipping_backend_list');
        }

        return array(
            'shipping' => $shipping,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups
        );
    }


    /**
     * edit
     *
     * @Route("/edit/edit/{uid}", name="psc_shop_shipping_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(
        Request $request,
        Field $fieldService,
        EntityManagerInterface $entityManager,
        SessionInterface $session,
        $uid
    ) {

        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shipping);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Shipping);
/** @var Shipping $shipping */
        $shipping = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')->findOneBy(['uid' => $uid]);
        $form = $this->createForm(ShippingType::class, $shipping);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $this->historyService->createHistoryEntry(new PSCHistory((string)$shipping->getUID()), $shipping);
            $entityManager->persist($shipping);
            $entityManager->flush();
            $session->getFlashBag()->add('success', 'Shipping \'' . $shipping->getTitle() . '\' has been upgraded!');
            return $this->redirectToRoute('psc_shop_shipping_backend_list');
        }

        return array(
            'shipping' => $shipping,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$shipping->getUID())
        );
    }

    /**
     * remove
     *
     * @Route("/edit/delete/{uid}", name="psc_shop_shipping_backend_delete")
     * @Template()
     *
     * @param Request $request
     * @param Session $session
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, SessionInterface $session, EntityManagerInterface $entityManager, $uid)
    {
        /** @var Shipping $shipping */
        $shipping = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')->findOneBy(['uid' => $uid]);
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $title = $shipping->getTitle();
                $entityManager->remove($shipping);
                $entityManager->flush();
                $session->getFlashBag()->add('success', 'Shipping \'' . $title . '\' has been deleted!');
                return $this->redirectToRoute('psc_shop_shipping_backend_list');
            }
            return $this->redirectToRoute('psc_shop_shipping_backend_list');
        }


        return array(
            'shipping' => $shipping,
            'form' => $form->createView()
        );
    }
}
