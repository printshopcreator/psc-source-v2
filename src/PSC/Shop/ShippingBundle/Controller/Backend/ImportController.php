<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Knp\Component\Pager\PaginatorInterface;
use PSC\Shop\EntityBundle\Document\Parcelnumber;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\ShippingBundle\Form\Backend\ParcelnumberImportType;
use PSC\Shop\ShippingBundle\Form\Backend\ParcelnumberType;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * DashboardController fürs Backend
 *
 * @package    PSC\Shop\Shipping
 * @subpackage Controller
 */
class ImportController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/parcel_number/import/index", name="psc_shop_shipping_backend_parcel_number_import_index")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param DocumentManager $documentManager
     * @param PaginatorInterface $paginator
     * @return array|View
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, DocumentManager $documentManager, PaginatorInterface $paginator)
    {
        $selectedShop = $shopService->getSelectedShop();
        $parcelNumber = new Parcelnumber();
        $parcelNumber->setShopId($selectedShop->getId());
        $parcelNumber->setCreated(new \Datetime());
        $form = $this->createForm(ParcelnumberType::class, $parcelNumber);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $documentManager->persist($parcelNumber);
            $documentManager->flush();
        }

        $formImport = $this->createForm(new ParcelnumberImportType($selectedShop));
        $formImport->handleRequest($request);
        if ($formImport->isSubmitted() && $formImport->isValid()) {
        /** @var UploadedFile $uploadFile */
            $uploadFile = $formImport->get('importFile')->getData();
            if (($handle = fopen($uploadFile->getRealPath(), "r")) !== false) {
                while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                    $parcelNumber = new Parcelnumber();
                    $parcelNumber->setShopId($selectedShop->getId());
                    $parcelNumber->setCreated(new \Datetime());
                    $parcelNumber->setShippingId($formImport->get('shippingId')->getData());
                    $parcelNumber->setNumber(trim($data[0]));
                    $documentManager->persist($parcelNumber);
                }
                fclose($handle);
                $documentManager->flush();
            }
        }

        $userRepository = $documentManager->getRepository('PSCEntityBundle:Parcelnumber');
        $qb = $userRepository->createQueryBuilder('parcelnumber');
        $pagination = $paginator->paginate($qb, $request->query->getInt('page', 1), 20);
        return array(
            'form' => $form->createView(),
            'formImport' => $formImport->createView(),
            'pagination' => $pagination,
        );
    }
}
