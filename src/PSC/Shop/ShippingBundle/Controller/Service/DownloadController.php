<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\Controller\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use PSC\Shop\ShippingBundle\Provider\ShippingProvider;
use PSC\Shop\ShippingBundle\Service\Registry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * CreateController fürs Service
 *
 * @package    PSC\Shop\Shipping
 * @subpackage Controller
 */
class DownloadController extends AbstractController
{
    /**
     * Download an Shipping
     *
     * @Route("/download/{typ}/{order}", name="psc_shop_shipping_service_download")
     *
     * @param Registry $registry
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param $typ
     * @param $order
     * @return Response
     * @throws \Exception
     */
    public function downloadAction(Registry $registry, EntityManagerInterface $entityManager, DocumentManager $documentManager, $typ, $order)
    {

        if ($typ == '') {
            throw new \Exception('Kein Payment angegeben');
        }

        if (!$registry->has($typ)) {
            throw new \Exception('Provider nicht gefunden');
        }

        /** @var ShippingProvider $provider */
        $provider = $registry->get($typ);
/** @var Order $order */
        $order = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('uuid' => $order));
/** @var Shop $shopEntity */
        $shopEntity = $order->getShop();
/** @var \PSC\Shop\EntityBundle\Document\Shop $order */
        $shopDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => $shopEntity->getUID()));
/** @var ShippingGatewaysettings $gwSettings */
        $gwSettings = $documentManager
            ->getRepository('PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings')
            ->findOneBy(array('shop' => (string)$shopEntity->getId(), 'gatewayType' => $typ));
        $provider->setShopEntity($shopEntity);
        $provider->setShopDoc($shopDoc);
        $provider->setShippingGatewaySettings($gwSettings);
        $data = $provider->handleShipping($order);
        $response = new Response(base64_decode($data['image']));
        $response->headers->set('Content-Type', 'image/gif');
        return $response;
    }
}
