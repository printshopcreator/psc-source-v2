<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\Controller\Service;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use PSC\Shop\ShippingBundle\Provider\ShippingProvider;
use PSC\Shop\ShippingBundle\Service\Registry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * CreateController fürs Service
 *
 * @package    PSC\Shop\Shipping
 * @subpackage Controller
 */
class CreateController extends AbstractController
{
    /**
     * Create an Shipping
     *
     * @Route("/create/{typ}/{order}", name="psc_shop_shipping_service_create")
     *
     * @param Registry $registry
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param $typ
     * @param $order
     * @return JsonResponse
     * @throws \Exception
     */
    public function createAction(Registry $registry, EntityManagerInterface $entityManager, DocumentManager $documentManager, $typ, $order)
    {

        if ($typ == '') {
            throw new \Exception('Kein Payment angegeben');
        }

        if (!$registry->has($typ)) {
            throw new \Exception('Provider nicht gefunden');
        }

        /** @var ShippingProvider $provider */
        $provider = $registry->get($typ);
/** @var Order $order */
        $order = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('uuid' => $order));
/** @var Shop $shopEntity */
        $shopEntity = $order->getShop();
/** @var \PSC\Shop\EntityBundle\Document\Shop $order */
        $shopDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => $shopEntity->getUID()));
/** @var ShippingGatewaysettings $gwSettings */
        $gwSettings = $documentManager
            ->getRepository('PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings')
            ->findOneBy(array('shop' => (string)$shopEntity->getId(), 'gatewayType' => $typ));
        $provider->setShopEntity($shopEntity);
        $provider->setShopDoc($shopDoc);
        $provider->setShippingGatewaySettings($gwSettings);
        $data = $provider->handleShipping($order);
        $json = new JsonResponse();
        $json->setContent(json_encode(array('success' => true, 'data' => $data)));
        return $json;
    }
}
