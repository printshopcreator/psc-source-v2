<?php

namespace PSC\Shop\ShippingBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Shipping as PSCShipping;
use PSC\Shop\ShippingBundle\Model\Shipping;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class One extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private Shop $shopService;

    public function __construct(EntityManagerInterface $entityManager, Shop $shopService)
    {
        $this->entityManager = $entityManager;
        $this->shopService = $shopService;
    }

    /**
     * get all shipment
     *
     * @Route("/{id}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="shippings",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ShippingBundle\Model\Shipping::class))
     * )
     * @OA\Tag(name="Shipping")
     */
    public function one(string $id): JsonResponse
    {
        $result = $this->entityManager->getRepository(PSCShipping::class)->findOneBy(['shop' => $this->shopService->getShopByDomain(), 'private' => 0, 'uid' => $id]);

        if ($result) {
            $output = new Shipping();
            $output->setTitle($result->getTitle());
            $output->setId($result->getUid());
        } else {
            $output = new NotFound();
        }
        return $this->json($output);
    }
}
