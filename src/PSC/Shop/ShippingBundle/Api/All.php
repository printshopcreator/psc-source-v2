<?php

namespace PSC\Shop\ShippingBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Shipping;
use PSC\Shop\ShippingBundle\Dto\All\Output;
use PSC\Shop\ShippingBundle\Model\Shipping as PSCShipping;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class All extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private Shop $shopService;

    private TokenStorageInterface $tokenStorage;

    public function __construct(EntityManagerInterface $entityManager, Shop $shopService, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->shopService = $shopService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * get all shipments
     *
     * @Route("/", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="shipments",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ShippingBundle\Dto\All\Output::class))
     * )
     * @OA\Tag(name="Shipping")
     */
    public function all(): JsonResponse
    {
        $output = [];
        $result = $this->entityManager->getRepository(Shipping::class)->findBy(['shop' => $this->shopService->getShopByDomain(), 'private' => 0]);
        foreach ($result as $shipping) {
            $shipObj = new PSCShipping();
            $shipObj->setId($shipping->getUid());
            $shipObj->setTitle($shipping->getTitle());
            $output[] = $shipObj;
        }
        return $this->json(new Output($output));
    }

    /**
     * get all shipments
     *
     * @Route("/my", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="shipments",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\ShippingBundle\Dto\All\Output::class))
     * )
     * @OA\Tag(name="Shipping")
     * @Security(name="Bearer")
     * @IsGranted("ROLE_USER")
     */
    public function my(): JsonResponse
    {
        $output = [];
        $result = $this->entityManager->getRepository(Shipping::class)->findBy(['shop' => $this->shopService->getShopByDomain(), 'private' => 0]);
        foreach ($result as $shipping) {
            $shipObj = new PSCShipping();
            $shipObj->setId($shipping->getUid());
            $shipObj->setTitle($shipping->getTitle());
            $output[] = $shipObj;
        }

        $user = $this->tokenStorage->getToken()->getUser();
        foreach ($user->shippings as $shipping) {
            if ($payment->getShop()->getId() == $this->shopService->getShopByDomain()->getId()) {
                $shipObj = new PSCShipping();
                $shipObj->setId($shipping->getUid());
                $shipObj->setTitle($shipping->getTitle());
                $output[] = $shipObj;
            }
        }

        return $this->json(new Output($output));
    }
}
