<?php

namespace PSC\Shop\ShippingBundle\Command;

use GuzzleHttp\Client;
use PSC\Component\SteplayouterBundle\Service\Pdf;
use PSC\Shop\EntityBundle\Document\Job;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\QueueBundle\Event\EventManager;
use PSC\Shop\QueueBundle\Event\Registry;
use PSC\Shop\QueueBundle\Type\QueueInterface;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use PSC\Shop\ShippingBundle\Provider\ShippingProvider;
use PSC\System\PluginBundle\Event\ClearCache;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;

class CreateCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('service:shipping:create')
            ->setDescription('Create Shippment PDF')
            ->addArgument(
                'type',
                InputArgument::REQUIRED,
                'Typ'
            )
            ->addArgument(
                'order',
                InputArgument::REQUIRED,
                'Auftrag'
            )
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'File'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $type = $input->getArgument('type');
        $orderUuid = $input->getArgument('order');
        $file = $input->getArgument('file');

        /** @var Registry $registry */
        $registry = $this->getContainer()->get('psc.shop.shipping.registry');

        if (!$registry->has($type)) {
            throw new \Exception('Provider nicht gefunden');
        }

        /** @var ShippingProvider $provider */
        $provider = $registry->get($type);

        /** @var Order $order */
        $order = $this->getContainer()->get('doctrine.orm.entity_manager')
            ->getRepository('PSC\Shop\EntityBundle\Entity\Order')
            ->findOneBy(array('uuid' => $orderUuid));

        /** @var Shop $shopEntity */
        $shopEntity = $order->getShop();

        /** @var \PSC\Shop\EntityBundle\Document\Shop $shopDoc */
        $shopDoc = $this->getContainer()->get('doctrine_mongodb')
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => $shopEntity->getUID()));



        /** @var ShippingGatewaysettings $gwSettings */
        $gwSettings = $this->getContainer()->get('doctrine_mongodb')
            ->getRepository('PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings')
            ->findOneBy(array('shop' => (string)$shopEntity->getId(), 'gatewayType' => $type));

        $provider->setShopEntity($shopEntity);
        $provider->setShopDoc($shopDoc);
        $provider->setShippingGatewaySettings($gwSettings);
        $data = $provider->handleShipping($order);

        if (isset($data['trackingNumber'])) {
            $order->setPackage($data['trackingNumber']);
            $em = $this->getContainer()->get('doctrine.orm.entity_manager');
            $em->persist($order);
            $em->flush();
        }
        if (isset($data['image'])) {
            file_put_contents($file, base64_decode($data['image']));
        }
    }
}
