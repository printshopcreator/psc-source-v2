<?php

namespace PSC\Shop\ShippingBundle\Dto\All;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;

class Output
{
    public function __construct(array $shippings)
    {
        $this->data = $shippings;
        $this->count = count($shippings);
    }

    /**
     * @var Shipping[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\ShippingBundle\Model\Shipping::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
