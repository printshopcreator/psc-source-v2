<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\DependencyInjection\Compiler;

use PSC\Shop\ShippingBundle\Service\Registry;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ShippingProviderPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        // always first check if the primary service is defined
        if (!$container->has(Registry::class)) {
            return;
        }

        $definition = $container->findDefinition(Registry::class);
// find all service IDs with the app.mail_transport tag
        $taggedServices = $container->findTaggedServiceIds('shippingProvider');
        foreach ($taggedServices as $id => $tags) {
        // add the transport service to the TransportChain service
            $definition->addMethodCall('add', [new Reference($id)]);
        }
    }
}
