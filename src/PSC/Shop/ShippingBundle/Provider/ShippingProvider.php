<?php

namespace PSC\Shop\ShippingBundle\Provider;

use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

class ShippingProvider implements ShippingProviderInterface
{
    /** @var Shop */
    protected $shopEntity = null;
/** @var \PSC\Shop\EntityBundle\Document\Shop */
    protected $shopDoc = null;
/** @var ShippingGatewaySettings */
    protected $shippingGatewaySettings = null;
    public function getType()
    {
        // TODO: Implement getType() method.
    }

    public function handleNotify(Request $request)
    {
        // TODO: Implement handleNotify() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function getTemplate()
    {
        // TODO: Implement getTemplate() method.
    }

    public function getSubForm(ShippingGatewaySettings $settings, FormBuilder $builder)
    {
        // TODO: Implement getSubForm() method.
    }

    public function saveDocument(ShippingGatewaySettings $settings, Form $form)
    {
        // TODO: Implement saveDocument() method.
    }

    /**
     * @return Shop
     */
    public function getShopEntity()
    {
        return $this->shopEntity;
    }

    /**
     * @param Shop $shopEntity
     */
    public function setShopEntity($shopEntity)
    {
        $this->shopEntity = $shopEntity;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Document\Shop
     */
    public function getShopDoc()
    {
        return $this->shopDoc;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Document\Shop $shopDoc
     */
    public function setShopDoc($shopDoc)
    {
        $this->shopDoc = $shopDoc;
    }

    /**
     * @return ShippingGatewaysettings
     */
    public function getShippingGatewaySettings()
    {
        return $this->shippingGatewaySettings;
    }

    /**
     * @param ShippingGatewaysettings $shippingGatewaySettings
     */
    public function setShippingGatewaySettings($shippingGatewaySettings)
    {
        $this->shippingGatewaySettings = $shippingGatewaySettings;
    }

    protected function getHost()
    {
        $is_https = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) == 'on';
        $http_protocol = $is_https ? 'https' : 'http';
        return $http_protocol . '://' . $_SERVER['HTTP_HOST'];
    }

    public function __toString(): string
    {
        return $this->getName();
    }
}
