<?php

namespace PSC\Shop\ShippingBundle\Provider;

use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

interface ShippingProviderInterface
{
    public function getType();
    public function handleNotify(Request $request);
    public function getName();
    public function getTemplate();
    public function getSubForm(ShippingGatewaySettings $settings, FormBuilder $builder);
    public function saveDocument(ShippingGatewaySettings $settings, Form $form);
}
