<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;

/**
 * @Document
 */
class ShippingGatewaySettings
{
    /**
     * @Id
     */
    protected $id;
/**
     * @var string $shop
     *
     * @Field(type="string")
     */
    protected $shop;
/**
     * @var string $type
     *
     * @Field(type="string")
     */
    protected $gatewayType;
/** @EmbedOne */
    protected $gatewayDocument;
/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param string $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function getGatewayType()
    {
        return $this->gatewayType;
    }

    /**
     * @param string $gatewayType
     */
    public function setGatewayType($gatewayType)
    {
        $this->gatewayType = $gatewayType;
    }

    /**
     * @return mixed
     */
    public function getGatewayDocument()
    {
        return $this->gatewayDocument;
    }

    /**
     * @param mixed $gatewayDocument
     */
    public function setGatewayDocument($gatewayDocument)
    {
        $this->gatewayDocument = $gatewayDocument;
    }
}
