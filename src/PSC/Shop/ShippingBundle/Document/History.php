<?php

namespace PSC\Shop\ShippingBundle\Document;

use PSC\Shop\EntityBundle\Entity\Shipping;
use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="shipping_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $shipping;

    function __construct(string $shipping = "")
    {
        parent::__construct();
        $this->shipping = $shipping;
    }

    public function getShipping()
    {
        return $this->shipping;
    }

    public function setShipping(string $shipping)
    {
        $this->shipping = $shipping;
    }

    public function getReferenceField(): string
    {
        return 'shipping';
    }
}
