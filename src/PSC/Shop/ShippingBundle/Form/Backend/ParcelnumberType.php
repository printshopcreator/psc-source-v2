<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelnumberType extends AbstractType
{
    protected $shopService;

    public function __construct(Shop $shopService)
    {
        $this->shopService = $shopService;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number', TextType::class, array('disabled' => false, 'required' => true, 'label' => 'Nummer'))
            ->add('shippingId', EntityType::class, array(
                'class' => 'PSC\Shop\EntityBundle\Entity\Shipping',
                'property' => 'title',
                'label' => 'Versand',
                'multiple' => false,
                'expanded' => false,
                'query_builder' => function (EntityRepository $er) {

                    return $er->createQueryBuilder('u')
                        ->where('u.shop = :shop')->setParameter('shop', $this->shopService->getSelectedShop()->getId());
                }))
            ->add('save', 'submit', array('label' => 'Hinzufügen'));
    }

    public function getName()
    {
        return 'parcelnumber';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Document\Parcelnumber',
        ));
    }
}
