<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ShippingBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\Shop\MediaBundle\Form\Type\ImageType;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Tax;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;

class ShippingType extends AbstractType
{
    /** @var Field  */
    protected $fields;
    protected $taxService;
    protected $formFactory;
    public function __construct(Field $fields, FormFactoryInterface $formFactory, Tax $taxService)
    {
        $this->fields = $fields;
        $this->taxService = $taxService;
        $this->formFactory = $formFactory;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, array('label' => 'Name', 'required' => true))
            ->add('parameter1', TextType::class, array('label' => 'parameter1', 'required' => false))
            ->add('price', TextType::class, array('label' => 'costs', 'required' => true))
            ->add('pricePercent', TextType::class, array('label' => 'CostPercentage', 'required' => false))
            ->add('pos', IntegerType::class, array('label' => 'Position', 'required' => true))
            ->add('weightFrom', IntegerType::class, array('label' => 'weightof', 'required' => true, 'empty_data' => 0))
            ->add('priceFrom', IntegerType::class, array('label' => 'priceof', 'required' => true, 'empty_data' => 0))
            ->add('taxClass', ChoiceType::class, array(
                'choices' => $this->taxService->getTaxesForForm(),
                'label' => 'taxclass'))
            ->add('weightTo', IntegerType::class, array('label' => 'weightuntil', 'required' => true, 'empty_data' => 0))
            ->add('priceTo', IntegerType::class, array('label' => 'priceuntil', 'required' => true, 'empty_data' => 0))
            ->add('private', CheckboxType::class, array('label' => 'Private', 'required' => false))
            ->add('enable', CheckboxType::class, array('label' => 'active', 'required' => false))
            ->add('noPayment', CheckboxType::class, array('label' => 'Hidepaymentmethod', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'description', 'required' => false))
            ->add('matrix', AceEditorType::class, array(
                'wrapper_attr' => array(), // aceeditor wrapper html attributes.
                'width' => '90%',
                'height' => '500',
                'font_size' => 14,
                'mode' => 'ace/mode/xml', // every single default mode must have ace/mode/* prefix
                'theme' => 'ace/theme/monokai', // every single default theme must have ace/theme/* prefix
                'tab_size' => null,
                'read_only' => null,
                'use_soft_tabs' => null,
                'use_wrap_mode' => null,
                'show_print_margin' => null,
                'required' => false,
                'highlight_active_line' => null
            ))
        ;
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shipping) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup()), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'shipping';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Shipping',
            'translation_domain' => 'core_shipping_edit'
        ));
    }
}
