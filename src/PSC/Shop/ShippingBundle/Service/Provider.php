<?php

namespace PSC\Shop\ShippingBundle\Service;

use LogicException;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryInterface;
use PSC\Shop\EntityBundle\Entity\Order;
use PSC\Shop\EntityBundle\Document\Order as OrderDoc;

class Provider
{
    public function getAll()
    {
        return array(
            'Keine' => 'Keine',
            'DHL'   => 'DHL',
            'UPS'   => 'UPS',
            'GLS'   => 'GLS'
        );
    }

    public function getTrackingUrl($type, $code)
    {
    }

    public function createShipment(Order $order, OrderDoc $orderDoc)
    {
    }
}
