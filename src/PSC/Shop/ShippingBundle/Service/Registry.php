<?php

namespace PSC\Shop\ShippingBundle\Service;

use LogicException;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryInterface;
use PSC\Shop\ShippingBundle\Provider\ShippingProviderInterface;

class Registry
{
    /** @var  ShippingProviderInterface[] */
    private $shippingProvider = array();
/** @return ShippingProviderInterface[] */
    public function all()
    {
        return $this->shippingProvider;
    }

    /**
     * @param $key string
     *
     * @return ShippingProviderInterface
     */
    public function get($key)
    {
        if (!$this->has($key)) {
            throw new LogicException('Shipping ' . $key . ' not registered');
        }

        return $this->shippingProvider[$key];
    }

    public function has($type)
    {
        return array_key_exists($type, $this->shippingProvider);
    }

    /**
     * @param ShippingProviderInterface $element
     */
    public function add(ShippingProviderInterface $element)
    {
        if ($this->has($element->getType())) {
            throw new LogicException('Shipping ' . $element->getType() . ' already registered');
        }

        $this->shippingProvider[$element->getType()] = $element;
    }

    /**
     * @param ShippingProviderInterface $element
     */
    public function remove(ShippingProviderInterface $element)
    {
        if (!$this->has($element->getType())) {
            throw new LogicException('Shipping ' . $element->getType() . ' not registered');
        }

        unset($this->shippingProvider[$element->getType()]);
    }
}
