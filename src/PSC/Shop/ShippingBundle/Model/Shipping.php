<?php

namespace PSC\Shop\ShippingBundle\Model;

use OpenApi\Annotations as OA;

class Shipping
{
    /**
     * @OA\Property(type="integer")
     */
    private int $id;

    /**
     * @OA\Property(type="string")
     */
    private string $title;

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
