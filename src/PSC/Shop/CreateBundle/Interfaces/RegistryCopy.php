<?php

namespace PSC\Shop\CreateBundle\Interfaces;

interface RegistryCopy
{
    public function getName(): string;
    public function getType(): string;
}
