<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

use PSC\Shop\EntityBundle\Document\Country;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\ResourcePrivilegeShop;
use PSC\Shop\EntityBundle\Entity\ShopAccount;
use PSC\Shop\EntityBundle\Entity\ShopContact;
use Ramsey\Uuid\Uuid;

class Shop extends Base
{
    public function DoCopy()
    {
        $copyShop = $this->getState()->getEntityManager()->getConnection()->fetchAssociative('SELECT * FROM shop WHERE id = ?', array($this->getState()->getSourceShop()->getId()));
        $uid = $copyShop['id'];
        unset($copyShop['id']);
        $copyShop['uid'] = Uuid::uuid4();
        $copyShop['apikey'] = Uuid::uuid4();
        $copyShop['name'] = $this->getState()->getAdditionalData('title');
        $this->getState()->getEntityManager()->getConnection()->insert('shop', $copyShop);
        $copyShop = $this->getState()->getEntityManager()
            ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')->findOneBy(['uid' => $this->getState()->getEntityManager()->getConnection()->lastInsertId()]);
/** @var \PSC\Shop\EntityBundle\Document\Shop $shopDoc */
        $shopDoc = $this->getState()->getMongo()
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$uid));
        if ($shopDoc) {
            $d = clone $shopDoc;
            $d->setId(null);
            $d->setUid($copyShop->getUID());
            $this->getState()->getMongo()->detach($d);
            $this->getState()->getMongo()->persist($d);
            $this->getState()->getMongo()->flush();
        }

        $account = new Account();
        $account->setTitle('No Account');
        $account->setInstall($this->getState()->getSourceShop()->getInstall());
        $account->setParentId(0);
        $copyShop->setDefaultAccount($account);
        $accountshop = new ShopAccount();
        $accountshop->setAccount($account);
        $accountshop->setShop($copyShop);
        $domain = new Domain($copyShop);
        $domain->setHost($this->getState()->getAdditionalData('domain'));
        $this->getState()->getEntityManager()->persist($account);
        $this->getState()->getEntityManager()->flush();
        $this->getState()->getEntityManager()->persist($copyShop);
        $this->getState()->getEntityManager()->flush();
        $this->getState()->getEntityManager()->persist($accountshop);
        $this->getState()->getEntityManager()->persist($domain);
        $temp = array(8, 9, 10, 13, 23, 28, 31, 32, 33, 34, 35, 38, 46, 48, 49, 50, 51);
        foreach ($temp as $key) {
            $resprevshop = new ResourcePrivilegeShop();
            $resprevshop->setShop($copyShop);
            $resprevshop->setResourceprivilege($key);
            $this->getState()->getEntityManager()->persist($resprevshop);
        }
        $this->getState()->getEntityManager()->flush();
        $shopContact1 = new ShopContact();
        $shopContact1->setContact($this->getState()->getUser());
        $shopContact1->setShop($copyShop);
        $shopContact1->setAdmin(true);
        $shopContact1->setLogin(true);
        $this->getState()->getEntityManager()->persist($shopContact1);
        $this->getState()->getEntityManager()->flush();
        $this->getState()->setTargetShop($copyShop);
        $countrys = $this->getState()->getMongo()
            ->getRepository('PSC\Shop\EntityBundle\Document\Country')
            ->findBy(array('shop' => (string)$this->getState()->getSourceShop()->getUid()));
/** @var Country $setting */
        foreach ($countrys as $country) {
            $tCountry = clone $country;
            $tCountry->setId(null);
            $tCountry->setShop($this->getState()->getTargetShop()->getUID());
            $this->getState()->getMongo()->detach($tCountry);
            $this->getState()->getMongo()->persist($tCountry);
            $this->getState()->getMongo()->flush();
        }

        $settings = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM shop_setting WHERE shop_id = ?', array($this->getState()->getSourceShop()->getId()));
        foreach ($settings as $setting) {
            unset($setting['id']);
            $setting['shop_id'] = $this->getState()->getTargetShop()->getUID();
            $this->getState()->getEntityManager()->getConnection()->insert('shop_setting', $setting);
        }
    }
}
