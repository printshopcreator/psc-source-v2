<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

use PSC\Shop\PaymentBundle\Document\Gatewaysettings;

class Payment extends Base
{
    public function DoCopy()
    {
        $payments = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM paymenttype WHERE shop_id = ?', array($this->getState()->getSourceShop()->getId()));
        foreach ($payments as $payment) {
            $orgUid = $payment['id'];
            unset($payment['id']);
            $payment['shop_id'] = $this->getState()->getTargetShop()->getUID();
            $this->getState()->getEntityManager()->getConnection()->insert('paymenttype', $payment);
            $newUid = $this->getState()->getEntityManager()->getConnection()->lastInsertId();
        /** @var \PSC\Shop\EntityBundle\Document\Payment $paymentDoc */
            $paymentDoc = $this->getState()->getMongo()
                ->getRepository('PSC\Shop\EntityBundle\Document\Payment')
                ->findOneBy(array('uid' => (string)$orgUid));
            if ($paymentDoc) {
                $d = clone $paymentDoc;
                $d->setId(null);
                $d->setUid($newUid);
                $this->getState()->getMongo()->detach($d);
                $this->getState()->getMongo()->persist($d);
                $this->getState()->getMongo()->flush();
            }

            $gatewaySettings = $this->getState()->getMongo()
                ->getRepository('PSC\Shop\PaymentBundle\Document\Gatewaysettings')
                ->findBy(array('shop' => (string)$this->getState()->getSourceShop()->getId()));
        /** @var Gatewaysettings $setting */
            foreach ($gatewaySettings as $setting) {
                $tSetting = clone $setting;
                $tSetting->setId(null);
                $tSetting->setShop($this->getState()->getTargetShop()->getUID());
                $this->getState()->getMongo()->detach($tSetting);
                $this->getState()->getMongo()->persist($tSetting);
                $this->getState()->getMongo()->flush();
            }
        }
    }
}
