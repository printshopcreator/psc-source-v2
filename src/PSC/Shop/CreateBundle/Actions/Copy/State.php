<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Security\Core\User\UserInterface;

final class State
{
    private Shop $sourceShop;
    private Shop $targetShop;
    private EntityManagerInterface $entityManager;
    private DocumentManager $mongo;
    private UserInterface $user;
    private array $additionalData = [];
    public function __construct(Shop $sourceShop, EntityManagerInterface $entityManager, DocumentManager $mongo, UserInterface $user)
    {
        $this->sourceShop = $sourceShop;
        $this->entityManager = $entityManager;
        $this->mongo = $mongo;
        $this->user = $user;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return DocumentManager
     */
    public function getMongo(): DocumentManager
    {
        return $this->mongo;
    }

    /**
     * @return Shop
     */
    public function getSourceShop(): Shop
    {
        return $this->sourceShop;
    }

    /**
     * @return Shop
     */
    public function getTargetShop(): Shop
    {
        return $this->targetShop;
    }

    /**
     * @param Shop $targetShop
     */
    public function setTargetShop(Shop $targetShop): void
    {
        $this->targetShop = $targetShop;
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function addAdditionalData(string $key, $data)
    {
        $this->additionalData[$key] = $data;
    }

    public function getAdditionalData(string $key)
    {
        if (!isset($this->additionalData[$key])) {
            return null;
        }
        return $this->additionalData[$key];
    }
}
