<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;

class Shipment extends Base
{
    public function DoCopy()
    {
        $shippings = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM shippingtype WHERE shop_id = ?', array($this->getState()->getSourceShop()->getId()));
        foreach ($shippings as $shipping) {
            $orgUid = $shipping['id'];
            unset($shipping['id']);
            $shipping['shop_id'] = $this->getState()->getTargetShop()->getUID();
            $this->getState()->getEntityManager()->getConnection()->insert('shippingtype', $shipping);
            $newUid = $this->getState()->getEntityManager()->getConnection()->lastInsertId();
            $gatewaySettings = $this->getState()->getMongo()
                ->getRepository('PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings')
                ->findBy(array('shop' => (string)$this->getState()->getSourceShop()->getId()));
        /** @var ShippingGatewaySettings $setting */
            foreach ($gatewaySettings as $setting) {
                $tSetting = clone $setting;
                $tSetting->setId(null);
                $tSetting->setShop($this->getState()->getTargetShop()->getUID());
                $this->getState()->getMongo()->detach($tSetting);
                $this->getState()->getMongo()->persist($tSetting);
                $this->getState()->getMongo()->flush();
            }
        }
    }
}
