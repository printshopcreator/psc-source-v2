<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

class Articlegroup extends Base
{
    public function DoCopy()
    {
        $articlegroups = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM article_group WHERE shop_id = ?', array($this->getState()->getSourceShop()->getId()));
        $articlegroupsTemp = [];
        foreach ($articlegroups as $articlegroup) {
            $orgUid = $articlegroup['id'];
            unset($articlegroup['id']);
            $articlegroup['shop_id'] = $this->getState()->getTargetShop()->getUID();
            $this->getState()->getEntityManager()->getConnection()->insert('article_group', $articlegroup);
            $newUid = $this->getState()->getEntityManager()->getConnection()->lastInsertId();
            $articlegroupsTemp[$orgUid] = $newUid;
        /** @var \PSC\Shop\EntityBundle\Document\Productgroup $productgroupDoc */
            $productgroupDoc = $this->getState()->getMongo()
                ->getRepository('PSC\Shop\EntityBundle\Document\Productgroup')
                ->findOneBy(array('uid' => (string)$orgUid));
            if ($productgroupDoc) {
                $d = clone $productgroupDoc;
                $d->setId(null);
                $d->setUid($newUid);
                $this->getState()->getMongo()->detach($d);
                $this->getState()->getMongo()->persist($d);
                $this->getState()->getMongo()->flush();
            }
        }

        $articlegroups = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM article_group WHERE shop_id = ?', array($this->getState()->getTargetShop()->getId()));
        foreach ($articlegroups as $articlegroup) {
            if ($articlegroup['parent'] != 0 || $articlegroup['parent'] != "") {
                if (isset($articlegroupsTemp[$articlegroup['parent']])) {
                    $articlegroup['parent'] = $articlegroupsTemp[$articlegroup['parent']];
                    $this->getState()->getEntityManager()->getConnection()->update('article_group', $articlegroup, ['id' => $articlegroup['id']]);
                }
            }
        }

        $this->getState()->addAdditionalData('articlegroups', $articlegroupsTemp);
    }
}
