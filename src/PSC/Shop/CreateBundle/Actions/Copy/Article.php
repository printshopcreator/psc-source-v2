<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

use PSC\Shop\EntityBundle\Entity\Product;
use Ramsey\Uuid\Uuid;

class Article extends Base
{
    public function DoCopy()
    {
        $articles = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM article WHERE shop_id = ?', array($this->getState()->getSourceShop()->getId()));
        foreach ($articles as $article) {
            $orgUid = $article['id'];
            $orginalDir = $article['a6_directory'];
            unset($article['id']);
            $article['uuid'] = Uuid::uuid4();
            $article['shop_id'] = $this->getState()->getTargetShop()->getUID();
            $this->getState()->getEntityManager()->getConnection()->insert('article', $article);
            $newUid = $this->getState()->getEntityManager()->getConnection()->lastInsertId();
        /** @var Product $copyProduct */
            $copyProduct = $this->getState()->getEntityManager()
                ->getRepository('PSC\Shop\EntityBundle\Entity\Product')->findOneBy(['uid' => $newUid]);
            $path = str_split($newUid);
            $copyProduct->setTemplatePrintDirectory(implode('/', $path) . '/');
            $this->getState()->getEntityManager()->persist($copyProduct);
            $this->getState()->getEntityManager()->flush();
        /** @var \PSC\Shop\EntityBundle\Document\Product $productDoc */
            $productDoc = $this->getState()->getMongo()
                ->getRepository('PSC\Shop\EntityBundle\Document\Product')
                ->findOneBy(array('uid' => $orgUid));
            if ($productDoc) {
                $d = clone $productDoc;
                $d->setId(null);
                $d->setUid($copyProduct->getUid());
                $this->getState()->getMongo()->detach($d);
                $this->getState()->getMongo()->persist($d);
                $this->getState()->getMongo()->flush();
            }

            if (file_exists('/data/www/old/market/templateprint/orginal/' . $orginalDir . '/product.zip')) {
                if (!file_exists('/data/www/old/market/templateprint/orginal/' . implode('/', $path))) {
                    mkdir('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 0777, true);
                }
                copy('/data/www/old/market/templateprint/orginal/' . $orginalDir . '/product.zip', '/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/product.zip');
            }

            if (file_exists('/data/www/old/market/templateprint/orginal/' . $orginalDir . '/final.pdf')) {
                if (!file_exists('/data/www/old/market/templateprint/orginal/' . implode('/', $path))) {
                    mkdir('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 0777, true);
                }copy('/data/www/old/market/templateprint/orginal/' . $orginalDir . '/final.pdf', '/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/final.pdf');
            }

            if (file_exists('/data/www/old/market/templateprint/orginal/' . $orginalDir . '/preview.pdf')) {
                if (!file_exists('/data/www/old/market/templateprint/orginal/' . implode('/', $path))) {
                    mkdir('/data/www/old/market/templateprint/orginal/' . implode('/', $path), 0777, true);
                }
                copy('/data/www/old/market/templateprint/orginal/' . $orginalDir . '/preview.pdf', '/data/www/old/market/templateprint/orginal/' . implode('/', $path) . '/preview.pdf');
            }

            $articlesgroups = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM article_group_article WHERE article_id = ?', array($orgUid));
            $articlegroupsTemp = $this->getState()->getAdditionalData('articlegroups');
            if (!empty($articlesgroups) && !empty($articlegroupsTemp)) {
                foreach ($articlesgroups as $articlegroup) {
                    if (isset($articlegroup['id'])) {
                        unset($articlegroup['id']);
                    }
                    $articlegroup['article_id'] = $newUid;
                    if (isset($articlegroupsTemp[$articlegroup['articlegroup_id']])) {
                        $articlegroup['articlegroup_id'] = $articlegroupsTemp[$articlegroup['articlegroup_id']];
                        $this->getState()->getEntityManager()->getConnection()->insert('article_group_article', $articlegroup);
                    }
                }
            }
        }
    }
}
