<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

class Queue extends Base
{
    public function DoCopy()
    {
        /** @var \PSC\Shop\EntityBundle\Document\Queue[] $queuesObjs */
        $queuesObjs = $this->getState()->getMongo()
            ->getRepository('PSC\Shop\EntityBundle\Document\Queue')
            ->findBy(array('shop' => (string)$this->getState()->getSourceShop()->getUID()));
        foreach ($queuesObjs as $objQueue) {
            $f = clone $objQueue;
            $f->setId(null);
            $f->setShop($this->getState()->getTargetShop()->getUID());
            $this->getState()->getMongo()->detach($f);
            $this->getState()->getMongo()->persist($f);
        }

        $this->getState()->getMongo()->flush();
    }
}
