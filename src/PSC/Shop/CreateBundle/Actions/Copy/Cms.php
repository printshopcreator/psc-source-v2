<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

class Cms extends Base
{
    public function DoCopy()
    {
        $cms = $this->getState()->getEntityManager()->getConnection()->fetchAllAssociative('SELECT * FROM cms WHERE shop_id = ?', array($this->getState()->getSourceShop()->getId()));
        foreach ($cms as $cm) {
            $orgUid = $cm['id'];
            unset($cm['id']);
            $cm['shop_id'] = $this->getState()->getTargetShop()->getUID();
            $this->getState()->getEntityManager()->getConnection()->insert('cms', $cm);
            $newUid = $this->getState()->getEntityManager()->getConnection()->lastInsertId();
        /** @var \PSC\Shop\EntityBundle\Document\Cms $cmsDoc */
            $cmsDoc = $this->getState()->getMongo()
                ->getRepository('PSC\Shop\EntityBundle\Document\Cms')
                ->findOneBy(array('uid' => (string)$orgUid));
            if ($cmsDoc) {
                $d = clone $cmsDoc;
                $d->setId(null);
                $d->setUid($newUid);
                $this->getState()->getMongo()->detach($d);
                $this->getState()->getMongo()->persist($d);
                $this->getState()->getMongo()->flush();
            }
        }
    }
}
