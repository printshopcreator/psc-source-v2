<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\CreateBundle\Interfaces\Copy;
use PSC\Shop\EntityBundle\Entity\Shop;

abstract class Base implements Copy
{
    private State $state;
/**
     * @return State
     */
    public function getState(): State
    {
        return $this->state;
    }

    /**
     * @param State $state
     */
    public function setState(State $state): void
    {
        $this->state = $state;
    }
}
