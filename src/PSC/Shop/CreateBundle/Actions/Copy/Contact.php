<?php

namespace PSC\Shop\CreateBundle\Actions\Copy;

class Contact extends Base
{
    public function DoCopy()
    {
        $contacts = $entityManager->getConnection()->fetchAll('SELECT * FROM shop_contact WHERE shop_id = ?', array($selectedShop->getId()));
        foreach ($contacts as $contact) {
            if (isset($contact['id'])) {
                unset($contact['id']);
            }

            $contact['uuid'] = Uuid::uuid4();
            $contact['selected'] = 0;
            $contact['shop_id'] = $copyShop->getUID();
            $entityManager->getConnection()->insert('shop_contact', $contact);
        }
    }
}
