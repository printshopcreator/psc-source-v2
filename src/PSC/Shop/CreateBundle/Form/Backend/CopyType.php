<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\CreateBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\CreateBundle\Interfaces\RegistryCopy;
use PSC\Shop\CreateBundle\Service\Registry;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CopyType extends AbstractType
{
    private Registry $registry;
    public function __construct(Registry $registry)
    {
        $this->registry = $registry;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('title', TextType::class, array('label' => 'Name'))
            ->add('domain', TextType::class, array('label' => 'Domain'))
            ->add('article', CheckboxType::class, array('required' => false, 'label' => 'Copyproduct'))
            ->add('articlegroup', CheckboxType::class, array('required' => false, 'label' => 'Copyproductgroups'))
            //->add('contact', CheckboxType::class, array('required' => false, 'label' => 'Assigncustomers'))
            ->add('shipping', CheckboxType::class, array('required' => false, 'label' => 'Copyshippingmethods'))
            ->add('payment', CheckboxType::class, array('required' => false, 'label' => 'Copypaymentmethods'))
            ->add('queues', CheckboxType::class, array('required' => false, 'label' => 'CopyQueues'))
            ->add('cms', CheckboxType::class, array('required' => false, 'label' => 'CopyCMSpages'));
/** @var RegistryCopy $element */
        foreach ($this->registry->all() as $element) {
            $builder->add($element->getType(), CheckboxType::class, array('required' => false, 'label' => $element->getName()));
        }

            $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'shop';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'translation_domain' => 'core_create_create'
        ));
    }
}
