<?php

namespace PSC\Shop\CreateBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationCheckerInterface  */
    public $authorizationChecker = null;
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu->addChild('createShop', array(
                'route' => 'psc_shop_create_backend_start_index',
                'label' => 'Shop erstellen',
                'extras' => array(
                    'icon' => 'fas fa-plus-square',
                    'orderNumber' => 9999
                )
            ));
        }
    }
}
