<?php

namespace PSC\Shop\CreateBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuToolsListener
{
    /** @var null|AuthorizationCheckerInterface  */
    public $authorizationChecker = null;
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }
    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        $menu = $event->getMenu();
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu->addChild('copyShop', array(
                'route' => 'psc_shop_create_backend_copy_index',
                'label' => 'Shop kopieren',
                'extras' => array(
                    'icon' => 'fas fa-copy',
                    'orderNumber' => 9999
                )
            ));
        }
    }
}
