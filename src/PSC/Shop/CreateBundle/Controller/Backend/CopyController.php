<?php

namespace PSC\Shop\CreateBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\Shop\CreateBundle\Actions\Copy\Article;
use PSC\Shop\CreateBundle\Actions\Copy\Articlegroup;
use PSC\Shop\CreateBundle\Actions\Copy\Cms;
use PSC\Shop\CreateBundle\Actions\Copy\Payment;
use PSC\Shop\CreateBundle\Actions\Copy\Shipment;
use PSC\Shop\CreateBundle\Actions\Copy\State;
use PSC\Shop\CreateBundle\Form\Backend\CopyType;
use PSC\Shop\CreateBundle\Service\Registry;
use PSC\Shop\EntityBundle\Document\Queue;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\ResourcePrivilegeShop;
use PSC\Shop\EntityBundle\Entity\ShopAccount;
use PSC\Shop\EntityBundle\Entity\ShopContact;
use PSC\System\SettingsBundle\Service\Shop;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class StartController
 *
 * @package PSC\Shop\Create
 * @Route("/create/copy")
 */
class CopyController extends AbstractController
{
    /**
     * @Template()
     * @Route("/index", name="psc_shop_create_backend_copy_index")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function indexAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, DocumentManager $documentManager, Registry $registrys)
    {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $copyShop */
        $selectedShop = $shopService->getSelectedShop();
        $form = $this->createForm(CopyType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $domainCheck = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findBy(array('host' => $data['domain']));
            if (count($domainCheck) > 0) {
                $form->get('domain')->addError(new FormError("Domain schon vorhanden"));
            } else {
                $state = new State($selectedShop, $entityManager, $documentManager, $this->getUser());
                $state->addAdditionalData('title', $data['title']);
                $state->addAdditionalData('domain', $data['domain']);
                $shopCopy = new \PSC\Shop\CreateBundle\Actions\Copy\Shop();
                $shopCopy->setState($state);
                $shopCopy->DoCopy();
                if ($data['articlegroup']) {
                    $copyProductgroups = new Articlegroup();
                    $copyProductgroups->setState($state);
                    $copyProductgroups->DoCopy();
                }

                if ($data['article']) {
                    $copyProducts = new Article();
                    $copyProducts->setState($state);
                    $copyProducts->DoCopy();
                }

                if ($data['cms']) {
                    $copyCms = new Cms();
                    $copyCms->setState($state);
                    $copyCms->DoCopy();
                }

                if ($data['payment']) {
                    $copyPayment = new Payment();
                    $copyPayment->setState($state);
                    $copyPayment->DoCopy();
                }

                if ($data['shipping']) {
                    $copyShipment = new Shipment();
                    $copyShipment->setState($state);
                    $copyShipment->DoCopy();
                }

                if ($data['queues']) {
                    $copyQueues = new \PSC\Shop\CreateBundle\Actions\Copy\Queue();
                    $copyQueues->setState($state);
                    $copyQueues->DoCopy();
                }

                foreach ($registrys->all() as $element) {
                    if ($data[$element->getType()]) {
                        $element->setState($state);
                        $element->DoCopy();
                    }
                }

                return $this->redirect($this->generateUrl("psc_backend_dashboard_index"));
            }
        }

        return array(
            'form' => $form->createView(),
            'elements' => $registrys->all()
        );
    }
}
