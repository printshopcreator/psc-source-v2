<?php

namespace PSC\Shop\CreateBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use PSC\Libraries\AutoRegistryBundle\Service\ConfigurableElementInterface;
use PSC\Shop\CreateBundle\Form\Backend\CreateType;
use PSC\Shop\EntityBundle\Entity\Account;
use PSC\Shop\EntityBundle\Entity\Domain;
use PSC\Shop\EntityBundle\Entity\ResourcePrivilegeShop;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\ShopAccount;
use PSC\Shop\EntityBundle\Entity\ShopContact;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class StartController
 *
 * @package PSC\Shop\Create
 * @Route("/start")
 */
class StartController extends AbstractController
{
    /**
     * @Template()
     * @Route("/index", name="psc_shop_create_backend_start_index")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function indexAction(
        Request $request,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager
    ) {
        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $form = $this->createForm(CreateType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $domainCheck = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findBy(array('host' => $data['domain']));
            if (count($domainCheck) > 0) {
                $form->get('domain')->addError(new FormError("Domain schon vorhanden"));
            } else {
                $account = new Account();
                $account->setTitle('No Account');
                $account->setInstall($selectedShop->getInstall());
                $account->setParentId(0);
                $shop = new Shop();
                $shop->setUuid(Uuid::uuid4());
                $shop->setInstall($selectedShop->getInstall());
                $shop->setCustomTemplates(true);
                $shop->setMarket(false);
                $shop->setDisplayMarket(true);
                $shop->setPrivate(false);
                $shop->setTitle($data['title']);
                $shop->setLayout('bootstrap3');
                $shop->setDefaultFunc('index');
                $shop->setDefaultParam('all');
                $shop->setCreatorId($this->getUser()->getId());
                $shop->setUseEmailAsLogin(true);
                $shop->setNoVerify(true);
                $shop->setKeywords("shop");
                $shop->setAuthor("shop");
                $shop->setDescription("shop");
                $shop->setCopyright("shop");
                $shop->setShippingMode(1);
                $shop->setBetreiberName($selectedShop->getBetreiberName());
                $shop->setBetreiberAddress($selectedShop->getBetreiberAddress());
                $shop->setBetreiberStreet($selectedShop->getBetreiberStreet());
                $shop->setBetreiberEmail($selectedShop->getBetreiberEmail());
                $shop->setTemplateDisplayProducts(true);
                $shop->setTemplateDisplayMotive(false);
                $shop->setDefaultAccount($account);
                $accountshop = new ShopAccount();
                $accountshop->setAccount($account);
                $accountshop->setShop($shop);
                $domain = new Domain($shop);
                $domain->setHost($data['domain']);
                $shopContact1 = new ShopContact();
                $shopContact1->setContact($this->getUser());
                $shopContact1->setShop($shop);
                $shopContact1->setAdmin(true);
                $shopContact1->setLogin(true);
                $entityManager->persist($account);
                $entityManager->flush();
                $entityManager->persist($shop);
                $entityManager->flush();
                $entityManager->persist($accountshop);
                $entityManager->persist($shopContact1);
                $contactPSC = $entityManager
                ->getRepository('PSC\Shop\EntityBundle\Entity\Contact')->findOneBy(['uid' => 5]);
                if ($contactPSC) {
                    $shopContact2 = new ShopContact();
                    $shopContact2->setContact($contactPSC);
                    $shopContact2->setShop($shop);
                    $shopContact2->setAdmin(true);
                    $shopContact2->setLogin(true);
                    $entityManager->persist($shopContact2);
                }

                $entityManager->persist($domain);
                $temp = array(8, 9, 10, 13, 23, 28, 31, 32, 33, 34, 35, 38, 46, 48, 49, 50, 51);
                foreach ($temp as $key) {
                    $resprevshop = new ResourcePrivilegeShop();
                    $resprevshop->setShop($shop);
                    $resprevshop->setResourceprivilege($key);
                    $entityManager->persist($resprevshop);
                }
                $entityManager->flush();
                return $this->redirect($this->generateUrl("psc_backend_dashboard_index"));
            }
        }

        return array(
            'form' => $form->createView()
        );
    }

    protected function sync()
    {

//        $instances = $mongoDb
//            ->getRepository('PSCEntityBundle:Instance')
//            ->findAll();
//
//        $client = new Client();
//
//        foreach($instances as $instance) {
//            $domains = $entityManager
//                ->getRepository('PSC\Shop\EntityBundle\Entity\Domain')->findAll();
//
//            $temp = array();
//
//            /** @var Domain $domain */
//            foreach($domains as $domain) {
//                $temp[] = array('name' => strtolower($domain->getHost()), 'letsEncrypt' => $domain->isLetsEncrypt());
//            }
//
//            $response = $client->put($instance->getSyncServer() . '/index.php/json/push/domains',array(
//                    'json' => array(
//                        'uuid' => $instance->getAppId(),
//                        'data' => $temp
//                    )
//                )
//            );
//        }
    }
}
