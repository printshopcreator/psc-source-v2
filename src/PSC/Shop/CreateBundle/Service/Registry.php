<?php

namespace PSC\Shop\CreateBundle\Service;

use LogicException;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryInterface;
use PSC\Shop\CreateBundle\Interfaces\RegistryCopy;

class Registry implements RegistryInterface
{
    /** @var  RegistryCopy[] */
    private $elements = array();
/** @return RegistryCopy[] */
    public function all()
    {
        return $this->elements;
    }

    /**
     * @param $key string
     *
     * @return RegistryCopy
     */
    public function get($key)
    {
        if (!$this->has($key)) {
            throw new LogicException('Copy ' . $key . ' not registered');
        }

        return $this->elements[$key];
    }

    public function has($type)
    {
        return array_key_exists($type, $this->elements);
    }

    /**
     * @param RegistryElementInterface $element
     */
    public function add(RegistryElementInterface $element)
    {
        if ($this->has($element->getType())) {
            throw new LogicException('Copy ' . $element->getType() . ' already registered');
        }

        $this->elements[$element->getType()] = $element;
    }

    /**
     * @param RegistryElementInterface $element
     */
    public function remove(RegistryElementInterface $element)
    {
        if (!$this->has($element->getType())) {
            throw new LogicException('Copy ' . $element->getType() . ' not registered');
        }

        unset($this->elements[$element->getType()]);
    }
}
