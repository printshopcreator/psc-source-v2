<?php

namespace PSC\Shop\PaymentBundle\Provider;

use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

interface PaymentProviderInterface
{
    public function getType();
    public function handleNotify(Request $request);
    public function getName();
    public function getTemplate();
    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder);
    public function saveDocument(Gatewaysettings $settings, Form $form);
}
