<?php

namespace PSC\Shop\PaymentBundle\Provider;

use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\HttpFoundation\Request;

class PaymentProvider implements PaymentProviderInterface
{
    /** @var Shop */
    protected $shopEntity = null;
/** @var \PSC\Shop\EntityBundle\Document\Shop */
    protected $shopDoc = null;
/** @var Gatewaysettings */
    protected $gatewaySettings = null;
    public function getType()
    {
        // TODO: Implement getType() method.
    }

    public function handleNotify(Request $request)
    {
        // TODO: Implement handleNotify() method.
    }

    public function getName()
    {
        // TODO: Implement getName() method.
    }

    public function getTemplate()
    {
        // TODO: Implement getTemplate() method.
    }

    public function getSubForm(Gatewaysettings $settings, FormBuilder $builder)
    {
        // TODO: Implement getSubForm() method.
    }

    public function saveDocument(Gatewaysettings $settings, Form $form)
    {
        // TODO: Implement saveDocument() method.
    }

    /**
     * @return Shop
     */
    public function getShopEntity()
    {
        return $this->shopEntity;
    }

    /**
     * @param Shop $shopEntity
     */
    public function setShopEntity($shopEntity)
    {
        $this->shopEntity = $shopEntity;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Document\Shop
     */
    public function getShopDoc()
    {
        return $this->shopDoc;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Document\Shop $shopDoc
     */
    public function setShopDoc($shopDoc)
    {
        $this->shopDoc = $shopDoc;
    }

    /**
     * @return Gatewaysettings
     */
    public function getGatewaySettings()
    {
        return $this->gatewaySettings;
    }

    /**
     * @param Gatewaysettings $gatewaySettings
     */
    public function setGatewaySettings($gatewaySettings)
    {
        $this->gatewaySettings = $gatewaySettings;
    }

    protected function getHost()
    {
        $is_https = false;
        if ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) {
            $is_https = true;
        }
        $http_protocol = $is_https ? 'https' : 'http';
        return $http_protocol . '://' . $_SERVER['HTTP_HOST'];
    }
}
