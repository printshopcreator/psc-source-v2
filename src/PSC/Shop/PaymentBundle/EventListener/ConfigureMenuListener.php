<?php

namespace PSC\Shop\PaymentBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class ConfigureMenuListener
{
    /** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $submenu = $menu->addChild('Zahlarten', array(
                'route' => 'psc_shop_payment_backend_list',
                'extras' => array(
                    'icon' => 'fas fa-credit-card',
                    'orderNumber' => 23,
                    'routes' => array('psc_shop_payment_backend_create', 'psc_shop_payment_backend_edit')
                )
            ));
        }
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureGateway(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $menu->addChild('Payment Gateways', array(
                'route' => 'psc_shop_payment_backend_gateway_settings',
                'extras' => array(
                    'icon' => 'fas fa-credit-card',
                    'routes' => array('psc_shop_payment_backend_gateway_settings')
                )
            ));
        }
    }
}
