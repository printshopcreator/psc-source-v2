<?php

namespace PSC\Shop\PaymentBundle\Model;

use OpenApi\Annotations as OA;

class Payment
{
    /**
     * @OA\Property(type="integer")
     */
    private int $id;
/**
     * @OA\Property(type="string")
     */
    private string $title;
/**
     * @OA\Property(type="string")
     */
    private string $description;
/**
     * @OA\Property(type="number", format="double")
     */
    private float $price;
/**
     * @OA\Property(type="boolean")
     */
    private bool $percent;
/**
     * @OA\Property(type="integer")
     */
    private int $taxClass;

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getTitle(): string
    {
        return $this->title;
    }
}
