<?php

namespace PSC\Shop\PaymentBundle\Dto\All;

use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use PSC\Shop\PaymentBundle\Model\Payment;

class Output
{
    public function __construct(array $payments)
    {
        $this->data = $payments;
        $this->count = count($payments);
    }

    /**
     * @var Payment[]
     *
     * @OA\Property(type="array", @OA\Items(ref=@Model(type=\PSC\Shop\PaymentBundle\Model\Payment::class)))
     */
    public array $data = [];

    /**
     * @var int
     *
     * @OA\Property(type="integer")
     */
    public int $count = 0;
}
