<?php

namespace PSC\Shop\PaymentBundle\Document;

use PSC\Shop\EntityBundle\Entity\Payment;
use PSC\System\SettingsBundle\Document\History as PSCHistory;
use PSC\System\SettingsBundle\Interfaces\History as AliasedHistory;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;

/**
 * @Document(collection="payment_history")
 */
class History extends PSCHistory
{
    /**
     * @Field(type="string")
     */
    private string $payment;

    function __construct(string $payment = "")
    {
        parent::__construct();
        $this->payment = $payment;
    }

    public function getPayment()
    {
        return $this->payment;
    }

    public function setPayment(string $payment)
    {
        $this->payment = $payment;
    }

    public function getReferenceField(): string
    {
        return 'payment';
    }
}
