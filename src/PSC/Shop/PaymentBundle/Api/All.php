<?php

namespace PSC\Shop\PaymentBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Payment;
use PSC\Shop\PaymentBundle\Dto\All\Output;
use PSC\Shop\PaymentBundle\Model\Payment as PSCPayment;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class All extends AbstractController
{
    private EntityManagerInterface $entityManager;

    private Shop $shopService;

    private TokenStorageInterface $tokenStorage;

    public function __construct(EntityManagerInterface $entityManager, Shop $shopService, TokenStorageInterface $tokenStorage)
    {
        $this->entityManager = $entityManager;
        $this->shopService = $shopService;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * get all payments
     *
     * @Route("/", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="payments",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\PaymentBundle\Dto\All\Output::class))
     * )
     * @OA\Tag(name="Payment")
     */
    public function all(): JsonResponse
    {
        $output = [];
        $result = $this->entityManager->getRepository(Payment::class)->findBy(['shop' => $this->shopService->getShopByDomain(), 'private' => 0]);

        /** Payment $payment */
        foreach ($result as $payment) {
            $payObj = new PSCPayment();
            $payObj->setId($payment->getUid());
            $payObj->setTitle($payment->getTitle());
            $output[] = $payObj;
        }

        return $this->json(new Output($output));
    }

    /**
     * get all payments
     *
     * @Route("/my", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="get my payments",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\PaymentBundle\Dto\All\Output::class))
     * )
     * @OA\Tag(name="Payment")
     * @IsGranted("ROLE_USER")
     * @Security(name="Bearer")
     */
    public function my(): JsonResponse
    {
        $output = [];
        $result = $this->entityManager->getRepository(Payment::class)->findBy(['shop' => $this->shopService->getShopByDomain(), 'private' => 0]);

        /** Payment $payment */
        foreach ($result as $payment) {
            $payObj = new PSCPayment();
            $payObj->setId($payment->getUid());
            $payObj->setTitle($payment->getTitle());
            $output[] = $payObj;
        }

        $user = $this->tokenStorage->getToken()->getUser();
        foreach ($user->payments as $payment) {
            if ($payment->getShop()->getId() == $this->shopService->getShopByDomain()->getId()) {
                $payObj = new PSCPayment();
                $payObj->setId($payment->getUid());
                $payObj->setTitle($payment->getTitle());
                $output[] = $payObj;
            }
        }
        return $this->json(new Output($output));
    }
}
