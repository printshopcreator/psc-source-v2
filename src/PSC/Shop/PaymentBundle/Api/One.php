<?php

namespace PSC\Shop\PaymentBundle\Api;

use Doctrine\ORM\EntityManagerInterface;
use PSC\Component\ApiBundle\Dto\Error\NotFound;
use PSC\Shop\EntityBundle\Entity\Payment as PSCPayment;
use PSC\Shop\PaymentBundle\Model\Payment;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

class One extends AbstractController
{
    private EntityManagerInterface $entityManager;
    private Shop $shopService;
    public function __construct(EntityManagerInterface $entityManager, Shop $shopService)
    {
        $this->entityManager = $entityManager;
        $this->shopService = $shopService;
    }

    /**
     * get one payment
     *
     * @Route("/{id}", methods={"GET"})
     * @OA\Response(
     *     response=200,
     *     description="orders",
     *     @OA\JsonContent(ref=@Model(type=\PSC\Shop\PaymentBundle\Model\Payment::class))
     * )
     * @OA\Tag(name="Payment")
     */
    public function one(string $id)
    {
        $result = $this->entityManager->getRepository(PSCPayment::class)->findOneBy(['shop' => $this->shopService->getShopByDomain(), 'private' => 0, 'uid' => $id]);
        if ($result) {
            $output = new Payment();
            $output->setTitle($result->getTitle());
            $output->setId($result->getUid());
        } else {
            $output = new NotFound();
        }
        return $this->json($output);
    }
}
