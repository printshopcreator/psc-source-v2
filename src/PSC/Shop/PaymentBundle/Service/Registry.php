<?php

namespace PSC\Shop\PaymentBundle\Service;

use LogicException;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryElementInterface;
use PSC\Libraries\AutoRegistryBundle\Service\RegistryInterface;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;

class Registry
{
    /** @var  PaymentProviderInterface[] */
    private $paymentProvider = array();
/** @return PaymentProviderInterface[] */
    public function all()
    {
        return $this->paymentProvider;
    }

    /**
     * @param $key string
     *
     * @return PaymentProviderInterface
     */
    public function get($key)
    {
        if (!$this->has($key)) {
            throw new LogicException('Exporter ' . $key . ' not registered');
        }

        return $this->paymentProvider[$key];
    }

    public function has($type)
    {
        return array_key_exists($type, $this->paymentProvider);
    }

    /**
     * @param PaymentProviderInterface $element
     */
    public function add(PaymentProviderInterface $element)
    {
        if ($this->has($element->getType())) {
            throw new LogicException('Exporter ' . $element->getType() . ' already registered');
        }

        $this->paymentProvider[$element->getType()] = $element;
    }

    /**
     * @param PaymentProviderInterface $element
     */
    public function remove(PaymentProviderInterface $element)
    {
        if (!$this->has($element->getType())) {
            throw new LogicException('Exporter ' . $element->getType() . ' not registered');
        }

        unset($this->paymentProvider[$element->getType()]);
    }
}
