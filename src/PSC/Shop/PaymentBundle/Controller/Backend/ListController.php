<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\PaymentBundle\Controller\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * PaymentController fürs Backend
 *
 * @package    PSC\Shop\Payment
 * @subpackage Controller
 */
class ListController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/list/index", name="psc_shop_payment_backend_list")
     * @Template()
     *
     * @param Request $request
     * @param Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param PaginatorInterface $paginator
     * @return array
     * @throws \Doctrine\ORM\ORMException
     */
    public function indexAction(Request $request, Shop $shopService, EntityManagerInterface $entityManager, PaginatorInterface $paginator)
    {

        $selectedShop = $shopService->getSelectedShop();
        $userRepository = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Payment');
        $qb = $userRepository->createQueryBuilder('payment')
            ->andWhere('payment.shop = :shop_id')
            ->setParameter("shop_id", $selectedShop->getUid());
        $pagination = $paginator->paginate($query = $qb->getQuery(), $request->query->getInt('page', 1), 15);
        return array(
            'pagination' => $pagination
        );
    }
}
