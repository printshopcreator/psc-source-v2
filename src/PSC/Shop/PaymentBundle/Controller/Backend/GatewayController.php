<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\PaymentBundle\Controller\Backend;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Service\Registry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * GatewayController fürs Backend
 *
 * @package    PSC\Shop\Payment
 * @subpackage Controller
 */
class GatewayController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/gateway/settings", name="psc_shop_payment_backend_gateway_settings")
     * @Template()
     *
     * @param Request $request
     * @param FormFactoryInterface $formFactory
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param Registry $registry
     * @param DocumentManager $documentManager
     * @return array|View
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function settingsAction(Request $request, FormFactoryInterface $formFactory, \PSC\System\SettingsBundle\Service\Shop $shopService, Registry $registry, DocumentManager $documentManager)
    {

        /** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $builder = $this->createFormBuilder(null, array('action' => $this->generateUrl('psc_shop_payment_backend_gateway_settings', array())));
        foreach ($registry->all() as $gateway) {
            $gatewaySettings = $documentManager
                ->getRepository('PSC\Shop\PaymentBundle\Document\Gatewaysettings')
                ->findOneBy(array('gatewayType' => (string)$gateway->getType(), 'shop' => (string)$selectedShop->getId()));
            if (!$gatewaySettings) {
                $gatewaySettings = new Gatewaysettings();
                $gatewaySettings->setShop($selectedShop->getId());
                $gatewaySettings->setGatewayType($gateway->getType());
            }

            $builder->add($gateway->getSubForm($gatewaySettings, $formFactory->createNamedBuilder($gateway->getType())));
        }

        $builder->add('submit', SubmitType::class, array('label' => 'Speichern'));
        $form = $builder->getForm();
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            foreach ($registry->all() as $gateway) {
                $gatewaySettings = $documentManager
                    ->getRepository('PSC\Shop\PaymentBundle\Document\Gatewaysettings')
                    ->findOneBy(array('gatewayType' => (string)$gateway->getType(), 'shop' => (string)$selectedShop->getId()));
                if (!$gatewaySettings) {
                    $gatewaySettings = new Gatewaysettings();
                    $gatewaySettings->setShop($selectedShop->getId());
                    $gatewaySettings->setGatewayType($gateway->getType());
                }

                $gateway->saveDocument($gatewaySettings, $form);
                $documentManager->persist($gatewaySettings);
            }
            $documentManager->flush();
        }

        return array(
            'form' => $form->createView(),
            'gateways' => $registry->all()
        );
    }
}
