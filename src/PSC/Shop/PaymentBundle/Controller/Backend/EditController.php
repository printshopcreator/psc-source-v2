<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\PaymentBundle\Controller\Backend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Document\Payment as PSCPayment;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Payment;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\History as PSCHistory;
use PSC\Shop\PaymentBundle\Form\Backend\DeleteType;
use PSC\Shop\PaymentBundle\Form\Backend\PaymentType;
use PSC\Shop\PaymentBundle\PSCShopPaymentBundle;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Document\LogEntry;
use PSC\System\SettingsBundle\Service\History;
use PSC\System\SettingsBundle\Service\Log;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * EditController fürs Payment
 *
 * @package    PSC\Shop\Payment
 * @subpackage Controller
 */
class EditController extends AbstractController
{
    /**
     * @var Log
     */
    private Log $logService;
    private History $historyService;
    public function __construct(Log $logService, History $historyService)
    {
        $this->logService = $logService;
        $this->historyService = $historyService;
    }

    /**
     * Create
     *
     * @Route("/edit/create", name="psc_shop_payment_backend_create")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param SessionInterface $session
     * @param DocumentManager $documentManager
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function createAction(Request $request, Field $fieldService, \PSC\System\SettingsBundle\Service\Shop $shopService, EntityManagerInterface $entityManager, SessionInterface $session, DocumentManager $documentManager)
    {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Shipping);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Shipping);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $payment = new Payment();
        $payment->setShop($selectedShop);
        $payment->setInstall($selectedShop->getInstall());
        $form = $this->createForm(PaymentType::class, $payment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($payment);
            $entityManager->flush();
            $paymentDoc = new \PSC\Shop\EntityBundle\Document\Payment();
            $paymentDoc->setUid($payment->getUID());
            $paymentDoc->setPluginSettings($payment->getPluginSettings());
            $paymentDoc->setShippings($payment->getShippings());
            $paymentDoc->setPaymentGateway($payment->getPaymentGateway());
            $paymentDoc->setPaymentGatewaySettings($payment->getPaymentGatewaySettings());
            $documentManager->persist($paymentDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Payment \'' . $payment->getTitle() . '\' has been created!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopPaymentBundle::class, $payment->getTitle(), "Payment saved");
            return $this->redirectToRoute('psc_shop_payment_backend_list');
        }

        return array(
            'payment' => $payment,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups
        );
    }

    /**
     * edit
     *
     * @Route("/edit/edit/{uid}", name="psc_shop_payment_backend_edit")
     * @Template()
     *
     * @param Request $request
     * @param Field $fieldService
     * @param \PSC\System\SettingsBundle\Service\Shop $shopService
     * @param EntityManagerInterface $entityManager
     * @param DocumentManager $documentManager
     * @param SessionInterface $session
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Doctrine\ODM\MongoDB\MongoDBException
     * @throws \Doctrine\ORM\ORMException
     */
    public function editAction(
        Request $request,
        Field $fieldService,
        \PSC\System\SettingsBundle\Service\Shop $shopService,
        EntityManagerInterface $entityManager,
        DocumentManager $documentManager,
        SessionInterface $session,
        $uid
    ) {
        $customFields = $fieldService->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Payment);
        $customGroups = $fieldService->getGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Payment);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
/** @var Payment $payment */
        $payment = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Payment')->findOneBy(array('uid' => $uid, 'shop' => $selectedShop));
/** @var \PSC\Shop\EntityBundle\Document\Payment $paymentDoc */
        $paymentDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Payment')
            ->findOneBy(array('uid' => (string)$payment->getUid()));
        if (!$paymentDoc) {
            $paymentDoc = new \PSC\Shop\EntityBundle\Document\Payment();
            $paymentDoc->setUid($payment->getUID());
        }

        if (!$request->isMethod('POST')) {
            $payment->setShippings($paymentDoc->getShippings());
            $payment->setPluginSettings($paymentDoc->getPluginSettings());
            $payment->setPaymentGateway($paymentDoc->getPaymentGateway());
            $payment->setPaymentGatewaySettings($paymentDoc->getPaymentGatewaySettings());
        }

        $form = $this->createForm(PaymentType::class, $payment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $paymentDoc->setPluginSettings($payment->getPluginSettings());
            $paymentDoc->setShippings($payment->getShippings());
            $paymentDoc->setPaymentGateway($payment->getPaymentGateway());
            $paymentDoc->setPaymentGatewaySettings($payment->getPaymentGatewaySettings());
            $hist = new PSCHistory((string)$payment->getUID());
            $this->historyService->createHistoryEntry($hist, $payment, $paymentDoc);
            $entityManager->persist($payment);
            $entityManager->flush();
            $documentManager->persist($paymentDoc);
            $documentManager->flush();
            $session->getFlashBag()->add('success', 'Payment \'' . $payment->getTitle() . '\' has been upgraded!');
            $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopPaymentBundle::class, $payment->getTitle(), "Payment saved");
        // return $this->redirectToRoute('psc_shop_payment_backend_list');
        }

        return array(
            'payment' => $payment,
            'form' => $form->createView(),
            'customFields' => $customFields,
            'customGroups' => $customGroups,
            'changes' => $this->historyService->getHistory(new PSCHistory(), (string)$payment->getUID())
        );
    }

    /**
     * remove
     *
     * @Route("/edit/delete/{uid}", name="psc_shop_payment_backend_delete")
     * @Template()
     *
     * @param Request $request
     * @param SessionInterface $session
     * @param EntityManagerInterface $entityManager
     * @param $uid
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, SessionInterface $session, EntityManagerInterface $entityManager, $uid)
    {
        /** @var Payment $payment */
        $payment = $entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Payment')->findOneBy(['uid' => $uid]);
/** @var \PSC\Shop\EntityBundle\Entity\Shop $selectedShop */
        $selectedShop = $shopService->getSelectedShop();
        $form = $this->createForm(DeleteType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getClickedButton()->getName() == 'yes') {
                $title = $payment->getTitle();
                $entityManager->remove($payment);
                $entityManager->flush();
                $session->getFlashBag()->add('success', 'Payment \'' . $title . '\' has been deleted!');
                $this->logService->createLogEntry($selectedShop, $this->getUser(), LogEntry::INFO, PSCShopPaymentBundle::class, $title, "Payment deleted");
                return $this->redirectToRoute('psc_shop_payment_backend_list');
            }
            return $this->redirectToRoute('psc_shop_payment_backend_list');
        }


        return array(
            'payment' => $payment,
            'form' => $form->createView()
        );
    }
}
