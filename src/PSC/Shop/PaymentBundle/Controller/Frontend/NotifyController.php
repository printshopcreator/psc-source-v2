<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\PaymentBundle\Controller\Frontend;

use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\PaymentBundle\Document\Gatewaysettings;
use PSC\Shop\PaymentBundle\Provider\PaymentProvider;
use PSC\Shop\PaymentBundle\Provider\PaymentProviderInterface;
use PSC\Shop\PaymentBundle\Service\Registry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * NotifyController für Payment
 *
 * @package    PSC\Shop\Payment
 * @subpackage Controller
 */
class NotifyController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/notify/{apiKey}/{typ}", name="psc_shop_payment_backend_notify")
     * @Template()
     *
     * @param Request $request
     * @param Registry $registry
     * @param DocumentManager $documentManager
     * @param EntityManagerInterface $entityManager
     * @param $apiKey
     * @param $typ
     * @return View|Response
     * @throws \Exception
     */
    public function indexAction(Request $request, Registry $registry, DocumentManager $documentManager, EntityManagerInterface $entityManager, $apiKey, $typ)
    {
        if ($typ == '') {
            throw new \Exception('Kein Payment angegeben');
        }

        if (!$registry->has($typ)) {
            throw new \Exception('Provider nicht gefunden');
        }

        /** @var PaymentProvider $provider */
        $provider = $registry->get($typ);
/** @var \PSC\Shop\EntityBundle\Document\Shop $order */
        $shopDoc = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('apiKey' => $apiKey));
/** @var Shop $order */
        $shopEntity = $entityManager
            ->getRepository('PSC\Shop\EntityBundle\Entity\Shop')
            ->findOneBy(array('uid' => (string)$shopDoc->getUid()));
/** @var Gatewaysettings $gwSettings */
        $gwSettings = $documentManager
            ->getRepository('PSC\Shop\PaymentBundle\Document\Gatewaysettings')
            ->findOneBy(array('shop' => (string)$shopEntity->getId(), 'gatewayType' => $typ));
        $provider->setShopEntity($shopEntity);
        $provider->setShopDoc($shopDoc);
        $provider->setGatewaySettings($gwSettings);
        $provider->handleNotify($request);
        return new Response();
    }
}
