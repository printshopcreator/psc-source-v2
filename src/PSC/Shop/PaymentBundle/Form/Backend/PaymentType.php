<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\PaymentBundle\Form\Backend;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Norzechowicz\AceEditorBundle\Form\Extension\AceEditor\Type\AceEditorType;
use PSC\Backend\DashboardBundle\Form\Type\DatePickerType;
use PSC\Shop\MediaBundle\Form\Type\ImageType;
use PSC\Shop\PaymentBundle\Service\Registry;
use PSC\System\PluginBundle\Form\Chain\Field;
use PSC\System\SettingsBundle\Service\Shop;
use PSC\System\SettingsBundle\Service\Tax;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Trsteel\CkeditorBundle\Form\Type\CkeditorType;

class PaymentType extends AbstractType
{
    /** @var Field  */
    protected $fields;
    protected $formFactory;
    protected $entityManager;
    protected $shopService;
    protected $paymentRegistry;
/**
     * @var Tax
     */
    private Tax $taxService;
    public function __construct(Field $fields, FormFactoryInterface $formFactory, Shop $shop, EntityManagerInterface $entityManager, Registry $paymentRegistry, Tax $taxService)
    {
        $this->fields = $fields;
        $this->formFactory = $formFactory;
        $this->shopService = $shop;
        $this->entityManager = $entityManager;
        $this->paymentRegistry = $paymentRegistry;
        $this->taxService = $taxService;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $shippings = $this->entityManager->getRepository('PSC\Shop\EntityBundle\Entity\Shipping')->findBy(array("shop" => $this->shopService->getSelectedShop()->getUid(), "enable" => 1));
        $tmpShipping = [];
        foreach ($shippings as $shipping) {
            $tmpShipping[$shipping->getTitle()] = $shipping->getUid();
        }

        $tmpGateways = ['Noconnection' => ''];
        foreach ($this->paymentRegistry->all() as $payment) {
            $tmpGateways[$payment->getName()] = $payment->getType();
        }

        $builder
            ->add('title', TextType::class, array('label' => 'Name', 'required' => true))
            ->add('price', MoneyType::class, array('label' => 'cost', 'required' => true))
            ->add('pos', IntegerType::class, array('label' => 'Position', 'required' => true))
            ->add('percent', CheckboxType::class, array('label' => 'Prozentual', 'required' => false))
            ->add('private', CheckboxType::class, array('label' => 'Private', 'required' => false))
            ->add('enable', CheckboxType::class, array('label' => 'Active', 'required' => false))
            ->add('description', TextareaType::class, array('label' => 'description', 'required' => false))
            ->add('trustedShopName', TextType::class, array('label' => 'TrustedShopName', 'required' => false))
            ->add('paymentGateway', ChoiceType::class, array(
                'label' => 'PaymentGateway',
                'required' => false,
                'empty_data' => '',
                'choices' => $tmpGateways
            ))
            ->add('taxClass', ChoiceType::class, array(
                'choices' => $this->taxService->getTaxesForForm(),
                'label' => 'taxclass'))
            ->add('shippings', ChoiceType::class, array(
                'choices' => $tmpShipping,
                'multiple' => true,
                'label' => 'shippingmethods',
            ));
/** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Payment) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup()), $options));
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Payment) as $field) {
                $field->formPreSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Payment) as $field) {
                $field->formPostSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Payment) as $field) {
                $field->formPostSubmit($event);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Payment) as $field) {
                $field->formPreSubmit($event);
            }
        });
        $builder->add('save', SubmitType::class, array('label' => 'save'));
    }

    public function getName()
    {
        return 'payment';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PSC\Shop\EntityBundle\Entity\Payment',
            'translation_domain' => 'core_payment_edit'
        ));
    }
}
