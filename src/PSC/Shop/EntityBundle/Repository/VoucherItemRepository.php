<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Voucher;

/**
 * VoucherItemRepository
 *
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class VoucherItemRepository extends EntityRepository
{
    public function deleteAllByVoucher(Voucher $voucher)
    {
        $isDeleted = $this->createQueryBuilder("item")
            ->delete()
            ->where('item.voucher  = :voucherId')->setParameter("voucherId", $voucher->getUid())
            ->getQuery()->execute();
        return $isDeleted;
    }
}
