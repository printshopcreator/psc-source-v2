<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * CmsRepository
 *
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class CmsRepository extends EntityRepository
{
    public function findAllByShop(Shop $shop)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT c FROM PSCEntityBundle:Cms c
                WHERE c.shop = :shop_id')->setParameter('shop_id', $shop->getId());
        try {
            return $query->execute();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
