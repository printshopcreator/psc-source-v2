<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Contact;

/**
 * InstallRepository
 *
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class InstallRepository extends EntityRepository
{
}
