<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use DateTime;
use Doctrine\ORM\EntityRepository;

/**
 * RoleRepository
 *
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class RoleRepository extends EntityRepository
{
}
