<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * PaymentRepository
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class PaymentRepository extends EntityRepository
{
    public function updateTaxes($shop, $from, $to)
    {

        $this->getEntityManager()
            ->createQuery('
                UPDATE PSCEntityBundle:Payment s SET s.taxClass=:to
                WHERE s.taxClass = :from AND s.shop = :shop_id')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('shop_id', $shop)
            ->execute();
        return true;
    }
}
