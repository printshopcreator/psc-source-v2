<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ShippingRepository
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class ShippingRepository extends EntityRepository
{
    /**
     * Gibt alle Versandarten zurück;
     *
     * @param int $shopId Shopid
     *
     * @return Collection
     */
    public function getAllByShopIdWithPagination($shopId)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s FROM PSCEntityBundle:Shipping s
                WHERE s.shop = :shop_id')->setParameter('shop_id', $shopId);
        try {
            return $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * Gibt die Anzahl aller Versandarten zurück;
     *
     * @param int $shopId Shopid
     *
     * @return int
     */
    public function getAllByShopId($shopId)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s FROM PSCEntityBundle:Shipping s
                WHERE s.shop = :shop_id')->setParameter('shop_id', $shopId);
        try {
            return count($query->execute());
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    public function updateTaxes($shop, $from, $to)
    {

        $this->getEntityManager()
            ->createQuery('
                UPDATE PSCEntityBundle:Shipping s SET s.taxClass=:to
                WHERE s.taxClass = :from AND s.shop = :shop_id')
            ->setParameter('from', $from)
            ->setParameter('to', $to)
            ->setParameter('shop_id', $shop)
            ->execute();
        return true;
    }
}
