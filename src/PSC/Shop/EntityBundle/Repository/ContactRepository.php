<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Shop;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

/**
 * ContactRepository
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class ContactRepository extends EntityRepository implements UserLoaderInterface
{
    public function loadUserByUsername($username)
    {
        $user = $this->createQueryBuilder('c')
            ->where('c.username = :username AND c.enable = 1')
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
        if (null === $user) {
            $message = sprintf('Unable to find an active admin AppBundle:User object identified by "%s".', $username);
            throw new UsernameNotFoundException($message);
        }

        return $user;
    }

    /**
     * Gibt einen Kunden anhand seiner Logindaten zurück
     *
     * @param string $mail     Mailadresse
     * @param string $password Password
     * @param int    $shopId   Shopid
     *
     * @return Contact
     */
    public function getContactByEmailAndPasswordAndShop($mail, $password, $shopId)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s FROM PSCEntityBundle:ShopContact s
                JOIN s.contact c
                WHERE c.enable = 1 AND c.username = :username AND c.password = :password AND s.shop = :shop_id')->setParameter('username', $mail)
            ->setParameter('password', $password)
            ->setParameter('shop_id', $shopId);
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * Schaut ob ein Contact mit der Mail existiert
     *
     * @param string $mail   Mailadresse
     * @param int    $shopId Shopid
     *
     * @return Contact
     */
    public function getContactByEmailAndShop($mail, $shopId)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s FROM PSCEntityBundle:ShopContact s
                JOIN s.contact c
                WHERE c.enable = 1 AND c.username = :username AND s.shop = :shop_id')->setParameter('username', $mail)
            ->setParameter('shop_id', $shopId);
        try {
            return $query->getSingleResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * Gibt alle Contacts zu Shop
     *
     * @param string $mail   Mailadresse
     * @param int    $shopId Shopid
     *
     * @return Contact
     */
    public function getContactsByShop(Shop $shop)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s FROM PSCEntityBundle:ShopContact s
                JOIN s.contact c
                WHERE c.enable = 1 AND s.shop = :shop_id')->setParameter('shop_id', $shop->getId());
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }



    public function mySelectedShop(Contact $contact)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT sc FROM PSCEntityBundle:ShopContact sc
                JOIN sc.shop s
                WHERE sc.contact = :id AND sc.selected = 1 ORDER BY sc.admin ASC, s.title ASC')->setParameter('id', $contact->getId());
        try {
            $shop = $query->getOneOrNullResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        if (!$shop) {
            $query = $this->getEntityManager()
                ->createQuery('
                SELECT sc FROM PSCEntityBundle:ShopContact sc
                JOIN sc.shop s
                WHERE sc.contact = :id ORDER BY sc.admin ASC, s.title ASC')->setParameter('id', $contact->getId())
                ->setMaxResults(1);
            $shop = $query->getOneOrNullResult();
            if ($shop) {
                $this->getEntityManager()
                    ->createQuery('
                UPDATE PSCEntityBundle:ShopContact sc SET sc.selected=1
                WHERE sc.contact = :id AND sc.shop = :shop_id')->setParameter('id', $contact->getId())
                                ->setParameter('shop_id', $shop->getShop()->getUid())
                                ->execute();
                return $shop->getShop();
            }
        }

        return $shop->getShop();
    }
}
