<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * StockbookingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class StockbookingRepository extends EntityRepository
{
    /**
     * Gibt alle Bookings zurück;
     *
     * @param int   $productId Product Id
     * @param int   $start     Start
     * @param int   $limit     Anzahl der Einträge
     * @param array $sort      Sortierung
     * @param array $filters   Filter
     *
     * @return Collection
     */
    public function getAllByProductId($productId, $start, $limit, $sort, $filters)
    {
        $sortTemp = "";
        $clauses = array();
        if (!empty($sort)) {
            $sortTemp = " ORDER BY";
            foreach ($sort as $field) {
                $sortTemp .= ' s.' . $field['property'] . ' ' . $field['direction'];
            }
        }
        foreach ($filters as $filter) {
            $clauses[] = "s." . $filter['expression'] . ' ' . $filter['value'];
        }

        if (count($clauses) > 0) {
            $sortTemp = " AND " . implode(' AND ', $clauses) . $sortTemp;
        }

        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s FROM PSCEntityBundle:Stockbooking s
                WHERE s.product = :product' . $sortTemp)->setParameter('product', $productId)
            ->setMaxResults($limit)
            ->setFirstResult($start);
        try {
            return $query->execute();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * Gibt die Anzahl aller Bookings zurück;
     *
     * @param int $productId Product Id
     *
     * @return int
     */
    public function getCountByProductId($productId)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT s FROM PSCEntityBundle:Stockbooking s
                WHERE s.product = :product')->setParameter('product', $productId);
        try {
            return count($query->execute());
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
