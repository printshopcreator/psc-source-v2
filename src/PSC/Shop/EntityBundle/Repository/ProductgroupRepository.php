<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Repository;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * ProductgroupRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 *
 * @package    PSC\Shop\Entity
 * @subpackage Repositorys
 */
class ProductgroupRepository extends EntityRepository
{
    /**
     * Gibt alle Produktgruppen für eine ShopId;
     *
     * @param int $shopId Shopid
     *
     * @return int
     */
    public function getAllByShopId($shopId)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT p FROM PSCEntityBundle:Productgroup p
                WHERE p.shop = :shop_id')->setParameter('shop_id', $shopId);
        try {
            return $query->getArrayResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }

    /**
     * Gibt alle Produktgruppen für eine ShopId und Parent;
     *
     * @param int $shopId Shopid
     * @param int $parent Parent
     *
     * @return int
     */
    public function getAllByShopAndParent(Shop $shop, $parent, $enabled = null)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT p FROM PSCEntityBundle:Productgroup p
                WHERE p.shop = :shop_id AND p.parent = :parent AND p.enable in (:enabled) ORDER BY p.pos ASC')->setParameter('shop_id', $shop->getId())->setParameter('parent', $parent);
        if ($enabled == null) {
            $query->setParameter('enabled', '0,1');
        } elseif ($enabled) {
            $query->setParameter('enabled', '1');
        } else {
            $query->setParameter('enabled', '0');
        }

        try {
            return $query->execute();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }
    }
}
