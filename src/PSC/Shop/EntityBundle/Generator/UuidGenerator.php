<?php

namespace PSC\Shop\EntityBundle\Generator;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Id\AbstractIdGenerator;

/**
 * UUID generator for the Doctrine ORM.
 */
class UuidGenerator extends AbstractIdGenerator
{
    /**
     * Generate an identifier
     *
     * @param \Doctrine\ORM\EntityManager  $em
     * @param \Doctrine\ORM\Mapping\Entity $entity
     * @return \Ramsey\Uuid\UuidInterface
     */
    public function generate(EntityManager $em, $entity)
    {
        return \Ramsey\Uuid\Uuid::uuid4();
    }
}
