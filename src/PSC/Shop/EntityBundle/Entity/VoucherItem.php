<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Cms
 *
 * @ORM\Table(name="credit_system_detail")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\VoucherItemRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class VoucherItem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;
/**
     * Created
     *
     * @var Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $createdAt;
/**
     * Updated
     *
     * @var Datetime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updatedAt;
/**
     * Shop zu welcher die News gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PSC\Shop\EntityBundle\Entity\Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Voucher
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PSC\Shop\EntityBundle\Entity\Voucher")
     * @ORM\JoinColumn(name="creditsystem_id", referencedColumnName="id")
     */
    protected $voucher;
/**
     * used
     *
     * @var boolean
     *
     * @ORM\Column(name="used", type="boolean")
     */
    protected $used;
/**
     * Code
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=40)
     */
    protected $code;
/**
     * Voucher
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PSC\Shop\EntityBundle\Entity\Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;
    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return Datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param Datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param Datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return int
     */
    public function getVoucher()
    {
        return $this->voucher;
    }

    /**
     * @param int $voucher
     */
    public function setVoucher($voucher)
    {
        $this->voucher = $voucher;
    }

    /**
     * @return bool
     */
    public function isUsed()
    {
        return $this->used;
    }

    /**
     * @param bool $used
     */
    public function setUsed($used)
    {
        $this->used = $used;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return int
     */
    public function getContact()
    {
        if ($this->contact == null || $this->contact->getUid() == 0) {
            return new Contact();
        }
        return $this->contact;
    }

    /**
     * @param int $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }
}
