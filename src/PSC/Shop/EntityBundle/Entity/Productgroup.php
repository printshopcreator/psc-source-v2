<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shop
 *
 * @ORM\Table(name="article_group")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ProductgroupRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Productgroup
{
    protected $textArt;
    protected $textFormat;
    protected $einleitung;
    protected $description;
    protected $extraSettings;
/**
     * Id des Product
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Sort
     *
     * @var string
     *
     * @ORM\Column(name="pos", type="integer")
     */
    protected $pos;
/**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;
/**
     * Url des Product
     *
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    protected $url;
/**
     * Text
     *
     * @var string
     *
     * @ORM\Column(name="text", type="string")
     */
    protected $text;
/**
     * Privates Product
     *
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean")
     */
    protected $private;
/**
     * Text
     *
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string")
     */
    protected $metaKeywords;
/**
     * Text
     *
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string")
     */
    protected $metaDescription;
/**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="meta_author", type="string", length=255)
     */
    protected $metaAuthor;
/**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="meta_custom_title", type="string", length=255)
     */
    protected $metaCustomTitle;
/**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_title", type="string", length=255)
     */
    protected $metaOGTitle;
/**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_type", type="string", length=255)
     */
    protected $metaOGType;
/**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_url", type="string", length=255)
     */
    protected $metaOGUrl;
/**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_image", type="string", length=255)
     */
    protected $metaOGImage;
/**
     * Language
     *
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5)
     */
    protected $language;
/**
     * Text
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_description", type="string")
     */
    protected $metaOGDescription;
/**
     * Langdata des Product
     *
     * @var string
     *
     * @ORM\Column(name="lang_data", type="text", nullable=true)
     */
    protected $langData;
/**
     * Bild 1
     *
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    protected $image1;
/**
     * Bild 2
     *
     * @var string
     *
     * @ORM\Column(name="image_big", type="string", length=255, nullable=true)
     */
    protected $image2;
/**
     * Shop zu welcher die Versandart gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Aktive Produktgruppe
     *
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean")
     */
    protected $enable;
/**
     * Anzeige Title
     *
     * @var boolean
     *
     * @ORM\Column(name="display_title", type="boolean")
     */
    protected $displayTitle;
/**
     * Nicht im Menu
     *
     * @var boolean
     *
     * @ORM\Column(name="notinmenu", type="boolean")
     */
    protected $notInMenu;
/**
     * Parent
     *
     * @var integer
     *
     * @ORM\Column(name="parent", type="integer")
     */
    protected $parentRaw;
/**
     * @ORM\ManyToMany(targetEntity="Product", mappedBy="product_groups")
     **/
    private $products;
/**
     * @ORM\OneToMany(targetEntity="Productgroup", mappedBy="parent")
     **/
    private $children;
/**
     * @ORM\ManyToOne(targetEntity="Productgroup", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     **/
    private $parent;

    public function __construct()
    {
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
        $this->enable = true;
        $this->displayTitle = true;
        $this->notInMenu = false;
    }

    /**
     * @return array
     */
    public function getLangData()
    {
        if ($this->langData == "") {
            return array();
        }
        return json_decode($this->langData, true);
    }

    /**
     * @param array $langData
     */
    public function setLangData($langData)
    {
        if (!is_array($langData)) {
            $langData = array();
        }
        $this->langData = json_encode($langData);
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        if ($this->getParentRaw() == 0) {
            return null;
        }
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return string
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage1($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param string $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }



    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return int
     */
    public function getParentRaw()
    {
        return $this->parentRaw;
    }

    /**
     * @param int $parentRaw
     */
    public function setParentRaw($parentRaw)
    {
        $this->parentRaw = $parentRaw;
    }

    /**
     * @return string
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param string $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaAuthor()
    {
        return $this->metaAuthor;
    }

    /**
     * @param string $metaAuthor
     */
    public function setMetaAuthor($metaAuthor)
    {
        $this->metaAuthor = $metaAuthor;
    }

    /**
     * @return string
     */
    public function getMetaCustomTitle()
    {
        return $this->metaCustomTitle;
    }

    /**
     * @param string $metaCustomTitle
     */
    public function setMetaCustomTitle($metaCustomTitle)
    {
        $this->metaCustomTitle = $metaCustomTitle;
    }

    /**
     * @return string
     */
    public function getMetaOGTitle()
    {
        return $this->metaOGTitle;
    }

    /**
     * @param string $metaOGTitle
     */
    public function setMetaOGTitle($metaOGTitle)
    {
        $this->metaOGTitle = $metaOGTitle;
    }

    /**
     * @return string
     */
    public function getMetaOGType()
    {
        return $this->metaOGType;
    }

    /**
     * @param string $metaOGType
     */
    public function setMetaOGType($metaOGType)
    {
        $this->metaOGType = $metaOGType;
    }

    /**
     * @return string
     */
    public function getMetaOGUrl()
    {
        return $this->metaOGUrl;
    }

    /**
     * @param string $metaOGUrl
     */
    public function setMetaOGUrl($metaOGUrl)
    {
        $this->metaOGUrl = $metaOGUrl;
    }

    /**
     * @return string
     */
    public function getMetaOGImage()
    {
        return $this->metaOGImage;
    }

    /**
     * @param string $metaOGImage
     */
    public function setMetaOGImage($metaOGImage)
    {
        $this->metaOGImage = $metaOGImage;
    }

    /**
     * @return string
     */
    public function getMetaOGDescription()
    {
        return $this->metaOGDescription;
    }

    /**
     * @param string $metaOGDescription
     */
    public function setMetaOGDescription($metaOGDescription)
    {
        $this->metaOGDescription = $metaOGDescription;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return bool
     */
    public function isDisplayTitle()
    {
        return $this->displayTitle;
    }

    /**
     * @param bool $displayTitle
     */
    public function setDisplayTitle($displayTitle)
    {
        $this->displayTitle = $displayTitle;
    }

    /**
     * @return bool
     */
    public function isNotInMenu()
    {
        return $this->notInMenu;
    }

    /**
     * @param bool $notInMenu
     */
    public function setNotInMenu($notInMenu)
    {
        $this->notInMenu = $notInMenu;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param bool $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return mixed
     */
    public function getTextArt()
    {
        return $this->textArt;
    }

    /**
     * @param mixed $textArt
     */
    public function setTextArt($textArt)
    {
        $this->textArt = $textArt;
    }

    /**
     * @return mixed
     */
    public function getTextFormat()
    {
        return $this->textFormat;
    }

    /**
     * @param mixed $textFormat
     */
    public function setTextFormat($textFormat)
    {
        $this->textFormat = $textFormat;
    }

    /**
     * @return mixed
     */
    public function getEinleitung()
    {
        return $this->einleitung;
    }

    /**
     * @param mixed $einleitung
     */
    public function setEinleitung($einleitung)
    {
        $this->einleitung = $einleitung;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }
}
