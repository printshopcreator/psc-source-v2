<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shop
 *
 * @ORM\Table(name="shop")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ShopRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Shop
{
    /**
     * Id des Shops
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Apikey des Shops
     *
     * @var string
     *
     * @ORM\Column(name="apikey", type="string", length=40)
     */
    protected $apiKey;
/**
     * Titel des Shops
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $title;
/**
     * Uuid des Shops
     *
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=255)
     */
    protected $uuid;
/**
     * Market Shop
     *
     * @var boolean
     *
     * @ORM\Column(name="market", type="boolean")
     */
    protected $market;
/**
     * Logo2
     *
     * @var string
     *
     * @ORM\Column(name="logo2", type="string")
     */
    protected $logo2;
/**
     * googleanalyticscode
     *
     * @var string
     *
     * @ORM\Column(name="googleanalyticscode", type="string", nullable=true)
     */
    protected $googleanalyticscode;
/**
     * Install
     *
     * @var Install
     *
     * @ORM\ManyToOne(targetEntity="Install")
     * @ORM\JoinColumn(name="install_id", referencedColumnName="id")
     */
    protected $install;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;
/**
     * @var string
     *
     * @ORM\Column(name="layout", type="string", length=255, nullable=true)
     */
    private $layout;
/**
     * @var string
     *
     * @ORM\Column(name="defaultfunc", type="string", length=255, nullable=true)
     */
    private $defaultFunc;
/**
     * @var string
     *
     * @ORM\Column(name="defaultparam", type="string", length=255, nullable=true)
     */
    private $defaultParam;
/**
     * @var string
     *
     * @ORM\Column(name="mwert", type="string", length=2, nullable=true)
     */
    private $mwert;
/**
     * @var string
     *
     * @ORM\Column(name="logo1", type="string", length=255, nullable=true)
     */
    private $logo1;
/**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=500, nullable=true)
     */
    private $subTitle;
/**
     * @var string
     *
     * @ORM\Column(name="parameter", type="text", nullable=true)
     */
    private $parameter;
/**
     * @var string
     *
     * @ORM\Column(name="formel", type="text", nullable=true)
     */
    private $formel;
/**
     * @var integer
     *
     * @ORM\Column(name="land", type="bigint", nullable=true)
     */
    private $land;
/**
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean", nullable=true)
     */
    private $private;
/**
     * @var boolean
     *
     * @ORM\Column(name="useemailaslogin", type="boolean")
     */
    private $useEmailAsLogin;
/**
     * @var boolean
     *
     * @ORM\Column(name="noverify", type="boolean")
     */
    private $noVerify;
/**
     * @var string
     *
     * @ORM\Column(name="keywords", type="text", nullable=true)
     */
    private $keywords;
/**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=true)
     */
    private $description;
/**
     * @var string
     *
     * @ORM\Column(name="author", type="string", length=255, nullable=true)
     */
    private $author;
/**
     * @var string
     *
     * @ORM\Column(name="copyright", type="string", length=255, nullable=true)
     */
    private $copyright;
/**
     * @var string
     *
     * @ORM\Column(name="basketfield1", type="string", length=255, nullable=true)
     */
    private $basketfield1;
/**
     * @var string
     *
     * @ORM\Column(name="basketfield2", type="string", length=255, nullable=true)
     */
    private $basketfield2;
/**
     * @var boolean
     *
     * @ORM\Column(name="markettemplate", type="boolean", nullable=true)
     */
    private $markettemplate;
/**
     * @var string
     *
     * @ORM\Column(name="reportsite1", type="string", length=255, nullable=true)
     */
    private $reportsite1;
/**
     * @var string
     *
     * @ORM\Column(name="reportsite2", type="string", length=255, nullable=true)
     */
    private $reportsite2;
/**
     * @var boolean
     *
     * @ORM\Column(name="customtemplates", type="boolean")
     */
    private $customTemplates;
/**
     * @var string
     *
     * @ORM\Column(name="templatecopyright", type="text", nullable=true)
     */
    private $templateCopyright;
/**
     * @var boolean
     *
     * @ORM\Column(name="uploadfrontend", type="boolean")
     */
    private $uploadFrontend;
/**
     * @var integer
     *
     * @ORM\Column(name="uselanguage", type="integer", nullable=true)
     */
    private $uselanguage;
/**
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="default_account", referencedColumnName="id")
     */
    private $defaultAccount;
/**
     * @var integer
     *
     * @ORM\Column(name="template_navi_display_all", type="integer", nullable=true)
     */
    private $templateNaviDisplayAll;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products", type="integer", nullable=true)
     */
    private $templateDisplayProducts;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_motive", type="integer", nullable=true)
     */
    private $templateDisplayMotive;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_themenshops", type="integer", nullable=true)
     */
    private $templateDisplayThemenshops;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_all", type="integer", nullable=true)
     */
    private $templateDisplayProductsAll;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_product_templates", type="integer", nullable=true)
     */
    private $templateDisplayProductsProductTemplates;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_products", type="integer", nullable=true)
     */
    private $templateDisplayProductsProducts;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_templates", type="integer", nullable=true)
     */
    private $templateDisplayProductsTemplates;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_own_products", type="integer", nullable=true)
     */
    private $templateDisplayProductsOwnProducts;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_my_products", type="integer", nullable=true)
     */
    private $templateDisplayProductsMyProducts;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_custom_layouter", type="integer", nullable=true)
     */
    private $templateDisplayProductsCustomLayouter;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_calc", type="integer", nullable=true)
     */
    private $templateDisplayProductsCalc;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_depot", type="integer", nullable=true)
     */
    private $templateDisplayProductsDepot;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_motive_all", type="integer", nullable=true)
     */
    private $templateDisplayMotiveAll;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_motive_fav", type="integer", nullable=true)
     */
    private $templateDisplayMotiveFav;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_motive_my", type="integer", nullable=true)
     */
    private $templateDisplayMotiveMy;
/**
     * @var string
     *
     * @ORM\Column(name="browsericon", type="string", length=255, nullable=true)
     */
    private $browsericon;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_navi", type="integer", nullable=true)
     */
    private $templateDisplayProductsNavi;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_theme_navi", type="integer", nullable=true)
     */
    private $templateDisplayThemeNavi;
/**
     * @var boolean
     *
     * @ORM\Column(name="template_display_products_crossselling", type="boolean", nullable=true)
     */
    private $templateDisplayProductsCrossselling;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_help", type="integer", nullable=true)
     */
    private $templateDisplayHelp;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_help_cms", type="integer", nullable=true)
     */
    private $templateDisplayHelpCms;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_lesezeichen", type="integer", nullable=true)
     */
    private $templateDisplayLesezeichen;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_news", type="integer", nullable=true)
     */
    private $templateDisplayNews;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_newsletter", type="integer", nullable=true)
     */
    private $templateDisplayNewsletter;
/**
     * @var integer
     *
     * @ORM\Column(name="pmb", type="integer", nullable=true)
     */
    private $pmb;
/**
     * @var integer
     *
     * @ORM\Column(name="views", type="integer", nullable=true)
     */
    private $views;
/**
     * @var string
     *
     * @ORM\Column(name="preview_logo", type="string", length=255, nullable=true)
     */
    private $previewLogo;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_uploadcenter", type="integer", nullable=true)
     */
    private $templateDisplayUploadcenter;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_orders", type="integer", nullable=true)
     */
    private $templateDisplayOrders;
/**
     * @var boolean
     *
     * @ORM\Column(name="registration", type="boolean", nullable=true)
     */
    private $registration;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_lead_box", type="integer", nullable=true)
     */
    private $templateDisplayLeadBox;
/**
     * @var integer
     *
     * @ORM\Column(name="rate", type="integer", nullable=true)
     */
    private $rate;
/**
     * @var integer
     *
     * @ORM\Column(name="rate_count", type="integer", nullable=true)
     */
    private $rateCount;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_rate", type="integer", nullable=true)
     */
    private $templateDisplayRate;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_violation", type="integer", nullable=true)
     */
    private $templateDisplayViolation;
/**
     * @var string
     *
     * @ORM\Column(name="layouter_presets", type="text", nullable=true)
     */
    private $layouterPresets;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_company", type="string", length=100, nullable=true)
     */
    private $betreiberCompany;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_name", type="string", length=100, nullable=true)
     */
    private $betreiberName;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_street", type="string", length=100, nullable=true)
     */
    private $betreiberStreet;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_address", type="string", length=100, nullable=true)
     */
    private $betreiberAddress;
/**
     * @var integer
     *
     * @ORM\Column(name="display_top_modul", type="integer", nullable=true)
     */
    private $displayTopModul;
/**
     * @var string
     *
     * @ORM\Column(name="top_modul_settings", type="text", nullable=true)
     */
    private $topModulSettings;
/**
     * @var integer
     *
     * @ORM\Column(name="display_resale_modul", type="integer", nullable=true)
     */
    private $displayResaleModul;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_email", type="string", length=50, nullable=true)
     */
    private $betreiberEmail;
/**
     * @var integer
     *
     * @ORM\Column(name="template_lead_product_1", type="integer", nullable=true)
     */
    private $templateLeadProduct1;
/**
     * @var integer
     *
     * @ORM\Column(name="template_lead_product_2", type="integer", nullable=true)
     */
    private $templateLeadProduct2;
/**
     * @var integer
     *
     * @ORM\Column(name="template_lead_product_3", type="integer", nullable=true)
     */
    private $templateLeadProduct3;
/**
     * @var integer
     *
     * @ORM\Column(name="template_lead_product_4", type="integer", nullable=true)
     */
    private $templateLeadProduct4;
/**
     * @var integer
     *
     * @ORM\Column(name="display_lead_box_modul", type="integer", nullable=true)
     */
    private $displayLeadBoxModul;
/**
     * @var integer
     *
     * @ORM\Column(name="template_lead_product_5", type="integer", nullable=true)
     */
    private $templateLeadProduct5;
/**
     * @var boolean
     *
     * @ORM\Column(name="display_market", type="boolean", nullable=true)
     */
    private $displayMarket;
/**
     * @var integer
     *
     * @ORM\Column(name="creator_id", type="integer", nullable=true)
     */
    private $creatorId;
/**
     * @var string
     *
     * @ORM\Column(name="web_title", type="string", length=250, nullable=true)
     */
    private $webTitle;
/**
     * @var integer
     *
     * @ORM\Column(name="default_motiv_status", type="integer", nullable=true)
     */
    private $defaultMotivStatus;
/**
     * @var integer
     *
     * @ORM\Column(name="display_motiv_count", type="integer", nullable=true)
     */
    private $displayMotivCount;
/**
     * @var integer
     *
     * @ORM\Column(name="display_market_count", type="integer", nullable=true)
     */
    private $displayMarketCount;
/**
     * @var integer
     *
     * @ORM\Column(name="display_article_count", type="integer", nullable=true)
     */
    private $displayArticleCount;
/**
     * @var integer
     *
     * @ORM\Column(name="service_value", type="integer", nullable=true)
     */
    private $serviceValue;
/**
     * @var integer
     *
     * @ORM\Column(name="private_product", type="integer", nullable=true)
     */
    private $privateProduct;
/**
     * @var string
     *
     * @ORM\Column(name="sitemap", type="string", length=40, nullable=true)
     */
    private $sitemap;
/**
     * @var string
     *
     * @ORM\Column(name="report_agb", type="string", length=40, nullable=true)
     */
    private $reportAgb;
/**
     * @var string
     *
     * @ORM\Column(name="test_parameter", type="text", nullable=true)
     */
    private $testParameter;
/**
     * @var string
     *
     * @ORM\Column(name="test_formel", type="text", nullable=true)
     */
    private $testFormel;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_invoice", type="integer", nullable=true)
     */
    private $reportBackgroundInvoice;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_delivery", type="integer", nullable=true)
     */
    private $reportBackgroundDelivery;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_label", type="integer", nullable=true)
     */
    private $reportBackgroundLabel;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_job", type="integer", nullable=true)
     */
    private $reportBackgroundJob;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_offer", type="integer", nullable=true)
     */
    private $reportBackgroundOffer;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_order", type="integer", nullable=true)
     */
    private $reportBackgroundOrder;
/**
     * @var string
     *
     * @ORM\Column(name="redirect_login", type="string", length=255, nullable=true)
     */
    private $redirectLogin;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_customer_related", type="integer", nullable=true)
     */
    private $templateDisplayProductsCustomerRelated;
/**
     * @var integer
     *
     * @ORM\Column(name="template_display_products_related_carusel", type="integer", nullable=true)
     */
    private $templateDisplayProductsRelatedCarusel;
/**
     * @var boolean
     *
     * @ORM\Column(name="template_display_user_approval", type="boolean", nullable=true)
     */
    private $templateDisplayUserApproval;
/**
     * @var string
     *
     * @ORM\Column(name="robots", type="string", length=40, nullable=true)
     */
    private $robots;
/**
     * @var string
     *
     * @ORM\Column(name="custom_agb", type="text", nullable=true)
     */
    private $customAgb;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_offer_blank", type="integer", nullable=true)
     */
    private $reportBackgroundOfferBlank;
/**
     * @var integer
     *
     * @ORM\Column(name="report_background_offer_contact", type="integer", nullable=true)
     */
    private $reportBackgroundOfferContact;
/**
     * @var integer
     *
     * @ORM\Column(name="shipping_mode", type="integer", nullable=true)
     */
    private $shippingMode;
/**
     * @var string
     *
     * @ORM\Column(name="product_sort", type="string", length=8, nullable=true)
     */
    private $productSort;
/**
     * @var string
     *
     * @ORM\Column(name="product_sort_dir", type="string", length=4, nullable=true)
     */
    private $productSortDir;
/**
     * @var string
     *
     * @ORM\Column(name="xmlvideositemap", type="string", length=40, nullable=true)
     */
    private $xmlvideositemap;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_tel", type="string", length=255, nullable=true)
     */
    private $betreiberTel;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_uid", type="string", length=255, nullable=true)
     */
    private $betreiberUid;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_hid", type="string", length=255, nullable=true)
     */
    private $betreiberHid;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_sid", type="string", length=255, nullable=true)
     */
    private $betreiberSid;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_register", type="string", length=255, nullable=true)
     */
    private $betreiberRegister;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_vb", type="string", length=255, nullable=true)
     */
    private $betreiberVb;
/**
     * @var string
     *
     * @ORM\Column(name="layout_settings", type="text", nullable=true)
     */
    private $layoutSettings;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_description", type="text", nullable=true)
     */
    private $betreiberDescription;
/**
     * @var string
     *
     * @ORM\Column(name="custom_impress", type="text", nullable=true)
     */
    private $customImpress;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_rechtsform", type="string", length=255, nullable=true)
     */
    private $betreiberRechtsform;
/**
     * @var string
     *
     * @ORM\Column(name="betreiber_web", type="string", length=255, nullable=true)
     */
    private $betreiberWeb;
/**
     * @var boolean
     *
     * @ORM\Column(name="display_sender", type="boolean", nullable=true)
     */
    private $displaySender;
/**
     * @var boolean
     *
     * @ORM\Column(name="display_delivery", type="boolean", nullable=true)
     */
    private $displayDelivery;
/**
     * @var integer
     *
     * @ORM\Column(name="basket_offer_number", type="integer", nullable=true)
     */
    private $basketOfferNumber;
/**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=true)
     */
    private $deleted;
/**
     * @var string
     *
     * @ORM\Column(name="layouter_version", type="string", length=20, nullable=true)
     */
    private $layouterVersion;
/**
     * @var string
     *
     * @ORM\Column(name="basketposfield1", type="string", length=255, nullable=true)
     */
    private $basketposfield1;
/**
     * @var string
     *
     * @ORM\Column(name="basketposfield2", type="string", length=255, nullable=true)
     */
    private $basketposfield2;
/**
     * @var integer
     *
     * @ORM\Column(name="redirect_to_uploadcenter_after_basket_finish", type="integer", nullable=true)
     */
    private $redirectToUploadcenterAfterBasketFinish;
/**
     * @var integer
     *
     * @ORM\Column(name="template_switch", type="integer", nullable=true)
     */
    private $templateSwitch;
/**
     * @var string
     *
     * @ORM\Column(name="fullbackground", type="string", length=40, nullable=true)
     */
    private $fullbackground;
/**
     * @var string
     *
     * @ORM\Column(name="redirect_logout", type="string", length=255, nullable=true)
     */
    private $redirectLogout;
/**
     * @var string
     *
     * @ORM\Column(name="redirect_register", type="string", length=255, nullable=true)
     */
    private $redirectRegister;
/**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getLayout()
    {
        return $this->layout;
    }

    /**
     * @param string $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return string
     */
    public function getDefaultFunc()
    {
        return $this->defaultFunc;
    }

    /**
     * @param string $defaultFunc
     */
    public function setDefaultFunc($defaultFunc)
    {
        $this->defaultFunc = $defaultFunc;
    }

    /**
     * @return string
     */
    public function getDefaultParam()
    {
        return $this->defaultParam;
    }

    /**
     * @param string $defaultParam
     */
    public function setDefaultParam($defaultParam)
    {
        $this->defaultParam = $defaultParam;
    }

    /**
     * @return string
     */
    public function getMwert()
    {
        return $this->mwert;
    }

    /**
     * @param string $mwert
     */
    public function setMwert($mwert)
    {
        $this->mwert = $mwert;
    }

    /**
     * @return string
     */
    public function getLogo1()
    {
        return $this->logo1;
    }

    /**
     * @param string $logo1
     */
    public function setLogo1($logo1)
    {
        $this->logo1 = $logo1;
    }

    /**
     * @return string
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * @param string $subTitle
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

    /**
     * @return string
     */
    public function getParameter()
    {
        return $this->parameter;
    }

    /**
     * @param string $parameter
     */
    public function setParameter($parameter)
    {
        $this->parameter = $parameter;
    }

    /**
     * @return string
     */
    public function getFormel()
    {
        return $this->formel;
    }

    /**
     * @param string $formel
     */
    public function setFormel($formel)
    {
        $this->formel = $formel;
    }

    /**
     * @return int
     */
    public function getLand()
    {
        return $this->land;
    }

    /**
     * @param int $land
     */
    public function setLand($land)
    {
        $this->land = $land;
    }

    /**
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param boolean $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return boolean
     */
    public function isUseEmailAsLogin()
    {
        return $this->useEmailAsLogin;
    }

    /**
     * @param boolean $useEmailAsLogin
     */
    public function setUseEmailAsLogin($useEmailAsLogin)
    {
        $this->useEmailAsLogin = $useEmailAsLogin;
    }

    /**
     * @return boolean
     */
    public function isNoVerify()
    {
        return $this->noVerify;
    }

    /**
     * @param boolean $noVerify
     */
    public function setNoVerify($noVerify)
    {
        $this->noVerify = $noVerify;
    }

    /**
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param string $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param string $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * @param string $copyright
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }

    /**
     * @return string
     */
    public function getBasketfield1()
    {
        return $this->basketfield1;
    }

    /**
     * @param string $basketfield1
     */
    public function setBasketfield1($basketfield1)
    {
        $this->basketfield1 = $basketfield1;
    }

    /**
     * @return string
     */
    public function getBasketfield2()
    {
        return $this->basketfield2;
    }

    /**
     * @param string $basketfield2
     */
    public function setBasketfield2($basketfield2)
    {
        $this->basketfield2 = $basketfield2;
    }

    /**
     * @return boolean
     */
    public function isMarkettemplate()
    {
        return $this->markettemplate;
    }

    /**
     * @param boolean $markettemplate
     */
    public function setMarkettemplate($markettemplate)
    {
        $this->markettemplate = $markettemplate;
    }

    /**
     * @return string
     */
    public function getReportsite1()
    {
        return $this->reportsite1;
    }

    /**
     * @param string $reportsite1
     */
    public function setReportsite1($reportsite1)
    {
        $this->reportsite1 = $reportsite1;
    }

    /**
     * @return string
     */
    public function getReportsite2()
    {
        return $this->reportsite2;
    }

    /**
     * @param string $reportsite2
     */
    public function setReportsite2($reportsite2)
    {
        $this->reportsite2 = $reportsite2;
    }

    /**
     * @return boolean
     */
    public function isCustomTemplates()
    {
        return $this->customTemplates;
    }

    /**
     * @param boolean $customTemplates
     */
    public function setCustomTemplates($customTemplates)
    {
        $this->customTemplates = $customTemplates;
    }

    /**
     * @return string
     */
    public function getTemplateCopyright()
    {
        return $this->templateCopyright;
    }

    /**
     * @param string $templateCopyright
     */
    public function setTemplateCopyright($templateCopyright)
    {
        $this->templateCopyright = $templateCopyright;
    }

    /**
     * @return boolean
     */
    public function isUploadFrontend()
    {
        return $this->uploadFrontend;
    }

    /**
     * @param boolean $uploadFrontend
     */
    public function setUploadFrontend($uploadFrontend)
    {
        $this->uploadFrontend = $uploadFrontend;
    }

    /**
     * @return int
     */
    public function getUselanguage()
    {
        return $this->uselanguage;
    }

    /**
     * @param int $uselanguage
     */
    public function setUselanguage($uselanguage)
    {
        $this->uselanguage = $uselanguage;
    }

    /**
     * @return Account
     */
    public function getDefaultAccount()
    {
        return $this->defaultAccount;
    }

    /**
     * @param Account $defaultAccount
     */
    public function setDefaultAccount($defaultAccount)
    {
        $this->defaultAccount = $defaultAccount;
    }

    /**
     * @return int
     */
    public function getTemplateNaviDisplayAll()
    {
        return $this->templateNaviDisplayAll;
    }

    /**
     * @param int $templateNaviDisplayAll
     */
    public function setTemplateNaviDisplayAll($templateNaviDisplayAll)
    {
        $this->templateNaviDisplayAll = $templateNaviDisplayAll;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProducts()
    {
        return $this->templateDisplayProducts;
    }

    /**
     * @param int $templateDisplayProducts
     */
    public function setTemplateDisplayProducts($templateDisplayProducts)
    {
        $this->templateDisplayProducts = $templateDisplayProducts;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayMotive()
    {
        return $this->templateDisplayMotive;
    }

    /**
     * @param int $templateDisplayMotive
     */
    public function setTemplateDisplayMotive($templateDisplayMotive)
    {
        $this->templateDisplayMotive = $templateDisplayMotive;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayThemenshops()
    {
        return $this->templateDisplayThemenshops;
    }

    /**
     * @param int $templateDisplayThemenshops
     */
    public function setTemplateDisplayThemenshops($templateDisplayThemenshops)
    {
        $this->templateDisplayThemenshops = $templateDisplayThemenshops;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsAll()
    {
        return $this->templateDisplayProductsAll;
    }

    /**
     * @param int $templateDisplayProductsAll
     */
    public function setTemplateDisplayProductsAll($templateDisplayProductsAll)
    {
        $this->templateDisplayProductsAll = $templateDisplayProductsAll;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsProductTemplates()
    {
        return $this->templateDisplayProductsProductTemplates;
    }

    /**
     * @param int $templateDisplayProductsProductTemplates
     */
    public function setTemplateDisplayProductsProductTemplates($templateDisplayProductsProductTemplates)
    {
        $this->templateDisplayProductsProductTemplates = $templateDisplayProductsProductTemplates;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsProducts()
    {
        return $this->templateDisplayProductsProducts;
    }

    /**
     * @param int $templateDisplayProductsProducts
     */
    public function setTemplateDisplayProductsProducts($templateDisplayProductsProducts)
    {
        $this->templateDisplayProductsProducts = $templateDisplayProductsProducts;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsTemplates()
    {
        return $this->templateDisplayProductsTemplates;
    }

    /**
     * @param int $templateDisplayProductsTemplates
     */
    public function setTemplateDisplayProductsTemplates($templateDisplayProductsTemplates)
    {
        $this->templateDisplayProductsTemplates = $templateDisplayProductsTemplates;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsOwnProducts()
    {
        return $this->templateDisplayProductsOwnProducts;
    }

    /**
     * @param int $templateDisplayProductsOwnProducts
     */
    public function setTemplateDisplayProductsOwnProducts($templateDisplayProductsOwnProducts)
    {
        $this->templateDisplayProductsOwnProducts = $templateDisplayProductsOwnProducts;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsMyProducts()
    {
        return $this->templateDisplayProductsMyProducts;
    }

    /**
     * @param int $templateDisplayProductsMyProducts
     */
    public function setTemplateDisplayProductsMyProducts($templateDisplayProductsMyProducts)
    {
        $this->templateDisplayProductsMyProducts = $templateDisplayProductsMyProducts;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsCustomLayouter()
    {
        return $this->templateDisplayProductsCustomLayouter;
    }

    /**
     * @param int $templateDisplayProductsCustomLayouter
     */
    public function setTemplateDisplayProductsCustomLayouter($templateDisplayProductsCustomLayouter)
    {
        $this->templateDisplayProductsCustomLayouter = $templateDisplayProductsCustomLayouter;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsCalc()
    {
        return $this->templateDisplayProductsCalc;
    }

    /**
     * @param int $templateDisplayProductsCalc
     */
    public function setTemplateDisplayProductsCalc($templateDisplayProductsCalc)
    {
        $this->templateDisplayProductsCalc = $templateDisplayProductsCalc;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsDepot()
    {
        return $this->templateDisplayProductsDepot;
    }

    /**
     * @param int $templateDisplayProductsDepot
     */
    public function setTemplateDisplayProductsDepot($templateDisplayProductsDepot)
    {
        $this->templateDisplayProductsDepot = $templateDisplayProductsDepot;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayMotiveAll()
    {
        return $this->templateDisplayMotiveAll;
    }

    /**
     * @param int $templateDisplayMotiveAll
     */
    public function setTemplateDisplayMotiveAll($templateDisplayMotiveAll)
    {
        $this->templateDisplayMotiveAll = $templateDisplayMotiveAll;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayMotiveFav()
    {
        return $this->templateDisplayMotiveFav;
    }

    /**
     * @param int $templateDisplayMotiveFav
     */
    public function setTemplateDisplayMotiveFav($templateDisplayMotiveFav)
    {
        $this->templateDisplayMotiveFav = $templateDisplayMotiveFav;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayMotiveMy()
    {
        return $this->templateDisplayMotiveMy;
    }

    /**
     * @param int $templateDisplayMotiveMy
     */
    public function setTemplateDisplayMotiveMy($templateDisplayMotiveMy)
    {
        $this->templateDisplayMotiveMy = $templateDisplayMotiveMy;
    }

    /**
     * @return string
     */
    public function getBrowsericon()
    {
        return $this->browsericon;
    }

    /**
     * @param string $browsericon
     */
    public function setBrowsericon($browsericon)
    {
        $this->browsericon = $browsericon;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsNavi()
    {
        return $this->templateDisplayProductsNavi;
    }

    /**
     * @param int $templateDisplayProductsNavi
     */
    public function setTemplateDisplayProductsNavi($templateDisplayProductsNavi)
    {
        $this->templateDisplayProductsNavi = $templateDisplayProductsNavi;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayThemeNavi()
    {
        return $this->templateDisplayThemeNavi;
    }

    /**
     * @param int $templateDisplayThemeNavi
     */
    public function setTemplateDisplayThemeNavi($templateDisplayThemeNavi)
    {
        $this->templateDisplayThemeNavi = $templateDisplayThemeNavi;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayHelp()
    {
        return $this->templateDisplayHelp;
    }

    /**
     * @param int $templateDisplayHelp
     */
    public function setTemplateDisplayHelp($templateDisplayHelp)
    {
        $this->templateDisplayHelp = $templateDisplayHelp;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayHelpCms()
    {
        return $this->templateDisplayHelpCms;
    }

    /**
     * @param int $templateDisplayHelpCms
     */
    public function setTemplateDisplayHelpCms($templateDisplayHelpCms)
    {
        $this->templateDisplayHelpCms = $templateDisplayHelpCms;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayLesezeichen()
    {
        return $this->templateDisplayLesezeichen;
    }

    /**
     * @param int $templateDisplayLesezeichen
     */
    public function setTemplateDisplayLesezeichen($templateDisplayLesezeichen)
    {
        $this->templateDisplayLesezeichen = $templateDisplayLesezeichen;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayNews()
    {
        return $this->templateDisplayNews;
    }

    /**
     * @param int $templateDisplayNews
     */
    public function setTemplateDisplayNews($templateDisplayNews)
    {
        $this->templateDisplayNews = $templateDisplayNews;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayNewsletter()
    {
        return $this->templateDisplayNewsletter;
    }

    /**
     * @param int $templateDisplayNewsletter
     */
    public function setTemplateDisplayNewsletter($templateDisplayNewsletter)
    {
        $this->templateDisplayNewsletter = $templateDisplayNewsletter;
    }

    /**
     * @return int
     */
    public function getPmb()
    {
        return $this->pmb;
    }

    /**
     * @param int $pmb
     */
    public function setPmb($pmb)
    {
        $this->pmb = $pmb;
    }

    /**
     * @return int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param int $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * @return string
     */
    public function getPreviewLogo()
    {
        return $this->previewLogo;
    }

    /**
     * @param string $previewLogo
     */
    public function setPreviewLogo($previewLogo)
    {
        $this->previewLogo = $previewLogo;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayUploadcenter()
    {
        return $this->templateDisplayUploadcenter;
    }

    /**
     * @param int $templateDisplayUploadcenter
     */
    public function setTemplateDisplayUploadcenter($templateDisplayUploadcenter)
    {
        $this->templateDisplayUploadcenter = $templateDisplayUploadcenter;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayOrders()
    {
        return $this->templateDisplayOrders;
    }

    /**
     * @param int $templateDisplayOrders
     */
    public function setTemplateDisplayOrders($templateDisplayOrders)
    {
        $this->templateDisplayOrders = $templateDisplayOrders;
    }

    /**
     * @return boolean
     */
    public function isRegistration()
    {
        return $this->registration;
    }

    /**
     * @param boolean $registration
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayLeadBox()
    {
        return $this->templateDisplayLeadBox;
    }

    /**
     * @param int $templateDisplayLeadBox
     */
    public function setTemplateDisplayLeadBox($templateDisplayLeadBox)
    {
        $this->templateDisplayLeadBox = $templateDisplayLeadBox;
    }

    /**
     * @return int
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * @param int $rate
     */
    public function setRate($rate)
    {
        $this->rate = $rate;
    }

    /**
     * @return int
     */
    public function getRateCount()
    {
        return $this->rateCount;
    }

    /**
     * @param int $rateCount
     */
    public function setRateCount($rateCount)
    {
        $this->rateCount = $rateCount;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayRate()
    {
        return $this->templateDisplayRate;
    }

    /**
     * @param int $templateDisplayRate
     */
    public function setTemplateDisplayRate($templateDisplayRate)
    {
        $this->templateDisplayRate = $templateDisplayRate;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayViolation()
    {
        return $this->templateDisplayViolation;
    }

    /**
     * @param int $templateDisplayViolation
     */
    public function setTemplateDisplayViolation($templateDisplayViolation)
    {
        $this->templateDisplayViolation = $templateDisplayViolation;
    }

    /**
     * @return string
     */
    public function getLayouterPresets()
    {
        return $this->layouterPresets;
    }

    /**
     * @param string $layouterPresets
     */
    public function setLayouterPresets($layouterPresets)
    {
        $this->layouterPresets = $layouterPresets;
    }

    /**
     * @return string
     */
    public function getBetreiberCompany()
    {
        return $this->betreiberCompany;
    }

    /**
     * @param string $betreiberCompany
     */
    public function setBetreiberCompany($betreiberCompany)
    {
        $this->betreiberCompany = $betreiberCompany;
    }

    /**
     * @return string
     */
    public function getBetreiberName()
    {
        return $this->betreiberName;
    }

    /**
     * @param string $betreiberName
     */
    public function setBetreiberName($betreiberName)
    {
        $this->betreiberName = $betreiberName;
    }

    /**
     * @return string
     */
    public function getBetreiberStreet()
    {
        return $this->betreiberStreet;
    }

    /**
     * @param string $betreiberStreet
     */
    public function setBetreiberStreet($betreiberStreet)
    {
        $this->betreiberStreet = $betreiberStreet;
    }

    /**
     * @return string
     */
    public function getBetreiberAddress()
    {
        return $this->betreiberAddress;
    }

    /**
     * @param string $betreiberAddress
     */
    public function setBetreiberAddress($betreiberAddress)
    {
        $this->betreiberAddress = $betreiberAddress;
    }

    /**
     * @return int
     */
    public function getDisplayTopModul()
    {
        return $this->displayTopModul;
    }

    /**
     * @param int $displayTopModul
     */
    public function setDisplayTopModul($displayTopModul)
    {
        $this->displayTopModul = $displayTopModul;
    }

    /**
     * @return string
     */
    public function getTopModulSettings()
    {
        return $this->topModulSettings;
    }

    /**
     * @param string $topModulSettings
     */
    public function setTopModulSettings($topModulSettings)
    {
        $this->topModulSettings = $topModulSettings;
    }

    /**
     * @return int
     */
    public function getDisplayResaleModul()
    {
        return $this->displayResaleModul;
    }

    /**
     * @param int $displayResaleModul
     */
    public function setDisplayResaleModul($displayResaleModul)
    {
        $this->displayResaleModul = $displayResaleModul;
    }

    /**
     * @return string
     */
    public function getBetreiberEmail()
    {
        return $this->betreiberEmail;
    }

    /**
     * @param string $betreiberEmail
     */
    public function setBetreiberEmail($betreiberEmail)
    {
        $this->betreiberEmail = $betreiberEmail;
    }

    /**
     * @return int
     */
    public function getTemplateLeadProduct1()
    {
        return $this->templateLeadProduct1;
    }

    /**
     * @param int $templateLeadProduct1
     */
    public function setTemplateLeadProduct1($templateLeadProduct1)
    {
        $this->templateLeadProduct1 = $templateLeadProduct1;
    }

    /**
     * @return int
     */
    public function getTemplateLeadProduct2()
    {
        return $this->templateLeadProduct2;
    }

    /**
     * @param int $templateLeadProduct2
     */
    public function setTemplateLeadProduct2($templateLeadProduct2)
    {
        $this->templateLeadProduct2 = $templateLeadProduct2;
    }

    /**
     * @return int
     */
    public function getTemplateLeadProduct3()
    {
        return $this->templateLeadProduct3;
    }

    /**
     * @param int $templateLeadProduct3
     */
    public function setTemplateLeadProduct3($templateLeadProduct3)
    {
        $this->templateLeadProduct3 = $templateLeadProduct3;
    }

    /**
     * @return int
     */
    public function getTemplateLeadProduct4()
    {
        return $this->templateLeadProduct4;
    }

    /**
     * @param int $templateLeadProduct4
     */
    public function setTemplateLeadProduct4($templateLeadProduct4)
    {
        $this->templateLeadProduct4 = $templateLeadProduct4;
    }

    /**
     * @return int
     */
    public function getDisplayLeadBoxModul()
    {
        return $this->displayLeadBoxModul;
    }

    /**
     * @param int $displayLeadBoxModul
     */
    public function setDisplayLeadBoxModul($displayLeadBoxModul)
    {
        $this->displayLeadBoxModul = $displayLeadBoxModul;
    }

    /**
     * @return int
     */
    public function getTemplateLeadProduct5()
    {
        return $this->templateLeadProduct5;
    }

    /**
     * @param int $templateLeadProduct5
     */
    public function setTemplateLeadProduct5($templateLeadProduct5)
    {
        $this->templateLeadProduct5 = $templateLeadProduct5;
    }

    /**
     * @return boolean
     */
    public function getDisplayMarket()
    {
        return $this->displayMarket;
    }

    /**
     * @param boolean $displayMarket
     */
    public function setDisplayMarket($displayMarket)
    {
        $this->displayMarket = $displayMarket;
    }

    /**
     * @return int
     */
    public function getCreatorId()
    {
        return $this->creatorId;
    }

    /**
     * @param int $creatorId
     */
    public function setCreatorId($creatorId)
    {
        $this->creatorId = $creatorId;
    }

    /**
     * @return string
     */
    public function getWebTitle()
    {
        return $this->webTitle;
    }

    /**
     * @param string $webTitle
     */
    public function setWebTitle($webTitle)
    {
        $this->webTitle = $webTitle;
    }

    /**
     * @return int
     */
    public function getDefaultMotivStatus()
    {
        return $this->defaultMotivStatus;
    }

    /**
     * @param int $defaultMotivStatus
     */
    public function setDefaultMotivStatus($defaultMotivStatus)
    {
        $this->defaultMotivStatus = $defaultMotivStatus;
    }

    /**
     * @return int
     */
    public function getDisplayMotivCount()
    {
        return $this->displayMotivCount;
    }

    /**
     * @param int $displayMotivCount
     */
    public function setDisplayMotivCount($displayMotivCount)
    {
        $this->displayMotivCount = $displayMotivCount;
    }

    /**
     * @return int
     */
    public function getDisplayMarketCount()
    {
        return $this->displayMarketCount;
    }

    /**
     * @param int $displayMarketCount
     */
    public function setDisplayMarketCount($displayMarketCount)
    {
        $this->displayMarketCount = $displayMarketCount;
    }

    /**
     * @return int
     */
    public function getDisplayArticleCount()
    {
        return $this->displayArticleCount;
    }

    /**
     * @param int $displayArticleCount
     */
    public function setDisplayArticleCount($displayArticleCount)
    {
        $this->displayArticleCount = $displayArticleCount;
    }

    /**
     * @return int
     */
    public function getServiceValue()
    {
        return $this->serviceValue;
    }

    /**
     * @param int $serviceValue
     */
    public function setServiceValue($serviceValue)
    {
        $this->serviceValue = $serviceValue;
    }

    /**
     * @return int
     */
    public function getPrivateProduct()
    {
        return $this->privateProduct;
    }

    /**
     * @param int $privateProduct
     */
    public function setPrivateProduct($privateProduct)
    {
        $this->privateProduct = $privateProduct;
    }

    /**
     * @return string
     */
    public function getSitemap()
    {
        return $this->sitemap;
    }

    /**
     * @param string $sitemap
     */
    public function setSitemap($sitemap)
    {
        $this->sitemap = $sitemap;
    }

    /**
     * @return string
     */
    public function getReportAgb()
    {
        return $this->reportAgb;
    }

    /**
     * @param string $reportAgb
     */
    public function setReportAgb($reportAgb)
    {
        $this->reportAgb = $reportAgb;
    }

    /**
     * @return string
     */
    public function getTestParameter()
    {
        return $this->testParameter;
    }

    /**
     * @param string $testParameter
     */
    public function setTestParameter($testParameter)
    {
        $this->testParameter = $testParameter;
    }

    /**
     * @return string
     */
    public function getTestFormel()
    {
        return $this->testFormel;
    }

    /**
     * @param string $testFormel
     */
    public function setTestFormel($testFormel)
    {
        $this->testFormel = $testFormel;
    }

    /**
     * @return int
     */
    public function getReportBackgroundInvoice()
    {
        return $this->reportBackgroundInvoice;
    }

    /**
     * @param int $reportBackgroundInvoice
     */
    public function setReportBackgroundInvoice($reportBackgroundInvoice)
    {
        $this->reportBackgroundInvoice = $reportBackgroundInvoice;
    }

    /**
     * @return int
     */
    public function getReportBackgroundDelivery()
    {
        return $this->reportBackgroundDelivery;
    }

    /**
     * @param int $reportBackgroundDelivery
     */
    public function setReportBackgroundDelivery($reportBackgroundDelivery)
    {
        $this->reportBackgroundDelivery = $reportBackgroundDelivery;
    }

    /**
     * @return int
     */
    public function getReportBackgroundLabel()
    {
        return $this->reportBackgroundLabel;
    }

    /**
     * @param int $reportBackgroundLabel
     */
    public function setReportBackgroundLabel($reportBackgroundLabel)
    {
        $this->reportBackgroundLabel = $reportBackgroundLabel;
    }

    /**
     * @return int
     */
    public function getReportBackgroundJob()
    {
        return $this->reportBackgroundJob;
    }

    /**
     * @param int $reportBackgroundJob
     */
    public function setReportBackgroundJob($reportBackgroundJob)
    {
        $this->reportBackgroundJob = $reportBackgroundJob;
    }

    /**
     * @return int
     */
    public function getReportBackgroundOffer()
    {
        return $this->reportBackgroundOffer;
    }

    /**
     * @param int $reportBackgroundOffer
     */
    public function setReportBackgroundOffer($reportBackgroundOffer)
    {
        $this->reportBackgroundOffer = $reportBackgroundOffer;
    }

    /**
     * @return int
     */
    public function getReportBackgroundOrder()
    {
        return $this->reportBackgroundOrder;
    }

    /**
     * @param int $reportBackgroundOrder
     */
    public function setReportBackgroundOrder($reportBackgroundOrder)
    {
        $this->reportBackgroundOrder = $reportBackgroundOrder;
    }

    /**
     * @return string
     */
    public function getRedirectLogin()
    {
        return $this->redirectLogin;
    }

    /**
     * @param string $redirectLogin
     */
    public function setRedirectLogin($redirectLogin)
    {
        $this->redirectLogin = $redirectLogin;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsCustomerRelated()
    {
        return $this->templateDisplayProductsCustomerRelated;
    }

    /**
     * @param int $templateDisplayProductsCustomerRelated
     */
    public function setTemplateDisplayProductsCustomerRelated($templateDisplayProductsCustomerRelated)
    {
        $this->templateDisplayProductsCustomerRelated = $templateDisplayProductsCustomerRelated;
    }

    /**
     * @return int
     */
    public function getTemplateDisplayProductsRelatedCarusel()
    {
        return $this->templateDisplayProductsRelatedCarusel;
    }

    /**
     * @param int $templateDisplayProductsRelatedCarusel
     */
    public function setTemplateDisplayProductsRelatedCarusel($templateDisplayProductsRelatedCarusel)
    {
        $this->templateDisplayProductsRelatedCarusel = $templateDisplayProductsRelatedCarusel;
    }

    /**
     * @return string
     */
    public function getRobots()
    {
        return $this->robots;
    }

    /**
     * @param string $robots
     */
    public function setRobots($robots)
    {
        $this->robots = $robots;
    }

    /**
     * @return string
     */
    public function getCustomAgb()
    {
        return $this->customAgb;
    }

    /**
     * @param string $customAgb
     */
    public function setCustomAgb($customAgb)
    {
        $this->customAgb = $customAgb;
    }

    /**
     * @return int
     */
    public function getReportBackgroundOfferBlank()
    {
        return $this->reportBackgroundOfferBlank;
    }

    /**
     * @param int $reportBackgroundOfferBlank
     */
    public function setReportBackgroundOfferBlank($reportBackgroundOfferBlank)
    {
        $this->reportBackgroundOfferBlank = $reportBackgroundOfferBlank;
    }

    /**
     * @return int
     */
    public function getReportBackgroundOfferContact()
    {
        return $this->reportBackgroundOfferContact;
    }

    /**
     * @param int $reportBackgroundOfferContact
     */
    public function setReportBackgroundOfferContact($reportBackgroundOfferContact)
    {
        $this->reportBackgroundOfferContact = $reportBackgroundOfferContact;
    }

    /**
     * @return int
     */
    public function getShippingMode()
    {
        return $this->shippingMode;
    }

    /**
     * @param int $shippingMode
     */
    public function setShippingMode($shippingMode)
    {
        $this->shippingMode = $shippingMode;
    }

    /**
     * @return string
     */
    public function getProductSort()
    {
        return $this->productSort;
    }

    /**
     * @param string $productSort
     */
    public function setProductSort($productSort)
    {
        $this->productSort = $productSort;
    }

    /**
     * @return string
     */
    public function getProductSortDir()
    {
        return $this->productSortDir;
    }

    /**
     * @param string $productSortDir
     */
    public function setProductSortDir($productSortDir)
    {
        $this->productSortDir = $productSortDir;
    }

    /**
     * @return string
     */
    public function getXmlvideositemap()
    {
        return $this->xmlvideositemap;
    }

    /**
     * @param string $xmlvideositemap
     */
    public function setXmlvideositemap($xmlvideositemap)
    {
        $this->xmlvideositemap = $xmlvideositemap;
    }

    /**
     * @return string
     */
    public function getBetreiberTel()
    {
        return $this->betreiberTel;
    }

    /**
     * @param string $betreiberTel
     */
    public function setBetreiberTel($betreiberTel)
    {
        $this->betreiberTel = $betreiberTel;
    }

    /**
     * @return string
     */
    public function getBetreiberUid()
    {
        return $this->betreiberUid;
    }

    /**
     * @param string $betreiberUid
     */
    public function setBetreiberUid($betreiberUid)
    {
        $this->betreiberUid = $betreiberUid;
    }

    /**
     * @return string
     */
    public function getBetreiberHid()
    {
        return $this->betreiberHid;
    }

    /**
     * @param string $betreiberHid
     */
    public function setBetreiberHid($betreiberHid)
    {
        $this->betreiberHid = $betreiberHid;
    }

    /**
     * @return string
     */
    public function getBetreiberSid()
    {
        return $this->betreiberSid;
    }

    /**
     * @param string $betreiberSid
     */
    public function setBetreiberSid($betreiberSid)
    {
        $this->betreiberSid = $betreiberSid;
    }

    /**
     * @return string
     */
    public function getBetreiberRegister()
    {
        return $this->betreiberRegister;
    }

    /**
     * @param string $betreiberRegister
     */
    public function setBetreiberRegister($betreiberRegister)
    {
        $this->betreiberRegister = $betreiberRegister;
    }

    /**
     * @return string
     */
    public function getBetreiberVb()
    {
        return $this->betreiberVb;
    }

    /**
     * @param string $betreiberVb
     */
    public function setBetreiberVb($betreiberVb)
    {
        $this->betreiberVb = $betreiberVb;
    }

    /**
     * @return string
     */
    public function getLayoutSettings()
    {
        return $this->layoutSettings;
    }

    /**
     * @param string $layoutSettings
     */
    public function setLayoutSettings($layoutSettings)
    {
        $this->layoutSettings = $layoutSettings;
    }

    /**
     * @return string
     */
    public function getBetreiberDescription()
    {
        return $this->betreiberDescription;
    }

    /**
     * @param string $betreiberDescription
     */
    public function setBetreiberDescription($betreiberDescription)
    {
        $this->betreiberDescription = $betreiberDescription;
    }

    /**
     * @return string
     */
    public function getCustomImpress()
    {
        return $this->customImpress;
    }

    /**
     * @param string $customImpress
     */
    public function setCustomImpress($customImpress)
    {
        $this->customImpress = $customImpress;
    }

    /**
     * @return string
     */
    public function getBetreiberRechtsform()
    {
        return $this->betreiberRechtsform;
    }

    /**
     * @param string $betreiberRechtsform
     */
    public function setBetreiberRechtsform($betreiberRechtsform)
    {
        $this->betreiberRechtsform = $betreiberRechtsform;
    }

    /**
     * @return string
     */
    public function getBetreiberWeb()
    {
        return $this->betreiberWeb;
    }

    /**
     * @param string $betreiberWeb
     */
    public function setBetreiberWeb($betreiberWeb)
    {
        $this->betreiberWeb = $betreiberWeb;
    }

    /**
     * @return int
     */
    public function getDisplaySender()
    {
        return $this->displaySender;
    }

    /**
     * @param int $displaySender
     */
    public function setDisplaySender($displaySender)
    {
        $this->displaySender = $displaySender;
    }

    /**
     * @return int
     */
    public function getDisplayDelivery()
    {
        return $this->displayDelivery;
    }

    /**
     * @param int $displayDelivery
     */
    public function setDisplayDelivery($displayDelivery)
    {
        $this->displayDelivery = $displayDelivery;
    }

    /**
     * @return int
     */
    public function getBasketOfferNumber()
    {
        return $this->basketOfferNumber;
    }

    /**
     * @param int $basketOfferNumber
     */
    public function setBasketOfferNumber($basketOfferNumber)
    {
        $this->basketOfferNumber = $basketOfferNumber;
    }

    /**
     * @return string
     */
    public function getLayouterVersion()
    {
        return $this->layouterVersion;
    }

    /**
     * @param string $layouterVersion
     */
    public function setLayouterVersion($layouterVersion)
    {
        $this->layouterVersion = $layouterVersion;
    }

    /**
     * @return string
     */
    public function getBasketposfield1()
    {
        return $this->basketposfield1;
    }

    /**
     * @param string $basketposfield1
     */
    public function setBasketposfield1($basketposfield1)
    {
        $this->basketposfield1 = $basketposfield1;
    }

    /**
     * @return string
     */
    public function getBasketposfield2()
    {
        return $this->basketposfield2;
    }

    /**
     * @param string $basketposfield2
     */
    public function setBasketposfield2($basketposfield2)
    {
        $this->basketposfield2 = $basketposfield2;
    }

    /**
     * @return int
     */
    public function getRedirectToUploadcenterAfterBasketFinish()
    {
        return $this->redirectToUploadcenterAfterBasketFinish;
    }

    /**
     * @param int $redirectToUploadcenterAfterBasketFinish
     */
    public function setRedirectToUploadcenterAfterBasketFinish($redirectToUploadcenterAfterBasketFinish)
    {
        $this->redirectToUploadcenterAfterBasketFinish = $redirectToUploadcenterAfterBasketFinish;
    }

    /**
     * @return int
     */
    public function getTemplateSwitch()
    {
        return $this->templateSwitch;
    }

    /**
     * @param int $templateSwitch
     */
    public function setTemplateSwitch($templateSwitch)
    {
        $this->templateSwitch = $templateSwitch;
    }

    /**
     * @return string
     */
    public function getFullbackground()
    {
        return $this->fullbackground;
    }

    /**
     * @param string $fullbackground
     */
    public function setFullbackground($fullbackground)
    {
        $this->fullbackground = $fullbackground;
    }

    /**
     * @return string
     */
    public function getRedirectLogout()
    {
        return $this->redirectLogout;
    }

    /**
     * @param string $redirectLogout
     */
    public function setRedirectLogout($redirectLogout)
    {
        $this->redirectLogout = $redirectLogout;
    }

    /**
     * @return string
     */
    public function getRedirectRegister()
    {
        return $this->redirectRegister;
    }

    /**
     * @param string $redirectRegister
     */
    public function setRedirectRegister($redirectRegister)
    {
        $this->redirectRegister = $redirectRegister;
    }



    public function __construct()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
    }

    /**
     * @param boolean $market
     */
    public function setMarket($market)
    {
        $this->market = $market;
    }

    /**
     * @return boolean
     */
    public function getMarket()
    {
        return $this->market;
    }

    /**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getId()
    {
        return $this->uid;
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     *
     * @return Shop
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * Get apikey
     *
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * ToString Methode
     *
     * @return string
     */
    public function __toString()
    {
        return $this->apiKey;
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getLogo2()
    {
        return $this->logo2;
    }

    /**
     * @param string $logo2
     */
    public function setLogo2($logo2)
    {
        $this->logo2 = $logo2;
    }

    /**
     * @return string
     */
    public function getGoogleanalyticscode()
    {
        return $this->googleanalyticscode;
    }

    /**
     * @param string $googleanalyticscode
     */
    public function setGoogleanalyticscode($googleanalyticscode)
    {
        $this->googleanalyticscode = $googleanalyticscode;
    }

    /**
     * @return Install
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param Install $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return bool
     */
    public function isTemplateDisplayProductsCrossselling()
    {
        return $this->templateDisplayProductsCrossselling;
    }

    /**
     * @param bool $templateDisplayProductsCrossselling
     */
    public function setTemplateDisplayProductsCrossselling($templateDisplayProductsCrossselling)
    {
        $this->templateDisplayProductsCrossselling = $templateDisplayProductsCrossselling;
    }

    /**
     * @return bool
     */
    public function isTemplateDisplayUserApproval()
    {
        return $this->templateDisplayUserApproval;
    }

    /**
     * @param bool $templateDisplayUserApproval
     */
    public function setTemplateDisplayUserApproval($templateDisplayUserApproval)
    {
        $this->templateDisplayUserApproval = $templateDisplayUserApproval;
    }
}
