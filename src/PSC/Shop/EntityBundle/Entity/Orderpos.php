<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Orderpos
 *
 * @ORM\Table(name="orderspos")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\OrderposRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Orderpos
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
/**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order")
     * @ORM\JoinColumn(name="orders_id", referencedColumnName="id")
     */
    private $order;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="createdd", type="date", nullable=true)
     */
    private $createdDate;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedd", type="date", nullable=true)
     */
    private $updatedDate;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="createdt", type="time", nullable=true)
     */
    private $createdTime;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedt", type="time", nullable=true)
     */
    private $updatedTime;
/**
     * Install zu welcher die Bestellung gehört
     *
     * @var Install
     *
     * @ORM\ManyToOne(targetEntity="Install")
     * @ORM\JoinColumn(name="install_id", referencedColumnName="id")
     */
    protected $install;
/**
     * Shop zu welcher das Product gehört
     *
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * @var integer
     *
     * @ORM\Column(name="account_id", type="bigint", nullable=true)
     */
    private $account;
/**
     * @var integer
     *
     * @ORM\Column(name="count", type="integer", nullable=true)
     */
    private $count;
/**
     * @var string
     *
     * @ORM\Column(name="data", type="string", nullable=true)
     */
    private $data;
/**
     * @var float
     *
     * @ORM\Column(name="priceone", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceOneNetto;
/**
     * @var float
     *
     * @ORM\Column(name="priceonesteuer", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceOneSteuer;
/**
     * @var float
     *
     * @ORM\Column(name="priceonebrutto", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceOneBrutto;
/**
     * @var float
     *
     * @ORM\Column(name="priceall", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceAllNetto;
/**
     * @var float
     *
     * @ORM\Column(name="priceallsteuer", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceAllSteuer;
/**
     * @var float
     *
     * @ORM\Column(name="priceallbrutto", type="float", precision=10, scale=0, nullable=true)
     */
    private $priceAllBrutto;
/**
     * @var boolean
     *
     * @ORM\Column(name="uploadfinish", type="boolean", nullable=true)
     */
    private $uploadFinish;
/**
     * @ORM\OneToMany(targetEntity="Upload", mappedBy="orderPos")
     **/
    protected $uploads;
/**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id")
     */
    private $product;
/**
     * @var string
     *
     * @ORM\Column(name="question", type="text", nullable=true)
     */
    private $question;
/**
     * @var float
     *
     * @ORM\Column(name="resale_price", type="float", precision=10, scale=0, nullable=true)
     */
    private $resalePrice;
/**
     * @var integer
     *
     * @ORM\Column(name="typ", type="integer", nullable=false)
     */
    private $typ;
/**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=false)
     */
    private $status;
/**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=50, nullable=false)
     */
    private $uuid;
/**
     * @var integer
     *
     * @ORM\Column(name="layouter_mode", type="integer", nullable=false)
     */
    private $layouterMode;
/**
     * @var integer
     *
     * @ORM\Column(name="render_print", type="integer", nullable=false)
     */
    private $renderPrint;
/**
     * @var string
     *
     * @ORM\Column(name="xmlconfigfile", type="text", nullable=false)
     */
    private $xmlConfigFile;
/**
     * @var string
     *
     * @ORM\Column(name="xmlpagesfile", type="text", nullable=false)
     */
    private $xmlPagesFile;
/**
     * @var string
     *
     * @ORM\Column(name="xmlpagetemplatesfile", type="text", nullable=false)
     */
    private $xmlPageTemplatesFile;
/**
     * @var string
     *
     * @ORM\Column(name="xmlpageobjectsfile", type="text", nullable=false)
     */
    private $xmlPageObjectsFile;
/**
     * @var string
     *
     * @ORM\Column(name="xmlxslfofile", type="text", nullable=false)
     */
    private $xmlXslFoFile;
/**
     * @var string
     *
     * @ORM\Column(name="xmlpreviewxslfofile", type="text", nullable=false)
     */
    private $xmlPreviewXslFoFile;
/**
     * @var string
     *
     * @ORM\Column(name="xmlextendpreviewxslfofile", type="text", nullable=false)
     */
    private $xmlExtendPreviewXslFoFile;
/**
     * @var string
     *
     * @ORM\Column(name="calc_xml", type="text", nullable=false)
     */
    private $calcXml;
/**
     * @var integer
     *
     * @ORM\Column(name="hasgutschein", type="integer", nullable=false)
     */
    private $hasGutschein;
/**
     * @var integer
     *
     * @ORM\Column(name="weight", type="integer", nullable=false)
     */
    private $weight;
/**
     * @var string
     *
     * @ORM\Column(name="calc_values", type="text", nullable=false)
     */
    private $calcValues;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_date", type="date", nullable=false)
     */
    private $deliveryDate;
/**
     * @var integer
     *
     * @ORM\Column(name="maschine", type="integer", nullable=false)
     */
    private $maschine;
/**
     * @var integer
     *
     * @ORM\Column(name="papier", type="integer", nullable=false)
     */
    private $papier;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="proddate", type="date", nullable=false)
     */
    private $prodDate;
/**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=false)
     */
    private $version;
/**
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer", nullable=false)
     */
    private $pos;
/**
     * @var integer
     *
     * @ORM\Column(name="shipping_type", type="integer", nullable=false)
     */
    private $shippingType;
/**
     * @var float
     *
     * @ORM\Column(name="shipping_price", type="float", precision=10, scale=0, nullable=false)
     */
    private $shippingPrice;
/**
     * @var float
     *
     * @ORM\Column(name="shipping_price_mwert", type="float", precision=10, scale=0, nullable=false)
     */
    private $shippingPriceMwert;
/**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=255, nullable=false)
     */
    private $basketField1;
/**
     * @var string
     *
     * @ORM\Column(name="kst", type="string", length=255, nullable=true)
     */
    private $basketField2;
/**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param Order $order
     */
    public function setOrder($order)
    {
        $this->order = $order;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedDate()
    {
        return $this->createdDate;
    }

    /**
     * @param \DateTime $createdDate
     */
    public function setCreatedDate($createdDate)
    {
        $this->createdDate = $createdDate;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedDate()
    {
        return $this->updatedDate;
    }

    /**
     * @param \DateTime $updatedDate
     */
    public function setUpdatedDate($updatedDate)
    {
        $this->updatedDate = $updatedDate;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * @param \DateTime $createdTime
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedTime()
    {
        return $this->updatedTime;
    }

    /**
     * @param \DateTime $updatedTime
     */
    public function setUpdatedTime($updatedTime)
    {
        $this->updatedTime = $updatedTime;
    }

    /**
     * @return Install
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param Install $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return int
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param int $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param string $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return float
     */
    public function getPriceOneNetto()
    {
        return $this->priceOneNetto;
    }

    /**
     * @param float $priceOneNetto
     */
    public function setPriceOneNetto($priceOneNetto)
    {
        $this->priceOneNetto = $priceOneNetto;
    }

    /**
     * @return float
     */
    public function getPriceOneSteuer()
    {
        return $this->priceOneSteuer;
    }

    /**
     * @param float $priceOneSteuer
     */
    public function setPriceOneSteuer($priceOneSteuer)
    {
        $this->priceOneSteuer = $priceOneSteuer;
    }

    /**
     * @return float
     */
    public function getPriceOneBrutto()
    {
        return $this->priceOneBrutto;
    }

    /**
     * @param float $priceOneBrutto
     */
    public function setPriceOneBrutto($priceOneBrutto)
    {
        $this->priceOneBrutto = $priceOneBrutto;
    }

    /**
     * @return float
     */
    public function getPriceAllNetto()
    {
        return $this->priceAllNetto;
    }

    /**
     * @param float $priceAllNetto
     */
    public function setPriceAllNetto($priceAllNetto)
    {
        $this->priceAllNetto = $priceAllNetto;
    }

    /**
     * @return float
     */
    public function getPriceAllSteuer()
    {
        return $this->priceAllSteuer;
    }

    /**
     * @param float $priceAllSteuer
     */
    public function setPriceAllSteuer($priceAllSteuer)
    {
        $this->priceAllSteuer = $priceAllSteuer;
    }

    /**
     * @return float
     */
    public function getPriceAllBrutto()
    {
        return $this->priceAllBrutto;
    }

    /**
     * @param float $priceAllBrutto
     */
    public function setPriceAllBrutto($priceAllBrutto)
    {
        $this->priceAllBrutto = $priceAllBrutto;
    }

    /**
     * @return boolean
     */
    public function isUploadFinish()
    {
        return $this->uploadFinish;
    }

    /**
     * @param boolean $uploadFinish
     */
    public function setUploadFinish($uploadFinish)
    {
        $this->uploadFinish = $uploadFinish;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * @param string $question
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    }

    /**
     * @return float
     */
    public function getResalePrice()
    {
        return $this->resalePrice;
    }

    /**
     * @param float $resalePrice
     */
    public function setResalePrice($resalePrice)
    {
        $this->resalePrice = $resalePrice;
    }

    /**
     * @return int
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * @param int $typ
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return int
     */
    public function getLayouterMode()
    {
        return $this->layouterMode;
    }

    /**
     * @param int $layouterMode
     */
    public function setLayouterMode($layouterMode)
    {
        $this->layouterMode = $layouterMode;
    }

    /**
     * @return int
     */
    public function getRenderPrint()
    {
        return $this->renderPrint;
    }

    /**
     * @param int $renderPrint
     */
    public function setRenderPrint($renderPrint)
    {
        $this->renderPrint = $renderPrint;
    }

    /**
     * @return string
     */
    public function getXmlConfigFile()
    {
        return $this->xmlConfigFile;
    }

    /**
     * @param string $xmlConfigFile
     */
    public function setXmlConfigFile($xmlConfigFile)
    {
        $this->xmlConfigFile = $xmlConfigFile;
    }

    /**
     * @return string
     */
    public function getXmlPagesFile()
    {
        return $this->xmlPagesFile;
    }

    /**
     * @param string $xmlPagesFile
     */
    public function setXmlPagesFile($xmlPagesFile)
    {
        $this->xmlPagesFile = $xmlPagesFile;
    }

    /**
     * @return string
     */
    public function getXmlPageTemplatesFile()
    {
        return $this->xmlPageTemplatesFile;
    }

    /**
     * @param string $xmlPageTemplatesFile
     */
    public function setXmlPageTemplatesFile($xmlPageTemplatesFile)
    {
        $this->xmlPageTemplatesFile = $xmlPageTemplatesFile;
    }

    /**
     * @return string
     */
    public function getXmlPageObjectsFile()
    {
        return $this->xmlPageObjectsFile;
    }

    /**
     * @param string $xmlPageObjectsFile
     */
    public function setXmlPageObjectsFile($xmlPageObjectsFile)
    {
        $this->xmlPageObjectsFile = $xmlPageObjectsFile;
    }

    /**
     * @return string
     */
    public function getXmlXslFoFile()
    {
        return $this->xmlXslFoFile;
    }

    /**
     * @param string $xmlXslFoFile
     */
    public function setXmlXslFoFile($xmlXslFoFile)
    {
        $this->xmlXslFoFile = $xmlXslFoFile;
    }

    /**
     * @return string
     */
    public function getXmlPreviewXslFoFile()
    {
        return $this->xmlPreviewXslFoFile;
    }

    /**
     * @param string $xmlPreviewXslFoFile
     */
    public function setXmlPreviewXslFoFile($xmlPreviewXslFoFile)
    {
        $this->xmlPreviewXslFoFile = $xmlPreviewXslFoFile;
    }

    /**
     * @return string
     */
    public function getXmlExtendPreviewXslFoFile()
    {
        return $this->xmlExtendPreviewXslFoFile;
    }

    /**
     * @param string $xmlExtendPreviewXslFoFile
     */
    public function setXmlExtendPreviewXslFoFile($xmlExtendPreviewXslFoFile)
    {
        $this->xmlExtendPreviewXslFoFile = $xmlExtendPreviewXslFoFile;
    }

    public function hasCalcXml()
    {
        return ($this->calcXml != "" && strpos($this->calcXml, "artikel") !== false);
    }

    /**
     * @return string
     */
    public function getCalcXml()
    {
        return $this->calcXml;
    }

    /**
     * @param string $calcXml
     */
    public function setCalcXml($calcXml)
    {
        $this->calcXml = $calcXml;
    }

    /**
     * @return int
     */
    public function getHasGutschein()
    {
        return $this->hasGutschein;
    }

    /**
     * @param int $hasGutschein
     */
    public function setHasGutschein($hasGutschein)
    {
        $this->hasGutschein = $hasGutschein;
    }

    /**
     * @return int
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @return string
     */
    public function getCalcValues()
    {
        return json_decode($this->calcValues, true);
    }

    /**
     * @param string $calcValues
     */
    public function setCalcValues($calcValues)
    {
        $this->calcValues = json_encode($calcValues);
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return int
     */
    public function getMaschine()
    {
        return $this->maschine;
    }

    /**
     * @param int $maschine
     */
    public function setMaschine($maschine)
    {
        $this->maschine = $maschine;
    }

    /**
     * @return int
     */
    public function getPapier()
    {
        return $this->papier;
    }

    /**
     * @param int $papier
     */
    public function setPapier($papier)
    {
        $this->papier = $papier;
    }

    /**
     * @return \DateTime
     */
    public function getProdDate()
    {
        return $this->prodDate;
    }

    /**
     * @param \DateTime $prodDate
     */
    public function setProdDate($prodDate)
    {
        $this->prodDate = $prodDate;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param int $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return int
     */
    public function getShippingType()
    {
        return $this->shippingType;
    }

    /**
     * @param int $shippingType
     */
    public function setShippingType($shippingType)
    {
        $this->shippingType = $shippingType;
    }

    /**
     * @return float
     */
    public function getShippingPrice()
    {
        return $this->shippingPrice;
    }

    /**
     * @param float $shippingPrice
     */
    public function setShippingPrice($shippingPrice)
    {
        $this->shippingPrice = $shippingPrice;
    }

    /**
     * @return float
     */
    public function getShippingPriceMwert()
    {
        return $this->shippingPriceMwert;
    }

    /**
     * @param float $shippingPriceMwert
     */
    public function setShippingPriceMwert($shippingPriceMwert)
    {
        $this->shippingPriceMwert = $shippingPriceMwert;
    }

    /**
     * @return string
     */
    public function getBasketField1()
    {
        return $this->basketField1;
    }

    /**
     * @param string $basketField1
     */
    public function setBasketField1($basketField1)
    {
        $this->basketField1 = $basketField1;
    }

    /**
     * @return string
     */
    public function getBasketField2()
    {
        return $this->basketField2;
    }

    /**
     * @param string $basketField2
     */
    public function setBasketField2($basketField2)
    {
        $this->basketField2 = $basketField2;
    }

    /**
     * @return mixed
     */
    public function getUploads()
    {
        return $this->uploads;
    }

    /**
     * @param mixed $uploads
     */
    public function setUploads($uploads)
    {
        $this->uploads = $uploads;
    }
}
