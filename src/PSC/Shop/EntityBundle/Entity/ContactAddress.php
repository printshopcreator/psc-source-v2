<?php

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * ContactAddress
 *
 * @ORM\Table(name="contact_address")
 * @ORM\Entity
 */
class ContactAddress
{
    protected $kundenNr = '';
    protected $homepage = '';
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=40, nullable=true)
     */
    private $uuid;

    /**
     * @var integer
     *
     * @ORM\Column(name="display", type="integer", nullable=true)
     */
    private $display;

    /**
     * @var integer
     *
     * @ORM\Column(name="install_id", type="integer", nullable=true)
     */
    private $installId;

    /**
     * Contact der zur Adresse gehört
     *
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", length=20, nullable=true)
     */
    private $houseNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=10, nullable=true)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobil_phone", type="string", length=100, nullable=true)
     */
    private $mobilPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=100, nullable=true)
     */
    private $company;

    /**
     * @var integer
     *
     * @ORM\Column(name="anrede", type="integer", length=100, nullable=true)
     */
    private $salutation;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=100, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="company2", type="string", length=255, nullable=true)
     */
    private $company2;

    /**
     * @var string
     *
     * @ORM\Column(name="kostenstellung", type="string", length=255, nullable=true)
     */
    private $kostenstellung;

    /**
     * @var string
     *
     * @ORM\Column(name="abteilung", type="string", length=255, nullable=true)
     */
    private $abteilung;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255, nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="ustid", type="string", length=255, nullable=true)
     */
    private $ustid;

    /**
     * @var string
     *
     * @ORM\Column(name="zusatz1", type="string", length=255, nullable=true)
     */
    private $zusatz1;

    /**
     * @var string
     *
     * @ORM\Column(name="zusatz2", type="string", length=255, nullable=true)
     */
    private $zusatz2;

    /**
     * @var int
     *
     * @ORM\Column(name="pos",type="integer", nullable=true)
     */
    private $pos;

    public function __construct()
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        $this->uuid = Uuid::uuid4();
        $this->display = true;
        $this->phone = "";
        $this->mobilPhone = "";
        $this->email = "";
        $this->company = "";
        $this->company2 = "";
        $this->salutation = 1;
        $this->country = "";
        $this->pos = 1;
    }

    public function asArray()
    {
        return array(
            'firstname' => $this->getFirstname(),
            'lastname' => $this->getLastname(),
            'houseNumber' => $this->getHouseNumber(),
            'street' => $this->getStreet(),
            'zip' => $this->getZip(),
            'city' => $this->getCity(),
            'company1' => $this->getCompany(),
            'company2' => $this->getCompany2(),
            'zusatz1' => $this->getZusatz1(),
            'zusatz2' => $this->getZusatz2(),
            'anrede' => $this->getAnrede(),
            'salutation' => $this->getSalutation(),
            'mobilePhone' => $this->getMobilPhone(),
            'phone' => $this->getPhone(),
            'fax' => $this->getFax(),
            'country' => $this->getCountry(),
            'pos' => $this->getPos()
        );
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return int
     */
    public function getDisplay()
    {
        return $this->display;
    }

    /**
     * @param int $display
     */
    public function setDisplay($display)
    {
        $this->display = $display;
    }

    /**
     * @return int
     */
    public function getInstallId()
    {
        return $this->installId;
    }

    /**
     * @param int $installId
     */
    public function setInstallId($installId)
    {
        $this->installId = $installId;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMobilPhone()
    {
        return $this->mobilPhone;
    }

    /**
     * @param string $mobilPhone
     */
    public function setMobilPhone($mobilPhone)
    {
        $this->mobilPhone = $mobilPhone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return integer
     */
    public function getAnrede()
    {
        return $this->salutation;
    }

    /**
     * @param integer $anrede
     */
    public function setAnrede($anrede)
    {
        $this->salutation = $anrede;
    }

    /**
     * @return integer
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @param integer $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        if ($this->country == "") {
            return "DE";
        }
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getCompany2()
    {
        return $this->company2;
    }

    /**
     * @param string $company2
     */
    public function setCompany2($company2)
    {
        $this->company2 = $company2;
    }

    /**
     * @return string
     */
    public function getKostenstellung()
    {
        return $this->kostenstellung;
    }

    /**
     * @param string $kostenstellung
     */
    public function setKostenstellung($kostenstellung)
    {
        $this->kostenstellung = $kostenstellung;
    }

    /**
     * @return string
     */
    public function getAbteilung()
    {
        return $this->abteilung;
    }

    /**
     * @param string $abteilung
     */
    public function setAbteilung($abteilung)
    {
        $this->abteilung = $abteilung;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getUstid()
    {
        return $this->ustid;
    }

    /**
     * @param string $ustid
     */
    public function setUstid($ustid)
    {
        $this->ustid = $ustid;
    }

    /**
     * @return string
     */
    public function getZusatz1()
    {
        return $this->zusatz1;
    }

    /**
     * @param string $zusatz1
     */
    public function setZusatz1($zusatz1)
    {
        $this->zusatz1 = $zusatz1;
    }

    /**
     * @return string
     */
    public function getZusatz2()
    {
        return $this->zusatz2;
    }

    /**
     * @param string $zusatz2
     */
    public function setZusatz2($zusatz2)
    {
        $this->zusatz2 = $zusatz2;
    }

    /**
     * @return Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    public function anonymisieren()
    {
        $this->setCity('anonym');
        $this->setAbteilung('anonym');
        $this->setFirstname('anonym');
        $this->setCompany('anonym');
        $this->setCompany2('anonym');
        $this->setCountry('anonym');
        $this->setEmail('anonym');
        $this->setFax('');
        $this->setHouseNumber('anonym');
        $this->setKostenstellung('anonym');
        $this->setLastname('anonym');
        $this->setZusatz2('anonym');
        $this->setZusatz1('anonym');
        $this->setZip('anonym');
        $this->setStreet('anonym');
        $this->setPosition('anonym');
        $this->setPosition('anonym');
        $this->setPhone('');
        $this->setPos(1);
        $this->setMobilPhone('');
        $this->setUstid('');
    }

    /**
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param string $homepage
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }

    /**
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param int $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }
}
