<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Contact
 *
 * @ORM\Table(name="contact")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ContactRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Contact implements UserInterface
{
    public $kundenNr = '';
    public $calcValue1 = '';
    public $calcValue2 = '';
    public $custom1;
    public $custom2;
    public $custom3;
    public $defaultPayment;
    public $defaultShipment;
    public $custom4;
    public $custom5;
    public $custom6;
    public $custom7;
    public $custom8;
    public $custom9;
    public $custom10;
    public $custom11;
    public $custom12;
    public $custom13;
    public $custom14;
    public $custom15;
    public $custom16;
    public $custom17;
    public $custom18;
    public $custom19;
    public $custom20;
    public $custom21;
    public $custom22;
    public $custom23;
    public $custom24;

    public $showOtherOrders = false;
    public $showOtherOrdersAccountFilter;
    public $showOtherOrdersAccount;
    private $priceFactor;

    public $productsSub;
    public $productsOrg;

    public $extraSettings;

    public $pluginSettings = [];

    /**
     * Id des Kunden
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    public $uid;

    /**
     * Benutzername bzw EMail den Benutzers
     *
     * @var string
     *
     * @ORM\Column(name="self_email", type="string", length=40)
     */
    public $username;

    /**
     * Password des Benutzers
     *
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=40)
     */
    public $password;

    /**
     * Uuid des Benutzers
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=40)
     */
    public $uuid;

    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="contacts")
     * @ORM\JoinTable(name="contact_role",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     *      )
     **/
    public $roles;

    /**
     * @ORM\ManyToMany(targetEntity="Shop", inversedBy="contacts")
     * @ORM\JoinTable(name="shop_contact",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="shop_id", referencedColumnName="id")}
     *      )
     **/
    public $shops;

    /**
     * @ORM\OneToMany(targetEntity="ShopContact", mappedBy="contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     **/
    public $shopContact;

    /**
     * @param int $install_id
     */
    public function setInstallId($install_id)
    {
        $this->install_id = $install_id;
    }

    /**
     * @return int
     */
    public function getInstallId()
    {
        return $this->install_id;
    }

    /**
     * Install Id
     *
     * @var int
     *
     * @ORM\Column(name="install_id", type="integer")
     */
    public $install_id;

    /**
     * Account zu welcher dar Contact gehört
     *
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    public $account;

    /**
     * @ORM\ManyToMany(targetEntity="Account", inversedBy="contacts")
     * @ORM\JoinTable(name="contact_account",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id")}
     *      )
     **/
    public $collectingOrdersAccounts;

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Constructor
     *
     * @param string $email Emailadresse
     */
    public function __construct($email = "")
    {
        $this->setEmail($email);
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->enable = true;
        $this->mwert = true;
        $this->virtual = false;
        $this->showCalc = false;
        $this->testCalc = false;
        $this->budget = 0;
        $this->collectingOrders = false;
        $this->isSek = false;
        $this->passwordResetDate = time();
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime();
    }

    /**
     * Gibt die Id des Benutzers zurück
     *
     * @return int
     */
    public function getId()
    {
        return $this->uid;
    }

    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Gibt die Mailadresse des Benutzers
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->username;
    }

    /**
     * Setzt die Mailadresse des Benutzers
     *
     * @param string $username
     */
    public function setEmail($username)
    {
        $this->username = $username;
    }

    /**
     * Gibt das Password des Benutzers
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Setzt das Password des Benutzers
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * Setzt die ID des Benutzers
     *
     * @param int $id
     */
    public function setId($id)
    {
        $this->uid = $id;
    }

    public function asArray()
    {
        return array(
            'id' => $this->getId(),
            'email' => $this->getUsername()
        );
    }


    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->getEmail();
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @param string $email Setzt Username welchen die Emailadresse ist
     *
     * @return string The username
     */
    public function setUsername($email)
    {
        $this->setEmail($email);
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     *
     * @return void
     */
    public function eraseCredentials()
    {
        //$this->setPassword("");
    }

    /**
     * Gibt den Namen zurück
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getEmail();
    }

    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->enable;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="date", nullable=true)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="date", nullable=true)
     */
    private $updatedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean", nullable=true)
     */
    private $enable;

    /**
     * @var boolean
     *
     * @ORM\Column(name="virtual", type="boolean", nullable=true)
     */
    private $virtual;

    /**
     * @var boolean
     *
     * @ORM\Column(name="mwert", type="boolean", nullable=true)
     */
    private $mwert;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=true)
     */
    private $hash;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=5, nullable=true)
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="self_anrede", type="integer", nullable=true)
     */
    private $salutation;

    /**
     * @var string
     *
     * @ORM\Column(name="self_firstname", type="string", length=255, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="self_lastname", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="self_function", type="string", length=255, nullable=true)
     */
    private $function;

    /**
     * @var string
     *
     * @ORM\Column(name="self_department", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="self_street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="self_house_number", type="string", length=255, nullable=true)
     */
    private $houseNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="self_destrict", type="string", length=5, nullable=true)
     */
    private $destrict;

    /**
     * @var string
     *
     * @ORM\Column(name="self_zip", type="string", length=255, nullable=true)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="self_city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="self_state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="self_country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="self_web", type="string", length=255, nullable=true)
     */
    private $homepage;

    /**
     * @var string
     *
     * @ORM\Column(name="self_foto", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_alternative_type", type="string", length=255, nullable=true)
     */
    private $alternativType;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_alternative", type="string", length=255, nullable=true)
     */
    private $alternativ;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_mobile", type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @var boolean
     *
     * @ORM\Column(name="self_newsletter", type="boolean", nullable=true)
     */
    private $newsletter;

    /**
     * @var string
     *
     * @ORM\Column(name="self_information", type="text", nullable=true)
     */
    private $information;

    /**
     * @var string
     *
     * @ORM\Column(name="self_durchwahl", type="string", length=255, nullable=false)
     */
    private $durchwahl;

    /**
     * @var string
     *
     * @ORM\Column(name="self_abteilung", type="string", length=255, nullable=false)
     */
    private $abteilung;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_sek", type="integer", nullable=false)
     */
    private $isSek;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_vorwahl", type="string", length=255, nullable=false)
     */
    private $phonePrefix;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_durchwahl", type="string", length=255, nullable=false)
     */
    private $phoneAppendix;

    /**
     * @var string
     *
     * @ORM\Column(name="self_mobile_vorwahl", type="string", length=255, nullable=false)
     */
    private $mobilePrefix;

    /**
     * @var string
     *
     * @ORM\Column(name="self_mobile_durchwahl", type="string", length=255, nullable=false)
     */
    private $mobileAppendix;

    /**
     * @var string
     *
     * @ORM\Column(name="self_fax_vorwahl", type="string", length=255, nullable=false)
     */
    private $faxPrefix;

    /**
     * @var string
     *
     * @ORM\Column(name="self_fax_phone", type="string", length=255, nullable=false)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="self_fax_durchwahl", type="string", length=255, nullable=false)
     */
    private $faxAppendix;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_alternative_durchwahl", type="string", length=255, nullable=false)
     */
    private $alternativAppendix;

    /**
     * @var string
     *
     * @ORM\Column(name="ustid", type="string", length=255, nullable=false)
     */
    private $ustid;

    /**
     * @var string
     *
     * @ORM\Column(name="vonwo", type="string", length=255, nullable=false)
     */
    private $vonWo;

    /**
     * @var string
     *
     * @ORM\Column(name="passwordresetid", type="string", length=255, nullable=true)
     */
    private $passwordResetUuid;

    /**
     * @var string
     *
     * @ORM\Column(name="passwordresetdate", type="string", length=255, nullable=true)
     */
    private $passwordResetDate;

    /**
     * @var string
     *
     * @ORM\Column(name="self_title", type="string", length=255, nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="self_position", type="string", length=255, nullable=false)
     */
    private $position;

    /**
     * @var ContactAddress
     *
     * @ORM\ManyToOne(targetEntity="ContactAddress")
     * @ORM\JoinColumn(name="standart_delivery", referencedColumnName="id", nullable = true)
     */
    private $standartDelivery = null;

    /**
     * @var ContactAddress
     *
     * @ORM\ManyToOne(targetEntity="ContactAddress")
     * @ORM\JoinColumn(name="standart_sender", referencedColumnName="id", nullable = true)
     */
    private $standartSender = null;

    /**
     * @var ContactAddress
     *
     * @ORM\ManyToOne(targetEntity="ContactAddress")
     * @ORM\JoinColumn(name="standart_invoice", referencedColumnName="id", nullable = true)
     */
    private $standartInvoice = null;

    /**
     * @var integer
     *
     * @ORM\Column(name="show_calc", type="boolean", nullable=true)
     */
    private $showCalc;

    /**
     * Gesperter Contact
     *
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    public $locked;

    /**
     * @var integer
     *
     * @ORM\Column(name="test_calc", type="boolean", nullable=true)
     */
    private $testCalc;

    /**
     * @var integer
     *
     * @ORM\Column(name="production_status", type="integer", nullable=true)
     */
    private $productionStatus;

    /**
     * @var integer
     *
     * @ORM\Column(name="collecting_orders", type="integer", nullable=false)
     */
    private $collectingOrders;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_lv", type="string", length=255, nullable=true)
     */
    private $phoneAreaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="self_mobile_lv", type="string", length=255, nullable=true)
     */
    private $mobileAreaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="self_fax_lv", type="string", length=255, nullable=true)
     */
    private $faxAreaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="self_phone_alternative_lv", type="string", length=255, nullable=true)
     */
    private $alternativAreaCode;

    /**
     * @var string
     *
     * @ORM\Column(name="self_birthday", type="string", length=255, nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="self_kostenstellung", type="string", length=255, nullable=true)
     */
    private $kostenstellung;

    /**
     * @var string
     *
     * @ORM\Column(name="self_department2", type="string", length=255, nullable=true)
     */
    private $company2;

    /**
     * @var integer
     *
     * @ORM\Column(name="budget", type="integer", nullable=true)
     */
    private $budget;

    /**
     * @ORM\ManyToMany(targetEntity="Payment", inversedBy="contacts")
     * @ORM\JoinTable(name="contact_paymenttype",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="paymenttype_id", referencedColumnName="id")}
     *      )
     **/
    public $payments;

    /**
     * @ORM\ManyToMany(targetEntity="Cms", inversedBy="contacts")
     * @ORM\JoinTable(name="contact_cms",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="cms_id", referencedColumnName="id")}
     *      )
     **/
    public $cms;

    /**
     * @ORM\ManyToMany(targetEntity="Shipping", inversedBy="contacts")
     * @ORM\JoinTable(name="contact_shippingtype",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="shippingtype_id", referencedColumnName="id")}
     *      )
     **/
    public $shippings;

    /**
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="contacts")
     * @ORM\JoinTable(name="contact_article",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")}
     *      )
     **/
    public $products;

    /**
     * @ORM\ManyToMany(targetEntity="Productgroup", inversedBy="contacts")
     * @ORM\JoinTable(name="contact_productgroup",
     *      joinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="productgroup_id", referencedColumnName="id")}
     *      )
     **/
    public $productGroups;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=255)
     */
    public $bankKtoName;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="bank_kto", type="string", length=255)
     */
    public $bankKTO;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="bank_blz", type="string", length=255)
     */
    public $bankBLZ;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="bank_iban", type="string", length=255)
     */
    public $bankIban;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="bank_bic", type="string", length=255)
     */
    public $bankBic;

    /**
     *
     * @var string
     *
     * @ORM\Column(name="bank_bank_name", type="string", length=255)
     */
    public $bankName;

    /**
     * @return boolean
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param boolean $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        if ($this->language == '') {
            return 'de_DE';
        }
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return int
     */
    public function getIsSek()
    {
        return boolval($this->isSek);
    }

    /**
     * @param int $isSek
     */
    public function setIsSek($isSek)
    {
        $this->isSek = $isSek;
    }

    /**
     * @return string
     */
    public function getUstid()
    {
        return $this->ustid;
    }

    /**
     * @param string $ustid
     */
    public function setUstid($ustid)
    {
        $this->ustid = $ustid;
    }


    /**
     * @return ContactAddress
     */
    public function getStandartDelivery()
    {
        if ($this->standartDelivery == null || $this->standartDelivery->getUid() == 0) {
            return new ContactAddress();
        }
        return $this->standartDelivery;
    }

    /**
     * @param ContactAddress $standartDelivery
     */
    public function setStandartDelivery($standartDelivery)
    {
        $this->standartDelivery = $standartDelivery;
    }

    /**
     * @return ContactAddress
     */
    public function getStandartSender()
    {
        if ($this->standartSender == null || $this->standartSender->getUid() == 0) {
            return new ContactAddress();
        }
        return $this->standartSender;
    }

    /**
     * @param ContactAddress $standartSender
     */
    public function setStandartSender($standartSender)
    {
        $this->standartSender = $standartSender;
    }

    /**
     * @return ContactAddress
     */
    public function getStandartInvoice()
    {
        if ($this->standartInvoice == null || $this->standartInvoice->getUid() != 0) {
            return $this->standartInvoice;
        }
        return new ContactAddress();
    }

    /**
     * @param ContactAddress $standartInvoice
     */
    public function setStandartInvoice($standartInvoice)
    {
        $this->standartInvoice = $standartInvoice;
    }

    /**
     * @return int
     */
    public function getShowCalc()
    {
        return $this->showCalc;
    }

    /**
     * @param int $showCalc
     */
    public function setShowCalc($showCalc)
    {
        $this->showCalc = $showCalc;
    }

    /**
     * @return int
     */
    public function getTestCalc()
    {
        return $this->testCalc;
    }

    /**
     * @param int $testCalc
     */
    public function setTestCalc($testCalc)
    {
        $this->testCalc = $testCalc;
    }

    /**
     * @return int
     */
    public function getProductionStatus()
    {
        return $this->productionStatus;
    }

    /**
     * @param int $productionStatus
     */
    public function setProductionStatus($productionStatus)
    {
        $this->productionStatus = $productionStatus;
    }

    /**
     * @return int
     */
    public function getCollectingOrders()
    {
        return boolval($this->collectingOrders);
    }

    /**
     * @param int $collectingOrders
     */
    public function setCollectingOrders($collectingOrders)
    {
        $this->collectingOrders = $collectingOrders;
    }

    /**
     * @return int
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * @param int $budget
     */
    public function setBudget($budget)
    {
        $this->budget = $budget;
    }

    public function anonymisieren()
    {
        $this->setFirstname("anonym");
        $this->setLastname("anonym");
        $this->setAbteilung("anonym");
        $this->setStreet("anonym");
        $this->setHouseNumber("anonym");
        $this->setZip("anonym");
        $this->setCity("anonym");
        $this->setCountry("anonym");
        $this->setPosition("anonym");
        $this->setPhone("anonym");
        $this->setPhoneAreaCode('');
        $this->setPhonePrefix('');
        $this->setPhoneAppendix('');
        $this->setPhone('');
        $this->setTitle('');
        $this->setBirthday('');
        $this->setCompany('');
        $this->setCompany2('');
        $this->setFax('');
        $this->setFaxAreaCode('');
        $this->setFaxAppendix('');
        $this->setFaxPrefix('');
        $this->setEmail('anonym@anonym.de');
        $this->setName('anonym@anonym.de');
        $this->setEnable(false);
        $this->setPassword('anonym');
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }

    /**
     * @return string
     */
    public function getCalcValue1()
    {
        return $this->calcValue1;
    }

    /**
     * @param string $calcValue1
     */
    public function setCalcValue1($calcValue1)
    {
        $this->calcValue1 = $calcValue1;
    }

    /**
     * @return string
     */
    public function getCalcValue2()
    {
        return $this->calcValue2;
    }

    /**
     * @param string $calcValue2
     */
    public function setCalcValue2($calcValue2)
    {
        $this->calcValue2 = $calcValue2;
    }

    public function getDefaultPayment()
    {
        return $this->defaultPayment;
    }

    public function setDefaultPayment($payment)
    {
        $this->defaultPayment = $payment;
    }

    public function getDefaultShipment()
    {
        return $this->defaultShipment;
    }

    public function setDefaultShipment($shipping)
    {
        $this->defaultShipment = $shipping;
    }

    /**
     * @return mixed
     */
    public function getCustom1()
    {
        return $this->custom1;
    }

    /**
     * @param mixed $custom1
     */
    public function setCustom1($custom1)
    {
        $this->custom1 = $custom1;
    }

    /**
     * @return mixed
     */
    public function getCustom2()
    {
        return $this->custom2;
    }

    /**
     * @param mixed $custom2
     */
    public function setCustom2($custom2)
    {
        $this->custom2 = $custom2;
    }

    /**
     * @return mixed
     */
    public function getCustom3()
    {
        return $this->custom3;
    }

    /**
     * @param mixed $custom3
     */
    public function setCustom3($custom3)
    {
        $this->custom3 = $custom3;
    }

    /**
     * @return mixed
     */
    public function getCustom4()
    {
        return $this->custom4;
    }

    /**
     * @param mixed $custom4
     */
    public function setCustom4($custom4)
    {
        $this->custom4 = $custom4;
    }

    /**
     * @return mixed
     */
    public function getCustom5()
    {
        return $this->custom5;
    }

    /**
     * @param mixed $custom5
     */
    public function setCustom5($custom5)
    {
        $this->custom5 = $custom5;
    }

    /**
     * @return mixed
     */
    public function getCustom6()
    {
        return $this->custom6;
    }

    /**
     * @param mixed $custom6
     */
    public function setCustom6($custom6)
    {
        $this->custom6 = $custom6;
    }

    /**
     * @return mixed
     */
    public function getCustom7()
    {
        return $this->custom7;
    }

    /**
     * @param mixed $custom7
     */
    public function setCustom7($custom7)
    {
        $this->custom7 = $custom7;
    }

    /**
     * @return mixed
     */
    public function getCustom8()
    {
        return $this->custom8;
    }

    /**
     * @param mixed $custom8
     */
    public function setCustom8($custom8)
    {
        $this->custom8 = $custom8;
    }

    /**
     * @return mixed
     */
    public function getCustom9()
    {
        return $this->custom9;
    }

    /**
     * @param mixed $custom9
     */
    public function setCustom9($custom9)
    {
        $this->custom9 = $custom9;
    }

    /**
     * @return mixed
     */
    public function getCustom10()
    {
        return $this->custom10;
    }

    /**
     * @param mixed $custom10
     */
    public function setCustom10($custom10)
    {
        $this->custom10 = $custom10;
    }

    /**
     * @return mixed
     */
    public function getCustom11()
    {
        return $this->custom11;
    }

    /**
     * @param mixed $custom11
     */
    public function setCustom11($custom11)
    {
        $this->custom11 = $custom11;
    }

    /**
     * @return mixed
     */
    public function getCustom12()
    {
        return $this->custom12;
    }

    /**
     * @param mixed $custom12
     */
    public function setCustom12($custom12)
    {
        $this->custom12 = $custom12;
    }

    /**
     * @return mixed
     */
    public function getCustom13()
    {
        return $this->custom13;
    }

    /**
     * @param mixed $custom13
     */
    public function setCustom13($custom13)
    {
        $this->custom13 = $custom13;
    }

    /**
     * @return mixed
     */
    public function getCustom14()
    {
        return $this->custom14;
    }

    /**
     * @param mixed $custom14
     */
    public function setCustom14($custom14)
    {
        $this->custom14 = $custom14;
    }

    /**
     * @return mixed
     */
    public function getCustom15()
    {
        return $this->custom15;
    }

    /**
     * @param mixed $custom15
     */
    public function setCustom15($custom15)
    {
        $this->custom15 = $custom15;
    }

    /**
     * @return mixed
     */
    public function getCustom16()
    {
        return $this->custom16;
    }

    /**
     * @param mixed $custom16
     */
    public function setCustom16($custom16)
    {
        $this->custom16 = $custom16;
    }

    /**
     * @return mixed
     */
    public function getCustom17()
    {
        return $this->custom17;
    }

    /**
     * @param mixed $custom17
     */
    public function setCustom17($custom17)
    {
        $this->custom17 = $custom17;
    }

    /**
     * @return mixed
     */
    public function getCustom18()
    {
        return $this->custom18;
    }

    /**
     * @param mixed $custom18
     */
    public function setCustom18($custom18)
    {
        $this->custom18 = $custom18;
    }

    /**
     * @return mixed
     */
    public function getCustom19()
    {
        return $this->custom19;
    }

    /**
     * @param mixed $custom19
     */
    public function setCustom19($custom19)
    {
        $this->custom19 = $custom19;
    }

    /**
     * @return mixed
     */
    public function getCustom20()
    {
        return $this->custom20;
    }

    /**
     * @param mixed $custom20
     */
    public function setCustom20($custom20)
    {
        $this->custom20 = $custom20;
    }

    /**
     * @return mixed
     */
    public function getCustom21()
    {
        return $this->custom21;
    }

    /**
     * @param mixed $custom21
     */
    public function setCustom21($custom21)
    {
        $this->custom21 = $custom21;
    }

    /**
     * @return mixed
     */
    public function getCustom22()
    {
        return $this->custom22;
    }

    /**
     * @param mixed $custom22
     */
    public function setCustom22($custom22)
    {
        $this->custom22 = $custom22;
    }

    /**
     * @return mixed
     */
    public function getCustom23()
    {
        return $this->custom23;
    }

    /**
     * @param mixed $custom23
     */
    public function setCustom23($custom23)
    {
        $this->custom23 = $custom23;
    }

    /**
     * @return mixed
     */
    public function getCustom24()
    {
        return $this->custom24;
    }

    /**
     * @param mixed $custom24
     */
    public function setCustom24($custom24)
    {
        $this->custom24 = $custom24;
    }

    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getFunction()
    {
        return $this->function;
    }

    /**
     * @param string $function
     */
    public function setFunction($function)
    {
        $this->function = $function;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getDestrict()
    {
        return $this->destrict;
    }

    /**
     * @param string $destrict
     */
    public function setDestrict($destrict)
    {
        $this->destrict = $destrict;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return bool
     */
    public function isNewsletter()
    {
        return $this->newsletter;
    }

    /**
     * @param bool $newsletter
     */
    public function setNewsletter($newsletter)
    {
        $this->newsletter = $newsletter;
    }

    /**
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * @param string $information
     */
    public function setInformation($information)
    {
        $this->information = $information;
    }

    /**
     * @return string
     */
    public function getDurchwahl()
    {
        return $this->durchwahl;
    }

    /**
     * @param string $durchwahl
     */
    public function setDurchwahl($durchwahl)
    {
        $this->durchwahl = $durchwahl;
    }

    /**
     * @return string
     */
    public function getAbteilung()
    {
        return $this->abteilung;
    }

    /**
     * @param string $abteilung
     */
    public function setAbteilung($abteilung)
    {
        $this->abteilung = $abteilung;
    }
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * @param string $birthday
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;
    }

    /**
     * @return string
     */
    public function getKostenstellung()
    {
        return $this->kostenstellung;
    }

    /**
     * @param string $kostenstellung
     */
    public function setKostenstellung($kostenstellung)
    {
        $this->kostenstellung = $kostenstellung;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getBankKtoName()
    {
        return $this->bankKtoName;
    }

    /**
     * @param string $bankKtoName
     */
    public function setBankKtoName($bankKtoName)
    {
        $this->bankKtoName = $bankKtoName;
    }

    /**
     * @return string
     */
    public function getBankKTO()
    {
        return $this->bankKTO;
    }

    /**
     * @param string $bankKTO
     */
    public function setBankKTO($bankKTO)
    {
        $this->bankKTO = $bankKTO;
    }

    /**
     * @return string
     */
    public function getBankBLZ()
    {
        return $this->bankBLZ;
    }

    /**
     * @param string $bankBLZ
     */
    public function setBankBLZ($bankBLZ)
    {
        $this->bankBLZ = $bankBLZ;
    }

    /**
     * @return string
     */
    public function getBankIban()
    {
        return $this->bankIban;
    }

    /**
     * @param string $bankIban
     */
    public function setBankIban($bankIban)
    {
        $this->bankIban = $bankIban;
    }

    /**
     * @return string
     */
    public function getBankBic()
    {
        return $this->bankBic;
    }

    /**
     * @param string $bankBic
     */
    public function setBankBic($bankBic)
    {
        $this->bankBic = $bankBic;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return string
     */
    public function getVonWo()
    {
        return $this->vonWo;
    }

    /**
     * @param string $vonWo
     */
    public function setVonWo($vonWo)
    {
        $this->vonWo = $vonWo;
    }

    /**
     * @return bool
     */
    public function isMwert()
    {
        return $this->mwert;
    }

    /**
     * @param bool $mwert
     */
    public function setMwert($mwert)
    {
        $this->mwert = $mwert;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getCompany2()
    {
        return $this->company2;
    }

    /**
     * @param string $company2
     */
    public function setCompany2($company2)
    {
        $this->company2 = $company2;
    }

    /**
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param string $homepage
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @return int
     */
    public function getSalutation()
    {
        return $this->salutation;
    }

    /**
     * @param int $salutation
     */
    public function setSalutation($salutation)
    {
        $this->salutation = $salutation;
    }

    /**
     * @return string
     */
    public function getPhoneAreaCode()
    {
        return $this->phoneAreaCode;
    }

    /**
     * @param string $phoneAreaCode
     */
    public function setPhoneAreaCode($phoneAreaCode)
    {
        $this->phoneAreaCode = $phoneAreaCode;
    }

    /**
     * @return string
     */
    public function getAlternativType()
    {
        return $this->alternativType;
    }

    /**
     * @param string $alternativType
     */
    public function setAlternativType($alternativType)
    {
        $this->alternativType = $alternativType;
    }

    /**
     * @return string
     */
    public function getAlternativ()
    {
        return $this->alternativ;
    }

    /**
     * @param string $alternativ
     */
    public function setAlternativ($alternativ)
    {
        $this->alternativ = $alternativ;
    }

    /**
     * @return string
     */
    public function getPhonePrefix()
    {
        return $this->phonePrefix;
    }

    /**
     * @param string $phonePrefix
     */
    public function setPhonePrefix($phonePrefix)
    {
        $this->phonePrefix = $phonePrefix;
    }

    /**
     * @return string
     */
    public function getPhoneAppendix()
    {
        return $this->phoneAppendix;
    }

    /**
     * @param string $phoneAppendix
     */
    public function setPhoneAppendix($phoneAppendix)
    {
        $this->phoneAppendix = $phoneAppendix;
    }

    /**
     * @return string
     */
    public function getMobilePrefix()
    {
        return $this->mobilePrefix;
    }

    /**
     * @param string $mobilePrefix
     */
    public function setMobilePrefix($mobilePrefix)
    {
        $this->mobilePrefix = $mobilePrefix;
    }

    /**
     * @return string
     */
    public function getMobileAppendix()
    {
        return $this->mobileAppendix;
    }

    /**
     * @param string $mobileAppendix
     */
    public function setMobileAppendix($mobileAppendix)
    {
        $this->mobileAppendix = $mobileAppendix;
    }

    /**
     * @return string
     */
    public function getFaxPrefix()
    {
        return $this->faxPrefix;
    }

    /**
     * @param string $faxPrefix
     */
    public function setFaxPrefix($faxPrefix)
    {
        $this->faxPrefix = $faxPrefix;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getFaxAppendix()
    {
        return $this->faxAppendix;
    }

    /**
     * @param string $faxAppendix
     */
    public function setFaxAppendix($faxAppendix)
    {
        $this->faxAppendix = $faxAppendix;
    }

    /**
     * @return string
     */
    public function getAlternativAppendix()
    {
        return $this->alternativAppendix;
    }

    /**
     * @param string $alternativAppendix
     */
    public function setAlternativAppendix($alternativAppendix)
    {
        $this->alternativAppendix = $alternativAppendix;
    }

    /**
     * @return string
     */
    public function getMobileAreaCode()
    {
        return $this->mobileAreaCode;
    }

    /**
     * @param string $mobileAreaCode
     */
    public function setMobileAreaCode($mobileAreaCode)
    {
        $this->mobileAreaCode = $mobileAreaCode;
    }

    /**
     * @return string
     */
    public function getFaxAreaCode()
    {
        return $this->faxAreaCode;
    }

    /**
     * @param string $faxAreaCode
     */
    public function setFaxAreaCode($faxAreaCode)
    {
        $this->faxAreaCode = $faxAreaCode;
    }

    /**
     * @return string
     */
    public function getAlternativAreaCode()
    {
        return $this->alternativAreaCode;
    }

    /**
     * @param string $alternativAreaCode
     */
    public function setAlternativAreaCode($alternativAreaCode)
    {
        $this->alternativAreaCode = $alternativAreaCode;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return mixed
     */
    public function getRoles()
    {
        return $this->getRolesAsString();
    }

    /**
     * @return mixed
     */
    public function getRolesForm()
    {
        return $this->roles;
    }

    /**
     * @param mixed $roles
     */
    public function setRolesForm($roles)
    {
        $this->roles = $roles;
    }

    public function getRolesAsString()
    {
        $temp = array();

        foreach ($this->roles as $row) {
            if ($row->getUid() == 5 || $row->getUid() == 6) {
                $temp[] = "ROLE_USER";
            }
            if ($row->getUid() == 8) {
                $temp[] = "ROLE_PRODUCT_EDITOR";
            }
            if ($row->getUid() == 4) {
                $temp[] = "ROLE_SHOP_OPERATOR";
            }
            if ($row->getUid() == 7) {
                $temp[] = "ROLE_PRODUCTION";
            }
            if ($row->getUid() == 3) {
                $temp[] = "ROLE_ADMIN";
            }
            if ($row->getUid() == 2) {
                $temp[] = "ROLE_SYSTEM_ADMIN";
            }
        }

        return $temp;
    }

    /**
     * @return Account
     */
    public function getCollectingOrdersAccounts()
    {
        return $this->collectingOrdersAccounts;
    }

    /**
     * @param Account $collectingOrdersAccounts
     */
    public function setCollectingOrdersAccounts($collectingOrdersAccounts)
    {
        $this->collectingOrdersAccounts = $collectingOrdersAccounts;
    }

    /**
     * @return mixed
     */
    public function getShops()
    {
        return $this->shops;
    }

    /**
     * @param mixed $shops
     */
    public function setShops($shops)
    {
        $this->shops = $shops;
    }

    /**
     * @return mixed
     */
    public function getShopContact()
    {
        return $this->shopContact;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function isShowOtherOrders()
    {
        return $this->showOtherOrders;
    }

    /**
     * @param mixed $showOtherOrders
     */
    public function setShowOtherOrders($showOtherOrders)
    {
        $this->showOtherOrders = $showOtherOrders;
    }

    /**
     * @return mixed
     */
    public function getShowOtherOrdersAccountFilter()
    {
        return $this->showOtherOrdersAccountFilter;
    }

    /**
     * @param mixed $showOtherOrdersAccountFilter
     */
    public function setShowOtherOrdersAccountFilter($showOtherOrdersAccountFilter)
    {
        $this->showOtherOrdersAccountFilter = $showOtherOrdersAccountFilter;
    }

    /**
     * @return mixed
     */
    public function getShowOtherOrdersAccount()
    {
        return $this->showOtherOrdersAccount;
    }

    /**
     * @param mixed $showOtherOrdersAccount
     */
    public function setShowOtherOrdersAccount($showOtherOrdersAccount)
    {
        $this->showOtherOrdersAccount = $showOtherOrdersAccount;
    }

    /**
     * @return mixed
     */
    public function getProductsOrg()
    {
        return $this->productsOrg;
    }

    /**
     * @param mixed $productsOrg
     */
    public function setProductsOrg($productsOrg)
    {
        $this->productsOrg = $productsOrg;
    }

    /**
     * @return mixed
     */
    public function getProductsSub()
    {
        return $this->productsSub;
    }

    /**
     * @param mixed $productsSub
     */
    public function setProductsSub($productsSub)
    {
        $this->productsSub = $productsSub;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @param $module
     * @param $key
     * @param $value
     */
    public function setPluginSettingModule($module, $key, $value)
    {
        $this->pluginSettings[$module][$key] = $value;
    }

    public function getPluginSettingModule($module, $key)
    {
        if (!isset($this->pluginSettings[$module]) || !isset($this->pluginSettings[$module][$key])) {
            return null;
        }

        return $this->pluginSettings[$module][$key];
    }

    /**
     * @return \DateTime
     */
    public function getPasswordResetDate()
    {
        return $this->passwordResetDate;
    }

    /**
     * @param \DateTime $passwordResetDate
     */
    public function setPasswordResetDate($passwordResetDate)
    {
        $this->passwordResetDate = $passwordResetDate;
    }

    /**
     * @return string
     */
    public function getPasswordResetUuid()
    {
        return $this->passwordResetUuid;
    }

    /**
     * @param string $passwordResetUuid
     */
    public function setPasswordResetUuid($passwordResetUuid)
    {
        $this->passwordResetUuid = $passwordResetUuid;
    }

    /**
     * @return boolean
     */
    public function isVirtual()
    {
        return $this->virtual;
    }

    /**
     * @param boolean $virtual
     */
    public function setVirtual($virtual)
    {
        $this->virtual = $virtual;
    }

    /**
     * @return mixed
     */
    public function getProductGroups()
    {
        return $this->productGroups;
    }

    /**
     * @param mixed $productGroups
     */
    public function setProductGroups($productGroups)
    {
        $this->productGroups = $productGroups;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return mixed
     */
    public function getPriceFactor()
    {
        if (!$this->priceFactor) {
            return 1;
        }
        return $this->priceFactor;
    }

    /**
     * @param mixed $priceFactor
     */
    public function setPriceFactor($priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }
}
