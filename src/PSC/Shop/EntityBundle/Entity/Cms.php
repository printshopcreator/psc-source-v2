<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Cms
 *
 * @ORM\Table(name="cms")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\CmsRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Cms
{
    protected $extraSettings;
    protected $pluginSettings = [];
    public function __toString(): string
    {
        return $this->title;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;
/**
     * Titel
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;
/**
     * Sort
     *
     * @var float
     *
     * @ORM\Column(name="sor", type="integer")
     */
    protected $sort;
/**
     * Enable
     *
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean")
     */
    protected $enable;
/**
     * Pos
     *
     * @var string
     *
     * @ORM\Column(name="pos", type="text")
     */
    protected $pos;
/**
     * Menu
     *
     * @var string
     *
     * @ORM\Column(name="menu", type="text")
     */
    protected $menu;
/**
     * Shop zu welcher zu dem Shop gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Text
     *
     * @var string
     *
     * @ORM\Column(name="text1", type="text")
     */
    protected $text;
/**
     * NotInMenu
     *
     * @var boolean
     *
     * @ORM\Column(name="notinmenu", type="boolean")
     */
    protected $notInMenu;
/**
     * @ORM\Column(name="url", length=255, unique=true)
     */
    protected $url;
/**
     * Language
     *
     * @var string
     *
     * @ORM\Column(name="language", type="string")
     */
    protected $language;
/**
     * private
     *
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean")
     */
    protected $private;
/**
     * displayTitle
     *
     * @var boolean
     *
     * @ORM\Column(name="display_title", type="boolean")
     */
    protected $displayTitle;
/**
     * @var boolean
     *
     * @ORM\Column(name="display_only_logged_in", type="boolean")
     */
    protected $displayOnlyWhenLoggedIn;
/**
     * metaKeywords
     *
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", nullable=true)
     */
    protected $metaKeywords;
/**
     * metaAuthor
     *
     * @var string
     *
     * @ORM\Column(name="meta_author", type="string", nullable=true)
     */
    protected $metaAuthor;
/**
     * metaDescription
     *
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", nullable=true)
     */
    protected $metaDescription;
/**
     * metaCustomTitle
     *
     * @var string
     *
     * @ORM\Column(name="meta_custom_title", type="string", nullable=true)
     */
    protected $metaCustomTitle;
/**
     * metaOgTitle
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_title", type="string", nullable=true)
     */
    protected $metaOgTitle;
/**
     * metaOgType
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_type", type="string", nullable=true)
     */
    protected $metaOgType;
/**
     * metaOgUrl
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_url", type="string", nullable=true)
     */
    protected $metaOgUrl;
/**
     * metaOgImage
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_image", type="string", nullable=true)
     */
    protected $metaOgImage;
/**
     * metaOgDescription
     *
     * @var string
     *
     * @ORM\Column(name="meta_og_description", type="string", nullable=true)
     */
    protected $metaOgDescription;
/**
     * Parent
     *
     * @var integer
     *
     * @ORM\Column(name="parent", type="integer")
     */
    protected $parentRaw;
/**
     * @ORM\OneToMany(targetEntity="Cms", mappedBy="parent")
     **/
    private $children;
/**
     * @ORM\ManyToOne(targetEntity="Cms", inversedBy="children")
     * @ORM\JoinColumn(name="parent", referencedColumnName="id")
     **/
    private $parent;

    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param string $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return int
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param int $menu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return boolean
     */
    public function isNotInMenu()
    {
        return $this->notInMenu;
    }

    /**
     * @param boolean $notInMenu
     */
    public function setNotInMenu($notInMenu)
    {
        $this->notInMenu = $notInMenu;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    public function __set($name, $value)
    {
        $this->pluginSettings[$name] = $value;
    }

    public function __get($name)
    {
        if (isset($this->pluginSettings[$name])) {
            return $this->pluginSettings[$name];
        }

        return false;
    }

    public function __isset($name)
    {
        if (isset($this->pluginSettings[$name])) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param bool $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getMetaAuthor()
    {
        return $this->metaAuthor;
    }

    /**
     * @param string $metaAuthor
     */
    public function setMetaAuthor($metaAuthor)
    {
        $this->metaAuthor = $metaAuthor;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaCustomTitle()
    {
        return $this->metaCustomTitle;
    }

    /**
     * @param string $metaCustomTitle
     */
    public function setMetaCustomTitle($metaCustomTitle)
    {
        $this->metaCustomTitle = $metaCustomTitle;
    }

    /**
     * @return string
     */
    public function getMetaOgType()
    {
        return $this->metaOgType;
    }

    /**
     * @param string $metaOgType
     */
    public function setMetaOgType($metaOgType)
    {
        $this->metaOgType = $metaOgType;
    }

    /**
     * @return string
     */
    public function getMetaOgUrl()
    {
        return $this->metaOgUrl;
    }

    /**
     * @param string $metaOgUrl
     */
    public function setMetaOgUrl($metaOgUrl)
    {
        $this->metaOgUrl = $metaOgUrl;
    }

    /**
     * @return string
     */
    public function getMetaOgImage()
    {
        return $this->metaOgImage;
    }

    /**
     * @param string $metaOgImage
     */
    public function setMetaOgImage($metaOgImage)
    {
        $this->metaOgImage = $metaOgImage;
    }

    /**
     * @return string
     */
    public function getMetaOgDescription()
    {
        return $this->metaOgDescription;
    }

    /**
     * @param string $metaOgDescription
     */
    public function setMetaOgDescription($metaOgDescription)
    {
        $this->metaOgDescription = $metaOgDescription;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        if ($this->getParentRaw() == 0) {
            return false;
        }
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return int
     */
    public function getParentRaw()
    {
        return $this->parentRaw;
    }

    /**
     * @param int $parentRaw
     */
    public function setParentRaw($parentRaw)
    {
        $this->parentRaw = $parentRaw;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getMetaOgTitle()
    {
        return $this->metaOgTitle;
    }

    /**
     * @param string $metaOgTitle
     */
    public function setMetaOgTitle($metaOgTitle)
    {
        $this->metaOgTitle = $metaOgTitle;
    }

    /**
     * @return float
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * @param float $sort
     */
    public function setSort($sort)
    {
        $this->sort = $sort;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return bool
     */
    public function isDisplayTitle()
    {
        return $this->displayTitle;
    }

    /**
     * @param bool $displayTitle
     */
    public function setDisplayTitle($displayTitle)
    {
        $this->displayTitle = $displayTitle;
    }

    /**
     * @return bool
     */
    public function isDisplayOnlyWhenLoggedIn()
    {
        return $this->displayOnlyWhenLoggedIn;
    }

    /**
     * @param bool $displayOnlyWhenLoggedIn
     */
    public function setDisplayOnlyWhenLoggedIn($displayOnlyWhenLoggedIn)
    {
        $this->displayOnlyWhenLoggedIn = $displayOnlyWhenLoggedIn;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }
}
