<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PSC\Shop\EntityBundle\Entity\Generated\Shippingtype;

/**
 * Shipping
 *
 * @ORM\Table(name="shippingtype")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ShippingRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Shipping extends Shippingtype
{
    /**
     * Id der Versandart
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Titel der Versandart
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;
/**
     * Parameter 1
     *
     * @var string
     *
     * @ORM\Column(name="art", type="string", length=255)
     */
    protected $parameter1;
/**
     * Beschreibung
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     */
    protected $description;
/**
     * Matrix
     *
     * @var string
     *
     * @ORM\Column(name="xml", type="string")
     */
    protected $matrix;
/**
     * Einmalige Kosten der Versandart
     *
     * @var float
     *
     * @ORM\Column(name="kosten_fix", type="float")
     */
    protected $price = 0;
/**
     * Prozentuale Kosten
     *
     * @var float
     *
     * @ORM\Column(name="kosten_percent", type="float")
     */
    protected $pricePercent;
/**
     * Position der Versandart
     *
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer")
     */
    protected $pos;
/**
     * MWert
     *
     * @var integer
     *
     * @ORM\Column(name="mwert", type="decimal")
     */
    protected $taxClass;
/**
     * Gewicht von
     *
     * @var float
     *
     * @ORM\Column(name="weight_from", type="integer")
     */
    protected $weightFrom = 0;
/**
     * Kosten von
     *
     * @var float
     *
     * @ORM\Column(name="price_from", type="integer")
     */
    protected $priceFrom = 0;
/**
     * Gewicht bis
     *
     * @var float
     *
     * @ORM\Column(name="weight_to", type="integer")
     */
    protected $weightTo = 0;
/**
     * Kosten bis
     *
     * @var float
     *
     * @ORM\Column(name="price_to", type="integer")
     */
    protected $priceTo = 0;
/**
     * Shop zu welcher die Versandart gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Privat
     *
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean")
     */
    protected $private;
/**
     * Enable
     *
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean")
     */
    protected $enable = true;
/**
     * Zahlart ausblenden
     *
     * @var boolean
     *
     * @ORM\Column(name="no_payment", type="boolean")
     */
    protected $noPayment;
/**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * Gibt den Titel zurück
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Setzt den Titel
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gibt den Verknüpften Shop zurück
     *
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Setzt den verknüpften Shop
     *
     * @param Shop $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * Gibt die Fixen Kosten zurück
     *
     * @return float
     */
    public function getKosten()
    {
        return $this->kosten;
    }

    /**
     * Setzt die Fixen Kosten
     *
     * @param float $kosten
     */
    public function setKosten($kosten)
    {
        $this->kosten = floatval(str_replace(",", ".", $kosten));
    }

    /**
     * Gibt die Postion zurück
     *
     * @return float
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Setzt die Position der Versandart
     *
     * @param float $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    public function asArray()
    {
        return array(
            'title' => $this->getTitle(),
            'pos' => $this->getPos(),
            'id' => $this->getUID(),
            'price' => $this->getPrice(),
        );
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getTaxClass()
    {
        return $this->taxClass;
    }

    /**
     * @param int $taxClass
     */
    public function setTaxClass($taxClass)
    {
        $this->taxClass = $taxClass;
    }

    /**
     * @return float
     */
    public function getWeightFrom()
    {
        return $this->weightFrom;
    }

    /**
     * @param float $weightFrom
     */
    public function setWeightFrom($weightFrom)
    {
        $this->weightFrom = $weightFrom;
    }

    /**
     * @return float
     */
    public function getWeightTo()
    {
        return $this->weightTo;
    }

    /**
     * @param float $weightTo
     */
    public function setWeightTo($weightTo)
    {
        $this->weightTo = $weightTo;
    }

    /**
     * @return bool
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param bool $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return bool
     */
    public function isNoPayment()
    {
        return $this->noPayment;
    }

    /**
     * @param bool $noPayment
     */
    public function setNoPayment($noPayment)
    {
        $this->noPayment = $noPayment;
    }

    /**
     * @return string
     */
    public function getMatrix()
    {
        return $this->matrix;
    }

    /**
     * @param string $matrix
     */
    public function setMatrix($matrix)
    {
        $this->matrix = $matrix;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getPricePercent()
    {
        return $this->pricePercent;
    }

    /**
     * @param float $pricePercent
     */
    public function setPricePercent($pricePercent)
    {
        $this->pricePercent = $pricePercent;
    }

    /**
     * @return string
     */
    public function getParameter1()
    {
        return $this->parameter1;
    }

    /**
     * @param string $parameter1
     */
    public function setParameter1($parameter1)
    {
        $this->parameter1 = $parameter1;
    }

    /**
     * @return float
     */
    public function getPriceFrom()
    {
        return $this->priceFrom;
    }

    /**
     * @param float $priceFrom
     */
    public function setPriceFrom($priceFrom)
    {
        $this->priceFrom = $priceFrom;
    }

    /**
     * @return float
     */
    public function getPriceTo()
    {
        return $this->priceTo;
    }

    /**
     * @param float $priceTo
     */
    public function setPriceTo($priceTo)
    {
        $this->priceTo = $priceTo;
    }
}
