<?php

namespace PSC\Shop\EntityBundle\Entity\Generated;

use Doctrine\ORM\Mapping as ORM;

class Paymenttype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var integer
     *
     * @ORM\Column(name="install_id", type="bigint", nullable=true)
     */
    private $installId;

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="bigint", nullable=true)
     */
    private $shopId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean", nullable=true)
     */
    private $enable;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prozent", type="boolean", nullable=true)
     */
    private $prozent;

    /**
     * @var float
     *
     * @ORM\Column(name="wert", type="float", precision=10, scale=0, nullable=true)
     */
    private $wert;

    /**
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean", nullable=true)
     */
    private $private;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer", nullable=false)
     */
    private $pos;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="trustedshop_name", type="string", length=255, nullable=true)
     */
    private $trustedshopName;
}
