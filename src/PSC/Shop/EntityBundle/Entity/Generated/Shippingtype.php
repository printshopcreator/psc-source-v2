<?php

namespace PSC\Shop\EntityBundle\Entity\Generated;

use Doctrine\ORM\Mapping as ORM;

class Shippingtype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=true)
     */
    private $updated;

    /**
     * @var integer
     *
     * @ORM\Column(name="install_id", type="bigint", nullable=true)
     */
    private $installId;

    /**
     * @var integer
     *
     * @ORM\Column(name="shop_id", type="bigint", nullable=true)
     */
    private $shopId;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean", nullable=true)
     */
    private $enable;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="art", type="string", length=255, nullable=true)
     */
    private $art;

    /**
     * @var string
     *
     * @ORM\Column(name="land", type="string", length=255, nullable=true)
     */
    private $land;

    /**
     * @var integer
     *
     * @ORM\Column(name="mwert", type="bigint", nullable=true)
     */
    private $mwert;

    /**
     * @var string
     *
     * @ORM\Column(name="kosten_a", type="string", length=255, nullable=true)
     */
    private $kostenA;

    /**
     * @var string
     *
     * @ORM\Column(name="kosten_b", type="string", length=255, nullable=true)
     */
    private $kostenB;

    /**
     * @var string
     *
     * @ORM\Column(name="kosten_c", type="string", length=255, nullable=true)
     */
    private $kostenC;

    /**
     * @var string
     *
     * @ORM\Column(name="kosten_d", type="string", length=255, nullable=true)
     */
    private $kostenD;

    /**
     * @var string
     *
     * @ORM\Column(name="kosten_e", type="string", length=255, nullable=true)
     */
    private $kostenE;

    /**
     * @var string
     *
     * @ORM\Column(name="kosten_f", type="string", length=255, nullable=true)
     */
    private $kostenF;

    /**
     * @var string
     *
     * @ORM\Column(name="kosten_fix", type="string", length=255, nullable=true)
     */
    private $kostenFix;

    /**
     * @var integer
     *
     * @ORM\Column(name="pos", type="integer", nullable=false)
     */
    private $pos;

    /**
     * @var integer
     *
     * @ORM\Column(name="no_payment", type="integer", nullable=false)
     */
    private $noPayment;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=false)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="private", type="integer", nullable=false)
     */
    private $private;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight_from", type="integer", nullable=false)
     */
    private $weightFrom;

    /**
     * @var integer
     *
     * @ORM\Column(name="weight_to", type="integer", nullable=false)
     */
    private $weightTo;

    /**
     * @var string
     *
     * @ORM\Column(name="xml", type="text", nullable=false)
     */
    private $xml;

    /**
     * @var float
     *
     * @ORM\Column(name="kosten_percent", type="float", precision=10, scale=0, nullable=true)
     */
    private $kostenPercent;
}
