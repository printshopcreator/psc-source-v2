<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PSC\Shop\EntityBundle\Entity\Generated\Paymenttype;

/**
 * Order
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\OrderRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Order
{
    protected $pluginSettings = [];
/**
     * Id des Shops
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * UUID
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string")
     */
    protected $uuid;
/**
     * Brutto
     *
     * @var string
     *
     * @ORM\Column(name="preisbrutto", type="float")
     */
    protected $brutto;
/**
     * Created
     *
     * @var DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;
/**
     * Updated
     *
     * @var DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updated;
/**
     * Alias
     *
     * @var string
     *
     * @ORM\Column(name="alias", type="string")
     */
    protected $alias;
/**
     * Package
     *
     * @var string
     *
     * @ORM\Column(name="package", type="string")
     */
    protected $package;
/**
     * Status
     *
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;
/**
     * @ORM\OneToMany(targetEntity="Orderpos", mappedBy="order")
     **/
    private $positions;
/**
     * Shop zu welcher das Product gehört
     *
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Install zu welcher die Bestellung gehört
     *
     * @var Install
     *
     * @ORM\ManyToOne(targetEntity="Install")
     * @ORM\JoinColumn(name="install_id", referencedColumnName="id")
     */
    protected $install;
/**
     * Account zu welcher das Motiv gehört
     *
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;
/**
     * Contact zu welcher das Motiv gehört
     *
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;
/**
     * @var Payment
     *
     * @ORM\ManyToOne(targetEntity="Payment")
     * @ORM\JoinColumn(name="paymenttype_id", referencedColumnName="id")
     */
    protected $paymentType;
/**
     * @var Shipping
     *
     * @ORM\ManyToOne(targetEntity="Shipping")
     * @ORM\JoinColumn(name="shippingtype_id", referencedColumnName="id")
     */
    protected $shippingType;
/**
     * @var float
     *
     * @ORM\Column(name="preis", type="float", precision=10, scale=0, nullable=false)
     */
    protected $netto;
/**
     * @var float
     *
     * @ORM\Column(name="preissteuer", type="float", precision=10, scale=0, nullable=true)
     */
    protected $steuer;
/**
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean", nullable=false)
     */
    protected $enable;
/**
     * @var boolean
     *
     * @ORM\Column(name="delivery_same", type="boolean", nullable=true)
     */
    protected $deliverySame;
/**
     * @var boolean
     *
     * @ORM\Column(name="sender_same", type="boolean", nullable=true)
     */
    protected $senderSame;
/**
     * @var string
     *
     * @ORM\Column(name="info", type="text", nullable=true)
     */
    protected $info;
/**
     * @var string
     *
     * @ORM\Column(name="basketfield1", type="text", nullable=true)
     */
    protected $basketfield1;
/**
     * @var string
     *
     * @ORM\Column(name="basketfield2", type="text", nullable=true)
     */
    protected $basketfield2;
/**
     * @var string
     *
     * @ORM\Column(name="gutschein", type="string", length=40, nullable=true)
     */
    protected $gutschein;
/**
     * @var float
     *
     * @ORM\Column(name="gutscheinabzug", type="float", precision=10, scale=0, nullable=true)
     */
    protected $gutscheinabzug;
/**
     * @var ContactAddress
     *
     * @ORM\ManyToOne(targetEntity="ContactAddress")
     * @ORM\JoinColumn(name="delivery_address", referencedColumnName="id")
     */
    protected $deliveryAddress;
#


    /**
     * @var ContactAddress
     *
     * @ORM\ManyToOne(targetEntity="ContactAddress")
     * @ORM\JoinColumn(name="sender_address", referencedColumnName="id")
     */
    protected $senderAddress = null;
/**
     * @var ContactAddress
     *
     * @ORM\ManyToOne(targetEntity="ContactAddress")
     * @ORM\JoinColumn(name="invoice_address", referencedColumnName="id")
     */
    protected $invoiceAddress;
/**
     * @var float
     *
     * @ORM\Column(name="versandkosten", type="float", precision=10, scale=0, nullable=false)
     */
    protected $versandKosten;
/**
     * @var float
     *
     * @ORM\Column(name="zahlkosten", type="float", precision=10, scale=0, nullable=false)
     */
    protected $zahlKosten;
/**
     * @var integer
     *
     * @ORM\Column(name="gutscheinabzugtyp", type="integer", nullable=false)
     */
    protected $gutscheinAbzugTyp;
/**
     * @var integer
     *
     * @ORM\Column(name="use_account_as_invoice", type="integer", nullable=true)
     */
    protected $useAccountAsInvoice;
/**
     * @var string
     *
     * @ORM\Column(name="mwertalle", type="text", nullable=false)
     */
    protected $mwertAlle;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="delivery_date", type="date", nullable=false)
     */
    protected $deliveryDate;
/**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=false)
     */
    protected $version;
/**
     * @var string
     *
     * @ORM\Column(name="shippingtype_extra_label", type="string", length=255, nullable=true)
     */
    protected $shippingtypeExtraLabel;
/**
     * @var string
     *
     * @ORM\Column(name="file1", type="string", length=255, nullable=true)
     */
    protected $file1;
/**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=2, nullable=true)
     */
    protected $lang;
/**
     * @var int
     *
     * @ORM\Column(name="paymenttype_id", type="integer", length=8, nullable=true)
     */
    protected $paymentTypeId;
/**
     * @var int
     *
     * @ORM\Column(name="shippingtype_id", type="integer", length=8, nullable=true)
     */
    protected $shippingTypeId;
/**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", length=8, nullable=true)
     */
    protected $type;
/**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Shop $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Contact
     */
    public function getContact()
    {
        if ($this->contact == null) {
            return new Contact();
        }
        return $this->contact;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return $this->alias;
    }

    /**
     * @param string $alias
     */
    public function setAlias($alias)
    {
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getBrutto()
    {
        return $this->brutto;
    }

    /**
     * @param string $brutto
     */
    public function setBrutto($brutto)
    {
        $this->brutto = $brutto;
    }

    /**
     * @return string
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param string $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getPackage()
    {
        return $this->package;
    }

    /**
     * @param string $package
     */
    public function setPackage($package)
    {
        $this->package = $package;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return Payment
     */
    public function getPaymentType()
    {
        if ($this->paymentTypeId == 0 || $this->paymentTypeId == "") {
            return new Payment();
        }
        return $this->paymentType;
    }

    /**
     * @param Payment $paymentType
     */
    public function setPaymentType($paymentType)
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return Shipping
     */
    public function getShippingType()
    {
        if ($this->shippingTypeId == 0 || $this->shippingTypeId == "") {
            return new Shipping();
        }
        return $this->shippingType;
    }

    /**
     * @param Shipping $shippingType
     */
    public function setShippingType($shippingType)
    {
        $this->shippingType = $shippingType;
    }

    /**
     * @return float
     */
    public function getNetto()
    {
        return $this->netto;
    }

    /**
     * @param float $netto
     */
    public function setNetto($netto)
    {
        $this->netto = $netto;
    }

    /**
     * @return float
     */
    public function getSteuer()
    {
        return $this->steuer;
    }

    /**
     * @param float $steuer
     */
    public function setSteuer($steuer)
    {
        $this->steuer = $steuer;
    }

    /**
     * @return boolean
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param boolean $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return string
     */
    public function getInfo()
    {
        if ($this->info == "") {
            return array();
        }

        return json_decode($this->info, true);
    }

    /**
     * @param string $info
     */
    public function setInfo($info)
    {
        $this->info = $info;
    }

    /**
     * @return string
     */
    public function getBasketfield1()
    {
        return $this->basketfield1;
    }

    /**
     * @param string $basketfield1
     */
    public function setBasketfield1($basketfield1)
    {
        $this->basketfield1 = $basketfield1;
    }

    /**
     * @return string
     */
    public function getBasketfield2()
    {
        return $this->basketfield2;
    }

    /**
     * @param string $basketfield2
     */
    public function setBasketfield2($basketfield2)
    {
        $this->basketfield2 = $basketfield2;
    }

    /**
     * @return string
     */
    public function getGutschein()
    {
        return $this->gutschein;
    }

    /**
     * @param string $gutschein
     */
    public function setGutschein($gutschein)
    {
        $this->gutschein = $gutschein;
    }

    /**
     * @return float
     */
    public function getGutscheinabzug()
    {
        return $this->gutscheinabzug;
    }

    /**
     * @param float $gutscheinabzug
     */
    public function setGutscheinabzug($gutscheinabzug)
    {
        $this->gutscheinabzug = $gutscheinabzug;
    }

    /**
     * @return ContactAddress
     */
    public function getDeliveryAddress()
    {
        if ($this->deliverySame || $this->deliveryAddress->getUid() == 0) {
            return $this->getInvoiceAddress();
        }
        return $this->deliveryAddress;
    }

    /**
     * @param ContactAddress $deliveryAddress
     */
    public function setDeliveryAddress($deliveryAddress)
    {
        $this->deliveryAddress = $deliveryAddress;
    }

    /**
     * @return ContactAddress
     */
    public function getSenderAddress()
    {
        if ($this->senderAddress && $this->senderAddress->getUid() == 0) {
            return false;
        }
        return $this->senderAddress;
    }

    /**
     * @param ContactAddress $senderAddress
     */
    public function setSenderAddress($senderAddress)
    {
        $this->senderAddress = $senderAddress;
    }

    /**
     * @return ContactAddress
     */
    public function getInvoiceAddress()
    {
        if (!$this->invoiceAddress || ($this->invoiceAddress && $this->invoiceAddress->getUid() == 0)) {
            return $this->getContact()->getStandartInvoice();
        }
        return $this->invoiceAddress;
    }

    /**
     * @param ContactAddress $invoiceAddress
     */
    public function setInvoiceAddress($invoiceAddress)
    {
        $this->invoiceAddress = $invoiceAddress;
    }

    /**
     * @return float
     */
    public function getVersandKosten()
    {
        return $this->versandKosten;
    }

    /**
     * @param float $versandKosten
     */
    public function setVersandKosten($versandKosten)
    {
        $this->versandKosten = $versandKosten;
    }

    /**
     * @return float
     */
    public function getZahlKosten()
    {
        return $this->zahlKosten;
    }

    /**
     * @param float $zahlKosten
     */
    public function setZahlKosten($zahlKosten)
    {
        $this->zahlKosten = $zahlKosten;
    }

    /**
     * @return int
     */
    public function getGutscheinAbzugTyp()
    {
        return $this->gutscheinAbzugTyp;
    }

    /**
     * @param int $gutscheinAbzugTyp
     */
    public function setGutscheinAbzugTyp($gutscheinAbzugTyp)
    {
        $this->gutscheinAbzugTyp = $gutscheinAbzugTyp;
    }

    /**
     * @return int
     */
    public function getUseAccountAsInvoice()
    {
        return $this->useAccountAsInvoice;
    }

    /**
     * @param int $useAccountAsInvoice
     */
    public function setUseAccountAsInvoice($useAccountAsInvoice)
    {
        $this->useAccountAsInvoice = $useAccountAsInvoice;
    }

    /**
     * @return string
     */
    public function getMwertAlle()
    {
        $mwert = json_decode($this->mwertAlle, true);
        $tmpWert = array();
        foreach ($mwert as $key => $mw) {
            if ($mw > 0) {
                $tmpWert[$key] = $mw;
            }
        }
        return $tmpWert;
    }

    /**
     * @param string $mwertAlle
     */
    public function setMwertAlle($mwertAlle)
    {
        $this->mwertAlle = $mwertAlle;
    }

    /**
     * @return \DateTime
     */
    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate($deliveryDate)
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param int $version
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * @return string
     */
    public function getShippingtypeExtraLabel()
    {
        return $this->shippingtypeExtraLabel;
    }

    /**
     * @param string $shippingtypeExtraLabel
     */
    public function setShippingtypeExtraLabel($shippingtypeExtraLabel)
    {
        $this->shippingtypeExtraLabel = $shippingtypeExtraLabel;
    }

    /**
     * @return string
     */
    public function getFile1()
    {
        return $this->file1;
    }

    /**
     * @param string $file1
     */
    public function setFile1($file1)
    {
        $this->file1 = $file1;
    }

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
    }

    /**
     * @return Install
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param Install $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return mixed
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @param mixed $positions
     */
    public function setPositions($positions)
    {
        $this->positions = $positions;
    }

    /**
     * @return boolean
     */
    public function isDeliverySame()
    {
        return $this->deliverySame;
    }

    /**
     * @param boolean $deliverySame
     */
    public function setDeliverySame($deliverySame)
    {
        $this->deliverySame = $deliverySame;
    }

    /**
     * @return boolean
     */
    public function isSenderSame()
    {
        return $this->senderSame;
    }

    /**
     * @param boolean $senderSame
     */
    public function setSenderSame($senderSame)
    {
        $this->senderSame = $senderSame;
    }


    public function getWeight()
    {
        $weight = 0;
/** @var Orderpos $position */
        foreach ($this->positions as $position) {
            $weight = $weight + $position->getWeight();
        }

        return $weight;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        if($this->type == null || $this->type == "") return 1;
        return (int)$this->type;
    }

    /**
     * @param int $type
     */
    public function setType(int $type): void
    {
        $this->type = $type;
    }
}
