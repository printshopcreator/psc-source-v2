<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Cms
 *
 * @ORM\Table(name="credit_system")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\VoucherRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Voucher
{
    /**
     * Created
     *
     * @var Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $createdAt;
/**
     * Updated
     *
     * @var Datetime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updatedAt;
/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;
/**
     * Titel
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;
/**
     * Shop zu welcher die News gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="PSC\Shop\EntityBundle\Entity\Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * enable
     *
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean")
     */
    protected $enable;
/**
     * percent
     *
     * @var boolean
     *
     * @ORM\Column(name="percent", type="boolean")
     */
    protected $percent;
/**
     * more
     *
     * @var boolean
     *
     * @ORM\Column(name="more", type="boolean")
     */
    protected $more;
/**
     * from
     *
     * @var date
     *
     * @ORM\Column(name="f", type="date")
     */
    protected $fromDate;
/**
     * from
     *
     * @var date
     *
     * @ORM\Column(name="t", type="date")
     */
    protected $toDate;
/**
     * count
     *
     * @var int
     *
     * @ORM\Column(name="count", type="integer")
     */
    protected $count;
/**
     * mode
     *
     * @var int
     *
     * @ORM\Column(name="mode", type="integer")
     */
    protected $mode;
/**
     * value
     *
     * @var float
     *
     * @ORM\Column(name="wert", type="float")
     */
    protected $value;
/**
     * Code
     *
     * @var string
     *
     * @ORM\Column(name="pre_code", type="string", length=255, nullable=true)
     */
    protected $code;
/**
     * Articlegroup
     *
     * @var string
     *
     * @ORM\Column(name="articlegroup_id", type="string", length=255, nullable=true)
     */
    protected $productGroupIds;
/**
     * ProductIds
     *
     * @var string
     *
     * @ORM\Column(name="product_id", type="string", length=255, nullable=true)
     */
    protected $productIds;
/**
     * VoucherItems
     *
     * @var int
     *
     * @ORM\OneToMany(targetEntity="VoucherItem", mappedBy="voucher")
     */
    protected $voucherItems;
/**
     * Voucher constructor.
     */
    public function __construct()
    {
        $this->title = "Neuer Gutschein";
        $this->fromDate = new \DateTime();
        $this->toDate = new \DateTime();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }


    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return bool
     */
    public function isPercent()
    {
        return $this->percent;
    }

    /**
     * @param bool $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return bool
     */
    public function isMore()
    {
        return $this->more;
    }

    /**
     * @param bool $more
     */
    public function setMore($more)
    {
        $this->more = $more;
    }

    /**
     * @return date
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * @param date $fromDate
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;
    }

    /**
     * @return date
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * @param date $toDate
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getProductGroupIds()
    {
        return $this->productGroupIds;
    }

    /**
     * @param string $productGroupIds
     */
    public function setProductGroupIds($productGroupIds)
    {
        $this->productGroupIds = $productGroupIds;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getProductIds()
    {
        return $this->productIds;
    }

    /**
     * @param string $productIds
     */
    public function setProductIds($productIds)
    {
        $this->productIds = $productIds;
    }

    /**
     * @return Datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param Datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param Datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getVoucherItems()
    {
        return $this->voucherItems;
    }

    /**
     * @param int $voucherItems
     */
    public function setVoucherItems($voucherItems)
    {
        $this->voucherItems = $voucherItems;
    }

    /**
     * @return int
     */
    public function getMode(): int
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     */
    public function setMode(int $mode): void
    {
        $this->mode = $mode;
    }
}
