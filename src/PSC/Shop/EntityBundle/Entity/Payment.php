<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use PSC\Shop\EntityBundle\Entity\Generated\Paymenttype;

/**
 * Contact
 *
 * @ORM\Table(name="paymenttype")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\PaymentRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Payment extends Paymenttype
{
    /** @var array */
    protected $shippings = [];
/** @var string */
    protected $paymentGateway;
/** @var array */
    protected $paymentGatewaySettings = [];
/** @var array */
    protected $pluginSettings = [];
    public function __get($name)
    {
        if (!isset($this->pluginSettings[$name])) {
            return null;
        }

        return $this->pluginSettings[$name];
    }

    public function __set($name, $value)
    {
        $this->pluginSettings[$name] = $value;
    }
    /**
     * Id der Versandart
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Titel der Versandart
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;
/**
     * TrustedShop Name
     *
     * @var string
     *
     * @ORM\Column(name="trustedshop_name", type="string", length=255)
     */
    protected $trustedShopName;
/**
     * Description der Versandart
     *
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     */
    protected $description;
/**
     * Einmalige Kosten der Zahlart
     *
     * @var float
     *
     * @ORM\Column(name="wert", type="float")
     */
    protected $price;
/**
     * Enable
     *
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean")
     */
    protected $enable;
/**
     * Privat
     *
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean")
     */
    protected $private;
/**
     * Prozentual
     *
     * @var boolean
     *
     * @ORM\Column(name="prozent", type="boolean")
     */
    protected $percent;
/**
     * Position der Versandart
     *
     * @var float
     *
     * @ORM\Column(name="pos", type="integer")
     */
    protected $pos;
/**
     * Shop zu welcher die Versandart gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Install
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Install")
     * @ORM\JoinColumn(name="install_id", referencedColumnName="id")
     */
    protected $install;
/**
     * MWert
     *
     * @var integer
     *
     * @ORM\Column(name="mwert", type="decimal")
     */
    protected $taxClass;
/**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * Gibt den Titel zurück
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Setzt den Titel
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gibt den Verknüpften Shop zurück
     *
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Setzt den verknüpften Shop
     *
     * @param Shop $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * Gibt die Postion zurück
     *
     * @return float
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Setzt die Position der Versandart
     *
     * @param float $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    public function asArray()
    {
        return array(
            'title' => $this->getTitle(),
            'pos' => $this->getPos(),
            'price' => $this->getPrice(),
            'id' => $this->getUID()
        );
    }

    /**
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * @param boolean $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return boolean
     */
    public function isProzentual()
    {
        return $this->prozentual;
    }

    /**
     * @param boolean $prozentual
     */
    public function setProzentual($prozentual)
    {
        $this->prozentual = $prozentual;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param int $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return string
     */
    public function getTrustedShopName()
    {
        return $this->trustedShopName;
    }

    /**
     * @param string $trustedShopName
     */
    public function setTrustedShopName($trustedShopName)
    {
        $this->trustedShopName = $trustedShopName;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return bool
     */
    public function isPercent()
    {
        return $this->percent;
    }

    /**
     * @param bool $percent
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return array
     */
    public function getShippings()
    {
        return $this->shippings;
    }

    /**
     * @param array $shippings
     */
    public function setShippings($shippings)
    {
        $this->shippings = $shippings;
    }

    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @return string
     */
    public function getPaymentGateway(): string
    {
        return $this->paymentGateway;
    }

    /**
     * @param string $paymentGateway
     */
    public function setPaymentGateway(string $paymentGateway): void
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @return array
     */
    public function getPaymentGatewaySettings(): array
    {
        return $this->paymentGatewaySettings;
    }

    /**
     * @param array $paymentGatewaySettings
     */
    public function setPaymentGatewaySettings(array $paymentGatewaySettings): void
    {
        $this->paymentGatewaySettings = $paymentGatewaySettings;
    }

    /**
     * @return int
     */
    public function getTaxClass(): int
    {
        return (int)$this->taxClass;
    }

    /**
     * @param int $taxClass
     */
    public function setTaxClass(int $taxClass): void
    {
        $this->taxClass = $taxClass;
    }
}
