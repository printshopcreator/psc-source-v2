<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Install
 *
 * @ORM\Table(name="install")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\InstallRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Install
{
    /**
     * Id des Shops
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * colordb
     *
     * @var string
     *
     * @ORM\Column(name="colordb", type="string")
     */
    protected $colordb;
/**
    * @var string
    *
    * @ORM\Column(name="containerdb", type="text", nullable=true)
    */
    protected $paperContainer;
/**
    * @var string
    *
    * @ORM\Column(name="calc_template", type="text", nullable=true)
    */
    protected $calcTemplates;
/**
     * @var string
     *
     * @ORM\Column(name="calc_template_test", type="text", nullable=true)
     */
    protected $calcTemplatesTest;
/**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getColordb()
    {
        if ($this->colordb == "") {
            return '<root>
                <item id="general">
                    <color id="#ffffff" label="Weiß"/>
                    <color id="#ffd500" label="Sonnen Gelb"/>
                    <color id="#e7a020" label="Orange"/>
                    <color id="#42a62a" label="Grün"/>
                    <color id="#cfe0a4" label="Advocado Grün"/>
                    <color id="#89ba17" label="Gras Grün"/>
                    <color id="#2fcbb3" label="Petrolium Grün"/>
                    <color id="#93d5f2" label="Hell Blau"/>
                    <color id="#009ee0" label="Blau"/>
                    <color id="#0f64aa" label="Dunkel Blau"/>
                    <color id="#e2007a" label="Pink"/>
                    <color id="#e2001a" label="Rot"/>
                    <color id="#8a151a" label="Dunkel Rot"/>
                    <color id="#c50347" label="Brombeere"/>
                    <color id="#795599" label="Veilchen Lila"/>
                    <color id="#63256b" label="Lila"/>
                    <color id="#d5e47e" label="Beige"/>
                    <color id="#f2cb85" label="Hell Orange"/>
                    <color id="#663300" label="Braun"/>
                    <color id="#d5e47e" label="Schwarz Braun"/>
                    <color id="#000000" label="Schwarz"/>
                    <color id="#333333" label="Dunkel Grau"/>
                    <color id="#ababab" label="Grau"/>
                    <color id="#f2f2f2" label="Hell Grau"/>
                </item>
            </root>';
        }
        return $this->colordb;
    }

    /**
     * @param string $colordb
     */
    public function setColordb($colordb)
    {
        $this->colordb = $colordb;
    }

    /**
     * @return string
     */
    public function getPaperContainer()
    {
        if ($this->paperContainer) {
            return $this->paperContainer;
        }
        return '<root></root>';
    }

    /**
     * @param string $paperContainer
     */
    public function setPaperContainer($paperContainer)
    {
        $this->paperContainer = $paperContainer;
    }

    /**
     * @return string
     */
    public function getCalcTemplates()
    {
        return $this->calcTemplates;
    }

    /**
     * @param string $calcTemplates
     */
    public function setCalcTemplates($calcTemplates)
    {
        $this->calcTemplates = $calcTemplates;
    }

    /**
     * @return string
     */
    public function getCalcTemplatesTest(): string
    {
        return (string)$this->calcTemplatesTest;
    }

    /**
     * @param string $calcTemplatesTest
     */
    public function setCalcTemplatesTest(string $calcTemplatesTest): void
    {
        $this->calcTemplatesTest = $calcTemplatesTest;
    }
}
