<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Shop
 *
 * @ORM\Table(name="article")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ProductRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Product
{
    /** @var  \DateTime */
    protected $ablaufDatum;
    protected $ansprechPartner;
    protected $printPartnerEmail;
    protected $zusatzAbmessung;
    protected $zusatzDesigner;
    protected $zusatzShipping;
    protected $custom1;
    protected $custom2;
    protected $custom3;
    protected $custom4;
    protected $custom5;
    protected $custom6;
    protected $asRequest;
    protected $custom7;
    protected $custom8;
    protected $custom9;
    protected $custom10;
    protected $custom11;
    protected $custom12;
    protected $extraSettings;
    protected $availability;
    protected $weight;
    protected $text;
    /** @var  integer */
    protected $collectingOrdersAccount;
    /** @var  integer */
    protected $baseUnit = 0;
    /** @var  integer */
    protected $salesUnit = 0;
    /** @var  integer */
    protected $packagingUnit;
    /** @var  integer */
    protected $collectingOrdersAccountFilter;
    /** @var  boolean */
    protected $collectingOrdersChangePicture;
    /** @var  boolean */
    protected $collectingOrdersCopy;
    /** @var  boolean */
    protected $collectingOrdersNewContact;
    /** @var  boolean */
    protected $collectingOrdersInviteContact;
    /** @var boolean */
    protected $collectingOrdersIntegrated;

    /** @var bool */
    protected $uploadFromLatestOrder;
    /** @var integer */
    protected $uploadFromLatestOrderInitalStatus;

    /** @var bool */
    protected $uploadProvided;
    /** @var string */
    protected $uploadProvidedFile;
    /** @var integer */
    protected $uploadProvidedInitalStatus;

    /** @var  boolean */
    protected $hintEnable;
    /** @var  boolean */
    protected $customTabEnable;

    /** @var array */
    protected $pluginSettings = [];

    public function __get($name)
    {
        if (!isset($this->pluginSettings[$name])) {
            return null;
        }

        return $this->pluginSettings[$name];
    }

    public function __set($name, $value)
    {
        $this->pluginSettings[$name] = $value;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Id des Product
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;

    /**
     * Install
     *
     * @var int
     *
     * @ORM\Column(name="install_id", type="integer")
     */
    protected $install;

    /**
     * Pos
     *
     * @var string
     *
     * @ORM\Column(name="pos", type="integer")
     */
    protected $pos;

    /**
     * Titel des Product
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    protected $title;

    /**
     * Art des Product
     *
     * @var string
     *
     * @ORM\Column(name="text_art", type="string", length=255)
     */
    protected $textArt;

    /**
     * Format des Product
     *
     * @var string
     *
     * @ORM\Column(name="text_format", type="string", length=255)
     */
    protected $textFormat;

    /**
     * Sprache des Product
     *
     * @var string
     *
     * @ORM\Column(name="a9_left1", type="string", length=255)
     */
    protected $language;

    /**
     * from price
     *
     * @var string
     *
     * @ORM\Column(name="a4_abpreis", type="string", length=255)
     */
    protected $fromPrice;

    /**
     * Info des Product
     *
     * @var string
     *
     * @ORM\Column(name="info", type="text", nullable=true)
     */
    protected $description;

    /**
     * Einleitung des Product
     *
     * @var string
     *
     * @ORM\Column(name="einleitung", type="text", nullable=true)
     */
    protected $einleitung;

    /**
     * Set Config
     *
     * @var string
     *
     * @ORM\Column(name="set_config", type="text", nullable=true)
     */
    protected $setConfig;

    /**
     * Langdata des Product
     *
     * @var string
     *
     * @ORM\Column(name="lang_data", type="text", nullable=true)
     */
    protected $langData;

    /**
     * KalkulationsXML
     *
     * @var string
     *
     * @ORM\Column(name="a1_xml", type="text", nullable=true)
     */
    protected $calcXml;

    /**
     * originalProduct
     *
     * @var string
     *
     * @ORM\Column(name="a6_org_article", type="integer", nullable=true)
     */
    protected $originalProduct;

    /**
     * templatePrintDirectory
     *
     * @var string
     *
     * @ORM\Column(name="a6_directory", type="string", nullable=true)
     */
    protected $templatePrintDirectory;

    /**
     * UUID des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255)
     */
    protected $uuid;

    /**
     * ProduktNr Intern des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="article_nr_intern", type="string", length=255)
     */
    protected $nrIntern;

    /**
     * Url
     *
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    protected $url;

    /**
     * Kostenstelle
     *
     * @var string
     *
     * @ORM\Column(name="kostenstelle", type="string", length=255)
     */
    protected $kostenstelle;

    /**
     * Staffelpreis
     *
     * @var string
     *
     * @ORM\Column(name="a4_auflagen", type="string", length=255)
     */
    protected $scaledPrice;

    /**
     * Youtube ID
     *
     * @var string
     *
     * @ORM\Column(name="a9_back1", type="string", length=255)
     */
    protected $videoId;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_keywords", type="string", length=255, nullable=true)
     */
    protected $metaKeywords;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_description", type="string", length=255, nullable=true)
     */
    protected $metaDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_author", type="string", length=255, nullable=true)
     */
    protected $metaAuthor;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_custom_title", type="string", length=255, nullable=true)
     */
    protected $metaCustomTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_og_title", type="string", length=255, nullable=true)
     */
    protected $metaOgTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_og_type", type="string", length=255, nullable=true)
     */
    protected $metaOgType;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_og_url", type="string", length=255, nullable=true)
     */
    protected $metaOgUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_og_image", type="string", length=255, nullable=true)
     */
    protected $metaOgImage;

    /**
     * @var string
     *
     * @ORM\Column(name="meta_og_description", type="string", length=255, nullable=true)
     */
    protected $metaOgDescription;

    /**
     * ProduktNr Extern des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="article_nr_extern", type="string", length=255)
     */
    protected $nrExtern;

    /**
     * Bild 1 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=true)
     */
    protected $image1;

    /**
     * Bild 2 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file1", type="string", length=255, nullable=true)
     */
    protected $image2;

    /**
     * Bild 3 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file2", type="string", length=255, nullable=true)
     */
    protected $image3;

    /**
     * Bild 4 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file3", type="string", length=255, nullable=true)
     */
    protected $image4;

    /**
     * Bild 5 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file4", type="string", length=255, nullable=true)
     */
    protected $image5;

    /**
     * Bild 6 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file5", type="string", length=255, nullable=true)
     */
    protected $image6;

    /**
     * Bild 6 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file6", type="string", length=255, nullable=true)
     */
    protected $image7;

    /**
     * Bild 7 des Produkts
     *
     * @var string
     *
     * @ORM\Column(name="file7", type="string", length=255, nullable=true)
     */
    protected $image8;

    /**
     * Steplayouter XML
     *
     * @var string
     *
     * @ORM\Column(name="upload_steplayouter_xml", type="string", nullable=true)
     */
    protected $steplayouterXml;

    /**
     * Custom Tab Title
     *
     * @var string
     *
     * @ORM\Column(name="custom_1_title", type="string", length=255, nullable=true)
     */
    protected $customTabTitle;

    /**
     * Custom Tab Text
     *
     * @var string
     *
     * @ORM\Column(name="custom_1_text", type="string", nullable=true)
     */
    protected $customTabText;

    /**
     * Steplayouter Data
     *
     * @var string
     *
     * @ORM\Column(name="upload_steplayouter_data", type="string", nullable=true)
     */
    protected $steplayouterData;

    /**
     * Aktives Produkt
     *
     * @var boolean
     *
     * @ORM\Column(name="enable", type="boolean")
     */
    protected $enable;

    /**
     * Nicht im Shop anzeigen
     *
     * @var boolean
     *
     * @ORM\Column(name="display_not_in_overview", type="boolean")
     */
    protected $displayNotInOverview;

    /**
     * Kann auch gekauft werden wenn Preis 0€
     *
     * @var boolean
     *
     * @ORM\Column(name="can_buy_with_no_price", type="boolean")
     */
    protected $canBuyWithNoPrice;

    /**
     * Kein Preis anzeigen
     *
     * @var boolean
     *
     * @ORM\Column(name="display_no_price", type="boolean")
     */
    protected $displayNoPrice;

    /**
     * Privates Product
     *
     * @var boolean
     *
     * @ORM\Column(name="private", type="boolean")
     */
    protected $private;

    /**
     * Nicht bearbeiten Product
     *
     * @var boolean
     *
     * @ORM\Column(name="not_edit", type="boolean")
     */
    protected $notEdit;

    /**
     * Angebot
     *
     * @var boolean
     *
     * @ORM\Column(name="as_offer", type="boolean")
     */
    protected $asOffer;

    /**
     * Privates Product
     *
     * @var boolean
     *
     * @ORM\Column(name="not_buy", type="boolean")
     */
    protected $notBuy;

    /**
     * Packageformat
     *
     * @var string
     *
     * @ORM\Column(name="verpackungseinheit", type="string", length=255)
     */
    protected $packageFormat;

    /**
     * Vorlage 1
     *
     * @var string
     *
     * @ORM\Column(name="vorlage_file", type="string", length=255, nullable=true)
     */
    protected $hintFile1;

    /**
     * Vorlage 2
     *
     * @var string
     *
     * @ORM\Column(name="vorlage_file1", type="string", length=255, nullable=true)
     */
    protected $hintFile2;

    /**
     * Vorlage 3
     *
     * @var string
     *
     * @ORM\Column(name="vorlage_file2", type="string", length=255, nullable=true)
     */
    protected $hintFile3;

    /**
     * Vorlage 4
     *
     * @var string
     *
     * @ORM\Column(name="vorlage_file3", type="string", length=255, nullable=true)
     */
    protected $hintFile4;

    /**
     * Vorlage Info
     *
     * @var string
     *
     * @ORM\Column(name="vorlage_info", type="text", nullable=true)
     */
    protected $hintText;

    /**
     * Shop zu welcher das Product gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;

    /**
     * Freigabe Account
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="confirmaccount_id", referencedColumnName="id", nullable=true)
     */
    protected $confirmAccount;

    /**
     * Freigabebedürftig
     *
     * @var boolean
     *
     * @ORM\Column(name="confirm", type="boolean")
     */
    protected $confirm;

    /**
     * Ein Kontakt reicht zum Freigeben
     *
     * @var boolean
     *
     * @ORM\Column(name="confirmOne", type="boolean")
     */
    protected $confirmOne;

    /**
     * Lager aktiviert Produkt
     *
     * @var boolean
     *
     * @ORM\Column(name="stock", type="boolean")
     */
    protected $isStock;

    /**
     * Typ Produkt
     *
     * @var integer
     *
     * @ORM\Column(name="typ", type="integer")
     */
    protected $type;

    /**
     * Mwert
     *
     * @var integer
     *
     * @ORM\Column(name="mwert", type="decimal")
     */
    protected $mwert;

    /**
     * Buyed Produkt
     *
     * @var integer
     *
     * @ORM\Column(name="used", type="integer")
     */
    protected $buyed;

    /**
     * Lagerbestand Produkt
     *
     * @var integer
     *
     * @ORM\Column(name="stock_count", type="integer")
     */
    protected $stockCount;

    /**
     * Stockplace
     *
     * @var string
     *
     * @ORM\Column(name="stock_place", type="string", length=255)
     */
    protected $stockPlace;

    /**
     * Min Lagerbestand Produkt
     *
     * @var integer
     *
     * @ORM\Column(name="stock_count_min", type="integer")
     */
    protected $stockMinCount;

    /**
     * Maximal Anzahl zu kaufen
     *
     * @var integer
     *
     * @ORM\Column(name="stock_max_buy", type="integer")
     */
    protected $stockMaxBuy;

    /**
     * Preis
     *
     * @var float
     *
     * @ORM\Column(name="preis", type="float")
     */
    protected $price;

    /**
     * Upload Steplayouter2
     *
     * @var boolean
     *
     * @ORM\Column(name="upload_steplayouter2", type="boolean")
     */
    protected $uploadSteplayouter2;

    /**
     * Created
     *
     * @var Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $createdAt;

    /**
     * Updated
     *
     * @var Datetime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updatedAt;

    /**
     * @ORM\ManyToMany(targetEntity="Productgroup", inversedBy="products")
     * @ORM\JoinTable(name="article_group_article",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="articlegroup_id", referencedColumnName="id")}
     *      )
     **/
    protected $product_groups;

    /**
     * @ORM\ManyToMany(targetEntity="Contact")
     * @ORM\JoinTable(name="article_confirm_contact",
     *      joinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")}
     *      )
     **/
    protected $confirm_contacts;

    /**
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="products")
     * @ORM\JoinTable(name="base_article_releated",
     *      joinColumns={@ORM\JoinColumn(name="article1", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="article2", referencedColumnName="id")}
     *      )
     **/
    protected $releatedProducts;

    /**
     * initial status
     *
     * @var integer
     *
     * @ORM\Column(name="init_status", type="integer")
     */
    protected $initialStatus;

    /**
     * Upload Product
     *
     * @var boolean
     *
     * @ORM\Column(name="upload_article", type="boolean")
     */
    protected $uploadProduct;

    /**
     * Upload Product Status
     *
     * @var integer
     *
     * @ORM\Column(name="upload_article_status", type="integer")
     */
    protected $uploadProductInitalStatus;

    /**
     * Upload Post
     *
     * @var boolean
     *
     * @ORM\Column(name="upload_post", type="boolean")
     */
    protected $uploadPost;

    /**
     * Upload Post Status
     *
     * @var integer
     *
     * @ORM\Column(name="upload_post_status", type="integer")
     */
    protected $uploadPostInitalStatus;

    /**
     * Upload Email
     *
     * @var boolean
     *
     * @ORM\Column(name="upload_email", type="boolean")
     */
    protected $uploadEmail;

    /**
     * Upload Email Status
     *
     * @var integer
     *
     * @ORM\Column(name="upload_email_status", type="integer")
     */
    protected $uploadEmailInitalStatus;

    /**
     * Upload Center
     *
     * @var boolean
     *
     * @ORM\Column(name="upload_center", type="boolean")
     */
    protected $uploadCenter;

    /**
     * Upload Center Status
     *
     * @var integer
     *
     * @ORM\Column(name="upload_center_status", type="integer")
     */
    protected $uploadCenterInitalStatus;

    /**
     * Upload Steplayouter Status
     *
     * @var integer
     *
     * @ORM\Column(name="upload_steplayouter2_status", type="integer")
     */
    protected $uploadSteplayouter2InitalStatus;

    /**
     * Upload CollectingOrders
     *
     * @var boolean
     *
     * @ORM\Column(name="upload_collecting_orders", type="boolean")
     */
    protected $uploadCollectingOrders;

    /**
     * Upload CollectingOrders Status
     *
     * @var integer
     *
     * @ORM\Column(name="upload_collecting_orders_status", type="integer")
     */
    protected $uploadCollectingOrdersInitalStatus;

    /**
     * Upload Templateprint
     *
     * @var boolean
     *
     * @ORM\Column(name="upload_templateprint", type="boolean")
     */
    protected $uploadTemplateprint;

    /**
     * Upload Templateprint Status
     *
     * @var integer
     *
     * @ORM\Column(name="upload_templateprint_status", type="integer")
     */
    protected $uploadTemplateprintInitalStatus;

    /**
     * Konstruktor
     */
    public function __construct()
    {
        $this->enable  = false;
        $this->notBuy  = false;
        $this->private = false;
        $this->displayNotInOverview = false;
        $this->uuid = UUID::uuid4();
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
        $this->product_groups = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setOriginalProduct(0);
        $this->setIsStock(false);
        $this->setStockCount(0);
        $this->setStockMinCount(0);
        $this->setStockMaxBuy(0);
        $this->initialStatus = 10;
    }

    /**
     *excel
     */
    public function getExportArray()
    {

        return array(
            'id' => (string)$this->getId(),
            'uid' => (string)$this->getUID(),
            'uuid' => (string)$this->getUUID(),
            'title' => (string)$this->getTitle(),
            'textArt' => (string)$this->getTextArt(),
            'textFormat' => (string)$this->getTextFormat(),
            'nrExtern' => (string)$this->getNrExtern(),
            'nrIntern' => (string)$this->getNrIntern(),
            'ablaufDatum' => ($this->ablaufDatum == null) ? '' : $this->ablaufDatum->format("d.m.Y"),
            'ansprechPartner' => (string)$this->ansprechPartner,
            'preis' => (string)$this->getPrice(),
            'enable' => (string)$this->isEnable(),
            'shopId' => (string)$this->getShop()->getUid(),
            'productType' => (string)$this->getType(),
            'isStock' => $this->isStock() ? "1" : "0",
            'stockCount' => (string)$this->getStockCount(),
            'stockMinCount' => (string)$this->getStockMinCount(),
            'stockPlace' => (string)$this->getStockPlace(),
            'stockMaxBuy' => (string)$this->getStockMaxBuy(),
            'buyed' => (string)$this->getBuyed(),
            'printPartnerEmail' => (string)$this->getPrintPartnerEmail(),
            'zusatzAbmessung' => (string)$this->getZusatzAbmessung(),
            'zusatzDesigner' => (string)$this->getZusatzDesigner(),
            'zusatzShipping' => (string)$this->getZusatzShipping(),
            'custom1' => (string)$this->getCustom1(),
            'custom2' => (string)$this->getCustom2(),
            'custom3' => (string)$this->getCustom3(),
            'custom4' => (string)$this->getCustom4(),
            'custom5' => (string)$this->getCustom5(),
            'custom6' => (string)$this->getCustom6(),
            'custom7' => (string)$this->getCustom7(),
            'custom8' => (string)$this->getCustom8(),
            'custom9' => (string)$this->getCustom9(),
            'custom10' => (string)$this->getCustom10(),
            'custom11' => (string)$this->getCustom11(),
            'custom12' => (string)$this->getCustom12(),


        );
    }

    /**
     * Gibt die Id des Produktes zurück
     *
     * @return int
     */
    public function getId()
    {
        return $this->uid;
    }

    /**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * ToString Methode
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Get Title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set Title
     *
     * @param string $title
     *
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gibt den Shop zurück
     *
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * Setzt den Shop
     *
     * @param Shop $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * Gibt die UUID zurück
     *
     * @return string
     */
    public function getUUID()
    {
        return $this->uuid;
    }

    /**
     * Setzt die UUID
     *
     * @param string $uuid
     */
    public function setUUID($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * Darf das Produkt verkauft werden?
     *
     * @return boolean
     */
    public function isNotBuy()
    {
        return $this->notBuy;
    }

    /**
     * Setzt ob das Produkt verkauft werden darf
     *
     * @param boolean $notBuy
     */
    public function setNotBuy($notBuy)
    {
        $this->notBuy = $notBuy;
    }

    /**
     * Ist das Produkt privatisiert?
     *
     * @return boolean
     */
    public function isPrivate()
    {
        return $this->private;
    }

    /**
     * Setzt ob das Produkt privat ist
     *
     * @param boolean $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * Gibt die Beschreibung
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setzt die Beschreibung
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Ist es ein Lagerartikel?
     *
     * @return boolean
     */
    public function isStock()
    {
        return $this->isStock;
    }

    /**
     * Setzt ob es ein Lagerartikel ist
     *
     * @param boolean $isStock
     */
    public function setIsStock($isStock)
    {
        $this->isStock = $isStock;
    }

    /**
     * Gibt die Stückzahl zurück
     *
     * @return int
     */
    public function getStockCount()
    {
        return $this->stockCount;
    }

    /**
     * Setzt die aktuelle auf Lager Anzahl
     *
     * @param int $stockCount
     */
    public function setStockCount($stockCount)
    {
        $this->stockCount = $stockCount;
    }

    /**
     * Gibt die minimale Lageranzahl
     *
     * @return int
     */
    public function getStockMinCount()
    {
        return $this->stockMinCount;
    }

    /**
     * Setzt die minimale Lageranzahl
     *
     * @param int $stockMinCount
     */
    public function setStockMinCount($stockMinCount)
    {
        $this->stockMinCount = $stockMinCount;
    }

    /**
     * @return mixed
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * @param mixed $groups
     */
    public function setGroups($groups)
    {
        $this->groups = $groups;
    }

    /**
     * @return string
     */
    public function getNrExtern()
    {
        return $this->nrExtern;
    }

    /**
     * @param string $nrExtern
     */
    public function setNrExtern($nrExtern)
    {
        $this->nrExtern = $nrExtern;
    }

    /**
     * @return string
     */
    public function getNrIntern()
    {
        return $this->nrIntern;
    }

    /**
     * @param string $nrIntern
     */
    public function setNrIntern($nrIntern)
    {
        $this->nrIntern = $nrIntern;
    }

    /**
     * @return string
     */
    public function getSteplayouterXml()
    {
        return $this->steplayouterXml;
    }

    /**
     * @param string $steplayouterXml
     */
    public function setSteplayouterXml($steplayouterXml)
    {
        $this->steplayouterXml = $steplayouterXml;
    }

    /**
     * @return string
     */
    public function getSteplayouterData()
    {
        return json_decode($this->steplayouterData, true);
    }

    /**
     * @param string $steplayouterData
     */
    public function setSteplayouterData($steplayouterData)
    {
        $this->steplayouterData = json_encode($steplayouterData);
    }

    /**
     * @return Datetime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param Datetime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return Datetime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param Datetime $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    public function hasCalcXml()
    {
        return ($this->calcXml != "" && strpos($this->calcXml, "artikel") !== false);
    }

    /**
     * @return string
     */
    public function getCalcXml()
    {
        return $this->calcXml;
    }

    /**
     * @param string $calcXml
     */
    public function setCalcXml($calcXml)
    {
        $this->calcXml = $calcXml;
    }

    /**
     * @return mixed
     */
    public function getProductGroups()
    {
        return $this->product_groups;
    }

    /**
     * @param mixed $product_groups
     */
    public function setProductGroups($product_groups)
    {
        $this->product_groups = $product_groups;
    }

    /**
     * @return string
     */
    public function getLangData()
    {
        if ($this->langData == "") {
            return array();
        }
        return json_decode($this->langData, true);
    }

    /**
     * @param array $langData
     */
    public function setLangData($langData)
    {
        if (!is_array($langData)) {
            $langData = array();
        }
        $this->langData = json_encode($langData);
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getPackageFormat()
    {
        if ($this->packageFormat === null) {
            return "";
        }
        return $this->packageFormat;
    }

    /**
     * @param string $packageFormat
     */
    public function setPackageFormat($packageFormat)
    {
        $this->packageFormat = $packageFormat;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return boolean
     */
    public function isUploadSteplayouter2()
    {
        return $this->uploadSteplayouter2;
    }

    /**
     * @param boolean $uploadSteplayouter2
     */
    public function setUploadSteplayouter2($uploadSteplayouter2)
    {
        $this->uploadSteplayouter2 = $uploadSteplayouter2;
    }

    /**
     * @return string
     */
    public function getTextArt()
    {
        return $this->textArt;
    }

    /**
     * @param string $textArt
     */
    public function setTextArt($textArt)
    {
        $this->textArt = $textArt;
    }

    /**
     * @return mixed
     */
    public function getAblaufDatum()
    {
        return $this->ablaufDatum;
    }

    /**
     * @param mixed $ablaufDatum
     */
    public function setAblaufDatum($ablaufDatum)
    {
        $this->ablaufDatum = $ablaufDatum;
    }

    /**
     * @return mixed
     */
    public function getAnsprechPartner()
    {
        return $this->ansprechPartner;
    }

    /**
     * @param mixed $ansprechPartner
     */
    public function setAnsprechPartner($ansprechPartner)
    {
        $this->ansprechPartner = $ansprechPartner;
    }

    /**
     * @return string
     */
    public function getStockPlace()
    {
        return $this->stockPlace;
    }

    /**
     * @param string $stockPlace
     */
    public function setStockPlace($stockPlace)
    {
        $this->stockPlace = $stockPlace;
    }

    /**
     * @return string
     */
    public function getTextFormat()
    {
        return $this->textFormat;
    }

    /**
     * @param string $textFormat
     */
    public function setTextFormat($textFormat)
    {
        $this->textFormat = $textFormat;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getBuyed()
    {
        return $this->buyed;
    }

    /**
     * @param int $buyed
     */
    public function setBuyed($buyed)
    {
        $this->buyed = $buyed;
    }

    /**
     * @return mixed
     */
    public function getPrintPartnerEmail()
    {
        return $this->printPartnerEmail;
    }

    /**
     * @param mixed $printPartnerEmail
     */
    public function setPrintPartnerEmail($printPartnerEmail)
    {
        $this->printPartnerEmail = $printPartnerEmail;
    }

    /**
     * @return string
     */
    public function getOriginalProduct()
    {
        return $this->originalProduct;
    }

    /**
     * @param string $originalProduct
     */
    public function setOriginalProduct($originalProduct)
    {
        $this->originalProduct = $originalProduct;
    }

    /**
     * @return int
     */
    public function getMwert()
    {
        return $this->mwert;
    }

    /**
     * @param int $mwert
     */
    public function setMwert($mwert)
    {
        $this->mwert = $mwert;
    }

    /**
     * @return mixed
     */
    public function getZusatzDesigner()
    {
        return $this->zusatzDesigner;
    }

    /**
     * @param mixed $zusatzDesigner
     */
    public function setZusatzDesigner($zusatzDesigner)
    {
        $this->zusatzDesigner = $zusatzDesigner;
    }

    /**
     * @return mixed
     */
    public function getZusatzAbmessung()
    {
        return $this->zusatzAbmessung;
    }

    /**
     * @param mixed $zusatzAbmessung
     */
    public function setZusatzAbmessung($zusatzAbmessung)
    {
        $this->zusatzAbmessung = $zusatzAbmessung;
    }

    /**
     * @return mixed
     */
    public function getZusatzShipping()
    {
        return $this->zusatzShipping;
    }

    /**
     * @param mixed $zusatzShipping
     */
    public function setZusatzShipping($zusatzShipping)
    {
        $this->zusatzShipping = $zusatzShipping;
    }

    /**
     * @return mixed
     */
    public function getCustom1()
    {
        return $this->custom1;
    }

    /**
     * @param mixed $custom1
     */
    public function setCustom1($custom1)
    {
        $this->custom1 = $custom1;
    }

    /**
     * @return mixed
     */
    public function getCustom2()
    {
        return $this->custom2;
    }

    /**
     * @param mixed $custom2
     */
    public function setCustom2($custom2)
    {
        $this->custom2 = $custom2;
    }

    /**
     * @return mixed
     */
    public function getCustom3()
    {
        return $this->custom3;
    }

    /**
     * @param mixed $custom3
     */
    public function setCustom3($custom3)
    {
        $this->custom3 = $custom3;
    }

    /**
     * @return mixed
     */
    public function getCustom4()
    {
        return $this->custom4;
    }

    /**
     * @param mixed $custom4
     */
    public function setCustom4($custom4)
    {
        $this->custom4 = $custom4;
    }

    /**
     * @return mixed
     */
    public function getCustom5()
    {
        return $this->custom5;
    }

    /**
     * @param mixed $custom5
     */
    public function setCustom5($custom5)
    {
        $this->custom5 = $custom5;
    }

    /**
     * @return mixed
     */
    public function getCustom6()
    {
        return $this->custom6;
    }

    /**
     * @param mixed $custom6
     */
    public function setCustom6($custom6)
    {
        $this->custom6 = $custom6;
    }

    /**
     * @return mixed
     */
    public function getCustom7()
    {
        return $this->custom7;
    }

    /**
     * @param mixed $custom7
     */
    public function setCustom7($custom7)
    {
        $this->custom7 = $custom7;
    }

    /**
     * @return mixed
     */
    public function getCustom8()
    {
        return $this->custom8;
    }

    /**
     * @param mixed $custom8
     */
    public function setCustom8($custom8)
    {
        $this->custom8 = $custom8;
    }

    /**
     * @return mixed
     */
    public function getCustom9()
    {
        return $this->custom9;
    }

    /**
     * @param mixed $custom9
     */
    public function setCustom9($custom9)
    {
        $this->custom9 = $custom9;
    }

    /**
     * @return mixed
     */
    public function getCustom10()
    {
        return $this->custom10;
    }

    /**
     * @param mixed $custom10
     */
    public function setCustom10($custom10)
    {
        $this->custom10 = $custom10;
    }

    /**
     * @return mixed
     */
    public function getCustom11()
    {
        return $this->custom11;
    }

    /**
     * @param mixed $custom11
     */
    public function setCustom11($custom11)
    {
        $this->custom11 = $custom11;
    }

    /**
     * @return mixed
     */
    public function getCustom12()
    {
        return $this->custom12;
    }

    /**
     * @param mixed $custom12
     */
    public function setCustom12($custom12)
    {
        $this->custom12 = $custom12;
    }

    /**
     * @return bool
     */
    public function isUploadPost()
    {
        return $this->uploadPost;
    }

    /**
     * @param bool $uploadPost
     */
    public function setUploadPost($uploadPost)
    {
        $this->uploadPost = $uploadPost;
    }

    /**
     * @return int
     */
    public function getUploadPostInitalStatus()
    {
        return $this->uploadPostInitalStatus;
    }

    /**
     * @param int $uploadPostInitalStatus
     */
    public function setUploadPostInitalStatus($uploadPostInitalStatus)
    {
        $this->uploadPostInitalStatus = $uploadPostInitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadEmail()
    {
        return $this->uploadEmail;
    }

    /**
     * @param bool $uploadEmail
     */
    public function setUploadEmail($uploadEmail)
    {
        $this->uploadEmail = $uploadEmail;
    }

    /**
     * @return int
     */
    public function getUploadEmailInitalStatus()
    {
        return $this->uploadEmailInitalStatus;
    }

    /**
     * @param int $uploadEmailInitalStatus
     */
    public function setUploadEmailInitalStatus($uploadEmailInitalStatus)
    {
        $this->uploadEmailInitalStatus = $uploadEmailInitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadCenter()
    {
        return $this->uploadCenter;
    }

    /**
     * @param bool $uploadCenter
     */
    public function setUploadCenter($uploadCenter)
    {
        $this->uploadCenter = $uploadCenter;
    }

    /**
     * @return int
     */
    public function getUploadSteplayouter2InitalStatus()
    {
        return $this->uploadSteplayouter2InitalStatus;
    }

    /**
     * @param int $uploadSteplayouter2InitalStatus
     */
    public function setUploadSteplayouter2InitalStatus($uploadSteplayouter2InitalStatus)
    {
        $this->uploadSteplayouter2InitalStatus = $uploadSteplayouter2InitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadCollectingOrders()
    {
        return $this->uploadCollectingOrders;
    }

    /**
     * @param bool $uploadCollectingOrders
     */
    public function setUploadCollectingOrders($uploadCollectingOrders)
    {
        $this->uploadCollectingOrders = $uploadCollectingOrders;
    }

    /**
     * @return int
     */
    public function getUploadCollectingOrdersInitalStatus()
    {
        return $this->uploadCollectingOrdersInitalStatus;
    }

    /**
     * @param int $uploadCollectingOrdersInitalStatus
     */
    public function setUploadCollectingOrdersInitalStatus($uploadCollectingOrdersInitalStatus)
    {
        $this->uploadCollectingOrdersInitalStatus = $uploadCollectingOrdersInitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadTemplateprint()
    {
        return $this->uploadTemplateprint;
    }

    /**
     * @param bool $uploadTemplateprint
     */
    public function setUploadTemplateprint($uploadTemplateprint)
    {
        $this->uploadTemplateprint = $uploadTemplateprint;
    }

    /**
     * @return int
     */
    public function getUploadTemplateprintInitalStatus()
    {
        return $this->uploadTemplateprintInitalStatus;
    }

    /**
     * @param int $uploadTemplateprintInitalStatus
     */
    public function setUploadTemplateprintInitalStatus($uploadTemplateprintInitalStatus)
    {
        $this->uploadTemplateprintInitalStatus = $uploadTemplateprintInitalStatus;
    }

    /**
     * @return int
     */
    public function getUploadCenterInitalStatus()
    {
        return $this->uploadCenterInitalStatus;
    }

    /**
     * @param int $uploadCenterInitalStatus
     */
    public function setUploadCenterInitalStatus($uploadCenterInitalStatus)
    {
        $this->uploadCenterInitalStatus = $uploadCenterInitalStatus;
    }

    /**
     * @return int
     */
    public function getCollectingOrdersAccount()
    {
        return $this->collectingOrdersAccount;
    }

    /**
     * @param int $collectingOrdersAccount
     */
    public function setCollectingOrdersAccount($collectingOrdersAccount)
    {
        $this->collectingOrdersAccount = $collectingOrdersAccount;
    }

    /**
     * @return string
     */
    public function getTemplatePrintDirectory()
    {
        return $this->templatePrintDirectory;
    }

    /**
     * @param string $templatePrintDirectory
     */
    public function setTemplatePrintDirectory($templatePrintDirectory)
    {
        $this->templatePrintDirectory = $templatePrintDirectory;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersChangePicture()
    {
        return $this->collectingOrdersChangePicture;
    }

    /**
     * @param bool $collectingOrdersChangePicture
     */
    public function setCollectingOrdersChangePicture($collectingOrdersChangePicture)
    {
        $this->collectingOrdersChangePicture = $collectingOrdersChangePicture;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersCopy()
    {
        return $this->collectingOrdersCopy;
    }

    /**
     * @param bool $collectingOrdersCopy
     */
    public function setCollectingOrdersCopy($collectingOrdersCopy)
    {
        $this->collectingOrdersCopy = $collectingOrdersCopy;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersNewContact()
    {
        return $this->collectingOrdersNewContact;
    }

    /**
     * @param bool $collectingOrdersNewContact
     */
    public function setCollectingOrdersNewContact($collectingOrdersNewContact)
    {
        $this->collectingOrdersNewContact = $collectingOrdersNewContact;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersInviteContact()
    {
        return $this->collectingOrdersInviteContact;
    }

    /**
     * @param bool $collectingOrdersInviteContact
     */
    public function setCollectingOrdersInviteContact($collectingOrdersInviteContact)
    {
        $this->collectingOrdersInviteContact = $collectingOrdersInviteContact;
    }

    /**
     * @return int
     */
    public function getCollectingOrdersAccountFilter()
    {
        return $this->collectingOrdersAccountFilter;
    }

    /**
     * @param int $collectingOrdersAccountFilter
     */
    public function setCollectingOrdersAccountFilter($collectingOrdersAccountFilter)
    {
        $this->collectingOrdersAccountFilter = $collectingOrdersAccountFilter;
    }

    /**
     * @return mixed
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param mixed $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @return bool
     */
    public function isEnable()
    {
        return $this->enable;
    }

    /**
     * @param bool $enable
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;
    }

    /**
     * @return string
     */
    public function getImage1()
    {
        return $this->image1;
    }

    /**
     * @param string $image1
     */
    public function setImage1($image1)
    {
        $this->image1 = $image1;
    }

    /**
     * @return string
     */
    public function getImage2()
    {
        return $this->image2;
    }

    /**
     * @param string $image2
     */
    public function setImage2($image2)
    {
        $this->image2 = $image2;
    }

    /**
     * @return string
     */
    public function getImage3()
    {
        return $this->image3;
    }

    /**
     * @param string $image3
     */
    public function setImage3($image3)
    {
        $this->image3 = $image3;
    }

    /**
     * @return string
     */
    public function getImage4()
    {
        return $this->image4;
    }

    /**
     * @param string $image4
     */
    public function setImage4($image4)
    {
        $this->image4 = $image4;
    }

    /**
     * @return string
     */
    public function getImage5()
    {
        return $this->image5;
    }

    /**
     * @param string $image5
     */
    public function setImage5($image5)
    {
        $this->image5 = $image5;
    }

    /**
     * @return string
     */
    public function getImage6()
    {
        return $this->image6;
    }

    /**
     * @param string $image6
     */
    public function setImage6($image6)
    {
        $this->image6 = $image6;
    }

    /**
     * @return string
     */
    public function getImage7()
    {
        return $this->image7;
    }

    /**
     * @param string $image7
     */
    public function setImage7($image7)
    {
        $this->image7 = $image7;
    }

    /**
     * @return string
     */
    public function getImage8()
    {
        return $this->image8;
    }

    /**
     * @param string $image8
     */
    public function setImage8($image8)
    {
        $this->image8 = $image8;
    }

    /**
     * @return string
     */
    public function getVideoId()
    {
        return $this->videoId;
    }

    /**
     * @param string $videoId
     */
    public function setVideoId($videoId)
    {
        $this->videoId = $videoId;
    }

    /**
     * @return mixed
     */
    public function getReleatedProducts()
    {
        return $this->releatedProducts;
    }

    /**
     * @param mixed $releatedProducts
     */
    public function setReleatedProducts($releatedProducts)
    {
        $this->releatedProducts = $releatedProducts;
    }

    /**
     * @return string
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param string $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return string
     */
    public function getKostenstelle()
    {
        return $this->kostenstelle;
    }

    /**
     * @param string $kostenstelle
     */
    public function setKostenstelle($kostenstelle)
    {
        $this->kostenstelle = $kostenstelle;
    }

    /**
     * @return bool
     */
    public function isAsOffer()
    {
        return $this->asOffer;
    }

    /**
     * @param bool $asOffer
     */
    public function setAsOffer($asOffer)
    {
        $this->asOffer = $asOffer;
    }

    /**
     * @return bool
     */
    public function isDisplayNotInOverview()
    {
        return $this->displayNotInOverview;
    }

    /**
     * @param bool $displayNotInOverview
     */
    public function setDisplayNotInOverview($displayNotInOverview)
    {
        $this->displayNotInOverview = $displayNotInOverview;
    }

    /**
     * @return bool
     */
    public function isNotEdit()
    {
        return $this->notEdit;
    }

    /**
     * @param bool $notEdit
     */
    public function setNotEdit($notEdit)
    {
        $this->notEdit = $notEdit;
    }

    /**
     * @return int
     */
    public function getInitialStatus()
    {
        return $this->initialStatus;
    }

    /**
     * @param int $initialStatus
     */
    public function setInitialStatus($initialStatus)
    {
        $this->initialStatus = $initialStatus;
    }

    /**
     * @return bool
     */
    public function isUploadProduct()
    {
        return $this->uploadProduct;
    }

    /**
     * @param bool $uploadProduct
     */
    public function setUploadProduct($uploadProduct)
    {
        $this->uploadProduct = $uploadProduct;
    }

    /**
     * @return int
     */
    public function getUploadProductInitalStatus()
    {
        return $this->uploadProductInitalStatus;
    }

    /**
     * @param int $uploadProductInitalStatus
     */
    public function setUploadProductInitalStatus($uploadProductInitalStatus)
    {
        $this->uploadProductInitalStatus = $uploadProductInitalStatus;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersIntegrated()
    {
        return $this->collectingOrdersIntegrated;
    }

    /**
     * @param bool $collectingOrdersIntegrated
     */
    public function setCollectingOrdersIntegrated($collectingOrdersIntegrated)
    {
        $this->collectingOrdersIntegrated = $collectingOrdersIntegrated;
    }

    public function setUID($null)
    {
        $this->uid = $null;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return int
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param int $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return string
     */
    public function getMetaKeywords()
    {
        return $this->metaKeywords;
    }

    /**
     * @param string $metaKeywords
     */
    public function setMetaKeywords($metaKeywords)
    {
        $this->metaKeywords = $metaKeywords;
    }

    /**
     * @return string
     */
    public function getMetaDescription()
    {
        return $this->metaDescription;
    }

    /**
     * @param string $metaDescription
     */
    public function setMetaDescription($metaDescription)
    {
        $this->metaDescription = $metaDescription;
    }

    /**
     * @return string
     */
    public function getMetaAuthor()
    {
        return $this->metaAuthor;
    }

    /**
     * @param string $metaAuthor
     */
    public function setMetaAuthor($metaAuthor)
    {
        $this->metaAuthor = $metaAuthor;
    }

    /**
     * @return string
     */
    public function getMetaCustomTitle()
    {
        return $this->metaCustomTitle;
    }

    /**
     * @param string $metaCustomTitle
     */
    public function setMetaCustomTitle($metaCustomTitle)
    {
        $this->metaCustomTitle = $metaCustomTitle;
    }

    /**
     * @return string
     */
    public function getMetaOgTitle()
    {
        return $this->metaOgTitle;
    }

    /**
     * @param string $metaOgTitle
     */
    public function setMetaOgTitle($metaOgTitle)
    {
        $this->metaOgTitle = $metaOgTitle;
    }

    /**
     * @return string
     */
    public function getMetaOgType()
    {
        return $this->metaOgType;
    }

    /**
     * @param string $metaOgType
     */
    public function setMetaOgType($metaOgType)
    {
        $this->metaOgType = $metaOgType;
    }

    /**
     * @return string
     */
    public function getMetaOgUrl()
    {
        return $this->metaOgUrl;
    }

    /**
     * @param string $metaOgUrl
     */
    public function setMetaOgUrl($metaOgUrl)
    {
        $this->metaOgUrl = $metaOgUrl;
    }

    /**
     * @return string
     */
    public function getMetaOgImage()
    {
        return $this->metaOgImage;
    }

    /**
     * @param string $metaOgImage
     */
    public function setMetaOgImage($metaOgImage)
    {
        $this->metaOgImage = $metaOgImage;
    }

    /**
     * @return string
     */
    public function getMetaOgDescription()
    {
        return $this->metaOgDescription;
    }

    /**
     * @param string $metaOgDescription
     */
    public function setMetaOgDescription($metaOgDescription)
    {
        $this->metaOgDescription = $metaOgDescription;
    }

    /**
     * @return bool
     */
    public function isConfirmOne()
    {
        return $this->confirmOne;
    }

    /**
     * @param bool $confirmOne
     */
    public function setConfirmOne($confirmOne)
    {
        $this->confirmOne = $confirmOne;
    }

    /**
     * @return bool
     */
    public function isConfirm()
    {
        return $this->confirm;
    }

    /**
     * @param bool $confirm
     */
    public function setConfirm($confirm)
    {
        $this->confirm = $confirm;
    }

    /**
     * @return int
     */
    public function getConfirmAccount()
    {
        if ($this->confirmAccount == null || $this->confirmAccount->getUid() == 0) {
            return null;
        }
        return $this->confirmAccount;
    }

    /**
     * @param int $confirmAccount
     */
    public function setConfirmAccount($confirmAccount)
    {
        $this->confirmAccount = $confirmAccount;
    }

    /**
     * @return mixed
     */
    public function getConfirmContacts()
    {
        return $this->confirm_contacts;
    }

    /**
     * @param mixed $confirm_contacts
     */
    public function setConfirmContacts($confirm_contacts)
    {
        $this->confirm_contacts = $confirm_contacts;
    }

    /**
     * @return string
     */
    public function getCustomTabTitle()
    {
        return $this->customTabTitle;
    }

    /**
     * @param string $customTabTitle
     */
    public function setCustomTabTitle($customTabTitle)
    {
        $this->customTabTitle = $customTabTitle;
    }

    /**
     * @return string
     */
    public function getCustomTabText()
    {
        return $this->customTabText;
    }

    /**
     * @param string $customTabText
     */
    public function setCustomTabText($customTabText)
    {
        $this->customTabText = $customTabText;
    }

    /**
     * @return string
     */
    public function getHintFile1()
    {
        return $this->hintFile1;
    }

    /**
     * @param string $hintFile1
     */
    public function setHintFile1($hintFile1)
    {
        $this->hintFile1 = $hintFile1;
    }

    /**
     * @return string
     */
    public function getHintFile2()
    {
        return $this->hintFile2;
    }

    /**
     * @param string $hintFile2
     */
    public function setHintFile2($hintFile2)
    {
        $this->hintFile2 = $hintFile2;
    }

    /**
     * @return string
     */
    public function getHintFile3()
    {
        return $this->hintFile3;
    }

    /**
     * @param string $hintFile3
     */
    public function setHintFile3($hintFile3)
    {
        $this->hintFile3 = $hintFile3;
    }

    /**
     * @return string
     */
    public function getHintFile4()
    {
        return $this->hintFile4;
    }

    /**
     * @param string $hintFile4
     */
    public function setHintFile4($hintFile4)
    {
        $this->hintFile4 = $hintFile4;
    }

    /**
     * @return string
     */
    public function getHintText()
    {
        return $this->hintText;
    }

    /**
     * @param string $hintText
     */
    public function setHintText($hintText)
    {
        $this->hintText = $hintText;
    }

    /**
     * @return bool
     */
    public function isCanBuyWithNoPrice()
    {
        return $this->canBuyWithNoPrice;
    }

    /**
     * @param bool $canBuyWithNoPrice
     */
    public function setCanBuyWithNoPrice($canBuyWithNoPrice)
    {
        $this->canBuyWithNoPrice = $canBuyWithNoPrice;
    }

    /**
     * @return bool
     */
    public function isDisplayNoPrice()
    {
        return $this->displayNoPrice;
    }

    /**
     * @param bool $displayNoPrice
     */
    public function setDisplayNoPrice($displayNoPrice)
    {
        $this->displayNoPrice = $displayNoPrice;
    }

    /**
     * @return string
     */
    public function getFromPrice()
    {
        return $this->fromPrice;
    }

    /**
     * @param string $fromPrice
     */
    public function setFromPrice($fromPrice)
    {
        $this->fromPrice = $fromPrice;
    }

    /**
     * @return string
     */
    public function getSetConfig()
    {
        if ($this->setConfig != "") {
            $setConfig = json_decode($this->setConfig);

            if (is_array($setConfig) && count($setConfig) > 0) {
                return $setConfig;
            }
        }
        return [];
    }

    /**
     * @param string $setConfig
     */
    public function setSetConfig($setConfig)
    {
        $this->setConfig = $setConfig;
    }

    /**
     * @return string
     */
    public function getScaledPrice()
    {
        return $this->scaledPrice;
    }

    /**
     * @param string $scaledPrice
     */
    public function setScaledPrice($scaledPrice)
    {
        $this->scaledPrice = $scaledPrice;
    }

    public function getNrTitle()
    {
        return $this->nrIntern . ' ' . $this->title;
    }

    /**
     * @return string
     */
    public function getEinleitung()
    {
        return $this->einleitung;
    }

    /**
     * @param string $einleitung
     */
    public function setEinleitung($einleitung)
    {
        $this->einleitung = $einleitung;
    }

    /**
     * @return int
     */
    public function getStockMaxBuy()
    {
        return $this->stockMaxBuy;
    }

    /**
     * @param int $stockMaxBuy
     */
    public function setStockMaxBuy($stockMaxBuy)
    {
        $this->stockMaxBuy = $stockMaxBuy;
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return mixed
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param mixed $availability
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    }

    /**
     * @return int
     */
    public function getBaseUnit(): int
    {
        return (int)$this->baseUnit;
    }

    /**
     * @param int $baseUnit
     */
    public function setBaseUnit(int $baseUnit): void
    {
        $this->baseUnit = $baseUnit;
    }

    /**
     * @return int
     */
    public function getSalesUnit(): int
    {
        return (int)$this->salesUnit;
    }

    /**
     * @param int $salesUnit
     */
    public function setSalesUnit(int $salesUnit): void
    {
        $this->salesUnit = $salesUnit;
    }

    /**
     * @return int
     */
    public function getPackagingUnit(): int
    {
        return (int)$this->packagingUnit;
    }

    /**
     * @param int $packagingUnit
     */
    public function setPackagingUnit(int $packagingUnit): void
    {
        $this->packagingUnit = $packagingUnit;
    }

    /**
     * @return bool
     */
    public function isUploadFromLatestOrder(): bool
    {
        return (bool)$this->uploadFromLatestOrder;
    }

    /**
     * @param bool $uploadFromLatestOrder
     */
    public function setUploadFromLatestOrder(bool $uploadFromLatestOrder): void
    {
        $this->uploadFromLatestOrder = $uploadFromLatestOrder;
    }

    /**
     * @return int
     */
    public function getUploadFromLatestOrderInitalStatus(): int
    {
        return (int)$this->uploadFromLatestOrderInitalStatus;
    }

    /**
     * @param int $uploadFromLatestOrderInitalStatus
     */
    public function setUploadFromLatestOrderInitalStatus(int $uploadFromLatestOrderInitalStatus): void
    {
        $this->uploadFromLatestOrderInitalStatus = $uploadFromLatestOrderInitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadProvided(): bool
    {
        return (bool)$this->uploadProvided;
    }

    /**
     * @param bool $uploadProvided
     */
    public function setUploadProvided(bool $uploadProvided): void
    {
        $this->uploadProvided = $uploadProvided;
    }

    /**
     * @return string
     */
    public function getUploadProvidedFile(): string
    {
        return (string)$this->uploadProvidedFile;
    }

    /**
     * @param string $uploadProvidedFile
     */
    public function setUploadProvidedFile($uploadProvidedFile): void
    {
        $this->uploadProvidedFile = $uploadProvidedFile;
    }

    /**
     * @return int
     */
    public function getUploadProvidedInitalStatus(): int
    {
        return (int)$this->uploadProvidedInitalStatus;
    }

    /**
     * @param int $uploadProvidedInitalStatus
     */
    public function setUploadProvidedInitalStatus(int $uploadProvidedInitalStatus): void
    {
        $this->uploadProvidedInitalStatus = $uploadProvidedInitalStatus;
    }

    /**
     * @return bool
     */
    public function isHintEnable(): bool
    {
        return boolval($this->hintEnable);
    }

    /**
     * @param bool $hintEnable
     */
    public function setHintEnable(bool $hintEnable): void
    {
        $this->hintEnable = $hintEnable;
    }

    /**
     * @return bool
     */
    public function isCustomTabEnable(): bool
    {
        return boolval($this->customTabEnable);
    }

    /**
     * @param bool $customTabEnable
     */
    public function setCustomTabEnable(bool $customTabEnable): void
    {
        $this->customTabEnable = $customTabEnable;
    }

    public function isAsRequest(): bool
    {
        return boolval($this->asRequest);
    }

    public function setAsRequest(bool $var): void
    {
        $this->asRequest = $var;
    }
}
