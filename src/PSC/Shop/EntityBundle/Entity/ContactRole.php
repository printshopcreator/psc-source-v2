<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Shop
 *
 * @ORM\Table(name="contact_role")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ContactRoleRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class ContactRole
{
    /**
     * Id der ContactRole
     *
     * @var integer
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Contact
     *
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;
/**
     * Role
     *
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    protected $role;
/**
     * @return int
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param int $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }
}
