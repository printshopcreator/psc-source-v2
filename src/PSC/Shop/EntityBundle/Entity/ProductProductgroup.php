<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shop
 *
 * @ORM\Table(name="article_group_article")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ProductProductGroupRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class ProductProductgroup
{
    /**
     * Id des Product
     *
     * @var integer
     *
     * @ORM\ManyToOne(
     *      targetEntity="Product",
     *      inversedBy="groups"
     * )
     * @ORM\JoinColumn(name="article_id", referencedColumnName="id", nullable=false)
     */
    protected $product;
/**
     * Titel des Product
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="articlegroup_id", type="integer")
     */
    protected $group;
/**
     * @return int
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param int $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param int $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
    }
}
