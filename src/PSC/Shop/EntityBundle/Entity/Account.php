<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Account
 *
 * @ORM\Table(name="account")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\AccountRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Account
{
    protected $calcValue1 = '';
    protected $calcValue2 = '';
    protected $productsSub;
    protected $productsOrg;
    protected $priceFactor;
    protected $extraSettings;
/**
     * Id der Firma
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Titel der Firma
     *
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=255)
     */
    protected $title;
/**
     *
     * @var string
     *
     * @ORM\Column(name="appendix", type="string", length=255)
     */
    protected $appendix;
/**
     *
     * @var string
     *
     * @ORM\Column(name="street", type="string", length=255)
     */
    protected $street;
/**
     *
     * @var string
     *
     * @ORM\Column(name="destrict", type="string", length=255)
     */
    protected $destrict;
/**
     *
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=255)
     */
    protected $zip;
/**
     *
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     */
    protected $city;
/**
     *
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255)
     */
    protected $state;
/**
     *
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     */
    protected $country;
/**
     *
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    protected $email;
/**
     *
     * @var string
     *
     * @ORM\Column(name="house_number", type="string", length=255)
     */
    protected $houseNumber;
/**
     *
     * @var string
     *
     * @ORM\Column(name="homepage", type="string", length=255)
     */
    protected $homepage;
/**
     *
     * @var string
     *
     * @ORM\Column(name="phone_lv", type="string", length=255)
     */
    protected $phoneAreaCode;
/**
     *
     * @var string
     *
     * @ORM\Column(name="phone_vorwahl", type="string", length=255)
     */
    protected $phonePrefix;
/**
     *
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     */
    protected $phone;
/**
     *
     * @var string
     *
     * @ORM\Column(name="phone_durchwahl", type="string", length=255)
     */
    protected $phoneAppendix;
/**
     *
     * @var string
     *
     * @ORM\Column(name="mobile_lv", type="string", length=255)
     */
    protected $mobileAreaCode;
/**
     *
     * @var string
     *
     * @ORM\Column(name="mobile_vorwahl", type="string", length=255)
     */
    protected $mobilePrefix;
/**
     *
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=255)
     */
    protected $mobile;
/**
     *
     * @var string
     *
     * @ORM\Column(name="mobile_durchwahl", type="string", length=255)
     */
    protected $mobileAppendix;
/**
     *
     * @var string
     *
     * @ORM\Column(name="fax_lv", type="string", length=255)
     */
    protected $faxAreaCode;
/**
     *
     * @var string
     *
     * @ORM\Column(name="fax_vorwahl", type="string", length=255)
     */
    protected $faxPrefix;
/**
     *
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255)
     */
    protected $fax;
/**
     *
     * @var string
     *
     * @ORM\Column(name="fax_durchwahl", type="string", length=255)
     */
    protected $faxAppendix;
/**
     *
     * @var string
     *
     * @ORM\Column(name="alternativ_lv", type="string", length=255)
     */
    protected $alternativAreaCode;
/**
     *
     * @var string
     *
     * @ORM\Column(name="alternativ_type", type="string", length=255)
     */
    protected $alternativType;
/**
     *
     * @var string
     *
     * @ORM\Column(name="alternativ", type="string", length=255)
     */
    protected $alternativ;
/**
     *
     * @var string
     *
     * @ORM\Column(name="alternativ_durchwahl", type="string", length=255)
     */
    protected $alternativAppendix;
/**
     * Vorgänger der Firma
     *
     * @ORM\ManyToOne(targetEntity="Account", inversedBy="children")
     * @ORM\JoinColumn(name="filiale_id", referencedColumnName="id")
     */
    protected $parent;
/**
     * Vorgänger der Firma
     *
     * @var int
     *
     * @ORM\Column(name="filiale_id", type="integer", nullable=true)
     */
    protected $parentId = 0;
/**
     * Unterfirmen der Firma
     *
     * @ORM\OneToMany(targetEntity="Account", mappedBy="parent")
     */
    protected $children;
/**
     * Gesperter Account
     *
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;
/**
     * Benutze Account als Rechnungsadresse
     *
     * @var boolean
     *
     * @ORM\Column(name="use_account_as_invoice", type="boolean")
     */
    protected $useAccountAsInvoiceAddress;
/**
     * Install
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Install")
     * @ORM\JoinColumn(name="install_id", referencedColumnName="id")
     */
    protected $install;
/**
     * @ORM\ManyToMany(targetEntity="Shop", inversedBy="accounts")
     * @ORM\JoinTable(name="shop_account",
     *      joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="shop_id", referencedColumnName="id")}
     *      )
     **/
    public $shops;
/**
     * @ORM\ManyToMany(targetEntity="Payment", inversedBy="accounts")
     * @ORM\JoinTable(name="account_paymenttype",
     *      joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="paymenttype_id", referencedColumnName="id")}
     *      )
     **/
    public $payments;
/**
     * @ORM\ManyToMany(targetEntity="Shipping", inversedBy="accounts")
     * @ORM\JoinTable(name="account_shippingtype",
     *      joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="shippingtype_id", referencedColumnName="id")}
     *      )
     **/
    public $shippings;
/**
     * @ORM\ManyToMany(targetEntity="Product", inversedBy="accounts")
     * @ORM\JoinTable(name="account_article",
     *      joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="article_id", referencedColumnName="id")}
     *      )
     **/
    public $products;
/**
     * @ORM\ManyToMany(targetEntity="Productgroup", inversedBy="accounts")
     * @ORM\JoinTable(name="account_productgroup",
     *      joinColumns={@ORM\JoinColumn(name="account_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="productgroup_id", referencedColumnName="id")}
     *      )
     **/
    public $productGroups;
/**
     *
     * @var string
     *
     * @ORM\Column(name="bank_name", type="string", length=255)
     */
    protected $bankKtoName;
/**
     *
     * @var string
     *
     * @ORM\Column(name="bank_kto", type="string", length=255)
     */
    protected $bankKTO;
/**
     *
     * @var string
     *
     * @ORM\Column(name="bank_blz", type="string", length=255)
     */
    protected $bankBLZ;
/**
     *
     * @var string
     *
     * @ORM\Column(name="bank_iban", type="string", length=255)
     */
    protected $bankIban;
/**
     *
     * @var string
     *
     * @ORM\Column(name="bank_bic", type="string", length=255)
     */
    protected $bankBic;
/**
     *
     * @var string
     *
     * @ORM\Column(name="bank_bank_name", type="string", length=255)
     */
    protected $bankName;
/**
     *
     * @var string
     *
     * @ORM\Column(name="usid", type="string", length=255)
     */
    protected $ustid;
/**
     *
     * @var int
     *
     * @ORM\Column(name="typ", type="integer", length=2)
     */
    protected $typ;
/**
     *
     * @var string
     *
     * @ORM\Column(name="logo1", type="string", length=255)
     */
    protected $image;
/**
     *
     * @var string
     *
     * @ORM\Column(name="template_switch", type="string", length=255)
     */
    protected $templateSwitch;
/**
     *
     * @var string
     *
     * @ORM\Column(name="informations", type="string")
     */
    protected $information;
/**
     *
     * @var string
     *
     * @ORM\Column(name="mega_code", type="string", length=255)
     */
    protected $megaCode;
/**
     *
     * @var string
     *
     * @ORM\Column(name="hotel_name", type="string", length=255)
     */
    protected $hotelName;
/**
     *
     * @var string
     *
     * @ORM\Column(name="sub_hotel_name", type="string", length=255)
     */
    protected $subHotelName;
/**
     *
     * @var string
     *
     * @ORM\Column(name="email_gm", type="string", length=255)
     */
    protected $emailGm;
/**
     *
     * @var string
     *
     * @ORM\Column(name="zimmer", type="string", length=255)
     */
    protected $room;
/**
     *
     * @var string
     *
     * @ORM\Column(name="gz", type="string", length=255)
     */
    protected $gz;
/**
     *
     * @var string
     *
     * @ORM\Column(name="gf", type="string", length=255)
     */
    protected $gf;
/**
     *
     * @var string
     *
     * @ORM\Column(name="gsitz", type="string", length=255)
     */
    protected $gSitz;
/**
     *
     * @var string
     *
     * @ORM\Column(name="handelsregister", type="string", length=255)
     */
    protected $handelsRegister;
/**
     *
     * @var string
     *
     * @ORM\Column(name="hrb", type="string", length=255)
     */
    protected $hrb;
/**
     *
     * @var string
     *
     * @ORM\Column(name="vorsitz", type="string", length=255)
     */
    protected $vorsitz;
/**
     *
     * @var int
     *
     * @ORM\Column(name="anrede1", type="integer", length=2)
     */
    protected $salutation1;
/**
     *
     * @var string
     *
     * @ORM\Column(name="firstname1", type="string", length=255)
     */
    protected $firstname1;
/**
     *
     * @var string
     *
     * @ORM\Column(name="lastname1", type="string", length=255)
     */
    protected $lastname1;
/**
     *
     * @var int
     *
     * @ORM\Column(name="anrede2", type="integer", length=2)
     */
    protected $salutation2;
/**
     *
     * @var string
     *
     * @ORM\Column(name="firstname2", type="string", length=255)
     */
    protected $firstname2;
/**
     *
     * @var string
     *
     * @ORM\Column(name="lastname2", type="string", length=255)
     */
    protected $lastname2;
/**
     * Constructor
     * @param null $id
     */
    public function __construct($id = null)
    {
        $this->children = new ArrayCollection();
        $this->locked = false;
        $this->uid = $id;
        $this->products = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Fügt eine Unterfirma hinzu
     *
     * @param Account $child
     */
    public function addChild(Account $child)
    {
        $this->children->add($child);
        $child->setParent($this);
    }

    /**
     * Gibt den Titel zurück
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Setzt den Titel
     *
     * @param string $title
     *
     * @return string
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Gibt die übergeordnete Firma zurück
     *
     * @return Account
     */
    public function getParent()
    {
        if ($this->getParentId() == 0) {
            return false;
        }

        return $this->parent;
    }

    /**
     * Setzt die übergeordnete Firma
     *
     * @param Account $parent
     */
    public function setParent($parent = null)
    {
        $this->parent = $parent;
        if ($parent != null) {
            $this->parentId = $parent->getId();
        }
    }

    /**
     * gibt die Firma als Array zurück
     *
     * @return array
     */
    public function getArray()
    {
        return array(
            'title' => $this->getTitle()
        );
    }

    /**
     * gibt die Id zurück
     *
     * @return int
     */
    public function getId()
    {
        return $this->uid;
    }

    /**
     * Gibt die Unterfirmen zurück
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Gibt die ParentId zurück
     *
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Setzt die ParentId
     *
     * @param int $parentId ParentId
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
    }

    /**
     * @return int
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param int $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return string
     */
    public function getCalcValue1()
    {
        return $this->calcValue1;
    }

    /**
     * @param string $calcValue1
     */
    public function setCalcValue1($calcValue1)
    {
        $this->calcValue1 = $calcValue1;
    }

    /**
     * @return string
     */
    public function getCalcValue2()
    {
        return $this->calcValue2;
    }

    /**
     * @param string $calcValue2
     */
    public function setCalcValue2($calcValue2)
    {
        $this->calcValue2 = $calcValue2;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return mixed
     */
    public function getAppendix()
    {
        return $this->appendix;
    }

    /**
     * @param mixed $appendix
     */
    public function setAppendix($appendix)
    {
        $this->appendix = $appendix;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getDestrict()
    {
        return $this->destrict;
    }

    /**
     * @param string $destrict
     */
    public function setDestrict($destrict)
    {
        $this->destrict = $destrict;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param string $homepage
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @return string
     */
    public function getPhoneAreaCode()
    {
        return $this->phoneAreaCode;
    }

    /**
     * @param string $phoneAreaCode
     */
    public function setPhoneAreaCode($phoneAreaCode)
    {
        $this->phoneAreaCode = $phoneAreaCode;
    }

    /**
     * @return string
     */
    public function getPhonePrefix()
    {
        return $this->phonePrefix;
    }

    /**
     * @param string $phonePrefix
     */
    public function setPhonePrefix($phonePrefix)
    {
        $this->phonePrefix = $phonePrefix;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhoneAppendix()
    {
        return $this->phoneAppendix;
    }

    /**
     * @param string $phoneAppendix
     */
    public function setPhoneAppendix($phoneAppendix)
    {
        $this->phoneAppendix = $phoneAppendix;
    }

    /**
     * @return string
     */
    public function getMobileAreaCode()
    {
        return $this->mobileAreaCode;
    }

    /**
     * @param string $mobileAreaCode
     */
    public function setMobileAreaCode($mobileAreaCode)
    {
        $this->mobileAreaCode = $mobileAreaCode;
    }

    /**
     * @return string
     */
    public function getMobilePrefix()
    {
        return $this->mobilePrefix;
    }

    /**
     * @param string $mobilePrefix
     */
    public function setMobilePrefix($mobilePrefix)
    {
        $this->mobilePrefix = $mobilePrefix;
    }

    /**
     * @return string
     */
    public function getMobile()
    {
        return $this->mobile;
    }

    /**
     * @param string $mobile
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;
    }

    /**
     * @return string
     */
    public function getMobileAppendix()
    {
        return $this->mobileAppendix;
    }

    /**
     * @param string $mobileAppendix
     */
    public function setMobileAppendix($mobileAppendix)
    {
        $this->mobileAppendix = $mobileAppendix;
    }

    /**
     * @return string
     */
    public function getFaxAreaCode()
    {
        return $this->faxAreaCode;
    }

    /**
     * @param string $faxAreaCode
     */
    public function setFaxAreaCode($faxAreaCode)
    {
        $this->faxAreaCode = $faxAreaCode;
    }

    /**
     * @return string
     */
    public function getFaxPrefix()
    {
        return $this->faxPrefix;
    }

    /**
     * @param string $faxPrefix
     */
    public function setFaxPrefix($faxPrefix)
    {
        $this->faxPrefix = $faxPrefix;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getFaxAppendix()
    {
        return $this->faxAppendix;
    }

    /**
     * @param string $faxAppendix
     */
    public function setFaxAppendix($faxAppendix)
    {
        $this->faxAppendix = $faxAppendix;
    }

    /**
     * @return string
     */
    public function getAlternativAreaCode()
    {
        return $this->alternativAreaCode;
    }

    /**
     * @param string $alternativAreaCode
     */
    public function setAlternativAreaCode($alternativAreaCode)
    {
        $this->alternativAreaCode = $alternativAreaCode;
    }

    /**
     * @return string
     */
    public function getAlternativType()
    {
        return $this->alternativType;
    }

    /**
     * @param string $alternativType
     */
    public function setAlternativType($alternativType)
    {
        $this->alternativType = $alternativType;
    }

    /**
     * @return string
     */
    public function getAlternativ()
    {
        return $this->alternativ;
    }

    /**
     * @param string $alternativ
     */
    public function setAlternativ($alternativ)
    {
        $this->alternativ = $alternativ;
    }

    /**
     * @return string
     */
    public function getAlternativAppendix()
    {
        return $this->alternativAppendix;
    }

    /**
     * @param string $alternativAppendix
     */
    public function setAlternativAppendix($alternativAppendix)
    {
        $this->alternativAppendix = $alternativAppendix;
    }

    /**
     * @return bool
     */
    public function isUseAccountAsInvoiceAddress()
    {
        return $this->useAccountAsInvoiceAddress;
    }

    /**
     * @param bool $useAccountAsInvoiceAddress
     */
    public function setUseAccountAsInvoiceAddress($useAccountAsInvoiceAddress)
    {
        $this->useAccountAsInvoiceAddress = $useAccountAsInvoiceAddress;
    }

    /**
     * @return string
     */
    public function getBankKtoName()
    {
        return $this->bankKtoName;
    }

    /**
     * @param string $bankKtoName
     */
    public function setBankKtoName($bankKtoName)
    {
        $this->bankKtoName = $bankKtoName;
    }

    /**
     * @return string
     */
    public function getBankKTO()
    {
        return $this->bankKTO;
    }

    /**
     * @param string $bankKTO
     */
    public function setBankKTO($bankKTO)
    {
        $this->bankKTO = $bankKTO;
    }

    /**
     * @return string
     */
    public function getBankBLZ()
    {
        return $this->bankBLZ;
    }

    /**
     * @param string $bankBLZ
     */
    public function setBankBLZ($bankBLZ)
    {
        $this->bankBLZ = $bankBLZ;
    }

    /**
     * @return string
     */
    public function getBankIban()
    {
        return $this->bankIban;
    }

    /**
     * @param string $bankIban
     */
    public function setBankIban($bankIban)
    {
        $this->bankIban = $bankIban;
    }

    /**
     * @return string
     */
    public function getBankBic()
    {
        return $this->bankBic;
    }

    /**
     * @param string $bankBic
     */
    public function setBankBic($bankBic)
    {
        $this->bankBic = $bankBic;
    }

    /**
     * @return string
     */
    public function getBankName()
    {
        return $this->bankName;
    }

    /**
     * @param string $bankName
     */
    public function setBankName($bankName)
    {
        $this->bankName = $bankName;
    }

    /**
     * @return int
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * @param int $typ
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;
    }

    /**
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param string $image
     */
    public function setImage($image)
    {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getTemplateSwitch()
    {
        return $this->templateSwitch;
    }

    /**
     * @param string $templateSwitch
     */
    public function setTemplateSwitch($templateSwitch)
    {
        $this->templateSwitch = $templateSwitch;
    }

    /**
     * @return string
     */
    public function getMegaCode()
    {
        return $this->megaCode;
    }

    /**
     * @param string $megaCode
     */
    public function setMegaCode($megaCode)
    {
        $this->megaCode = $megaCode;
    }

    /**
     * @return string
     */
    public function getHotelName()
    {
        return $this->hotelName;
    }

    /**
     * @param string $hotelName
     */
    public function setHotelName($hotelName)
    {
        $this->hotelName = $hotelName;
    }

    /**
     * @return string
     */
    public function getSubHotelName()
    {
        return $this->subHotelName;
    }

    /**
     * @param string $subHotelName
     */
    public function setSubHotelName($subHotelName)
    {
        $this->subHotelName = $subHotelName;
    }

    /**
     * @return string
     */
    public function getEmailGm()
    {
        return $this->emailGm;
    }

    /**
     * @param string $emailGm
     */
    public function setEmailGm($emailGm)
    {
        $this->emailGm = $emailGm;
    }

    /**
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param string $room
     */
    public function setRoom($room)
    {
        $this->room = $room;
    }

    /**
     * @return string
     */
    public function getGz()
    {
        return $this->gz;
    }

    /**
     * @param string $gz
     */
    public function setGz($gz)
    {
        $this->gz = $gz;
    }

    /**
     * @return string
     */
    public function getGf()
    {
        return $this->gf;
    }

    /**
     * @param string $gf
     */
    public function setGf($gf)
    {
        $this->gf = $gf;
    }

    /**
     * @return string
     */
    public function getGSitz()
    {
        return $this->gSitz;
    }

    /**
     * @param string $gSitz
     */
    public function setGSitz($gSitz)
    {
        $this->gSitz = $gSitz;
    }

    /**
     * @return string
     */
    public function getHandelsRegister()
    {
        return $this->handelsRegister;
    }

    /**
     * @param string $handelsRegister
     */
    public function setHandelsRegister($handelsRegister)
    {
        $this->handelsRegister = $handelsRegister;
    }

    /**
     * @return string
     */
    public function getHrb()
    {
        return $this->hrb;
    }

    /**
     * @param string $hrb
     */
    public function setHrb($hrb)
    {
        $this->hrb = $hrb;
    }

    /**
     * @return string
     */
    public function getVorsitz()
    {
        return $this->vorsitz;
    }

    /**
     * @param string $vorsitz
     */
    public function setVorsitz($vorsitz)
    {
        $this->vorsitz = $vorsitz;
    }

    /**
     * @return int
     */
    public function getSalutation1()
    {
        return $this->salutation1;
    }

    /**
     * @param int $salutation1
     */
    public function setSalutation1($salutation1)
    {
        $this->salutation1 = $salutation1;
    }

    /**
     * @return string
     */
    public function getFirstname1()
    {
        return $this->firstname1;
    }

    /**
     * @param string $firstname1
     */
    public function setFirstname1($firstname1)
    {
        $this->firstname1 = $firstname1;
    }

    /**
     * @return string
     */
    public function getLastname1()
    {
        return $this->lastname1;
    }

    /**
     * @param string $lastname1
     */
    public function setLastname1($lastname1)
    {
        $this->lastname1 = $lastname1;
    }

    /**
     * @return int
     */
    public function getSalutation2()
    {
        return $this->salutation2;
    }

    /**
     * @param int $salutation2
     */
    public function setSalutation2($salutation2)
    {
        $this->salutation2 = $salutation2;
    }

    /**
     * @return string
     */
    public function getFirstname2()
    {
        return $this->firstname2;
    }

    /**
     * @param string $firstname2
     */
    public function setFirstname2($firstname2)
    {
        $this->firstname2 = $firstname2;
    }

    /**
     * @return string
     */
    public function getLastname2()
    {
        return $this->lastname2;
    }

    /**
     * @param string $lastname2
     */
    public function setLastname2($lastname2)
    {
        $this->lastname2 = $lastname2;
    }

    /**
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * @param string $information
     */
    public function setInformation($information)
    {
        $this->information = $information;
    }

    /**
     * @return string
     */
    public function getUstid()
    {
        return $this->ustid;
    }

    /**
     * @param string $ustid
     */
    public function setUstid($ustid)
    {
        $this->ustid = $ustid;
    }

    public function getTitleAppendix()
    {
        return $this->title . ' ' . $this->appendix;
    }

    /**
     * @return mixed
     */
    public function getProductsSub()
    {
        return $this->productsSub;
    }

    /**
     * @param mixed $productsSub
     */
    public function setProductsSub($productsSub)
    {
        $this->productsSub = $productsSub;
    }

    /**
     * @return mixed
     */
    public function getProductsOrg()
    {
        return $this->productsOrg;
    }

    /**
     * @param mixed $productsOrg
     */
    public function setProductsOrg($productsOrg)
    {
        $this->productsOrg = $productsOrg;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * @return bool
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @return mixed
     */
    public function getProductGroups()
    {
        return $this->productGroups;
    }

    /**
     * @param mixed $productGroups
     */
    public function setProductGroups($productGroups)
    {
        $this->productGroups = $productGroups;
    }

    /**
     * @return mixed
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param mixed $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return mixed
     */
    public function getPriceFactor()
    {
        if (!$this->priceFactor) {
            return 1;
        }
        return $this->priceFactor;
    }

    /**
     * @param mixed $priceFactor
     */
    public function setPriceFactor($priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }
}
