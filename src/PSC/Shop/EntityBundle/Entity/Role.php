<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Security\Core\Role\RoleInterface;

/**
 * Role
 *
 * @ORM\Table(name="role")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\RoleRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Role
{
    /**
     * Id
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Title
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    protected $title;
/**
     * level
     *
     * @var string
     *
     * @ORM\Column(name="level", type="integer", length=6)
     */
    protected $level;
/**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * @param string $level
     */
    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function __toString()
    {
        return $this->getTitle();
    }


    /**
     * Returns the role.
     *
     * This method returns a string representation whenever possible.
     *
     * When the role cannot be represented with sufficient precision by a
     * string, it should return null.
     *
     * @return string|null A string representation of the role, or null
     */
    public function getRole()
    {
        if ($this->uid == 5 || $this->uid == 6) {
            return "ROLE_USER";
        }
        if ($this->uid == 8) {
            return "ROLE_PRODUCT_EDITOR";
        }
        if ($this->uid == 4) {
            return "ROLE_SHOP_OPERATOR";
        }
        if ($this->uid == 7) {
            return "ROLE_PRODUCTION";
        }
        if ($this->uid == 3) {
            return "ROLE_ADMIN";
        }
        if ($this->uid == 2) {
            return "ROLE_SYSTEM_ADMIN";
        }

        return "ROLE_GUEST";
    }
}
