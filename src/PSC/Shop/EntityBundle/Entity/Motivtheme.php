<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Motivtheme
 *
 * @ORM\Table(name="motiv_theme")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\MotivthemeRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Motivtheme
{
    /**
     * Id des Motives
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Titel
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    protected $title;
/**
     * Shop zu welcher das Product gehört
     *
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * UUID
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string")
     */
    protected $uuid;
/**
     * @ORM\OneToMany(targetEntity="Motivtheme", mappedBy="parent")
     **/
    private $children;
/**
     * @ORM\ManyToOne(targetEntity="Motivtheme", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     **/
    private $parent;
    // ...

    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Shop $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return mixed
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * @param mixed $children
     */
    public function setChildren($children)
    {
        $this->children = $children;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }
}
