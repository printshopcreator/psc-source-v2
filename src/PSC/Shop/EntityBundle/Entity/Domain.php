<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Image
 *
 * @ORM\Table(name="domain")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\DomainRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Domain
{
    public $svg;
/**
     * Id des Motives
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Host
     *
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $host;
/**
     * Redirect
     *
     * @var string
     *
     * @ORM\Column(name="redirect", type="string", nullable=true)
     */
    protected $redirect;
/**
     * Created
     *
     * @var Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;
/**
     * Created
     *
     * @var boolean
     *
     * @ORM\Column(name="letsEncrypt", type="boolean", nullable=true)
     */
    protected $letsEncrypt;
/**
     * Updated
     *
     * @var Datetime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    protected $updated;
/**
     * Shop zu welcher das Product gehört
     *
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Konstruktor
     */
    public function __construct(Shop $shop)
    {
        $this->created = new \DateTime();
        $this->updated = new \DateTime();
        $this->redirect = '';
        $this->letsEncrypt = false;
        $this->shop = $shop;
    }

    public function getIp()
    {
        return gethostbyname($this->host);
    }

    /**
     * @return Shop
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param Shop $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return Datetime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param Datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * @param string $redirect
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * @return Datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param Datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost($host)
    {
        $this->host = $host;
    }

    /**
     * @return boolean
     */
    public function isLetsEncrypt()
    {
        return $this->letsEncrypt;
    }

    /**
     * @param boolean $letsEncrypt
     */
    public function setLetsEncrypt($letsEncrypt)
    {
        $this->letsEncrypt = $letsEncrypt;
    }
}
