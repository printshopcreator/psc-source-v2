<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Shop
 *
 * @ORM\Table(name="shop_contact")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ShopContactRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class ShopContact
{
    /**
     * Id des Shops
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(name="id")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id = null;
/**
     * @var string
     *
     * @ORM\Column(name="uuid")
     *
     */
    protected $uuid = null;
/**
     * Shop zu welcher das Product gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Contact der die Buchung gemacht hat
     *
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;
/**
     * Admin des Shops
     *
     * @var boolean
     *
     * @ORM\Column(name="admin", type="boolean")
     */
    protected $admin;
/**
     * Selected Shop
     *
     * @var boolean
     *
     * @ORM\Column(name="selected", type="boolean")
     */
    protected $selected;
/**
     * Login des Shops
     *
     * @var boolean
     *
     * @ORM\Column(name="login", type="boolean")
     */
    protected $login;
/**
     * ShopContact constructor.
     */
    public function __construct()
    {
        $this->uuid = Uuid::uuid4();
    }


    /**
     * @return boolean
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /**
     * @param boolean $admin
     */
    public function setAdmin($admin)
    {
        $this->admin = $admin;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return boolean
     */
    public function getSelected()
    {
        return $this->selected;
    }

    /**
     * @param boolean $selected
     */
    public function setSelected($selected)
    {
        $this->selected = $selected;
    }

    /**
     * @return boolean
     */
    public function isLogin()
    {
        return $this->login;
    }

    /**
     * @param boolean $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setUid($id)
    {
        $this->id = $id;
    }
}
