<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Layoutdesigndata
 *
 * @ORM\Table(name="layouter_design_data")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\LayoutdesigndataRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Layoutdesigndata
{
    /**
     * Id des Shops
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * dynamicWidth
     *
     * @var integer
     *
     * @ORM\Column(name="dynamicWidth", type="integer", nullable=true)
     */
    protected $dynamicWidth;
/**
     * dynamicHeight
     *
     * @var integer
     *
     * @ORM\Column(name="dynamicHeight", type="integer", nullable=true)
     */
    protected $dynamicHeight;
/**
     * dynamicSize
     *
     * @var boolean
     *
     * @ORM\Column(name="dynamicSize", type="boolean")
     */
    protected $dynamicSize;
/**
     * Design Data
     *
     * @var string
     *
     * @ORM\Column(name="design", type="text", nullable=true)
     */
    protected $design;
/**
     * DataTable
     *
     * @var string
     *
     * @ORM\Column(name="datatable", type="text", nullable=true)
     */
    protected $datatable;
/**
     * UUID des Designs
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=255, nullable=true)
     */
    protected $uuid;
/**
     * UUID des Articles
     *
     * @var string
     *
     * @ORM\Column(name="article_uuid", type="string", length=255, nullable=true)
     */
    protected $article_uuid;
/**
     * Title des Designs
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    protected $title;
/**
     * Preview Path des Designs
     *
     * @var string
     *
     * @ORM\Column(name="preview_path", type="string", length=255, nullable=true)
     */
    protected $preview_path;
/**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return string
     */
    public function getDesign()
    {
        return json_decode($this->design, true);
    }

    /**
     * @param string $design
     */
    public function setDesign($design)
    {
        $this->design = json_encode($design);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getPreviewPath()
    {
        return $this->preview_path;
    }

    /**
     * @param string $preview_path
     */
    public function setPreviewPath($preview_path)
    {
        $this->preview_path = $preview_path;
    }

    /**
     * @return string
     */
    public function getArticleUuid()
    {
        return $this->article_uuid;
    }

    /**
     * @param string $article_uuid
     */
    public function setArticleUuid($article_uuid)
    {
        $this->article_uuid = $article_uuid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getDatatable()
    {
        return json_decode($this->datatable, true);
    }

    /**
     * @param string $datatable
     */
    public function setDatatable($datatable)
    {
        $this->datatable = json_encode($datatable);
    }

    /**
     * @return bool
     */
    public function isDynamicSize()
    {
        return $this->dynamicSize;
    }

    /**
     * @param bool $dynamicSize
     */
    public function setDynamicSize($dynamicSize)
    {
        $this->dynamicSize = $dynamicSize;
    }

    /**
     * @return int
     */
    public function getDynamicHeight()
    {
        return $this->dynamicHeight;
    }

    /**
     * @param int $dynamicHeight
     */
    public function setDynamicHeight($dynamicHeight)
    {
        $this->dynamicHeight = $dynamicHeight;
    }

    /**
     * @return int
     */
    public function getDynamicWidth()
    {
        return $this->dynamicWidth;
    }

    /**
     * @param int $dynamicWidth
     */
    public function setDynamicWidth($dynamicWidth)
    {
        $this->dynamicWidth = $dynamicWidth;
    }
}
