<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\ImageRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Image
{
    /**
     * Id des Shops
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $uid;
/**
     * Path
     *
     * @var string
     *
     * @ORM\Column(name="path", type="string")
     */
    protected $path;
/**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }
}
