<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Image
 *
 * @ORM\Table(name="motiv")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\MotivRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Motiv
{
    public $svg;
/**
     * Id des Motives
     *
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $uid;
/**
     * Titel
     *
     * @var string
     *
     * @ORM\Column(name="title", type="string")
     */
    protected $title;
/**
     * UUID
     *
     * @var string
     *
     * @ORM\Column(name="uuid", type="string")
     */
    protected $uuid;
/**
     * Dpi Org
     *
     * @var string
     *
     * @ORM\Column(name="dpi_org", type="string")
     */
    protected $dpi_org;
/**
     * DPI Con
     *
     * @var string
     *
     * @ORM\Column(name="dpi_con", type="string")
     */
    protected $dpi_con;
/**
     * Org Filename
     *
     * @var string
     *
     * @ORM\Column(name="orgfilename", type="string")
     */
    protected $org_filename;
/**
     * Typ
     *
     * @var string
     *
     * @ORM\Column(name="typ", type="string")
     */
    protected $type;
/**
     * Exif
     *
     * @var string
     *
     * @ORM\Column(name="exif", type="string")
     */
    protected $exif;
/**
     * File Work
     *
     * @var string
     *
     * @ORM\Column(name="file_work", type="string")
     */
    protected $file_work;
/**
     * File Thumb
     *
     * @var string
     *
     * @ORM\Column(name="file_thumb", type="string")
     */
    protected $file_thumb;
/**
     * File Mid
     *
     * @var string
     *
     * @ORM\Column(name="file_mid", type="string")
     */
    protected $file_mid;
/**
     * File Orginal
     *
     * @var string
     *
     * @ORM\Column(name="file_orginal", type="string")
     */
    protected $file_orginal;
/**
     * Copyright
     *
     * @var string
     *
     * @ORM\Column(name="copyright", type="string")
     */
    protected $copyright;
/**
     * Created
     *
     * @var Datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;
/**
     * Width
     *
     * @var int
     *
     * @ORM\Column(name="width", type="integer")
     */
    protected $width;
/**
     * Install Id
     *
     * @var int
     *
     * @ORM\Column(name="install_id", type="integer")
     */
    protected $install_id;
/**
     * Height
     *
     * @var int
     *
     * @ORM\Column(name="height", type="integer")
     */
    protected $height;
/**
     * Mid Width
     *
     * @var int
     *
     * @ORM\Column(name="mid_width", type="integer")
     */
    protected $mid_width;
/**
     * Mid Height
     *
     * @var int
     *
     * @ORM\Column(name="mid_height", type="integer")
     */
    protected $mid_height;
/**
     * Status
     *
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    protected $status;
/**
     * Shop zu welcher das Product gehört
     *
     * @var Shop
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;
/**
     * Account zu welcher das Motiv gehört
     *
     * @var Account
     *
     * @ORM\ManyToOne(targetEntity="Account")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    protected $account;
/**
     * Contact zu welcher das Motiv gehört
     *
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;
/**
     * @param boolean $resale_download
     */
    public function setResaleDownload($resale_download)
    {
        $this->resale_download = $resale_download;
    }

    /**
     * @return boolean
     */
    public function getResaleDownload()
    {
        return $this->resale_download;
    }

    /**
     * @param boolean $resale_market
     */
    public function setResaleMarket($resale_market)
    {
        $this->resale_market = $resale_market;
    }

    /**
     * @return boolean
     */
    public function getResaleMarket()
    {
        return $this->resale_market;
    }

    /**
     * @param boolean $resale_shop
     */
    public function setResaleShop($resale_shop)
    {
        $this->resale_shop = $resale_shop;
    }

    /**
     * @return boolean
     */
    public function getResaleShop()
    {
        return $this->resale_shop;
    }

    /**
     * Ressale Shop
     *
     * @var boolean
     *
     * @ORM\Column(name="resale_shop", type="boolean")
     */
    protected $resale_shop;
/**
     * Ressale Market
     *
     * @var boolean
     *
     * @ORM\Column(name="resale_market", type="boolean")
     */
    protected $resale_market;
/**
     * Ressale Download
     *
     * @var boolean
     *
     * @ORM\Column(name="resale_download", type="boolean")
     */
    protected $resale_download;
    public function __construct()
    {
        $this->uuid = Uuid::uuid1()->toString();
        $this->created = new \DateTime();
        $this->deleted = false;
    }


    /**
     * Gibt die Id zurück
     *
     * @return int
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getFileWork()
    {
        return $this->file_work;
    }

    /**
     * @param string $file_work
     */
    public function setFileWork($file_work)
    {
        $this->file_work = $file_work;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return string
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * @param string $copyright
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }

    /**
     * @return string
     */
    public function getFileThumb()
    {
        return $this->file_thumb;
    }

    /**
     * @param string $file_thumb
     */
    public function setFileThumb($file_thumb)
    {
        $this->file_thumb = $file_thumb;
    }

    /**
     * @return string
     */
    public function getFileMid()
    {
        return $this->file_mid;
    }

    /**
     * @param string $file_mid
     */
    public function setFileMid($file_mid)
    {
        $this->file_mid = $file_mid;
    }

    /**
     * @return string
     */
    public function getFileOrginal()
    {
        return $this->file_orginal;
    }

    /**
     * @param string $file_orginal
     */
    public function setFileOrginal($file_orginal)
    {
        $this->file_orginal = $file_orginal;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return string
     */
    public function getExif()
    {
        return $this->exif;
    }

    /**
     * @param string $exif
     */
    public function setExif($exif)
    {
        $this->exif = $exif;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return int
     */
    public function getMidWidth()
    {
        return $this->mid_width;
    }

    /**
     * @param int $mid_width
     */
    public function setMidWidth($mid_width)
    {
        $this->mid_width = $mid_width;
    }

    /**
     * @return int
     */
    public function getMidHeight()
    {
        return $this->mid_height;
    }

    /**
     * @param int $mid_height
     */
    public function setMidHeight($mid_height)
    {
        $this->mid_height = $mid_height;
    }

    /**
     * @return string
     */
    public function getOrgFilename()
    {
        return $this->org_filename;
    }

    /**
     * @param string $org_filename
     */
    public function setOrgFilename($org_filename)
    {
        $this->org_filename = $org_filename;
    }

    /**
     * @return string
     */
    public function getDpiOrg()
    {
        return $this->dpi_org;
    }

    /**
     * @param string $dpi_org
     */
    public function setDpiOrg($dpi_org)
    {
        $this->dpi_org = $dpi_org;
    }

    /**
     * @return string
     */
    public function getDpiCon()
    {
        return $this->dpi_con;
    }

    /**
     * @param string $dpi_con
     */
    public function setDpiCon($dpi_con)
    {
        $this->dpi_con = $dpi_con;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return int
     */
    public function getInstallId()
    {
        return $this->install_id;
    }

    /**
     * @param int $install_id
     */
    public function setInstallId($install_id)
    {
        $this->install_id = $install_id;
    }
}
