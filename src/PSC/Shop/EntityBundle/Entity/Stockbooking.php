<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Shop
 *
 * @ORM\Table(name="stockbooking")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\StockbookingRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Stockbooking
{
    /**
     * Id des Product
     *
     * @var string
     *
     * @ORM\Column(name="uid", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $uid;
/**
     * Artikel zu welcher die Buchung gehört
     *
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    protected $product;
/**
     * Contact der die Buchung gemacht hat
     *
     * @var Contact
     *
     * @ORM\ManyToOne(targetEntity="Contact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    protected $contact;
/**
     * Info der Buchung
     *
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    protected $description;
/**
     * Anzahl der Buchung
     *
     * @var int
     *
     * @ORM\Column(name="amount", type="text")
     */
    protected $amount;
/**
     * Erzeugt am
     *
     * @var datetime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    protected $created;
/**
     * Lieferschein der Buchung
     *
     * @var string
     *
     * @ORM\Column(name="delivery", type="string", length=255)
     */
    protected $delivery;
/**
     * Constructer
     * @param Product $product
     */
    public function __construct($product)
    {
        $this->uid = Uuid::uuid4();
        $this->created = new \DateTime();
        $this->product = $product;
    }

    /**
     * Gibt die Id zurück
     *
     * @return string
     */
    public function getUID()
    {
        return $this->uid;
    }

    /**
     * Gibt die Beschreibung zurück
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Setzt die Beschreibung
     *
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Gibt die Anzahl zurück
     *
     * @return int
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Setzt die Anzahl
     *
     * @param int $amount
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Gibt einen Lieferschein zurück
     *
     * @return string
     */
    public function getDelivery()
    {
        return $this->delivery;
    }

    /**
     * Setzt den Liefersein
     *
     * @param string $delivery
     */
    public function setDelivery($delivery)
    {
        $this->delivery = $delivery;
    }

    /**
     * Contact welcher die Buchung angelegt hat
     *
     * @return \PSC\Shop\EntityBundle\Entity\Contact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Contact welcher die Buchung angelegt hat
     *
     * @return \PSC\Shop\EntityBundle\Entity\Contact
     */
    public function getContactName()
    {
        return $this->contact->getEmail();
    }

    /**
     * Setzt den Kontakt welcher die Buchung angelegt hat
     *
     * @param \PSC\Shop\EntityBundle\Entity\Contact $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Gibt das verknüpfte Produkt zurück
     *
     * @return \PSC\Shop\EntityBundle\Entity\Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Setzt das Produkt
     *
     * @param \PSC\Shop\EntityBundle\Entity\Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * Wann wurde die Buchung erzeugt
     *
     * @return \Datetime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Setzt wann die Buchung erzeugt wird
     *
     * @param \Datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
}
