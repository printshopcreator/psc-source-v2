<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Paper
 *
 * @ORM\Table(name="papierdb")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\PaperRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Paper
{
    protected $papierTyp1;
    protected $papierTyp2;
    protected $papierTyp3;
    protected $papierTyp4;
    protected $papierTyp5;
    protected $papierTyp6;
    protected $papierTyp7;
    protected $papierTyp8;
    protected $papierTyp9;
    protected $papierTyp10;
    protected $papierTyp11;
    protected $papierTyp12;
    protected $papierTyp13;
    protected $papierTyp14;
    protected $umschlagen;
    protected $happy;
    protected $eq;
    protected $sense;
    protected $sky;
    protected $glam;
    protected $post;
    protected $volume;
/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
/**
     * @var integer
     *
     * @ORM\Column(name="install_id", type="integer", nullable=true)
     */
    private $install;
/**
     * @var string
     *
     * @ORM\Column(name="art_nr", type="string", length=25, nullable=true)
     */
    private $artNr;
/**
     * @var string
     *
     * @ORM\Column(name="auslauf", type="string", length=1, nullable=true)
     */
    private $auslauf;
/**
     * @var string
     *
     * @ORM\Column(name="description_1", type="string", length=255, nullable=true)
     */
    private $description1;
/**
     * @var string
     *
     * @ORM\Column(name="description_2", type="string", length=255, nullable=true)
     */
    private $description2;
/**
     * @var string
     *
     * @ORM\Column(name="grammatur", type="string", length=6, nullable=true)
     */
    private $grammatur;
/**
     * @var string
     *
     * @ORM\Column(name="staerke", type="string", length=60, nullable=true)
     */
    private $staerke;
/**
     * @var string
     *
     * @ORM\Column(name="breite", type="string", length=60, nullable=true)
     */
    private $breite;
/**
     * @var string
     *
     * @ORM\Column(name="hoehe", type="string", length=60, nullable=true)
     */
    private $hoehe;
/**
     * @var string
     *
     * @ORM\Column(name="laenge", type="string", length=60, nullable=true)
     */
    private $laenge;
/**
     * @var string
     *
     * @ORM\Column(name="rollenlaenge", type="string", length=60, nullable=true)
     */
    private $rollenLaenge;
/**
     * @var integer
     *
     * @ORM\Column(name="gewicht", type="integer", nullable=true)
     */
    private $gewicht;
/**
     * @var string
     *
     * @ORM\Column(name="papierausruestung", type="string", length=60, nullable=true)
     */
    private $papierAusruestung;
/**
     * @var string
     *
     * @ORM\Column(name="farbnummer", type="string", length=60, nullable=true)
     */
    private $farbNummer;
/**
     * @var string
     *
     * @ORM\Column(name="farbbezeichnung", type="string", length=60, nullable=true)
     */
    private $farbBezeichnung;
/**
     * @var string
     *
     * @ORM\Column(name="huelsendurchmesser", type="string", length=60, nullable=true)
     */
    private $huelsenDurchmesser;
/**
     * @var string
     *
     * @ORM\Column(name="kleber", type="string", length=60, nullable=true)
     */
    private $kleber;
/**
     * @var string
     *
     * @ORM\Column(name="abdeckpapier", type="string", length=60, nullable=true)
     */
    private $abdeckPapier;
/**
     * @var string
     *
     * @ORM\Column(name="laufrichtung", type="string", length=2, nullable=true)
     */
    private $laufRichtung;
/**
     * @var integer
     *
     * @ORM\Column(name="mengenangabe", type="integer", nullable=true)
     */
    private $mengenAngabe;
/**
     * @var integer
     *
     * @ORM\Column(name="mengenangabe_palette", type="integer", nullable=true)
     */
    private $mengenangabePalette;
/**
     * @var string
     *
     * @ORM\Column(name="inhalt", type="string", length=60, nullable=true)
     */
    private $inhalt;
/**
     * @var string
     *
     * @ORM\Column(name="etiketten_je_blatt", type="string", length=60, nullable=true)
     */
    private $etikettenJeBlatt;
/**
     * @var string
     *
     * @ORM\Column(name="mengeneinheit", type="string", length=30, nullable=true)
     */
    private $mengenEinheit;
/**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_1", type="integer", nullable=true)
     */
    private $staffelmenge1;
/**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_1", type="float", precision=10, scale=0, nullable=true)
     */
    private $staffelpreis1;
/**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_2", type="integer", nullable=true)
     */
    private $staffelmenge2;
/**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_2", type="float", precision=10, scale=0, nullable=true)
     */
    private $staffelpreis2;
/**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_3", type="integer", nullable=true)
     */
    private $staffelmenge3;
/**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_3", type="float", precision=10, scale=0, nullable=true)
     */
    private $staffelpreis3;
/**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_4", type="integer", nullable=true)
     */
    private $staffelmenge4;
/**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_4", type="float", precision=10, scale=0, nullable=true)
     */
    private $staffelpreis4;
/**
     * @var integer
     *
     * @ORM\Column(name="staffelmenge_5", type="integer", nullable=true)
     */
    private $staffelmenge5;
/**
     * @var float
     *
     * @ORM\Column(name="staffelpreis_5", type="float", precision=10, scale=0, nullable=true)
     */
    private $staffelpreis5;
/**
     * @var string
     *
     * @ORM\Column(name="lagerort", type="string", length=7, nullable=true)
     */
    private $lagerOrt;
/**
     * @var string
     *
     * @ORM\Column(name="verkaufshinweise", type="text", nullable=true)
     */
    private $verkaufshinweise;
/**
     * @var string
     *
     * @ORM\Column(name="abnahmeinfo", type="string", length=60, nullable=true)
     */
    private $abnahmeInfo;
/**
     * @var string
     *
     * @ORM\Column(name="produkt_beschreibung", type="text", nullable=true)
     */
    private $produktBeschreibung;
/**
     * @var string
     *
     * @ORM\Column(name="produkt_eigenschaften", type="text", nullable=true)
     */
    private $produktEigenschaften;
/**
     * @var string
     *
     * @ORM\Column(name="produkt_vorteile", type="text", nullable=true)
     */
    private $produktVorteile;
/**
     * @var string
     *
     * @ORM\Column(name="produkt_nutzen", type="text", nullable=true)
     */
    private $produktNutzen;
/**
     * @var string
     *
     * @ORM\Column(name="produkt_anwendungen", type="text", nullable=true)
     */
    private $produktAnwendungen;
/**
     * @var string
     *
     * @ORM\Column(name="produkt_besonderheit", type="text", nullable=true)
     */
    private $produktBesonderheit;
/**
     * @var string
     *
     * @ORM\Column(name="musterbuch", type="string", length=40, nullable=true)
     */
    private $musterbuch;
/**
     * @var string
     *
     * @ORM\Column(name="zur_aufnahme_von", type="string", length=60, nullable=true)
     */
    private $zurAufnahmeVon;
/**
     * @var string
     *
     * @ORM\Column(name="eigenschaften", type="text", nullable=true)
     */
    private $eigenschaften;
/**
     * @var float
     *
     * @ORM\Column(name="preis", type="float", precision=10, scale=0, nullable=true)
     */
    private $preis;
/**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=40, nullable=true)
     */
    private $uuid;
/**
     * @var float
     *
     * @ORM\Column(name="offset_fix", type="float", precision=10, scale=0, nullable=true)
     */
    private $offsetFix;
/**
     * @var float
     *
     * @ORM\Column(name="offset_var", type="float", precision=10, scale=0, nullable=true)
     */
    private $offsetVar;
/**
     * @var float
     *
     * @ORM\Column(name="digital_fix", type="float", precision=10, scale=0, nullable=true)
     */
    private $digitalFix;
/**
     * @var float
     *
     * @ORM\Column(name="digital_var", type="float", precision=10, scale=0, nullable=true)
     */
    private $digitalVar;
/**
     * Paper constructor.
     */
    public function __construct()
    {
        $this->install = '';
        $this->artNr = '';
        $this->auslauf = '';
        $this->description1 = '';
        $this->description2 = '';
        $this->grammatur = '100';
        $this->staerke = '';
        $this->breite = '';
        $this->hoehe = '';
        $this->laenge = '';
        $this->rollenLaenge = '';
        $this->gewicht = '';
        $this->papierAusruestung = '';
        $this->farbNummer = '';
        $this->farbBezeichnung = '';
        $this->huelsenDurchmesser = '';
        $this->kleber = '';
        $this->abdeckPapier = '';
        $this->laufRichtung = '';
        $this->mengenAngabe = '';
        $this->mengenangabePalette = '';
        $this->inhalt = '';
        $this->etikettenJeBlatt = '';
        $this->mengenEinheit = '';
        $this->staffelmenge1 = '';
        $this->staffelpreis1 = '';
        $this->staffelmenge2 = '';
        $this->staffelpreis2 = '';
        $this->staffelmenge3 = '';
        $this->staffelpreis3 = '';
        $this->staffelmenge4 = '';
        $this->staffelpreis4 = '';
        $this->staffelmenge5 = '';
        $this->staffelpreis5 = '';
        $this->lagerOrt = '';
        $this->verkaufshinweise = '';
        $this->abnahmeInfo = '';
        $this->produktBeschreibung = '';
        $this->produktEigenschaften = '';
        $this->produktVorteile = '';
        $this->produktNutzen = '';
        $this->produktAnwendungen = '';
        $this->produktBesonderheit = '';
        $this->musterbuch = '';
        $this->zurAufnahmeVon = '';
        $this->eigenschaften = '';
        $this->preis = '';
        $this->uuid = Uuid::uuid4();
        $this->offsetFix = '';
        $this->offsetVar = '';
        $this->digitalFix = '';
        $this->digitalVar = '';
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getInstall()
    {
        return $this->install;
    }

    /**
     * @param int $install
     */
    public function setInstall($install)
    {
        $this->install = $install;
    }

    /**
     * @return string
     */
    public function getArtNr()
    {
        return $this->artNr;
    }

    /**
     * @param string $artNr
     */
    public function setArtNr($artNr)
    {
        $this->artNr = $artNr;
    }

    /**
     * @return string
     */
    public function getAuslauf()
    {
        return $this->auslauf;
    }

    /**
     * @param string $auslauf
     */
    public function setAuslauf($auslauf)
    {
        $this->auslauf = $auslauf;
    }

    /**
     * @return string
     */
    public function getDescription1()
    {
        return $this->description1;
    }

    /**
     * @param string $description1
     */
    public function setDescription1($description1)
    {
        $this->description1 = $description1;
    }

    /**
     * @return string
     */
    public function getDescription2()
    {
        return $this->description2;
    }

    /**
     * @param string $description2
     */
    public function setDescription2($description2)
    {
        $this->description2 = $description2;
    }

    /**
     * @return string
     */
    public function getGrammatur()
    {
        return $this->grammatur;
    }

    /**
     * @param string $grammatur
     */
    public function setGrammatur($grammatur)
    {
        $this->grammatur = $grammatur;
    }

    /**
     * @return string
     */
    public function getStaerke()
    {
        return $this->staerke;
    }

    /**
     * @param string $staerke
     */
    public function setStaerke($staerke)
    {
        $this->staerke = $staerke;
    }

    /**
     * @return string
     */
    public function getBreite()
    {
        return $this->breite;
    }

    /**
     * @param string $breite
     */
    public function setBreite($breite)
    {
        $this->breite = $breite;
    }

    /**
     * @return string
     */
    public function getHoehe()
    {
        return $this->hoehe;
    }

    /**
     * @param string $hoehe
     */
    public function setHoehe($hoehe)
    {
        $this->hoehe = $hoehe;
    }

    /**
     * @return string
     */
    public function getLaenge()
    {
        return $this->laenge;
    }

    /**
     * @param string $laenge
     */
    public function setLaenge($laenge)
    {
        $this->laenge = $laenge;
    }

    /**
     * @return string
     */
    public function getRollenLaenge()
    {
        return $this->rollenLaenge;
    }

    /**
     * @param string $rollenLaenge
     */
    public function setRollenLaenge($rollenLaenge)
    {
        $this->rollenLaenge = $rollenLaenge;
    }

    /**
     * @return int
     */
    public function getGewicht()
    {
        return $this->gewicht;
    }

    /**
     * @param int $gewicht
     */
    public function setGewicht($gewicht)
    {
        $this->gewicht = $gewicht;
    }

    /**
     * @return string
     */
    public function getPapierAusruestung()
    {
        return $this->papierAusruestung;
    }

    /**
     * @param string $papierAusruestung
     */
    public function setPapierAusruestung($papierAusruestung)
    {
        $this->papierAusruestung = $papierAusruestung;
    }

    /**
     * @return string
     */
    public function getFarbNummer()
    {
        return $this->farbNummer;
    }

    /**
     * @param string $farbNummer
     */
    public function setFarbNummer($farbNummer)
    {
        $this->farbNummer = $farbNummer;
    }

    /**
     * @return string
     */
    public function getFarbBezeichnung()
    {
        return $this->farbBezeichnung;
    }

    /**
     * @param string $farbBezeichnung
     */
    public function setFarbBezeichnung($farbBezeichnung)
    {
        $this->farbBezeichnung = $farbBezeichnung;
    }

    /**
     * @return string
     */
    public function getHuelsenDurchmesser()
    {
        return $this->huelsenDurchmesser;
    }

    /**
     * @param string $huelsenDurchmesser
     */
    public function setHuelsenDurchmesser($huelsenDurchmesser)
    {
        $this->huelsenDurchmesser = $huelsenDurchmesser;
    }

    /**
     * @return string
     */
    public function getKleber()
    {
        return $this->kleber;
    }

    /**
     * @param string $kleber
     */
    public function setKleber($kleber)
    {
        $this->kleber = $kleber;
    }

    /**
     * @return string
     */
    public function getAbdeckPapier()
    {
        return $this->abdeckPapier;
    }

    /**
     * @param string $abdeckPapier
     */
    public function setAbdeckPapier($abdeckPapier)
    {
        $this->abdeckPapier = $abdeckPapier;
    }

    /**
     * @return string
     */
    public function getLaufRichtung()
    {
        return $this->laufRichtung;
    }

    /**
     * @param string $laufRichtung
     */
    public function setLaufRichtung($laufRichtung)
    {
        $this->laufRichtung = $laufRichtung;
    }

    /**
     * @return int
     */
    public function getMengenAngabe()
    {
        return $this->mengenAngabe;
    }

    /**
     * @param int $mengenAngabe
     */
    public function setMengenAngabe($mengenAngabe)
    {
        $this->mengenAngabe = $mengenAngabe;
    }

    /**
     * @return int
     */
    public function getMengenangabePalette()
    {
        return $this->mengenangabePalette;
    }

    /**
     * @param int $mengenangabePalette
     */
    public function setMengenangabePalette($mengenangabePalette)
    {
        $this->mengenangabePalette = $mengenangabePalette;
    }

    /**
     * @return string
     */
    public function getInhalt()
    {
        return $this->inhalt;
    }

    /**
     * @param string $inhalt
     */
    public function setInhalt($inhalt)
    {
        $this->inhalt = $inhalt;
    }

    /**
     * @return string
     */
    public function getEtikettenJeBlatt()
    {
        return $this->etikettenJeBlatt;
    }

    /**
     * @param string $etikettenJeBlatt
     */
    public function setEtikettenJeBlatt($etikettenJeBlatt)
    {
        $this->etikettenJeBlatt = $etikettenJeBlatt;
    }

    /**
     * @return string
     */
    public function getMengenEinheit()
    {
        return $this->mengenEinheit;
    }

    /**
     * @param string $mengenEinheit
     */
    public function setMengenEinheit($mengenEinheit)
    {
        $this->mengenEinheit = $mengenEinheit;
    }

    /**
     * @return int
     */
    public function getStaffelmenge1()
    {
        return $this->staffelmenge1;
    }

    /**
     * @param int $staffelmenge1
     */
    public function setStaffelmenge1($staffelmenge1)
    {
        $this->staffelmenge1 = $staffelmenge1;
    }

    /**
     * @return float
     */
    public function getStaffelpreis1()
    {
        return $this->staffelpreis1;
    }

    /**
     * @param float $staffelpreis1
     */
    public function setStaffelpreis1($staffelpreis1)
    {
        $this->staffelpreis1 = $staffelpreis1;
    }

    /**
     * @return int
     */
    public function getStaffelmenge2()
    {
        return $this->staffelmenge2;
    }

    /**
     * @param int $staffelmenge2
     */
    public function setStaffelmenge2($staffelmenge2)
    {
        $this->staffelmenge2 = $staffelmenge2;
    }

    /**
     * @return float
     */
    public function getStaffelpreis2()
    {
        return $this->staffelpreis2;
    }

    /**
     * @param float $staffelpreis2
     */
    public function setStaffelpreis2($staffelpreis2)
    {
        $this->staffelpreis2 = $staffelpreis2;
    }

    /**
     * @return int
     */
    public function getStaffelmenge3()
    {
        return $this->staffelmenge3;
    }

    /**
     * @param int $staffelmenge3
     */
    public function setStaffelmenge3($staffelmenge3)
    {
        $this->staffelmenge3 = $staffelmenge3;
    }

    /**
     * @return float
     */
    public function getStaffelpreis3()
    {
        return $this->staffelpreis3;
    }

    /**
     * @param float $staffelpreis3
     */
    public function setStaffelpreis3($staffelpreis3)
    {
        $this->staffelpreis3 = $staffelpreis3;
    }

    /**
     * @return int
     */
    public function getStaffelmenge4()
    {
        return $this->staffelmenge4;
    }

    /**
     * @param int $staffelmenge4
     */
    public function setStaffelmenge4($staffelmenge4)
    {
        $this->staffelmenge4 = $staffelmenge4;
    }

    /**
     * @return float
     */
    public function getStaffelpreis4()
    {
        return $this->staffelpreis4;
    }

    /**
     * @param float $staffelpreis4
     */
    public function setStaffelpreis4($staffelpreis4)
    {
        $this->staffelpreis4 = $staffelpreis4;
    }

    /**
     * @return int
     */
    public function getStaffelmenge5()
    {
        return $this->staffelmenge5;
    }

    /**
     * @param int $staffelmenge5
     */
    public function setStaffelmenge5($staffelmenge5)
    {
        $this->staffelmenge5 = $staffelmenge5;
    }

    /**
     * @return float
     */
    public function getStaffelpreis5()
    {
        return $this->staffelpreis5;
    }

    /**
     * @param float $staffelpreis5
     */
    public function setStaffelpreis5($staffelpreis5)
    {
        $this->staffelpreis5 = $staffelpreis5;
    }

    /**
     * @return string
     */
    public function getLagerOrt()
    {
        return $this->lagerOrt;
    }

    /**
     * @param string $lagerOrt
     */
    public function setLagerOrt($lagerOrt)
    {
        $this->lagerOrt = $lagerOrt;
    }

    /**
     * @return string
     */
    public function getVerkaufshinweise()
    {
        return $this->verkaufshinweise;
    }

    /**
     * @param string $verkaufshinweise
     */
    public function setVerkaufshinweise($verkaufshinweise)
    {
        $this->verkaufshinweise = $verkaufshinweise;
    }

    /**
     * @return string
     */
    public function getAbnahmeInfo()
    {
        return $this->abnahmeInfo;
    }

    /**
     * @param string $abnahmeInfo
     */
    public function setAbnahmeInfo($abnahmeInfo)
    {
        $this->abnahmeInfo = $abnahmeInfo;
    }

    /**
     * @return string
     */
    public function getProduktBeschreibung()
    {
        return $this->produktBeschreibung;
    }

    /**
     * @param string $produktBeschreibung
     */
    public function setProduktBeschreibung($produktBeschreibung)
    {
        $this->produktBeschreibung = $produktBeschreibung;
    }

    /**
     * @return string
     */
    public function getProduktEigenschaften()
    {
        return $this->produktEigenschaften;
    }

    /**
     * @param string $produktEigenschaften
     */
    public function setProduktEigenschaften($produktEigenschaften)
    {
        $this->produktEigenschaften = $produktEigenschaften;
    }

    /**
     * @return string
     */
    public function getProduktVorteile()
    {
        return $this->produktVorteile;
    }

    /**
     * @param string $produktVorteile
     */
    public function setProduktVorteile($produktVorteile)
    {
        $this->produktVorteile = $produktVorteile;
    }

    /**
     * @return string
     */
    public function getProduktNutzen()
    {
        return $this->produktNutzen;
    }

    /**
     * @param string $produktNutzen
     */
    public function setProduktNutzen($produktNutzen)
    {
        $this->produktNutzen = $produktNutzen;
    }

    /**
     * @return string
     */
    public function getProduktAnwendungen()
    {
        return $this->produktAnwendungen;
    }

    /**
     * @param string $produktAnwendungen
     */
    public function setProduktAnwendungen($produktAnwendungen)
    {
        $this->produktAnwendungen = $produktAnwendungen;
    }

    /**
     * @return string
     */
    public function getProduktBesonderheit()
    {
        return $this->produktBesonderheit;
    }

    /**
     * @param string $produktBesonderheit
     */
    public function setProduktBesonderheit($produktBesonderheit)
    {
        $this->produktBesonderheit = $produktBesonderheit;
    }

    /**
     * @return string
     */
    public function getMusterbuch()
    {
        return $this->musterbuch;
    }

    /**
     * @param string $musterbuch
     */
    public function setMusterbuch($musterbuch)
    {
        $this->musterbuch = $musterbuch;
    }

    /**
     * @return string
     */
    public function getZurAufnahmeVon()
    {
        return $this->zurAufnahmeVon;
    }

    /**
     * @param string $zurAufnahmeVon
     */
    public function setZurAufnahmeVon($zurAufnahmeVon)
    {
        $this->zurAufnahmeVon = $zurAufnahmeVon;
    }

    /**
     * @return string
     */
    public function getEigenschaften()
    {
        return $this->eigenschaften;
    }

    /**
     * @param string $eigenschaften
     */
    public function setEigenschaften($eigenschaften)
    {
        $this->eigenschaften = $eigenschaften;
    }

    /**
     * @return float
     */
    public function getPreis()
    {
        return $this->preis;
    }

    /**
     * @param float $preis
     */
    public function setPreis($preis)
    {
        $this->preis = $preis;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return float
     */
    public function getOffsetFix()
    {
        return $this->offsetFix;
    }

    /**
     * @param float $offsetFix
     */
    public function setOffsetFix($offsetFix)
    {
        $this->offsetFix = $offsetFix;
    }

    /**
     * @return float
     */
    public function getOffsetVar()
    {
        return $this->offsetVar;
    }

    /**
     * @param float $offsetVar
     */
    public function setOffsetVar($offsetVar)
    {
        $this->offsetVar = $offsetVar;
    }

    /**
     * @return float
     */
    public function getDigitalFix()
    {
        return $this->digitalFix;
    }

    /**
     * @param float $digitalFix
     */
    public function setDigitalFix($digitalFix)
    {
        $this->digitalFix = $digitalFix;
    }

    /**
     * @return float
     */
    public function getDigitalVar()
    {
        return $this->digitalVar;
    }

    /**
     * @param float $digitalVar
     */
    public function setDigitalVar($digitalVar)
    {
        $this->digitalVar = $digitalVar;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp1()
    {
        return $this->papierTyp1;
    }

    /**
     * @param mixed $papierTyp1
     */
    public function setPapierTyp1($papierTyp1)
    {
        $this->papierTyp1 = $papierTyp1;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp2()
    {
        return $this->papierTyp2;
    }

    /**
     * @param mixed $papierTyp2
     */
    public function setPapierTyp2($papierTyp2)
    {
        $this->papierTyp2 = $papierTyp2;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp3()
    {
        return $this->papierTyp3;
    }

    /**
     * @param mixed $papierTyp3
     */
    public function setPapierTyp3($papierTyp3)
    {
        $this->papierTyp3 = $papierTyp3;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp4()
    {
        return $this->papierTyp4;
    }

    /**
     * @param mixed $papierTyp4
     */
    public function setPapierTyp4($papierTyp4)
    {
        $this->papierTyp4 = $papierTyp4;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp5()
    {
        return $this->papierTyp5;
    }

    /**
     * @param mixed $papierTyp5
     */
    public function setPapierTyp5($papierTyp5)
    {
        $this->papierTyp5 = $papierTyp5;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp6()
    {
        return $this->papierTyp6;
    }

    /**
     * @param mixed $papierTyp6
     */
    public function setPapierTyp6($papierTyp6)
    {
        $this->papierTyp6 = $papierTyp6;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp7()
    {
        return $this->papierTyp7;
    }

    /**
     * @param mixed $papierTyp7
     */
    public function setPapierTyp7($papierTyp7)
    {
        $this->papierTyp7 = $papierTyp7;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp8()
    {
        return $this->papierTyp8;
    }

    /**
     * @param mixed $papierTyp8
     */
    public function setPapierTyp8($papierTyp8)
    {
        $this->papierTyp8 = $papierTyp8;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp9()
    {
        return $this->papierTyp9;
    }

    /**
     * @param mixed $papierTyp9
     */
    public function setPapierTyp9($papierTyp9)
    {
        $this->papierTyp9 = $papierTyp9;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp10()
    {
        return $this->papierTyp10;
    }

    /**
     * @param mixed $papierTyp10
     */
    public function setPapierTyp10($papierTyp10)
    {
        $this->papierTyp10 = $papierTyp10;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp11()
    {
        return $this->papierTyp11;
    }

    /**
     * @param mixed $papierTyp11
     */
    public function setPapierTyp11($papierTyp11)
    {
        $this->papierTyp11 = $papierTyp11;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp12()
    {
        return $this->papierTyp12;
    }

    /**
     * @param mixed $papierTyp12
     */
    public function setPapierTyp12($papierTyp12)
    {
        $this->papierTyp12 = $papierTyp12;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp13()
    {
        return $this->papierTyp13;
    }

    /**
     * @param mixed $papierTyp13
     */
    public function setPapierTyp13($papierTyp13)
    {
        $this->papierTyp13 = $papierTyp13;
    }

    /**
     * @return mixed
     */
    public function getUmschlagen()
    {
        return $this->umschlagen;
    }

    /**
     * @param mixed $umschlagen
     */
    public function setUmschlagen($umschlagen)
    {
        $this->umschlagen = $umschlagen;
    }

    /**
     * @return mixed
     */
    public function getHappy()
    {
        return $this->happy;
    }

    /**
     * @param mixed $happy
     */
    public function setHappy($happy)
    {
        $this->happy = $happy;
    }

    /**
     * @return mixed
     */
    public function getEq()
    {
        return $this->eq;
    }

    /**
     * @param mixed $eq
     */
    public function setEq($eq)
    {
        $this->eq = $eq;
    }

    /**
     * @return mixed
     */
    public function getSense()
    {
        return $this->sense;
    }

    /**
     * @param mixed $sense
     */
    public function setSense($sense)
    {
        $this->sense = $sense;
    }

    /**
     * @return mixed
     */
    public function getSky()
    {
        return $this->sky;
    }

    /**
     * @param mixed $sky
     */
    public function setSky($sky)
    {
        $this->sky = $sky;
    }

    /**
     * @return mixed
     */
    public function getGlam()
    {
        return $this->glam;
    }

    /**
     * @param mixed $glam
     */
    public function setGlam($glam)
    {
        $this->glam = $glam;
    }

    /**
     * @return mixed
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @param mixed $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * @return mixed
     */
    public function getPapierTyp14()
    {
        return $this->papierTyp14;
    }

    /**
     * @param mixed $papierTyp14
     */
    public function setPapierTyp14($papierTyp14)
    {
        $this->papierTyp14 = $papierTyp14;
    }

    /**
     * @return mixed
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param mixed $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }

    public function getSammelform()
    {
        if ($this->getHappy()) {
            return 2;
        }
        if ($this->getEq()) {
            return 3;
        }
        if ($this->getSense()) {
            return 4;
        }
        if ($this->getSky()) {
            return 5;
        }
        if ($this->getGlam()) {
            return 6;
        }
        if ($this->getPost()) {
            return 7;
        }
        return 1;
    }
}
