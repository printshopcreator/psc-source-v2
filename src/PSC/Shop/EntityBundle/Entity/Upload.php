<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * Upload
 *
 * @ORM\Table(name="upload")
 * @ORM\Entity(repositoryClass="PSC\Shop\EntityBundle\Repository\UploadRepository")
 *
 * @package    PSC\Shop\Entity
 * @subpackage Entitys
 */
class Upload
{
    public $file;
/**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $uid;
/**
     * @var Orderpos
     *
     * @ORM\ManyToOne(targetEntity="Orderpos")
     * @ORM\JoinColumn(name="orderpos_id", referencedColumnName="id")
     */
    protected $orderPos;
/**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
/**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;
/**
     * @var string
     *
     * @ORM\Column(name="typ", type="string", length=255, nullable=true)
     */
    private $typ;
/**
     * @var string
     *
     * @ORM\Column(name="pathpreview", type="string", length=255, nullable=true)
     */
    private $pathpreview;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    private $created;
/**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime", nullable=false)
     */
    private $updated;
/**
     * @var string
     *
     * @ORM\Column(name="chunktitle", type="string", length=255, nullable=true)
     */
    private $chunktitle;
/**
     * @var string
     *
     * @ORM\Column(name="uuid", type="string", length=40, nullable=false)
     */
    private $uuid;
/**
     * @var integer
     *
     * @ORM\Column(name="preflight_status", type="integer", nullable=true)
     */
    private $preflightStatus;
/**
     * @var string
     *
     * @ORM\Column(name="preflight_errors", type="text", nullable=true)
     */
    private $preflightErrors;
/**
     * Export Product
     *
     * @var boolean
     *
     * @ORM\Column(name="export", type="boolean")
     */
    protected $export;
/**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param int $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return 'uploads/' . $this->getOrderPos()->getShop()->getUuid() . '/article/' . $this->path;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getTyp()
    {
        return $this->typ;
    }

    /**
     * @param string $typ
     */
    public function setTyp($typ)
    {
        $this->typ = $typ;
    }

    /**
     * @return string
     */
    public function getPathpreview()
    {
        return $this->pathpreview;
    }

    /**
     * @param string $pathpreview
     */
    public function setPathpreview($pathpreview)
    {
        $this->pathpreview = $pathpreview;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return string
     */
    public function getChunktitle()
    {
        return $this->chunktitle;
    }

    /**
     * @param string $chunktitle
     */
    public function setChunktitle($chunktitle)
    {
        $this->chunktitle = $chunktitle;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return int
     */
    public function getPreflightStatus()
    {
        return $this->preflightStatus;
    }

    /**
     * @param int $preflightStatus
     */
    public function setPreflightStatus($preflightStatus)
    {
        $this->preflightStatus = $preflightStatus;
    }

    /**
     * @return string
     */
    public function getPreflightErrors()
    {
        return $this->preflightErrors;
    }

    /**
     * @param string $preflightErrors
     */
    public function setPreflightErrors($preflightErrors)
    {
        $this->preflightErrors = $preflightErrors;
    }

    /**
     * @return Orderpos
     */
    public function getOrderPos()
    {
        return $this->orderPos;
    }

    /**
     * @param Orderpos $orderPos
     */
    public function setOrderPos($orderPos)
    {
        $this->orderPos = $orderPos;
    }

    /**
     * @return boolean
     */
    public function isExport()
    {
        return $this->export;
    }

    /**
     * @param boolean $export
     */
    public function setExport($export)
    {
        $this->export = $export;
    }
}
