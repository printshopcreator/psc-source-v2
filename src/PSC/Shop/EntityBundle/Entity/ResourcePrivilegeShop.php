<?php

namespace PSC\Shop\EntityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Resourceprivilegeshop
 *
 * @ORM\Table(name="resourceprivilegeshop")
 * @ORM\Entity
 */
class ResourcePrivilegeShop
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * Shop zu welcher das Product gehört
     *
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="Shop")
     * @ORM\JoinColumn(name="shop_id", referencedColumnName="id")
     */
    protected $shop;

    /**
     * @var integer
     *
     * @ORM\Column(name="resourceprivilege_id", type="integer", nullable=false)
     */
    protected $resourceprivilege;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param int $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return int
     */
    public function getResourceprivilege()
    {
        return $this->resourceprivilege;
    }

    /**
     * @param int $resourceprivilege
     */
    public function setResourceprivilege($resourceprivilege)
    {
        $this->resourceprivilege = $resourceprivilege;
    }
}
