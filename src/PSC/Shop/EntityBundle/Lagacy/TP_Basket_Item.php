<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

if (!class_exists("TP_Basket_Item")) {
    class TP_Basket_Item
    {
        private $_calc_values = array();
        private $_options = array();
        private $_additionalInfos = array();
        private $_upload_mode = "";
        private $_count = 0;
        private $_netto = 0;
        private $_steuer = 0;
        private $_brutto = 0;
        private $_rabatte = array();
        private $_layouter_id = "";
        private $_article = array();
        private $_inLoad = "";
        private $_weight = 0;
        private $_weight_single = 0;
        private $_ref = false;
        private $_template_print_id = false;

        public function getRef() {
            return $this->_ref;
        }

        public function setRef($var) {
            $this->_ref = $var;
        }

        public function getTemplatePrintId() {
            return $this->_template_print_id;
        }

        public function getNewTemplatePrintId() {
            $this->_template_print_id = md5(TP_Util::uuid());
            return $this->_template_print_id;
        }

        public function setTemplatePrintId($file) {
            $this->_template_print_id = $file;
        }

        public function getWeight() {
            return $this->_weight;
        }

        public function setWeight($var) {
            $this->_weight = round($var, 0);
        }

        public function getWeightSingle()
        {
            return $this->_weight_single;
        }

        public function setWeightSingle($weight_single)
        {
            $this->_weight_single = round($weight_single, 2);
        }

        public function getInLoad()
        {
            return $this->_inLoad;
        }

        public function setInLoad($var)
        {
            $this->_inLoad = $var;
        }

        public function setCalcValues($calcValues)
        {
            $this->_calc_values = $calcValues;
        }

        public function getCalcValues()
        {
            return $this->_calc_values;
        }

        public function setOptions($options)
        {
            $this->_options = $options;
        }

        public function getOptions()
        {
            return $this->_options;
        }

        public function setUploadMode($var)
        {
            $this->_upload_mode = $var;
        }

        public function getUploadMode()
        {
            return $this->_upload_mode;
        }

        /**
         * @return int
         */
        public function getCount()
        {
            return $this->_count;
        }

        /**
         * @param int $count
         */
        public function setCount($count)
        {
            $this->_count = $count;
        }

        /**
         * @return int
         */
        public function getNetto()
        {
            return $this->_netto;
        }

        /**
         * @param int $netto
         */
        public function setNetto($netto)
        {
            $this->_netto = $netto;
        }

        /**
         * @return int
         */
        public function getSteuer()
        {
            return $this->_steuer;
        }

        /**
         * @param int $steuer
         */
        public function setSteuer($steuer)
        {
            $this->_steuer = $steuer;
        }

        /**
         * @return int
         */
        public function getBrutto()
        {
            return $this->_brutto;
        }

        /**
         * @param int $brutto
         */
        public function setBrutto($brutto)
        {
            $this->_brutto = $brutto;
        }

        /**
         * @return string
         */
        public function getLayouterId()
        {
            return $this->_layouter_id;
        }

        /**
         * @param string $layouter_id
         */
        public function setLayouterId($layouter_id)
        {
            $this->_layouter_id = $layouter_id;
        }

        /**
         * @return array
         */
        public function getAdditionalInfos(): array
        {
            return $this->_additionalInfos;
        }

        /**
         * @param array $additionalInfos
         */
        public function setAdditionalInfos(array $additionalInfos): void
        {
            $this->_additionalInfos = $additionalInfos;
        }

        /**
         * @return array
         */
        public function getRabatte(): array
        {
            return $this->_rabatte;
        }

        /**
         * @param array $rabatte
         */
        public function setRabatte(array $rabatte): void
        {
            $this->_rabatte = $rabatte;
        }

        /**
         * @return array
         */
        public function getArticle(): array
        {
            return $this->_article;
        }

        /**
         * @param array $article
         */
        public function setArticle(array $article): void
        {
            $this->_article = $article;
        }
    }

}
