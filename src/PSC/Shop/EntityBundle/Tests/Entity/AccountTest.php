<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Tests\Entity;

use PSC\Shop\EntityBundle\Entity\Account;

/**
 * Account Entity Test
 *
 * @package    PSC\Shop\Entity
 * @subpackage Tests
 */
class AccountTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing Title
     *
     * @return void
     */
    public function testTitle()
    {
        $account = $this->getAccount();
        $this->assertNull($account->getTitle());
        $account->setTitle('tony');
        $this->assertEquals('tony', $account->getTitle());
    }

    /**
     * Testing Id
     *
     * @return void
     */
    public function testId()
    {
        $account = $this->getAccount();
        $this->assertNull($account->getId());
        $account->setId(12);
        $this->assertEquals(12, $account->getId());
    }

    /**
     * Testing Locked
     *
     * @return void
     */
    public function testLocked()
    {
        $account = $this->getAccount();
        $this->assertNull($account->getId());
        $account->setLock(false);
        $account->setMasterLock(false);
        $this->assertFalse($account->isLocked());
        $account->setLock(true);
        $this->assertTrue($account->isLocked());
        $account->setLock(false);
        $account->setMasterLock(true);
        $this->assertTrue($account->isLocked());
    }

    /**
     * Testing Title
     *
     * @return void
     */
    public function testParent()
    {
        $account = $this->getAccount();
        $this->assertFalse($account->getParent());
    }

    /**
     * Testing Childrens
     *
     * @return void
     */
    public function testChildrens()
    {
        $account = $this->getAccount();
        $accountParent = $this->getAccount();
        $accountParent1 = $this->getAccount();
        $this->assertInstanceOf('\Doctrine\Common\Collections\ArrayCollection', $account->getChildren());
        $account->addChild($accountParent);
        $account->addChild($accountParent1);
        $this->assertEquals(2, count($account->getChildren()));
    }

    /**
     * Get Mock
     *
     * @return Account
     */
    protected function getAccount()
    {
        return $this->getMockForAbstractClass('PSC\Shop\EntityBundle\Tests\TestAccount');
    }
}
