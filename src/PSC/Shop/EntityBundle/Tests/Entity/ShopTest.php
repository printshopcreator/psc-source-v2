<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Tests\Entity;

use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * Shop Entity Test
 *
 * @package    PSC\Shop\Entity
 * @subpackage Tests
 */
class ShopTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing Model
     *
     * @return void
     */
    public function testModel()
    {
        $account = $this->getShop();
        $this->assertNull($account->getApiKey());
        $this->assertNull($account->getTitle());
        $account->setApiKey("testapikey");
        $account->setTitle("Test Shop");
        $this->assertEquals("testapikey", $account->getApiKey());
        $this->assertEquals("Test Shop", $account->getTitle());
    }

    /**
     * Testing ApiKey
     *
     * @return void
     */
    public function testApiKey()
    {
        $account = $this->getShop();
        $this->assertNull($account->getApiKey());
        $account->setApiKey("testapikey");
        $this->assertEquals("testapikey", $account->getApiKey());
    }

    /**
     * Testing __toString
     *
     * @return void
     */
    public function testToString()
    {
        $account = $this->getShop();
        $this->assertNull($account->__toString());
        $account->setApiKey("testapikey");
        $this->assertEquals("testapikey", $account->__toString());
    }

    /**
     * Get Mock
     *
     * @return Shop
     */
    protected function getShop()
    {
        return $this->getMockForAbstractClass('PSC\Shop\EntityBundle\Entity\Shop');
    }
}
