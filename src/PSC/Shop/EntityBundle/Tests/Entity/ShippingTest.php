<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Tests\Entity;

use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * Shipping Entity Test
 *
 * @package    PSC\Shop\Entity
 * @subpackage Tests
 */
class ShippingTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing Model
     *
     * @return void
     */
    public function testModel()
    {
        $account = $this->getShipping();
        $account->setTitle("DHL");
        $account->setPos(1);
        $account->setKosten(10.34);
        $this->assertEquals("DHL", $account->getTitle());
        $this->assertEquals(1, $account->getPos());
        $this->assertEquals(10.34, $account->getKosten());
        $account->setKosten("10,34");
        $this->assertEquals(10.34, $account->getKosten());
        $shop = $this->getShop();
        $shop->setTitle("Test Shop");
        $account->setShop($shop);
        $this->assertEquals("Test Shop", $account->getShop()->getTitle());
    }

    /**
     * Get Mock
     *
     * @return Shipping
     */
    protected function getShipping()
    {
        return $this->getMockForAbstractClass('PSC\Shop\EntityBundle\Entity\Shipping');
    }

    /**
     * Get Mock
     *
     * @return Shop
     */
    protected function getShop()
    {
        return $this->getMockForAbstractClass('PSC\Shop\EntityBundle\Entity\Shop');
    }
}
