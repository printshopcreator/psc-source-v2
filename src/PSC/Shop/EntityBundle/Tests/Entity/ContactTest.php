<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Tests\Entity;

use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * Contact Entity Test
 *
 * @package    PSC\Shop\Entity
 * @subpackage Tests
 */
class ContactTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Testing EmailAndPassword
     *
     * @return void
     */
    public function testEmailAndPassword()
    {
        $contact = $this->getContact();
        $this->assertEquals("", $contact->getEmail());
        $this->assertEquals("", $contact->getPassword());
        $contact->setEmail('tony@email.com');
        $contact->setPassword('pass');
        $this->assertEquals('tony@email.com', $contact->getEmail());
        $this->assertEquals('pass', $contact->getPassword());
    }

    /**
     * Testing Id
     *
     * @return void
     */
    public function testId()
    {
        $contact = $this->getContact();
        $this->assertNull($contact->getId());
        $contact->setId(12);
        $this->assertEquals(12, $contact->getId());
    }

    /**
     * Get Mock
     *
     * @return Contact
     */
    protected function getContact()
    {
        return $this->getMockForAbstractClass('PSC\Shop\EntityBundle\Tests\TestContact');
    }
}
