<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Tests;

use PSC\Shop\EntityBundle\Entity\Account;

/**
 * Test AccountClass
 *
 * @package    PSC\Shop\Entity
 * @subpackage Tests
 */
class TestAccount extends Account
{
    /**
     * Sets the id
     *
     * @param int $uid
     *
     * @return int
     */
    public function setId($uid)
    {
        $this->uid = $uid;
    }
}
