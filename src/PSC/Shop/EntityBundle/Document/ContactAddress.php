<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document
 */
class ContactAddress
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var string $kundenNr;
     *
     * @Field(type="string")
     */
    protected $kundenNr;
/**
     * @var string $homepage;
     *
     * @Field(type="string")
     */
    protected $homepage;
/**
     * @return string
     */
    public function getHomepage()
    {
        return $this->homepage;
    }

    /**
     * @param string $homepage
     */
    public function setHomepage($homepage)
    {
        $this->homepage = $homepage;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
