<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;

/**
 * @Document
 */
class Country
{
    /**
     * @Id
     */
    protected $id;
/**
     * @var string $shop
     *
     * @Field(type="string")
     */
    protected $shop;
/**
     * @var string $code
     *
     * @Field(type="string")
     */
    protected $code;
/**
     * @var boolean $withTaxWithoutUstNr
     *
     * @Field(type="bool")
     */
    protected $withTaxWithoutUstNr;
/**
     * @var boolean $withTaxWithUstNr
     *
     * @Field(type="bool")
     */
    protected $withTaxWithUstNr;
/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param string $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return bool
     */
    public function isWithTaxWithoutUstNr()
    {
        return $this->withTaxWithoutUstNr;
    }

    /**
     * @param bool $withTaxWithoutUstNr
     */
    public function setWithTaxWithoutUstNr($withTaxWithoutUstNr)
    {
        $this->withTaxWithoutUstNr = $withTaxWithoutUstNr;
    }

    /**
     * @return bool
     */
    public function isWithTaxWithUstNr()
    {
        return $this->withTaxWithUstNr;
    }

    /**
     * @param bool $withTaxWithUstNr
     */
    public function setWithTaxWithUstNr($withTaxWithUstNr)
    {
        $this->withTaxWithUstNr = $withTaxWithUstNr;
    }
}
