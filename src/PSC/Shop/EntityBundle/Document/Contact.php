<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document
 */
class Contact
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var string $kundenNr;
     *
     * @Field(type="string")
     */
    protected $kundenNr;
/**
     * @var string $defaultPayment;
     *
     * @Field(type="int")
     */
    protected $defaultPayment;
/**
     * @var string $defaultShipment;
     *
     * @Field(type="int")
     */
    protected $defaultShipment;
/**
     * @var string $calcValue1;
     *
     * @Field(type="string")
     */
    protected $calcValue1;
/**
     * @var string $calcValue2;
     *
     * @Field(type="string")
     */
    protected $calcValue2;
/**
     * @var string $custom1;
     *
     * @Field(type="string")
     */
    protected $custom1;
/**
     * @var string $custom2;
     *
     * @Field(type="string")
     */
    protected $custom2;
/**
     * @var string $custom3;
     *
     * @Field(type="string")
     */
    protected $custom3;
/**
     * @var string $custom4;
     *
     * @Field(type="string")
     */
    protected $custom4;
/**
     * @var string $custom5;
     *
     * @Field(type="string")
     */
    protected $custom5;
/**
     * @var string $custom6;
     *
     * @Field(type="string")
     */
    protected $custom6;
/**
     * @var string $custom7;
     *
     * @Field(type="string")
     */
    protected $custom7;
/**
     * @var string $custom8;
     *
     * @Field(type="string")
     */
    protected $custom8;
/**
     * @var string $custom9;
     *
     * @Field(type="string")
     */
    protected $custom9;
/**
     * @var string $custom10;
     *
     * @Field(type="string")
     */
    protected $custom10;
/**
     * @var string $custom11;
     *
     * @Field(type="string")
     */
    protected $custom11;
/**
     * @var string $custom12;
     *
     * @Field(type="string")
     */
    protected $custom12;
/**
     * @var string $custom13;
     *
     * @Field(type="string")
     */
    protected $custom13;
/**
     * @var string $custom14;
     *
     * @Field(type="string")
     */
    protected $custom14;
/**
     * @var string $custom15;
     *
     * @Field(type="string")
     */
    protected $custom15;
/**
     * @var string $custom16;
     *
     * @Field(type="string")
     */
    protected $custom16;
/**
     * @var string $custom17;
     *
     * @Field(type="string")
     */
    protected $custom17;
/**
     * @var string $custom18;
     *
     * @Field(type="string")
     */
    protected $custom18;
/**
     * @var string $custom19;
     *
     * @Field(type="string")
     */
    protected $custom19;
/**
     * @var string $custom20;
     *
     * @Field(type="string")
     */
    protected $custom20;
/**
     * @var string $custom21;
     *
     * @Field(type="string")
     */
    protected $custom21;
/**
     * @var string $custom22;
     *
     * @Field(type="string")
     */
    protected $custom22;
/**
     * @var string $custom23;
     *
     * @Field(type="string")
     */
    protected $custom23;
/**
     * @var string $custom24;
     *
     * @Field(type="string")
     */
    protected $custom24;
/**
     * @var boolean $showOtherOrders;
     *
     * @Field(type="bool")
     */
    protected $showOtherOrders;
/**
     * @var integer $showOtherOrdersAccountFilter;
     *
     * @Field(type="int")
     */
    protected $showOtherOrdersAccountFilter;
/**
     * @var \PSC\Shop\EntityBundle\Entity\Account $showOtherOrdersAccount;
     *
     * @Field(type="int")
     */
    protected $showOtherOrdersAccount;
/**
     * @Field(type="hash")
     */
    protected $pluginSettings = [];
/**
     * @var string $extraSettings;
     *
     * @Field(type="string")
     */
    protected $extraSettings;
/**
     * @var float $priceFactor;
     *
     * @Field(type="float")
     */
    protected $priceFactor;
/**
     * @return bool
     */
    public function isShowOtherOrders()
    {
        return $this->showOtherOrders;
    }

    /**
     * @param bool $showOtherOrders
     */
    public function setShowOtherOrders($showOtherOrders)
    {
        $this->showOtherOrders = $showOtherOrders;
    }

    /**
     * @return int
     */
    public function getShowOtherOrdersAccountFilter()
    {
        return $this->showOtherOrdersAccountFilter;
    }

    /**
     * @param int $showOtherOrdersAccountFilter
     */
    public function setShowOtherOrdersAccountFilter($showOtherOrdersAccountFilter)
    {
        $this->showOtherOrdersAccountFilter = $showOtherOrdersAccountFilter;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    public function setDefaultPayment($payment)
    {
        $this->defaultPayment = $payment;
    }

    public function getDefaultPayment()
    {
        return $this->defaultPayment;
    }

    public function setDefaultShipment($shipment)
    {
        $this->defaultShipment = $shipment;
    }

    public function getDefaultShipment()
    {
        return $this->defaultShipment;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }

    /**
     * @return string
     */
    public function getCalcValue1()
    {
        return $this->calcValue1;
    }

    /**
     * @param string $calcValue1
     */
    public function setCalcValue1($calcValue1)
    {
        $this->calcValue1 = $calcValue1;
    }

    /**
     * @return string
     */
    public function getCalcValue2()
    {
        return $this->calcValue2;
    }

    /**
     * @param string $calcValue2
     */
    public function setCalcValue2($calcValue2)
    {
        $this->calcValue2 = $calcValue2;
    }

    /**
     * @return string
     */
    public function getCustom1()
    {
        return $this->custom1;
    }

    /**
     * @param string $custom1
     */
    public function setCustom1($custom1)
    {
        $this->custom1 = $custom1;
    }

    /**
     * @return string
     */
    public function getCustom2()
    {
        return $this->custom2;
    }

    /**
     * @param string $custom2
     */
    public function setCustom2($custom2)
    {
        $this->custom2 = $custom2;
    }

    /**
     * @return string
     */
    public function getCustom3()
    {
        return $this->custom3;
    }

    /**
     * @param string $custom3
     */
    public function setCustom3($custom3)
    {
        $this->custom3 = $custom3;
    }

    /**
     * @return string
     */
    public function getCustom4()
    {
        return $this->custom4;
    }

    /**
     * @param string $custom4
     */
    public function setCustom4($custom4)
    {
        $this->custom4 = $custom4;
    }

    /**
     * @return string
     */
    public function getCustom5()
    {
        return $this->custom5;
    }

    /**
     * @param string $custom5
     */
    public function setCustom5($custom5)
    {
        $this->custom5 = $custom5;
    }

    /**
     * @return string
     */
    public function getCustom6()
    {
        return $this->custom6;
    }

    /**
     * @param string $custom6
     */
    public function setCustom6($custom6)
    {
        $this->custom6 = $custom6;
    }

    /**
     * @return string
     */
    public function getCustom7()
    {
        return $this->custom7;
    }

    /**
     * @param string $custom7
     */
    public function setCustom7($custom7)
    {
        $this->custom7 = $custom7;
    }

    /**
     * @return string
     */
    public function getCustom8()
    {
        return $this->custom8;
    }

    /**
     * @param string $custom8
     */
    public function setCustom8($custom8)
    {
        $this->custom8 = $custom8;
    }

    /**
     * @return string
     */
    public function getCustom9()
    {
        return $this->custom9;
    }

    /**
     * @param string $custom9
     */
    public function setCustom9($custom9)
    {
        $this->custom9 = $custom9;
    }

    /**
     * @return string
     */
    public function getCustom10()
    {
        return $this->custom10;
    }

    /**
     * @param string $custom10
     */
    public function setCustom10($custom10)
    {
        $this->custom10 = $custom10;
    }

    /**
     * @return string
     */
    public function getCustom11()
    {
        return $this->custom11;
    }

    /**
     * @param string $custom11
     */
    public function setCustom11($custom11)
    {
        $this->custom11 = $custom11;
    }

    /**
     * @return string
     */
    public function getCustom12()
    {
        return $this->custom12;
    }

    /**
     * @param string $custom12
     */
    public function setCustom12($custom12)
    {
        $this->custom12 = $custom12;
    }

    /**
     * @return string
     */
    public function getCustom13()
    {
        return $this->custom13;
    }

    /**
     * @param string $custom13
     */
    public function setCustom13($custom13)
    {
        $this->custom13 = $custom13;
    }

    /**
     * @return string
     */
    public function getCustom14()
    {
        return $this->custom14;
    }

    /**
     * @param string $custom14
     */
    public function setCustom14($custom14)
    {
        $this->custom14 = $custom14;
    }

    /**
     * @return string
     */
    public function getCustom15()
    {
        return $this->custom15;
    }

    /**
     * @param string $custom15
     */
    public function setCustom15($custom15)
    {
        $this->custom15 = $custom15;
    }

    /**
     * @return string
     */
    public function getCustom16()
    {
        return $this->custom16;
    }

    /**
     * @param string $custom16
     */
    public function setCustom16($custom16)
    {
        $this->custom16 = $custom16;
    }

    /**
     * @return string
     */
    public function getCustom17()
    {
        return $this->custom17;
    }

    /**
     * @param string $custom17
     */
    public function setCustom17($custom17)
    {
        $this->custom17 = $custom17;
    }

    /**
     * @return string
     */
    public function getCustom18()
    {
        return $this->custom18;
    }

    /**
     * @param string $custom18
     */
    public function setCustom18($custom18)
    {
        $this->custom18 = $custom18;
    }

    /**
     * @return string
     */
    public function getCustom19()
    {
        return $this->custom19;
    }

    /**
     * @param string $custom19
     */
    public function setCustom19($custom19)
    {
        $this->custom19 = $custom19;
    }

    /**
     * @return string
     */
    public function getCustom20()
    {
        return $this->custom20;
    }

    /**
     * @param string $custom20
     */
    public function setCustom20($custom20)
    {
        $this->custom20 = $custom20;
    }

    /**
     * @return string
     */
    public function getCustom21()
    {
        return $this->custom21;
    }

    /**
     * @param string $custom21
     */
    public function setCustom21($custom21)
    {
        $this->custom21 = $custom21;
    }

    /**
     * @return string
     */
    public function getCustom22()
    {
        return $this->custom22;
    }

    /**
     * @param string $custom22
     */
    public function setCustom22($custom22)
    {
        $this->custom22 = $custom22;
    }

    /**
     * @return string
     */
    public function getCustom23()
    {
        return $this->custom23;
    }

    /**
     * @param string $custom23
     */
    public function setCustom23($custom23)
    {
        $this->custom23 = $custom23;
    }

    /**
     * @return string
     */
    public function getCustom24()
    {
        return $this->custom24;
    }

    /**
     * @param string $custom24
     */
    public function setCustom24($custom24)
    {
        $this->custom24 = $custom24;
    }

    /**
     * @return \PSC\Shop\EntityBundle\Entity\Account
     */
    public function getShowOtherOrdersAccount()
    {
        return $this->showOtherOrdersAccount;
    }

    /**
     * @param \PSC\Shop\EntityBundle\Entity\Account $showOtherOrdersAccount
     */
    public function setShowOtherOrdersAccount($showOtherOrdersAccount)
    {
        $this->showOtherOrdersAccount = $showOtherOrdersAccount;
    }

    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @param $module
     * @param $key
     * @param $value
     */
    public function setPluginSettingModule($module, $key, $value)
    {
        $this->pluginSettings[$module ] [$key] = $value;
    }

    public function getPluginSettingModule($module, $key)
    {
        if (!isset($this->pluginSettings[$module ]) || !isset($this->pluginSettings[$module ] [$key])) {
            return null;
        }

        return $this->pluginSettings[$module ] [$key];
    }

    /**
     * @return string
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param string $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return float
     */
    public function getPriceFactor(): float
    {
        if (!$this->priceFactor) {
            return 1;
        }
        return $this->priceFactor;
    }

    /**
     * @param float $priceFactor
     */
    public function setPriceFactor(float $priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }
}
