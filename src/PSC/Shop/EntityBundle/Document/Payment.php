<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Date;

/**
 * @Document
 */
class Payment
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var string $paymentGateway
     *
     * @Field(type="string")
     */
    protected $paymentGateway;
/**
     * @var array $paymentGatewaySettings
     *
     * @Field(type="hash")
     */
    protected $paymentGatewaySettings;
/**
     * @var array $shippings;
     *
     * @Field(type="hash")
     */
    protected $shippings = [];
/**
     * @var array $pluginSettings;
     *
     * @Field(type="hash")
     */
    protected $pluginSettings = [];
    public function __get($name)
    {
        // TODO: Implement __get() method.
    }

    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @return array
     */
    public function getShippings()
    {
        return $this->shippings;
    }

    /**
     * @param array $shippings
     */
    public function setShippings($shippings)
    {
        $this->shippings = $shippings;
    }

    /**
     * @return string
     */
    public function getPaymentGateway(): string
    {
        return (string)$this->paymentGateway;
    }

    /**
     * @param string $paymentGateway
     */
    public function setPaymentGateway(string $paymentGateway): void
    {
        $this->paymentGateway = $paymentGateway;
    }

    /**
     * @return array
     */
    public function getPaymentGatewaySettings(): array
    {
        return (array)$this->paymentGatewaySettings;
    }

    /**
     * @param array $paymentGatewaySettings
     */
    public function setPaymentGatewaySettings(array $paymentGatewaySettings): void
    {
        $this->paymentGatewaySettings = $paymentGatewaySettings;
    }
}
