<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;

/**
 * @Document
 */
class Order
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var boolean $packageExported;
     *
     * @Field(type="bool")
     */
    protected $packageExported;

    /**
     *
     * @Field(type="bool")
     */
    protected $sendDataToShipping = true;

    /**
     * @return bool
     */
    public function isSendDataToShipping(): bool
    {
        return (bool)$this->sendDataToShipping;
    }

    /**
     * @param bool $sendDataToShipping
     */
    public function setSendDataToShipping(bool $sendDataToShipping): void
    {
        $this->sendDataToShipping = $sendDataToShipping;
    }
/**
     * @var string $externalOrderNumber;
     *
     * @Field(type="string")
     */
    protected $externalOrderNumber;
/**
     * @var string $paymentRef;
     *
     * @Field(type="string")
     */
    protected $paymentRef;
/**
     * @var string $paymentToken;
     *
     * @Field(type="string")
     */
    protected $paymentToken;
/**
     * @Field(type="hash")
     */
    protected $pluginSettings = [];
/**
     * @Field(type="float")
     */
    protected $gutscheinAbzugNetto = 0;
/** @EmbedOne */
    protected $invoiceAddressSaved;
/** @EmbedOne */
    protected $deliveryAddressSaved;
/** @EmbedOne */
    protected $senderAddressSaved;
/**
     * @var boolean $withTax
     *
     * @Field(type="bool")
     */
    protected $withTax;
/**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return boolean
     */
    public function isPackageExported()
    {
        return $this->packageExported;
    }

    /**
     * @param boolean $packageExported
     */
    public function setPackageExported($packageExported)
    {
        $this->packageExported = $packageExported;
    }

    /**
     * @return string
     */
    public function getPaymentRef()
    {
        return $this->paymentRef;
    }

    /**
     * @param string $paymentRef
     */
    public function setPaymentRef($paymentRef)
    {
        $this->paymentRef = $paymentRef;
    }

    /**
     * @return string
     */
    public function getPaymentToken()
    {
        return $this->paymentToken;
    }

    /**
     * @param string $paymentToken
     */
    public function setPaymentToken($paymentToken)
    {
        $this->paymentToken = $paymentToken;
    }

    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @param $module
     * @param $key
     * @param $value
     */
    public function setPluginSettingModule($module, $key, $value)
    {
        $this->pluginSettings[$module ] [$key] = $value;
    }

    public function getPluginSettingModule($module, $key)
    {
        if (!isset($this->pluginSettings[$module ]) || !isset($this->pluginSettings[$module ] [$key])) {
            return null;
        }

        return $this->pluginSettings[$module ] [$key];
    }

    /**
     * @return mixed
     */
    public function getSenderAddressSaved()
    {
        return $this->senderAddressSaved;
    }

    /**
     * @param mixed $senderAddressSaved
     */
    public function setSenderAddressSaved($senderAddressSaved)
    {
        $this->senderAddressSaved = $senderAddressSaved;
    }

    /**
     * @return mixed
     */
    public function getInvoiceAddressSaved()
    {
        return $this->invoiceAddressSaved;
    }

    /**
     * @param mixed $invoiceAddressSaved
     */
    public function setInvoiceAddressSaved($invoiceAddressSaved)
    {
        $this->invoiceAddressSaved = $invoiceAddressSaved;
    }

    /**
     * @return mixed
     */
    public function getDeliveryAddressSaved()
    {
        return $this->deliveryAddressSaved;
    }

    /**
     * @param mixed $deliveryAddressSaved
     */
    public function setDeliveryAddressSaved($deliveryAddressSaved)
    {
        $this->deliveryAddressSaved = $deliveryAddressSaved;
    }

    /**
     * @return bool
     */
    public function isWithTax()
    {
        return $this->withTax;
    }

    /**
     * @param bool $withTax
     */
    public function setWithTax($withTax)
    {
        $this->withTax = $withTax;
    }

    /**
     * @return string
     */
    public function getExternalOrderNumber(): string
    {
        return (string)$this->externalOrderNumber;
    }

    /**
     * @param string $externalOrderNumber
     */
    public function setExternalOrderNumber(string $externalOrderNumber): void
    {
        $this->externalOrderNumber = $externalOrderNumber;
    }

    /**
     * @return ?float
     */
    public function getGutscheinAbzugNetto(): ?float
    {
        return (float)$this->gutscheinAbzugNetto;
    }

    /**
     * @param ?float $gutscheinAbzugNetto
     */
    public function setGutscheinAbzugNetto(?float $gutscheinAbzugNetto): void
    {
        $this->gutscheinAbzugNetto = $gutscheinAbzugNetto;
    }
}
