<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Parcelnumber
{
    /**
     * @MongoDB\Id
     */
    protected $id;
/**
     * @var \DateTime $created;
     *
     * @MongoDB\Field(type="date")
     */
    protected $created;
/**
     * @MongoDB\Field(type="string")
     */
    protected $shippingId;
/**
     * @MongoDB\Field(type="int")
     */
    protected $shopId;
/**
     * @MongoDB\Field(type="string")
     */
    protected $number;
/**
     * @MongoDB\Field(type="boolean")
     */
    protected $used;

    public function __construct()
    {
        $this->used = 0;
        $this->created = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getShippingId()
    {
        return $this->shippingId;
    }

    /**
     * @param mixed $shippingId
     */
    public function setShippingId($shippingId)
    {
        $this->shippingId = $shippingId->getUid();
    }

    /**
     * @return mixed
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * @param mixed $number
     */
    public function setNumber($number)
    {
        $this->number = $number;
    }

    /**
     * @return mixed
     */
    public function getUsed()
    {
        return $this->used;
    }

    /**
     * @param mixed $used
     */
    public function setUsed($used)
    {
        $this->used = $used;
    }

    /**
     * @return mixed
     */
    public function getShopId()
    {
        return $this->shopId;
    }

    /**
     * @param mixed $shopId
     */
    public function setShopId($shopId)
    {
        $this->shopId = $shopId;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }
}
