<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document\Repository;

use Doctrine\ODM\MongoDB\Repository\DocumentRepository;

class MotivRepository extends DocumentRepository
{
    public function findByContactId($contactId, $start, $limit)
    {
        return $this->createQueryBuilder()
            ->field('contactId')->equals($contactId)
            ->limit($limit)
            ->skip($start)
            ->getQuery()
            ->execute();
    }

    public function countByContactId($contactId)
    {
        return $this->createQueryBuilder()->count()
            ->field('contactId')->equals($contactId)
            ->getQuery()
            ->execute();
    }

    public function findBySessionId($sessionId, $start, $limit)
    {
        return $this->createQueryBuilder()
            ->field('sessionId')->equals($sessionId)
            ->limit($limit)
            ->skip($start)
            ->getQuery()
            ->execute();
    }

    public function countBySessionId($sessionId)
    {
        return $this->createQueryBuilder()->count()
            ->field('sessionId')->equals($sessionId)
            ->getQuery()
            ->execute();
    }
}
