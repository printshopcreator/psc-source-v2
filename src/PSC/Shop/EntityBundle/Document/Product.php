<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document
 */
class Product
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;

    /**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;

    /**
     * @var string $ansprechPartner;
     *
     * @Field(type="string")
     */
    protected $ansprechPartner;

    /**
     * @var \DateTime $ablaufDatum;
     *
     * @Field(type="date")
     */
    protected $ablaufDatum;

    /**
     * @var string $printPartnerEmail;
     *
     * @Field(type="string")
     */
    protected $printPartnerEmail;

    /**
     * @var string $zusatzDesigner;
     *
     * @Field(type="string")
     */
    protected $zusatzDesigner;

    /**
     * @var string $zusatzAbmessung;
     *
     * @Field(type="string")
     */
    protected $zusatzAbmessung;

    /**
     * @var string $zusatzShipping;
     *
     * @Field(type="string")
     */
    protected $zusatzShipping;

    /**
     * @var string $custom1;
     *
     * @Field(type="string")
     */
    protected $custom1;

    /**
     * @var string $custom2;
     *
     * @Field(type="string")
     */
    protected $custom2;

    /**
     * @var string $custom3;
     *
     * @Field(type="string")
     */
    protected $custom3;

    /**
     * @var string $custom4;
     *
     * @Field(type="string")
     */
    protected $custom4;

    /**
     * @var string $custom5;
     *
     * @Field(type="string")
     */
    protected $custom5;

    /**
     * @var string $custom6;
     *
     * @Field(type="string")
     */
    protected $custom6;

    /**
     * @var string $custom7;
     *
     * @Field(type="string")
     */
    protected $custom7;

    /**
     * @var string $custom8;
     *
     * @Field(type="string")
     */
    protected $custom8;

    /**
     * @var string $custom9;
     *
     * @Field(type="string")
     */
    protected $custom9;

    /**
     * @var string $custom10;
     *
     * @Field(type="string")
     */
    protected $custom10;

    /**
     * @var string $custom11;
     *
     * @Field(type="string")
     */
    protected $custom11;

    /**
     * @var string $custom12;
     *
     * @Field(type="string")
     */
    protected $custom12;

    /**
     * @var string $text;
     *
     * @Field(type="string")
     */
    protected $text;

    /**
     * @var integer $collectingOrdersAccountFilter;
     *
     * @Field(type="int")
     */
    protected $collectingOrdersAccountFilter;

    /**
     * @var integer $collectingOrdersAccount;
     *
     * @Field(type="int")
     */
    protected $collectingOrdersAccount;

    /**
     * @var boolean $collectingOrdersChangePicture;
     *
     * @Field(type="bool")
     */
    protected $collectingOrdersChangePicture;

    /**
     * @var boolean $collectingOrdersCopy;
     *
     * @Field(type="bool")
     */
    protected $collectingOrdersCopy;

    /**
     * @var boolean $collectingOrdersNewContact;
     *
     * @Field(type="bool")
     */
    protected $collectingOrdersNewContact;

    /**
     * @var boolean $collectingOrdersInviteContact;
     *
     * @Field(type="bool")
     */
    protected $collectingOrdersInviteContact;

    /**
     * @var boolean $collectingOrdersIntegrated;
     *
     * @Field(type="bool")
     */
    protected $collectingOrdersIntegrated;

    /**
     * @var boolean
     *
     * @Field(type="bool")
     */
    protected $hintEnable = false;

    /**
     * @var boolean
     *
     * @Field(type="bool")
     */
    protected $asRequest = false;

    /**
     * @var boolean
     *
     * @Field(type="bool")
     */
    protected $customTabEnable = false;

    /**
     * @var array $pluginSettings;
     *
     * @Field(type="hash")
     */
    protected $pluginSettings = [];

    /**
     * @var string $extraSettings;
     *
     * @Field(type="string")
     */
    protected $extraSettings;

    /**
     * @var string $weight;
     *
     * @Field(type="string")
     */
    protected $weight;

    /**
     * @var string $availability;
     *
     * @Field(type="string")
     */
    protected $availability;

    /**
     * @var integer $salesUnit;
     *
     * @Field(type="int")
     */
    protected $salesUnit;

    /**
     * @var integer $baseUnit;
     *
     * @Field(type="int")
     */
    protected $baseUnit;

    /**
     * @var integer $packagingUnit;
     *
     * @Field(type="int")
     */
    protected $packagingUnit;

    /**
     * @var integer $uploadFromLatestOrderInitalStatus;
     * @Field(type="int")
     */
    protected $uploadFromLatestOrderInitalStatus;

    /**
     * @var boolean $uploadFromLatestOrder;
     * @Field(type="bool")
     *
     */
    protected $uploadFromLatestOrder;

    /**
     * @var integer $uploadProvidedInitalStatus;
     * @Field(type="int")
     */
    protected $uploadProvidedInitalStatus;

    /**
     * @var boolean $uploadProvided;
     * @Field(type="bool")
     *
     */
    protected $uploadProvided;

    /**
     * @var string $uploadProvidedFile;
     * @Field(type="string")
     *
     */
    protected $uploadProvidedFile;

    public function __get($name)
    {
        // TODO: Implement __get() method.
    }

    /**
     * @return int
     */
    public function getSalesUnit(): int
    {
        return (int)$this->salesUnit;
    }

    /**
     * @param int $salesUnit
     */
    public function setSalesUnit(int $salesUnit): void
    {
        $this->salesUnit = $salesUnit;
    }

    /**
     * @return int
     */
    public function getBaseUnit(): int
    {
        return (int)$this->baseUnit;
    }

    /**
     * @param int $baseUnit
     */
    public function setBaseUnit(int $baseUnit): void
    {
        $this->baseUnit = $baseUnit;
    }

    /**
     * @return int
     */
    public function getPackagingUnit(): int
    {
        return (int)$this->packagingUnit;
    }

    /**
     * @param int $packagingUnit
     */
    public function setPackagingUnit(int $packagingUnit): void
    {
        $this->packagingUnit = $packagingUnit;
    }

    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    public function getWeight()
    {
        return $this->weight;
    }

    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getAnsprechPartner()
    {
        return $this->ansprechPartner;
    }

    /**
     * @param string $ansprechPartner
     */
    public function setAnsprechPartner($ansprechPartner)
    {
        $this->ansprechPartner = $ansprechPartner;
    }

    /**
     * @return \DateTime
     */
    public function getAblaufDatum()
    {
        return $this->ablaufDatum;
    }

    /**
     * @param \DateTime $ablaufDatum
     */
    public function setAblaufDatum($ablaufDatum)
    {
        $this->ablaufDatum = $ablaufDatum;
    }

    /**
     * @return string
     */
    public function getPrintPartnerEmail()
    {
        return $this->printPartnerEmail;
    }

    /**
     * @param string $printPartnerEmail
     */
    public function setPrintPartnerEmail($printPartnerEmail)
    {
        $this->printPartnerEmail = $printPartnerEmail;
    }

    /**
     * @return string
     */
    public function getZusatzAbmessung()
    {
        return $this->zusatzAbmessung;
    }

    /**
     * @param string $zusatzAbmessung
     */
    public function setZusatzAbmessung($zusatzAbmessung)
    {
        $this->zusatzAbmessung = $zusatzAbmessung;
    }

    /**
     * @return string
     */
    public function getZusatzDesigner()
    {
        return $this->zusatzDesigner;
    }

    /**
     * @param string $zusatzDesigner
     */
    public function setZusatzDesigner($zusatzDesigner)
    {
        $this->zusatzDesigner = $zusatzDesigner;
    }

    /**
     * @return string
     */
    public function getZusatzShipping()
    {
        return $this->zusatzShipping;
    }

    /**
     * @param string $zusatzShipping
     */
    public function setZusatzShipping($zusatzShipping)
    {
        $this->zusatzShipping = $zusatzShipping;
    }

    /**
     * @return string
     */
    public function getCustom1()
    {
        return $this->custom1;
    }

    /**
     * @param string $custom1
     */
    public function setCustom1($custom1)
    {
        $this->custom1 = $custom1;
    }

    /**
     * @return string
     */
    public function getCustom2()
    {
        return $this->custom2;
    }

    /**
     * @param string $custom2
     */
    public function setCustom2($custom2)
    {
        $this->custom2 = $custom2;
    }

    /**
     * @return string
     */
    public function getCustom3()
    {
        return $this->custom3;
    }

    /**
     * @param string $custom3
     */
    public function setCustom3($custom3)
    {
        $this->custom3 = $custom3;
    }

    /**
     * @return string
     */
    public function getCustom4()
    {
        return $this->custom4;
    }

    /**
     * @param string $custom4
     */
    public function setCustom4($custom4)
    {
        $this->custom4 = $custom4;
    }

    /**
     * @return string
     */
    public function getCustom5()
    {
        return $this->custom5;
    }

    /**
     * @param string $custom5
     */
    public function setCustom5($custom5)
    {
        $this->custom5 = $custom5;
    }

    /**
     * @return string
     */
    public function getCustom6()
    {
        return $this->custom6;
    }

    /**
     * @param string $custom6
     */
    public function setCustom6($custom6)
    {
        $this->custom6 = $custom6;
    }

    /**
     * @return string
     */
    public function getCustom7()
    {
        return $this->custom7;
    }

    /**
     * @param string $custom7
     */
    public function setCustom7($custom7)
    {
        $this->custom7 = $custom7;
    }

    /**
     * @return string
     */
    public function getCustom8()
    {
        return $this->custom8;
    }

    /**
     * @param string $custom8
     */
    public function setCustom8($custom8)
    {
        $this->custom8 = $custom8;
    }

    /**
     * @return string
     */
    public function getCustom9()
    {
        return $this->custom9;
    }

    /**
     * @param string $custom9
     */
    public function setCustom9($custom9)
    {
        $this->custom9 = $custom9;
    }

    /**
     * @return string
     */
    public function getCustom10()
    {
        return $this->custom10;
    }

    /**
     * @param string $custom10
     */
    public function setCustom10($custom10)
    {
        $this->custom10 = $custom10;
    }

    /**
     * @return string
     */
    public function getCustom11()
    {
        return $this->custom11;
    }

    /**
     * @param string $custom11
     */
    public function setCustom11($custom11)
    {
        $this->custom11 = $custom11;
    }

    /**
     * @return string
     */
    public function getCustom12()
    {
        return $this->custom12;
    }

    /**
     * @param string $custom12
     */
    public function setCustom12($custom12)
    {
        $this->custom12 = $custom12;
    }

    /**
     * @return integer
     */
    public function getCollectingOrdersAccount()
    {
        return $this->collectingOrdersAccount;
    }

    /**
     * @param int $collectingOrdersAccount
     */
    public function setCollectingOrdersAccount($collectingOrdersAccount)
    {
        $this->collectingOrdersAccount = $collectingOrdersAccount;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersChangePicture()
    {
        return $this->collectingOrdersChangePicture;
    }

    /**
     * @param bool $collectingOrdersChangePicture
     */
    public function setCollectingOrdersChangePicture($collectingOrdersChangePicture)
    {
        $this->collectingOrdersChangePicture = $collectingOrdersChangePicture;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersCopy()
    {
        return $this->collectingOrdersCopy;
    }

    /**
     * @param bool $collectingOrdersCopy
     */
    public function setCollectingOrdersCopy($collectingOrdersCopy)
    {
        $this->collectingOrdersCopy = $collectingOrdersCopy;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersNewContact()
    {
        return $this->collectingOrdersNewContact;
    }

    /**
     * @param bool $collectingOrdersNewContact
     */
    public function setCollectingOrdersNewContact($collectingOrdersNewContact)
    {
        $this->collectingOrdersNewContact = $collectingOrdersNewContact;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersInviteContact()
    {
        return $this->collectingOrdersInviteContact;
    }

    /**
     * @param bool $collectingOrdersInviteContact
     */
    public function setCollectingOrdersInviteContact($collectingOrdersInviteContact)
    {
        $this->collectingOrdersInviteContact = $collectingOrdersInviteContact;
    }

    /**
     * @param integer $collectingOrdersAccountFilter
     */
    public function setCollectingOrdersAccountFilter($collectingOrdersAccountFilter)
    {
        $this->collectingOrdersAccountFilter = $collectingOrdersAccountFilter;
    }

    /**
     * @return int
     */
    public function getCollectingOrdersAccountFilter()
    {
        return $this->collectingOrdersAccountFilter;
    }



    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    public function getPluginSetting($module, $key)
    {
        if (!isset($this->pluginSettings['module']) && !isset($this->pluginSettings[$module][$key])) {
            return false;
        }

        return $this->pluginSettings[$module][$key];
    }

    /**
     * @param $module
     * @param $key
     * @param $value
     */
    public function setPluginSettingModule($module, $key, $value)
    {
        $this->pluginSettings[$module ] [$key] = $value;
    }

    public function getPluginSettingModule($module, $key)
    {
        if (!isset($this->pluginSettings[$module ]) || !isset($this->pluginSettings[$module ] [$key])) {
            return null;
        }

        return $this->pluginSettings[$module ] [$key];
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @return bool
     */
    public function isCollectingOrdersIntegrated()
    {
        return $this->collectingOrdersIntegrated;
    }

    /**
     * @param bool $collectingOrdersIntegrated
     */
    public function setCollectingOrdersIntegrated($collectingOrdersIntegrated)
    {
        $this->collectingOrdersIntegrated = $collectingOrdersIntegrated;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param string $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return string
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param string $availability
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
    }

    /**
     * @return int
     */
    public function getUploadFromLatestOrderInitalStatus(): int
    {
        return (int)$this->uploadFromLatestOrderInitalStatus;
    }

    /**
     * @param int $uploadFromLatestOrderInitalStatus
     */
    public function setUploadFromLatestOrderInitalStatus(int $uploadFromLatestOrderInitalStatus): void
    {
        $this->uploadFromLatestOrderInitalStatus = $uploadFromLatestOrderInitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadFromLatestOrder(): bool
    {
        return (bool)$this->uploadFromLatestOrder;
    }

    /**
     * @param bool $uploadFromLatestOrder
     */
    public function setUploadFromLatestOrder(bool $uploadFromLatestOrder): void
    {
        $this->uploadFromLatestOrder = $uploadFromLatestOrder;
    }

    /**
     * @return int
     */
    public function getUploadProvidedInitalStatus(): int
    {
        return (int)$this->uploadProvidedInitalStatus;
    }

    /**
     * @param int $uploadProvidedInitalStatus
     */
    public function setUploadProvidedInitalStatus(int $uploadProvidedInitalStatus): void
    {
        $this->uploadProvidedInitalStatus = $uploadProvidedInitalStatus;
    }

    /**
     * @return bool
     */
    public function isUploadProvided(): bool
    {
        return (bool)$this->uploadProvided;
    }

    /**
     * @param bool $uploadProvided
     */
    public function setUploadProvided(bool $uploadProvided): void
    {
        $this->uploadProvided = $uploadProvided;
    }

    /**
     * @return string
     */
    public function getUploadProvidedFile(): string
    {
        return (string)$this->uploadProvidedFile;
    }

    /**
     * @param string $uploadProvidedFile
     */
    public function setUploadProvidedFile(string $uploadProvidedFile): void
    {
        $this->uploadProvidedFile = $uploadProvidedFile;
    }

    /**
     * @return bool
     */
    public function isHintEnable(): bool
    {
        return boolval($this->hintEnable);
    }

    /**
     * @param bool $hintEnable
     */
    public function setHintEnable(bool $hintEnable): void
    {
        $this->hintEnable = $hintEnable;
    }

    /**
     * @return bool
     */
    public function isCustomTabEnable(): bool
    {
        return boolval($this->customTabEnable);
    }

    /**
     * @param bool $customTabEnable
     */
    public function setCustomTabEnable(bool $customTabEnable): void
    {
        $this->customTabEnable = $customTabEnable;
    }

    public function isAsRequest(): bool
    {
        return boolval($this->asRequest);
    }

    public function setAsRequest(bool $var): void
    {
        $this->asRequest = $var;
    }
}
