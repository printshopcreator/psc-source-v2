<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;

/**
 * @Document
 */
class Install
{
    /**
     * @Id
     */
    protected $id;
/**
     * @Field(type="string")
     */
    protected $installId;
/**
     * @Field(type="int")
     */
    protected $calendarStartYear;
/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCalendarStartYear()
    {
        return $this->calendarStartYear;
    }

    /**
     * @param mixed $calendarStartYear
     */
    public function setCalendarStartYear($calendarStartYear)
    {
        $this->calendarStartYear = $calendarStartYear;
    }

    /**
     * @return mixed
     */
    public function getInstallId()
    {
        return $this->installId;
    }

    /**
     * @param mixed $installId
     */
    public function setInstallId($installId)
    {
        $this->installId = $installId;
    }
}
