<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Date;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Boolean;

/** @EmbeddedDocument */
class Tracking
{
    /**
     * @var string $shippingProvider ;
     *
     * @Field(type="string")
     */
    protected $shippingProvider;
/**
     * @var string $trackingNumber ;
     *
     * @Field(type="string")
     */
    protected $trackingNumber;
/**
     * @var string $trackingUrl ;
     *
     * @Field(type="string")
     */
    protected $trackingUrl;
/**
     * @return string
     */
    public function getShippingProvider()
    {
        return $this->shippingProvider;
    }

    /**
     * @param string $shippingProvider
     */
    public function setShippingProvider($shippingProvider)
    {
        $this->shippingProvider = $shippingProvider;
    }

    /**
     * @return string
     */
    public function getTrackingNumber()
    {
        return $this->trackingNumber;
    }

    /**
     * @param string $trackingNumber
     */
    public function setTrackingNumber($trackingNumber)
    {
        $this->trackingNumber = $trackingNumber;
    }

    /**
     * @return string
     */
    public function getTrackingUrl()
    {
        return $this->trackingUrl;
    }

    /**
     * @param string $trackingUrl
     */
    public function setTrackingUrl($trackingUrl)
    {
        $this->trackingUrl = $trackingUrl;
    }
}
