<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Date;

/**
 * @Document
 */
class Productgroup
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var string $textArt;
     *
     * @Field(type="string")
     */
    protected $textArt;
/**
     * @var string $textFormat;
     *
     * @Field(type="string")
     */
    protected $textFormat;
/**
     * @var string $einleitung;
     *
     * @Field(type="string")
     */
    protected $einleitung;
/**
     * @var string $description;
     *
     * @Field(type="string")
     */
    protected $description;
/**
     * @var string $extraSettings;
     *
     * @Field(type="string")
     */
    protected $extraSettings;
/**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getTextArt()
    {
        return $this->textArt;
    }

    /**
     * @param string $textArt
     */
    public function setTextArt($textArt)
    {
        $this->textArt = $textArt;
    }

    /**
     * @return string
     */
    public function getEinleitung()
    {
        return $this->einleitung;
    }

    /**
     * @param string $einleitung
     */
    public function setEinleitung($einleitung)
    {
        $this->einleitung = $einleitung;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getTextFormat()
    {
        return $this->textFormat;
    }

    /**
     * @param string $textFormat
     */
    public function setTextFormat($textFormat)
    {
        $this->textFormat = $textFormat;
    }

    /**
     * @return string
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param string $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }
}
