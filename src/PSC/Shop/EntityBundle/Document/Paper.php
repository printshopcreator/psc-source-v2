<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document
 */
class Paper
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var boolean $papierTyp1
     *
     * @Field(type="bool")
     */
    protected $papierTyp1;
/**
     * @var boolean $papierTyp2
     *
     * @Field(type="bool")
     */
    protected $papierTyp2;
/**
     * @var boolean $papierTyp3
     *
     * @Field(type="bool")
     */
    protected $papierTyp3;
/**
     * @var boolean $papierTyp4;
     *
     * @Field(type="bool")
     */
    protected $papierTyp4;
/**
     * @var boolean $papierTyp5
     *
     * @Field(type="bool")
     */
    protected $papierTyp5;
/**
     * @var boolean $papierTyp6
     *
     * @Field(type="bool")
     */
    protected $papierTyp6;
/**
     * @var boolean $papierTyp7
     *
     * @Field(type="bool")
     */
    protected $papierTyp7;
/**
     * @var boolean $papierTyp8
     *
     * @Field(type="bool")
     */
    protected $papierTyp8;
/**
     * @var boolean $papierTyp9
     *
     * @Field(type="bool")
     */
    protected $papierTyp9;
/**
     * @var boolean $papierTyp10
     *
     * @Field(type="bool")
     */
    protected $papierTyp10;
/**
     * @var boolean $papierTyp11
     *
     * @Field(type="bool")
     */
    protected $papierTyp11;
/**
     * @var boolean $papierTyp12
     *
     * @Field(type="bool")
     */
    protected $papierTyp12;
/**
     * @var boolean $papierTyp13
     *
     * @Field(type="bool")
     */
    protected $papierTyp13;
/**
     * @var boolean $papierTyp14
     *
     * @Field(type="bool")
     */
    protected $papierTyp14;
/**
     * @var boolean $umschlagen
     *
     * @Field(type="bool")
     */
    protected $umschlagen;
/**
     * @var boolean $happy
     *
     * @Field(type="bool")
     */
    protected $happy;
/**
     * @var boolean $eq
     *
     * @Field(type="bool")
     */
    protected $eq;
/**
     * @var boolean $sense
     *
     * @Field(type="bool")
     */
    protected $sense;
/**
     * @var boolean $sky
     *
     * @Field(type="bool")
     */
    protected $sky;
/**
     * @var boolean $glam
     *
     * @Field(type="bool")
     */
    protected $glam;
/**
     * @var boolean $post
     *
     * @Field(type="bool")
     */
    protected $post;
/**
     * @var string $volume
     *
     * @Field(type="string")
     */
    protected $volume;
/**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp1()
    {
        return $this->papierTyp1;
    }

    /**
     * @param boolean $papierTyp1
     */
    public function setPapierTyp1($papierTyp1)
    {
        $this->papierTyp1 = $papierTyp1;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp2()
    {
        return $this->papierTyp2;
    }

    /**
     * @param boolean $papierTyp2
     */
    public function setPapierTyp2($papierTyp2)
    {
        $this->papierTyp2 = $papierTyp2;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp3()
    {
        return $this->papierTyp3;
    }

    /**
     * @param boolean $papierTyp3
     */
    public function setPapierTyp3($papierTyp3)
    {
        $this->papierTyp3 = $papierTyp3;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp4()
    {
        return $this->papierTyp4;
    }

    /**
     * @param boolean $papierTyp4
     */
    public function setPapierTyp4($papierTyp4)
    {
        $this->papierTyp4 = $papierTyp4;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp5()
    {
        return $this->papierTyp5;
    }

    /**
     * @param boolean $papierTyp5
     */
    public function setPapierTyp5($papierTyp5)
    {
        $this->papierTyp5 = $papierTyp5;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp6()
    {
        return $this->papierTyp6;
    }

    /**
     * @param boolean $papierTyp6
     */
    public function setPapierTyp6($papierTyp6)
    {
        $this->papierTyp6 = $papierTyp6;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp7()
    {
        return $this->papierTyp7;
    }

    /**
     * @param boolean $papierTyp7
     */
    public function setPapierTyp7($papierTyp7)
    {
        $this->papierTyp7 = $papierTyp7;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp8()
    {
        return $this->papierTyp8;
    }

    /**
     * @param boolean $papierTyp8
     */
    public function setPapierTyp8($papierTyp8)
    {
        $this->papierTyp8 = $papierTyp8;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp9()
    {
        return $this->papierTyp9;
    }

    /**
     * @param boolean $papierTyp9
     */
    public function setPapierTyp9($papierTyp9)
    {
        $this->papierTyp9 = $papierTyp9;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp10()
    {
        return $this->papierTyp10;
    }

    /**
     * @param boolean $papierTyp10
     */
    public function setPapierTyp10($papierTyp10)
    {
        $this->papierTyp10 = $papierTyp10;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp11()
    {
        return $this->papierTyp11;
    }

    /**
     * @param boolean $papierTyp11
     */
    public function setPapierTyp11($papierTyp11)
    {
        $this->papierTyp11 = $papierTyp11;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp12()
    {
        return $this->papierTyp12;
    }

    /**
     * @param boolean $papierTyp12
     */
    public function setPapierTyp12($papierTyp12)
    {
        $this->papierTyp12 = $papierTyp12;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp13()
    {
        return $this->papierTyp13;
    }

    /**
     * @param boolean $papierTyp13
     */
    public function setPapierTyp13($papierTyp13)
    {
        $this->papierTyp13 = $papierTyp13;
    }

    /**
     * @return boolean
     */
    public function isUmschlagen()
    {
        return $this->umschlagen;
    }

    /**
     * @param boolean $umschlagen
     */
    public function setUmschlagen($umschlagen)
    {
        $this->umschlagen = $umschlagen;
    }

    /**
     * @return boolean
     */
    public function isHappy()
    {
        return $this->happy;
    }

    /**
     * @param boolean $happy
     */
    public function setHappy($happy)
    {
        $this->happy = $happy;
    }

    /**
     * @return boolean
     */
    public function isEq()
    {
        return $this->eq;
    }

    /**
     * @param boolean $eq
     */
    public function setEq($eq)
    {
        $this->eq = $eq;
    }

    /**
     * @return boolean
     */
    public function isSense()
    {
        return $this->sense;
    }

    /**
     * @param boolean $sense
     */
    public function setSense($sense)
    {
        $this->sense = $sense;
    }

    /**
     * @return boolean
     */
    public function isSky()
    {
        return $this->sky;
    }

    /**
     * @param boolean $sky
     */
    public function setSky($sky)
    {
        $this->sky = $sky;
    }

    /**
     * @return boolean
     */
    public function isGlam()
    {
        return $this->glam;
    }

    /**
     * @param boolean $glam
     */
    public function setGlam($glam)
    {
        $this->glam = $glam;
    }

    /**
     * @return boolean
     */
    public function isPost()
    {
        return $this->post;
    }

    /**
     * @param boolean $post
     */
    public function setPost($post)
    {
        $this->post = $post;
    }

    /**
     * @return boolean
     */
    public function isPapierTyp14()
    {
        return $this->papierTyp14;
    }

    /**
     * @param boolean $papierTyp14
     */
    public function setPapierTyp14($papierTyp14)
    {
        $this->papierTyp14 = $papierTyp14;
    }

    /**
     * @return string
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * @param string $volume
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;
    }
}
