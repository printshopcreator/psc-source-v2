<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document
 */
class Instance
{
    /**
     * @Id
     */
    protected $id;
/**
     * @Field(type="string")
     */
    protected $appId;
/**
     * @Field(type="string")
     */
    protected $syncServer;
/**
     * @Field(type="string")
     */
    protected $ftpUsername;
/**
     * @Field(type="string")
     */
    protected $ftpPassword;
/**
     * @var int $smtpPort;
     *
     * @Field(type="int")
     */
    protected $smtpPort;
/**
     * @var boolean $smtpOwn
     *
     * @Field(type="bool")
     */
    protected $smtpOwn;
/**
     * @var boolean $smtpTls
     *
     * @Field(type="bool")
     */
    protected $smtpTls;
/**
     * @Field(type="string")
     */
    protected $smtpHost;
/**
     * @Field(type="string")
     */
    protected $smtpPassword;
/**
     * @Field(type="string")
     */
    protected $smtpUsername;
/**
     * @var string $numberPattern;
     *
     * @Field(type="string")
     */
    protected $numberPattern;
/**
     * @var int $numberStart;
     *
     * @Field(type="int")
     */
    protected $numberStart;
/**
     * @var string $offerNumberPattern;
     *
     * @Field(type="string")
     */
    protected $offerNumberPattern;
/**
     * @var int $offerNumberStart;
     *
     * @Field(type="int")
     */
    protected $offerNumberStart;
/**
     * @var string $extraSettings;
     *
     * @Field(type="string")
     */
    protected $extraSettings;
/**
     * @var string $maintenanceText;
     *
     * @Field(type="string")
     */
    protected $maintenanceText;
/**
     * @var string $maintenanceTitle;
     *
     * @Field(type="string")
     */
    protected $maintenanceTitle;
/**
     * @var bool $maintenanceMode;
     *
     * @Field(type="bool")
     */
    protected $maintenanceMode;
/**
     * @var string $supporturl;
     *
     * @Field(type="string")
     */
    protected $supporturl;
/**
     * @var string $supporttoken;
     *
     * @Field(type="string")
     */
    protected $supporttoken;
/**
     * @var string $monitoringkey;
     *
     * @Field(type="string")
     */
    protected $monitoringkey;
/**
     * @var string $supportprotokoll;
     *
     * @Field(type="bool")
     */
    protected $supportprotokoll;
/**
     * @var string $supportlogin;
     *
     * @Field(type="bool")
     */
    protected $supportlogin;
/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAppId()
    {
        return $this->appId;
    }

    /**
     * @param mixed $appId
     */
    public function setAppId($appId)
    {
        $this->appId = $appId;
    }

    /**
     * @return mixed
     */
    public function getFtpUsername()
    {
        return $this->ftpUsername;
    }

    /**
     * @param mixed $ftpUsername
     */
    public function setFtpUsername($ftpUsername)
    {
        $this->ftpUsername = $ftpUsername;
    }

    /**
     * @return mixed
     */
    public function getFtpPassword()
    {
        return $this->ftpPassword;
    }

    /**
     * @param mixed $ftpPassword
     */
    public function setFtpPassword($ftpPassword)
    {
        $this->ftpPassword = $ftpPassword;
    }

    /**
     * @return mixed
     */
    public function getSyncServer()
    {
        return $this->syncServer;
    }

    /**
     * @param mixed $syncServer
     */
    public function setSyncServer($syncServer)
    {
        $this->syncServer = $syncServer;
    }

    /**
     * @return int
     */
    public function getSmtpPort()
    {
        return $this->smtpPort;
    }

    /**
     * @param int $smtpPort
     */
    public function setSmtpPort($smtpPort)
    {
        $this->smtpPort = $smtpPort;
    }

    /**
     * @return bool
     */
    public function isSmtpOwn()
    {
        return $this->smtpOwn;
    }

    /**
     * @param bool $smtpOwn
     */
    public function setSmtpOwn($smtpOwn)
    {
        $this->smtpOwn = $smtpOwn;
    }

    /**
     * @return bool
     */
    public function isSmtpTls()
    {
        return $this->smtpTls;
    }

    /**
     * @param bool $smtpTls
     */
    public function setSmtpTls($smtpTls)
    {
        $this->smtpTls = $smtpTls;
    }

    /**
     * @return mixed
     */
    public function getSmtpHost()
    {
        return $this->smtpHost;
    }

    /**
     * @param mixed $smtpHost
     */
    public function setSmtpHost($smtpHost)
    {
        $this->smtpHost = $smtpHost;
    }

    /**
     * @return mixed
     */
    public function getSmtpPassword()
    {
        return $this->smtpPassword;
    }

    /**
     * @param mixed $smtpPassword
     */
    public function setSmtpPassword($smtpPassword)
    {
        $this->smtpPassword = $smtpPassword;
    }

    /**
     * @return mixed
     */
    public function getSmtpUsername()
    {
        return $this->smtpUsername;
    }

    /**
     * @param mixed $smtpUsername
     */
    public function setSmtpUsername($smtpUsername)
    {
        $this->smtpUsername = $smtpUsername;
    }

    /**
     * @return int
     */
    public function getNumberStart()
    {
        return $this->numberStart;
    }

    /**
     * @param int $numberStart
     */
    public function setNumberStart($numberStart)
    {
        $this->numberStart = $numberStart;
    }

    /**
     * @return string
     */
    public function getNumberPattern()
    {
        return $this->numberPattern;
    }

    /**
     * @param string $numberPattern
     */
    public function setNumberPattern($numberPattern)
    {
        $this->numberPattern = $numberPattern;
    }

    /**
     * @return string
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param string $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return int
     */
    public function getOfferNumberStart(): int
    {
        return $this->offerNumberStart;
    }

    /**
     * @param int $offerNumberStart
     */
    public function setOfferNumberStart(int $offerNumberStart): void
    {
        $this->offerNumberStart = $offerNumberStart;
    }

    /**
     * @return string
     */
    public function getOfferNumberPattern(): string
    {
        return $this->offerNumberPattern;
    }

    /**
     * @param string $offerNumberPattern
     */
    public function setOfferNumberPattern(string $offerNumberPattern): void
    {
        $this->offerNumberPattern = $offerNumberPattern;
    }

    /**
     * @return string
     */
    public function getSupporturl(): string
    {
        return (string)$this->supporturl;
    }

    /**
     * @param string $offerNumberPattern
     */
    public function setSupporturl($supporturl)
    {
        $this->supporturl = $supporturl;
    }

    /**
     * @return string
     */
    public function getSupporttoken(): string
    {
        return (string)$this->supporttoken;
    }

    /**
     * @param string $offerNumberPattern
     */
    public function setSupporttoken($supporttoken)
    {
        $this->supporttoken = $supporttoken;
    }

    /**
     * @return string
     */
    public function getMonitoringkey(): string
    {
        return (string)$this->monitoringkey;
    }

    /**
     * @param string $offerNumberPattern
     */
    public function setMonitoringkey($monitoringkey)
    {
        $this->monitoringkey = $monitoringkey;
    }

    /**
     * @return bool
     */
    public function isSupportprotokoll()
    {
        return $this->supportprotokoll;
    }

    /**
     * @param bool $smtpTls
     */
    public function setSupportprotokoll($supportprotokoll)
    {
        $this->supportprotokoll = $supportprotokoll;
    }

    /**
     * @return bool
     */
    public function isSupportlogin()
    {
        return $this->supportlogin;
    }

    /**
     * @param bool $smtpTls
     */
    public function setSupportlogin($supportlogin)
    {
        $this->supportlogin = $supportlogin;
    }

    public function getFormatedSupportUrl()
    {
        if ($this->supporturl == "") {
            return "";
        }
        $url = parse_url($this->supporturl);
        return $url['scheme'] . '://' . $url['host'];
    }

    /**
     * @return bool
     */
    public function isMaintenanceMode(): bool
    {
        return (bool)$this->maintenanceMode;
    }

    /**
     * @param bool $maintenanceMode
     */
    public function setMaintenanceMode(bool $maintenanceMode): void
    {
        $this->maintenanceMode = $maintenanceMode;
    }

    /**
     * @return string
     */
    public function getMaintenanceText(): string
    {
        return (string)$this->maintenanceText;
    }

    /**
     * @param string $maintenanceText
     */
    public function setMaintenanceText(?string $maintenanceText): void
    {
        $this->maintenanceText = $maintenanceText;
    }

    /**
     * @return string
     */
    public function getMaintenanceTitle(): string
    {
        return (string)$this->maintenanceTitle;
    }

    /**
     * @param string $maintenanceTitle
     */
    public function setMaintenanceTitle(?string $maintenanceTitle): void
    {
        $this->maintenanceTitle = $maintenanceTitle;
    }
}
