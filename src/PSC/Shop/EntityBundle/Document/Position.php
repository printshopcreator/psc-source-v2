<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedMany;

/**
 * @Document
 */
class Position
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var string $templatePrintId ;
     *
     * @Field(type="string")
     */
    protected $templatePrintId;
/**
     * @var string $layouterId ;
     *
     * @Field(type="string")
     */
    protected $layouterId;
/**
     * @var string $uploadMode ;
     *
     * @Field(type="string")
     */
    protected $uploadMode;
/**
     * @var string $customerInfo ;
     *
     * @Field(type="string")
     */
    protected $customerInfo;
/**
     * @var array
     *
     * @Field(type="collection")
     */
    protected $deliveryData;
/** @EmbedMany(targetDocument="Tracking") */
    private $trackings = array();
/**
     * @var string $externalOrderNumber;
     *
     * @Field(type="string")
     */
    protected $externalOrderNumber;
/**
     * @var array
     *
     * @Field(type="collection")
     */
    protected $additionalInfos;
/**
     * @var array
     *
     * @Field(type="collection")
     */
    protected $calcReferences;
    public function __construct()
    {
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTemplatePrintId()
    {
        return $this->templatePrintId;
    }

    /**
     * @param string $templatePrintId
     */
    public function setTemplatePrintId($templatePrintId)
    {
        $this->templatePrintId = $templatePrintId;
    }


    /**
     * @return string
     */
    public function getCustomerInfo()
    {
        return $this->customerInfo;
    }

    public function setCustomerInfo($customerInfo)
    {
        $this->customerInfo = $customerInfo;
    }

    /**
     * @return string
     */
    public function getUploadMode()
    {
        return $this->uploadMode;
    }

    public function setUploadMode($uploadMode)
    {
        $this->uploadMode = $uploadMode;
    }

    /**
     * @return string
     */
    public function getLayouterId()
    {
        return $this->layouterId;
    }

    /**
     * @param string $layouterId
     */
    public function setLayouterId($layouterId)
    {
        $this->layouterId = $layouterId;
    }

    /**
     * @return mixed
     */
    public function getTrackings()
    {
        return $this->trackings;
    }

    /**
     * @param mixed $trackings
     */
    public function setTrackings($trackings)
    {
        $this->trackings = $trackings;
    }

    /**
     * @param Tracking $tracking
     */
    public function addTracking($tracking)
    {
        $this->trackings[] = $tracking;
    }

    public function emptyTracking()
    {
        $this->trackings = [];
    }

    /**
     * @return array
     */
    public function getDeliveryData()
    {
        return $this->deliveryData;
    }

    /**
     * @param array $deliveryData
     */
    public function setDeliveryData($deliveryData)
    {
        $this->deliveryData = $deliveryData;
    }

    /**
     * @return array
     */
    public function getAdditionalInfos()
    {
        return $this->additionalInfos;
    }

    /**
     * @param array $additionalInfos
     */
    public function setAdditionalInfos($additionalInfos)
    {
        $this->additionalInfos = $additionalInfos;
    }

    public function getAdditionalInfo($key)
    {
        foreach ($this->additionalInfos as $info) {
            if ($info['key'] == $key) {
                return $info;
            }
        }
    }

    /**
     * @return array
     */
    public function getCalcReferences()
    {
        return $this->calcReferences;
    }

    /**
     * @param array $calcReferences
     */
    public function setCalcReferences($calcReferences)
    {
        $this->calcReferences = $calcReferences;
    }

    /**
     * @return string
     */
    public function getExternalOrderNumber(): string
    {
        return (string)$this->externalOrderNumber;
    }

    /**
     * @param string $externalOrderNumber
     */
    public function setExternalOrderNumber(string $externalOrderNumber): void
    {
        $this->externalOrderNumber = $externalOrderNumber;
    }
}
