<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document
 */
class Shop
{
    public $basketField1;
    public $basketField2;
    public $basketPosField1;
    public $basketPosField2;
    public $productSort;
    public $productSortDir;
    public $title;
    public $subTitle;
    public $keywords;
    public $description;
    public $copyright;
    public $author;
    public $private;
    public $registration;
    public $deleted = false;
    public $betreiberCompany;
    public $betreiberName;
    public $betreiberStreet;
    public $betreiberAddress;
    public $betreiberEmail;
    public $betreiberTel;
    public $betreiberUid;
    public $betreiberHid;
    public $betreiberSid;
    public $betreiberRegister;
    public $betreiberVb;
    public $betreiberRechtsform;
    public $betreiberWeb;
    public $redirectLogin;
    public $redirectLogout;
    public $logo1;
    public $logo2;
    public $sitemap;
    public $robots;
/**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var string $apiKey;
     *
     * @Field(type="string")
     */
    protected $apiKey;
/**
     * @Field(type="string")
     */
    protected string $defaultCurrency = "EUR";
/**
     * @Field(type="string")
     */
    protected string $defaultLocale = "de_DE";
/**
     * @var string $numberPattern;
     *
     * @Field(type="string")
     */
    protected $numberPattern;
/**
     * @var int $numberStart;
     *
     * @Field(type="int")
     */
    protected $numberStart;
/**
     * @var boolean $ownNumber
     *
     * @Field(type="bool")
     */
    protected $ownNumber;
/**
     * @var boolean
     *
     * @Field(type="bool")
     */
    protected $guestEnable;
/**
     * @var string $contactNumberPattern;
     *
     * @Field(type="string")
     */
    protected $contactNumberPattern;
/**
     * @var int $contactNumberStart;
     *
     * @Field(type="int")
     */
    protected $contactNumberStart;
/**
     * @var float $priceFactor;
     *
     * @Field(type="float")
     */
    protected $priceFactor;
/**
     * @var boolean $contactOwnNumber
     *
     * @Field(type="bool")
     */
    protected $contactOwnNumber;
/**
     * @var string $offerNumberPattern;
     *
     * @Field(type="string")
     */
    protected $offerNumberPattern;
/**
     * @var int $offerNumberStart;
     *
     * @Field(type="int")
     */
    protected $offerNumberStart;
/**
     * @var boolean $offerOwnNumber
     *
     * @Field(type="bool")
     */
    protected $offerOwnNumber;
/**
     * @Field(type="string")
     */
    protected $docInvoiceJasperXml;
/**
     * @Field(type="string")
     */
    protected $docInvoicePosition;
/**
     * @Field(type="string")
     */
    protected $docJobticketJasperXml;
/**
     * @Field(type="string")
     */
    protected $docJobticketPosition;
/**
     * @Field(type="string")
     */
    protected $docJobticketPPJasperXml;
/**
     * @Field(type="string")
     */
    protected $docJobticketPPPosition;
/**
     * @Field(type="string")
     */
    protected $docOfferJasperXml;
/**
     * @Field(type="string")
     */
    protected $docOfferPosition;
/**
     * @Field(type="string")
     */
    protected $docOrderJasperXml;
/**
     * @Field(type="string")
     */
    protected $docOrderPosition;
/**
     * @Field(type="string")
     */
    protected $docLabelJasperXml;
/**
     * @Field(type="string")
     */
    protected $docLabelPosition;
/**
     * @Field(type="string")
     */
    protected $docDeliveryJasperXml;
/**
     * @Field(type="string")
     */
    protected $docDeliveryPosition;
/**
     * @Field(type="string")
     */
    protected $docStornoJasperXml;
/**
     * @Field(type="string")
     */
    protected $docStornoPosition;
/**
     * @Field(type="string")
     */
    protected $redirectAfterBuy;
/**
     * @var boolean $redirectToSSL
     *
     * @Field(type="bool")
     */
    protected $redirectToSSL;
/**
     * @Field(type="string")
     */
    protected $theme;
/**
     * @Field(type="string")
     */
    protected $layouterTheme;
/**
     * @Field(type="string")
     */
    protected $betreiberFax;
/**
     * @var boolean $createPackageAfterBuy
     *
     * @Field(type="bool")
     */
    protected $createPackageAfterBuy;
/**
     * @var boolean $tpSaveUserData
     *
     * @Field(type="bool")
     */
    protected $tpSaveUserData;
/**
     * @var string $productFieldName1;
     *
     * @Field(type="string")
     */
    protected $productFieldName1;
/**
     * @var string $productFieldName2;
     *
     * @Field(type="string")
     */
    protected $productFieldName2;
/**
     * @var string $productFieldName3;
     *
     * @Field(type="string")
     */
    protected $productFieldName3;
/**
     * @var string $productFieldName4;
     *
     * @Field(type="string")
     */
    protected $productFieldName4;
/**
     * @var string $productFieldName5;
     *
     * @Field(type="string")
     */
    protected $productFieldName5;
/**
     * @var string $productFieldName6;
     *
     * @Field(type="string")
     */
    protected $productFieldName6;
/**
     * @var string $productFieldName7;
     *
     * @Field(type="string")
     */
    protected $productFieldName7;
/**
     * @var string $productFieldName8;
     *
     * @Field(type="string")
     */
    protected $productFieldName8;
/**
     * @var string $productFieldName9;
     *
     * @Field(type="string")
     */
    protected $productFieldName9;
/**
     * @var string $productFieldName10;
     *
     * @Field(type="string")
     */
    protected $productFieldName10;
/**
     * @var string $productFieldName12;
     *
     * @Field(type="string")
     */
    protected $productFieldName11;
/**
     * @var string $productFieldName12;
     *
     * @Field(type="string")
     */
    protected $productFieldName12;
/**
     * @var string $customerFieldName1;
     *
     * @Field(type="string")
     */
    protected $customerFieldName1;
/**
     * @var string $customerFieldName2;
     *
     * @Field(type="string")
     */
    protected $customerFieldName2;
/**
     * @var string $customerFieldName3;
     *
     * @Field(type="string")
     */
    protected $customerFieldName3;
/**
     * @var string $customerFieldName4;
     *
     * @Field(type="string")
     */
    protected $customerFieldName4;
/**
     * @var string $customerFieldName5;
     *
     * @Field(type="string")
     */
    protected $customerFieldName5;
/**
     * @var string $customerFieldName6;
     *
     * @Field(type="string")
     */
    protected $customerFieldName6;
/**
     * @var string $customerFieldName7;
     *
     * @Field(type="string")
     */
    protected $customerFieldName7;
/**
     * @var string $customerFieldName8;
     *
     * @Field(type="string")
     */
    protected $customerFieldName8;
/**
     * @var string $customerFieldName9;
     *
     * @Field(type="string")
     */
    protected $customerFieldName9;
/**
     * @var string $customerFieldName10;
     *
     * @Field(type="string")
     */
    protected $customerFieldName10;
/**
     * @var string $customerFieldName12;
     *
     * @Field(type="string")
     */
    protected $customerFieldName11;
/**
     * @var string $customerFieldName12;
     *
     * @Field(type="string")
     */
    protected $customerFieldName12;
/**
     * @var string $customerFieldName13;
     *
     * @Field(type="string")
     */
    protected $customerFieldName13;
/**
     * @var string $customerFieldName14;
     *
     * @Field(type="string")
     */
    protected $customerFieldName14;
/**
     * @var string $customerFieldName15;
     *
     * @Field(type="string")
     */
    protected $customerFieldName15;
/**
     * @var string $customerFieldName16;
     *
     * @Field(type="string")
     */
    protected $customerFieldName16;
/**
     * @var string $customerFieldName17;
     *
     * @Field(type="string")
     */
    protected $customerFieldName17;
/**
     * @var string $customerFieldName18;
     *
     * @Field(type="string")
     */
    protected $customerFieldName18;
/**
     * @var string $customerFieldName19;
     *
     * @Field(type="string")
     */
    protected $customerFieldName19;
/**
     * @var string $customerFieldName20;
     *
     * @Field(type="string")
     */
    protected $customerFieldName20;
/**
     * @var string $customerFieldName21;
     *
     * @Field(type="string")
     */
    protected $customerFieldName21;
/**
     * @var string $customerFieldName22;
     *
     * @Field(type="string")
     */
    protected $customerFieldName22;
/**
     * @var string $customerFieldName23;
     *
     * @Field(type="string")
     */
    protected $customerFieldName23;
/**
     * @var string $customerFieldName24;
     *
     * @Field(type="string")
     */
    protected $customerFieldName24;
/**
     * @var string $mailOrderCustomerSubjectTemplate;
     *
     * @Field(type="string")
     */
    protected $mailOrderCustomerSubjectTemplate;
/**
     * @var string $mailOrderCustomerTextTemplate;
     *
     * @Field(type="string")
     */
    protected $mailOrderCustomerTextTemplate;
/**
     * @var string $imprintText;
     *
     * @Field(type="string")
     */
    protected $imprintText;
/**
     * @var string $privacyPolicyText;
     *
     * @Field(type="string")
     */
    protected $privacyPolicyText;
/**
     * @var string $cancellationTermsText;
     *
     * @Field(type="string")
     */
    protected $cancellationTermsText;
/**
     * @var string $conditionsText;
     *
     * @Field(type="string")
     */
    protected $conditionsText;
/**
     * @Field(type="string")
     */
    protected $docJobticketBg;
/**
     * @Field(type="string")
     */
    protected $docJobticketPositionBg;
/**
     * @Field(type="string")
     */
    protected $docJobticketPPBg;
/**
     * @Field(type="string")
     */
    protected $docJobticketPPPositionBg;
/**
     * @Field(type="string")
     */
    protected $docOfferBg;
/**
     * @Field(type="string")
     */
    protected $docOfferPositionBg;
/**
     * @Field(type="string")
     */
    protected $docOrderBg;
/**
     * @Field(type="string")
     */
    protected $docOrderPositionBg;
/**
     * @Field(type="string")
     */
    protected $docInvoiceBg;
/**
     * @Field(type="string")
     */
    protected $docInvoiceFileName;
/**
     * @Field(type="string")
     */
    protected $docOfferFileName;
/**
     * @Field(type="string")
     */
    protected $docStornoFileName;
/**
     * @Field(type="string")
     */
    protected $docJobticketFileName;
/**
     * @Field(type="string")
     */
    protected $docJobticketPPFileName;
/**
     * @Field(type="string")
     */
    protected $docOrderFileName;
/**
     * @Field(type="string")
     */
    protected $docDeliveryFileName;
/**
     * @Field(type="string")
     */
    protected $docLabelFileName;
/**
     * @Field(type="string")
     */
    protected $docInvoicePositionBg;
/**
     * @Field(type="string")
     */
    protected $docLabelBg;
/**
     * @Field(type="string")
     */
    protected $docLabelPositionBg;
/**
     * @Field(type="string")
     */
    protected $docDeliveryBg;
/**
     * @Field(type="string")
     */
    protected $docDeliveryPositionBg;
/**
     * @Field(type="string")
     */
    protected $docStornoBg;
/**
     * @Field(type="string")
     */
    protected $docStornoPositionBg;
/**
     * @Field(type="hash")
     */
    protected $pluginSettings = [];
/**
     * @Field(type="string")
     */
    protected $docOfferProductBg;
/**
     * @Field(type="string")
     */
    protected $docOfferProductXml;
/**
     * @Field(type="string")
     */
    protected $senderCompany;
/**
     * @Field(type="string")
     */
    protected $senderFirstname;
/**
     * @Field(type="string")
     */
    protected $senderLastname;
/**
     * @Field(type="string")
     */
    protected $senderStreet;
/**
     * @Field(type="string")
     */
    protected $senderHouseNumber;
/**
     * @Field(type="string")
     */
    protected $senderZip;
/**
     * @Field(type="string")
     */
    protected $senderCity;
/**
     * @Field(type="string")
     */
    protected $senderCountry;
/**
     * @Field(type="string")
     */
    protected $senderTel;
/**
     * @Field(type="string")
     */
    protected $senderFax;
/**
     * @Field(type="string")
     */
    protected $senderMobile;
/**
     * @Field(type="string")
     */
    protected $senderEmail;
/**
     * @var string $extraSettings;
     *
     * @Field(type="string")
     */
    protected $extraSettings;
/**
     *
     * @Field(type="string")
     */
    protected string $routingConfigCMS = '';
/**
     *
     * @Field(type="string")
     */
    protected string $routingConfigProduct = '';
/**
     *
     * @Field(type="string")
     */
    protected string $routingConfigProductGroup = '';
    public bool $noVerify;
/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getImprintText()
    {
        return $this->imprintText;
    }

    /**
     * @param string $imprintText
     */
    public function setImprintText($imprintText)
    {
        $this->imprintText = $imprintText;
    }

    /**
     * @return string
     */
    public function getPrivacyPolicyText()
    {
        return $this->privacyPolicyText;
    }

    /**
     * @param string $privacyPolicyText
     */
    public function setPrivacyPolicyText($privacyPolicyText)
    {
        $this->privacyPolicyText = $privacyPolicyText;
    }

    /**
     * @return string
     */
    public function getCancellationTermsText()
    {
        return $this->cancellationTermsText;
    }

    /**
     * @param string $cancellationTermsText
     */
    public function setCancellationTermsText($cancellationTermsText)
    {
        $this->cancellationTermsText = $cancellationTermsText;
    }

    /**
     * @return string
     */
    public function getConditionsText()
    {
        return $this->conditionsText;
    }

    /**
     * @param string $conditionsText
     */
    public function setConditionsText($conditionsText)
    {
        $this->conditionsText = $conditionsText;
    }



    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getNumberPattern()
    {
        return $this->numberPattern;
    }

    /**
     * @param string $numberPattern
     */
    public function setNumberPattern($numberPattern)
    {
        $this->numberPattern = $numberPattern;
    }

    /**
     * @return mixed
     */
    public function getNumberStart()
    {
        return $this->numberStart;
    }

    /**
     * @param mixed $numberStart
     */
    public function setNumberStart($numberStart)
    {
        $this->numberStart = $numberStart;
    }

    /**
     * @return mixed
     */
    public function getOwnNumber()
    {
        return $this->ownNumber;
    }

    /**
     * @param mixed $ownNumber
     */
    public function setOwnNumber($ownNumber)
    {
        $this->ownNumber = $ownNumber;
    }

    /**
     * @return string
     */
    public function getContactNumberPattern()
    {
        return $this->contactNumberPattern;
    }

    /**
     * @param string $contactNumberPattern
     */
    public function setContactNumberPattern($contactNumberPattern)
    {
        $this->contactNumberPattern = $contactNumberPattern;
    }

    /**
     * @return int
     */
    public function getContactNumberStart()
    {
        return $this->contactNumberStart;
    }

    /**
     * @param int $contactNumberStart
     */
    public function setContactNumberStart($contactNumberStart)
    {
        $this->contactNumberStart = $contactNumberStart;
    }

    /**
     * @return boolean
     */
    public function getContactOwnNumber()
    {
        return $this->contactOwnNumber;
    }

    /**
     * @return boolean
     */
    public function isContactOwnNumber()
    {
        return $this->contactOwnNumber;
    }

    /**
     * @param boolean $contactOwnNumber
     */
    public function setContactOwnNumber($contactOwnNumber)
    {
        $this->contactOwnNumber = $contactOwnNumber;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
     * @param string $apiKey
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
    }

    /**
     * @return mixed
     */
    public function getDocInvoiceJasperXml()
    {
        return $this->docInvoiceJasperXml;
    }

    /**
     * @param mixed $docInvoiceJasperXml
     */
    public function setDocInvoiceJasperXml($docInvoiceJasperXml)
    {
        $this->docInvoiceJasperXml = $docInvoiceJasperXml;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketJasperXml()
    {
        return $this->docJobticketJasperXml;
    }

    /**
     * @param mixed $docJobticketJasperXml
     */
    public function setDocJobticketJasperXml($docJobticketJasperXml)
    {
        $this->docJobticketJasperXml = $docJobticketJasperXml;
    }

    /**
     * @return mixed
     */
    public function getDocOfferJasperXml()
    {
        return $this->docOfferJasperXml;
    }

    /**
     * @param mixed $docOfferJasperXml
     */
    public function setDocOfferJasperXml($docOfferJasperXml)
    {
        $this->docOfferJasperXml = $docOfferJasperXml;
    }

    /**
     * @return mixed
     */
    public function getDocOrderJasperXml()
    {
        return $this->docOrderJasperXml;
    }

    /**
     * @param mixed $docOrderJasperXml
     */
    public function setDocOrderJasperXml($docOrderJasperXml)
    {
        $this->docOrderJasperXml = $docOrderJasperXml;
    }

    /**
     * @return mixed
     */
    public function getDocLabelJasperXml()
    {
        return $this->docLabelJasperXml;
    }

    /**
     * @param mixed $docLabelJasperXml
     */
    public function setDocLabelJasperXml($docLabelJasperXml)
    {
        $this->docLabelJasperXml = $docLabelJasperXml;
    }

    /**
     * @return mixed
     */
    public function getDocDeliveryJasperXml()
    {
        return $this->docDeliveryJasperXml;
    }

    /**
     * @param mixed $docDeliveryJasperXml
     */
    public function setDocDeliveryJasperXml($docDeliveryJasperXml)
    {
        $this->docDeliveryJasperXml = $docDeliveryJasperXml;
    }

    /**
     * @return mixed
     */
    public function getRedirectAfterBuy()
    {
        return $this->redirectAfterBuy;
    }

    /**
     * @param mixed $redirectAfterBuy
     */
    public function setRedirectAfterBuy($redirectAfterBuy)
    {
        $this->redirectAfterBuy = $redirectAfterBuy;
    }

    /**
     * @return mixed
     */
    public function getDocStornoJasperXml()
    {
        return $this->docStornoJasperXml;
    }

    /**
     * @param mixed $docStornoJasperXml
     */
    public function setDocStornoJasperXml($docStornoJasperXml)
    {
        $this->docStornoJasperXml = $docStornoJasperXml;
    }

    /**
     * @return mixed
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * @param mixed $theme
     */
    public function setTheme($theme)
    {
        $this->theme = $theme;
    }

    /**
     * @return boolean
     */
    public function isCreatePackageAfterBuy()
    {
        return $this->createPackageAfterBuy;
    }

    /**
     * @param boolean $createPackageAfterBuy
     */
    public function setCreatePackageAfterBuy($createPackageAfterBuy)
    {
        $this->createPackageAfterBuy = $createPackageAfterBuy;
    }

    /**
     * @return boolean
     */
    public function isRedirectToSSL()
    {
        return $this->redirectToSSL;
    }

    /**
     * @param boolean $redirectToSSL
     */
    public function setRedirectToSSL($redirectToSSL)
    {
        $this->redirectToSSL = $redirectToSSL;
    }

    /**
     * @return string
     */
    public function getProductFieldName1()
    {
        return $this->productFieldName1;
    }

    /**
     * @param string $productFieldName1
     */
    public function setProductFieldName1($productFieldName1)
    {
        $this->productFieldName1 = $productFieldName1;
    }

    /**
     * @return string
     */
    public function getProductFieldName2()
    {
        return $this->productFieldName2;
    }

    /**
     * @param string $productFieldName2
     */
    public function setProductFieldName2($productFieldName2)
    {
        $this->productFieldName2 = $productFieldName2;
    }

    /**
     * @return string
     */
    public function getProductFieldName3()
    {
        return $this->productFieldName3;
    }

    /**
     * @param string $productFieldName3
     */
    public function setProductFieldName3($productFieldName3)
    {
        $this->productFieldName3 = $productFieldName3;
    }

    /**
     * @return string
     */
    public function getProductFieldName4()
    {
        return $this->productFieldName4;
    }

    /**
     * @param string $productFieldName4
     */
    public function setProductFieldName4($productFieldName4)
    {
        $this->productFieldName4 = $productFieldName4;
    }

    /**
     * @return string
     */
    public function getProductFieldName5()
    {
        return $this->productFieldName5;
    }

    /**
     * @param string $productFieldName5
     */
    public function setProductFieldName5($productFieldName5)
    {
        $this->productFieldName5 = $productFieldName5;
    }

    /**
     * @return string
     */
    public function getProductFieldName6()
    {
        return $this->productFieldName6;
    }

    /**
     * @param string $productFieldName6
     */
    public function setProductFieldName6($productFieldName6)
    {
        $this->productFieldName6 = $productFieldName6;
    }

    /**
     * @return string
     */
    public function getProductFieldName7()
    {
        return $this->productFieldName7;
    }

    /**
     * @param string $productFieldName7
     */
    public function setProductFieldName7($productFieldName7)
    {
        $this->productFieldName7 = $productFieldName7;
    }

    /**
     * @return string
     */
    public function getProductFieldName8()
    {
        return $this->productFieldName8;
    }

    /**
     * @param string $productFieldName8
     */
    public function setProductFieldName8($productFieldName8)
    {
        $this->productFieldName8 = $productFieldName8;
    }

    /**
     * @return string
     */
    public function getProductFieldName9()
    {
        return $this->productFieldName9;
    }

    /**
     * @param string $productFieldName9
     */
    public function setProductFieldName9($productFieldName9)
    {
        $this->productFieldName9 = $productFieldName9;
    }

    /**
     * @return string
     */
    public function getProductFieldName10()
    {
        return $this->productFieldName10;
    }

    /**
     * @param string $productFieldName10
     */
    public function setProductFieldName10($productFieldName10)
    {
        $this->productFieldName10 = $productFieldName10;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName1()
    {
        return $this->customerFieldName1;
    }

    /**
     * @param string $customerFieldName1
     */
    public function setCustomerFieldName1($customerFieldName1)
    {
        $this->customerFieldName1 = $customerFieldName1;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName2()
    {
        return $this->customerFieldName2;
    }

    /**
     * @param string $customerFieldName2
     */
    public function setCustomerFieldName2($customerFieldName2)
    {
        $this->customerFieldName2 = $customerFieldName2;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName3()
    {
        return $this->customerFieldName3;
    }

    /**
     * @param string $customerFieldName3
     */
    public function setCustomerFieldName3($customerFieldName3)
    {
        $this->customerFieldName3 = $customerFieldName3;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName4()
    {
        return $this->customerFieldName4;
    }

    /**
     * @param string $customerFieldName4
     */
    public function setCustomerFieldName4($customerFieldName4)
    {
        $this->customerFieldName4 = $customerFieldName4;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName5()
    {
        return $this->customerFieldName5;
    }

    /**
     * @param string $customerFieldName5
     */
    public function setCustomerFieldName5($customerFieldName5)
    {
        $this->customerFieldName5 = $customerFieldName5;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName6()
    {
        return $this->customerFieldName6;
    }

    /**
     * @param string $customerFieldName6
     */
    public function setCustomerFieldName6($customerFieldName6)
    {
        $this->customerFieldName6 = $customerFieldName6;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName7()
    {
        return $this->customerFieldName7;
    }

    /**
     * @param string $customerFieldName7
     */
    public function setCustomerFieldName7($customerFieldName7)
    {
        $this->customerFieldName7 = $customerFieldName7;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName8()
    {
        return $this->customerFieldName8;
    }

    /**
     * @param string $customerFieldName8
     */
    public function setCustomerFieldName8($customerFieldName8)
    {
        $this->customerFieldName8 = $customerFieldName8;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName9()
    {
        return $this->customerFieldName9;
    }

    /**
     * @param string $customerFieldName9
     */
    public function setCustomerFieldName9($customerFieldName9)
    {
        $this->customerFieldName9 = $customerFieldName9;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName10()
    {
        return $this->customerFieldName10;
    }

    /**
     * @param string $customerFieldName10
     */
    public function setCustomerFieldName10($customerFieldName10)
    {
        $this->customerFieldName10 = $customerFieldName10;
    }

    /**
     * @return string
     */
    public function getProductFieldName11()
    {
        return $this->productFieldName11;
    }

    /**
     * @param string $productFieldName11
     */
    public function setProductFieldName11($productFieldName11)
    {
        $this->productFieldName11 = $productFieldName11;
    }

    /**
     * @return string
     */
    public function getProductFieldName12()
    {
        return $this->productFieldName12;
    }

    /**
     * @param string $productFieldName12
     */
    public function setProductFieldName12($productFieldName12)
    {
        $this->productFieldName12 = $productFieldName12;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName11()
    {
        return $this->customerFieldName11;
    }

    /**
     * @param string $customerFieldName11
     */
    public function setCustomerFieldName11($customerFieldName11)
    {
        $this->customerFieldName11 = $customerFieldName11;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName12()
    {
        return $this->customerFieldName12;
    }

    /**
     * @param string $customerFieldName12
     */
    public function setCustomerFieldName12($customerFieldName12)
    {
        $this->customerFieldName12 = $customerFieldName12;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName13()
    {
        return $this->customerFieldName13;
    }

    /**
     * @param string $customerFieldName13
     */
    public function setCustomerFieldName13($customerFieldName13)
    {
        $this->customerFieldName13 = $customerFieldName13;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName14()
    {
        return $this->customerFieldName14;
    }

    /**
     * @param string $customerFieldName14
     */
    public function setCustomerFieldName14($customerFieldName14)
    {
        $this->customerFieldName14 = $customerFieldName14;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName15()
    {
        return $this->customerFieldName15;
    }

    /**
     * @param string $customerFieldName15
     */
    public function setCustomerFieldName15($customerFieldName15)
    {
        $this->customerFieldName15 = $customerFieldName15;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName16()
    {
        return $this->customerFieldName16;
    }

    /**
     * @param string $customerFieldName16
     */
    public function setCustomerFieldName16($customerFieldName16)
    {
        $this->customerFieldName16 = $customerFieldName16;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName17()
    {
        return $this->customerFieldName17;
    }

    /**
     * @param string $customerFieldName17
     */
    public function setCustomerFieldName17($customerFieldName17)
    {
        $this->customerFieldName17 = $customerFieldName17;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName18()
    {
        return $this->customerFieldName18;
    }

    /**
     * @param string $customerFieldName18
     */
    public function setCustomerFieldName18($customerFieldName18)
    {
        $this->customerFieldName18 = $customerFieldName18;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName19()
    {
        return $this->customerFieldName19;
    }

    /**
     * @param string $customerFieldName19
     */
    public function setCustomerFieldName19($customerFieldName19)
    {
        $this->customerFieldName19 = $customerFieldName19;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName20()
    {
        return $this->customerFieldName20;
    }

    /**
     * @param string $customerFieldName20
     */
    public function setCustomerFieldName20($customerFieldName20)
    {
        $this->customerFieldName20 = $customerFieldName20;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName21()
    {
        return $this->customerFieldName21;
    }

    /**
     * @param string $customerFieldName21
     */
    public function setCustomerFieldName21($customerFieldName21)
    {
        $this->customerFieldName21 = $customerFieldName21;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName22()
    {
        return $this->customerFieldName22;
    }

    /**
     * @param string $customerFieldName22
     */
    public function setCustomerFieldName22($customerFieldName22)
    {
        $this->customerFieldName22 = $customerFieldName22;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName23()
    {
        return $this->customerFieldName23;
    }

    /**
     * @param string $customerFieldName23
     */
    public function setCustomerFieldName23($customerFieldName23)
    {
        $this->customerFieldName23 = $customerFieldName23;
    }

    /**
     * @return string
     */
    public function getCustomerFieldName24()
    {
        return $this->customerFieldName24;
    }

    /**
     * @param string $customerFieldName24
     */
    public function setCustomerFieldName24($customerFieldName24)
    {
        $this->customerFieldName24 = $customerFieldName24;
    }

    /**
     * @return mixed
     */
    public function getLayouterTheme()
    {
        if ($this->layouterTheme == '') {
            return 'psc';
        }
        return $this->layouterTheme;
    }

    /**
     * @param mixed $layouterTheme
     */
    public function setLayouterTheme($layouterTheme)
    {
        $this->layouterTheme = $layouterTheme;
    }

    /**
     * @return string
     */
    public function getMailOrderCustomerSubjectTemplate()
    {
        return $this->mailOrderCustomerSubjectTemplate;
    }

    /**
     * @param string $mailOrderCustomerSubjectTemplate
     */
    public function setMailOrderCustomerSubjectTemplate($mailOrderCustomerSubjectTemplate)
    {
        $this->mailOrderCustomerSubjectTemplate = $mailOrderCustomerSubjectTemplate;
    }

    /**
     * @return string
     */
    public function getMailOrderCustomerTextTemplate()
    {
        return $this->mailOrderCustomerTextTemplate;
    }

    /**
     * @param string $mailOrderCustomerTextTemplate
     */
    public function setMailOrderCustomerTextTemplate($mailOrderCustomerTextTemplate)
    {
        $this->mailOrderCustomerTextTemplate = $mailOrderCustomerTextTemplate;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketPPJasperXml()
    {
        return $this->docJobticketPPJasperXml;
    }

    /**
     * @param mixed $docJobticketPPJasperXml
     */
    public function setDocJobticketPPJasperXml($docJobticketPPJasperXml)
    {
        $this->docJobticketPPJasperXml = $docJobticketPPJasperXml;
    }

    /**
     * @return mixed
     */
    public function getDocInvoicePosition()
    {
        return $this->docInvoicePosition;
    }

    /**
     * @param mixed $docInvoicePosition
     */
    public function setDocInvoicePosition($docInvoicePosition)
    {
        $this->docInvoicePosition = $docInvoicePosition;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketPosition()
    {
        return $this->docJobticketPosition;
    }

    /**
     * @param mixed $docJobticketPosition
     */
    public function setDocJobticketPosition($docJobticketPosition)
    {
        $this->docJobticketPosition = $docJobticketPosition;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketPPPosition()
    {
        return $this->docJobticketPPPosition;
    }

    /**
     * @param mixed $docJobticketPPPosition
     */
    public function setDocJobticketPPPosition($docJobticketPPPosition)
    {
        $this->docJobticketPPPosition = $docJobticketPPPosition;
    }

    /**
     * @return mixed
     */
    public function getDocOfferPosition()
    {
        return $this->docOfferPosition;
    }

    /**
     * @param mixed $docOfferPosition
     */
    public function setDocOfferPosition($docOfferPosition)
    {
        $this->docOfferPosition = $docOfferPosition;
    }

    /**
     * @return mixed
     */
    public function getDocOrderPosition()
    {
        return $this->docOrderPosition;
    }

    /**
     * @param mixed $docOrderPosition
     */
    public function setDocOrderPosition($docOrderPosition)
    {
        $this->docOrderPosition = $docOrderPosition;
    }

    /**
     * @return mixed
     */
    public function getDocLabelPosition()
    {
        return $this->docLabelPosition;
    }

    /**
     * @param mixed $docLabelPosition
     */
    public function setDocLabelPosition($docLabelPosition)
    {
        $this->docLabelPosition = $docLabelPosition;
    }

    /**
     * @return mixed
     */
    public function getDocDeliveryPosition()
    {
        return $this->docDeliveryPosition;
    }

    /**
     * @param mixed $docDeliveryPosition
     */
    public function setDocDeliveryPosition($docDeliveryPosition)
    {
        $this->docDeliveryPosition = $docDeliveryPosition;
    }

    /**
     * @return mixed
     */
    public function getDocStornoPosition()
    {
        return $this->docStornoPosition;
    }

    /**
     * @param mixed $docStornoPosition
     */
    public function setDocStornoPosition($docStornoPosition)
    {
        $this->docStornoPosition = $docStornoPosition;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketBg()
    {
        return $this->docJobticketBg;
    }

    /**
     * @param mixed $docJobticketBg
     */
    public function setDocJobticketBg($docJobticketBg)
    {
        $this->docJobticketBg = $docJobticketBg;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketPositionBg()
    {
        return $this->docJobticketPositionBg;
    }

    /**
     * @param mixed $docJobticketPositionBg
     */
    public function setDocJobticketPositionBg($docJobticketPositionBg)
    {
        $this->docJobticketPositionBg = $docJobticketPositionBg;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketPPBg()
    {
        return $this->docJobticketPPBg;
    }

    /**
     * @param mixed $docJobticketPPBg
     */
    public function setDocJobticketPPBg($docJobticketPPBg)
    {
        $this->docJobticketPPBg = $docJobticketPPBg;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketPPPositionBg()
    {
        return $this->docJobticketPPPositionBg;
    }

    /**
     * @param mixed $docJobticketPPPositionBg
     */
    public function setDocJobticketPPPositionBg($docJobticketPPPositionBg)
    {
        $this->docJobticketPPPositionBg = $docJobticketPPPositionBg;
    }

    /**
     * @return mixed
     */
    public function getDocOfferBg()
    {
        return $this->docOfferBg;
    }

    /**
     * @param mixed $docOfferBg
     */
    public function setDocOfferBg($docOfferBg)
    {
        $this->docOfferBg = $docOfferBg;
    }

    /**
     * @return mixed
     */
    public function getDocOfferPositionBg()
    {
        return $this->docOfferPositionBg;
    }

    /**
     * @param mixed $docOfferPositionBg
     */
    public function setDocOfferPositionBg($docOfferPositionBg)
    {
        $this->docOfferPositionBg = $docOfferPositionBg;
    }

    /**
     * @return mixed
     */
    public function getDocOrderBg()
    {
        return $this->docOrderBg;
    }

    /**
     * @param mixed $docOrderBg
     */
    public function setDocOrderBg($docOrderBg)
    {
        $this->docOrderBg = $docOrderBg;
    }

    /**
     * @return mixed
     */
    public function getDocOrderPositionBg()
    {
        return $this->docOrderPositionBg;
    }

    /**
     * @param mixed $docOrderPositionBg
     */
    public function setDocOrderPositionBg($docOrderPositionBg)
    {
        $this->docOrderPositionBg = $docOrderPositionBg;
    }

    /**
     * @return mixed
     */
    public function getDocInvoiceBg()
    {
        return $this->docInvoiceBg;
    }

    /**
     * @param mixed $docInvoiceBg
     */
    public function setDocInvoiceBg($docInvoiceBg)
    {
        $this->docInvoiceBg = $docInvoiceBg;
    }

    /**
     * @return mixed
     */
    public function getDocInvoicePositionBg()
    {
        return $this->docInvoicePositionBg;
    }

    /**
     * @param mixed $docInvoicePositionBg
     */
    public function setDocInvoicePositionBg($docInvoicePositionBg)
    {
        $this->docInvoicePositionBg = $docInvoicePositionBg;
    }

    /**
     * @return mixed
     */
    public function getDocLabelBg()
    {
        return $this->docLabelBg;
    }

    /**
     * @param mixed $docLabelBg
     */
    public function setDocLabelBg($docLabelBg)
    {
        $this->docLabelBg = $docLabelBg;
    }

    /**
     * @return mixed
     */
    public function getDocLabelPositionBg()
    {
        return $this->docLabelPositionBg;
    }

    /**
     * @param mixed $docLabelPositionBg
     */
    public function setDocLabelPositionBg($docLabelPositionBg)
    {
        $this->docLabelPositionBg = $docLabelPositionBg;
    }

    /**
     * @return mixed
     */
    public function getDocDeliveryBg()
    {
        return $this->docDeliveryBg;
    }

    /**
     * @param mixed $docDeliveryBg
     */
    public function setDocDeliveryBg($docDeliveryBg)
    {
        $this->docDeliveryBg = $docDeliveryBg;
    }

    /**
     * @return mixed
     */
    public function getDocDeliveryPositionBg()
    {
        return $this->docDeliveryPositionBg;
    }

    /**
     * @param mixed $docDeliveryPositionBg
     */
    public function setDocDeliveryPositionBg($docDeliveryPositionBg)
    {
        $this->docDeliveryPositionBg = $docDeliveryPositionBg;
    }

    /**
     * @return mixed
     */
    public function getDocStornoBg()
    {
        return $this->docStornoBg;
    }

    /**
     * @param mixed $docStornoBg
     */
    public function setDocStornoBg($docStornoBg)
    {
        $this->docStornoBg = $docStornoBg;
    }

    /**
     * @return mixed
     */
    public function getDocStornoPositionBg()
    {
        return $this->docStornoPositionBg;
    }

    /**
     * @param mixed $docStornoPositionBg
     */
    public function setDocStornoPositionBg($docStornoPositionBg)
    {
        $this->docStornoPositionBg = $docStornoPositionBg;
    }

    /**
     * @return mixed
     */
    public function getProductSortDir()
    {
        return $this->productSortDir;
    }

    /**
     * @param mixed $productSortDir
     */
    public function setProductSortDir($productSortDir)
    {
        $this->productSortDir = $productSortDir;
    }

    /**
     * @return mixed
     */
    public function getProductSort()
    {
        return $this->productSort;
    }

    /**
     * @param mixed $productSort
     */
    public function setProductSort($productSort)
    {
        $this->productSort = $productSort;
    }

    /**
     * @return array
     */
    public function getPluginSettings()
    {
        return $this->pluginSettings;
    }

    /**
     * @param array $pluginSettings
     */
    public function setPluginSettings($pluginSettings)
    {
        $this->pluginSettings = $pluginSettings;
    }

    /**
     * @param $module
     * @param $key
     * @param $value
     */
    public function setPluginSettingModule($module, $key, $value)
    {
        $this->pluginSettings[$module ] [$key] = $value;
    }

    public function getPluginSettingModule($module, $key)
    {
        if (!isset($this->pluginSettings[$module ]) || !isset($this->pluginSettings[$module ] [$key])) {
            return null;
        }

        return $this->pluginSettings[$module ] [$key];
    }

    /**
     * @return mixed
     */
    public function getDocOfferProductBg()
    {
        return $this->docOfferProductBg;
    }

    /**
     * @param mixed $docOfferProductBg
     */
    public function setDocOfferProductBg($docOfferProductBg)
    {
        $this->docOfferProductBg = $docOfferProductBg;
    }

    /**
     * @return mixed
     */
    public function getDocOfferProductXml()
    {
        return $this->docOfferProductXml;
    }

    /**
     * @param mixed $docOfferProductXml
     */
    public function setDocOfferProductXml($docOfferProductXml)
    {
        $this->docOfferProductXml = $docOfferProductXml;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSubTitle()
    {
        return $this->subTitle;
    }

    /**
     * @param mixed $subTitle
     */
    public function setSubTitle($subTitle)
    {
        $this->subTitle = $subTitle;
    }

    /**
     * @return mixed
     */
    public function getKeywords()
    {
        return $this->keywords;
    }

    /**
     * @param mixed $keywords
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * @param mixed $copyright
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getPrivate()
    {
        return $this->private;
    }

    /**
     * @param mixed $private
     */
    public function setPrivate($private)
    {
        $this->private = $private;
    }

    /**
     * @return mixed
     */
    public function getRegistration()
    {
        return $this->registration;
    }

    /**
     * @param mixed $registration
     */
    public function setRegistration($registration)
    {
        $this->registration = $registration;
    }

    /**
     * @return mixed
     */
    public function getBetreiberCompany()
    {
        return $this->betreiberCompany;
    }

    /**
     * @param mixed $betreiberCompany
     */
    public function setBetreiberCompany($betreiberCompany)
    {
        $this->betreiberCompany = $betreiberCompany;
    }

    /**
     * @return mixed
     */
    public function getBetreiberName()
    {
        return $this->betreiberName;
    }

    /**
     * @param mixed $betreiberName
     */
    public function setBetreiberName($betreiberName)
    {
        $this->betreiberName = $betreiberName;
    }

    /**
     * @return mixed
     */
    public function getBetreiberStreet()
    {
        return $this->betreiberStreet;
    }

    /**
     * @param mixed $betreiberStreet
     */
    public function setBetreiberStreet($betreiberStreet)
    {
        $this->betreiberStreet = $betreiberStreet;
    }

    /**
     * @return mixed
     */
    public function getBetreiberAddress()
    {
        return $this->betreiberAddress;
    }

    /**
     * @param mixed $betreiberAddress
     */
    public function setBetreiberAddress($betreiberAddress)
    {
        $this->betreiberAddress = $betreiberAddress;
    }

    /**
     * @return mixed
     */
    public function getBetreiberEmail()
    {
        return $this->betreiberEmail;
    }

    /**
     * @param mixed $betreiberEmail
     */
    public function setBetreiberEmail($betreiberEmail)
    {
        $this->betreiberEmail = $betreiberEmail;
    }

    /**
     * @return mixed
     */
    public function getBetreiberTel()
    {
        return $this->betreiberTel;
    }

    /**
     * @param mixed $betreiberTel
     */
    public function setBetreiberTel($betreiberTel)
    {
        $this->betreiberTel = $betreiberTel;
    }

    /**
     * @return mixed
     */
    public function getBetreiberUid()
    {
        return $this->betreiberUid;
    }

    /**
     * @param mixed $betreiberUid
     */
    public function setBetreiberUid($betreiberUid)
    {
        $this->betreiberUid = $betreiberUid;
    }

    /**
     * @return mixed
     */
    public function getBetreiberHid()
    {
        return $this->betreiberHid;
    }

    /**
     * @param mixed $betreiberHid
     */
    public function setBetreiberHid($betreiberHid)
    {
        $this->betreiberHid = $betreiberHid;
    }

    /**
     * @return mixed
     */
    public function getBetreiberSid()
    {
        return $this->betreiberSid;
    }

    /**
     * @param mixed $betreiberSid
     */
    public function setBetreiberSid($betreiberSid)
    {
        $this->betreiberSid = $betreiberSid;
    }

    /**
     * @return mixed
     */
    public function getBetreiberRegister()
    {
        return $this->betreiberRegister;
    }

    /**
     * @param mixed $betreiberRegister
     */
    public function setBetreiberRegister($betreiberRegister)
    {
        $this->betreiberRegister = $betreiberRegister;
    }

    /**
     * @return mixed
     */
    public function getBetreiberVb()
    {
        return $this->betreiberVb;
    }

    /**
     * @param mixed $betreiberVb
     */
    public function setBetreiberVb($betreiberVb)
    {
        $this->betreiberVb = $betreiberVb;
    }

    /**
     * @return mixed
     */
    public function getBetreiberRechtsform()
    {
        return $this->betreiberRechtsform;
    }

    /**
     * @param mixed $betreiberRechtsform
     */
    public function setBetreiberRechtsform($betreiberRechtsform)
    {
        $this->betreiberRechtsform = $betreiberRechtsform;
    }

    /**
     * @return mixed
     */
    public function getBetreiberWeb()
    {
        return $this->betreiberWeb;
    }

    /**
     * @param mixed $betreiberWeb
     */
    public function setBetreiberWeb($betreiberWeb)
    {
        $this->betreiberWeb = $betreiberWeb;
    }

    /**
     * @return mixed
     */
    public function getLogo1()
    {
        return $this->logo1;
    }

    /**
     * @param mixed $logo1
     */
    public function setLogo1($logo1)
    {
        $this->logo1 = $logo1;
    }

    /**
     * @return mixed
     */
    public function getLogo2()
    {
        return $this->logo2;
    }

    /**
     * @param mixed $logo2
     */
    public function setLogo2($logo2)
    {
        $this->logo2 = $logo2;
    }

    /**
     * @return mixed
     */
    public function getDocInvoiceFileName()
    {
        return $this->docInvoiceFileName;
    }

    /**
     * @param mixed $docInvoiceFileName
     */
    public function setDocInvoiceFileName($docInvoiceFileName)
    {
        $this->docInvoiceFileName = $docInvoiceFileName;
    }

    /**
     * @return mixed
     */
    public function getDocInvoiceBg2()
    {
        return $this->docInvoiceBg2;
    }

    /**
     * @param mixed $docInvoiceBg2
     */
    public function setDocInvoiceBg2($docInvoiceBg2)
    {
        $this->docInvoiceBg2 = $docInvoiceBg2;
    }

    /**
     * @return mixed
     */
    public function getDocOfferFileName()
    {
        return $this->docOfferFileName;
    }

    /**
     * @param mixed $docOfferFileName
     */
    public function setDocOfferFileName($docOfferFileName)
    {
        $this->docOfferFileName = $docOfferFileName;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketFileName()
    {
        return $this->docJobticketFileName;
    }

    /**
     * @param mixed $docJobticketFileName
     */
    public function setDocJobticketFileName($docJobticketFileName)
    {
        $this->docJobticketFileName = $docJobticketFileName;
    }

    /**
     * @return mixed
     */
    public function getDocJobticketPPFileName()
    {
        return $this->docJobticketPPFileName;
    }

    /**
     * @param mixed $docJobticketPPFileName
     */
    public function setDocJobticketPPFileName($docJobticketPPFileName)
    {
        $this->docJobticketPPFileName = $docJobticketPPFileName;
    }

    /**
     * @return mixed
     */
    public function getDocOrderFileName()
    {
        return $this->docOrderFileName;
    }

    /**
     * @param mixed $docOrderFileName
     */
    public function setDocOrderFileName($docOrderFileName)
    {
        $this->docOrderFileName = $docOrderFileName;
    }

    /**
     * @return mixed
     */
    public function getDocDeliveryFileName()
    {
        return $this->docDeliveryFileName;
    }

    /**
     * @param mixed $docDeliveryFileName
     */
    public function setDocDeliveryFileName($docDeliveryFileName)
    {
        $this->docDeliveryFileName = $docDeliveryFileName;
    }

    /**
     * @return mixed
     */
    public function getDocLabelFileName()
    {
        return $this->docLabelFileName;
    }

    /**
     * @param mixed $docLabelFileName
     */
    public function setDocLabelFileName($docLabelFileName)
    {
        $this->docLabelFileName = $docLabelFileName;
    }

    /**
     * @return mixed
     */
    public function getDocStornoFileName()
    {
        return $this->docStornoFileName;
    }

    /**
     * @param mixed $docStornoFileName
     */
    public function setDocStornoFileName($docStornoFileName)
    {
        $this->docStornoFileName = $docStornoFileName;
    }

    /**
     * @return mixed
     */
    public function getBetreiberFax()
    {
        return $this->betreiberFax;
    }

    /**
     * @param mixed $betreiberFax
     */
    public function setBetreiberFax($betreiberFax)
    {
        $this->betreiberFax = $betreiberFax;
    }

    /**
     * @return mixed
     */
    public function getSenderCompany()
    {
        return $this->senderCompany;
    }

    /**
     * @param mixed $senderCompany
     */
    public function setSenderCompany($senderCompany)
    {
        $this->senderCompany = $senderCompany;
    }

    /**
     * @return mixed
     */
    public function getSenderFirstname()
    {
        return $this->senderFirstname;
    }

    /**
     * @param mixed $senderFirstname
     */
    public function setSenderFirstname($senderFirstname)
    {
        $this->senderFirstname = $senderFirstname;
    }

    /**
     * @return mixed
     */
    public function getSenderLastname()
    {
        return $this->senderLastname;
    }

    /**
     * @param mixed $senderLastname
     */
    public function setSenderLastname($senderLastname)
    {
        $this->senderLastname = $senderLastname;
    }

    /**
     * @return mixed
     */
    public function getSenderStreet()
    {
        return $this->senderStreet;
    }

    /**
     * @param mixed $senderStreet
     */
    public function setSenderStreet($senderStreet)
    {
        $this->senderStreet = $senderStreet;
    }

    /**
     * @return mixed
     */
    public function getSenderHouseNumber()
    {
        return $this->senderHouseNumber;
    }

    /**
     * @param mixed $senderHouseNumber
     */
    public function setSenderHouseNumber($senderHouseNumber)
    {
        $this->senderHouseNumber = $senderHouseNumber;
    }

    /**
     * @return mixed
     */
    public function getSenderZip()
    {
        return $this->senderZip;
    }

    /**
     * @param mixed $senderZip
     */
    public function setSenderZip($senderZip)
    {
        $this->senderZip = $senderZip;
    }

    /**
     * @return mixed
     */
    public function getSenderCity()
    {
        return $this->senderCity;
    }

    /**
     * @param mixed $senderCity
     */
    public function setSenderCity($senderCity)
    {
        $this->senderCity = $senderCity;
    }

    /**
     * @return mixed
     */
    public function getSenderCountry()
    {
        return $this->senderCountry;
    }

    /**
     * @param mixed $senderCountry
     */
    public function setSenderCountry($senderCountry)
    {
        $this->senderCountry = $senderCountry;
    }

    /**
     * @return mixed
     */
    public function getSenderTel()
    {
        return $this->senderTel;
    }

    /**
     * @param mixed $senderTel
     */
    public function setSenderTel($senderTel)
    {
        $this->senderTel = $senderTel;
    }

    /**
     * @return mixed
     */
    public function getSenderFax()
    {
        return $this->senderFax;
    }

    /**
     * @param mixed $senderFax
     */
    public function setSenderFax($senderFax)
    {
        $this->senderFax = $senderFax;
    }

    /**
     * @return mixed
     */
    public function getSenderMobile()
    {
        return $this->senderMobile;
    }

    /**
     * @param mixed $senderMobile
     */
    public function setSenderMobile($senderMobile)
    {
        $this->senderMobile = $senderMobile;
    }

    /**
     * @return mixed
     */
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }

    /**
     * @param mixed $senderEmail
     */
    public function setSenderEmail($senderEmail)
    {
        $this->senderEmail = $senderEmail;
    }

    /**
     * @return bool
     */
    public function isTpSaveUserData()
    {
        return $this->tpSaveUserData;
    }

    /**
     * @param bool $tpSaveUserData
     */
    public function setTpSaveUserData($tpSaveUserData)
    {
        $this->tpSaveUserData = $tpSaveUserData;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
    }

    /**
     * @return string
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param string $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return string
     */
    public function getOfferNumberPattern(): string
    {
        return $this->offerNumberPattern;
    }

    /**
     * @param string $offerNumberPattern
     */
    public function setOfferNumberPattern(string $offerNumberPattern): void
    {
        $this->offerNumberPattern = $offerNumberPattern;
    }

    /**
     * @return int
     */
    public function getOfferNumberStart(): int
    {
        return $this->offerNumberStart;
    }

    /**
     * @param int $offerNumberStart
     */
    public function setOfferNumberStart(int $offerNumberStart): void
    {
        $this->offerNumberStart = $offerNumberStart;
    }

    /**
     * @return bool
     */
    public function isOfferOwnNumber(): bool
    {
        return $this->offerOwnNumber;
    }

    /**
     * @param bool $offerOwnNumber
     */
    public function setOfferOwnNumber(bool $offerOwnNumber): void
    {
        $this->offerOwnNumber = $offerOwnNumber;
    }

    /**
     * @return float
     */
    public function getPriceFactor(): float
    {
        if (!$this->priceFactor) {
            return 1;
        }
        return $this->priceFactor;
    }

    /**
     * @param float $priceFactor
     */
    public function setPriceFactor(float $priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }

    /**
     * @return bool
     */
    public function isGuestEnable(): bool
    {
        return boolval($this->guestEnable);
    }

    /**
     * @param bool $guestEnable
     */
    public function setGuestEnable(bool $guestEnable): void
    {
        $this->guestEnable = $guestEnable;
    }

    public function setDefaultCurrency(string $var): void
    {
        $this->defaultCurrency = $var;
    }

    public function getDefaultCurrency(): string
    {
        return $this->defaultCurrency;
    }

    public function getDefaultLocale(): string
    {
        return $this->defaultLocale;
    }

    public function setDefaultLocale(string $var): void
    {
        $this->defaultLocale = $var;
    }

    public function getRoutingConfigCMS(): string
    {
        return $this->routingConfigCMS;
    }

    public function setRoutingConfigCMS(string $var): void
    {
        $this->routingConfigCMS = $var;
    }

    public function getRoutingConfigProduct(): string
    {
        return $this->routingConfigProduct;
    }

    public function setRoutingConfigProduct(string $var): void
    {
        $this->routingConfigProduct = $var;
    }

    public function getRoutingConfigProductGroup(): string
    {
        return $this->routingConfigProductGroup;
    }

    public function setRoutingConfigProductGroup(string $var): void
    {
        $this->routingConfigProductGroup = $var;
    }
}
