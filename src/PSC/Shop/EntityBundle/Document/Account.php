<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document
 */
class Account
{
    /**
     * @var string $id
     *
     * @Id
     */
    protected $id;
/**
     * @var string $uid
     *
     * @Field(type="string")
     */
    protected $uid;
/**
     * @var string $calcValue1;
     *
     * @Field(type="string")
     */
    protected $calcValue1;
/**
     * @var string $calcValue2;
     *
     * @Field(type="string")
     */
    protected $calcValue2;
/**
     * @var string $extraSettings;
     *
     * @Field(type="string")
     */
    protected $extraSettings;
/**
     * @var float $priceFactor;
     *
     * @Field(type="float")
     */
    protected $priceFactor;
/**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return string
     */
    public function getCalcValue1()
    {
        return $this->calcValue1;
    }

    /**
     * @param string $calcValue1
     */
    public function setCalcValue1($calcValue1)
    {
        $this->calcValue1 = $calcValue1;
    }

    /**
     * @return string
     */
    public function getCalcValue2()
    {
        return $this->calcValue2;
    }

    /**
     * @param string $calcValue2
     */
    public function setCalcValue2($calcValue2)
    {
        $this->calcValue2 = $calcValue2;
    }

    /**
     * @return string
     */
    public function getExtraSettings()
    {
        return $this->extraSettings;
    }

    /**
     * @param string $extraSettings
     */
    public function setExtraSettings($extraSettings)
    {
        $this->extraSettings = $extraSettings;
    }

    /**
     * @return float
     */
    public function getPriceFactor(): float
    {
        if (!$this->priceFactor) {
            return 1;
        }
        return $this->priceFactor;
    }

    /**
     * @param float $priceFactor
     */
    public function setPriceFactor(float $priceFactor): void
    {
        $this->priceFactor = $priceFactor;
    }
}
