<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;

/**
 * @Document(repositoryClass="PSC\Shop\EntityBundle\Document\Repository\MotivRepository")
 */
class Motiv
{
    /**
     * @Id
     */
    protected $id;
/**
     * @Field(type="string")
     */
    protected $sessionId;
/**
     * @Field(type="string")
     */
    protected $contactId;
/**
     * @Field(type="string")
     */
    protected $title;
/**
     * @Field(type="int")
     */
    protected $dpiOrg;
/**
     * @Field(type="string")
     */
    protected $orgFilename;
/**
     * @Field(type="string")
     */
    protected $fileOrginal;
/**
     * @Field(type="string")
     */
    protected $fileWork;
/**
     * @Field(type="string")
     */
    protected $fileMid;
/**
     * @Field(type="string")
     */
    protected $fileThumb;
/**
     * @Field(type="int")
     */
    protected $width;
/**
     * @Field(type="int")
     */
    protected $height;
/**
     * @Field(type="int")
     */
    protected $midWidth;
/**
     * @Field(type="int")
     */
    protected $midHeight;
/**
     * @Field(type="string")
     */
    protected $format;
/**
     * @Field(type="string")
     */
    protected $copyright;
/**
     * @Field(type="hash")
     */
    protected $exif;
/**
     * @param mixed $contactId
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;
    }

    /**
     * @return mixed
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * @param mixed $copyright
     */
    public function setCopyright($copyright)
    {
        $this->copyright = $copyright;
    }

    /**
     * @return mixed
     */
    public function getCopyright()
    {
        return $this->copyright;
    }

    /**
     * @param mixed $dpiOrg
     */
    public function setDpiOrg($dpiOrg)
    {
        $this->dpiOrg = $dpiOrg;
    }

    /**
     * @return mixed
     */
    public function getDpiOrg()
    {
        return $this->dpiOrg;
    }

    /**
     * @param mixed $exif
     */
    public function setExif($exif)
    {
        $this->exif = $exif;
    }

    /**
     * @return mixed
     */
    public function getExif()
    {
        return $this->exif;
    }

    /**
     * @param mixed $fileMid
     */
    public function setFileMid($fileMid)
    {
        $this->fileMid = $fileMid;
    }

    /**
     * @return mixed
     */
    public function getFileMid()
    {
        return $this->fileMid;
    }

    /**
     * @param mixed $fileOrginal
     */
    public function setFileOrginal($fileOrginal)
    {
        $this->fileOrginal = $fileOrginal;
    }

    /**
     * @return mixed
     */
    public function getFileOrginal()
    {
        return $this->fileOrginal;
    }

    /**
     * @param mixed $fileThumb
     */
    public function setFileThumb($fileThumb)
    {
        $this->fileThumb = $fileThumb;
    }

    /**
     * @return mixed
     */
    public function getFileThumb()
    {
        return $this->fileThumb;
    }

    /**
     * @param mixed $fileWork
     */
    public function setFileWork($fileWork)
    {
        $this->fileWork = $fileWork;
    }

    /**
     * @return mixed
     */
    public function getFileWork()
    {
        return $this->fileWork;
    }

    /**
     * @param mixed $format
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param mixed $height
     */
    public function setHeight($height)
    {
        $this->height = $height;
    }

    /**
     * @return mixed
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $midHeight
     */
    public function setMidHeight($midHeight)
    {
        $this->midHeight = $midHeight;
    }

    /**
     * @return mixed
     */
    public function getMidHeight()
    {
        return $this->midHeight;
    }

    /**
     * @param mixed $midWidth
     */
    public function setMidWidth($midWidth)
    {
        $this->midWidth = $midWidth;
    }

    /**
     * @return mixed
     */
    public function getMidWidth()
    {
        return $this->midWidth;
    }

    /**
     * @param mixed $orgFilename
     */
    public function setOrgFilename($orgFilename)
    {
        $this->orgFilename = $orgFilename;
    }

    /**
     * @return mixed
     */
    public function getOrgFilename()
    {
        return $this->orgFilename;
    }

    /**
     * @param mixed $sessionId
     */
    public function setSessionId($sessionId)
    {
        $this->sessionId = $sessionId;
    }

    /**
     * @return mixed
     */
    public function getSessionId()
    {
        return $this->sessionId;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = $width;
    }

    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }
}
