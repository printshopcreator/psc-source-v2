<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document\Embed;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;

/**
 * @EmbeddedDocument
 */
class ContactAddress
{
/**
invoiceAddressSaved" : { "id" : "2", "uuid" : "0001-ac11000b-546c8de6-cedf-bb260533", "display" : true,
"install_id" : "1", "contact_id" : "1", "created" : "2014-11-19 13:32:38", "updated" : "2019-01-24 17:47:24", "type" : "1", "anrede" : "3", "pos" : null, "company" : "Musterfirma", "company2" : null, "kostenstellung" : null, "abteilung" :
null, "firstname" : "admin", "lastname" : "admin", "street" : "Musterstrasse", "house_number" : "1", "zip" : "12345", "city" : "Musterort", "phone" : "12345", "mobil_phone" : "", "email" : "admin@shop.de", "country" : "AL", "fax" : "", "position" : null, "ustid" : null, "zusatz1" : null, "zusatz2" : null, "kundenNr" : "" }
 */
    /**
     * @var string
     *
     * @Field(type="string")
     */
    private $firstname;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $lastname;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $street;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $houseNumber;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $zip;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $city;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $phone;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $mobilPhone;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $email;
/**
     * @var integer
     *
     * @Field(type="int")
     */
    private $type;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $company;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $anrede;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $country;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $fax;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $company2;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $kostenstellung;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $abteilung;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $position;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $ustid;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $zusatz1;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $zusatz2;
/**
     * @var int
     *
     * @Field(type="int")
     */
    private $pos;
/**
     * @var string
     *
     * @Field(type="string")
     */
    private $kundenNr;
/**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->houseNumber;
    }

    /**
     * @param string $houseNumber
     */
    public function setHouseNumber($houseNumber)
    {
        $this->houseNumber = $houseNumber;
    }

    /**
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @param string $zip
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getMobilPhone()
    {
        return $this->mobilPhone;
    }

    /**
     * @param string $mobilPhone
     */
    public function setMobilPhone($mobilPhone)
    {
        $this->mobilPhone = $mobilPhone;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param string $company
     */
    public function setCompany($company)
    {
        $this->company = $company;
    }

    /**
     * @return string
     */
    public function getSalutation()
    {
        return $this->anrede;
    }

    /**
     * @return string
     */
    public function getAnrede()
    {
        return $this->anrede;
    }

    /**
     * @param string $anrede
     */
    public function setAnrede($anrede)
    {
        $this->anrede = $anrede;
    }

    /**
     * @param string $anrede
     */
    public function setSalutation($anrede)
    {
        $this->anrede = $anrede;
    }

    /**
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry($country)
    {
        $this->country = $country;
    }

    /**
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * @param string $fax
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }

    /**
     * @return string
     */
    public function getCompany2()
    {
        return $this->company2;
    }

    /**
     * @param string $company2
     */
    public function setCompany2($company2)
    {
        $this->company2 = $company2;
    }

    /**
     * @return string
     */
    public function getKostenstellung()
    {
        return $this->kostenstellung;
    }

    /**
     * @param string $kostenstellung
     */
    public function setKostenstellung($kostenstellung)
    {
        $this->kostenstellung = $kostenstellung;
    }

    /**
     * @return string
     */
    public function getAbteilung()
    {
        return $this->abteilung;
    }

    /**
     * @param string $abteilung
     */
    public function setAbteilung($abteilung)
    {
        $this->abteilung = $abteilung;
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * @return string
     */
    public function getUstid()
    {
        return $this->ustid;
    }

    /**
     * @param string $ustid
     */
    public function setUstid($ustid)
    {
        $this->ustid = $ustid;
    }

    /**
     * @return string
     */
    public function getZusatz1()
    {
        return $this->zusatz1;
    }

    /**
     * @param string $zusatz1
     */
    public function setZusatz1($zusatz1)
    {
        $this->zusatz1 = $zusatz1;
    }

    /**
     * @return string
     */
    public function getZusatz2()
    {
        return $this->zusatz2;
    }

    /**
     * @param string $zusatz2
     */
    public function setZusatz2($zusatz2)
    {
        $this->zusatz2 = $zusatz2;
    }

    /**
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param int $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return string
     */
    public function getKundenNr()
    {
        return $this->kundenNr;
    }

    /**
     * @param string $kundenNr
     */
    public function setKundenNr($kundenNr)
    {
        $this->kundenNr = $kundenNr;
    }
}
