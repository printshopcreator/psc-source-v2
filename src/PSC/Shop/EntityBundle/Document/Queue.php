<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\EntityBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Field;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbeddedDocument;
use Doctrine\ODM\MongoDB\Mapping\Annotations\EmbedOne;

/**
 * @Document
 */
class Queue
{
    /**
     * @Id
     */
    protected $id;
/**
     * @var string $shop
     *
     * @Field(type="string")
     */
    protected $shop;
/**
     * @var string $name
     *
     * @Field(type="string")
     */
    protected $name;
/**
     * @var string $type
     *
     * @Field(type="string")
     */
    protected $queueType;
/**
     * @var string $eventType
     *
     * @Field(type="string")
     */
    protected $eventType;
/**
     * @var int $pos
     *
     * @Field(type="int")
     */
    protected $pos;
/**
     * @var boolean $active
     *
     * @Field(type="bool")
     */
    protected $active;
/** @EmbedOne */
    protected $eventDocument;
/** @EmbedOne */
    protected $queueDocument;
/**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getShop()
    {
        return $this->shop;
    }

    /**
     * @param mixed $shop
     */
    public function setShop($shop)
    {
        $this->shop = $shop;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * @param int $pos
     */
    public function setPos($pos)
    {
        $this->pos = $pos;
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->active;
    }

    /**
     * @param boolean $active
     */
    public function setActive($active)
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getQueueType()
    {
        return $this->queueType;
    }

    /**
     * @param string $queueType
     */
    public function setQueueType($queueType)
    {
        $this->queueType = $queueType;
    }

    /**
     * @return string
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @param string $eventType
     */
    public function setEventType($eventType)
    {
        $this->eventType = $eventType;
    }

    /**
     * @return mixed
     */
    public function getEventDocument()
    {
        return $this->eventDocument;
    }

    /**
     * @param mixed $eventDocument
     */
    public function setEventDocument($eventDocument)
    {
        $this->eventDocument = $eventDocument;
    }

    /**
     * @return mixed
     */
    public function getQueueDocument()
    {
        return $this->queueDocument;
    }

    /**
     * @param mixed $queueDocument
     */
    public function setQueueDocument($queueDocument)
    {
        $this->queueDocument = $queueDocument;
    }
}
