<?php

namespace PSC\Shop\ThemeBundle\Changer;

use PSC\Shop\ThemeBundle\Event\HttpThemeEvent;
use PSC\Shop\ThemeBundle\Resolver\ThemeResolverInterface;
use PSC\Shop\ThemeBundle\Core\ThemeInterface;
use PSC\Shop\ThemeBundle\Selector\ThemeSelectorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ThemeChanger implements ThemeChangerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * @var ThemeSelectorInterface
     */
    private $selector;

    /**
     * @var ThemeResolverInterface
     */
    private $resolver;

    /**
     * Constructor.
     *
     * @param ThemeSelectorInterface   $selector   A theme selector
     * @param ThemeResolverInterface   $resolver   A theme resolver
     * @param EventDispatcherInterface $dispatcher An event dispatcher
     */
    public function __construct(ThemeSelectorInterface $selector, ThemeResolverInterface $resolver, EventDispatcherInterface $dispatcher)
    {
        $this->selector = $selector;
        $this->dispatcher = $dispatcher;
        $this->resolver = $resolver;
    }

    /**
     * {@inheritdoc}
     */
    public function change($theme, Request $request)
    {
        if (is_string($theme)) {
            $themeName = $theme;
        } elseif ($theme instanceof ThemeInterface) {
            $themeName = $theme->getName();
        } else {
            throw new \InvalidArgumentException(
                'The given theme has a wrong type. Expected a theme name or an instance of the ThemeInterface.'
            );
        }

        // Apply
        $this->resolver->setThemeName($themeName, $request);

        // Theme
        if (is_string($theme)) {
            $theme = $this->selector->select($request);
        }

        // Dispatch the event
        $this->dispatcher->dispatch(ThemeChangerEvents::CHANGED, new HttpThemeEvent($theme, $request));
    }
}
