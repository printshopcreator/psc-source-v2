<?php

namespace PSC\Shop\ThemeBundle\Changer;

final class ThemeChangerEvents
{
    /**
     * @var string
     */
    const CHANGED = 'psc.shop.theme.changed';
}
