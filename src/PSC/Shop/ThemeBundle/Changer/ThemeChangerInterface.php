<?php

namespace PSC\Shop\ThemeBundle\Changer;

use PSC\Shop\ThemeBundle\Core\ThemeInterface;
use Symfony\Component\HttpFoundation\Request;

interface ThemeChangerInterface
{
    /**
     * Changes the current theme with the new one.
     *
     * @param string|ThemeInterface $theme   A theme name or a theme instance
     * @param Request               $request A Request instance
     */
    public function change($theme, Request $request);
}
