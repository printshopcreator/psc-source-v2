<?php

namespace PSC\Shop\ThemeBundle\Resolver;

use PSC\Shop\ThemeBundle\Core\ThemeCollection;
use PSC\Shop\ThemeBundle\Core\VirtualThemeInterface;
use PSC\Shop\ThemeBundle\Resolver\Filter\ThemeFilterInterface;
use Symfony\Component\HttpFoundation\Request;

class VirtualThemeResolver implements VirtualThemeResolverInterface
{
    /**
     * @var ThemeFilterInterface[]
     */
    protected $filters;

    /**
     * Constructor.
     *
     * @param ThemeFilterInterface[] $filters Theme filters
     */
    public function __construct(array $filters = array())
    {
        $this->filters = array();
        foreach ($filters as $filter) {
            $this->addFilter($filter);
        }
    }

    /**
     * Adds a theme filter.
     *
     * @param ThemeFilterInterface $filter A theme filter
     */
    public function addFilter(ThemeFilterInterface $filter)
    {
        $this->filters[] = $filter;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \InvalidArgumentException If the given virtual theme has not got any themes
     * @throws \RuntimeException         When there is no matching theme
     * @throws \RuntimeException         When there is more than one matching theme
     */
    public function resolveTheme(VirtualThemeInterface $theme, Request $request)
    {
        $themes = $theme->getThemes();
        $count = count($themes);
        switch ($count) {
            case 0:
                throw new \InvalidArgumentException('A theme collection cannot be empty.');
            case 1:
                return $themes->first();
        }

        $collection = new ThemeCollection($themes->all());
        foreach ($this->filters as $filter) {
            $filter->filter($collection, $request);
            if (1 === $count = count($collection)) {
                break;
            }
        }

        switch ($count) {
            case 1:
                // passed
                return $collection->first();
            case 0:
                // not passed
                throw new \RuntimeException('There is no matching theme for the given theme set.');
            default:
                // not passed
                throw new \RuntimeException('There is more than one matching theme for the given themes set.');
        }
    }
}
