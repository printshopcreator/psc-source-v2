<?php

namespace PSC\Shop\ThemeBundle\Resolver;

use PSC\Shop\ThemeBundle\Core\ThemeSource;
use PSC\Shop\ThemeBundle\Resolver\ThemeResolverInterface;
use PSC\System\SettingsBundle\Service\Shop;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Cookie;

class ShopThemeResolver implements ThemeResolverInterface
{
    /** @var \PSC\Shop\EntityBundle\Document\Shop */
    protected $shop;

    /** @var ThemeSource  */
    protected $source;

    /** @var Shop */
    protected $serviceShop;

    /**
     * Constructor.
     * @param Shop $serviceShop
     * @param ThemeSource $source
     */
    public function __construct(Shop $serviceShop, ThemeSource $source)
    {
        $this->serviceShop = $serviceShop;
        $this->source = $source;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveThemeName(Request $request)
    {
        $this->shop = $this->serviceShop->getMongoShopByDomain();
        if ($this->shop->getTheme() == "") {
            return null;
        }

        if ($this->source->hasTheme($this->shop->getTheme())) {
            return $this->shop->getTheme();
        }
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function setThemeName($themeName, Request $request)
    {
    }
}
