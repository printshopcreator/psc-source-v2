<?php

namespace PSC\Shop\ThemeBundle\Resolver\EventListener;

use PSC\Shop\ThemeBundle\Resolver\ResponseWriterInterface;
use PSC\Shop\ThemeBundle\Resolver\ThemeResolverInterface;
use PSC\Shop\ThemeBundle\Resolver\VirtualThemeResolver;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ThemeResolverListener implements EventSubscriberInterface
{
    /**
     * @var ThemeResolverInterface
     */
    private $resolver;

    /**
     * Constructor.
     *
     * @param ThemeResolverInterface $resolver A theme resolver
     */
    public function __construct(ThemeResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * Saves theme changes done in the request only when the theme resolver
     * implements the ResponseWriterInterface.
     *
     * @param ResponseEvent $event An event
     */
    public function onKernelResponse(ResponseEvent $event)
    {
        if (!$event->isMasterRequest() || !$this->resolver instanceof ResponseWriterInterface) {
            return;
        }

        $this->resolver->writeResponse($event->getRequest(), $event->getResponse());
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::RESPONSE => 'onKernelResponse',
        );
    }
}
