<?php

namespace PSC\Shop\ThemeBundle\Resolver;

use Symfony\Component\HttpFoundation\Request;

class SessionThemeResolver implements ThemeResolverInterface
{
    /**
     * @var string
     */
    const SESSION_NAME = '_jungi_theme';

    /**
     * {@inheritdoc}
     */
    public function resolveThemeName(Request $request)
    {
        if (!$request->hasSession()) {
            return;
        }

        return $request->getSession()->get(self::SESSION_NAME);
    }

    /**
     * {@inheritdoc}
     */
    public function setThemeName($themeName, Request $request)
    {
        if (!$request->hasSession()) {
            return;
        }

        $request->getSession()->set(self::SESSION_NAME, $themeName);
    }
}
