<?php

namespace PSC\Shop\ThemeBundle\Resolver;

use PSC\Shop\ThemeBundle\Core\ThemeInterface;
use PSC\Shop\ThemeBundle\Core\VirtualThemeInterface;
use Symfony\Component\HttpFoundation\Request;

interface VirtualThemeResolverInterface
{
    /**
     * Resolves a suitable theme for the given virtual theme.
     *
     * @param VirtualThemeInterface $theme   A virtual theme
     * @param Request               $request A Request instance
     *
     * @return ThemeInterface
     */
    public function resolveTheme(VirtualThemeInterface $theme, Request $request);
}
