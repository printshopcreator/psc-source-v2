<?php

namespace PSC\Shop\ThemeBundle\Resolver;

use Symfony\Component\HttpFoundation\Request;

interface ThemeResolverInterface
{
    /**
     * Returns the current theme name for the given request.
     *
     * @param Request $request A request instance
     *
     * @return string|null Returns null if the current theme name
     *                     can not be resolved
     */
    public function resolveThemeName(Request $request);

    /**
     * Sets the current theme for the given request.
     *
     * @param string  $themeName A theme name
     * @param Request $request   A request instance
     */
    public function setThemeName($themeName, Request $request);
}
