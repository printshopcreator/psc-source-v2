<?php

namespace PSC\Shop\ThemeBundle\Resolver;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface ResponseWriterInterface
{
    /**
     * Writes theme changes done in the given Request to the given Response.
     *
     * @param Request  $request  A Request instance
     * @param Response $response A Response instance
     */
    public function writeResponse(Request $request, Response $response);
}
