<?php

namespace PSC\Shop\ThemeBundle\Resolver;

use Symfony\Component\HttpFoundation\Request;

class InMemoryThemeResolver implements ThemeResolverInterface
{
    /**
     * @var string
     */
    private $theme;

    /**
     * @var bool
     */
    private $fixed;

    /**
     * Constructor.
     *
     * @param string $theme A theme name (optional if $fixed var is true)
     * @param bool   $fixed Is a fixed? (optional)
     *
     * @throws \LogicException If the theme name is not passed when the $fixed is false
     */
    public function __construct($theme = null, $fixed = true)
    {
        if (!$fixed && null === $theme) {
            throw new \LogicException('You must provide the theme name if you are not using fixed state.');
        }

        $this->theme = $theme;
        $this->fixed = $fixed;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveThemeName(Request $request)
    {
        return $this->theme;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \BadMethodCallException When the fixed state is switched on
     */
    public function setThemeName($themeName, Request $request)
    {
        if ($this->fixed) {
            throw new \BadMethodCallException('This theme resolver does not allow for setting new themes.');
        }

        $this->theme = $themeName;
    }
}
