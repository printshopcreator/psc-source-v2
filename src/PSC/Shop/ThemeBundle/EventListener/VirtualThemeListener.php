<?php

namespace PSC\Shop\ThemeBundle\EventListener;

use PSC\Shop\ThemeBundle\Changer\ThemeChangerEvents;
use PSC\Shop\ThemeBundle\Core\VirtualThemeInterface;
use PSC\Shop\ThemeBundle\Event\HttpThemeEvent;
use PSC\Shop\ThemeBundle\Resolver\VirtualThemeResolverInterface;
use PSC\Shop\ThemeBundle\Selector\Event\ResolvedThemeEvent;
use PSC\Shop\ThemeBundle\Selector\ThemeSelectorEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Request;

class VirtualThemeListener implements EventSubscriberInterface
{
    /**
     * @var VirtualThemeResolverInterface
     */
    private $resolver;

    /**
     * Constructor.
     *
     * @param VirtualThemeResolverInterface $resolver A virtual theme resolver
     */
    public function __construct(VirtualThemeResolverInterface $resolver)
    {
        $this->resolver = $resolver;
    }

    /**
     * @param ResolvedThemeEvent $event
     */
    public function onResolvedTheme(ResolvedThemeEvent $event)
    {
        $theme = $event->getTheme();
        if (!$theme instanceof VirtualThemeInterface) {
            return;
        }

        $this->handle($theme, $event->getRequest());
    }

    /**
     * @param HttpThemeEvent $event
     */
    public function onChangedTheme(HttpThemeEvent $event)
    {
        $theme = $event->getTheme();
        if (!$theme instanceof VirtualThemeInterface || null !== $theme->getPointedTheme()) {
            return;
        }

        $this->handle($theme, $event->getRequest());
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            ThemeSelectorEvents::RESOLVED => array('onResolvedTheme', 10),
            ThemeChangerEvents::CHANGED => array('onChangedTheme', 10),
        );
    }

    /**
     * Handles an event.
     *
     * @param VirtualThemeInterface $theme   A virtual theme
     * @param Request               $request A request
     */
    private function handle(VirtualThemeInterface $theme, Request $request)
    {
        $theme->setPointedTheme($this->resolver->resolveTheme($theme, $request));
    }
}
