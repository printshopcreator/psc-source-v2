<?php

namespace PSC\Shop\ThemeBundle\EventListener;

use PSC\Shop\ThemeBundle\Changer\ThemeChangerEvents;
use PSC\Shop\ThemeBundle\Core\ThemeHolderInterface;
use PSC\Shop\ThemeBundle\Event\HttpThemeEvent;
use PSC\Shop\ThemeBundle\Selector\Exception\NullThemeException;
use PSC\Shop\ThemeBundle\Selector\ThemeSelectorInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ThemeHolderListener implements EventSubscriberInterface
{
    /**
     * @var ThemeSelectorInterface
     */
    private $selector;

    /**
     * @var ThemeHolderInterface
     */
    private $holder;

    /**
     * @var bool
     */
    private $ignoreNullTheme;

    /**
     * Constructor.
     *
     * @param ThemeHolderInterface   $holder          A theme holder
     * @param ThemeSelectorInterface $selector        A theme selector
     * @param bool                   $ignoreNullTheme Whether to ignore the situation when the theme selector
     *                                                will not match any theme for the request (optional)
     */
    public function __construct(ThemeHolderInterface $holder, ThemeSelectorInterface $selector, $ignoreNullTheme = true)
    {
        $this->selector = $selector;
        $this->holder = $holder;
        $this->ignoreNullTheme = (bool) $ignoreNullTheme;
    }

    /**
     * Handles an event in aim to get the theme for the current request.
     *
     * @param ControllerEvent $event An event
     *
     * @throws NullThemeException
     */
    public function onKernelController(ControllerEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        try {
            $theme = $this->selector->select($event->getRequest());
        } catch (NullThemeException $e) {
            if ($this->ignoreNullTheme) {
                return;
            }

            throw $e;
        }

        $this->holder->setTheme($theme);
    }

    /**
     * Handles the change theme event.
     *
     * @param HttpThemeEvent $event An event
     */
    public function onChangedTheme(HttpThemeEvent $event)
    {
        $this->holder->setTheme($event->getTheme());
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::CONTROLLER => array('onKernelController', -100), // should be the last
            ThemeChangerEvents::CHANGED => array('onChangedTheme'),
        );
    }
}
