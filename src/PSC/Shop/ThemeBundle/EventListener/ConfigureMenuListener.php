<?php

namespace PSC\Shop\ThemeBundle\EventListener;

use PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent;
use PSC\Shop\SettingsBundle\Event\MenuEvents;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

class ConfigureMenuListener
{
    protected $eventDispatcher;
/** @var null|AuthorizationChecker  */
    public $authorizationChecker = null;
    public function __construct(EventDispatcherInterface $eventDispatcher, AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->authorizationChecker = $authorizationChecker;
    }

    /**
     * @param \PSC\Backend\DashboardBundle\Event\ConfigureMenuEvent $event
     */
    public function onMenuConfigureMain(ConfigureMenuEvent $event)
    {
        if ($this->authorizationChecker->isGranted('ROLE_ADMIN')) {
            $menu = $event->getMenu();
            $menu->addChild('Theme', array(
                'route' => 'psc_shop_theme_backend_settings',
                'extras' => array(
                    'icon' => 'fas fa-envelope'
                )
            ));
        }
    }
}
