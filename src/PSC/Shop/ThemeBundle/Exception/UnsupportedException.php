<?php

namespace PSC\Shop\ThemeBundle\Exception;

class UnsupportedException extends \InvalidArgumentException
{
}
