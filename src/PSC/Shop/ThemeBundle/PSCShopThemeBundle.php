<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ThemeBundle;

use PSC\Shop\ThemeBundle\DependencyInjection\Compiler\CacheWarmerPass;
use PSC\Shop\ThemeBundle\DependencyInjection\Compiler\ThemeFilterPass;
use PSC\System\PluginBundle\Interfaces\Plugin;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * ThemeBundle
 *
 * @package PSC\Shop\Theme
 */
class PSCShopThemeBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new CacheWarmerPass());
        $container->addCompilerPass(new ThemeFilterPass());
/* @var DependencyInjection\PSCShopThemeExtension $ext */
        $ext = $this->getContainerExtension();
        try {
            if (extension_loaded('mongodb')) {
                $collection = (new \MongoDB\Client('mongodb://mongodb/'))->psc->Plugin;
                $plugins = $collection->find(array('installed' => true));
        /** @var Plugin $plugin */
                foreach ($plugins as $plugin) {
                    if (file_exists($container->getParameter('kernel.project_dir') . '/var/plugins/' . $plugin['path'] . '/Resources/config/theme.yml')) {
                        $ext->registerMappingFile($container->getParameter('kernel.project_dir') . '/var/plugins/' . $plugin['path'] . '/Resources/config/theme.yml');
                    }
                }
            }
        } catch (\Exception $e) {
            dump($e);
        }


        // Default tags
        $ext->registerTag(array(
            'PSC\Shop\ThemeBundle\Tag\MobileDevices',
            'PSC\Shop\ThemeBundle\Tag\TabletDevices',
            'PSC\Shop\ThemeBundle\Tag\DesktopDevices',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function boot()
    {
        $this->container->get('psc.shop.theme.source_initializer')->initialize();
    }
}
