<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ThemeBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\ThemeBundle\Core\ThemeSource;
use PSC\Shop\ThemeBundle\Core\ThemeSourceInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChangeType extends AbstractType
{
    /** @var ThemeSource */
    protected $themes;
    public function __construct(ThemeSource $themes)
    {
        $this->themes = $themes;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $tmp = [];
        foreach ($this->themes->getThemes() as $theme) {
            $tmp[$theme->getName()] = $theme->getName();
        }

        $builder
            ->add('theme', ChoiceType::class, array(
                    'choices'  => $tmp
                ))
            ->add('layouterTheme', ChoiceType::class, array(
                    'choices'  => $this->getLayouterThemes(), 'label' => 'Layoutertheme'))
            ->add('save', SubmitType::class, array('label' => 'Ändern'));
    }

    public function getName()
    {
        return 'changeTheme';
    }

    public function getLayouterThemes()
    {
        return [
            'Old' => 'old',
            'PSC' => 'psc',
            'Sweet' => 'sweet'
        ];
    }
}
