<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ThemeBundle\Form\Backend;

use Doctrine\ORM\EntityRepository;
use PSC\Shop\ThemeBundle\Core\ThemeSource;
use PSC\System\PluginBundle\Form\Chain\Field;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SettingsType extends AbstractType
{
    /** @var ThemeSource */

    protected $themes;
/** @var Field  */
    protected $fields;
/**
     * @var FormFactory
     */
    private $formFactory;
    public function __construct(Field $fields, FormFactoryInterface $formFactory, ThemeSource $themes)
    {
        $this->themes = $themes;
        $this->fields = $fields;
        $this->formFactory = $formFactory;
    }


    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
        foreach ($this->fields->getThemeFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Theme, $options['themeType']) as $field) {
            $builder->add($field->buildForm($this->formFactory->createNamedBuilder($field->getGroup()), $options));
        }

        $builder->add('save', SubmitType::class, array('label' => 'Speichern'));
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) use ($options) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getThemeFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Theme, $options['themeType']) as $field) {
                $field->formPreSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($options) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getThemeFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Theme, $options['themeType']) as $field) {
                $field->formPostSetData($event);
            }
        });
        $builder->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) use ($options) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getThemeFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Theme, $options['themeType']) as $field) {
                $field->formPostSubmit($event);
            }
        });
        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($options) {

            /** @var \PSC\System\PluginBundle\Form\Interfaces\Field $field */
            foreach ($this->fields->getThemeFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Theme, $options['themeType']) as $field) {
                $field->formPreSubmit($event);
            }
        });
    }

    public function getName()
    {
        return 'themeSettings';
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'shopEntity' => null,
            'shopDoc' => null,
            'themeType' => "",
        ));
    }
}
