<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ThemeBundle\Controller\Backend;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\EntityManagerInterface;
use PSC\Shop\EntityBundle\Entity\Motiv;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\ShippingBundle\Document\ShippingGatewaySettings;
use PSC\Shop\ShippingBundle\Service\Registry;
use PSC\Shop\ThemeBundle\Core\Theme;
use PSC\Shop\ThemeBundle\Core\ThemeSource;
use PSC\Shop\ThemeBundle\Form\Backend\ChangeType;
use PSC\Shop\ThemeBundle\Form\Backend\SettingsType;
use PSC\System\PluginBundle\Form\Chain\Field;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * GatewayController fürs Backend
 *
 * @package    PSC\Shop\Shipping
 * @subpackage Controller
 */
class SettingsController extends AbstractController
{
    /**
     * Default Seite
     *
     * @Route("/settings", name="psc_shop_theme_backend_settings")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request, \PSC\System\SettingsBundle\Service\Shop $shopService, DocumentManager $documentManager, EntityManagerInterface $entityManager, Field $fieldService)
    {

        $selectedShop = $shopService->getSelectedShop();
/** @var \PSC\Shop\EntityBundle\Document\Shop $shop */
        $shop = $documentManager
            ->getRepository('PSC\Shop\EntityBundle\Document\Shop')
            ->findOneBy(array('uid' => (string)$selectedShop->getUid()));
        if ($shop === null) {
            $shop = new \PSC\Shop\EntityBundle\Document\Shop();
            $shop->setUid($selectedShop->getUid());
        }

        $form = $this->createForm(ChangeType::class, $shop);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            if ($form->getData()->getTheme()) {
                $shop->setTheme($form->getData()->getTheme());
            }

            $shop->setLayouterTheme($form->getData()->getLayouterTheme());
            $documentManager->persist($shop);
            $documentManager->flush();
        }
        $tmp = array();
        $formTheme = $this->createForm(SettingsType::class, $tmp, ['shopEntity' => $selectedShop, 'shopDoc' => $shop, 'themeType' => $shop->getTheme()]);
        $customThemeFields = $fieldService->getThemeFields(\PSC\System\PluginBundle\Form\Interfaces\Field::Theme, $shop->getTheme());
        $customThemeGroups = $fieldService->getThemeGroups(\PSC\System\PluginBundle\Form\Interfaces\Field::Theme, $shop->getTheme());
        $formTheme->handleRequest($request);
        if ($formTheme->isSubmitted() && $formTheme->isValid()) {
            $entityManager->persist($selectedShop);
            $entityManager->flush();
            $documentManager->persist($shop);
            $documentManager->flush();
        }

        return array(
            'form' => $form->createView(),
            'formTheme' => $formTheme->createView(),
            'customThemeFields' => $customThemeFields,
            'customThemeGroups' => $customThemeGroups
        );
    }
}
