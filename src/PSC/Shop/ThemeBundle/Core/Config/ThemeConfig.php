<?php

namespace PSC\Shop\ThemeBundle\Core\Config;

use PSC\Shop\ThemeBundle\Core\Config\Cms\CmsConfig;

abstract class ThemeConfig
{
    /**
     * @var string
     */
    protected $cms;

    /**
     * @return CmsConfig
     */
    public function getCms()
    {
        return $this->cms;
    }

    /**
     * @param CmsConfig $cms
     */
    public function setCms($cms)
    {
        $this->cms = $cms;
    }
}
