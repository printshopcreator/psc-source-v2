<?php

namespace PSC\Shop\ThemeBundle\Core\Config;

use PSC\Shop\ThemeBundle\Core\Config\Cms\CmsEssence;

class ThemeConfigEssence extends ThemeConfig
{
    /**
     * Creates a new builder instance.
     *
     * @return ThemeConfigEssenceBuilder
     */
    public static function createBuilder()
    {
        return new ThemeConfigEssenceBuilder();
    }

    /**
     * Constructor.
     *
     * @param ThemeConfigEssenceBuilder $builder The Information builder
     */
    public function __construct(ThemeConfigEssenceBuilder $builder)
    {
        $fields = $builder->getFields();

        $this->cms = $fields->cms;
    }
}
