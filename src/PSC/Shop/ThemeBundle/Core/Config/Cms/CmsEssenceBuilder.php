<?php

namespace PSC\Shop\ThemeBundle\Core\Config\Cms;

class CmsEssenceBuilder
{
    /**
     * @var PositionInterface[]
     */
    private $positions = array();

    public function addPosition(PositionInterface $position)
    {
        $this->positions[] = $position;

        return $this;
    }

    public function addPositions(array $positions)
    {
        foreach ($positions as $position) {
            $this->addPosition($position);
        }

        return $this;
    }

    /**
     * Returns the all fields of the builder.
     *
     * @return \stdClass
     */
    public function getFields()
    {
        $obj = new \stdClass();
        $obj->positions = $this->positions;

        return $obj;
    }

    /**
     * Builds the ThemeInfoEssence instance.
     *
     * @return CmsEssence
     *
     * @throws \RuntimeException When the name is missing
     */
    public function getCms()
    {
        return new CmsEssence($this);
    }

    /**
     * @return PositionInterface[]
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @param PositionInterface[] $positions
     * @return CmsEssenceBuilder
     */
    public function setPositions($positions)
    {
        $this->positions = $positions;

        return $this;
    }
}
