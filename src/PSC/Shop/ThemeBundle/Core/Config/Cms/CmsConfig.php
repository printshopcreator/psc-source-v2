<?php

namespace PSC\Shop\ThemeBundle\Core\Config\Cms;

abstract class CmsConfig
{
    /**
     * @var PositionInterface[]
     */
    protected $positions = array();

    /**
     * @return PositionInterface[]
     */
    public function getPositions()
    {
        return $this->positions;
    }

    /**
     * @return array
     */
    public function getPositionsAsArray()
    {
        $temp = array();
        foreach ($this->positions as $position) {
            $temp[$position->getName()] = $position->getId();
        }
        return $temp;
    }

    /**
     * @param PositionInterface[] $positions
     */
    public function setPositions($positions)
    {
        $this->positions = $positions;
    }

    public function getPos($id)
    {
        foreach ($this->positions as $pos) {
            if ($id == $pos->getId()) {
                return $pos;
            }
        }

        return new Position("main");
    }
}
