<?php

namespace PSC\Shop\ThemeBundle\Core\Config\Cms;

interface PositionInterface
{
    /**
     * Returns the author name.
     *
     * @return string
     */
    public function getName();

    /**
     * Returns the id.
     *
     * @return string
     */
    public function getId();
}
