<?php

namespace PSC\Shop\ThemeBundle\Core\Config\Cms;

class Position implements PositionInterface
{
    /**
     * @var string
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;


    /**
     * Constructor.
     *
     * @param $id
     * @param string $name An author name
     */
    public function __construct($id, $name = null)
    {
        if (!$id) {
            throw new \RuntimeException('You must provide an id');
        }

        $this->id = $id;
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * String representation.
     *
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s', $this->name);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
}
