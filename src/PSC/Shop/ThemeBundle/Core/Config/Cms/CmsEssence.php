<?php

namespace PSC\Shop\ThemeBundle\Core\Config\Cms;

class CmsEssence extends CmsConfig
{
    /**
     * Creates a new builder instance.
     *
     * @return CmsEssenceBuilder
     */
    public static function createBuilder()
    {
        return new CmsEssenceBuilder();
    }

    /**
     * Constructor.
     *
     * @param CmsEssenceBuilder $builder The Information builder
     */
    public function __construct(CmsEssenceBuilder $builder)
    {
        $fields = $builder->getFields();

        $this->positions = $fields->positions;
    }
}
