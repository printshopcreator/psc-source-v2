<?php

namespace PSC\Shop\ThemeBundle\Core\Config;

class ThemeConfigEssenceBuilder
{
    /**
     * @var string
     */
    private $cms;

    /**
     * Returns the all fields of the builder.
     *
     * @return \stdClass
     */
    public function getFields()
    {
        $obj = new \stdClass();
        $obj->cms = $this->cms;

        return $obj;
    }

    /**
     * Builds the ThemeInfoEssence instance.
     *
     * @return ThemeConfigEssence
     *
     * @throws \RuntimeException When the name is missing
     */
    public function getThemeConfig()
    {
        return new ThemeConfigEssence($this);
    }

    /**
     * @return string
     */
    public function getCms()
    {
        return $this->cms;
    }

    /**
     * @param string $cms
     */
    public function setCms($cms)
    {
        $this->cms = $cms;

        return $this;
    }
}
