<?php

namespace PSC\Shop\ThemeBundle\Core;

use PSC\Shop\ThemeBundle\Core\Information\ThemeInfoEssence;
use PSC\Shop\ThemeBundle\Exception\ThemeNotFoundException;
use PSC\Shop\ThemeBundle\Core\Information\ThemeInfo;
use PSC\Shop\ThemeBundle\Core\Information\ThemeConfigEssence;
use PSC\Shop\ThemeBundle\Tag\TagCollection;

class VirtualTheme implements VirtualThemeInterface
{
    /**
     * @var string
     */
    protected $pointed;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var FrozenThemeCollection
     */
    protected $themes;

    /**
     * @var TagCollection
     */
    protected $tags;

    /**
     * @var ThemeInfo
     */
    protected $info;

    /**
     * Constructor.
     *
     * @param string           $name   An unique theme name
     * @param ThemeInterface[] $themes Themes that belongs to the virtual theme
     * @param ThemeInfo        $info   An information instance (optional)
     * @param TagCollection    $tags   Tags (optional)
     *
     * @throws \RuntimeException When in the children themes there is an another virtual theme
     */
    public function __construct($name, array $themes, ThemeInfo $info = null, TagCollection $tags = null)
    {
        $this->name = $name;
        $this->pointed = null;
        $this->info = $info ?: ThemeInfoEssence::createBuilder()->setName('empty')->getThemeInfo();
        $this->tags = $tags ?: new TagCollection();

        foreach ($themes as $theme) {
            if ($theme instanceof VirtualThemeInterface) {
                throw new \RuntimeException(sprintf('You cannot attach a virtual theme to an another virtual theme.'));
            }
        }

        $this->themes = new FrozenThemeCollection($themes);
    }

    /**
     * {@inheritdoc}
     */
    public function setPointedTheme($theme)
    {
        if (is_string($theme)) {
            $themeName = $theme;
            $belongs = $this->themes->has($theme);
        } elseif ($theme instanceof ThemeInterface) {
            $themeName = $theme->getName();
            $belongs = $this->themes->contains($theme);
        } else {
            throw new \InvalidArgumentException(
                'The given theme has a wrong type. Expected a theme name or an instance of the ThemeInterface.'
            );
        }

        if (!$belongs) {
            throw new ThemeNotFoundException(sprintf(
                'The theme "%s" not belongs to the virtual theme "%s".',
                $themeName,
                $this->name
            ));
        }

        $this->pointed = $themeName;
    }

    /**
     * {@inheritdoc}
     */
    public function getPointedTheme()
    {
        if ($this->pointed) {
            return $this->themes->get($this->pointed);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     *
     * @throws \RuntimeException When the pointed theme is not set
     */
    public function getPath()
    {
        if (!$this->pointed) {
            throw new \RuntimeException('The path cannot be returned, because the decorated theme is not set.');
        }

        return $this->getPointedTheme()->getPath();
    }

    /**
     * {@inheritdoc}
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Returns the theme collection of the virtual theme.
     *
     * @return FrozenThemeCollection
     */
    public function getThemes()
    {
        return $this->themes;
    }

    /**
     * {@inheritdoc}
     */
    public function getInformation()
    {
        return $this->info;
    }

    public function getConfig()
    {
        // TODO: Implement getConfig() method.
    }
}
