<?php

namespace PSC\Shop\ThemeBundle\Core;

interface ThemeHolderInterface
{
    /**
     * Returns the current theme.
     *
     * @return ThemeInterface|null Null if the theme was not set
     */
    public function getTheme();

    /**
     * Sets the current theme.
     *
     * @param ThemeInterface $theme A theme
     */
    public function setTheme(ThemeInterface $theme);
}
