<?php

namespace PSC\Shop\ThemeBundle\Core;

use Jungi\Bundle\ThemeBundle\Tag\TagCollection;
use Jungi\Bundle\ThemeBundle\Tag\TagInterface;

interface ThemeSourceInterface
{
    /**
     * Adds a new theme.
     *
     * @param ThemeInterface $theme A theme
     */
    public function addTheme(ThemeInterface $theme);

    /**
     * Checks if the given theme exists.
     *
     * @param string $name A theme name
     *
     * @return bool
     */
    public function hasTheme($name);

    /**
     * Returns the theme by name.
     *
     * @param string $name A theme name
     *
     * @return ThemeInterface
     *
     * @throws \Jungi\Bundle\ThemeBundle\Exception\ThemeNotFoundException
     */
    public function getTheme($name);

    /**
     * Returns all themes.
     *
     * @return ThemeInterface[]
     */
    public function getThemes();

    /**
     * Returns the theme which has given tags.
     *
     * @param TagInterface|TagInterface[] $tags      A single tag or tags
     * @param string                      $condition A condition (optional)
     *
     * @return ThemeInterface|null Null if the theme can not be found
     */
    public function findThemeWithTags($tags, $condition = TagCollection::COND_AND);

    /**
     * Returns all themes which has given tags.
     *
     * @param TagInterface|TagInterface[] $tags      A single tag or tags
     * @param string                      $condition A condition (optional)
     *
     * @return ThemeInterface[]
     */
    public function findThemesWithTags($tags, $condition = TagCollection::COND_AND);
}
