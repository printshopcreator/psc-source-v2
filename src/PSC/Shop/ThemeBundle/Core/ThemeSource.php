<?php

namespace PSC\Shop\ThemeBundle\Core;

use PSC\Shop\ThemeBundle\Exception\ThemeNotFoundException;
use PSC\Shop\ThemeBundle\Tag\TagCollection;

class ThemeSource implements ThemeSourceInterface
{
    /**
     * @var ThemeCollection
     */
    protected $themes;

    /**
     * Constructor.
     *
     * @param ThemeInterface[] $themes Themes (optional)
     */
    public function __construct(array $themes = array())
    {
        $this->themes = new ThemeCollection($themes);
    }

    /**
     * {@inheritdoc}
     */
    public function addTheme(ThemeInterface $theme)
    {
        $this->themes->add($theme);
    }

    /**
     * {@inheritdoc}
     */
    public function hasTheme($name)
    {
        return $this->themes->has($name);
    }

    /**
     * {@inheritdoc}
     */
    public function getTheme($name)
    {
        if ($name === null || $name === "") {
            $name = 'bootstrap4';
        }
        if (null !== $theme = $this->themes->get($name)) {
            return $theme;
        }

        throw new ThemeNotFoundException($name);
    }

    /**
     * {@inheritdoc}
     */
    public function getThemes()
    {
        return $this->themes->all();
    }

    /**
     * {@inheritdoc}
     */
    public function findThemeWithTags($tags, $condition = TagCollection::COND_AND)
    {
        return $this->themes->findOneByTags($tags, $condition);
    }

    /**
     * {@inheritdoc}
     */
    public function findThemesWithTags($tags, $condition = TagCollection::COND_AND)
    {
        return $this->themes->findByTags($tags, $condition);
    }
}
