<?php

namespace PSC\Shop\ThemeBundle\Core;

use PSC\Shop\ThemeBundle\Exception\ThemeNotFoundException;

interface VirtualThemeInterface extends ThemeInterface
{
    /**
     * Sets a theme which will be used by the virtual theme.
     *
     * @param string|ThemeInterface $pointed A theme name or a theme instance
     *
     * @throws \InvalidArgumentException If the passed theme has a wrong type
     * @throws ThemeNotFoundException    If the given theme does not belongs to the virtual theme
     */
    public function setPointedTheme($pointed);

    /**
     * Returns the parent theme.
     *
     * @return ThemeInterface
     */
    public function getPointedTheme();

    /**
     * Returns the child themes of the virtual theme.
     *
     * @return ThemeCollection
     */
    public function getThemes();
}
