<?php

namespace PSC\Shop\ThemeBundle\Core;

class ThemeHolder implements ThemeHolderInterface
{
    /**
     * @var ThemeInterface
     */
    protected $theme;

    /**
     * {@inheritdoc}
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * {@inheritdoc}
     */
    public function setTheme(ThemeInterface $theme)
    {
        $this->theme = $theme;
    }
}
