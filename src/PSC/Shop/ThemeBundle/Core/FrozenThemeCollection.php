<?php

namespace PSC\Shop\ThemeBundle\Core;

class FrozenThemeCollection extends ThemeCollection
{
    /**
     * Constructor.
     *
     * @param ThemeInterface[] $themes Themes (optional)
     */
    public function __construct(array $themes = array())
    {
        $this->themes = array();
        foreach ($themes as $theme) {
            parent::add($theme);
        }
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function add(ThemeInterface $theme)
    {
        throw new \LogicException('This collection is frozen, so you cannot add any theme.');
    }

    /**
     * {@inheritdoc}
     *
     * @throws \LogicException
     */
    public function remove($themeName)
    {
        throw new \LogicException('This collection is frozen, so you cannot remove any theme.');
    }
}
