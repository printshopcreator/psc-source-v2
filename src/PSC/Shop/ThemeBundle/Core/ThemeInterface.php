<?php

namespace PSC\Shop\ThemeBundle\Core;

interface ThemeInterface
{
    /**
     * Returns the unique theme name.
     *
     * A theme name should be as simple as possible e.g. "footheme", "bar-theme"
     *
     * @return string
     */
    public function getName();

    /**
     * Returns the theme tag collection.
     *
     * @return \PSC\Shop\ThemeBundle\Tag\TagCollection
     */
    public function getTags();

    /**
     * Returns the absolute path to the theme directory.
     *
     * @return string
     */
    public function getPath();

    /**
     * Returns the information about the theme.
     *
     * @return \PSC\Shop\ThemeBundle\Core\Information\ThemeInfo
     */
    public function getInformation();

    /**
     * Returns the information about the theme.
     *
     * @return \PSC\Shop\ThemeBundle\Core\Config\ThemeConfig
     */
    public function getConfig();
}
