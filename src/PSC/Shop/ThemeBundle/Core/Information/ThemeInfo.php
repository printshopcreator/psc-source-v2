<?php

namespace PSC\Shop\ThemeBundle\Core\Information;

abstract class ThemeInfo
{
    /**
     * @var AuthorInterface[]
     */
    protected $authors = array();

    /**
     * @var string
     */
    protected $description;

    /**
     * @var string
     */
    protected $name;

    /**
     * Returns the friendly theme name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Returns the authors.
     *
     * @return AuthorInterface[]
     */
    public function getAuthors()
    {
        return $this->authors;
    }

    /**
     * Returns the description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }
}
