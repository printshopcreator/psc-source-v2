<?php

namespace PSC\Shop\ThemeBundle\Core\Information;

interface AuthorInterface
{
    /**
     * Returns the author name.
     *
     * @return string
     */
    public function getName();

    /**
     * Returns the author email.
     *
     * @return string
     */
    public function getEmail();

    /**
     * Returns the author homepage.
     *
     * @return string|null
     */
    public function getHomepage();
}
