<?php

namespace PSC\Shop\ThemeBundle\Core\Information;

class ThemeInfoEssence extends ThemeInfo
{
    /**
     * Creates a new builder instance.
     *
     * @return ThemeInfoEssenceBuilder
     */
    public static function createBuilder()
    {
        return new ThemeInfoEssenceBuilder();
    }

    /**
     * Constructor.
     *
     * @param ThemeInfoEssenceBuilder $builder The Information builder
     */
    public function __construct(ThemeInfoEssenceBuilder $builder)
    {
        $fields = $builder->getFields();

        $this->name = $fields->name;
        $this->description = $fields->description;
        $this->authors = $fields->authors;
    }
}
