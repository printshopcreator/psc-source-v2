<?php

namespace PSC\Shop\ThemeBundle\Core;

use PSC\Shop\ThemeBundle\Core\Config\ThemeConfig;
use PSC\Shop\ThemeBundle\Core\Config\ThemeConfigEssence;
use PSC\Shop\ThemeBundle\Core\Information\ThemeInfo;
use PSC\Shop\ThemeBundle\Core\Information\ThemeInfoEssence;
use PSC\Shop\ThemeBundle\Tag\TagCollection;

class Theme implements ThemeInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var TagCollection
     */
    protected $tags;

    /**
     * @var ThemeInfo
     */
    protected $info;

    /**
     * @var ThemeConfig
     */
    protected $config;

    /**
     * Constructor.
     *
     * @param string $name An unique theme name
     * @param string $path A path to theme resources
     * @param ThemeInfo $info An information instance (optional)
     * @param TagCollection $tags A tag collection (optional)
     * @param ThemeConfig $config
     */
    public function __construct($name, $path, ThemeInfo $info = null, TagCollection $tags = null, ThemeConfig $config = null)
    {
        $this->name = $name;
        $this->path = $path;
        $this->info = $info ?: ThemeInfoEssence::createBuilder()->setName('empty')->getThemeInfo();
        $this->config = $config ?: ThemeConfigEssence::createBuilder()->getThemeConfig();
        $this->tags = $tags ?: new TagCollection();
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * {@inheritdoc}
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * {@inheritdoc}
     */
    public function getInformation()
    {
        return $this->info;
    }

    /**
     * @return ThemeConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param ThemeConfig $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }
}
