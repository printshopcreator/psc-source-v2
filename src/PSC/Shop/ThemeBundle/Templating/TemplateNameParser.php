<?php

namespace PSC\Shop\ThemeBundle\Templating;

use PSC\Shop\ThemeBundle\Core\ThemeHolderInterface;
use PSC\Shop\ThemeBundle\Core\VirtualThemeInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Templating\TemplateReferenceInterface;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateNameParser as BaseTemplateNameParser;
use Symfony\Bundle\FrameworkBundle\Templating\TemplateReference as BaseTemplateReference;

class TemplateNameParser extends BaseTemplateNameParser
{
    /**
     * @var ThemeHolderInterface
     */
    private $holder;

    /**
     * Constructor.
     *
     * @param ThemeHolderInterface $holder A theme holder
     * @param KernelInterface      $kernel A KernelInterface instance
     */
    public function __construct(ThemeHolderInterface $holder, KernelInterface $kernel)
    {
        parent::__construct($kernel);

        $this->holder = $holder;
    }

    /**
     * {@inheritdoc}
     */
    public function parse($name)
    {
        if ($name instanceof TemplateReferenceInterface) {
            return $name;
        } elseif (isset($this->cache[$name])) {
            return $this->cache[$name];
        }

        // The current theme
        $theme = $this->holder->getTheme();

        // Use parent method if there is no theme
        $parent = parent::parse($name);
        if (null === $theme || !$parent instanceof BaseTemplateReference) {
            return $parent;
        }

        if ($theme instanceof VirtualThemeInterface) {
            $reference = new VirtualTemplateReference($parent, $theme->getName(), $theme->getPointedTheme()->getName());
        } else {
            $reference = new TemplateReference($parent, $theme->getName());
        }

        return $this->cache[$name] = $reference;
    }
}
