<?php

namespace PSC\Shop\ThemeBundle\Event;

use Symfony\Component\HttpFoundation\Request;
use PSC\Shop\ThemeBundle\Core\ThemeInterface;
use Symfony\Contracts\EventDispatcher\Event;

class HttpThemeEvent extends Event
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var ThemeInterface
     */
    protected $theme;

    /**
     * Constructor.
     *
     * @param ThemeInterface $theme   A theme
     * @param Request        $request A Request object
     */
    public function __construct(ThemeInterface $theme, Request $request)
    {
        $this->theme = $theme;
        $this->request = $request;
    }

    /**
     * Returns the request object.
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Returns the theme.
     *
     * @return ThemeInterface
     */
    public function getTheme()
    {
        return $this->theme;
    }
}
