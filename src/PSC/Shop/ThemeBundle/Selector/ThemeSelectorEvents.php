<?php

namespace PSC\Shop\ThemeBundle\Selector;

final class ThemeSelectorEvents
{
    /**
     * @var string
     */
    const RESOLVED = 'psc.shop.theme.selector.resolved';

    /**
     * @var string
     */
    const SELECTED = 'psc.shop.theme.selector.selected';
}
