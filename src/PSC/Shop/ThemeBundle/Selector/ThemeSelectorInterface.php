<?php

namespace PSC\Shop\ThemeBundle\Selector;

use PSC\Shop\ThemeBundle\Selector\Exception\NullThemeException;
use Symfony\Component\HttpFoundation\Request;

interface ThemeSelectorInterface
{
    /**
     * Selects the current theme for the given Request.
     *
     * @param Request $request A Request instance
     *
     * @return \PSC\Shop\ThemeBundle\Core\ThemeInterface
     *
     * @throws NullThemeException If there is no any matching theme for the request
     */
    public function select(Request $request);
}
