<?php

namespace PSC\Shop\ThemeBundle\Selector\Event;

use PSC\Shop\ThemeBundle\Core\ThemeInterface;
use PSC\Shop\ThemeBundle\Event\HttpThemeEvent;
use PSC\Shop\ThemeBundle\Resolver\ThemeResolverInterface;
use Symfony\Component\HttpFoundation\Request;

class ResolvedThemeEvent extends HttpThemeEvent
{
    /**
     * @var ThemeResolverInterface
     */
    protected $resolver;

    /**
     * Constructor.
     *
     * @param ThemeInterface         $theme    A theme
     * @param ThemeResolverInterface $resolver A theme resolver
     * @param Request                $request  A Request object
     */
    public function __construct(ThemeInterface $theme, ThemeResolverInterface $resolver, Request $request)
    {
        $this->resolver = $resolver;

        parent::__construct($theme, $request);
    }

    /**
     * Returns the theme resolver.
     *
     * @return ThemeResolverInterface
     */
    public function getThemeResolver()
    {
        return $this->resolver;
    }
}
