<?php

namespace PSC\Shop\ThemeBundle\Selector\Event;

use PSC\Shop\ThemeBundle\Core\ThemeInterface;
use PSC\Shop\ThemeBundle\Resolver\ThemeResolverInterface;
use Symfony\Component\HttpFoundation\Request;

class DetailedResolvedThemeEvent extends ResolvedThemeEvent
{
    /**
     * @var string
     */
    const PRIMARY_RESOLVER = 'primary';

    /**
     * @var string
     */
    const FALLBACK_RESOLVER = 'fallback';

    /**
     * @var string
     */
    protected $resolverType;

    /**
     * Constructor.
     *
     * @param string                 $resolverType A theme resolver type
     * @param ThemeInterface         $theme        A theme
     * @param ThemeResolverInterface $resolver     A theme resolver
     * @param Request                $request      A Request object
     *
     * @throws \InvalidArgumentException When the theme resolver type is invalid
     */
    public function __construct($resolverType, ThemeInterface $theme, ThemeResolverInterface $resolver, Request $request)
    {
        $types = array(self::PRIMARY_RESOLVER, self::FALLBACK_RESOLVER);
        if (!in_array($resolverType, $types)) {
            throw new \InvalidArgumentException(sprintf(
                'The given theme resolver type "%s" is invalid, the supported types: %s.',
                $resolverType,
                implode(', ', $types)
            ));
        }

        $this->resolverType = $resolverType;

        // Parent
        parent::__construct($theme, $resolver, $request);
    }

    /**
     * Checks whether the theme in the event was resolved by given theme resolver type.
     *
     * @param string $type A theme resolver type
     *
     * @return bool
     */
    public function wasResolvedBy($type)
    {
        return $this->resolverType == $type;
    }

    /**
     * Returns the theme resolver type.
     *
     * @return string
     */
    public function getThemeResolverType()
    {
        return $this->resolverType;
    }
}
