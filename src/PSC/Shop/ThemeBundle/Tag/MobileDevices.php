<?php

namespace PSC\Shop\ThemeBundle\Tag;

class MobileDevices extends AbstractMobileDevices
{
    /**
     * {@inheritdoc}
     */
    public static function getName()
    {
        return 'psc.shop.theme.tag.mobile_devices';
    }
}
