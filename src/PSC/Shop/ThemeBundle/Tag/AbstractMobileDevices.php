<?php

namespace PSC\Shop\ThemeBundle\Tag;

abstract class AbstractMobileDevices implements TagInterface
{
    /**
     * @var array
     */
    protected $systems;

    /**
     * Constructor.
     *
     * @param string|array $systems Operating systems (optional)
     *                              Operating systems should be the same as in the MobileDetect class
     */
    public function __construct($systems = array())
    {
        $this->systems = (array) $systems;
    }

    /**
     * Returns the operating systems.
     *
     * @return array
     */
    public function getSystems()
    {
        return $this->systems;
    }

    /**
     * {@inheritdoc}
     */
    public function isEqual(TagInterface $tag)
    {
        return $tag instanceof static && ((!$this->systems || !$tag->systems) || array_intersect($this->systems, $tag->systems));
    }
}
