<?php

namespace PSC\Shop\ThemeBundle\Tag;

class DesktopDevices implements TagInterface
{
    /**
     * {@inheritdoc}
     */
    public function isEqual(TagInterface $tag)
    {
        return $tag instanceof static;
    }

    /**
     * {@inheritdoc}
     */
    public static function getName()
    {
        return 'psc.shop.theme.tag.desktop_devices';
    }
}
