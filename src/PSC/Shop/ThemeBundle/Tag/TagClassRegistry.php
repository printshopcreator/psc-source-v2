<?php

namespace PSC\Shop\ThemeBundle\Tag;

class TagClassRegistry implements TagClassRegistryInterface
{
    /**
     * @var array
     */
    protected $classes = array();

    /**
     * Constructor.
     *
     * @param array $classes Tag classes (optional)
     */
    public function __construct(array $classes = array())
    {
        foreach ($classes as $tagName => $class) {
            $this->registerTagClass($tagName, $class);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function registerTagClass($name, $class)
    {
        $this->classes[$name] = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function hasTagClass($name)
    {
        return isset($this->classes[$name]);
    }

    /**
     * {@inheritdoc}
     */
    public function getTagClass($name)
    {
        if (!$this->hasTagClass($name)) {
            throw new \InvalidArgumentException(
                sprintf('The given tag name "%s" was not registered.', $name)
            );
        }

        return $this->classes[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function getTagClasses()
    {
        return $this->classes;
    }
}
