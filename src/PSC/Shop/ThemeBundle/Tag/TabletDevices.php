<?php

namespace PSC\Shop\ThemeBundle\Tag;

class TabletDevices extends AbstractMobileDevices
{
    /**
     * {@inheritdoc}
     */
    public static function getName()
    {
        return 'psc.shop.theme.tag.tablet_devices';
    }
}
