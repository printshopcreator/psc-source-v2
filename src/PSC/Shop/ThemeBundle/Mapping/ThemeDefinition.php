<?php

namespace PSC\Shop\ThemeBundle\Mapping;

class ThemeDefinition
{
    /**
     * @var Tag[]
     */
    protected $tags = array();

    /**
     * @var ThemeInfo
     */
    protected $info;

    /**
     * @var ThemeConfig
     */
    protected $config;

    /**
     * @var string
     */
    protected $path;

    /**
     * Constructor.
     *
     * @param string    $path A path (optional)
     * @param Tag[]     $tags Tag definitions (optional)
     * @param ThemeInfo $info A theme info (optional)
     */
    public function __construct($path = null, array $tags = array(), ThemeInfo $info = null, ThemeConfig $config = null)
    {
        $this->path = $path;
        $this->info = $info;
        $this->config = $config;
        foreach ($tags as $tag) {
            $this->addTag($tag);
        }
    }

    /**
     * Sets a path to theme resources.
     *
     * @param $path
     *
     * @return ThemeDefinition
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Returns the path to theme resources.
     *
     * @return null|string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param array $tags
     *
     * @return ThemeDefinition
     */
    public function setTags(array $tags)
    {
        $this->tags = array();
        foreach ($tags as $tag) {
            $this->addTag($tag);
        }

        return $this;
    }

    /**
     * Adds a tag definition.
     *
     * @param Tag $definition
     *
     * @return ThemeDefinition
     */
    public function addTag(Tag $definition)
    {
        $this->tags[] = $definition;

        return $this;
    }

    /**
     * Returns the tag definitions.
     *
     * @return Tag[]
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Sets a theme info definition.
     *
     * @param ThemeInfo $definition
     *
     * @return ThemeDefinition
     */
    public function setInfo(ThemeInfo $definition)
    {
        $this->info = $definition;

        return $this;
    }

    /**
     * Returns the theme info definition.
     *
     * @return ThemeInfo
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @return ThemeConfig
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param ThemeConfig $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }
}
