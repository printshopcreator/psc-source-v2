<?php

/*
 * This file is part of the JungiThemeBundle package.
 *
 * (c) Piotr Kugla <piku235@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace PSC\Shop\ThemeBundle\Mapping\Processor;

use PSC\Shop\ThemeBundle\Mapping\ThemeDefinition;
use PSC\Shop\ThemeBundle\Mapping\ThemeDefinitionRegistryInterface;
use PSC\Shop\ThemeBundle\Mapping\VirtualThemeDefinition;
use Symfony\Component\Config\FileLocatorInterface;

/**
 * ThemePathWorker.
 *
 * @author Piotr Kugla <piku235@gmail.com>
 */
class ThemePathWorker extends AbstractThemeWorker
{
    /**
     * @var FileLocatorInterface
     */
    private $locator;

    /**
     * Constructor.
     *
     * @param FileLocatorInterface $locator A file locator
     */
    public function __construct(FileLocatorInterface $locator)
    {
        $this->locator = $locator;
    }

    /**
     * {@inheritdoc}
     */
    protected function processTheme($name, ThemeDefinition $definition, ThemeDefinitionRegistryInterface $registry)
    {
        if (!$definition instanceof VirtualThemeDefinition) {
            $definition->setPath($this->locator->locate($definition->getPath()));
        }
    }
}
