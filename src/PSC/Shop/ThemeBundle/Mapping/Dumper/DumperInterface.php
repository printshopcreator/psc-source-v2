<?php

namespace PSC\Shop\ThemeBundle\Mapping\Dumper;

use PSC\Shop\ThemeBundle\Mapping\ThemeDefinitionRegistryInterface;

interface DumperInterface
{
    /**
     * Dumps the given theme definition registry.
     *
     * @param ThemeDefinitionRegistryInterface $registry
     *
     * @return string
     */
    public function dump(ThemeDefinitionRegistryInterface $registry);
}
