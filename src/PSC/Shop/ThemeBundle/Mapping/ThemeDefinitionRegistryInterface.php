<?php

namespace PSC\Shop\ThemeBundle\Mapping;

interface ThemeDefinitionRegistryInterface
{
    /**
     * Adds a theme definition.
     *
     * @param string          $name       A theme name
     * @param ThemeDefinition $definition A theme definition
     *
     * @throws \RuntimeException If there is a theme definition under the same name
     */
    public function registerThemeDefinition($name, ThemeDefinition $definition);

    /**
     * @param string $name A theme name
     *
     * @return bool
     */
    public function hasThemeDefinition($name);

    /**
     * Returns the given theme definition.
     *
     * @param string $name A theme name
     *
     * @return ThemeDefinition
     *
     * @throws \RuntimeException When the given theme definition does not exist
     */
    public function getThemeDefinition($name);

    /**
     * Returns the all registered theme definitions.
     *
     * @return ThemeDefinition[]
     */
    public function getThemeDefinitions();

    /**
     * Removes a theme definition.
     *
     * @param string $name A theme name
     */
    public function removeThemeDefinition($name);
}
