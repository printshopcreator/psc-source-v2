<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ThemeBundle\DependencyInjection;

use InvalidArgumentException;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 *
 * @package PSC\Shop\Theme
 */
class PSCShopThemeExtension extends Extension
{
    /**
     * @var array
     */
    private $tagClasses;
/**
     * @var array
     */
    private $mappingFiles = array();
/**
     * Loads a specific configuration.
     *
     * @param array            $configs   An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws InvalidArgumentException When provided tag is not defined in this extension
     *
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        // Config
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
        $loader->load('mappings.yml');
        $loader->load('fundamental.yml');
        $loader->load('listeners.yml');
        $loader->load('templating.yml');
        $loader->load('extensions.yml');
// Register theme mapping files
        $this->processThemeMappings($config, $container);
// Register tag classes
        $this->registerTag($config['tags']);
        $tagClassRegistry = $container->getDefinition('psc.shop.theme.tag.class_registry');
        $tagClassRegistry->replaceArgument(0, $this->tagClasses);
// Ignore null themes
        $container->setParameter('psc_shop_theme.listener.holder.ignore_null_theme', $config['holder']['ignore_null_theme']);
// Primary theme resolver conf
        $this->processThemeResolver('psc.shop.theme.resolver.primary', 'primary', $config, $container);
// Fallback theme resolver conf
        if (isset($config['resolver']['fallback'])) {
            $this->processThemeResolver('psc.shop.theme.resolver.fallback', 'fallback', $config, $container);
        }

        // Theme registry conf
        if (isset($config['source']['id'])) {
            $container->setAlias('psc_shop_theme.source', $config['source']['id']);
        }

        // Theme holder conf
        if (isset($config['holder']['id'])) {
            $container->setAlias('psc_shop_theme.holder', $config['holder']['id']);
        }

        // Theme selector service
        if (isset($config['selector']['id'])) {
            $container->setAlias('psc_shop_theme.selector', $config['selector']['id']);
        }

        // Validation listener
        if (!$config['selector']['validation_listener']['enabled']) {
            $container->removeDefinition('psc.shop.theme.selector.listener.validation');
        } else {
            $container->setParameter('psc_shop_theme.selector.listener.validation.suspects', $config['selector']['validation_listener']['suspects']);
        }

        // Device theme filter
        if (!$config['resolver']['virtual']['device_filter']) {
            $container->removeDefinition('psc.shop.theme.resolver.filter.device');
        }
    }

    /**
     * Registers a theme mapping file to load.
     *
     * @param string $file A theme mapping file
     * @param string $type A type of theme mapping file (optional)
     */
    public function registerMappingFile($file, $type = null)
    {
        $this->mappingFiles[] = array($file, $type);
    }

    /**
     * Registers a tag class or tag classes.
     *
     * @param string|array $class a collection or a single fully qualified class name
     *
     * @throws \RuntimeException         When the tag class is not exist
     * @throws \InvalidArgumentException When the given tag class does not implement the TagInterface
     */
    public function registerTag($class)
    {
        foreach ((array) $class as $child) {
            $child = '\\' . ltrim($child, '\\');
            if (!class_exists($child)) {
                throw new \RuntimeException(sprintf('The tag with the class "%s" is not exist.', $child));
            }

            $reflection = new \ReflectionClass($child);
            if (!$reflection->implementsInterface('PSC\Shop\ThemeBundle\Tag\TagInterface')) {
                throw new \InvalidArgumentException(sprintf('The given class "%s" is not a tag. All tags must implement the "PSC\Shop\ThemeBundle\Tag\TagInterface".', $child));
            }

            $this->tagClasses[$reflection->getMethod('getName')->invoke(null)] = $child;
        }
    }

    private function processThemeMappings(array $config, ContainerBuilder $container)
    {
        $mappingFiles = $this->mappingFiles;
        foreach ($config['mappings'] as $mapping) {
            $mappingFiles[] = array($mapping['resource'], $mapping['type']);
        }

        $def = $container->getDefinition('psc.shop.theme.source_initializer');
        $def->replaceArgument(0, $mappingFiles);
    }

    private function processThemeResolver($id, $for, array $config, ContainerBuilder $container)
    {
        $type = key($config['resolver'][$for]);
        $resolver = $config['resolver'][$for][$type];
        if ($type != 'id') {
            switch ($type) {
                case 'in_memory':
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      $definition = new Definition('PSC\\Shop\\ThemeBundle\\Resolver\\InMemoryThemeResolver');
                    $definition->addArgument($resolver);

                    break;
                case 'cookie':
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      $definition = new Definition('PSC\\Shop\\ThemeBundle\\Resolver\\CookieThemeResolver');
                    $definition->addArgument($resolver);

                    break;
                case 'session':
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      $definition = new Definition('PSC\\Shop\\ThemeBundle\\Resolver\\SessionThemeResolver');

                    break;
                default:
                    throw new \InvalidArgumentException('Not supported type.');
            }

            // Append the definition
            $container->setDefinition($id, $definition);
        } else {
            $container->setAlias($id, $resolver);
        }
    }
}
