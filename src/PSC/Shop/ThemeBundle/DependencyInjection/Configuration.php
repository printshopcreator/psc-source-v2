<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Shop\ThemeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 *
 * @package PSC\Shop\Theme
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('psc_shop_theme');
        $rootNode = $treeBuilder->getRootNode();
        $rootNode
            ->fixXmlConfig('mapping')
            ->children()
            ->append($this->addThemeSourceNode())
            ->append($this->addThemeHolderNode())
            ->append($this->addThemeSelectorNode())
            ->append($this->addThemeResolverNode())
            ->append($this->addThemeMappingsNode())
            ->arrayNode('tags')
            ->info('list of tag classes that will be registered')
            ->prototype('scalar')->cannotBeEmpty()->end()
            ->end()
            ->end();
        return $treeBuilder;
    }

    protected function addThemeMappingsNode()
    {
        $builder = new TreeBuilder('mappings');
        $rootNode = $builder->getRootNode();
        $rootNode
            ->info('list of theme mapping files')
            ->prototype('array')
            ->children()
            ->scalarNode('type')
            ->cannotBeEmpty()
            ->defaultNull()
            ->end()
            ->scalarNode('resource')
            ->cannotBeEmpty()
            ->isRequired()
            ->end()
            ->end()
            ->end();
        return $rootNode;
    }

    protected function addThemeSourceNode()
    {
        $builder = new TreeBuilder('source');
        $rootNode = $builder->getRootNode();
        $rootNode
            ->addDefaultsIfNotSet()
            ->info('theme source configuration')
            ->children()
            ->scalarNode('id')
            ->info('symfony service id')
            ->cannotBeEmpty()
            ->end()
            ->end();
        return $rootNode;
    }

    protected function addThemeHolderNode()
    {
        $builder = new TreeBuilder('holder');
        $rootNode = $builder->getRootNode();
        $rootNode
            ->addDefaultsIfNotSet()
            ->info('theme holder configuration')
            ->children()
            ->scalarNode('id')
            ->info('symfony service id')
            ->cannotBeEmpty()
            ->end()
            ->booleanNode('ignore_null_theme')
            ->info('whether to ignore a situation when the theme selector will do not match any theme for the request.')
            ->defaultTrue()
            ->end()
            ->end();
        return $rootNode;
    }

    protected function addThemeSelectorNode()
    {
        $builder = new TreeBuilder('selector');
        $rootNode = $builder->getRootNode();
        $resolverNorm = function ($v) {

            if (isset($v['suspects'])) {
                array_walk($v['suspects'], function (&$class) {

                    if (false === strpos($class, '\\')) {
                        $class = 'PSC\\Shop\\ThemeBundle\\Resolver\\' . $class;
                    }

                    if (!class_exists($class)) {
                        throw new \InvalidArgumentException(sprintf('The theme resolver "%s" can not be found.', $class));
                    }
                });
            }

            return $v;
        };
        $rootNode
            ->addDefaultsIfNotSet()
            ->info('theme selector configuration')
            ->children()
            ->scalarNode('id')
            ->info('symfony service id')
            ->cannotBeEmpty()
            ->end()
            ->arrayNode('validation_listener')
            ->info('theme validation listener configuration')
            ->addDefaultsIfNotSet()
            ->canBeEnabled()
            ->fixXmlConfig('suspect')
            ->children()
            ->arrayNode('suspects')
            ->info('a list of theme resolvers which should be validated')
            ->prototype('scalar')->cannotBeEmpty()->end()
            ->end()
            ->end()
            ->beforeNormalization()
            ->always()
            ->then($resolverNorm)
            ->end()
            ->end()
            ->end();
        return $rootNode;
    }

    protected function addThemeResolverNode()
    {
        $builder = new TreeBuilder('resolver');
        $rootNode = $builder->getRootNode();
        $rootNode
            ->addDefaultsIfNotSet()
            ->isRequired()
            ->info('general theme resolver configuration')
            ->children()
            ->append($this->addVirtualThemeResolverNode())
            ->append($this->addPrimaryThemeResolverNode())
            ->append($this->addFallbackThemeResolverNode())
            ->end();
        return $rootNode;
    }

    protected function addVirtualThemeResolverNode()
    {
        $builder = new TreeBuilder('virtual');
        $rootNode = $builder->getRootNode();
        $rootNode
            ->addDefaultsIfNotSet()
            ->info('virtual theme resolver configuration')
            ->children()
            ->scalarNode('id')
            ->info('symfony service id')
            ->cannotBeEmpty()
            ->end()
            ->booleanNode('device_filter')
            ->info('use the device theme filter')
            ->defaultTrue()
            ->end()
            ->end();
        return $rootNode;
    }

    protected function addFallbackThemeResolverNode()
    {
        $builder = new TreeBuilder('fallback');
        $rootNode = $builder->getRootNode();
        $rootNode
            ->info('fallback theme resolver configuration')
            ->canBeUnset();
        $this->configureThemeResolverNode($rootNode, true);
        return $rootNode;
    }

    protected function addPrimaryThemeResolverNode()
    {
        $builder = new TreeBuilder('primary');
        $rootNode = $builder->getRootNode();
        $rootNode
            ->info('theme resolver configuration')
            ->isRequired();
        $this->configureThemeResolverNode($rootNode);
        return $rootNode;
    }

    protected function configureThemeResolverNode(ArrayNodeDefinition $node)
    {
        $selfNode = $node
            ->beforeNormalization()
            ->ifString()
            ->then(function ($v) {

                return array(
                    'id' => $v,
                );
            })
            ->end()
            ->children();
// Service
        $selfNode
            ->scalarNode('id')
            ->info('symfony service id')
            ->cannotBeEmpty()
            ->end();
// Cookie
        $selfNode
            ->arrayNode('cookie')
            ->info('cookie theme resolver')
            ->canBeUnset()
            ->children()
            ->integerNode('lifetime')->defaultValue(2592000)->end()
            ->scalarNode('path')->defaultValue('/')->end()
            ->scalarNode('domain')->end()
            ->booleanNode('secure')->defaultFalse()->end()
            ->booleanNode('httpOnly')->defaultTrue()->end()
            ->end()
            ->end();
// InMemory
        $selfNode
            ->scalarNode('in_memory')
            ->info('in memory theme resolver')
            ->cannotBeEmpty()
            ->end();
// Session
        $selfNode
            ->booleanNode('session')
            ->info('session theme resolver')
            ->validate()
            ->ifTrue(function ($v) {

                return $v === false;
            })
            ->thenUnset()
            ->end()
            ->end();
// Validation
        $selfNode
            ->end()
            ->validate()
            ->ifTrue(function ($v) {

                return count($v) > 1;
            })
            ->thenInvalid('You cannot use more than one theme resolver.')
            ->end();
    }
}
