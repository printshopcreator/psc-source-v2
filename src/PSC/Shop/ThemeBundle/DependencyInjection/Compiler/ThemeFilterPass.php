<?php

namespace PSC\Shop\ThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ThemeFilterPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('psc.shop.theme.resolver.virtual')) {
            return;
        }

        $definition = $container->getDefinition('psc.shop.theme.resolver.virtual');
        foreach ($container->findTaggedServiceIds('psc.shop.theme.filter') as $id => $attrs) {
            $definition->addMethodCall('addFilter', array(new Reference($id)));
        }
    }
}
