<?php

namespace PSC\Shop\ThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

class CacheWarmerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->hasDefinition('templating.cache_warmer.template_paths')) {
            return;
        }

        $cacheWarmer = $container->getDefinition('templating.cache_warmer.template_paths');
        $cacheWarmer->replaceArgument(0, new Reference('psc.shop.theme.cache_warmer.chain_finder'));
    }
}
