<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Default Controller for TestBundle
 *
 * @package    PSC\Site\Test
 * @subpackage Controller
 */
class DefaultController extends AbstractController
{
    /**
     * Default Action für das TestBundle
     *
     * @param string $name
     *
     * @Route("/hello/{name}", name="site_test_index")
     * @Template()
     *
     * @return View
     */
    public function indexAction($name)
    {
        return array('name' => $name);
    }
}
