<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\ArrayInput;
use Liip\FunctionalTestBundle\Test\WebTestCase as BaseWebTestCase;

/**
 * WebTestCase for setup Database
 *
 * @package    PSC\Site\Test
 * @subpackage Tests
 */
abstract class WebTestCase extends BaseWebTestCase
{
    protected $client;
/**
     * Setup
     *
     * @return void
     */
    protected function setUp()
    {
        $this->loadFixtures(array(
            'PSC\Site\TestBundle\DataFixtures\ORM\LoadShop',

            'PSC\Site\TestBundle\DataFixtures\ORM\LoadShipping',

            'PSC\Site\TestBundle\DataFixtures\ORM\LoadContact',

            'PSC\Site\TestBundle\DataFixtures\ORM\LoadAccount',

            'PSC\Site\TestBundle\DataFixtures\ORM\LoadProduct',

            'PSC\Site\TestBundle\DataFixtures\ORM\LoadProductgroup'
        ));
        $this->client = static::createClient();
    }

    /**
     * Login
     *
     * @return void
     */
    protected function login()
    {
        $this->client->request('POST', '/route', array(
                "extAction" => "PSCBackendPortal_User",
                "extMethod" => "login",
                "extTID"    => 3,
                "extType"   => "rpc",
                "extUpload" => "false",
                "username"  => "info@test.com",
                "password"  => "password"
            ));
    }
}
