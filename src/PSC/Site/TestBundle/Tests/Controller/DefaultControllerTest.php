<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\Tests\Controller;

use PSC\Site\TestBundle\Tests\WebTestCase;

/**
 * Default Test for TestBundle
 *
 * @package    PSC\Site\Test
 * @subpackage Tests
 */
class DefaultControllerTest extends WebTestCase
{
    /**
     * Test Index
     *
     * @return void
     */
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/hello/Fabien');
        $this->assertTrue($crawler->filter('html:contains("Hello Fabien")')->count() > 0);
    }
}
