<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Shop;
use PSC\Shop\EntityBundle\Entity\Account;

/**
 * Load Accountfixtures
 *
 * @package    PSC\Site\Test
 * @subpackage Fixtures
 */
class LoadAccount extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * Dependency Container
     *
     * @var ContainerInterface
     */
    private $container;
/**
     * Loads the Data
     *
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $account = new Account();
        $account->setMasterLock(false);
        $account->setTitle("Test Firma");
        $account2 = new Account();
        $account2->setMasterLock(false);
        $account2->setTitle("Test UnterFirma");
        $account3 = new Account();
        $account3->setMasterLock(false);
        $account3->setTitle("Test UnterUnterFirma");
        $account4 = new Account();
        $account4->setMasterLock(true);
        $account4->setTitle("Test UnterUnterFirma Locked");
        $account->addChild($account2);
        $account2->addChild($account3);
        $account2->addChild($account4);
        $manager->persist($account);
        $manager->persist($account2);
        $manager->persist($account3);
        $manager->persist($account4);
        $this->addReference('account', $account);
        $manager->flush();
    }

    /**
     * Set the order
     *
     * @return int
     */
    public function getOrder()
    {
        return 30;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
