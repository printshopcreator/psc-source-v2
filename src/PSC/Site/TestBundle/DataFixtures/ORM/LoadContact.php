<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PSC\Shop\EntityBundle\Entity\ShopContact;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * Load Contactfixtures
 *
 * @package    PSC\Site\Test
 * @subpackage Fixtures
 */
class LoadContact extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * Dependency Container
     *
     * @var ContainerInterface
     */
    private $container;
/**
     * Loads the Data
     *
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $shop = $manager->merge($this->getReference('shop'));
        $contact = new Contact();
        $contact->setEmail("info@test.com");
        $contact->setPassword("password");
        $contact->setInstallId(1);
        $manager->persist($contact);
        $cs = new ShopContact();
        $cs->setContact($contact);
        $cs->setShop($shop);
        $cs->setAdmin(true);
        $cs->setUid(1);
        $manager->persist($cs);
        $contact1 = new Contact();
        $contact1->setEmail("customer@test.com");
        $contact1->setPassword("password");
        $contact1->setInstallId(1);
        $manager->persist($contact1);
        $cs1 = new ShopContact();
        $cs1->setContact($contact1);
        $cs1->setShop($shop);
        $cs1->setAdmin(false);
        $cs1->setUid(2);
        $manager->persist($cs1);
        $this->addReference('contact', $contact);
        $this->addReference('contactUser', $contact1);
        $manager->flush();
    }

    /**
     * Set the order
     *
     * @return int
     */
    public function getOrder()
    {
        return 20;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
