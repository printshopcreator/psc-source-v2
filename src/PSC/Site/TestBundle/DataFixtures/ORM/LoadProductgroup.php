<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\Productgroup;
use PSC\Shop\EntityBundle\Entity\ProductProductgroup;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * Load Productgroupfixtures
 *
 * @package    PSC\Site\Test
 * @subpackage Fixtures
 */
class LoadProductgroup extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * Dependency Container
     *
     * @var ContainerInterface
     */
    private $container;
/**
     * Loads the Data
     *
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $shop = $manager->merge($this->getReference('shop'));
//$product = $manager->merge($this->getReference('product'));

        $productGroup = new Productgroup();
        $productGroup->setTitle("Productgroup 1");
        $productGroup->setShop($shop);
        $manager->persist($productGroup);
        $manager->flush();
    }

    /**
     * Set the order
     *
     * @return int
     */
    public function getOrder()
    {
        return 60;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
