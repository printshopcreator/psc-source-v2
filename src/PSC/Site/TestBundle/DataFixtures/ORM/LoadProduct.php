<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use PSC\Shop\EntityBundle\Entity\Product;
use PSC\Shop\EntityBundle\Entity\ProductProductgroup;
use PSC\Shop\EntityBundle\Entity\Stockbooking;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use PSC\Shop\EntityBundle\Entity\Contact;
use PSC\Shop\EntityBundle\Entity\Shop;

/**
 * Load Productfixtures
 *
 * @package    PSC\Site\Test
 * @subpackage Fixtures
 */
class LoadProduct extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * Dependency Container
     *
     * @var ContainerInterface
     */
    private $container;
/**
     * Loads the Data
     *
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {
        $shop = $manager->merge($this->getReference('shop'));
        $contact = $manager->merge($this->getReference('contact'));
        $product = new Product();
        $product->setTitle("Product 1");
        $product->setDescription("Beschreibung");
        $product->setActive(true);
        $product->setIsStock(true);
        $product->setStockCount(10);
        $product->setStockMinCount(10);
        $product->setPrivate(false);
        $product->setNrExtern("nr-extern");
        $product->setNrIntern("nr-intern");
        $product->setShop($shop);
        $manager->persist($product);
        $stbooking = new Stockbooking();
        $stbooking->setProduct($product);
        $stbooking->setDescription("Fixture");
        $stbooking->setAmount(10);
        $stbooking->setDelivery("nix");
        $stbooking->setContact($contact);
        $manager->persist($stbooking);
        $manager->flush();
        $this->addReference('product', $product);
    }

    /**
     * Set the order
     *
     * @return int
     */
    public function getOrder()
    {
        return 50;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
