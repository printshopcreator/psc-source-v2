<?php

/**
 * PrintshopCreator Suite
 *
 * PHP Version 5.3
 *
 * @author    Thomas Peterson <info@thomas-peterson.de>
 * @copyright 2012-2013 PrintshopCreator GmbH
 * @license   Private
 * @link      http://www.printshopcreator.de
 */

namespace PSC\Site\TestBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use PSC\Shop\EntityBundle\Entity\Shipping;

/**
 * Load Shippingfixtures
 *
 * @package    PSC\Site\Test
 * @subpackage Fixtures
 */
class LoadShipping extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * Dependency Container
     *
     * @var ContainerInterface
     */
    private $container;
/**
     * Loads the Data
     *
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager)
    {

        $shop = $manager->merge($this->getReference('shop'));
        $shipping1 = new Shipping();
        $shipping1->setTitle("DHL");
        $shipping1->setPos(1);
        $shipping1->setKosten(10.12);
        $shipping1->setShop($shop);
        $manager->persist($shipping1);
        $manager->flush();
        $shipping2 = new Shipping();
        $shipping2->setTitle("UPS");
        $shipping2->setPos(2);
        $shipping2->setKosten(14.12);
        $shipping2->setShop($shop);
        $manager->persist($shipping2);
        $manager->flush();
    }

    /**
     * Set the order
     *
     * @return int
     */
    public function getOrder()
    {
        return 40;
    }

    /**
     * Sets the Container.
     *
     * @param ContainerInterface $container A ContainerInterface instance
     *
     * @return void
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }
}
