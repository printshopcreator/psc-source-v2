<?php

namespace PSC\Libraries\AutoRegistryBundle;

use PSC\Libraries\AutoRegistryBundle\Service\AutoRegistryCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class PSCLibrariesAutoRegistryBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new AutoRegistryCompilerPass());
    }
}
