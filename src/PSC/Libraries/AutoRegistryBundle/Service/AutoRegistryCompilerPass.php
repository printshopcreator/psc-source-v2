<?php

namespace PSC\Libraries\AutoRegistryBundle\Service;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\DependencyInjection\Reference;

class AutoRegistryCompilerPass implements CompilerPassInterface
{
    /**
     * Proccess
     *
     * @param ContainerBuilder $container
     *
     * @api
     */
    public function process(ContainerBuilder $container)
    {
        $registries = $container->findTaggedServiceIds('autoregistry');
        foreach ($registries as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                if (!$container->has($id)) {
                    continue;
                }
                $service = $container->getDefinition($id);
                $this->processRegistry($container, $service, $attributes['tag']);
            }
        }
    }

    public function processRegistry(ContainerBuilder $container, Definition $registry, $tag)
    {
        $elements = $container->findTaggedServiceIds($tag);
        foreach ($elements as $id => $tagAttributes) {
            $registry->addMethodCall('add', array(new Reference($id)));
        }
    }
}
