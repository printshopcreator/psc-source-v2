<?php

namespace PSC\Libraries\AutoRegistryBundle\Service;

/**
 * Interface RegistryInterface
 * @package ScayTrase\AutoRegistryBundle\Service
 */
interface RegistryInterface
{
    /** @return RegistryElementInterface[] */
    public function all();
/**
     * @param $key string
     * @return RegistryElementInterface
     */
    public function has($key);
/**
     * @param $key string
     * @return RegistryElementInterface
     */
    public function get($key);
/**
     * @param RegistryElementInterface $element
     */
    public function add(RegistryElementInterface $element);
/**
     * @param RegistryElementInterface $element
     */
    public function remove(RegistryElementInterface $element);
}
