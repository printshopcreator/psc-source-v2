<?php

namespace PSC\Libraries\AutoRegistryBundle\Service;

/**
 * Interface RegistryElementInterface
 * @package ScayTrase\AutoRegistryBundle\Service
 */
interface RegistryElementInterface
{
    /**
     * @return string
     */
    public function getType();
/**
     * @return string
     */
    public function getDescription();
}
