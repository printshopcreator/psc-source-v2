<?php

namespace PSC\Libraries\CalcBundle\Session;

class Calc implements \Serializable
{
    protected $variables = [];
    protected $product = '';
    public function serialize()
    {
        return serialize([
                $this->product,
                $this->variables,
            ]);
    }

    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        list(
            $this->product,
            $this->variables,
            ) = $data;
    }

    /**
     * @return array
     */
    public function getVariables()
    {
        return $this->variables;
    }

    /**
     * @param array $variables
     */
    public function setVariables($variables)
    {
        $this->variables = $variables;
    }

    /**
     * @return null
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param null $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }
}
