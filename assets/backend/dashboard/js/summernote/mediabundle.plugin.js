(function (factory) {
    /* global define */
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else {
        // Browser globals: jQuery
        factory(window.jQuery);
    }
}(function ($){
    $.extend($.summernote.plugins, {
        'media': function(context) {
            var self = this;

            // ui has renders to build ui elements.
            //  - you can create a button with `ui.button`
            var ui = $.summernote.ui;

            // add hello button
            context.memo('button.media', function() {
                // create button
                var button = ui.button({
                    contents: '<i class="fa fa-images"/> Media',
                    tooltip: 'Media',
                    click: function() {
                        mediaBundleBrowser(context.$note.attr('id'));
                    },
                });

                // create jQuery object from button instance.
                var $media = button.render();
                return $media;
            });
        }
    });

}));