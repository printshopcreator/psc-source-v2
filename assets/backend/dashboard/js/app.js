define(function() {

    return {
        // General App init
        init() {

            psc.ajaxModal.init();
            psc.mediaChooser.init();
            psc.order.init();
            if (typeof bootstrapApp === 'function') {
                bootstrapApp();
            }
        }
    }
});

$(function() {

    $('a[data-bs-toggle="tab"]').on('shown.bs.tab', function (e) {
        if($(e.target).parent().parent().parent().attr('id') && $(e.target).parent().parent().parent().attr('id') == 'doc') {
            localStorage.setItem('lastTabDoc', $(this).attr('href'));
        }else{
            localStorage.setItem('lastTab', $(this).attr('href'));
        };

    });

    // go to the latest tab, if it exists:
    var lastTab = localStorage.getItem('lastTab');
    if (lastTab) {
        $('[href="' + lastTab + '"]').tab('show');
    }

    var lastTabDoc = localStorage.getItem('lastTabDoc');
    if (lastTabDoc) {
        $('[href="' + lastTabDoc + '"]').tab('show');
    }

    $('.nav li ul').parent().find('a').click(function() {
        $(this).parent().find("ul").toggle();
    });

    psc.app.init();

    $('input[data-bs-toggle="toggle"], input.ajaxToogle').change(function() {
        $.getJSON($(this).data('url'));
    });

    $('select.multiple').multiselect({
        submitAllLeft: false,
        submitAllRight: true,
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
        },
        fireSearch: function(value) {
            return value.length > 1;
        }
    });

    $('.summernote').summernote({
        height: 400,
        toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'italic', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']],
                ['table', ['table']],
                ['insert', ['link', 'hr']],
                ['view', ['fullscreen', 'codeview']],
                ['help', ['help']],
                ['media', ['media']]
        ]
    });
});