define(function() {

    return {

        init() {
            this.urlChooser();
            this.initDelBtn();
        },

        // Del btn
        initDelBtn() {
            $('body').on('click', '.js-media-chooser-del-preview-btn', function (e) {
                var $this = $(this),
                    linkedID = $this.data('linked-id'),
                    $widget = $('#' + linkedID + '-widget'),
                    $input = $('#' + linkedID);

                $this.parent().find('.media-chooser__preview').remove();

                $(".media-thumbnail__icon").remove();

                $widget.removeClass('media-chooser--choosen');
                $input.val('');
            });
        },

        // URL-Chooser
        urlChooser() {

            // Link Chooser select
            $('body').on('click', '.js-url-chooser-link-select', function (e) {
                e.preventDefault();

                var $this = $(this),
                    slug = $this.data('slug'),
                    id = $this.data('id'),
                    replaceUrl = $this.closest('nav').data('replace-url');

                // Store values
                itemUrl = (slug ? slug : '');
                itemId = id;

                // Replace URL
                $.ajax({
                    url: replaceUrl,
                    type: 'GET',
                    data: {'text': itemUrl},
                    success: function (response) {
                        replacedUrl = response.text;

                        // Update preview
                        $('#url-chooser__selection-preview').text('Selection: ' + replacedUrl);
                    }
                });
            });

            // Media Chooser select
            $('body').on('click', '.js-url-chooser-media-select', function (e) {
                e.preventDefault();

                var $this = $(this),
                    path = $this.data('path'),
                    thumbPath = $this.data('thumb-path'),
                    id = $this.data('id'),
                    title = $this.data('title'),
                    cke = $this.data('cke'),
                    replaceUrl = $this.closest('.thumbnail-wrapper').data('replace-url');

                // Store values
                itemUrl = path;
                itemId = id;
                itemTitle = title;
                itemThumbPath = thumbPath;

                // Save
                if (!cke) {
                    var isMediaChooser = $(window.frameElement).closest('.js-ajax-modal').data('media-chooser');

                    if (isMediaChooser) {
                        psc.mediaChooser.saveMediaChooserModal(false);
                    } else {
                        // Replace URL
                        $.ajax({
                            url: replaceUrl,
                            type: 'GET',
                            data: {'text': itemUrl},
                            success: function (response) {
                                replacedUrl = response.text;
                            }
                        }).done(function () {
                            psc.mediaChooser.saveUrlChooserModal(false);
                        });
                    }

                } else {
                    psc.mediaChooser.saveMediaChooserModal(true);
                }
            });


            // Cancel
            $('#cancel-url-chooser-modal').on('click', function () {
                var cke = $(this).data('cke');

                if (!cke) {
                    var $parentModal = $(window.frameElement).closest('.js-ajax-modal'),
                        parentModalId = $parentModal.attr('id');

                    parent.$('#' + parentModalId).modal('hide');

                } else {
                    window.close();
                }
            });


            // OK
            $(document).on('click', '#save-url-chooser-modal', function () {
                var cke = $(this).data('cke');

                psc.mediaChooser.saveUrlChooserModal(cke);
            });
        },

        // Save for URL-chooser
        saveUrlChooserModal(cke) {
            if (!cke) {
                var $parentModal = $(window.frameElement).closest('.js-ajax-modal'),
                    linkedInputId = $parentModal.data('linked-input-id'),
                    parentModalId = $parentModal.attr('id');

                // Set val
                parent.$('#' + linkedInputId).val(itemUrl).change();

                // Set proper URL
                parent.$('#' + linkedInputId).parent().find('.js-urlchooser-value').val(replacedUrl);

                // Close modal
                parent.$('#' + parentModalId).modal('hide');

            } else {
                var funcNum = getUrlParam('CKEditorFuncNum');

                // Set val
                window.opener.CKEDITOR.tools.callFunction(funcNum, itemUrl);

                // Close window
                window.close();
            }
        },


        // Save for Media-chooser
        saveMediaChooserModal(cke) {
            if (!cke) {
                var $parentModal = $(window.frameElement).closest('.js-ajax-modal'),
                    linkedInputId = $parentModal.data('linked-input-id'),
                    parentModalId = $parentModal.attr('id');

                // Set val
                parent.$('#' + linkedInputId).val(itemId).change();

                // Update preview
                var $mediaChooser = parent.$('#' + linkedInputId + '-widget'),
                    $previewImg = parent.$('#' + linkedInputId + '__preview__img'),
                    $previewTitle = parent.$('#' + linkedInputId + '__preview__title');

                $mediaChooser.addClass('media-chooser--choosen');
                $previewTitle.html(itemTitle);

                if (itemThumbPath === "") {
                    var $parent = $previewTitle.parent();
                    $parent.prepend('<i class="fa fa-file-o media-thumbnail__icon"></i>');
                } else {
                    $previewImg.attr('src', itemThumbPath);
                }

                // Close modal
                parent.$('#' + parentModalId).modal('hide');

            } else {
                var funcNum = getUrlParam('CKEditorFuncNum');

                // Set val
                window.opener.CKEDITOR.tools.callFunction(funcNum, itemUrl);

                // Close window
                window.close();
            }
        },


        // Get Url Parameters
        getUrlParam(paramName) {
            var reParam = new RegExp('(?:[\?&]|&amp;)' + paramName + '=([^&]+)', 'i'),
                match = window.location.search.match(reParam);

            return (match && match.length > 1) ? match[1] : '';
        }

    }
});
