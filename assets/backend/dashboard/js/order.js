define(function() {
    const Order_List_Detail = ({ uuid, basketField1, customerInfo, specialProductTypeObject, basketField2, pos, count, allGross, product, status, allNet }, orderUuid) => `
        <tr style="${psc.order.get_pos_bg_color(status)}" class="${psc.order.get_pos_bg_color_class(status)}">
        <td>-</td>
        <td></td>
        <td>${pos}</td>
        <td colspan="4"><a href="/apps/backend/product/edit/index/${product.uuid}">${product.title}</a><br/>ArtNr intern: ${product.nrIntern}<br/>
        ${psc.order.get_special_product_options(specialProductTypeObject)}
        </td>
        <td>Auflage: ${count}</td>
        <td>Kunden Info: ${customerInfo}</td>
        <td>${basketField1}</td>
        <td>${basketField2}</td>
        <td></td>
        <td class="text-end">${new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(allNet)} <strong>(${new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(allGross)})</strong></td>
        <td><div class="btn-group btn-group-sm">
                <button type="button" class="btn btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ${psc.order.get_pos_status(status).internalName}
                </button>
                <div class="dropdown-menu">
                    ${psc.order.get_pos_status_loop(orderUuid, uuid)}
                </div>
            </div></td>
        <td></td>
    </tr>
    `;

    const Order_List_Detail_Simple = ({ uuid, basketField1, customerInfo, specialProductTypeObject, basketField2, pos, count, allGross, product, status, allNet }, orderUuid) => `
        <tr style="${psc.order.get_pos_bg_color(status)}" class="${psc.order.get_pos_bg_color_class(status)}">
        <td>-</td>
        <td></td>
        <td>${pos}</td>
        <td colspan="4"><a href="/apps/backend/product/edit/index/${product.uuid}">${product.title}</a><br/>ArtNr intern: ${product.nrIntern}<br/>
        ${psc.order.get_special_product_options(specialProductTypeObject)}</td>
        <td>Auflage: ${count}</td>
        <td>Kunden Info: ${customerInfo}</td>
        <td>${basketField1}</td>
        <td>${basketField2}</td>
        <td></td>
        <td class="text-end">${new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(allNet)} <strong>(${new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' }).format(allGross)})</strong></td>
        <td>${psc.order.get_pos_status(status).internalName}</td>
        <td></td>
    </tr>
    `;

    const Order_List_Pos_Status = (status, orderUuid, posUuid) => `
        <button type="button" class="dropdown-item switch_pos" data-order-uuid="${orderUuid}" data-pos-uuid="${posUuid}" data-status="${status.code}">${status.internalName}</button>
    `;

    const Order_List_Pos_Calc_Option = (elm) => `
        <tr><td>${elm.name}</td><td>${elm.value}</td></tr>
    `;
    
    return {
        
        init() {

            this.load_status();
            this.bind_buttons();
        },

        get_special_product_options(specialProductTypeObject) {
            if(specialProductTypeObject.typ != 6) {
                return '';
            }

            return '<table>' + specialProductTypeObject.options.map(elm => {
                if(elm.type != 'hidden') {
                    return Order_List_Pos_Calc_Option(elm);
                }
            }).join('') + '</table>';
        },

        load_status() {
            $.ajax({
                url: "/apps/api/system/status",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + jwt_token
                },
                method: 'GET',
                success: function(result) {
                    psc.order.status_order = result.order;
                    psc.order.status_pos = result.position;
                }
            });
        },

        bind_buttons() {
            $('.toogle_list_detail_view').off();
            $('.toogle_list_detail_view').on('click', function() {
                var uuid = $(this).data().uuid; 
                if($('#rows-' + uuid + ' > tr').length > 0) {
                    $('#rows-' + uuid).html('');
                }else{
                    psc.order.load_detail(uuid);
                }
            });

            $('.toogle_list_simple_detail_view').off();
            $('.toogle_list_simple_detail_view').on('click', function() {
                var uuid = $(this).data().uuid; 
                if($('#rows-' + uuid + ' > tr').length > 0) {
                    $('#rows-' + uuid).html('');
                }else{
                    psc.order.load_detail(uuid, true);
                }
            });

            
        },

        load_detail(orderUuid, simple = false) {
            $.ajax({
                url: "/apps/api/order/getonebyuuid",
                contentType: "application/json",
                headers: {
                    "Authorization": "Bearer " + jwt_token
                },
                method: 'POST',
                data: JSON.stringify({
                    uuid: orderUuid
                }),
                success: function(result) {
                    psc.order.build_details(orderUuid, result, simple);
                }
            })
        },

        build_details(orderUuid, result, simple = false) {
            if(simple) {
                $('#rows-' + result.uuid).html(result.positions.map(x => Order_List_Detail_Simple(x, orderUuid)).join(''));
            }else{
                $('#rows-' + result.uuid).html(result.positions.map(x => Order_List_Detail(x, orderUuid)).join(''));
            }
            
            this.bind_status_buttons();
        },

        bind_status_buttons() {
            $('.switch_pos').off();
            $('.switch_pos').on('click', function() {

                var orderUuid = $(this).data().orderUuid;
                var posUuid = $(this).data().posUuid;
                var status = $(this).data().status;

                $.ajax({
                    url: "/apps/api/position/status/change",
                    contentType: "application/json",
                    headers: {
                        "Authorization": "Bearer " + jwt_token
                    },
                    data: JSON.stringify({
                        position: posUuid,
                        status: status,
                    }),
                    method: 'POST',
                    success: function(result) {
                        $('#rows-' + orderUuid).html('');
                        psc.order.load_detail(orderUuid);
                    }
                });
            });
        },

        get_pos_bg_color(status) {

            /*
            {% if orderStatuse.getPosStatusColor(pos.status) != '' %}background-color: {{ orderStatuse.getPosStatusColor(pos.status) }}{% endif %}"
            */
            const found = psc.order.status_pos.find(stat => stat.code == status);
            
            if(found && found.color != "") {
                return 'background-color: ' + found.color;
            }
            
        },

        get_pos_status(status) {

            /*
            {% if orderStatuse.getPosStatusColor(pos.status) != '' %}background-color: {{ orderStatuse.getPosStatusColor(pos.status) }}{% endif %}"
            */
            const found = psc.order.status_pos.find(stat => stat.code == status);
            if(found) {
                return found;
            }

            return {internalName: 'notFound'};

        },

        get_pos_bg_color_class(status) {

            /*
            class="{% if pos.status == 170 %}bg-lightdark{% elseif pos.status == 200 or pos.status == 210 %}bg-lightsuccess{% else %}bg-lightdanger{% endif %}
            */

            if(status == 170) {
                return 'bg-lightdark';
            }
            if(status == 200 || status == 210) {
                return 'bg-lightsuccess';
            }

            return 'bg-lightdanger';
        },

        get_pos_status_loop(orderUuid, posUuid) {
            return psc.order.status_pos.map(x => Order_List_Pos_Status(x, orderUuid, posUuid)).join('');
        }
        
    }
});