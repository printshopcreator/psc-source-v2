global.$ = global.jQuery = window.jQuery = require('jquery');
require('@popperjs/core/dist/cjs/popper');
require('bootstrap/dist/js/bootstrap');
require('./js/tether.min');
require('./js/dropzone');
global.Raphael = require('raphael/raphael.min');
require('morris.js/morris.min');
require('summernote/dist/summernote-bs4');
require('./js/summernote/mediabundle.plugin');
require('multiselect-two-sides/dist/js/multiselect');
require('bootstrap-toggle/js/bootstrap-toggle.min');
import 'ace-builds';
import 'ace-builds/webpack-resolver';
global.Dropzone = require('./js/dropzone');

global.cargobay = {};
global.cargobay.general = require('./js/tools/cargobay.general.min');
global.cargobay.toggle =  require('./js/tools/cargobay.toogle.min');

global.psc = {};
global.psc.ajaxModal = require('./js/tools/ajaxModal');
global.psc.mediaChooser = require('./js/tools/mediaChooser');
global.psc.app = require('./js/app');
global.psc.order = require('./js/order.js');

global.psc.app.init();

import './js/tools/mediaChooser';
import 'bootstrap/scss/bootstrap.scss';
import './css/tools/cargobay.toogle.min.css';
import './less/base.less';
import 'summernote/dist/summernote-bs4.min.css';
//import 'multiselect/css/multi-select.css';
import 'bootstrap-toggle/css/bootstrap-toggle.min.css';