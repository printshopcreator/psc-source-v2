var Encore = require('@symfony/webpack-encore');
const glob = require('glob');
// Manually configure the runtime environment if not already configured yet by the "encore" command.
// It's useful when you use tools that rely on webpack.config.js file.
if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev');
}

function getEntries (){
    return glob.sync("./var/plugins/System/**/*/Webpack/index.{js,ts}");
}

Encore
    // directory where compiled assets will be stored
    .setOutputPath('web/build/')
    // public path used by the web server to access the output path
    .setPublicPath('/apps/build')
    // only needed for CDN's or sub-directory deploy
    .setManifestKeyPrefix('/apps')

    .addEntry('backend/dashboard', './assets/backend/dashboard/dashboard.js')

    .splitEntryChunks()

    .enableSingleRuntimeChunk()

    .cleanupOutputBeforeBuild()
    .enableBuildNotifications()
    .enableSourceMaps(!Encore.isProduction())
    // enables hashed filenames (e.g. app.abc123.css)
    .enableVersioning(Encore.isProduction())

    // enables @babel/preset-env polyfills
    .configureBabelPresetEnv((config) => {
        config.useBuiltIns = 'usage';
        config.corejs = 3;
    })

    // enables Sass/SCSS support
    .enableSassLoader()
    .enableLessLoader()
    // uncomment if you use TypeScript
    .enableTypeScriptLoader()
;
for (const entry of getEntries()) {
    const splEntry = entry.split('/');
    Encore.addEntry('plugins/' + splEntry[3].toLowerCase() + '/' + splEntry[4].toLowerCase() + '/' + splEntry[5].toLowerCase(), entry);
}

module.exports = Encore.getWebpackConfig();
